package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.codeholic.kotumb.app.Model.AppLanguage;
import com.codeholic.kotumb.app.R;

import java.util.ArrayList;

public class LanguageAdapter extends BaseAdapter {
    private final ArrayList<AppLanguage> languages;
    private final Activity activity;
    private AppLanguage selectedLang;
    private RadioButton lastCheckBtn;
    private String oldLang;
    private ArrayList<RadioButton> buttons = new ArrayList<>();

    public LanguageAdapter(ArrayList<AppLanguage> languages, Activity activity) {
        this.languages = languages;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return languages.size();
    }

    @Override
    public Object getItem(int i) {
        return languages.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(activity).inflate(R.layout.language_item, viewGroup, false);
        final RadioButton radioButton = v.findViewById(R.id.radio);
        if (oldLang.equalsIgnoreCase(languages.get(i).getLang_code())) {
            radioButton.setChecked(true);
            lastCheckBtn = radioButton;
        } else {
            radioButton.setChecked(false);
        }
        radioButton.setText(languages.get(i).getLanguage());
        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (lastCheckBtn != null && b) {
                    lastCheckBtn.setChecked(false);
                    lastCheckBtn = radioButton;
                    selectedLang = languages.get(i);
                    System.out.println("Checked Language  "+selectedLang.getLanguage());
                    uncheckAll();
                    radioButton.setChecked(true);
                }
            }
        });
        buttons.add(radioButton);
        return v;
    }

    private void uncheckAll() {
        for (RadioButton button : buttons) {
            button.setChecked(false);
        }
    }

    public AppLanguage getSelectedLang() {
        return selectedLang;
    }

    public void setSelectedLang(AppLanguage selectedLang) {
        this.selectedLang = selectedLang;
    }

    public void setOldLang(String oldLang) {
        this.oldLang = oldLang;
    }
}
