package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Adapter.InviteForGroupRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.View.DividerItemDecoration;
import com.shahroz.svlibrary.interfaces.onSearchListener;
import com.shahroz.svlibrary.interfaces.onSimpleSearchActionsListener;
import com.shahroz.svlibrary.widgets.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class GroupInviteActivity extends AppCompatActivity implements View.OnClickListener, onSimpleSearchActionsListener, onSearchListener {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.tvNouser)
    TextView tvNouser;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.fabAddConnections)
    FloatingActionButton fabAddConnections;

    @BindView(R.id.adView)
    ImageView adView;
    @BindView(R.id.rlAdView)
    RelativeLayout rlAdView;
    public static ArrayList<String> admins = new ArrayList<>();
    public static ArrayList<String> members = new ArrayList<>();

    List<User> users = new ArrayList<>();
    InviteForGroupRecyclerViewAdapter inviteForGroupRecyclerViewAdapter;
    int pagination = 0;
    private LinearLayoutManager linearLayoutManager;
    Group group = new Group();
    com.codeholic.kotumb.app.View.ProgressBar progressBar = null;
    private boolean mSearchViewAdded = false;
    private MaterialSearchView mSearchView;
    private WindowManager mWindowManager;
    private MenuItem searchItem;
    private boolean searchActive = false;
    private boolean search = true;
    private String queryText = "";
    private static ArrayList<String> selectedUserIds = new ArrayList<>();

    // onCreate method initializing view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connections_group_invite);
        ButterKnife.bind(this);
        init();
        initToolbar();
        //setView();
        onSearch("");
        setClickListener();
//        Toast.makeText(this, "members: "+members.size()+" admins:"+admins.size(), Toast.LENGTH_SHORT).show();
        /*IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.UPDATE_USER);
        filter.addAction(ResponseParameters.REQUEST_SEND);
        registerReceiver(receiver, filter);*/

        IntentFilter filterAdd = new IntentFilter();
        filterAdd.addAction(ResponseParameters.ADD_RECEIVED);
        registerReceiver(getAd, filterAdd);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //clearSelection();
    }

    public static void clearSelection() {
        members.clear();
        admins.clear();
    }


    @Override
    public void onBackPressed() {
        clearSelection();
        super.onBackPressed();
    }

    private void setClickListener() {
        fabAddConnections.setOnClickListener(this);
    }

    private BroadcastReceiver getAd = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            try {
//                HomeScreenActivity.setUpAdImage(intent.getStringExtra(ResponseParameters.ADD_RECEIVED), rlAdView, adView, GroupInviteActivity.this, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void setUpAdImage(String adResponse) {
        hideAdView();
        try {
            if (!adResponse.isEmpty()) {
                final JSONObject ad = new JSONObject(adResponse);
                if (ad.has(ResponseParameters.AD)) {
                    if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.IMAGE)) {
                        if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE).isEmpty()) {
                            rlAdView.setVisibility(VISIBLE);
                            new AQuery(this).id(adView).image(API.imageAdUrl + ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE), true, true, 300, R.drawable.ic_user);
                            adView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.URL)) {
                                            if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL).isEmpty()) {
                                                Utility.prepareCustomTab(GroupInviteActivity.this, ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL));
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideAdView() {
        rlAdView.setVisibility(GONE);
    }

    //init data of user
    private void init() {
        if (getIntent().hasExtra(ResponseParameters.CREATE_GROUP)) {
            group.groupId = "0";
        } else {
            group = (Group) getIntent().getSerializableExtra(ResponseParameters.GROUP);
        }

    }

    //init toolbar function
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.header_main_menu_option3));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        mSearchView = new MaterialSearchView(this);
        mSearchView.setOnSearchListener(this);
        mSearchView.setSearchResultsListener(this);
        mSearchView.setHintText(getResources().getString(R.string.invitations_search));


        if (mToolbar != null) {
            // Delay adding SearchView until Toolbar has finished loading
            mToolbar.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!mSearchViewAdded && mWindowManager != null) {
                            if (mWindowManager != null){
                                mWindowManager.addView(mSearchView,
                                        MaterialSearchView.getSearchViewLayoutParams(GroupInviteActivity.this));
                            }

                            mSearchViewAdded = true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    @Override
    public void onSearch(String query) {
        Log.e("onSearch", "onSearch " + searchActive);
        if (search) {
            try {
                if (!Utility.isConnectingToInternet(this)) { // checking net connection
                    llLoading.setVisibility(GONE);
                    if (query.trim().isEmpty() || users.size() <= 0) {
                        llNotFound.setVisibility(View.VISIBLE);
                        tvNouser.setText(getResources().getString(R.string.app_no_internet_error));
                    }
                    final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }

                queryText = query;
                pagination = 0;
                llLoading.setVisibility(VISIBLE);
                llNotFound.setVisibility(View.GONE);
                users.clear();
                linearLayoutManager = new LinearLayoutManager(this);
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                rvUserList.setLayoutManager(linearLayoutManager);
                inviteForGroupRecyclerViewAdapter = new InviteForGroupRecyclerViewAdapter(rvUserList, this, users);
                rvUserList.addItemDecoration(
                        new DividerItemDecoration(this, R.drawable.divider));
                rvUserList.setAdapter(inviteForGroupRecyclerViewAdapter);
                //inviteForGroupRecyclerViewAdapter.setSelectedUsers(members);


                API.sendRequestToServerGET(this, API.FIND_MEMBERS_TO_JOIN_GROUP + (pagination) + "/" + group.groupId + "/" + SharedPreferencesMethod.getUserId(this) + "/" + query, API.FIND_MEMBERS_TO_JOIN_GROUP);// service call for getting connection
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void searchViewOpened() {
        Log.e("searchViewOpened", "searchViewOpened " + searchActive);
        searchActive = true;
        search = true;
    }

    @Override
    public void searchViewClosed() {
        Log.e("searchViewClosed", "searchViewClosed " + queryText);
        if (!queryText.trim().isEmpty()) {
            Log.e("inside if", "inside if");
            onSearch("");
        }
        search = false;
        //Util.showSnackBarMessage(fab,"Search View Closed");
    }

    @Override
    public void onItemClicked(String item) {

    }

    @Override
    public void onScroll() {

    }

    @Override
    public void error(String localizedMessage) {

    }

    @Override
    public void onCancelSearch() {
        Log.e("onCancelSearch", "onCancelSearch");
        searchActive = false;
        mSearchView.hide();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        searchItem = menu.findItem(R.id.search);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                mSearchView.display();
                openKeyboard();
                return true;
            }
        });
        if (searchActive)
            mSearchView.display();
        return true;
    }

    private void openKeyboard() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mSearchView.getSearchView().dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                mSearchView.getSearchView().dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
            }
        }, 200);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }


    //setview for getting user connection
    private void setView() {
        try {
            if (!Utility.isConnectingToInternet(this)) { // checking net connection
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(View.VISIBLE);
                tvNouser.setText(getResources().getString(R.string.app_no_internet_error));
                final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            API.sendRequestToServerGET(this, API.FIND_MEMBERS_TO_JOIN_GROUP + (pagination) + "/" + group.groupId + "/" + SharedPreferencesMethod.getUserId(this), API.FIND_MEMBERS_TO_JOIN_GROUP);// service call for getting connection
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //getResponse for getting list of urlImages
    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        Log.e("response", "" + outPut);

        if (progressBar != null) {
            progressBar.dismiss(); // dismissing progressbar
        }

        try {
            if (i == 0) { // for getting urlImages
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Error)) { // error response
                        users.clear();
                        inviteForGroupRecyclerViewAdapter.notifyDataSetChanged();
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.USERS)) {
                            userList = outPut.getJSONArray(ResponseParameters.USERS);
                        }
                        users = new ArrayList<>();
                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                            user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                            user.setCity(searchResults.getString(ResponseParameters.City));
                            user.setState(searchResults.getString(ResponseParameters.State));
                            user.setUserInfo(searchResults.toString());
                            users.add(user);
                        }
                        //setting options for recycler adapter
                        linearLayoutManager = new LinearLayoutManager(this);
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserList.setLayoutManager(linearLayoutManager);
                        inviteForGroupRecyclerViewAdapter = new InviteForGroupRecyclerViewAdapter(rvUserList, this, users);
                        rvUserList.addItemDecoration(
                                new DividerItemDecoration(this, R.drawable.divider));
                        rvUserList.setAdapter(inviteForGroupRecyclerViewAdapter);
                        //load more setup for recycler adapter
                        inviteForGroupRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                if (users.size() >= 10) {
                                    rvUserList.post(new Runnable() {
                                        public void run() {
                                            users.add(null);
                                            inviteForGroupRecyclerViewAdapter.notifyItemInserted(users.size() - 1);
                                        }
                                    });

                                    API.sendRequestToServerGET(GroupInviteActivity.this, API.FIND_MEMBERS_TO_JOIN_GROUP + (pagination + 10) + "/" + group.groupId + "/" + SharedPreferencesMethod.getUserId(GroupInviteActivity.this) + "/" + queryText, API.FIND_MEMBERS_TO_JOIN_GROUP_UPDATE);// service call for getting connection
                                }
                            }
                        });

                        if (users.size() > 0) {
                            llNotFound.setVisibility(View.GONE);
                        } else {
                            llNotFound.setVisibility(View.VISIBLE);
                        }
                    } else {
                        //no userfound
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } else if (i == 1) {// for updating list
                users.remove(users.size() - 1); // removing of loading item
                inviteForGroupRecyclerViewAdapter.notifyItemRemoved(users.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response

                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.USERS)) {
                            userList = outPut.getJSONArray(ResponseParameters.USERS);
                        }

                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                            user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                            user.setCity(searchResults.getString(ResponseParameters.City));
                            user.setState(searchResults.getString(ResponseParameters.State));
                            user.setUserInfo(searchResults.toString());
                            users.add(user);
                        }
                        if (userList.length() > 0) {
                            pagination = pagination + 6;
                            //updating recycler view
                            inviteForGroupRecyclerViewAdapter.notifyDataSetChanged();
                            inviteForGroupRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            } else if (i == 2) { // for getting urlImages
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        final Snackbar snackbar = Snackbar.make(rvUserList, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        clearSelection();
                        finish();
                    } else {
                        //no userfound
                        final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                        snackbar.show();
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            }

        } catch (Exception e) {
            //mo user found
            llNotFound.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            if (!intent.hasExtra("UPDATE_USER")) { // for not updating from its own adapter
                User updatedUser = intent.getParcelableExtra("USER");
                Log.e("test", "test " + updatedUser.getUserInfo());
                for (int i = 0; i < users.size(); i++) {
                    User user = users.get(i);
                    if (updatedUser.getUserId().equals(user.getUserId())) {
                        try {
                            users.remove(i);
                            users.add(i, updatedUser);
                            inviteForGroupRecyclerViewAdapter.notifyItemChanged(i);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        /*if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }*/
        if (getAd != null) {
            unregisterReceiver(getAd);
            getAd = null;
        }
        super.onDestroy();
    }

    public void changeView() {
        if (members.size() > 0 || admins.size() > 0) {
            fabAddConnections.show();
        } else {
            fabAddConnections.hide();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabAddConnections:
                try {
                    if (!Utility.isConnectingToInternet(this)) { // checking net connection

                        final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }
                    HashMap<String, Object> input = new HashMap();
                    input.put(RequestParameters.GROUPID, "" + group.groupId);

                    String adminString = "";

                    for (String s : admins) {
                        adminString += s + ",";
                    }
                    adminString.substring(0, adminString.length());

                    String memberString = "";

                    for (String s : members) {
                        memberString += s + ",";
                    }
                    if (!adminString.trim().isEmpty())
                        adminString = adminString.substring(0, adminString.length() - 1);
                    if (!memberString.trim().isEmpty())
                        memberString = memberString.substring(0, memberString.length() - 1);
                    if (getIntent().hasExtra(ResponseParameters.CREATE_GROUP)) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(RequestParameters.ADMINS, adminString);
                        returnIntent.putExtra(RequestParameters.MEMBERS, memberString);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else {
                        input.put(RequestParameters.ADMINS, "" + adminString);
                        input.put(RequestParameters.MEMBERS, "" + memberString);
                        input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(this));
                        Log.e("input", "" + input);
                        progressBar = new com.codeholic.kotumb.app.View.ProgressBar(this);
                        progressBar.show(getResources().getString(R.string.app_please_wait_text));
                        API.sendRequestToServerPOST_PARAM(this, API.GROUP_INVITE_MEMBER, input);// service call for getting connection
                        Utils.logEventGroupInviteSend(GroupInviteActivity.this, group.groupId);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
