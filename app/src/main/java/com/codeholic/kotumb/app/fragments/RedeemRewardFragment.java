package com.codeholic.kotumb.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codeholic.kotumb.app.Model.RewardObject;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;

import static com.codeholic.kotumb.app.Utility.ResponseParameters.REDEEM_REWARD;

public class RedeemRewardFragment extends Fragment {

    RecyclerView rewardRCView;
    private CurrentRewardFragment.RewardListAdapter rewardListAdapter;
    Activity activity;
    ArrayList<RewardObject> currentRewardList = new ArrayList<>();
    RelativeLayout loadingView;
    TextView nodataText;
    TextView tvRedeemCount;
    LinearLayout laout_redeemcount;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reward, container, false);
        ButterKnife.bind(activity);
        rewardRCView = view.findViewById(R.id.fragment_list_rv);
        nodataText = view.findViewById(R.id.noDataText);
        tvRedeemCount = view.findViewById(R.id.tv_redeemCount);
        laout_redeemcount = view.findViewById(R.id.laout_redeemcount);
        loadingView = view.findViewById(R.id.loadingLayout);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        rewardRCView.setLayoutManager(linearLayoutManager);
        rewardRCView.setHasFixedSize(true);
        rewardListAdapter = new CurrentRewardFragment.RewardListAdapter(new ArrayList<>(), "Redeem");
        rewardRCView.setAdapter(rewardListAdapter);

        laout_redeemcount.setVisibility(View.VISIBLE);;
        tvRedeemCount.setText(""+SharedPreferencesMethod.getInt(activity,"REDEEM_TOTAL"));

        getCurrentReward();
        return view;
    }

    private void getCurrentReward(){
        loadingView.setVisibility(View.VISIBLE);
//        API.sendRequestToServerGET_FRAGMENT(activity,this,API.REDEEEM_POINTS + "/"+ SharedPreferencesMethod.getUserId(activity)+"/redeem",API.REDEEEM_POINTS);
        API.sendRequestToServerGET_FRAGMENT(activity,this,API.REDEEEM_POINTS + "/"+ SharedPreferencesMethod.getUserId(activity)+"/redeem",API.REDEEEM_POINTS);
    }

    public void getResponse(JSONObject outPut){
        loadingView.setVisibility(View.GONE);

        try {
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Errors)) {
//                    final Snackbar snackbar = Snackbar.make(getView(), Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
//                    snackbar.show();
                    nodataText.setVisibility(View.VISIBLE);
                } else {
                    parseResponse(outPut);
                }
            } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                nodataText.setVisibility(View.VISIBLE);
//                final Snackbar snackbar = Snackbar.make(getView(), getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
//                snackbar.show();
            } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                final Snackbar snackbar = Snackbar.make(getView(), getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
            }


        } catch (Exception e) {
            Log.e("REWARD","Exception is : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void parseResponse(JSONObject jsonObject){
        try {
            if (jsonObject.has(REDEEM_REWARD)){

                currentRewardList.clear();

                JSONArray parent = jsonObject.getJSONArray(REDEEM_REWARD);
                for (int i=0;i<parent.length();i++){
                    JSONObject child = parent.getJSONObject(i);
                    RewardObject rewardObject = new RewardObject(child.getString(ResponseParameters.POINTS),child.getString(ResponseParameters.REWARDTYPE),child.getString(ResponseParameters.CREATED_AT));
                    currentRewardList.add(rewardObject);
                    rewardListAdapter.addItem(currentRewardList);
                    rewardListAdapter.notifyDataSetChanged();
                }

                // int points = Integer.parseInt(jsonObject.getString(ResponseParameters.POINTS));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
