package com.codeholic.kotumb.app.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Activity.ShowVideos;
import com.codeholic.kotumb.app.Adapter.PublishVideosAdapter;
import com.codeholic.kotumb.app.Model.VideoData;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PublishedVideosFragment extends Fragment implements ShowVideos.VideoInterface {

    List<VideoData> videoDataList=new ArrayList<>();
    private Toolbar mToolbar;
    private TextView header,tv_video_notfound;
    int pagination = 0;
    boolean loading=true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private RecyclerView rv_video_list;
    private LinearLayoutManager linearLayoutManager = null;
    private PublishVideosAdapter userVideoListAdapter;
    private LinearLayout llLoading,mainLayout,llDataNotFound;
    private ProgressBar pagination_loader;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String URL="";
    View view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((ShowVideos)getActivity()).registerData(0,(ShowVideos.VideoInterface) this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_user_video_list, container, false);
        findViews();
        return view;
    }


    private void findViews(){
        mToolbar=view.findViewById(R.id.toolbar_actionbar);
        mToolbar.setVisibility(View.GONE);
        header=view.findViewById(R.id.header);
        header.setVisibility(View.GONE);
        rv_video_list=view.findViewById(R.id.rv_video_list);
        swipeRefreshLayout=view.findViewById(R.id.swipelayout);
        mainLayout=view.findViewById(R.id.mainLayout);
        llLoading=view.findViewById(R.id.llLoading);
        llDataNotFound=view.findViewById(R.id.llDataNotFound);
        tv_video_notfound=view.findViewById(R.id.tv_video_notfound);
        pagination_loader=view.findViewById(R.id.pagination_loader);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                videoDataList.clear();
                initStartups();
            }
        });
        if(isNetworkAvailable()) {
            initStartups();
        }
        else{
            Utils.showPopup(getActivity(),getActivity().getResources().getString(R.string.app_no_internet_error));
        }
    }

    private void initStartups(){

        videoDataList.clear();
//        URL=  API.MY_VIDEO_GET+ SharedPreferencesMethod.getUserId(context);
//        API.sendRequestToServerGET(context, URL+"/"+pagination, API.MY_VIDEO_GET);
//        initToolbar("My Video List");
//        onRefresh(URL,API.MY_VIDEO_GET);
        URL= API.base_api+"services/user_published_video/"+ SharedPreferencesMethod.getUserInfo(getActivity()).getUserId();
        System.out.println("Url=="+URL);
        AQuery aQuery=new AQuery(getActivity());
        aQuery.ajax(URL, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                JSONObject outPut=json;
                System.out.println("output=="+outPut);
                if (outPut.has(ResponseParameters.Success)){
                    llLoading.setVisibility(View.GONE);
                    mainLayout.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                    if (videoDataList.size()!=0){
                        videoDataList.clear();
                    }
                    try{
                        if (outPut.getJSONArray("published_videos").length()>0){
                            for (int j=0; j<outPut.getJSONArray("published_videos").length(); j++){
                                JSONObject videoObj = outPut.getJSONArray("published_videos").getJSONObject(j);
                                VideoData videoData=new VideoData();
                                System.out.println("Video Array Response    "+videoObj.getString(ResponseParameters.TITLE));
                                videoData.setUserFirstName(videoObj.getString(ResponseParameters.FirstName));
                                videoData.setUserLastName(videoObj.getString(ResponseParameters.LastName));
                                videoData.setVideoTitle(videoObj.getString(ResponseParameters.TITLE));
                                videoData.setVideoDescription(videoObj.getString(ResponseParameters.DESCRIPTION));
                                videoData.setVideoName(videoObj.getString(ResponseParameters.MEDIA));
                                videoData.setLike(videoObj.getString("like_status"));
                                videoData.setVideoType(videoObj.getString(ResponseParameters.MEDIA_TYPE));
                                videoData.setUserAvatar(videoObj.getString(ResponseParameters.Avatar));
                                videoData.setVideoUploadedDate(videoObj.getString(ResponseParameters.CREATED_AT));
                                videoData.setVideoEndDate(videoObj.getString(ResponseParameters.END_DATE));
                                videoData.setVideoId(videoObj.getString(ResponseParameters.Id));
                                videoData.setUserId(videoObj.getString(ResponseParameters.UserId));
                                videoData.setVideoLikeCount(videoObj.getString(ResponseParameters.LIKE_COUNT));
                                videoData.setVideoStatus(videoObj.getString(ResponseParameters.STATUS));
                                videoData.setVideoApproved(videoObj.getString(ResponseParameters.Approved));
                                videoData.setVideoPublish(videoObj.getString(ResponseParameters.Publish));
                                videoData.setVideoThumbnail(videoObj.getString(ResponseParameters.THUMBNAIL));
                                videoDataList.add(videoData);
                            }
                            linearLayoutManager = new LinearLayoutManager(getActivity());
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rv_video_list.setLayoutManager(linearLayoutManager);
                            userVideoListAdapter = new PublishVideosAdapter(getActivity(), videoDataList,getActivity());
                            userVideoListAdapter.notifyDataSetChanged();
                            rv_video_list.setAdapter(userVideoListAdapter);
                        }else{
                            llLoading.setVisibility(View.GONE);
                            llDataNotFound.setVisibility(View.VISIBLE);
                            String s=outPut.getString("message");
                            tv_video_notfound.setText(s);
                        }
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }else{
                    swipeRefreshLayout.setRefreshing(false);
                    llLoading.setVisibility(View.GONE);
                    llDataNotFound.setVisibility(View.VISIBLE);
                    tv_video_notfound.setText(getResources().getString(R.string.video_server_error_text));
                    System.out.println("Video List Response Error  "+outPut.toString());
                }
            }
        });

      /*  if (intent.hasExtra("my_video")){
            URL=  API.MY_VIDEO_GET+ SharedPreferencesMethod.getUserId(context);
            API.sendRequestToServerGET(context, URL+"/"+pagination, API.MY_VIDEO_GET);
            initToolbar("My Video List");
            onRefresh(URL,API.MY_VIDEO_GET);
        }else if(intent.hasExtra("other_video")){
            API.sendRequestToServerGET(context, API.VIDEO_PUBLISH,API.VIDEO_PUBLISH);
            initToolbar("My Publish Video List");
            onRefresh(API.VIDEO_PUBLISH,API.VIDEO_PUBLISH);
        }*/

    }

    @Override
    public void onStop() {
        if(videoDataList.size()>0)
        userVideoListAdapter.pauseAllVideo();
        super.onStop();
    }

    @Override
    public void StopAllVideos() {
        if(videoDataList.size()>0)
        userVideoListAdapter.stopVideo();
    }

    @Override
    public void PauseAllVideos() {
        if(videoDataList.size()>0)
        userVideoListAdapter.pauseAllVideo();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
