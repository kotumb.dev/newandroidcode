package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.GroupPostActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.fragments.GroupFragment;

import java.util.HashMap;
import java.util.List;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class GroupListRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Group> groups;
    Activity activity;
    Fragment connectionFragment;
    AlertDialog alert;


    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;


    public void setImageBaseUrl(String base_url) {
        this.base_url = base_url;
    }

    String base_url = "";
    int type = 0;
    boolean share = false;
    Post post;

    public void setSharePost(boolean share, Post post) {
        this.share = share;
        this.post = post;
    }

    public GroupListRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<Group> groups, Fragment connectionFragment) {
        this.groups = groups;
        this.activity = activity;
        this.connectionFragment = connectionFragment;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }


    @Override
    public int getItemViewType(int position) {
        return groups.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return groups.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.group_list_item, parent, false);
            GroupViewHolder GroupViewHolder = new GroupViewHolder(view);
            return GroupViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
    }

    public void setType(int type) {
        this.type = type;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {
        Group group = groups.get(position);
        if (viewholder instanceof GroupViewHolder) {
            GroupViewHolder holder = (GroupViewHolder) viewholder;
            holder.tvUserName.setText("" + group.groupName.trim());
            if (type == 1) { // public group
                if (group.status.trim().equalsIgnoreCase("1")) {
                    holder.tvJoin.setText(activity.getResources().getString(R.string.group_requested_text));
                } else if (group.status.trim().equalsIgnoreCase("4")) {
                    holder.tvJoin.setText(activity.getResources().getString(R.string.group_invited_text));
                } else if (groups.get(position).status.trim().equalsIgnoreCase("0")) {
                    holder.tvJoin.setText(activity.getResources().getString(R.string.group_join_text));
                }
                holder.llJoinContainer.setVisibility(View.VISIBLE);
                holder.tvGroupPrivacy.setVisibility(View.GONE);
                holder.tvGroupType.setVisibility(View.GONE);
            } else {
                holder.llJoinContainer.setVisibility(View.GONE);
                if (group.privacy.trim().equalsIgnoreCase("1")) {
                    holder.tvGroupPrivacy.setText("  |  " + activity.getResources().getString(R.string.group_privacy_text_public));
                    holder.tvGroupPrivacy.setVisibility(View.VISIBLE);
                } else {
                    holder.tvGroupPrivacy.setText("  |  " + activity.getResources().getString(R.string.create_group_type_drpodown_option_private));
                    holder.tvGroupPrivacy.setVisibility(View.VISIBLE);
                }

                if (group.role.trim().equalsIgnoreCase("0")) {
                    holder.tvGroupType.setText(activity.getResources().getString(R.string.group_list_group_type));
                    holder.tvGroupType.setVisibility(View.VISIBLE);
                } else {
                    holder.tvGroupType.setVisibility(View.GONE);
                }
            }


            try {
              //  int parseInt = Integer.parseInt(group.category);
                holder.tvGroupSummery.setText(group.category_name);
            } catch (NumberFormatException e) {
                holder.tvGroupSummery.setText(group.category);
                e.printStackTrace();
            }
            holder.tvMemberCount.setText(group.members_count + " " + activity.getResources().getString(R.string.group_member_text));

            if(base_url.isEmpty()){
                base_url = Utils.base_url_group;
            }

            new AQuery(activity).id(holder.ivGroupImage).image(base_url + group.logo.trim(), true, true, 300, R.drawable.ic_user);
           /* if (group.getProfileHeadline() != null && !group.getProfileHeadline().trim().equalsIgnoreCase("null") && !group.getProfileHeadline().trim().isEmpty()) {
                holder.tvGroupSummery.setText("" + group.getProfileHeadline().trim());
                holder.tvGroupSummery.setVisibility(View.VISIBLE);
            } else {
                holder.tvGroupSummery.setVisibility(View.GONE);
            }
            if (group.getProfileSummary() != null && !group.getProfileSummary().trim().equalsIgnoreCase("null") && !group.getProfileSummary().trim().isEmpty()) {
                holder.tvAbout.setText("" + group.getProfileSummary().trim());
                holder.tvAbout.setVisibility(View.VISIBLE);
            } else {
                holder.tvAbout.setText("");
            }
            if (group.getAadhaarInfo().equalsIgnoreCase("true")) {
                holder.tvAadharVerified.setVisibility(View.VISIBLE);
            } else {
                holder.tvAadharVerified.setVisibility(View.GONE);
            }
            holder.tvMemberCount.setText(group.getCity().trim() + ", " + group.getState().trim());
            */
            setOnClickListener(holder, group, position);

        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    public void setOnClickListener(final GroupViewHolder holder, final Group group, final int position) {
        holder.rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (share) {
                    if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                        Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                        return;
                    }

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(activity.getString(R.string.share_in) +" "+ group.groupName + "?");
                    builder.setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            HashMap<String, Object> input = new HashMap();
                            input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(activity));
                            input.put(RequestParameters.GROUPID, group.groupId);
                            input.put(RequestParameters.SHAREDPOSTID, post.post_id);
                            input.put(RequestParameters.SHAREDPOSTGROUPID, post.group_id);
                            input.put(RequestParameters.SHAREDPOSTUSERID, post.userId);
                            API.sendRequestToServerPOST_PARAM(activity, API.SHARE_GROUP_POST, input);// service call for getting connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.post_has_been_shared), Toast.LENGTH_SHORT).show();
                            Utils.showPopup(activity, activity.getResources().getString(R.string.post_has_been_shared), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    activity.finish();
                                }
                            });
                        }
                    });
                    builder.setNegativeButton(activity.getString(R.string.cancel_btn_text), null);
                    builder.show();
                } else {
                    Intent intent = new Intent(activity, GroupPostActivity.class);
                    intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, base_url);
                    intent.putExtra(ResponseParameters.GROUP, group);
                    intent.putExtra(ResponseParameters.TYPE, type);
                    activity.startActivity(intent);
                }

            }
        });

        holder.llJoinContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (groups.get(position).status.trim().equalsIgnoreCase("1")) {
                    showPopUp(activity.getResources().getString(R.string.group_delete_request_popup_msg), position);
                } else if (groups.get(position).status.trim().equalsIgnoreCase("4")) {
                    showPopUp(activity.getResources().getString(R.string.group_accept_invite_popup_msg), position);
                } else if (groups.get(position).status.trim().equalsIgnoreCase("0")) {
                    HashMap<String, Object> input = new HashMap();
                    input.put(RequestParameters.GROUPID, "" + groups.get(position).groupId);
                    input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(activity));
                    input.put(RequestParameters.ROLE, "1");
                    API.sendRequestToServerPOST_PARAM(activity, API.GROUP_JOIN_REQUEST, input);// service call for getting connection
                    groups.get(position).status = "1";//change to requested
                    Utils.logEventGroupJoinRequest(activity, group.groupId);
                    notifyItemChanged(position);
                }
                //android.util.Log.e("TAG", groups.get(position).status);
            }
        });


    }

    private void showPopUp(String title, final int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);
        final TextView tvCancel = dialogView.findViewById(R.id.tvCancel);

        if (groups.get(position).status.trim().equalsIgnoreCase("4")) {
            tvYes.setText(activity.getResources().getString(R.string.request_accept_btn_text));
            tvCancel.setText(activity.getResources().getString(R.string.request_decline_btn_text));
        }

        tvTitle.setText(title);
        /*if(!showYes){
            view.setVisibility(View.GONE);
            llYes.setVisibility(View.GONE);
        }

        if(!showCancel){
            view.setVisibility(View.GONE);
            llCancel.setVisibility(View.GONE);
        }*/

        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    alert.dismiss();
                    return;
                }

                if (groups.get(position).status.trim().equalsIgnoreCase("1")) { // cancel request
                    groups.get(position).status = "0";//change to requested
                    notifyItemChanged(position);

                    String url = API.GROUP_DELETE_MEMBER + groups.get(position).groupId + "/" + SharedPreferencesMethod.getUserId(activity);
                    API.sendRequestToServerGET(activity, url, API.GROUP_DELETE_MEMBER);
                    Log.e("TAG", "" + url);

                } else if (groups.get(position).status.trim().equalsIgnoreCase("4")) { //accept invite

                    String url = API.GROUP_ACCEPT_REQUEST + groups.get(position).groupId + "/" + SharedPreferencesMethod.getUserId(activity);
                    API.sendRequestToServerGET(activity, url, API.GROUP_ACCEPT_REQUEST);
                    Utils.logEventGroupInviteAccept(activity, groups.get(position).groupId);
                    notifyItemRemoved(position);
                    groups.remove(position);
                    notifyItemRangeChanged(0, groups.size());
                    if (groups.size() <= 0)
                        ((GroupFragment.PlaceholderFragment) connectionFragment).showNoUser();

                }
                alert.dismiss();
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    alert.dismiss();
                    return;
                }
                if (groups.get(position).status.trim().equalsIgnoreCase("4")) { // decline invite

                    String url = API.GROUP_DELETE_MEMBER + groups.get(position).groupId + "/" + SharedPreferencesMethod.getUserId(activity);
                    API.sendRequestToServerGET(activity, url, API.GROUP_DELETE_MEMBER);
                    groups.get(position).status = "0";//change to requested
                    notifyItemChanged(position);

                    Log.e("TAG", "" + url);
                }
                alert.dismiss();
            }
        });

        alert = dialogBuilder.create();
        alert.setCancelable(true);
        alert.show();

    }


    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvGroupSummery, tvMemberCount, tvGroupPrivacy, tvCount, tvJoin, tvGroupType;
        ImageView ivGroupImage;
        RelativeLayout rlContainer;
        LinearLayout llJoinContainer;


        GroupViewHolder(View view) {
            super(view);
            tvUserName = view.findViewById(R.id.tvUserName);
            ivGroupImage = view.findViewById(R.id.ivGroupImage);
            tvGroupSummery = view.findViewById(R.id.tvGroupSummery);
            tvMemberCount = view.findViewById(R.id.tvMemberCount);
            rlContainer = view.findViewById(R.id.rlContainer);
            tvGroupPrivacy = view.findViewById(R.id.tvGroupPrivacy);
            tvGroupType = view.findViewById(R.id.tvGroupType);
            tvCount = view.findViewById(R.id.tvCount);
            tvJoin = view.findViewById(R.id.tvJoin);
            llJoinContainer = view.findViewById(R.id.llJoinContainer);
        }
    }
}