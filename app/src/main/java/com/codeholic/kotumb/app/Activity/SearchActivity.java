package com.codeholic.kotumb.app.Activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.codeholic.kotumb.app.Adapter.UserRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.EventNames;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.searchview)
    SearchView searchview;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;



    int pagination = 0;
    private LinearLayoutManager linearLayoutManager;

    Activity context;
    List<User> users;
    UserRecyclerViewAdapter userRecyclerViewAdapter;
    private boolean logged;

    //oncreate method for setting content view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        init();
        initToolbar();
        searchViewSetUp();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.UPDATE_USER);
        registerReceiver(receiver, filter);
    }

    //setting toolbar and header title
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //init values and show user details
    private void init() {
        context = this;
    }

    //search view setup
    private void searchViewSetUp() {
        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String newText) {
                performSearch(newText);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                performSearch(newText);
                return false;
            }
        });

        disableSearchViewActionMode(searchview);

        searchview.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSearch("s");
            }
        });

    }

    private void performSearch(String newText) {
        pagination = 0;

        List<User> users = new ArrayList<>();
        rvUserList.setAdapter(new UserRecyclerViewAdapter(rvUserList, SearchActivity.this, users));

        if (!newText.isEmpty()) {
            String url = API.SEARCH_USERS + "/" + newText + "/" + pagination + "/?userid=" + SharedPreferencesMethod.getUserId(SearchActivity.this);
            API.sendRequestToServerGET(context, url, API.SEARCH_USERS); // service call for user list
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvUserList.setLayoutManager(linearLayoutManager);
        /*rvGroupList.setHasFixedSize(true);
        rvGroupList.setNestedScrollingEnabled(false);*/
        List<User> users = new ArrayList<>();
        rvUserList.setAdapter(new UserRecyclerViewAdapter(rvUserList, this, users));
        if (!searchview.getQuery().toString().trim().isEmpty()) {
            pagination = 0;
            API.sendRequestToServerGET(context, API.SEARCH_USERS + "/" + searchview.getQuery().toString().trim() + "/" + pagination + "/?userid=" + SharedPreferencesMethod.getUserId(SearchActivity.this), API.SEARCH_USERS);//search list service call
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void disableSearchViewActionMode(SearchView searchView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ((EditText) searchView.findViewById(R.id.search_src_text)).setCustomSelectionActionModeCallback(new ActionMode.Callback() {
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    return false;
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {

                }
            });
        }
    }

    //getting response from server
    public void getResponse(JSONObject outPut, int i) {
        try {
            Log.e("response", outPut.toString());
            if (i == 0) { // get search user list
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(rvUserList, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else if (outPut.has(ResponseParameters.Success)) {

                        //Logging FireBase Events
                        if (!logged) {
                            Utils.logEvent(EventNames.HOME_PAGE_SEARCH, new Bundle(), this);
                            logged = true;
                        }

                        if (outPut.has(ResponseParameters.RESULTS)) {
                            users = new ArrayList<>();
                            for (int j = 0; j < outPut.getJSONArray(ResponseParameters.RESULTS).length(); j++) {
                                JSONObject searchResults = outPut.getJSONArray(ResponseParameters.RESULTS).getJSONObject(j);
                                User user = new User();
                                user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                                user.setLastName(searchResults.getString(ResponseParameters.LastName));
                                user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                                user.setUserId(searchResults.getString(ResponseParameters.UserId));
                                user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                                user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                                user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                                user.setCity(searchResults.getString(ResponseParameters.City));
                                user.setState(searchResults.getString(ResponseParameters.State));
                                user.setUserInfo(searchResults.toString());
                                users.add(user);
                            }
                            //setting recycler view
                            linearLayoutManager = new LinearLayoutManager(this);
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rvUserList.setLayoutManager(linearLayoutManager);
                            userRecyclerViewAdapter = new UserRecyclerViewAdapter(rvUserList, this, users);
                            rvUserList.setAdapter(userRecyclerViewAdapter);
                            //setting load more to recycler view
                            userRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                @Override
                                public void onLoadMore() {
                                    if (users.size() >= 10) {
                                        Log.e("inside", "inside");
                                        rvUserList.post(new Runnable() {
                                            public void run() {
                                                users.add(null); // add loading item
                                                userRecyclerViewAdapter.notifyItemInserted(users.size() - 1);
                                            }
                                        });
                                        API.sendRequestToServerGET(context, API.SEARCH_USERS + "/" + searchview.getQuery().toString().trim() + "/" + (pagination + 10) + "/?userid=" + SharedPreferencesMethod.getUserId(SearchActivity.this), API.SEARCH_USERS_UPDATE);// get next urlImages
                                    }
                                }
                            });
                        }
                    } else {
                        final Snackbar snackbar = Snackbar.make(rvUserList, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    }
                }
            }
            if (i == 1) { // load more response
                users.remove(users.size() - 1); // remove loading item
                userRecyclerViewAdapter.notifyItemRemoved(users.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                    } else if (outPut.has(ResponseParameters.Success)) {
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            for (int j = 0; j < outPut.getJSONArray(ResponseParameters.RESULTS).length(); j++) {
                                JSONObject searchResults = outPut.getJSONArray(ResponseParameters.RESULTS).getJSONObject(j);
                                User user = new User();
                                user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                                user.setLastName(searchResults.getString(ResponseParameters.LastName));
                                user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                                user.setUserId(searchResults.getString(ResponseParameters.UserId));
                                user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                                user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                                user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                                user.setCity(searchResults.getString(ResponseParameters.City));
                                user.setState(searchResults.getString(ResponseParameters.State));
                                user.setUserInfo(searchResults.toString());
                                users.add(user);
                            }
                            pagination = pagination + 10;
                            //add next urlImages
                            userRecyclerViewAdapter.notifyDataSetChanged();
                            userRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            Log.e("test", "test");
            User updatedUser = intent.getParcelableExtra("USER");
            for (int i = 0; i < users.size(); i++) {
                User user = users.get(i);
                if (updatedUser.getUserId().equals(user.getUserId())) {
                    try {
                        users.remove(i);
                        users.add(i, updatedUser);
                        userRecyclerViewAdapter.notifyItemChanged(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroy();
    }
}
