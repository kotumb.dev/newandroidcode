package com.codeholic.kotumb.app.Activity;

import android.content.Context;
import android.content.DialogInterface;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Adapter.LinksRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Link;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.fragments.BaseFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;

public class FavrtLinksActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;


    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favrt_links);
        ButterKnife.bind(this);
        initToolbar();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

       /* mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));*/
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    //init toolbar function
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.home_favourite_title));


        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class FavLinkFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        @BindView(R.id.rvUserList)
        RecyclerView rvUserList;
        @BindView(R.id.tvLinks)
        TextView tvLinks;

        @BindView(R.id.llLoading)
        LinearLayout llLoading;

        @BindView(R.id.llNotFound)
        LinearLayout llNotFound;

        @BindView(R.id.adView)
        ImageView adView;
        @BindView(R.id.rlAdView)
        RelativeLayout rlAdView;

        User user;
        int pagination = 0;
        List<Link> links;
        private LinearLayoutManager linearLayoutManager;
        LinksRecyclerViewAdapter linksRecyclerViewAdapter;

        public FavLinkFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static FavLinkFragment newInstance(int sectionNumber) {
            FavLinkFragment fragment = new FavLinkFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.content_links, container, false);
            ButterKnife.bind(this, rootView);
            init();
            setView();
            return rootView;
        }

        private void init() {
            user = SharedPreferencesMethod.getUserInfo(getActivity());
        }

        //setview for getting user links
        private void setView() {
            try {
                if (!Utility.isConnectingToInternet(getActivity())) { // checking net connection
                    llLoading.setVisibility(GONE);
                    llNotFound.setVisibility(View.VISIBLE);
                    tvLinks.setText(getResources().getString(R.string.app_no_internet_error));
                    final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }
                tvLinks.setText(getResources().getString(R.string.app_no_link_found));
                API.sendRequestToServerGET_FRAGMENT(getActivity(), FavLinkFragment.this, API.FAVOURITES_LINKS_ADMIN + (pagination) + "/?userid=" + user.getUserId(), API.FAVOURITES_LINKS_ADMIN);// service call for getting fav links
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //getResponse for getting list of links
        public void getResponse(JSONObject outPut, int i) {
            llLoading.setVisibility(GONE);
            Log.e("response", "" + outPut);
            try {
                if (i == 0) { // for getting links
                    if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                        if (outPut.has(ResponseParameters.Errors)) { // error response
                            llNotFound.setVisibility(View.VISIBLE);
                        } else if (outPut.has(ResponseParameters.Success)) { // success response
                            JSONArray userList = new JSONArray();
                            if (outPut.has(ResponseParameters.LINKS)) {
                                userList = outPut.getJSONArray(ResponseParameters.LINKS);
                            }
                            links = new ArrayList<>();
                            for (int j = 0; j < userList.length(); j++) {
                                JSONObject searchResults = userList.getJSONObject(j);
                                Link link = new Link();
                                link.setLinkId(searchResults.getString(ResponseParameters.Id));
                                link.setLink(searchResults.getString(ResponseParameters.URL));
                                link.setLinkName(searchResults.getString(ResponseParameters.TITLE));
                                links.add(link);
                            }
                            //setting options for recycler adapter
                            linearLayoutManager = new LinearLayoutManager(getActivity());
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rvUserList.setLayoutManager(linearLayoutManager);
                            linksRecyclerViewAdapter = new LinksRecyclerViewAdapter(rvUserList, getActivity(), links);
                            rvUserList.setAdapter(linksRecyclerViewAdapter);
                            //load more setup for recycler adapter
                            linksRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                @Override
                                public void onLoadMore() {
                                    if (links.size() >= 6) {
                                        rvUserList.post(new Runnable() {
                                            public void run() {
                                                links.add(null);
                                                linksRecyclerViewAdapter.notifyItemInserted(links.size() - 1);
                                            }
                                        });
                                        API.sendRequestToServerGET_FRAGMENT(getActivity(), FavLinkFragment.this, API.FAVOURITES_LINKS_ADMIN + (pagination + 6) + "/?userid=" + user.getUserId(), API.FAVOURITES_LINKS_UPDATE_ADMIN);// service call for getting fav links
                                    }
                                }
                            });
                        } else {
                            //no userfound
                            llNotFound.setVisibility(View.VISIBLE);
                        }
                    } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                        //error response
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                        //no net connection
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (i == 1) {// for updating list
                    links.remove(links.size() - 1); // removing of loading item
                    linksRecyclerViewAdapter.notifyItemRemoved(links.size());
                    if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                        if (outPut.has(ResponseParameters.Success)) { // success response

                            JSONArray userList = new JSONArray();
                            if (outPut.has(ResponseParameters.LINKS)) {
                                userList = outPut.getJSONArray(ResponseParameters.LINKS);
                            }

                            for (int j = 0; j < userList.length(); j++) {
                                JSONObject searchResults = userList.getJSONObject(j);
                                Link link = new Link();
                                link.setLinkId(searchResults.getString(ResponseParameters.Id));
                                link.setLink(searchResults.getString(ResponseParameters.URL));
                                link.setLinkName(searchResults.getString(ResponseParameters.TITLE));
                                links.add(link);
                            }
                            if (userList.length() > 0) {
                                pagination = pagination + 6;
                                //updating recycler view
                                linksRecyclerViewAdapter.notifyDataSetChanged();
                                linksRecyclerViewAdapter.setLoaded();
                            }
                        }
                    }
                }

            } catch (Exception e) {
                //mo user found
                llNotFound.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class ListLinkFragment extends BaseFragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        com.codeholic.kotumb.app.View.ProgressBar progressBar = null;
        @BindView(R.id.rvUserList)
        RecyclerView rvUserList;
        @BindView(R.id.tvLinks)
        TextView tvLinks;

        @BindView(R.id.llLoading)
        LinearLayout llLoading;

        @BindView(R.id.llNotFound)
        LinearLayout llNotFound;

        @BindView(R.id.adView)
        ImageView adView;
        @BindView(R.id.rlAdView)
        RelativeLayout rlAdView;

        User user;
        int pagination = 0;
        List<Link> links = new ArrayList<>();
        private LinearLayoutManager linearLayoutManager = null;
        LinksRecyclerViewAdapter linksRecyclerViewAdapter = null;


        public ListLinkFragment() {
        }


        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ListLinkFragment newInstance(int sectionNumber) {
            ListLinkFragment fragment = new ListLinkFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            // Do something that differs the Activity's menu here
            inflater.inflate(R.menu.menu_link, menu);
            super.onCreateOptionsMenu(menu, inflater);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {

                case R.id.action_add:
                    openAddLinkPopUp();
                    break;

                default:
                    break;
            }

            return false;
        }

        private void openAddLinkPopUp() {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            View dialogView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.add_fvrt_popup, null);
            dialogBuilder.setView(dialogView);
            final EditText etTitle = (EditText) dialogView.findViewById(R.id.etTitle);
            final TextInputLayout etTitleLayout = (TextInputLayout) dialogView.findViewById(R.id.etTitleLayout);
            final EditText etUrl = (EditText) dialogView.findViewById(R.id.etUrl);
            final TextInputLayout etUrlLayout = (TextInputLayout) dialogView.findViewById(R.id.etUrlLayout);
            dialogBuilder.setPositiveButton(getResources().getString(R.string.save_btn_text), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });


            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.setCancelable(true);
            alertDialog.show();
            alertDialog.getButton(-1).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (Boolean.valueOf(false).booleanValue()) {
                        alertDialog.dismiss();
                    }
                    etTitleLayout.setErrorEnabled(false);
                    etUrlLayout.setErrorEnabled(false);
                    if (etTitle.getText().toString().trim().isEmpty()) {
                        etTitleLayout.setErrorEnabled(true);
                        etTitleLayout.setError(getResources().getString(R.string.empty_title_error));
                    } else if (etUrl.getText().toString().trim().isEmpty()) {
                        etUrlLayout.setErrorEnabled(true);
                        etUrlLayout.setError(getResources().getString(R.string.empty_url_error));
                    } else if (!Patterns.WEB_URL.matcher(etUrl.getText().toString().trim()).matches()) {
                        etUrlLayout.setErrorEnabled(true);
                        etUrlLayout.setError(getResources().getString(R.string.valid_url_error));
                    } else if (Utility.isConnectingToInternet(getActivity())) {
                        HashMap<String, Object> input = new HashMap();
                        input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(getActivity()));
                        input.put(RequestParameters.TITLE, "" + etTitle.getText().toString().trim());
                        input.put(RequestParameters.URL, "" + etUrl.getText().toString().trim());
                        progressBar = new com.codeholic.kotumb.app.View.ProgressBar(getActivity());
                        progressBar.show(getActivity().getResources().getString(R.string.app_please_wait_text));

                        Log.e("API.SHARE_RESUME", "" + API.SHARE_RESUME);
                        API.sendRequestToServerPOST_PARAM_FRAGMENT(getActivity(), ListLinkFragment.this, API.ADD_FAV, input);// service call for share resume
                        alertDialog.dismiss();
                    } else {
                        Snackbar.make(etTitleLayout, getResources().getString(R.string.app_no_internet_error), 0).show();
                    }
                }
            });
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.content_links, container, false);
            ButterKnife.bind(this, rootView);
            init();
            setView();
            return rootView;
        }

        private void init() {
            user = SharedPreferencesMethod.getUserInfo(getActivity());
        }

        //setview for getting user links
        private void setView() {
            try {
                if (!Utility.isConnectingToInternet(getActivity())) { // checking net connection
                    llLoading.setVisibility(GONE);
                    llNotFound.setVisibility(View.VISIBLE);
                    tvLinks.setText(getResources().getString(R.string.app_no_internet_error));
                    final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }
                tvLinks.setText(getResources().getString(R.string.app_no_link_found));
                API.sendRequestToServerGET_FRAGMENT(getActivity(), ListLinkFragment.this, API.FAVOURITES_LINKS + (pagination) + "//?userid=" + user.getUserId(), API.FAVOURITES_LINKS);// service call for getting fav links
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void showNotFound(){
            llLoading.setVisibility(GONE);
            llNotFound.setVisibility(View.VISIBLE);
            rvUserList.setVisibility(GONE);
        }

        //getResponse for getting list of links
        public void getResponse(JSONObject outPut, int i) {
            llLoading.setVisibility(GONE);
            if (this.progressBar != null) {
                this.progressBar.dismiss();
            }
            Log.e("response", "" + outPut);
            try {
                if (i == 0) { // for getting links
                    if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                        if (outPut.has(ResponseParameters.Errors)) { // error response
                            llNotFound.setVisibility(View.VISIBLE);
                        } else if (outPut.has(ResponseParameters.Success)) { // success response
                            JSONArray userList = new JSONArray();
                            if (outPut.has(ResponseParameters.LINKS)) {
                                userList = outPut.getJSONArray(ResponseParameters.LINKS);
                            }
                            links = new ArrayList<>();
                            for (int j = 0; j < userList.length(); j++) {
                                JSONObject searchResults = userList.getJSONObject(j);
                                Link link = new Link();
                                link.setLinkId(searchResults.getString(ResponseParameters.Id));
                                link.setLink(searchResults.getString(ResponseParameters.URL));
                                link.setLinkName(searchResults.getString(ResponseParameters.TITLE));
                                links.add(link);
                            }
                            //setting options for recycler adapter
                            linearLayoutManager = new LinearLayoutManager(getActivity());
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rvUserList.setLayoutManager(linearLayoutManager);
                            linksRecyclerViewAdapter = new LinksRecyclerViewAdapter(rvUserList, getActivity(), links);
                            linksRecyclerViewAdapter.setUpDeleteLayout(true);
                            linksRecyclerViewAdapter.setFragment(this);
                            rvUserList.setAdapter(linksRecyclerViewAdapter);
                            //load more setup for recycler adapter
                            linksRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                @Override
                                public void onLoadMore() {
                                    if (links.size() >= 6) {
                                        rvUserList.post(new Runnable() {
                                            public void run() {
                                                links.add(null);
                                                linksRecyclerViewAdapter.notifyItemInserted(links.size() - 1);
                                            }
                                        });
                                        API.sendRequestToServerGET_FRAGMENT(getActivity(), ListLinkFragment.this, API.FAVOURITES_LINKS + (pagination + 6) + "//?userid=" + user.getUserId(), API.FAVOURITES_LINKS_UPDATE);// service call for getting fav links
                                    }
                                }
                            });
                        } else {
                            //no userfound
                            llNotFound.setVisibility(View.VISIBLE);
                        }
                    } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                        //error response
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                        //no net connection
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (i == 1) {// for updating list
                    links.remove(links.size() - 1); // removing of loading item
                    linksRecyclerViewAdapter.notifyItemRemoved(links.size());
                    if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                        if (outPut.has(ResponseParameters.Success)) { // success response

                            JSONArray userList = new JSONArray();
                            if (outPut.has(ResponseParameters.LINKS)) {
                                userList = outPut.getJSONArray(ResponseParameters.LINKS);
                            }

                            for (int j = 0; j < userList.length(); j++) {
                                JSONObject searchResults = userList.getJSONObject(j);
                                Link link = new Link();
                                link.setLinkId(searchResults.getString(ResponseParameters.Id));
                                link.setLink(searchResults.getString(ResponseParameters.URL));
                                link.setLinkName(searchResults.getString(ResponseParameters.TITLE));
                                links.add(link);
                            }
                            if (userList.length() > 0) {
                                pagination = pagination + 6;
                                //updating recycler view
                                linksRecyclerViewAdapter.notifyDataSetChanged();
                                linksRecyclerViewAdapter.setLoaded();
                            }
                        }
                    }
                } else if (i == 2) {// for updating list


                    if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                        if (outPut.has(ResponseParameters.Success)) { // success response
                            if (outPut.has(ResponseParameters.FAV_LINK)) {
                                JSONObject FAV = outPut.getJSONObject(ResponseParameters.FAV_LINK);
                                Link link = new Link();
                                link.setLinkId(FAV.getString(ResponseParameters.Id));
                                link.setLink(FAV.getString(ResponseParameters.URL));
                                link.setLinkName(FAV.getString(ResponseParameters.TITLE));
                                links.add(link);
                                if (linearLayoutManager == null) {
                                    llLoading.setVisibility(GONE);
                                    llNotFound.setVisibility(View.GONE);
                                    rvUserList.setVisibility(View.VISIBLE);
                                    linearLayoutManager = new LinearLayoutManager(getActivity());
                                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                    rvUserList.setLayoutManager(linearLayoutManager);
                                    linksRecyclerViewAdapter = new LinksRecyclerViewAdapter(rvUserList, getActivity(), links);
                                    linksRecyclerViewAdapter.setUpDeleteLayout(true);
                                    linksRecyclerViewAdapter.setFragment(this);
                                    rvUserList.setAdapter(linksRecyclerViewAdapter);
                                } else {
                                    linksRecyclerViewAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }

            } catch (Exception e) {
                //mo user found
                llNotFound.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a FavLinkFragment (defined as a static inner class below).
            if (position == 0)
                return FavLinkFragment.newInstance(position + 1);
            else
                return ListLinkFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.fav_link_title);
                case 1:
                    return getResources().getString(R.string.user_links_text);
            }
            return "";
        }
    }
}
