package com.codeholic.kotumb.app.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.codeholic.kotumb.app.Model.Note;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.shagi.materialdatepicker.date.DatePickerFragmentDialog;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddNoteActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.cvTime)
    CardView cvTime;

    @BindView(R.id.cvCalender)
    CardView cvCalender;

    @BindView(R.id.cvSave)
    CardView cvSave;

    @BindView(R.id.tvSave)
    TextView tvSave;

    @BindView(R.id.tvDate)
    TextView tvDate;

    @BindView(R.id.tvTime)
    TextView tvTime;

    @BindView(R.id.tvDateError)
    TextView tvDateError;

    @BindView(R.id.tvTimeError)
    TextView tvTimeError;

    @BindView(R.id.etNoteLayout)
    TextInputLayout etNoteLayout;

    @BindView(R.id.etNote)
    EditText etNote;

    String toDate = "";
    String toTime = "";

    Note note = new Note();

    AlertDialog titleAlert;
    com.codeholic.kotumb.app.View.ProgressBar progressBar = null;

    // oncreate method initializing view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        ButterKnife.bind(this);
        initToolbar();
        setClickListener();
        setTextWatcher(etNote, etNoteLayout);
        if (getIntent().hasExtra(RequestParameters.NOTE)) {
            fillValues();
        }
    }

    private void fillValues() {
        try {
            note = getIntent().getParcelableExtra(RequestParameters.NOTE);
            etNote.setText(note.getNoteDes().trim());
            etNote.setSelection(etNote.getText().toString().trim().length());

            Calendar calendar = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat df_ = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
            Date _24HourDt = _24HourSDF.parse(note.getNoteTime());
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            calendar.setTime(df.parse(note.getNoteDate()));
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(df.parse(df.format(Calendar.getInstance().getTime())));
            if (calendar.getTimeInMillis() >= calendar1.getTimeInMillis()) {
                //tvDate.setText(df_.format(df.parse(note.getNoteDate())));
                DateFormat df__ = new SimpleDateFormat("dd-MM-yyyy");
                tvDate.setText(df__.format(df.parse(note.getNoteDate())));
                toDate = note.getNoteDate();
                calendar.setTime(df_.parse(note.getNoteDate() + " " + note.getNoteTime()));
                calendar1.setTime(df_.parse(df_.format(Calendar.getInstance().getTime())));
                if (calendar.getTimeInMillis() > calendar1.getTimeInMillis()) {
                    tvTime.setText(_12HourSDF.format(_24HourDt));
                    toTime = note.getNoteTime();
                }

            }
            //
            tvSave.setText(getResources().getString(R.string.update_btn_text));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickListener() {
        cvCalender.setOnClickListener(this);
        cvTime.setOnClickListener(this);
        cvSave.setOnClickListener(this);
    }

    // initToolbar method initializing Toolbar
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        // setting title
        header.setText(getResources().getString(R.string.add_note_title));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setTextWatcher(final EditText editText, final TextInputLayout textInputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    textInputLayout.setErrorEnabled(false);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        Calendar calendar;
        switch (v.getId()) {
            case R.id.cvTime:

                if (!toDate.isEmpty()) {
                    try {
                        calendar = Calendar.getInstance();
                        int hour = calendar.get(Calendar.HOUR_OF_DAY);
                        int minute = calendar.get(Calendar.MINUTE);

                        if (toTime.toString().trim().isEmpty()) {
                            if (calendar.get(Calendar.MINUTE) >= 45) {
                                minute = (calendar.get(Calendar.MINUTE) + 15) % 60;
                                hour = calendar.get(Calendar.HOUR_OF_DAY) + 1;
                            } else {
                                minute = (calendar.get(Calendar.MINUTE) + 15);
                                hour = calendar.get(Calendar.HOUR_OF_DAY);
                            }
                        } else {
                            DateFormat df_ = new SimpleDateFormat("HH:mm:ss");
                            calendar.setTime(df_.parse(toTime));
                            hour = calendar.get(Calendar.HOUR_OF_DAY);
                            minute = calendar.get(Calendar.MINUTE);
                        }

                        TimePickerDialog tpd = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                                Log.e("Time", hourOfDay + " " + minute + " " + second);
                                String hour;
                                String min;
                                if (String.valueOf(hourOfDay).length() == 1) {
                                    hour = "0" + String.valueOf(hourOfDay).length();
                                } else {
                                    hour = String.valueOf(hourOfDay);
                                }
                                if (String.valueOf(minute).length() == 1) {
                                    min = "0" + String.valueOf(minute).length();
                                } else {
                                    min = String.valueOf(minute);
                                }

                                toTime = hour + ":" + min + ":00";
                                tvTimeError.setVisibility(View.GONE);
                                try {
                                    SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
                                    SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
                                    Date _24HourDt = _24HourSDF.parse(hour + ":" + min);
                                    tvTime.setText(_12HourSDF.format(_24HourDt));
                                } catch (Exception e) {

                                }
                            }
                        }, hour, minute, false);


                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        Date toDay = Calendar.getInstance().getTime();
                        String toDayS = df.format(toDay);
                        if (toDate.toString().trim().equalsIgnoreCase(toDayS.trim())) {
                            int hourMin = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                            int minuteMin = Calendar.getInstance().get(Calendar.MINUTE);
                            if (Calendar.getInstance().get(Calendar.MINUTE) >= 45) {
                                minuteMin = (Calendar.getInstance().get(Calendar.MINUTE) + 15) % 60;
                                hourMin = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + 1;
                            } else {
                                minuteMin = (Calendar.getInstance().get(Calendar.MINUTE) + 15);
                                hourMin = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                            }
                            tpd.setMinTime(hourMin, minuteMin, 0);
                        }
                        tpd.enableMinutes(true);
                        tpd.setVersion(TimePickerDialog.Version.VERSION_2);
                        tpd.show(getFragmentManager(), "TimePickDialog");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
//                    Toast.makeText(this, getResources().getString(R.string.select_date_first), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(this,getResources().getString(R.string.select_date_first));
                }
                break;
            case R.id.cvCalender:
               // showCal();
                showCal2();
                break;
            case R.id.cvSave:
                if (validation()) {
                    TitlePopup();
                }
                break;
        }
    }

    private void showCal2() {
        Calendar currentTime = Calendar.getInstance();
        Calendar calendar;
        try {
            calendar = Calendar.getInstance();
            Calendar currentDate = Calendar.getInstance();

            if (toDate.toString().trim().isEmpty()) {
                if (currentDate.get(Calendar.MINUTE) >= 45 && currentDate.get(Calendar.HOUR_OF_DAY) == 11) {
                    currentDate.add(Calendar.DATE, 1);
                }
            } else {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                currentDate.setTime(df.parse(toDate.toString().trim()));
            }

            if (calendar.get(Calendar.MINUTE) >= 45 && calendar.get(Calendar.HOUR_OF_DAY) == 11) {
                calendar.add(Calendar.DATE, 1);
            }

            new SpinnerDatePickerDialogBuilder()
                    .context(AddNoteActivity.this)
                    .callback(new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            monthOfYear += 1;
                            String month = "" + monthOfYear;
                            String date = "" + dayOfMonth;
                            if (date.length() == 1) {
                                date = "0" + date;
                            }
                            if (month.length() == 1) {
                                month = "0" + month;
                            }
                            tvDate.setText(date + "-" + month + "-" + year);
                            tvDateError.setVisibility(View.GONE);
                            toDate = year + "-" + month + "-" + date;

                            try {
                                if (!toTime.trim().isEmpty()) {
                                    Calendar calendar = Calendar.getInstance();
                                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                    DateFormat df_ = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
                                    Date _24HourDt = _24HourSDF.parse(toTime);
                                    calendar.setTime(df.parse(toDate));
                                    Calendar calendar1 = Calendar.getInstance();
                                    calendar1.setTime(df.parse(df.format(Calendar.getInstance().getTime())));
                                    calendar.setTime(df_.parse(toDate + " " + toTime));
                                    calendar1.setTime(df_.parse(df_.format(Calendar.getInstance().getTime())));
                                    if (!(calendar.getTimeInMillis() > calendar1.getTimeInMillis())) {
                                        toTime = "";
                                        tvTime.setText(getResources().getString(R.string.note_set_alarm_text));
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    })
                    .spinnerTheme(R.style.NumberPickerStyle)
                    .showTitle(true)
                    .showDaySpinner(true)
                    .defaultDate(currentTime.get(Calendar.YEAR), currentTime.get(Calendar.MONTH), currentTime.get(Calendar.DAY_OF_MONTH))
                   // .maxDate(currentTime.get(Calendar.YEAR)-18, currentTime.get(Calendar.MONTH), currentTime.get(Calendar.DAY_OF_MONTH))
                    .minDate(currentTime.get(Calendar.YEAR), currentTime.get(Calendar.MONTH), currentTime.get(Calendar.DAY_OF_MONTH))
                    .build()
                    .show();

        }catch (Exception e){

        }

    }

    private void showCal() {
        Calendar calendar;
        try {
            calendar = Calendar.getInstance();
            Calendar currentDate = Calendar.getInstance();

            if (toDate.toString().trim().isEmpty()) {
                if (currentDate.get(Calendar.MINUTE) >= 45 && currentDate.get(Calendar.HOUR_OF_DAY) == 11) {
                    currentDate.add(Calendar.DATE, 1);
                }
            } else {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                currentDate.setTime(df.parse(toDate.toString().trim()));
            }

            if (calendar.get(Calendar.MINUTE) >= 45 && calendar.get(Calendar.HOUR_OF_DAY) == 11) {
                calendar.add(Calendar.DATE, 1);
            }


            DatePickerFragmentDialog datePickerDialog = DatePickerFragmentDialog.newInstance(new DatePickerFragmentDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePickerFragmentDialog view, int year, int monthOfYear, int dayOfMonth) {
                    monthOfYear += 1;
                    String month = "" + monthOfYear;
                    String date = "" + dayOfMonth;
                    if (date.length() == 1) {
                        date = "0" + date;
                    }
                    if (month.length() == 1) {
                        month = "0" + month;
                    }
                    tvDate.setText(date + "-" + month + "-" + year);
                    tvDateError.setVisibility(View.GONE);
                    toDate = year + "-" + month + "-" + date;

                    try {
                        if (!toTime.trim().isEmpty()) {
                            Calendar calendar = Calendar.getInstance();
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                            DateFormat df_ = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
                            Date _24HourDt = _24HourSDF.parse(toTime);
                            calendar.setTime(df.parse(toDate));
                            Calendar calendar1 = Calendar.getInstance();
                            calendar1.setTime(df.parse(df.format(Calendar.getInstance().getTime())));
                            calendar.setTime(df_.parse(toDate + " " + toTime));
                            calendar1.setTime(df_.parse(df_.format(Calendar.getInstance().getTime())));
                            if (!(calendar.getTimeInMillis() > calendar1.getTimeInMillis())) {
                                toTime = "";
                                tvTime.setText(getResources().getString(R.string.note_set_alarm_text));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE));


            datePickerDialog.setMinDate(calendar.getTimeInMillis());

            datePickerDialog.setTitle("");
            datePickerDialog.show(getSupportFragmentManager(), "");
        } catch (Exception e) {

        }
                /*new DatePickerBuilder(this, new OnSelectDateListener() {
                    @Override
                    public void onSelect(List<Calendar> calendar) {

                    }
                }) .pickerType(CalendarView.ONE_DAY_PICKER)
                        .date(Calendar.getInstance()) // Initial date as Calendar object
                        *//*.minimumDate(Calendar.getInstance()) // Minimum available date
                        .maximumDate(Calendar.getInstance()) // Maximum available date*//*
                        .headerColor(R.color.colorPrimaryDark) // Color of the dialog header
                        .headerLabelColor(R.color.white) // Color of the header label
                        *//*.previousButtonSrc(R.drawable.drawable) // Custom drawable of the previous arrow
                        .forwardButtonSrc(R.drawable.drawable) // Custom drawable of the forward arrow*//*
                        .abbreviationsBarColor(R.color.colorPrimary) // Color of bar with day symbols
                        .abbreviationsLabelsColor(R.color.white) // Color of symbol labels
                        .pagesColor(R.color.white) // Color of the calendar background
                        .selectionColor(R.color.colorPrimary) // Color of the selection circle
                        .selectionLabelColor(R.color.white) // Color of the label in the circle
                        .daysLabelsColor(R.color.colorPrimary) // Color of days numbers
                        .anotherMonthsDaysLabelsColor(android.R.color.darker_gray) // Color of visible days numbers from previous and next month page
                        .disabledDaysLabelsColor(android.R.color.darker_gray) // Color of disabled days numbers
                        .todayLabelColor(R.color.colorPrimaryDark) // Color of the today number
                        .dialogButtonsColor(R.color.colorPrimary).show(); // Color of "Cancel" and "OK" buttons*/
    }

    private void TitlePopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.title_popup, null);
        dialogBuilder.setView(dialogView);
        final EditText etTitle = (EditText) dialogView.findViewById(R.id.etTitle);
        if (getIntent().hasExtra(RequestParameters.NOTE)) {
            etTitle.setText(note.getNoteTitle());
        }
        final TextInputLayout etTitleLayout = (TextInputLayout) dialogView.findViewById(R.id.etTitleLayout);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.save_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AddNoteActivity.this.titleAlert.dismiss();
            }
        });
        this.titleAlert = dialogBuilder.create();
        this.titleAlert.setCancelable(false);
        this.titleAlert.show();
        this.titleAlert.getButton(-1).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Boolean.valueOf(false).booleanValue()) {
                    AddNoteActivity.this.titleAlert.dismiss();
                }
                etTitleLayout.setErrorEnabled(false);
                if (etTitle.getText().toString().trim().isEmpty()) {
                    etTitleLayout.setErrorEnabled(true);
                    etTitleLayout.setError(AddNoteActivity.this.getResources().getString(R.string.note_title_empty_input));
                } else if (Utility.isConnectingToInternet(AddNoteActivity.this)) {
                    HashMap<String, Object> input = new HashMap();
                    input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(AddNoteActivity.this));
                    input.put(RequestParameters.TITLE, "" + etTitle.getText().toString().trim());
                    input.put(RequestParameters.DESCRIPTION, "" + etNote.getText().toString().trim());
                    input.put(RequestParameters.DATE, "" + toDate);
                    input.put(RequestParameters.TIME, "" + toTime);
                    Log.e("input", "" + input);
                    progressBar = new com.codeholic.kotumb.app.View.ProgressBar(AddNoteActivity.this);
                    progressBar.show(getResources().getString(R.string.app_please_wait_text));
                    if (getIntent().hasExtra(RequestParameters.NOTE)) {
                        input.put(RequestParameters.NOTEID, "" + note.getNoteID());
                        API.sendRequestToServerPOST_PARAM(AddNoteActivity.this, API.UPDATE_NOTE, input);// service call for share resume
                    } else {
                        API.sendRequestToServerPOST_PARAM(AddNoteActivity.this, API.CREATE_NOTE, input);// service call for share resume
                    }

                    AddNoteActivity.this.titleAlert.dismiss();
                } else {
                    Snackbar.make(etTitleLayout, AddNoteActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                }
            }
        });
    }

    private boolean validation() {
        boolean result = true;
        etNoteLayout.setErrorEnabled(false);
        tvDateError.setVisibility(View.GONE);
        tvTimeError.setVisibility(View.GONE);
        if (etNote.getText().toString().isEmpty()) {
            etNoteLayout.setErrorEnabled(true);
            etNoteLayout.setError(getResources().getString(R.string.note_description_empty_input));
            result = false;
        }
        if (toDate.trim().isEmpty()) {
            tvDateError.setVisibility(View.VISIBLE);
            result = false;
        }
        if (toTime.trim().isEmpty()) {
            tvTimeError.setVisibility(View.VISIBLE);
            result = false;
        }
        return result;
    }

    public void getResponse(JSONObject outPut, int i) {
        try {
            if (progressBar != null)
                progressBar.dismiss();
            Log.e("output", "" + outPut);
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Success)) {
                    Constants.note = new Note();
                    JSONObject noteList = outPut.getJSONObject(ResponseParameters.NOTE);
                    Constants.note.setNoteID(noteList.getString(ResponseParameters.Id));
                    Constants.note.setNoteTitle(noteList.getString(ResponseParameters.TITLE));
                    Constants.note.setNoteDes(noteList.getString(ResponseParameters.DESCRIPTION));
                    Constants.note.setNoteDate(noteList.getString(ResponseParameters.Date));
                    Constants.note.setNoteTime(noteList.getString(ResponseParameters.TIME));
                    final Snackbar snackbar = Snackbar.make(etNote, outPut.getString(ResponseParameters.Success), Toast.LENGTH_SHORT);
                    snackbar.show();
                    finish();
                } else if (outPut.has(ResponseParameters.Errors)) {
                    final Snackbar snackbar = Snackbar.make(etNote, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            } else {
                final Snackbar snackbar = Snackbar.make(etNote, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                snackbar.show();
            }
        } catch (Exception e) {

        }
    }
}
