package com.codeholic.kotumb.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DELL on 11/1/2017.
 */

public class User implements Parcelable {
    String Userdata = "userdata";
    String UserId = "userId";
    String ReferenceId = "referenceId";
    String UserName = "userName";
    String MobileNumber = "";
    String Otp = "otp";
    String IsConfirmed = "isConfirmed";
    String DisplayName = "displayName";
    String Set1Questions = "";
    String Set2Questions = "";
    String FirstName = "";
    String LastName = "";
    String DOB = "";
    String Gender = "";
    String Email = "";
    String Avatar = "";
    String Address = "";
    String City = "";
    String State = "";
    String ZipCode = "";
    String alternateMobile = "";
    String profileSummary = "";
    String profileHeadline = "";
    String aadhaarInfo = "";
    String userInfo = "0";
    String emailVisibility = "0";
    private String isChallenged = "0";
    String mobileVisibility = "0";
    String addressVisibility = "0";
    String dobVisibility = "0";
    String genderVisibility = "0";
    String middleName = "";
    private String isBlocked = "0";
    private String isDeleted = "0";

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    String role = "";

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getDobVisibility() {
        return dobVisibility;
    }

    public void setDobVisibility(String dobVisibility) {
        this.dobVisibility = dobVisibility;
    }

    public String getGenderVisibility() {
        return genderVisibility;
    }

    public void setGenderVisibility(String genderVisibility) {
        this.genderVisibility = genderVisibility;
    }

    public String getEmailVisibility() {
        return emailVisibility;
    }

    public void setEmailVisibility(String emailVisibility) {
        this.emailVisibility = emailVisibility;
    }

    public String getMobileVisibility() {
        return mobileVisibility;
    }

    public void setMobileVisibility(String mobileVisibility) {
        this.mobileVisibility = mobileVisibility;
    }

    public String getAddressVisibility() {
        return addressVisibility;
    }

    public void setAddressVisibility(String addressVisibility) {
        this.addressVisibility = addressVisibility;
    }

    public String getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(String userInfo) {
        this.userInfo = userInfo;
    }

    public String getAadhaarInfo() {
        return aadhaarInfo;
    }

    public void setAadhaarInfo(String aadhaarInfo) {
        this.aadhaarInfo = aadhaarInfo;
    }

    public String getProfileSummary() {
        return profileSummary;
    }

    public void setProfileSummary(String profileSummary) {
        this.profileSummary = profileSummary;
    }

    public String getProfileHeadline() {
        return profileHeadline;
    }

    public void setProfileHeadline(String profileHeadline) {
        this.profileHeadline = profileHeadline;
    }

    public String getAlternateMobile() {
        return alternateMobile;
    }

    public void setAlternateMobile(String alternateMobile) {
        this.alternateMobile = alternateMobile;
    }

    public String getFirstName() {
        String s = FirstName + " " + middleName;
        return s.trim();
    }

    public String getOnlyFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getAvatar() {
        return Avatar;
    }

    public void setAvatar(String avatar) {
        Avatar = avatar;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public static Creator<User> getCREATOR() {
        return CREATOR;
    }


    public String getSet1Questions() {
        return Set1Questions;
    }

    public void setSet1Questions(String set1Questions) {
        Set1Questions = set1Questions;
    }

    public String getSet2Questions() {
        return Set2Questions;
    }

    public void setSet2Questions(String set2Questions) {
        Set2Questions = set2Questions;
    }

    protected User(Parcel in) {
        Userdata = in.readString();
        UserId = in.readString();
        ReferenceId = in.readString();
        UserName = in.readString();
        MobileNumber = in.readString();
        Otp = in.readString();
        IsConfirmed = in.readString();
        DisplayName = in.readString();
        Set1Questions = in.readString();
        Set2Questions = in.readString();
        FirstName = in.readString();
        LastName = in.readString();
        DOB = in.readString();
        Gender = in.readString();
        Email = in.readString();
        Avatar = in.readString();
        Address = in.readString();
        City = in.readString();
        State = in.readString();
        ZipCode = in.readString();
        alternateMobile = in.readString();
        profileSummary = in.readString();
        profileHeadline = in.readString();
        aadhaarInfo = in.readString();
        userInfo = in.readString();
        emailVisibility = in.readString();
        mobileVisibility = in.readString();
        addressVisibility = in.readString();
        dobVisibility = in.readString();
        genderVisibility = in.readString();
        middleName = in.readString();
    }

    public User() {

    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getUserdata() {
        return Userdata;
    }

    public void setUserdata(String userdata) {
        Userdata = userdata;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getReferenceId() {
        return ReferenceId;
    }

    public void setReferenceId(String referenceId) {
        ReferenceId = referenceId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getOtp() {
        return Otp;
    }

    public void setOtp(String otp) {
        Otp = otp;
    }

    public String getIsConfirmed() {
        return IsConfirmed;
    }

    public void setIsConfirmed(String isConfirmed) {
        IsConfirmed = isConfirmed;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable
     * instance's marshaled representation. For example, if the object will
     * include a file descriptor in the output of {@link #writeToParcel(Parcel, int)},
     * the return value of this method must include the
     * {@link #CONTENTS_FILE_DESCRIPTOR} bit.
     *
     * @return a bitmask indicating the set of special object types marshaled
     * by this Parcelable object instance.
     * @see #CONTENTS_FILE_DESCRIPTOR
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Userdata);
        dest.writeString(UserId);
        dest.writeString(ReferenceId);
        dest.writeString(UserName);
        dest.writeString(MobileNumber);
        dest.writeString(Otp);
        dest.writeString(IsConfirmed);
        dest.writeString(DisplayName);
        dest.writeString(Set1Questions);
        dest.writeString(Set2Questions);
        dest.writeString(FirstName);
        dest.writeString(LastName);
        dest.writeString(DOB);
        dest.writeString(Gender);
        dest.writeString(Email);
        dest.writeString(Avatar);
        dest.writeString(Address);
        dest.writeString(City);
        dest.writeString(State);
        dest.writeString(ZipCode);
        dest.writeString(alternateMobile);
        dest.writeString(profileSummary);
        dest.writeString(profileHeadline);
        dest.writeString(aadhaarInfo);
        dest.writeString(userInfo);
        dest.writeString(emailVisibility);
        dest.writeString(mobileVisibility);
        dest.writeString(addressVisibility);
        dest.writeString(dobVisibility);
        dest.writeString(genderVisibility);
        dest.writeString(middleName);
    }

    public void setIsBlocked(String isBlocked) {
        this.isBlocked = isBlocked;
    }

    public String getIsBlocked() {
        return isBlocked;
    }

    public String getIsChallenged() {
        return isChallenged;
    }

    public void setIsChallenged(String isChallenged) {
        this.isChallenged = isChallenged;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }
}
