package com.codeholic.kotumb.app.Activity;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.util.Pair;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Adapter.ChatRecyclerViewAdapter;
import com.codeholic.kotumb.app.Firebase.MyFirebaseMessagingService;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Message;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.EmojiMapUtil;
import com.codeholic.kotumb.app.Utility.EventNames;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.BaseActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String REMOVE = "remove";
    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.ivUser)
    ImageView ivUser;

    @BindView(R.id.back)
    LinearLayout back;

    @BindView(R.id.llProfile)
    LinearLayout llProfile;

    @BindView(R.id.recyclerChat)
    RecyclerView recyclerChat;

    @BindView(R.id.etMessage)
    EmojiconEditText etMessage;

    @BindView(R.id.layout_group_chat_chatbox)
    View layout_group_chat_chatbox;

    @BindView(R.id.block_msg)
    TextView block_msg;

    @BindView(R.id.ivSendMessage)
    ImageButton ivSendMessage;

    @BindView(R.id.ivAttachment)
    ImageButton ivAttachment;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.tvCount)
    TextView tvCount;

    @BindView(R.id.container)
    RelativeLayout container;

    @BindView(R.id.ibSmiley)
    ImageView ibSmiley;

    @BindView(R.id.action_context_bar)
    RelativeLayout action_context_bar;

    @BindView(R.id.iv_close)
    ImageView iv_close;

    @BindView(R.id.iv_delete)
    ImageView iv_delete;

    @BindView(R.id.iv_reply)
    ImageView iv_reply;

    @BindView(R.id.main_reply_container)
    RelativeLayout mainContainer;

    @BindView(R.id.cancel_chat)
    ImageView cancel_chat;


    @BindView(R.id.reply_txt)
    TextView reply_txt;


    @BindView(R.id.iv_copy)
    ImageView iv_copy;

    @BindView(R.id.tvSelectionCount)
    TextView tvSelectionCount;



    int unreadCount = 0;
    String messageId="";
    String RefrenceMessage="";
    private User user;
    List<Message> messages = new ArrayList<>();
    ChatRecyclerViewAdapter messageRecyclerViewAdapter;
    int pagination = 0;
    LinearLayoutManager linearLayoutManager;
    EmojIconActions emojIcon;
    boolean lastItem = true;
    com.codeholic.kotumb.app.View.ProgressBar progressBar = null;
    MenuItem action_delete = null;
    private String key = "";
    private boolean fabClicked;
    private boolean contextMenuVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        init();
        initToolbar();
        getMessage();
        setClickListener();
        try {
            setUpRecyclerView();
        } catch (Exception e) {
            e.printStackTrace();
        }


        key = SharedPreferencesMethod.getUserId(this) + "/" + user.getUserId();

        String retrievedMsg = SharedPreferencesMethod.getString(this, key);
        if (retrievedMsg != null) {
            etMessage.setText(retrievedMsg);
            etMessage.setSelection(retrievedMsg.length());
        }

        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String msg = charSequence.toString().trim();
                SharedPreferencesMethod.setString(ChatActivity.this, key, msg);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        callMarkAsReadService(user);
//        sendPdfAsMessage();

        if (getIntent().hasExtra("videoData")){
            String description = getIntent().getStringExtra("videoData");
            // String sourceString = "<b>" +"Click Here For More Details"+ "</b> ";


             etMessage.setText(description);
           // sendMessage(getMessageObject(description));
            System.out.println("Video Data   "+description+"   "+user.getFirstName());
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.UPDATE_CHAT);
        registerReceiver(receiveMessage, filter);
    }

    public void hideShowFab(boolean hide) {
        if (hide) {
            unreadCount = 0;
            tvCount.setText("");
            tvCount.setVisibility(GONE);
            fab.hide();
        } else {
            fab.show();
        }
    }

    public void onCreateContextMenu(final ContextMenu menu,
                                    final View v, final ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_delete, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                // remove stuff here
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }


/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
* This Code Section Return By Arpit Kanda
* For Attachment PDF in Messages*/


    public void messageReply(){
        mainContainer.setVisibility(VISIBLE);
        ArrayList<Pair<Integer, Message>> selectedItems = messageRecyclerViewAdapter.getSelectedItems();
        StringBuilder text = new StringBuilder();
        for(Pair<Integer, Message> pair: selectedItems){
            String reply = pair.second.getReply();
            RefrenceMessage=pair.second.getReply();
            messageId=pair.second.getId();
            String userName;
            if (!pair.second.getUserId().equalsIgnoreCase(SharedPreferencesMethod.getUserId(this))) {
                userName = user.getFirstName() +" " + user.getLastName();
            } else {
                User userInfo = SharedPreferencesMethod.getUserInfo(this);
                userName = userInfo.getFirstName() +" "+ userInfo.getLastName();
            }
            CharSequence refsequence = Html.fromHtml(reply);
            SpannableStringBuilder refstrBuilder = new SpannableStringBuilder(refsequence);
            URLSpan[] refurls = refstrBuilder.getSpans(0, refsequence.length(), URLSpan.class);
            for (URLSpan refspan : refurls) {
                makeLinkClickable(refstrBuilder, refspan);
            }
            reply_txt.setText(refstrBuilder);
            contextMenuVisible = false;
            action_context_bar.setVisibility(GONE);
            messageRecyclerViewAdapter.clearSelection();
        }

    }



    protected void makeLinkClickable(final SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                Utility.prepareCustomTab(ChatActivity.this, span.getURL());
                // Do something with span.getURL() to handle the link click...
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }


    public void messageReplyValidation(int size){
        if (size>=2){
            iv_reply.setVisibility(GONE);
        }else{
            iv_reply.setVisibility(VISIBLE);
        }
    }


 public void getReplys(final Message message,final JSONObject results) throws JSONException {
        try{
            if (results.getJSONObject("reference")==null){
                System.out.println("Value is Null "+results.getJSONObject("reference"));
                message.setRefrenceMessage(results.getJSONObject("reference").getString(""));
            }else{
                message.setRefrenceMessage(results.getJSONObject("reference").getString("reply"));
            }
        }catch (Exception ex){
            ex.printStackTrace();
            System.out.println("Error JSON   "+ex);
        }

    }


























    //This Method is Created By Arpit Kanda
    //OnAcitvityResult For Get and Set PDF into Message Sections
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constant.REQUEST_CODE_PICK_FILE) {
            if (resultCode == RESULT_OK) {
                ArrayList<NormalFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                StringBuilder builder = new StringBuilder();
                for (NormalFile file : list) {
                    String path = file.getPath();
                    builder.append(path + "\n");
                    final    String pdf = builder.toString();
                    System.out.println("File Name  "+new File(pdf).getName());
                    File fileObj = new File(path);
                    String fileSizeReadable = FileUtils.byteCountToDisplaySize(fileObj.length());
                    File f = new File(path);
                    long length = f.length();
                    System.out.println("File Length    "+length);
                    length = length/1024;
                    if (length<10240){
                        final ProgressDialog progressDialog;
                        progressDialog = new ProgressDialog(this);
                        progressDialog.setMessage("PDF Uploading");
                        progressDialog.show();
//                        getMessageMedia(new File(path),"pdf",progressDialog);
                        sendMessage(getMessageMedia(new File(path),"pdf",progressDialog));
                    }else{
                        Utils.showPopup(this,getResources().getString(R.string.pdf_size_error_text));
                    }
                }
            }
        }
    }





    public void sendPdfAsMessage(){
        Intent intent4 = new Intent(ChatActivity.this, com.vincent.filepicker.activity.NormalFilePickActivity.class);
        intent4.putExtra(Constant.MAX_NUMBER, 9);
        intent4.putExtra(BaseActivity.IS_NEED_FOLDER_LIST, true);
        intent4.putExtra(NormalFilePickActivity.SUFFIX, new String[] {"pdf"});
        startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);
    }










    //Message PDF Attechment Service Call(HttpClient)
    public Message getMessageMedia(File file,final String type,final ProgressDialog progressDialog) {
        final Message message = new Message();
        try {
            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(this))
                    .addFormDataPart(RequestParameters.TO_USERID, "" + user.getUserId())
                    .addFormDataPart(RequestParameters.MESSAGE_MEDIA,file.getName(),RequestBody.create(MediaType.parse("application/pdf"),file))
                    .addFormDataPart(RequestParameters.MESSAGE_MEDIA_TYPE, "" + type)
                    .addFormDataPart(RequestParameters.MESSAGE,"" +"PDF")
                    .addFormDataPart("msg_identifer", System.currentTimeMillis() + "")
                    .build();
            Request request = new Request.Builder()
                    .url(API.SEND_MESSAGE)
                    .post(body)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(final Call call, final IOException e) {
                    System.out.println("PDF Faliure   "+e.toString());
                }
                @Override
                public void onResponse(final Call call, final Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        try{
                            String resStr = response.body().string().toString();
                            JSONObject json = new JSONObject(resStr);
                            System.out.println("PDF Response Error   "+json.toString());
                        }catch (Exception ex){
                            System.out.println("PDF Response Error   "+ex.toString());
                        }
                    }else{
                            String resStr = response.body().string().toString();
                            System.out.println("PDF Response   "+resStr);
                            try{
                                progressDialog.dismiss();
                                JSONObject json = new JSONObject(resStr);
                                JSONObject messageObject = json.getJSONObject("message_info");
                                System.out.println("Message Result  "+messageObject.toString());
                                message.setId(messageObject.getString("id"));
                                message.setMessageMedia(messageObject.getString("media"));
                                message.setReply("PDF");
                                message.setReadStatus("0");
                                message.setMessageAT(messageObject.getString("messagedAT"));
                                message.setMessageMediaType("pdf");
                                message.setUserId(SharedPreferencesMethod.getUserId(getApplicationContext()));
                            }catch (Exception ex){
                              System.out.println("PDF Not Send  "+ex.toString());
                            }
                    }

                }
            });

        }catch (Exception ex) {
            System.out.println("Media Message Error  "+ex);
        }
        String value = System.currentTimeMillis() + "";
        message.setId(REMOVE);
        message.setMsg_identifer(value);
        message.setMessageMediaType(type);
        message.setReadStatus("0");
        message.setMessageAT("Sending...");
        message.setUserId(SharedPreferencesMethod.getUserId(getApplicationContext()));
        return message;
    }



    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/




    private BroadcastReceiver receiveMessage = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.hasExtra(ResponseParameters.MESSAGE_READ)) {
                    for (Message message : messages) {
                        message.setReadStatus("1");
//                        getMessage();
                    }
                    messageRecyclerViewAdapter.notifyDataSetChanged();
                    return;
                }
                User user = intent.getParcelableExtra("USER");
                if (ChatActivity.this.user.getUserId().equalsIgnoreCase(user.getUserId()) && !intent.hasExtra(ResponseParameters.UPDATE_CHAT)) {
                    JSONObject jsonObject = new JSONObject(user.getUserInfo());
                    if (jsonObject.has(ResponseParameters.LAST_MSG_FLAG)) {
                        if (jsonObject.getBoolean(ResponseParameters.LAST_MSG_FLAG)) {
                            JSONObject messageObject = jsonObject.getJSONObject(ResponseParameters.LAST_MSG_INFO);
                            System.out.println("Get Last Info   "+messageObject.toString());
                            Message message = new Message();
                            message.setId(messageObject.getString("id"));
                            message.setReply(messageObject.getString("reply"));
                            getReplys(message,messageObject);
                            message.setReadStatus(messageObject.getString("read_status"));
                            message.setMessageAT(messageObject.getString("messagedAT"));
                            message.setUserId(messageObject.getString("user_id"));
                            message.setMessageMediaType(messageObject.getString("mediaType"));
                            message.setMessageMedia(messageObject.getString("media"));
                            message.setMsg_identifer(messageObject.getString("msg_identifer"));
                            if (!message.getUserId().equalsIgnoreCase(SharedPreferencesMethod.getUserId(context))) {
                                Pair<Integer, Message> msg = findMessage(message);
                                if (msg == null) {
                                    messages.add(0, message);
                                    messageRecyclerViewAdapter.notifyItemInserted(0);
//                                    messageRecyclerViewAdapter.notifyDataSetChanged();
                                } else {
                                    //Toast.makeText(context, msg.getReply() + " already added", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Pair<Integer, Message> pair = findMessage(message);
                                if (pair == null) {
                                    //Toast.makeText(context, "not found", Toast.LENGTH_SHORT).show();
                                } else {
                                    // Toast.makeText(context, "found at"+pos, Toast.LENGTH_SHORT).show();
                                    try {
                                        int pos = pair.first;
                                        Message msg = pair.second;
                                        if (!msg.getReadStatus().equalsIgnoreCase("-1")) {
                                            message.setReadStatus(msg.getReadStatus()); //experimental
//                                            getMessage();
                                        }
                                        messages.set(pos, message);
                                        System.out.println("Sent Message  "+message);
                                        messageRecyclerViewAdapter.notifyDataSetChanged();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        messages.set(0, message);
                                        System.out.println("Error Message  "+message);
                                        messageRecyclerViewAdapter.notifyDataSetChanged();
                                    }
                                }
                            }
//                            messages.remove(messageToRemove);
                            showDeleteBtn();
                           /* messages.clear();
                            messages.addAll(removeDuplicates(messages));*/
                            // messageRecyclerViewAdapter.notifyItemInserted(0);
                            if (intent.hasExtra(ResponseParameters.MESSAGE_SENT)) { // scroll if you have sent message
                                recyclerChat.scrollToPosition(0);
                            } else {
                                tvCount.setVisibility(View.VISIBLE);
                                unreadCount = unreadCount + 1;
                                tvCount.setText("" + unreadCount);
                                hideShowFab(false);
                                callMarkAsReadService(user);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void callMarkAsReadService(User user) {
        try {
            String cid = new JSONObject(user.getUserInfo()).getString("cid");
            String url = API.MARK_AS_READ + user.getUserId() + "/" + cid;
            Context context1 = getApplicationContext();
            API.sendRequestToServerGET(context1, url, API.MARK_AS_READ);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Pair<Integer, Message> findMessage(Message message) {
        for (int i = 0; i < messages.size(); i++) {
            Message msg = messages.get(i);
            if (msg.getMsg_identifer().equalsIgnoreCase(message.getMsg_identifer())) {
                return new Pair<Integer, Message>(i, msg);
            }
        }
        return null;
    }

    private ArrayList<Message> removeDuplicates(List<Message> messages) {
        ArrayList<Message> messageArrayList = new ArrayList<>();
        for (Message message : messages) {
            if (!contains(messageArrayList, message)) {
                messageArrayList.add(message);
            }
        }
        return messageArrayList;
    }

    private boolean contains(ArrayList<Message> messageArrayList, Message message) {
        for (Message msg : messageArrayList) {
            if (msg.getId().equalsIgnoreCase(message.getId())) {
                return true;
            }
        }
        return false;
    }


    private void setClickListener() {
        ivSendMessage.setOnClickListener(this);
        ivAttachment.setOnClickListener(this);
        fab.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        iv_close.setOnClickListener(this);
        iv_delete.setOnClickListener(this);
        iv_copy.setOnClickListener(this);
        iv_reply.setOnClickListener(this);
        header.setOnClickListener(this);
        cancel_chat.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_delete, menu);
        action_delete = menu.findItem(R.id.action_delete);
        showDeleteBtn();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {
            deletePopUp();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void deletePopUp() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(getResources().getString(R.string.app_alert_text));
        dialogBuilder.setMessage(getResources().getString(R.string.confirm_delete_message));
        dialogBuilder.setCancelable(true);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.done_btn_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                try {
                    if (!Utility.isConnectingToInternet(ChatActivity.this)) {
                        final Snackbar snackbar = Snackbar.make(recyclerChat, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }
                    progressBar = new com.codeholic.kotumb.app.View.ProgressBar(ChatActivity.this);
                    progressBar.show(getResources().getString(R.string.app_please_wait_text));


                    Log.e("CID", "");
                    API.sendRequestToServerGET(ChatActivity.this, API.DELETE_CONVERSATION + "/" + new JSONObject(user.getUserInfo()).getString("cid") + "/" + SharedPreferencesMethod.getUserId(ChatActivity.this), API.DELETE_CONVERSATION);// service call for getting messages
                    //sendRequest(activity, API.DELETE_NOTE + SharedPreferencesMethod.getUserId(activity) + "/" + note.getNoteID());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });

        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        dialogBuilder.create().show();
    }

    private void setUpRecyclerView() {
        try {
            linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            linearLayoutManager.setReverseLayout(true);
            linearLayoutManager.setStackFromEnd(true);
            recyclerChat.setLayoutManager(linearLayoutManager);
            messageRecyclerViewAdapter = new ChatRecyclerViewAdapter(recyclerChat, this, messages, user);
            recyclerChat.setAdapter(messageRecyclerViewAdapter);
            recyclerChat.smoothScrollToPosition(0);

            messageRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    Log.e("load more", "load moe");
                    recyclerChat.post(new Runnable() {
                        public void run() {
                            messages.add(null);
                            messageRecyclerViewAdapter.notifyItemInserted(messages.size() - 1);
                        }
                    });
                    API.sendRequestToServerGET(ChatActivity.this, API.CHAT_LIST + SharedPreferencesMethod.getUserId(ChatActivity.this) + "/" + user.getUserId() + "/" + "/?offset=" + (pagination + 6), API.CHAT_LIST_UPDATE);// service call for getting messages
                }
            });

            recyclerChat.addOnLayoutChangeListener(new OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v,
                                           int left, int top, int right, int bottom,
                                           int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    if (bottom < oldBottom) {
                        recyclerChat.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (lastItem)
                                    recyclerChat.scrollToPosition(0);
                                lastItem = false;
                            }
                        }, 100);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void init() {


        user = getIntent().getParcelableExtra("USER");
//        user = getIntent().getParcelableExtra("USER");

        if (user!=null) {
            try {
                JSONObject info = new JSONObject(user.getUserInfo());
                if (info.has("userInfo")) {
                    JSONObject userInfo = info.getJSONObject("userInfo");
                    String test = (String) userInfo.get("isDeleted");
                    String isBlocked = userInfo.getString("isBlocked");
                    user.setIsBlocked(isBlocked);
                    if (test.equalsIgnoreCase("1")) {
                        block_msg.setText(getString(R.string.user_deleted_chat_msg));
                    }
                    if (userInfo.has("isDeleted")) {
                        String isDeleted = userInfo.getString("isDeleted");
                        user.setIsDeleted(isDeleted);

                    } else {
                        user.setIsDeleted("0");
                    }
                    user.setRole(userInfo.getString(ResponseParameters.Role));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (user.getIsBlocked().equalsIgnoreCase("1")
                    || user.getIsDeleted().equalsIgnoreCase("1")) {
                layout_group_chat_chatbox.setVisibility(GONE);
            }

            if (user.getRole().equalsIgnoreCase("RU1")) {
                layout_group_chat_chatbox.setVisibility(GONE);
            }

            if (user.getIsBlocked().equalsIgnoreCase("1")
                    || user.getIsDeleted().equalsIgnoreCase("1")) {
                layout_group_chat_chatbox.setVisibility(GONE);
                block_msg.setVisibility(VISIBLE);

                if (user.getIsDeleted().equalsIgnoreCase("1")) {
                    block_msg.setText(getString(R.string.user_deleted_chat_msg));
                    header.setText("Kotumb User");
                    new AQuery(this).id(ivUser).image(API.imageUrl + "user_removed.png", true, true, 300, R.drawable.ic_user);
                    header.setClickable(false);
                }
            }

            Constants.openChatUser = user;
            emojIcon = new EmojIconActions(this, container, etMessage, ibSmiley);
            emojIcon.setIconsIds(R.drawable.ic_keyboard, R.drawable.ic_smiley);
            // emojIcon.setUseSystemEmoji(true);
            emojIcon.ShowEmojIcon();
            SharedPreferencesMethod.setString(this, ResponseParameters.NOTIFICATIONS, "");
            // for updating read count
            if (getIntent().hasExtra(ResponseParameters.UPDATE_CHAT)) {
                try {
                    Intent broadCast = new Intent();
                    broadCast.setAction(ResponseParameters.UPDATE_CHAT);
                    JSONObject jsonObject = new JSONObject(user.getUserInfo());
                    jsonObject.put(ResponseParameters.UNREAD_MESSAGE_COUNT, 0);
                    user.setUserInfo(jsonObject.toString());
                    Log.e("test", "" + user.getUserInfo());
                    broadCast.putExtra("USER", user);
                    broadCast.putExtra(ResponseParameters.UPDATE_CHAT, "");
                    sendBroadcast(broadCast);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else {
            startActivity(new Intent(ChatActivity.this,MessageActivity.class));
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Constants.openChatUser = new User();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.openChatUser = user;

        //clear notification of message
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(MyFirebaseMessagingService.ID_SMALL_NOTIFICATION);
        //clear notification of message
    }

    // initToolbar method initializing view
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("");
        //setting screen title
        if (user.getIsDeleted().equalsIgnoreCase("1")) {
            header.setText("Kotumb User");
            new AQuery(this).id(ivUser).image(API.imageUrl +"user_removed.png", true, true, 300, R.drawable.ic_user);
        }else{
            header.setText(user.getFirstName().trim() + " " + user.getLastName().trim());
            new AQuery(this).id(ivUser).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.ic_user);
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (!contextMenuVisible) {
            super.onBackPressed();
            getMessage();
        } else {
            hideContextMenu();
        }
    }

    public void hideContextMenu() {
        contextMenuVisible = false;
        action_context_bar.setVisibility(GONE);
        messageRecyclerViewAdapter.clearSelection();
    }

    private void getMessage() {
        try {
            if (!Utility.isConnectingToInternet(this)) { // checking net connection
                final Snackbar snackbar = Snackbar.make(recyclerChat, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            String url = API.CHAT_LIST + SharedPreferencesMethod.getUserId(this) + "/" + user.getUserId() + "/" + "/?offset=0";
            API.sendRequestToServerGET(this, url, API.CHAT_LIST);// service call for getting messages


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(Message message) {
        final Message messageObj = message;
        messageObj.setMessageAT("Sending...");
        String format = getSentString();
        messageObj.setReadStatus("-1");
        messageObj.setMessageAT("Today at " + format);
        messages.add(0, messageObj);
        messageRecyclerViewAdapter.notifyDataSetChanged();
        try {
            if (!Utility.isConnectingToInternet(this)) { // check net connection
                final Snackbar snackbar = Snackbar.make(ivSendMessage, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    String format = getSentString();
                    messageObj.setMessageAT("Today at " + format);
                    messageObj.setSent(true);
                    messages.set(0, messageObj);
                    messageRecyclerViewAdapter.notifyDataSetChanged();
                    recyclerChat.smoothScrollToPosition(0);


                    FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( ChatActivity.this,  new OnSuccessListener<InstanceIdResult>() {
                        @Override
                        public void onSuccess(InstanceIdResult instanceIdResult) {
                            String newToken = instanceIdResult.getToken();
                            if (!newToken.isEmpty()){
//                                sendRegistrationToServer(newToken);
                            }

                        }
                    });

                }
            }, 400);
        } catch (Exception e) {
            final Snackbar snackbar = Snackbar.make(ivSendMessage, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
            snackbar.show();
            e.printStackTrace();
        }

    }


    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
        try {
            HashMap<String, Object> input = new HashMap<>();
            input.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(getApplicationContext()));
            input.put(RequestParameters.TOKEN, token);
            API.sendRequestToServerPOST_PARAM(getApplicationContext(), API.ADD_TOKEN, input);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @NonNull
    private String getSentString() {
        Date time = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mma");
        String format = simpleDateFormat.format(time);
        if (format.startsWith("0")) {
            format = format.substring(1, format.length());
        }
        return format;
    }

    @NonNull
    private Message getMessageObject(String textMessage) {
        HashMap<String, Object> input = new HashMap<>();
        input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(this));
        input.put(RequestParameters.TO_USERID, "" + user.getUserId());
//        String message = EmojiMapUtil.replaceUnicodeEmojis(etMessage.getText().toString());
        input.put(RequestParameters.MESSAGE, "" + textMessage);
        input.put("referenceId", "" + messageId);
        String value = System.currentTimeMillis() + "";
        input.put("msg_identifer", value);
        Log.e("input", "" + input);
        etMessage.setText("");
        API.sendRequestToServerPOST_PARAM(this, API.SEND_MESSAGE, input); // service call for account verification
        Date now = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        Message messageObj = new Message();
        messageObj.setId(REMOVE);
        messageObj.setMsg_identifer(value);
        messageObj.setReply(textMessage);
        messageObj.setReadStatus("0");
        messageObj.setMessageAT("Sending...");
        messageObj.setRefrenceMessage(RefrenceMessage);
        messageObj.setRefrenceId(messageId);
        messageObj.setUserId(SharedPreferencesMethod.getUserId(getApplicationContext()));
        mainContainer.setVisibility(GONE);
        return messageObj;
    }




    //getResponse for getting list of urlImages
    public void getResponse(JSONObject outPut, int i) {
        Log.e("response", "" + outPut);
        System.out.println("Sent Response  "+outPut);
        try {
            if (progressBar != null) {
                progressBar.dismiss(); // dismissing progressbar
            }

            if (i == 0) { // for getting urlImages message
                System.out.println("Message Send Response  "+outPut);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        JSONArray chatList = new JSONArray();
                        if (outPut.has(ResponseParameters.MESSAGES)) {
                            chatList = outPut.getJSONArray(ResponseParameters.MESSAGES);
                        }
                        int length = chatList.length();
                        fabClicked = false;
                        int i1 = fabClicked ? 1 : length;
                        for (int j = 0; j < i1; j++) {
                            JSONObject messageObject = chatList.getJSONObject(j);
                            Message message = new Message();
                            message.setId(messageObject.getString("id"));
                            message.setReply(messageObject.getString("reply"));
                            getReplys(message,messageObject);
                            message.setReadStatus(messageObject.getString("read_status"));
                            message.setMessageAT(messageObject.getString("messagedAT"));
                            message.setMessageMediaType(messageObject.getString("mediaType"));
                            message.setMessageMedia(messageObject.getString("media"));
                            message.setUserId(messageObject.getString("user_id"));
                            message.setSent(true);
                            if (fabClicked) {
                                messages.add(0, message);
                            } else {
                                messages.add(message);
                            }
                        }
                        fabClicked = false;
                        showDeleteBtn();
                        messageRecyclerViewAdapter.notifyDataSetChanged();
                        recyclerChat.scrollToPosition(0);

                    } else {
                        //no userfound
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                }
            } else if (i == 1) {// for updating list
                System.out.println("Message Get Response  "+outPut);
                messages.remove(messages.size() - 1); // removing of loading item
                messageRecyclerViewAdapter.notifyItemRemoved(messages.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response

                        JSONArray chatList = new JSONArray();
                        if (outPut.has(ResponseParameters.MESSAGES)) {
                            chatList = outPut.getJSONArray(ResponseParameters.MESSAGES);
                        }
                        int length = chatList.length();
                        for (int j = 0; j < length; j++) {
                            JSONObject messageObject = chatList.getJSONObject(j);
                            Message message = new Message();
                            message.setId(messageObject.getString("id"));
                            message.setReply(messageObject.getString("reply"));
                            getReplys(message,messageObject);
                            message.setReadStatus(messageObject.getString("read_status"));
                            message.setMessageAT(messageObject.getString("messagedAT"));
                            message.setMessageMediaType(messageObject.getString("mediaType"));
                            message.setMessageMedia(messageObject.getString("media"));
                            message.setUserId(messageObject.getString("user_id"));
                            message.setSent(true);
//                            messages.remove(0);
                            messages.add(message);
                        }
                        if (chatList.length() > 0) {
                            pagination = pagination + 6;
                            //updating recycler view
                            messageRecyclerViewAdapter.notifyDataSetChanged();
                            messageRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            }

            if (i == 2) { // for send message
                Log.e("output", "" + outPut);
                System.out.println("Message Response3  "+outPut);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                    } else if (outPut.has(ResponseParameters.Success)) { // success response

                        Bundle bundle = new Bundle();
                        bundle.putString("UserTo", user.getUserId());
                        bundle.putString("UserFrom", SharedPreferencesMethod.getUserId(ChatActivity.this));
                        Utils.logEvent(EventNames.USER_SEND_MESSAGE, bundle, this);


                       /* Message message = messages.get(0);
                        message.setMessageAT("Sent");
                        messageRecyclerViewAdapter.notifyDataSetChanged();*/
                        if (outPut.has(ResponseParameters.MESSAGES_INFO)) {
//                            messages.remove(messageToRemove);
//                            messageRecyclerViewAdapter.notifyDataSetChanged();
                            showDeleteBtn();
//                            System.out.println("Message Info  "+messageObject.toString());
//                            Toast.makeText(this, messageObject.toString(), Toast.LENGTH_SHORT).show();
//                            Message message = new Message();
//                            message.setId(messageObject.getString("id"));
//                            message.setReply(messageObject.getString("reply"));
//                            message.setReadStatus(messageObject.getString("read_status"));
//                            message.setMessageAT(messageObject.getString("messagedAT"));
//                            message.setLinkName(messageObject.getString("user_id"));
//                            messages.add(0, message);
//                            messageRecyclerViewAdapter.notifyItemInserted(0);
//                            recyclerChat.scrollToPosition(0);
//                            Intent broadCast = new Intent();
//                            broadCast.setAction(ResponseParameters.UPDATE_CHAT);
//                            JSONObject jsonObject = new JSONObject(user.getUserInfo());
//                            jsonObject.put(ResponseParameters.LAST_MSG_FLAG, true);
//                            jsonObject.put(ResponseParameters.LAST_MSG_INFO, messageObject);
//                            user.setUserInfo(jsonObject.toString());
//                            broadCast.putExtra("USER", user);
//                            broadCast.putExtra(ResponseParameters.UPDATE_CHAT, "");
//                            sendBroadcast(broadCast);
                        }

                    } else {
                        //no userfound
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                }
            } else if (i == 3) {// for updating list
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response
                        clear();
                        Utils.showPopup(ChatActivity.this, getString(R.string.chat_delete_success_msg));
                    }
                }
            } else if (i == 6) {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response
//                        messageRecyclerViewAdapter.notifyDataSetChanged();
                        Utility.hideLoading();
                        recyclerChat.smoothScrollToPosition(0);
                    }
                }
            }

        } catch (Exception e) {
            //mo user found
            e.printStackTrace();
        }
//        sendPdfAsMessage();
//        getReplys();

       //
    }

    public void clear() {
        final int size = messages.size();
        messages.clear();
        messageRecyclerViewAdapter.notifyItemRangeRemoved(0, size);
        showDeleteBtn();
        try {
            Intent broadCast = new Intent();
            broadCast.setAction(ResponseParameters.UPDATE_CHAT);
            JSONObject jsonObject = new JSONObject(user.getUserInfo());
            jsonObject.put(ResponseParameters.UNREAD_MESSAGE_COUNT, 0);
            jsonObject.put(ResponseParameters.LAST_MSG_FLAG, false);
            user.setUserInfo(jsonObject.toString());
            broadCast.putExtra("USER", user);
            broadCast.putExtra(ResponseParameters.UPDATE_CHAT, "");
            sendBroadcast(broadCast);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showDeleteBtn() {
        if (action_delete != null)
            if (messages.size() > 0) {
                action_delete.setVisible(true);
            } else {
                action_delete.setVisible(false);
            }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivSendMessage:
                if (!etMessage.getText().toString().trim().isEmpty()){
                    if (messageId.isEmpty()||RefrenceMessage.isEmpty()){
                        sendMessage(getMessageObject(EmojiMapUtil.replaceUnicodeEmojis(etMessage.getText().toString())));
                    }else{
                        sendMessage(getMessageObject(EmojiMapUtil.replaceUnicodeEmojis(etMessage.getText().toString())));
                        messageId="";
                        RefrenceMessage="";
                    }

                }
                break;
            case R.id.fab:
                hideShowFab(false);
                fabClicked = true;
                getMessage();
                break;
            case R.id.llProfile:
                if (user.getIsDeleted().equalsIgnoreCase("1")) {
                }else{
                    openProfile();
                }

                break;
            case R.id.header:
                if (user.getIsDeleted().equalsIgnoreCase("1")) {
                }else{
                    openProfile();
                }

                break;
            case R.id.iv_close:
                action_context_bar.setVisibility(GONE);
                contextMenuVisible = false;
                messageRecyclerViewAdapter.clearSelection();
                break;
            case R.id.iv_delete:
                deleteSelectedMessages();
                break;
            case R.id.iv_copy:
                copySelectedMessages();
                break;
            case R.id.iv_reply:
                messageReply();
                break;

            case R.id.ivAttachment:
              sendPdfAsMessage();
                break;
            case R.id.cancel_chat:
                mainContainer.setVisibility(GONE);
                contextMenuVisible = false;
                action_context_bar.setVisibility(GONE);
                messageRecyclerViewAdapter.clearSelection();
                messageId="";
                break;
        }
    }

    private void copySelectedMessages() {
        ArrayList<Pair<Integer, Message>> selectedItems = messageRecyclerViewAdapter.getSelectedItems();
        int size = selectedItems.size();
        Utils.showPopup(this, size+" messages copied");
        StringBuilder text = new StringBuilder();
        for(Pair<Integer, Message> pair: selectedItems){
            String reply = pair.second.getReply();
            String userName;
            if (!pair.second.getUserId().equalsIgnoreCase(SharedPreferencesMethod.getUserId(this))) {
                userName = user.getFirstName() +" " + user.getLastName();
            } else {
                User userInfo = SharedPreferencesMethod.getUserInfo(this);
                userName = userInfo.getFirstName() +" "+ userInfo.getLastName();
            }
            text.append(userName+": " +reply).append("\n");
        }

        Utils.copyToClipboard(text.toString(), this);
        hideContextMenu();
    }

    private void deleteSelectedMessages() {
        String string = getString(R.string.delete_selected_msg_dialog_text);
        Utils.showPopup(this, string,
                getString(R.string.delete_btn_text), getString(R.string.cancel_btn_text),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        messageRecyclerViewAdapter.deleteSelection();
                        hideContextMenu();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
    }

    private void openProfile() {
        if (user.getRole().equalsIgnoreCase("KU1")) {
            Intent intent = new Intent(this, OtherUserProfileActivity.class);
            intent.putExtra("USER", user);
            System.out.println("User Object Data "+ user.getUserInfo());
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        if (receiveMessage != null) {
            unregisterReceiver(receiveMessage);
            receiveMessage = null;
        }

        super.onDestroy();
    }


    public void showContextMenu() {
        int size = messageRecyclerViewAdapter.getSelectedItems().size();
        if (size > 0) {
            contextMenuVisible = true;
            action_context_bar.setVisibility(VISIBLE);
            String s = getString(R.string.message_selected_text);
            tvSelectionCount.setText(size + " " + s);
            messageReplyValidation(size);
        } else {
            contextMenuVisible = false;
            action_context_bar.setVisibility(GONE);
            messageRecyclerViewAdapter.clearSelection();
            mainContainer.setVisibility(GONE);
        }
    }
}
