package com.codeholic.kotumb.app.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Adapter.LikeListRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.Model.PostLikes;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.View.DividerItemDecoration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class PostLikesActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.tvNouser)
    TextView tvNouser;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.fabAddConnections)
    FloatingActionButton fabAddConnections;

    @BindView(R.id.adView)
    ImageView adView;
    @BindView(R.id.rlAdView)
    RelativeLayout rlAdView;
    public ArrayList<PostLikes> postLikes = new ArrayList<>();
    Post post = new Post();
    Bundle bundle;

    LikeListRecyclerViewAdapter likeListRecyclerViewAdapter;
    int pagination = 0;
    private LinearLayoutManager linearLayoutManager;
    com.codeholic.kotumb.app.View.ProgressBar progressBar = null;
    private BroadcastReceiver receiver;

    // onCreate method initializing view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connections_group_invite);
        ButterKnife.bind(this);
        initToolbar();
        init();
        setBroadCast();
        IntentFilter filterAdd = new IntentFilter();
        filterAdd.addAction(ResponseParameters.ADD_RECEIVED);
        registerReceiver(getAd, filterAdd);
    }

    private BroadcastReceiver getAd = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            try {
//                HomeScreenActivity.setUpAdImage(intent.getStringExtra(ResponseParameters.ADD_RECEIVED), rlAdView, adView, PostLikesActivity.this, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    private void setUpAdImage(String adResponse) {
        hideAdView();
        try {
            if (!adResponse.isEmpty()) {
                final JSONObject ad = new JSONObject(adResponse);
                if (ad.has(ResponseParameters.AD)) {
                    if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.IMAGE)) {
                        if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE).isEmpty()) {
                            rlAdView.setVisibility(VISIBLE);
                            new AQuery(this).id(adView).image(API.imageAdUrl + ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE), true, true, 300, R.drawable.ic_user);
                            adView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.URL)) {
                                            if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL).isEmpty()) {
                                                Utility.prepareCustomTab(PostLikesActivity.this, ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL));
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideAdView() {
        rlAdView.setVisibility(GONE);
    }

    private void setBroadCast() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //do something based on the intent's action

                if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_POST_LIKE)) {
                    Post post = (Post) intent.getSerializableExtra(ResponseParameters.POST);
                    PostLikes postLike = (PostLikes) intent.getSerializableExtra(ResponseParameters.LIKEINFO);
                    if (PostLikesActivity.this.post.post_id.trim().equalsIgnoreCase(post.post_id.trim())) {
                        boolean flag = true;
                        if (PostLikesActivity.this.post.post_id.trim().equalsIgnoreCase(post.post_id.trim())) {
                            for (int i = 0; i < postLikes.size(); i++) {
                                if (postLikes.get(i).userId.trim().equalsIgnoreCase(postLike.userId.trim())) {
                                    flag = false;
                                    break;
                                }
                            }
                        }
                        if (likeListRecyclerViewAdapter != null && flag) {
                            postLikes.add(postLike);
                            likeListRecyclerViewAdapter.notifyItemRangeInserted(postLikes.size() - 1, 1);

                        }
                    }
                }

                if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_POST_UNLIKE)) {
                    PostLikes postLike = (PostLikes) intent.getSerializableExtra(ResponseParameters.LIKEINFO);
                    Post post = (Post) intent.getSerializableExtra(ResponseParameters.POST);
                    if (PostLikesActivity.this.post.post_id.trim().equalsIgnoreCase(post.post_id.trim())) {
                        for (int i = 0; i < postLikes.size(); i++) {
                            if (postLikes.get(i).userId.trim().equalsIgnoreCase(postLike.userId.trim())) {
                                postLikes.remove(i);
                                if (likeListRecyclerViewAdapter != null) {
                                    likeListRecyclerViewAdapter.notifyItemRemoved(i);
                                    likeListRecyclerViewAdapter.notifyItemRangeChanged(0, postLikes.size());
                                }
                                break;
                            }
                        }
                    }
                }
                Log.e("test", "test");
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.GROUP_POST_LIKE);
        filter.addAction(ResponseParameters.GROUP_POST_UNLIKE);
        registerReceiver(receiver, filter);
    }

    //init data of user
    private void init() {
        bundle = getIntent().getBundleExtra(ResponseParameters.BUNDLE);
        //postLikes = (ArrayList<PostLikes>) bundle.getSerializable(ResponseParameters.POST_LIKES);
        post = (Post) bundle.getSerializable(ResponseParameters.POST);


        API.sendRequestToServerGET(PostLikesActivity.this, API.GROUP_POST_LIKES + post.post_id + "/" + (pagination), API.GROUP_POST_LIKES);// service call for getting connection


        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvUserList.setLayoutManager(linearLayoutManager);
        likeListRecyclerViewAdapter = new LikeListRecyclerViewAdapter(rvUserList, this, postLikes);
        likeListRecyclerViewAdapter.setImageBaseUrl(bundle.getString(ResponseParameters.IMAGE_BASE_URL));
        rvUserList.addItemDecoration(
                new DividerItemDecoration(this, R.drawable.divider));
        rvUserList.setAdapter(likeListRecyclerViewAdapter);
        //llLoading.setVisibility(GONE);
        //load more setup for recycler adapter
        likeListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (postLikes.size() >= 10) {
                    rvUserList.post(new Runnable() {
                        public void run() {
                            postLikes.add(null);
                            likeListRecyclerViewAdapter.notifyItemInserted(postLikes.size() - 1);
                        }
                    });

                    API.sendRequestToServerGET(PostLikesActivity.this, API.GROUP_POST_LIKES + postLikes.get(0).post_id + "/" + (pagination + 10), API.GROUP_POST_LIKES_UPDATE);// service call for getting connection
                }
            }
        });


        /*if (postLikes.size() <= 0) {
            llNotFound.setVisibility(View.VISIBLE);
            tvNouser.setText(getResources().getString(R.string.no_likes_on_this_post_msg));
        }*/
    }

    //init toolbar function
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.likes_btn_text));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //setview for getting user connection

    //getResponse for getting list of urlImages
    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        Log.e("response", "" + outPut);

        if (progressBar != null) {
            progressBar.dismiss(); // dismissing progressbar
        }

        try {
            if (i == 0) {// for updating list
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {

                    if (outPut.has(ResponseParameters.Success)) { // success response
                        ArrayList<PostLikes> updatedPostLies = new ArrayList<>();
                        Gson gson = new Gson(); // creates a Gson instance
                        Type listType = new TypeToken<List<PostLikes>>() {
                        }.getType();
                        updatedPostLies = gson.fromJson(outPut.getString(ResponseParameters.GROUP_POST_LIKES), listType);
                        postLikes.addAll(updatedPostLies);
                        if (updatedPostLies.size() > 0) {
                            //updating recycler view
                            likeListRecyclerViewAdapter.notifyItemRangeInserted(postLikes.size() - updatedPostLies.size(), updatedPostLies.size());
                            likeListRecyclerViewAdapter.setLoaded();
                        } else {
                            llNotFound.setVisibility(VISIBLE);
                        }
                    } else {
                        llNotFound.setVisibility(VISIBLE);
                    }
                } else {
                    llNotFound.setVisibility(VISIBLE);
                }
            } else if (i == 1) {// for updating list
                postLikes.remove(postLikes.size() - 1); // removing of loading item
                likeListRecyclerViewAdapter.notifyItemRemoved(postLikes.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response
                        ArrayList<PostLikes> updatedPostLies = new ArrayList<>();


                        Gson gson = new Gson(); // creates a Gson instance
                        Type listType = new TypeToken<List<PostLikes>>() {
                        }.getType();
                        updatedPostLies = gson.fromJson(outPut.getString(ResponseParameters.GROUP_POST_LIKES), listType);
                        postLikes.addAll(updatedPostLies);
                        if (updatedPostLies.size() > 0) {
                            pagination = pagination + 10;
                            //updating recycler view
                            likeListRecyclerViewAdapter.notifyItemRangeInserted(postLikes.size() - updatedPostLies.size(), updatedPostLies.size());
                            likeListRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            }

        } catch (Exception e) {
            //mo user found

            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        if (getAd != null) {
            unregisterReceiver(getAd);
            getAd = null;
        }
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabAddConnections:

                break;
        }
    }
}
