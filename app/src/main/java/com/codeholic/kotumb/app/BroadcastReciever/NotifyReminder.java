package com.codeholic.kotumb.app.BroadcastReciever;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.codeholic.kotumb.app.Activity.AddNoteActivity;
import com.codeholic.kotumb.app.Model.Note;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Services.GetPreLoadDataService;
import com.codeholic.kotumb.app.Services.OpenCustomTabService;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;


public class NotifyReminder extends BroadcastReceiver {

    public static final int ID_BIG_NOTIFICATION = 234;
    public static int ID_SMALL_NOTIFICATION = 235;
    public static int CALLSERVICE = 236;
    public static String SERVICE_CALL = "SERVICE_CALL";
    public static String CONTACT_US = "CONTACT_US";
    private Context mCtx;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Log.e("onReceive", "onReceive");
        mCtx = context;
        if (intent.getAction().equalsIgnoreCase(SERVICE_CALL)) {
            GetPreLoadDataService.startActionGetNameOrg(mCtx);
            GetPreLoadDataService.startActionGetSchools(mCtx);
            GetPreLoadDataService.startActionGetCourses(mCtx);
            GetPreLoadDataService.startActionGetRoles(mCtx);
            GetPreLoadDataService.startActionGetSkills(mCtx);
            GetPreLoadDataService.startActionGetLanguage(mCtx);
        } else if (intent.getAction().equalsIgnoreCase(CONTACT_US)) {

            //Log.e("TAG", "URL " + API.contactUsLink + SharedPreferencesMethod.getUserId(mCtx));

            OpenCustomTabService.startActionOpenSupportChat(mCtx, intent.getStringExtra(ResponseParameters.DESCRIPTION));
        } else {
            Note note = null;

            ByteArrayInputStream bis = new ByteArrayInputStream(intent.getByteArrayExtra("NOTE"));
            ObjectInput in = null;
            try {
                in = new ObjectInputStream(bis);
                note = (Note) in.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            ID_SMALL_NOTIFICATION = Integer.parseInt(note.getNoteID());
            Intent intent_note = new Intent(context, AddNoteActivity.class);
            intent_note.putExtra(RequestParameters.NOTE, (Parcelable) note);
            showSmallNotification(note.getNoteTitle(), note.getNoteDes(), intent_note);
        }

    }

    public void showSmallNotification(String title, String message, Intent intent) {
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mCtx,
                        ID_SMALL_NOTIFICATION,
                        intent,
                        PendingIntent.FLAG_ONE_SHOT
                );


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx);
        Notification notification;
        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message).setBigContentTitle(title))
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mCtx.getResources(), R.mipmap.ic_launcher))
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long[0])
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ID_SMALL_NOTIFICATION, notification);
    }
}
