package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Adapter.FriendListRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.DisplayUserObject;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchConnectionActivtiy extends AppCompatActivity {

    public static final String CONNECTIONS = "connections";
    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.searchview)
    SearchView searchview;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.no_data)
    TextView no_data;

    int pagination = 0;
    private String newText = "";
    Activity context;
    ArrayList<User> userObjects = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private FriendListRecyclerViewAdapter friendListRecyclerViewAdapter;

    //oncreate method for setting content view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_connections);
        ButterKnife.bind(this);
        init();
        initToolbar();
        searchViewSetUp();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.UPDATE_USER);
    }

    //setting toolbar and header title
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //init values and show user details
    private void init() {
        context = this;
    }

    //search view setup
    private void searchViewSetUp() {
        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {


            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                SearchConnectionActivtiy.this.newText = newText;
                userObjects.clear();
                pagination = 0;
                if (friendListRecyclerViewAdapter != null) {
                    friendListRecyclerViewAdapter.notifyDataSetChanged();
                }
                //rvUserList.setAdapter(getAdapter());

                if (!newText.isEmpty()) {
                    String userId = SharedPreferencesMethod.getUserId(SearchConnectionActivtiy.this);
                    String url = API.CONNECTECTIONS + userId + "/" + userId + "/" + newText;
                    API.sendRequestToServerGET(context, url, API.CONNECTECTIONS + 2); // service call for user list
                    no_data.setVisibility(View.GONE);
                    llLoading.setVisibility(View.VISIBLE);
                    userObjects.clear();
                }
                return false;
            }
        });

    }


    //getting response from server
    public void getResponse(JSONObject outPut, int i) {
        try {
            Log.e("response", outPut.toString());
            if (i == 0) { // get search user list
                onGetResponseSearch(outPut);
            }
            if (i == 1) { // load more response
                onGetLoadMoreResponse(outPut);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onGetLoadMoreResponse(JSONObject outPut) throws JSONException {
        userObjects.remove(userObjects.size() - 1); // remove loading item
        friendListRecyclerViewAdapter.notifyItemRemoved(userObjects.size());
        if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
            if (outPut.has(ResponseParameters.Errors)) {
            } else if (outPut.has(ResponseParameters.Success)) {
                if (outPut.has(ResponseParameters.CONNECTIONS)) {
                    JSONArray jsonArray = outPut.getJSONArray(ResponseParameters.CONNECTIONS);
                    for (int j = 0; j < jsonArray.length(); j++) {
                        JSONObject searchResults = jsonArray.getJSONObject(j);
                        User user = new User();
                        user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                        user.setLastName(searchResults.getString(ResponseParameters.LastName));
                        user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                        user.setUserId(searchResults.getString(ResponseParameters.UserId));
                        user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                        user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                        user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                        user.setCity(searchResults.getString(ResponseParameters.City));
                        user.setState(searchResults.getString(ResponseParameters.State));
                        user.setUserInfo(searchResults.toString());
                        userObjects.add(user);
                    }
                    pagination = pagination + 6;
                    //add next urlImages
                    friendListRecyclerViewAdapter.notifyDataSetChanged();
                    friendListRecyclerViewAdapter.setLoaded();

                }
            }
        }
    }

    private void onGetResponseSearch(JSONObject outPut) throws JSONException {
        llLoading.setVisibility(View.GONE);
        if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
            if (outPut.has(ResponseParameters.Errors)) { // error
                final Snackbar snackbar = Snackbar.make(rvUserList, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                snackbar.show();
            } else if (outPut.has(ResponseParameters.Success)) {
                if (outPut.has(CONNECTIONS)) {
                    JSONArray outPutJSONArray = outPut.getJSONArray(CONNECTIONS);
                    userObjects.clear();

                    userObjects = new ArrayList<>();
                    for (int j = 0; j < outPutJSONArray.length(); j++) {
                        JSONObject searchResults = outPut.getJSONArray(ResponseParameters.CONNECTIONS).getJSONObject(j);
                        User user = new User();
                        user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                        user.setLastName(searchResults.getString(ResponseParameters.LastName));
                        user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                        user.setUserId(searchResults.getString(ResponseParameters.UserId));
                        user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                        user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                        user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                        user.setCity(searchResults.getString(ResponseParameters.City));
                        user.setState(searchResults.getString(ResponseParameters.State));
                        user.setUserInfo(searchResults.toString());
                        userObjects.add(user);
                    }

                    linearLayoutManager = new LinearLayoutManager(SearchConnectionActivtiy.this);
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    rvUserList.setLayoutManager(linearLayoutManager);
                    rvUserList.setNestedScrollingEnabled(false);
                    friendListRecyclerViewAdapter = new FriendListRecyclerViewAdapter(rvUserList, this, userObjects, null);
                    rvUserList.setAdapter(friendListRecyclerViewAdapter);


                    friendListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {
                            if (userObjects.size() >= 6) {
                                Log.e("inside", "inside");
                                rvUserList.post(new Runnable() {
                                    public void run() {
                                        userObjects.add(null);
                                        friendListRecyclerViewAdapter.notifyItemInserted(userObjects.size() - 1);
                                    }
                                });
                                String userId = SharedPreferencesMethod.getUserId(SearchConnectionActivtiy.this);
                                String url = API.CONNECTECTIONS + userId + "/" + userId + "/" + newText + "/?offset=" + (pagination + 6);
                                API.sendRequestToServerGET(SearchConnectionActivtiy.this, url, API.CONNECTECTIONS_UPDATE + 2);
                            }
                        }
                    });
                }
            } else {
                final Snackbar snackbar = Snackbar.make(rvUserList, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                snackbar.show();
            }
        }
    }

    private void onLoadMoreCalled() {
        if (userObjects.size() >= 10) {
            Log.e("inside", "inside");
            rvUserList.post(new Runnable() {
                public void run() {
                    userObjects.add(null); // add loading item
                    //userRecyclerViewAdapter.notifyItemInserted(userObjects.size() - 1);
                }
            });
            API.sendRequestToServerGET(context, API.SEARCH_USERS + "/" + searchview.getQuery().toString().trim() + "/" + (pagination + 10) + "/?userid=" + SharedPreferencesMethod.getUserId(SearchConnectionActivtiy.this), API.SEARCH_USERS_UPDATE);// get next urlImages
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @NonNull
    private View.OnClickListener getItemClickListener(final DisplayUserObject userObject) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchConnectionActivtiy.this, OrganizationActivity.class);
                intent.putExtra("data", new Gson().toJson(userObject));
                startActivity(intent);
            }
        };
    }
}
