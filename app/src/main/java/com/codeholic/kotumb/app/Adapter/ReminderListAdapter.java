package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Activity.AddNoteActivity;
import com.codeholic.kotumb.app.Model.Note;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.fragments.BaseFragment;
import com.codeholic.kotumb.app.fragments.NotepadFragment;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by DELL on 11/17/2017.
 */

public class ReminderListAdapter extends RecyclerView.Adapter<ReminderListAdapter.NoteViewHolder> {
    List<Note> notes;
    Activity activity;
    private BaseFragment fragment;

    public ReminderListAdapter(Activity activity, List<Note> notes) {
        this.notes = notes;
        this.activity = activity;
    }

    public void setFragment(BaseFragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reminder_list_item, parent, false);
        NoteViewHolder noteViewHolder = new NoteViewHolder(view);
        return noteViewHolder;
    }


    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        Note note = notes.get(position);
        if (!note.getNoteTitle().trim().isEmpty()) {
            StringBuilder sb = new StringBuilder(note.getNoteTitle());
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
            holder.tvNoteName.setText(sb.toString());
        }
        holder.tvNote.setText(note.getNoteDes());
        if (!note.getNoteTime().trim().isEmpty()) {
            try {
                holder.tvNoteTime.setVisibility(View.VISIBLE);
                DateFormat df = new SimpleDateFormat("HH:mm:ss");
                DateFormat df_ = new SimpleDateFormat("hh:mm a");
                holder.tvNoteTime.setText(df_.format(df.parse(note.getNoteTime())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            holder.tvNoteTime.setVisibility(View.GONE);
        }
        if (!note.getNoteDate().trim().isEmpty()) {
            try {
                holder.tvNoteDate.setVisibility(View.VISIBLE);
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");
                holder.tvNoteDate.setText(df_.format(df.parse(note.getNoteDate())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            holder.tvNoteDate.setVisibility(View.GONE);
        }
        setOnClickListener(holder, note, position);
    }

    public void setOnClickListener(final NoteViewHolder holder, final Note note, final int position) {

        holder.cvMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.REMINDER = position;
                Intent intent = new Intent(activity, AddNoteActivity.class);
                intent.putExtra(RequestParameters.NOTE, (Parcelable) note);
                activity.startActivity(intent);
            }
        });

        holder.llAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Calendar calendar = Calendar.getInstance();

                    SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
                    Date _24HourDt = _24HourSDF.parse(note.getNoteTime());
                    calendar.setTime(_24HourDt);

                    int hour;
                    int minute;
                    minute = (calendar.get(Calendar.MINUTE));
                    hour = calendar.get(Calendar.HOUR_OF_DAY);

                    TimePickerDialog tpd = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                            Log.e("Time", hourOfDay + " " + minute + " " + second);
                        }
                    }, hour, minute, false);

                    try {
                        tpd.setMaxTime(hour, minute, 0);
                        tpd.setMinTime(hour, minute, 0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    tpd.enableMinutes(true);
                    tpd.setVersion(TimePickerDialog.Version.VERSION_2);
                    tpd.show(activity.getFragmentManager(), "TimePickDialog");


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        holder.llCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Calendar calendar = Calendar.getInstance();
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = df.parse(note.getNoteDate());
                    calendar.setTime(date);
                    DatePickerDialog datePickerDialog = new DatePickerDialog(activity, R.style.datepicker,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {


                                }
                            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                    try {
                        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                    } catch (Exception e) {

                    }
                    datePickerDialog.setTitle("");
                    datePickerDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                dialogBuilder.setTitle(activity.getResources().getString(R.string.app_alert_text));
                dialogBuilder.setMessage(activity.getResources().getString(R.string.confirm_delete_message));
                dialogBuilder.setCancelable(true);
                dialogBuilder.setPositiveButton(activity.getResources().getString(R.string.done_btn_text), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            if (!Utility.isConnectingToInternet(activity)) {
                                final Snackbar snackbar = Snackbar.make(holder.llDelete, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                                snackbar.show();
                                return;
                            }
                            notes.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, notes.size());
                            if (notes.size() == 0 && fragment != null) {
                                ((NotepadFragment) fragment).showNotFound();
                            }

                            sendRequest(activity, API.DELETE_NOTE + SharedPreferencesMethod.getUserId(activity) + "/" + note.getNoteID());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                });

                dialogBuilder.setNegativeButton(activity.getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });

                dialogBuilder.create().show();


            }
        });

    }


    public void sendRequest(final Activity context, String url) {
        try {
            AQuery aq = new AQuery(context);
            //Log.d("Url ", url);
            aq.ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {
                        if (json != null) {
                            Log.e(context.getClass().getName(), json.toString());
                            json.put("RP_MESSAGE", "ALL_OKAY");
                            getResponse(json);
                        } else {
                            JSONObject output = new JSONObject();
                            if (status.getCode() == AjaxStatus.NETWORK_ERROR) {
                                output.put("RP_MESSAGE", "NO NETWORK");
                            } else {
                                output.put("RP_MESSAGE", "ERROR");
                            }
                            getResponse(output);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        getResponse(new JSONObject());
                    }
                }
            }.method(AQuery.METHOD_GET).header("Auth-Key","KOTUMB__AuTh_keY"));


        } catch (Exception e) {
            e.printStackTrace();
            getResponse(new JSONObject());
        }
    }


    public void getResponse(JSONObject output) {
        try {
            if (output.has(ResponseParameters.Success)) {
//                Toast.makeText(activity, output.getString(ResponseParameters.Success), Toast.LENGTH_SHORT).show();
                Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
            } else {
//                Toast.makeText(activity, activity.getResources().getString(R.string.error_text), Toast.LENGTH_SHORT).show();
                Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class NoteViewHolder extends RecyclerView.ViewHolder {
        TextView tvNoteName, tvNote, tvNoteDate, tvNoteTime;
        CardView cvMain;
        LinearLayout llAlarm, llCalender, llDelete;

        NoteViewHolder(View view) {
            super(view);
            cvMain = (CardView) view.findViewById(R.id.cvMain);
            tvNoteName = (TextView) view.findViewById(R.id.tvNoteName);

            tvNoteDate = (TextView) view.findViewById(R.id.tvNoteDate);
            tvNoteTime = (TextView) view.findViewById(R.id.tvNoteTime);
            tvNote = (TextView) view.findViewById(R.id.tvNote);
            llAlarm = (LinearLayout) view.findViewById(R.id.llAlarm);
            llCalender = (LinearLayout) view.findViewById(R.id.llCalender);
            llDelete = (LinearLayout) view.findViewById(R.id.llDelete);
        }
    }
}
