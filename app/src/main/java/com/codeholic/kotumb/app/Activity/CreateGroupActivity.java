package com.codeholic.kotumb.app.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ConvertBitmap;
import com.codeholic.kotumb.app.Utility.EventNames;
import com.codeholic.kotumb.app.Utility.ImgeCroper.Crop;
import com.codeholic.kotumb.app.Utility.PermissionsUtils;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.RotateImageCode;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.View.CircleImageView;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateGroupActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, IPickResult, TextWatcher {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;
/*
    @BindView(R.id.container)
    NestedScrollView nestedScrollView;*/

    @BindView(R.id.etGroupNameLayout)
    TextInputLayout etGroupNameLayout;

    @BindView(R.id.etGroupName)
    TextInputEditText etGroupName;

    @BindView(R.id.etGroupTypeLayout)
    TextInputLayout etGroupTypeLayout;

    @BindView(R.id.etGroupCateLayout)
    TextInputLayout etGroupCateLayout;

    @BindView(R.id.etGroupSumLayout)
    TextInputLayout etGroupSumLayout;

    @BindView(R.id.etGroupSum)
    TextInputEditText etGroupSum;

    @BindView(R.id.spGroupType)
    Spinner spGroupType;

    @BindView(R.id.spGroupCategory)
    Spinner spGroupCategory;

    @BindView(R.id.llSave)
    LinearLayout llSave;

    @BindView(R.id.llSpace)
    LinearLayout llSpace;

    @BindView(R.id.lladdMembers)
    LinearLayout lladdMembers;

    @BindView(R.id.ivGroupImage)
    CircleImageView ivGroupImage;

    @BindView(R.id.rlChangeImage)
    RelativeLayout rlChangeImage;

    @BindView(R.id.tvSave)
    TextView tvSave;

    @BindView(R.id.customCategoryEditText)
    EditText customCategoryEditText;

    @BindView(R.id.customCategoryLayout)
    TextInputLayout customCategoryLayout;

    AlertDialog titleAlert;
    AlertDialog alert;
    com.codeholic.kotumb.app.View.ProgressBar progressBar = null;
    Context context;
    Group group;
    JSONArray group_categories = new JSONArray();
    public static final int REQUEST_CROP = 6709;
    public static final int REQUEST_CONNECTION = 6710;
    String logo = "";
    String admins = "";
    String members = "";
    private boolean isCustomeCatgorySet;

    // oncreate method initializing view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        ButterKnife.bind(this);
        initToolbar();
        init();
        setClickListener();
        setTextWatcher(etGroupName, etGroupNameLayout);
        setTextWatcher(etGroupSum, etGroupSumLayout);
        populateSpinner();
        //setTextWatcher(etNote, etNoteLayout);

        getCategories();

       // nestedScrollView.setNestedScrollingEnabled(true);
    }


    private boolean keyboardShown(View rootView) {

        final int softKeyboardHeight = 100;
        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        DisplayMetrics dm = rootView.getResources().getDisplayMetrics();
        int heightDiff = rootView.getBottom() - r.bottom;
        return heightDiff > softKeyboardHeight * dm.density;
    }


    private void init() {
        context = this;
        etGroupName.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (keyboardShown(etGroupName.getRootView())) {
                    Log.d("keyboard", "keyboard UP");
                    etGroupName.setCursorVisible(true);
                    etGroupName.clearFocus();
                } else {
                    Log.d("keyboard", "keyboard Down");
                    etGroupName.setCursorVisible(false);
                    etGroupName.clearFocus();
                }
            }
        });

        etGroupSum.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (keyboardShown(etGroupSum.getRootView())) {
                    Log.d("keyboard", "keyboard UP 1");
                    etGroupSum.setCursorVisible(true);
                } else {
                    Log.d("keyboard", "keyboard Down 1");
                    etGroupSum.setCursorVisible(false);
                    etGroupSum.clearFocus();
                }
            }
        });


        if (getIntent().hasExtra(ResponseParameters.GROUP)) {
            header.setText(getResources().getString(R.string.header_main_update_group_title));
            group = (Group) getIntent().getSerializableExtra(ResponseParameters.GROUP);
            fillValues();
        }
    }

    private void fillValues() {
        try {
            etGroupName.setText(group.groupName.trim());
            etGroupSum.setText(group.groupSummary.trim());
            new AQuery(this).id(ivGroupImage).image(getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL) + group.logo, true, true, 300, R.drawable.ic_user);
            lladdMembers.setVisibility(View.GONE);
            llSpace.setVisibility(View.GONE);
            tvSave.setText(getResources().getString(R.string.update_btn_text));
            Log.e("TAG", " " + group.category);
            Log.e("TAG", " " + group.privacy);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCategories() {
        try {
            if (!Utility.isConnectingToInternet(context)) { // check net connection
                final Snackbar snackbar = Snackbar.make(llSave, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            progressBar = new com.codeholic.kotumb.app.View.ProgressBar(CreateGroupActivity.this);
            progressBar.show(getResources().getString(R.string.app_please_wait_text));

            String url = API.GROUP_CATEGORIES + SharedPreferencesMethod.getUserId(this);
            API.sendRequestToServerGET(context, url, API.GROUP_CATEGORIES);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickListener() {
        llSave.setOnClickListener(this);
        lladdMembers.setOnClickListener(this);
        rlChangeImage.setOnClickListener(this);
    }


    //override method of picking image
    @Override
    public void onPickResult(PickResult pickResult) {
        Log.e("TAG", "Result ");
        if (pickResult.getError() == null) {
            //If you want the Uri.
            //Mandatory to refresh image from Uri.
            //getImageView().setImageURI(null);

            //Setting the real returned image.
            //getImageView().setImageURI(r.getUri());

            //If you want the Bitmap.

            //imvUserPic.setImageBitmap(pickResult.getBitmap());
            beginCrop(pickResult.getUri());
            //r.getPath();
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            //Toast.makeText(this, pickResult.getError().getMessage(), Toast.LENGTH_LONG).show();
            Utils.showPopup(this, pickResult.getError().getMessage());
        }
    }

    //for cropping image
    private void beginCrop(Uri source) {
        try {
            Uri destination = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/kotumb_group.jpeg"));
            Crop.of(source, destination).asSquare().start(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // initToolbar method initializing Toolbar
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        // setting title
        header.setText(getResources().getString(R.string.header_main_create_group_title));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setTextWatcher(final EditText editText, final TextInputLayout textInputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    textInputLayout.setErrorEnabled(false);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //onActivityResult
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CONNECTION) {
            if (data.hasExtra(RequestParameters.MEMBERS)) {
                members = data.getStringExtra(RequestParameters.MEMBERS);
            }
            if (data.hasExtra(RequestParameters.ADMINS)) {
                admins = data.getStringExtra(RequestParameters.ADMINS);
            }

            Log.e("DATA", admins + " " + members);
        }

        if (requestCode == REQUEST_CROP) {// for request crop of image
            Uri destination = Crop.getOutput(data);

            try {
                Bitmap bitmap1 = MediaStore.Images.Media.getBitmap(context.getContentResolver(), destination);
                bitmap1 = ConvertBitmap.Mytransform(bitmap1);
                bitmap1 = RotateImageCode.rotateImage(bitmap1, new File(Environment.getExternalStorageDirectory() + "/appy_celebration_temp.jpeg"));
                ByteArrayOutputStream datasecond = new ByteArrayOutputStream();
                bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, datasecond);
                byte[] bitmapdata = datasecond.toByteArray();

                File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temp2.jpeg");
                if (f.exists())
                    f.delete();
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bitmapdata);
                fo.close();
                String PicturePath = Environment.getExternalStorageDirectory() + File.separator + "temp2.jpeg";

                bitmap1 = BitmapFactory.decodeFile(PicturePath);
                //picByteArray = bitmapdata;


                if (bitmap1 != null) {
                    try {
                        ivGroupImage.setImageBitmap(bitmap1);
                        ivGroupImage.setVisibility(View.VISIBLE);
                        logo = "data:image/png;base64," + Base64.encodeToString(bitmapdata, Base64.NO_WRAP);
                        /*HashMap<String, Object> input = new HashMap<>();

                        input.put(RequestParameters.AVATAR,  "data:image/png;base64,"+ Base64.encodeToString(bitmapdata, Base64.NO_WRAP));
                        input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(context));
                        Log.e("input", "" + input);
                        API.sendRequestToServerPOST_PARAM(context, API.UPDATE_PROFILE_PIC, input);
                        pbImageLoading.setVisibility(View.VISIBLE);
                        cvEdit.setVisibility(View.GONE);*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lladdMembers:
                Intent i = new Intent(this, GroupInviteActivity.class);
                i.putExtra(ResponseParameters.CREATE_GROUP, "");
                startActivityForResult(i, REQUEST_CONNECTION);

                break;
            case R.id.llSave:
                if (validation()) {
                    createGroup();
                }
                break;

            case R.id.rlChangeImage:
                GelleryOrCameraPopup();
                break;
        }
    }

    private void createGroup() {
        //Toast.makeText(context, "createGroup Called", Toast.LENGTH_SHORT).show();
        try {
            if (!Utility.isConnectingToInternet(context)) { // check net connection
                final Snackbar snackbar = Snackbar.make(llSave, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }


            if (getIntent().hasExtra(ResponseParameters.GROUP)) {
                HashMap<String, Object> input = new HashMap();
                input.put(RequestParameters.GROUPID, "" + group.groupId);
                input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(CreateGroupActivity.this));
                input.put(RequestParameters.GROUP_NAME, "" + etGroupName.getText().toString().trim());
                input.put(RequestParameters.GROUP_SUMMARY, "" + etGroupSum.getText().toString().trim());
                if (!logo.trim().isEmpty())
                    input.put(RequestParameters.LOGO, "" + logo);
                if (!isCustomeCatgorySet) {
                    input.put(RequestParameters.CATEGORY, "" + group_categories.getJSONObject(spGroupCategory.getSelectedItemPosition()).getString(ResponseParameters.Id));
                } else {
                    input.put(RequestParameters.CATEGORY_CUSTOM, 1);
                    input.put(RequestParameters.CATEGORY, customCategoryEditText.getText().toString().trim());
                }
                input.put(RequestParameters.PRIVACY, "" + spGroupType.getSelectedItemPosition());
                Log.e("input", "" + input);
                progressBar = new com.codeholic.kotumb.app.View.ProgressBar(CreateGroupActivity.this);
                progressBar.show(getResources().getString(R.string.app_please_wait_text));
                String url = API.EDIT_GROUP;
                API.sendRequestToServerPOST_PARAM(CreateGroupActivity.this, url, input);// service call for share resume
            } else {
                HashMap<String, Object> input = new HashMap();
                input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(CreateGroupActivity.this));
                input.put(RequestParameters.GROUP_NAME, "" + etGroupName.getText().toString().trim());
                input.put(RequestParameters.GROUP_SUMMARY, "" + etGroupSum.getText().toString().trim());
                input.put(RequestParameters.LOGO, "" + logo);
                if (!isCustomeCatgorySet) {
                    input.put(RequestParameters.CATEGORY, "" + group_categories.getJSONObject(spGroupCategory.getSelectedItemPosition()).getString(ResponseParameters.Id));
                } else {
                    input.put(RequestParameters.CATEGORY, customCategoryEditText.getText().toString().trim());
                    input.put(RequestParameters.CATEGORY_CUSTOM, 1);
                }
                input.put(RequestParameters.PRIVACY, "" + spGroupType.getSelectedItemPosition());
                input.put(RequestParameters.ADMINS, "" + admins);
                input.put(RequestParameters.MEMBERS, "" + members);
                Log.e("input", "" + input);
                progressBar = new com.codeholic.kotumb.app.View.ProgressBar(CreateGroupActivity.this);
                progressBar.show(getResources().getString(R.string.app_please_wait_text));
                String url = API.CREATE_GROUP;
                API.sendRequestToServerPOST_PARAM(CreateGroupActivity.this, url, input);// service call for share resume
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //galleryOrCameraPopup for selecting image
    public void GelleryOrCameraPopup() {
        PickSetup pickSetup = new PickSetup().setTitle(getResources().getString(R.string.select_option_message_for_selecting_group_image));
        pickSetup.setTitleColor(getResources().getColor(R.color.textColor));
        pickSetup.setProgressText(getResources().getString(R.string.app_please_wait_text));
        pickSetup.setProgressTextColor(getResources().getColor(R.color.textColor));
        pickSetup.setCancelText(getResources().getString(R.string.cancel_btn_text));
        pickSetup.setCancelTextColor(getResources().getColor(R.color.colorPrimary));
        pickSetup.setButtonTextColor(getResources().getColor(R.color.textColor));
        pickSetup.setDimAmount(0.3f);
        pickSetup.setFlip(true);
        pickSetup.setMaxSize(500);
        pickSetup.setPickTypes(EPickType.GALLERY, EPickType.CAMERA);
        pickSetup.setCameraButtonText(getResources().getString(R.string.app_camera_btn_text));
        pickSetup.setGalleryButtonText(getResources().getString(R.string.app_gallery_btn_text));
        pickSetup.setButtonOrientationInt(LinearLayoutCompat.HORIZONTAL);
        pickSetup.setSystemDialog(false);
        pickSetup.setGalleryIcon(R.mipmap.gallery_colored);
        pickSetup.setCameraIcon(R.mipmap.camera_colored);
        pickSetup.setIconGravityInt(Gravity.LEFT);

        if (Build.VERSION.SDK_INT >= PermissionsUtils.SDK_INT_MARSHMALLOW) { // for permission
            if (PermissionsUtils.getInstance(context).requiredPermissionsGranted(context)) {
                PickImageDialog.build(pickSetup)
                        .setOnPickResult(this).show(this);
            }else {
                PickImageDialog.build(pickSetup)
                        .setOnPickResult(this).show(this);
            }
        } else {
            PickImageDialog.build(pickSetup)
                    .setOnPickResult(this).show(this);
            //new MyDialog(context, MyDialog.UPDATE_PROFILE, null).show();
        }
    }

    // onRequestPermissionsResult for geeting permission from user
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermissionsUtils.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                if (PermissionsUtils.getInstance(context).areAllPermissionsGranted(grantResults)) {
                    GelleryOrCameraPopup();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showPopUp(String title, boolean showYes, boolean showCancel) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = (LinearLayout) dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = (LinearLayout) dialogView.findViewById(R.id.llCancel);

        final LinearLayout first_layer = (LinearLayout) dialogView.findViewById(R.id.first_layer);
        final TextView tvTitle = (TextView) dialogView.findViewById(R.id.title);
        final View view = dialogView.findViewById(R.id.space);
        tvTitle.setText(title);
        if (!showYes) {
            view.setVisibility(View.GONE);
            llYes.setVisibility(View.GONE);
        }

        if (!showCancel) {
            view.setVisibility(View.GONE);
            llCancel.setVisibility(View.GONE);
        }

        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GroupInviteActivity.clearSelection();
                alert.dismiss();
                finish();
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert = dialogBuilder.create();
        alert.setCancelable(false);

        /*mobileAvailibilityPopup.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {



                final Animation myAnim = AnimationUtils.loadAnimation(CreateGroupActivity.this, R.anim.bounce);

                // Use bounce interpolator with amplitude 0.2 and frequency 20
                BounceInterpolator interpolator = new BounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);



                dialogView.startAnimation(myAnim);
            }
        });*/


        alert.show();

    }

    private boolean validation() {
        boolean result = true;
        disableError();
        clearFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etGroupName.getWindowToken(), 0);
        if (etGroupName.getText().toString().trim().isEmpty()) {
            if (result) {
                etGroupName.requestFocus();
                imm.showSoftInput(etGroupName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etGroupNameLayout, getResources().getString(R.string.enter_group_name_error_text));
            result = false;
        }
        if (result)
            if (spGroupType.getSelectedItemPosition() == 2) {
                showPopUp(getResources().getString(R.string.select_group_type_error_text), false, true);
                result = false;

                if (etGroupSum.getText().toString().trim().isEmpty()) {
                    enableError(etGroupSumLayout, getResources().getString(R.string.select_group_summary_error_text));
                    result = false;
                }

                return result;
            }

        if (result)
            if (spGroupCategory.getSelectedItemPosition() == group_categories.length() + 1) {
                showPopUp(getResources().getString(R.string.select_group_category_error_text), false, true);
                result = false;

                if (etGroupSum.getText().toString().trim().isEmpty()) {
                    enableError(etGroupSumLayout, getResources().getString(R.string.select_group_summary_error_text));
                    result = false;
                }

                return result;
            }

        if (etGroupSum.getText().toString().trim().isEmpty()) {
            if (result) {
                etGroupSum.requestFocus();
                imm.showSoftInput(etGroupSum, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etGroupSumLayout, getResources().getString(R.string.select_group_summary_error_text));
            result = false;
        }

        if (isCustomeCatgorySet && customCategoryEditText.getText().toString().isEmpty()) {
            //customCategoryEditText.setError("Required");
            customCategoryLayout.setError("Required");
            result = false;
            return false;
        }

        if (isAlreadyCategory(customCategoryEditText.getText().toString().toLowerCase())) {
            customCategoryLayout.setError("Category already exists");
            result = false;
        }


        return result;
    }

    private void showErrorMessagePopup(final String title, String message) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);


        dialogBuilder.setTitle(title);
        dialogBuilder.setMessage(message);
        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (title.equalsIgnoreCase(ResponseParameters.Success)) {
                    finish();
                }
                CreateGroupActivity.this.titleAlert.dismiss();
            }
        });
        this.titleAlert = dialogBuilder.create();
        this.titleAlert.setCancelable(true);
        this.titleAlert.show();
    }


    void clearFocus() {
        etGroupName.clearFocus();
        etGroupSum.clearFocus();
    }


    private void populateSpinner() {
        try {
            spGroupType.setOnItemSelectedListener(this);
            List<String> groupType = new ArrayList<String>();
            groupType.add(getResources().getString(R.string.create_group_type_drpodown_option_private));
            groupType.add(getResources().getString(R.string.create_group_type_drpodown_option_public));
            groupType.add(getResources().getString(R.string.create_group_edittext_group_type_hint));
            ArrayAdapter<String> questionOneAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_dropdown_item, groupType) {
                @Override
                public int getCount() {
                    int count = super.getCount();
                    return count > 0 ? count - 1 : count;
                }
            };
            spGroupType.setAdapter(questionOneAdapter);
            spGroupType.setSelection(2);

            if (getIntent().hasExtra(ResponseParameters.GROUP)) {
                spGroupType.setSelection(Integer.parseInt(group.privacy));
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void disableError() {
        etGroupNameLayout.setErrorEnabled(false);
        etGroupTypeLayout.setErrorEnabled(false);
        etGroupCateLayout.setErrorEnabled(false);
        etGroupSumLayout.setErrorEnabled(false);
    }

    void enableError(TextInputLayout inputLayout, String errorText) {
        inputLayout.setErrorEnabled(true);
        inputLayout.setError(errorText);
    }

    public void getResponse(JSONObject outPut, int i) {
        try {
            //  Toast.makeText(context, outPut+"", Toast.LENGTH_LONG).show();
            copyToClipboard(outPut + "");
            if (progressBar != null)
                progressBar.dismiss();
            Log.e("output", "" + outPut);

            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Success)) {
                    if (i == 0) {
                        group_categories = outPut.getJSONArray(ResponseParameters.GROUP_CATEGORIES);
                        setUpCategoriesSpinner();
                    } else if (i == 1) {
                        if (outPut.has(ResponseParameters.GROUP)) {
                            //send broadcast to add group on group list
                            String string = outPut.getString(ResponseParameters.Success);
                            showPopUp("Success", true, false);

                            Bundle bundle = new Bundle();
                            bundle.putString("UserId", SharedPreferencesMethod.getUserId(this));
                            Utils.logEvent(EventNames.CREATE_GROUP, bundle, this);
                        }
                    }
                } else if (outPut.has(ResponseParameters.Errors)) {
                    Snackbar.make(llSave, Utils.getErrorMessage(outPut), 0).show();
                } else {
                    Snackbar.make(llSave, CreateGroupActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                }
            } else {
                Snackbar.make(llSave, CreateGroupActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
            }

        } catch (Exception e) {

        }
    }

    private void copyToClipboard(String text) {
       /* ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", text);
        clipboard.setPrimaryClip(clip);*/
    }

    private void setUpCategoriesSpinner() {
        try {
            spGroupCategory.setOnItemSelectedListener(this);
            List<String> groupType = new ArrayList<String>();
            for (int i = 0; i < group_categories.length(); i++) {
                groupType.add(group_categories.getJSONObject(i).getString(ResponseParameters.CATEGORY));
                if (i == group_categories.length() - 1) {
                    groupType.add("Add Custom Category");
                }
            }
            groupType.add(getResources().getString(R.string.create_group_edittext_group_category_hint));
            ArrayAdapter<String> questionOneAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_dropdown_item, groupType) {
                @Override
                public int getCount() {
                    int count = super.getCount();
                    return count > 0 ? count - 1 : count;
                }
            };
            spGroupCategory.setAdapter(questionOneAdapter);
            spGroupCategory.setSelection(group_categories.length() + 1);
            if (getIntent().hasExtra(ResponseParameters.GROUP)) {
                for (int i = 0; i < group_categories.length(); i++) {
                    if (group.category.trim().equalsIgnoreCase(group_categories.getJSONObject(i).getString(ResponseParameters.Id).trim())) {
                        spGroupCategory.setSelection(i);
                        break;
                    }
                }
            }

            spGroupCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i == group_categories.length()) {
//                        Toast.makeText(context, "Custom category", Toast.LENGTH_SHORT).show();
                        customCategoryEditText.setVisibility(View.VISIBLE);
                        customCategoryLayout.setVisibility(View.VISIBLE);
                        customCategoryEditText.addTextChangedListener(CreateGroupActivity.this);
                        isCustomeCatgorySet = true;
                    } else {
                        customCategoryLayout.setVisibility(View.GONE);
                        customCategoryEditText.setVisibility(View.GONE);
                        isCustomeCatgorySet = false;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            switch (parent.getId()) {
                case R.id.spGroupCategory:
                    if (position > group_categories.length()) {
                        Log.e("TAG", group_categories.getJSONObject(position).getString(ResponseParameters.Id));
                        Log.e("TAG", group_categories.getJSONObject(position).getString(ResponseParameters.CATEGORY));
                    }
                    break;
                case R.id.spGroupType:
                    Log.e("TAG", "" + spGroupType.getSelectedItem().toString());
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onBackPressed() {

        showPopUp(getResources().getString(R.string.create_group_back_message), true, true);

        //super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GroupInviteActivity.clearSelection();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        isAlreadyCategory(charSequence);
    }

    private boolean isAlreadyCategory(CharSequence charSequence) {
        for (int j = 0; j < group_categories.length(); j++) {
            try {
                String inputCatgeory = charSequence.toString().toLowerCase().trim();
                String category = group_categories.getJSONObject(j).getString(ResponseParameters.CATEGORY).toLowerCase().trim();
                if (inputCatgeory.equalsIgnoreCase(category)) {
//                    customCategoryEditText.setError("Category already exists");
                    customCategoryLayout.setError("Category already exists");
                    return true;
                }else{
                    customCategoryLayout.setError(null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        customCategoryEditText.setError(null);
        return false;
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
