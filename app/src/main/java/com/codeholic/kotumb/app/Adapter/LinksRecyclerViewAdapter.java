package com.codeholic.kotumb.app.Adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.os.Build;
import androidx.browser.customtabs.CustomTabsClient;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.browser.customtabs.CustomTabsServiceConnection;
import androidx.browser.customtabs.CustomTabsSession;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Link;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.CustomTabsHelper;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.fragments.BaseFragment;
import com.codeholic.kotumb.app.fragments.FavrtLinksFragment;

import org.json.JSONObject;

import java.util.List;


public class LinksRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Link> links;
    Activity activity;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;

    CustomTabsServiceConnection mCustomTabsServiceConnection;
    CustomTabsClient mClient;
    CustomTabsSession mCustomTabsSession;
    CustomTabsIntent.Builder builder;
    boolean warmedUp = false;
    boolean deleteLayout = false;
    private BaseFragment fragment;

    public LinksRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<Link> links) {
        this.links = links;
        this.activity = activity;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }



    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return links.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return links.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.link_list_item, parent, false);
            UserViewHolder UserViewHolder = new UserViewHolder(view);
            return UserViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;

    }

    public void setUpDeleteLayout(boolean deleteLayout) {
        this.deleteLayout = deleteLayout;
    }

    public void setFragment(BaseFragment fragment) {
        this.fragment = fragment;
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {
        Link link = links.get(position);

        if (viewholder instanceof UserViewHolder) {
            UserViewHolder holder = (UserViewHolder) viewholder;
            holder.tvLinkName.setText("" + link.getLinkName());
            holder.tvLink.setText("" + link.getLink());
            setOnClickListener(holder, link, position);
            if (deleteLayout) {
                holder.tvOptionMenu.setVisibility(View.VISIBLE);
            } else {
                holder.tvOptionMenu.setVisibility(View.GONE);
            }

        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    public void setOnClickListener(final UserViewHolder holder, final Link link, final int position) {

        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Uri uri = Uri.parse(""+link.getLink());
                CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
                intentBuilder.setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary));
                intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
                intentBuilder.setStartAnimations(activity, android.R.anim.fade_in, android.R.anim.fade_out);
                intentBuilder.setExitAnimations(activity, android.R.anim.fade_in, android.R.anim.fade_out);
                CustomTabsIntent customTabsIntent = intentBuilder.build();
                customTabsIntent.launchUrl(activity, uri);*/
                if (link.getLink().startsWith("http"))
                    prepareCustomTab(link.getLink());
                else
                    prepareCustomTab("http://" + link.getLink());
            }
        });
        if (deleteLayout) {
            holder.tvOptionMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(activity, holder.tvOptionMenu);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.options_link_menu);
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.itemEdit:
                                    holder.cvContainer.performClick();
                                    break;
                                case R.id.itemDelete:
                                    if (!Utility.isConnectingToInternet(activity)) {
                                        final Snackbar snackbar = Snackbar.make(holder.tvOptionMenu, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                                        snackbar.show();
                                        break;
                                    }
                                    links.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, links.size());
                                    if (links.size() <= 0 && fragment != null){
                                        ((FavrtLinksFragment.ListLinkFragment) fragment).showNotFound();
                                    }
                                    sendRequest(activity, API.DELETE_FAV+""+link.getLinkId());
                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();
                }
            });
        }
    }

    public void sendRequest(final Activity context, String url) {
        try {
            AQuery aq = new AQuery(context);
            //Log.d("Url ", url);
            aq.ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {
                        if (json != null) {
                            Log.e(context.getClass().getName(), json.toString());
                            json.put("RP_MESSAGE", "ALL_OKAY");
                        } else {
                            JSONObject output = new JSONObject();
                            if (status.getCode() == AjaxStatus.NETWORK_ERROR) {
                                output.put("RP_MESSAGE", "NO NETWORK");
                            } else {
                                output.put("RP_MESSAGE", "ERROR");
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.method(AQuery.METHOD_GET).header("Auth-Key","KOTUMB__AuTh_keY"));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareCustomTab(final String url) {

        builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        // builder.setSecondaryToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
        builder.setShowTitle(true);
        builder.addDefaultShareMenuItem();

        Bitmap icon = getBitmapFromDrawable(activity, R.drawable.ic_arrow_back);
        PendingIntent pendingIntent = createPendingShareIntent();

        final Bitmap backButton = getBitmapFromDrawable(activity, R.drawable.ic_arrow_back);
        builder.setCloseButtonIcon(backButton);

        builder.setStartAnimations(activity, R.anim.slide_in_right, R.anim.slide_out_left);
        builder.setExitAnimations(activity, R.anim.slide_in_left, R.anim.slide_out_right);
        //builder.re
        //builder.setActionButton(icon, activity.getString(R.string.app_name), pendingIntent, true);
        // builder.addMenuItem("Share this page", createPendingShareIntent());
        mCustomTabsServiceConnection = new CustomTabsServiceConnection() {
            @Override
            public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
                //Pre-warming
                mClient = customTabsClient;
                mClient.warmup(0L);
                mCustomTabsSession = mClient.newSession(null);
                mCustomTabsSession.mayLaunchUrl(Uri.parse(url), null, null);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mClient = null;
            }
        };
        warmedUp = CustomTabsClient.bindCustomTabsService(activity, "com.android.chrome", mCustomTabsServiceConnection);
        if (url.isEmpty())
            return;




        CustomTabsIntent intentCustomTabs = builder.build();
        intentCustomTabs.intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        String packageName = CustomTabsHelper.getPackageNameToUse(activity);
        if ( packageName != null ) {
            intentCustomTabs.intent.setPackage(packageName);
        }

        intentCustomTabs.launchUrl(activity, Uri.parse(url));
    }


    private Bitmap getBitmapFromDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof VectorDrawable) {
            return getBitmapFromVectorDrawable((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("Unable to convert to bitmap");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private Bitmap getBitmapFromVectorDrawable(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    private PendingIntent createPendingShareIntent() {
        Intent actionIntent = new Intent(Intent.ACTION_SEND);
        actionIntent.setType("text/plain");
        actionIntent.putExtra(Intent.EXTRA_TEXT, "Share text");
        return PendingIntent.getActivity(
                activity, 0, actionIntent, 0);
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvLinkName, tvLink;
        CardView cvContainer;
        LinearLayout tvOptionMenu;

        UserViewHolder(View view) {
            super(view);
            tvLinkName = (TextView) view.findViewById(R.id.tvLinkName);
            tvLink = (TextView) view.findViewById(R.id.tvLink);
            cvContainer = (CardView) view.findViewById(R.id.cvContainer);
            tvOptionMenu = (LinearLayout) view.findViewById(R.id.tvOptionMenu);
        }
    }
}