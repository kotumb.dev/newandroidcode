package com.codeholic.kotumb.app.Utility;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.File;
import java.io.IOException;

/**
 * Created by rahul on 12/22/2015.
 */
public class RotateImageCode {
    public static Bitmap rotateImage(final Bitmap bitmap,
                                     final File fileWithExifInfo) {
        if (bitmap == null) {
            return null;
        }
        Bitmap rotatedBitmap = bitmap;
        int orientation = 0;
        try {
            orientation = getImageOrientation(fileWithExifInfo
                    .getAbsolutePath());
            if (orientation != 0) {
                Matrix matrix = new Matrix();
                matrix.postRotate(orientation, (float) bitmap.getWidth() / 2,
                        (float) bitmap.getHeight() / 2);
                rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                        bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                bitmap.recycle();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotatedBitmap;
    }
    public static int getImageOrientation(final String file) throws IOException {

        int val = 0;
        Matrix matrix = new Matrix();
        try {
            ExifInterface exif = new ExifInterface(file);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            if (orientation == 6) {
                val = 90;
                matrix.postRotate(90);
            } else if (orientation == 3) {
                val = 180;
                matrix.postRotate(180);
            } else if (orientation == 8) {
                val = 270;
                matrix.postRotate(270);
            }
        } catch(IOException ex) {
            ex.printStackTrace();
        }

        return val;

//        ExifInterface exif = new ExifInterface(file);
//        int orientation = exif
//                .getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//        switch (orientation) {
//            case ExifInterface.ORIENTATION_NORMAL:
//                return 0;
//            case ExifInterface.ORIENTATION_ROTATE_90:
//                return 90;
//            case ExifInterface.ORIENTATION_ROTATE_180:
//                return 180;
//            case ExifInterface.ORIENTATION_ROTATE_270:
//                return 270;
//            default:
//                return 0;
//        }
    }
}