package com.codeholic.kotumb.app.Services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.Utility;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class OpenCustomTabService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_OPEN_SUPPORT_CHAT = "com.codeholic.kotumb.app.Services.action.OPEN_SUPPORT_CHAT";



    public OpenCustomTabService() {
        super("OpenCustomTabService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionOpenSupportChat(Context context, String description) {
        Intent intent = new Intent(context, OpenCustomTabService.class);
        intent.setAction(ACTION_OPEN_SUPPORT_CHAT);
        intent.putExtra(ResponseParameters.DESCRIPTION, description);
        context.startService(intent);
    }



    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_OPEN_SUPPORT_CHAT.equals(action)) {
                handleActionOpenSupportChat(intent.getStringExtra(ResponseParameters.DESCRIPTION));
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionOpenSupportChat(String description) {
        // TODO: Handle action open support chat
        Utility.prepareCustomTab(this, API.contactUsOpenChat + description);
    }
}
