package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class UpdatePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etUserName, etPassword, etCPassword, etOldPassword;
    private TextInputLayout userNameLayout, passwordLayout, cPasswordLayout, etOldPasswordLayout;
    private TextView tvRegister, tvUserNames, tvChangePass;
    private LinearLayout llSuggestion;
    private ProgressBar pbLoading, pbUserName;
    private CardView cvRegister;
    private Activity context;
    private ImageView ivWrong, ivCorrect;
    private boolean userNameCorrect = false;
    private boolean isUserNameLoading = false;
    private String languageToLoad = "";
    private String passOld = "";
    Configuration config = new Configuration();
    Locale locale;
    User user = new User();

    //oncreate method for setting content view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languageToLoad = SharedPreferencesMethod.getDefaultLanguage(this);
        changeLocale(languageToLoad);
    }

    //change locale
    private void changeLocale(String languageToLoad) {
        locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setView();
    }

    //setting content view
    private void setView() {
        setContentView(R.layout.activity_change_info);
        initView();
        setClickListener();
        setTextWatcher(etPassword, passwordLayout);
        setTextWatcher(etCPassword, cPasswordLayout);
        setTextWatcher(etOldPassword, etOldPasswordLayout);
    }

    //initializing views
    private void initView() {
        context = this;
        user = SharedPreferencesMethod.getUserInfo(context);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etUserName = (EditText) findViewById(R.id.etUserName);
        etUserName.setVisibility(GONE);
        etCPassword = (EditText) findViewById(R.id.etCPassword);
        etOldPassword = (EditText) findViewById(R.id.etOldPassword);
        etOldPasswordLayout = (TextInputLayout) findViewById(R.id.etOldPasswordLayout);
        userNameLayout = (TextInputLayout) findViewById(R.id.etUserNameLayout);
        userNameLayout.setVisibility(GONE);
        passwordLayout = (TextInputLayout) findViewById(R.id.etPasswordLayout);
        cPasswordLayout = (TextInputLayout) findViewById(R.id.etCPasswordLayout);
        tvRegister = (TextView) findViewById(R.id.tvRegister);
        cvRegister = (CardView) findViewById(R.id.cv_register);
        pbLoading = (ProgressBar) findViewById(R.id.pbLoading);
        ivWrong = (ImageView) findViewById(R.id.ivWrong);
        ivCorrect = (ImageView) findViewById(R.id.ivCorrect);
        pbUserName = (ProgressBar) findViewById(R.id.pbUserName);
        ivCorrect.setVisibility(VISIBLE);
        tvUserNames = (TextView) findViewById(R.id.tvUserNames);
        tvChangePass= (TextView) findViewById(R.id.tvChangePass);
        tvChangePass.setVisibility(VISIBLE);
        llSuggestion = (LinearLayout) findViewById(R.id.llSuggestion);
    }

    //set click listeners on views
    private void setClickListener() {
        cvRegister.setOnClickListener(this);
    }

    //textwatcher method for all edittext
    private void setTextWatcher(final EditText editText, final TextInputLayout textInputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (editText == etPassword) {
                    passOld = etPassword.getText().toString().trim();
                    Log.e("beforeTextChanged", "" + etPassword.getText().toString().trim());
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    textInputLayout.setErrorEnabled(false);
                }

                if (editText == etPassword) {
                    passwordLayout.setErrorEnabled(false);
                    if (s.length() > 0) {
                        if (!etCPassword.getText().toString().trim().isEmpty() && !passOld.equals(etPassword.getText().toString().trim())) {
                            etCPassword.setText("");
                            enableError(cPasswordLayout, getResources().getString(R.string.password_empty_input));
                        }
                        /*Pattern pass = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z!@#$%^&*\\d]{1,12}");
                        boolean userPassMatcher = pass.matcher(etPassword.getText().toString().trim()).find();
                        if (!userPassMatcher) {
                            enableError(passwordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
                        } else */if (etPassword.getText().toString().trim().length() < 6) {
                            enableError(passwordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
                        }
                    }
                }

                if (editText == etCPassword) {
                    cPasswordLayout.setErrorEnabled(false);
                    if (etCPassword.getText().toString().trim().isEmpty()) {
                        enableError(cPasswordLayout, getResources().getString(R.string.password_empty_input));
                    } else if (!etPassword.getText().toString().trim().equals(etCPassword.getText().toString().trim())) {
                        enableError(cPasswordLayout, getResources().getString(R.string.confirm_password_input_match));
                    }
                }

                if (editText == etOldPassword) {
                    etOldPasswordLayout.setErrorEnabled(false);
                    /*Pattern pass = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z!@#$%^&*\\d]{1,12}");
                    boolean userPassMatcher = pass.matcher(etOldPassword.getText().toString().trim()).find();
                    if (!userPassMatcher) {
                        enableError(etOldPasswordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
                    } else */if (etOldPassword.getText().toString().trim().length() < 6) {
                        enableError(etOldPasswordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //disabling error of input layout
    void disableError() {
        passwordLayout.setErrorEnabled(false);
        etOldPasswordLayout.setErrorEnabled(false);
        cPasswordLayout.setErrorEnabled(false);
    }

    //enabling error of input layout
    void enableError(TextInputLayout inputLayout, String error) {
        inputLayout.setErrorEnabled(true);
        inputLayout.setError(error);
    }

    //validation for fields
    private boolean validation() {
        disableError();
        boolean result = true;
        //Pattern p = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z!@#$%^&*\\d]{1,12}");
        //Pattern pass = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z!@#$%^&*\\d]{1,12}");
        //Pattern oldpass = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z!@#$%^&*\\d]{8,12}");


        //boolean userNameMatcher = p.matcher(etOldPassword.getText().toString().trim()).find();
        //boolean userPassMatcher = pass.matcher(etPassword.getText().toString().trim()).find();
        // boolean oldPassMatcher = oldpass.matcher(etOldPassword.getText().toString().trim()).find();


        if (etOldPassword.getText().toString().trim().isEmpty()) {
            enableError(etOldPasswordLayout, getResources().getString(R.string.old_password_empty_input));
            result = false;
        } /*else if (!userNameMatcher) {
            enableError(etOldPasswordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
            result = false;
        }*/
        if (etPassword.getText().toString().trim().isEmpty()) {
            enableError(passwordLayout, getResources().getString(R.string.password_empty_input));
            result = false;
        } /*else if (!userPassMatcher) {
            enableError(passwordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
            result = false;
        } */else if (etPassword.getText().toString().trim().length() < 6) {
            enableError(passwordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
            result = false;
        }
        if (etCPassword.getText().toString().trim().isEmpty()) {
            enableError(cPasswordLayout, getResources().getString(R.string.password_empty_input));
            result = false;
        } else if (!etPassword.getText().toString().trim().equals(etCPassword.getText().toString().trim())) {
            enableError(cPasswordLayout, getResources().getString(R.string.confirm_password_input_match));
            result = false;
        }
        return result;
    }

    //get response from service
    public void getResponse(JSONObject outPut, int type) {
        try {
            tvRegister.setVisibility(VISIBLE);
            pbLoading.setVisibility(GONE);
            setVisible();
            if (type == 0) { // change login info response
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(cvRegister, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        if (outPut.has(ResponseParameters.Success)) {
                            if (outPut.getString(ResponseParameters.Success).equalsIgnoreCase("success")) {
                                //SharedPreferencesMethod.setUserDetails(this, user);
                                Toast.makeText(context, outPut.getString(ResponseParameters.MSG), Toast.LENGTH_SHORT).show();
                                this.finish();
                            } else {
                                final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                                snackbar.show();
                            }
                        } else {
                            final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                            snackbar.show();
                        }
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { //error
                    final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { //no network
                    final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        } catch (Exception e) {
            final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
            snackbar.show();
            e.printStackTrace();
        }
    }

    //enable all edit text
    void setVisible() {
        etPassword.setEnabled(true);
        etCPassword.setEnabled(true);
        etOldPassword.setEnabled(true);
        cvRegister.setClickable(true);
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cv_register: // update click
                if (validation()) { // check validation
                    try {
                        if (!Utility.isConnectingToInternet(context)) { // check net connection
                            final Snackbar snackbar = Snackbar.make(v, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        HashMap<String, Object> input = new HashMap<>();
                        input.put(RequestParameters.USERID, "" + user.getUserId());
                        input.put(RequestParameters.OLD_PASSWORD, "" + etOldPassword.getText().toString().trim());
                        input.put(RequestParameters.PASSWORD, "" + etPassword.getText().toString().trim());
                        input.put(RequestParameters.CONFIRM_PASSWORD, "" + etPassword.getText().toString().trim());
                        tvRegister.setVisibility(GONE);
                        pbLoading.setVisibility(VISIBLE);
                        cvRegister.setClickable(false);
                        etPassword.setEnabled(false);
                        etOldPassword.setEnabled(false);
                        Log.e("INPUT", API.CHANGE_PASSWORD + " " + input.toString());
                        API.sendRequestToServerPOST_PARAM(context, API.CHANGE_PASSWORD, input); // service call for update info
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }
}
