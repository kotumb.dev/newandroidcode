package com.codeholic.kotumb.app.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.CreatePostActivity;
import com.codeholic.kotumb.app.Activity.GroupPostActivity;
import com.codeholic.kotumb.app.Activity.ImagesShowActivity;
import com.codeholic.kotumb.app.Activity.PostDetailsActivity;
import com.codeholic.kotumb.app.Activity.PostLikesActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import org.jitsi.meet.sdk.JitsiMeetActivity;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;
import static android.view.View.VISIBLE;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class PostListRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Post> posts;
    Activity activity;
    Context context;
    AlertDialog alert;
    AlertDialog confirmAlert;
    private HashMap<Integer, Integer> mViewPageStates = new HashMap<>();
    private HashMap<String, String> countTexts = new HashMap<String, String>();
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;
    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;
    private  int commentsPagination=0;
    int type = 0;
    public String role;
    private String group_id = "0";

    public static final String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";


    public void setType(int type) {
        this.type = type;
    }

    public void setUserImageBaseUrl(String base_url) {
        this.userImageBaseUrl = base_url;
    }

    String userImageBaseUrl = "";

    public void setGroupImageBaseUrl(String base_url) {
        this.groupImageBaseUrl = base_url;
    }

    String groupImageBaseUrl = "";

    public PostListRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<Post> posts, String group_id) {
        this.posts = posts;
        this.activity = activity;
        context = activity.getBaseContext();
        this.group_id = group_id;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }





    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }


    @Override
    public int getItemViewType(int position) {
        return posts.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return posts.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.group_post_item, parent, false);
            GroupViewHolder GroupViewHolder = new GroupViewHolder(view);
            return GroupViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, final int position) {
        final Post post = this.posts.get(position);
        if (viewholder instanceof GroupViewHolder) {
            ArrayList<String> imageNameList = new ArrayList<>();
            final GroupViewHolder holder = (GroupViewHolder) viewholder;
            String source = "" + post.firstName.trim() + " " + post.middleName + " " + post.lastName.trim() + " ";
            final SpannableString spannableName = new SpannableString(source.replaceAll("  ", " "));
            spannableName.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, spannableName.length(), SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.mainCommmentContainer.setVisibility(View.GONE);
            System.out.println("Group Post Media    "+post.image);
//            holder.ivUserImage.setOnClickListener(Utility.getListenerUserClick(activity, post.userId));
            if (post.sharedPostUserName.trim().isEmpty()) {
                holder.tvUserName.setText(TextUtils.concat(spannableName));
//                holder.tvUserName.setOnClickListener(Utility.getListenerUserClick(activity, post.userId));
            } else {
                SpannableString shared = new SpannableString(activity.getResources().getString(R.string.shared_btn_text));
                shared.setSpan(new RelativeSizeSpan(.7f),
                        0, shared.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                SpannableString sharedUserName = new SpannableString(" " + post.sharedPostUserName.trim());
                sharedUserName.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, sharedUserName.length(), SPAN_EXCLUSIVE_EXCLUSIVE);

                SpannableString postText = new SpannableString(" " + activity.getResources().getString(R.string.post_btn_text));
                postText.setSpan(new RelativeSizeSpan(.7f),
                        0, postText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tvUserName.setText(TextUtils.concat(spannableName, shared, sharedUserName, postText));
                //holder.tvUserName.setOnTouchListener(Utility.getTouchListenerForUser(post, spannableName, activity, holder.tvUserName));
            }


            holder.tvUserName.setTextColor(activity.getResources().getColor(android.R.color.secondary_text_light));


            if (!post.content.trim().isEmpty()) {
                holder.tvPostContent.setVisibility(View.VISIBLE);
                holder.tvPostContent.setText(post.content);
                final ArrayList<String> finalImageNameList = imageNameList;
                holder.tvPostContent.post(new Runnable() {
                    @Override
                    public void run() {
                        int lineCount = holder.tvPostContent.getLineCount();
                        int length = post.content.length();
                        if (lineCount > 5 && length > 0) {
                            holder.tvreadmore.setVisibility(View.VISIBLE);
                        } else {
                            holder.tvreadmore.setVisibility(View.GONE);
                        }
                    }
                });
            } else {
                holder.tvPostContent.setVisibility(View.GONE);
            }


            Log.e("tomboy", "onBindViewHolder: "+position );

            holder.tvPostContent.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        int mOffset = holder.tvPostContent.getOffsetForPosition(motionEvent.getX(), motionEvent.getY());
                        //  mTxtOffset.setText("" + mOffset);

                        String tappedwordwithspaceandnewline = Utility.findWordForRightHanded(holder.tvPostContent.getText().toString(), mOffset);
                        String tappedword = tappedwordwithspaceandnewline
                                .replaceAll(System.getProperty("line.separator"), "")
                                .replaceAll(" ","");


                        //Toast.makeText(context, tappedword , Toast.LENGTH_SHORT).show();

                        if (tappedword.contains("meet.jit.si/")) {

                            holder.tvPostContent.setLinksClickable(false);

                            String[] splitUrl = post.content.split("meet.jit.si/");

                            JitsiMeetConferenceOptions options
                                    = new JitsiMeetConferenceOptions.Builder()
                                    .setRoom(splitUrl[1])
                                    .build();
                            // Launch the new activity with the given options. The launch() method takes care
                            // of creating the required Intent and passing the options.
                            JitsiMeetActivity.launch(activity, options);

                        } else if(tappedword.toLowerCase().contains("https")||tappedword.toLowerCase().contains("http")){

                            holder.tvPostContent.setLinksClickable(true);

                            if(tappedword.toLowerCase().contains("https")) {

                                navigateToActivity( tappedword );

                            }else if(tappedword.toLowerCase().contains("http")){

                                navigateToActivity( tappedword);

                            }else {
                                Log.e("postdetailsacxxx", "onTouch3: some url"+tappedword );
                                Intent intent = new Intent(context, PostDetailsActivity.class);
                                System.out.println("Send Data    " + post + "    " + ((PostDetailsActivity)context).group + "    " + groupImageBaseUrl + "   " + "  " + type);
                                intent.putExtra(ResponseParameters.POST, post);
                                intent.putExtra(ResponseParameters.GROUP, ((PostDetailsActivity)context).group);
                                intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, groupImageBaseUrl);
                                intent.putExtra(ResponseParameters.TYPE, type);
                                context.startActivity(intent);
                            }

                        }

                    }

                    holder.tvPostContent.setLinksClickable(false);

                    return false;
                }
            });



            /*holder.tvPostContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Log.e("Postlistrvadapater", "onClick: clicked" );

                    if (post.content.contains("meet.jit.si/")){

                        String[] splitUrl = post.content.split("meet.jit.si/");

                        JitsiMeetConferenceOptions options
                                = new JitsiMeetConferenceOptions.Builder()
                                .setRoom(splitUrl[1])
                                .build();
                        // Launch the new activity with the given options. The launch() method takes care
                        // of creating the required Intent and passing the options.
                        JitsiMeetActivity.launch(activity, options);

                    }
                    else {

                        String REGEX = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
                        String http = "[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
                        Pattern p = Pattern.compile(REGEX, Pattern.CASE_INSENSITIVE);
                        Matcher m = p.matcher(post.content);
                        Pattern pattern = Pattern.compile(http);
                        Matcher matcher = pattern.matcher(post.content);

                        if (matcher.find()){
                            navigateToActivity("https://",post.content);


                        }else if (m.find()){
                            navigateToActivity("",post.content);

                        }
                        else {
                            Intent intent = new Intent(activity, PostDetailsActivity.class);
                            System.out.println("Send Data    " + post + "    " + ((GroupPostActivity) activity).group + "    " + groupImageBaseUrl + "   " + "  " + type);
                            intent.putExtra(ResponseParameters.POST, post);
                            intent.putExtra(ResponseParameters.GROUP, ((GroupPostActivity) activity).group);
                            intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, groupImageBaseUrl);
                            intent.putExtra(ResponseParameters.TYPE, type);
                            activity.startActivity(intent);
                        }
                    }
                }
            });*/

            String countText = "";

            if (post.total_likes == 0) {
                holder.tvLike.setText(activity.getResources().getString(R.string.like_btn_text));
            } else if (post.total_likes == 1) {
                //holder.tvLike.setText(post.total_likes + " " + activity.getResources().getString(R.string.like_btn_text));
                countText = "1 Like";
            } else {
                //holder.tvLike.setText(post.total_likes + " " + activity.getResources().getString(R.string.likes_btn_text));
                countText = post.total_likes + " Likes";
            }

            if (post.total_comments == 0) {
                holder.tvComments.setText(activity.getResources().getString(R.string.comment_btn_text));
            } else if (post.total_comments == 1) {
//                holder.tvComments.setText(post.total_comments + activity.getResources().getString(R.string.comment_btn_text));
                if (!countText.isEmpty()) {
                    countText = countText + "     ";
                }
                countText = countText + "1 Comment";
            } else {
//                holder.tvComments.setText(post.total_comments + activity.getResources().getString(R.string.comments_btn_text));
                if (!countText.isEmpty()) {
                    countText = countText + "     ";
                }
                countText = countText + post.total_comments + " Comments";
            }

            if (post.total_likes > 0) {
                String like = post.total_likes > 1 ? " Likes" : " Like";
                holder.tvCountLike.setText(post.total_likes + like);
                holder.tvCountLike.setVisibility(View.VISIBLE);
                countTexts.put(post.post_id, countText);
                holder.tvCountLike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent PostLikesIntent = new Intent(context, PostLikesActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(ResponseParameters.POST, posts.get(position));
                        bundle.putString(ResponseParameters.IMAGE_BASE_URL, ((GroupPostActivity) activity).USER_IMAGE_BASE_URL);
                        PostLikesIntent.putExtra(ResponseParameters.BUNDLE, bundle);
                        activity.startActivity(PostLikesIntent);
                    }
                });
            } else {
                holder.tvCountLike.setVisibility(View.GONE);
            }

            if (post.total_comments > 0) {
                String like = post.total_likes > 1 ? " Comments" : " Comment";
                holder.tvCountComment.setText(post.total_comments + like);
                holder.tvCountComment.setVisibility(View.VISIBLE);
                countTexts.put(post.post_id, countText);
                holder.tvCountComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((GroupPostActivity) activity).showCommentView(post, position);
                    }
                });
            } else {
                holder.tvCountComment.setVisibility(View.GONE);
            }

            holder.tvTime.setText(post.duration.trim());
            new AQuery(activity).id(holder.ivUserImage).image(userImageBaseUrl + post.avatar.trim(), true, true, 300, R.drawable.ic_user);
            Log.e("TAG", "image " + post.image);
            if (!post.image.trim().isEmpty()) {
                /*holder.slidesPager.setVisibility(View.VISIBLE);
                holder.pageIndicatorView.setVisibility(View.VISIBLE);
                holder.llPointer.setVisibility(View.VISIBLE);
                holder.pageIndicatorView.setViewPager(holder.slidesPager);*/

                String[] splitArray = post.image.split("\\|");

                imageNameList = new ArrayList<String>(Arrays.asList(splitArray));

                //configurePagerHolder(holder, position);
            }
                /*holder.llPointer.setVisibility(View.GONE);
                holder.slidesPager.setVisibility(View.GONE);
                holder.pageIndicatorView.setVisibility(View.GONE);*/

            if (post.self_like == 1) {
                Drawable img = activity.getResources().getDrawable(R.drawable.ic_thumb_up_liked);
                holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            } else {
                Drawable img = activity.getResources().getDrawable(R.drawable.ic_thumb_up);
                holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            }


            holder.ivImageOne.setVisibility(View.GONE);
            holder.ivImageTwo.setVisibility(View.GONE);
            holder.ivImageThree.setVisibility(View.GONE);
            holder.ivImageFour.setVisibility(View.GONE);
            holder.ivImageFive.setVisibility(View.GONE);
            holder.llSquareContainer.setVisibility(View.GONE);
            holder.ivImageSingle.setVisibility(View.GONE);
            if (!post.image.isEmpty()){
                if (Utils.getExtension(post.image).equalsIgnoreCase("pdf")){
                    holder.pdf_image.setVisibility(VISIBLE);
                }else{
                    holder.pdf_image.setVisibility(View.GONE);
                    if (imageNameList.size() == 0) {
                        holder.llImageContainer.setVisibility(View.GONE);
                    } else {
                        holder.llImageContainer.setVisibility(View.VISIBLE);
                        if (imageNameList.size() == 1) {
                            new AQuery(activity).id(holder.ivImageSingle).image(groupImageBaseUrl + imageNameList.get(0), true, true, 300, R.drawable.no_image);
                            holder.ivImageSingle.setVisibility(View.VISIBLE);
                            System.out.println("Imagess    "+imageNameList.get(0));
                        } else if (imageNameList.size() ==3) {
                            new AQuery(activity).id(holder.ivImageThree).image(groupImageBaseUrl + imageNameList.get(0), true, true, 300, R.drawable.no_image);
                            holder.ivImageThree.setVisibility(View.VISIBLE);
                            new AQuery(activity).id(holder.ivImageFour).image(groupImageBaseUrl + imageNameList.get(1), true, true, 300, R.drawable.no_image);
                            holder.ivImageFour.setVisibility(View.VISIBLE);
                            new AQuery(activity).id(holder.ivImageFive).image(groupImageBaseUrl + imageNameList.get(2), true, true, 300, R.drawable.no_image);
                            holder.ivImageFive.setVisibility(View.VISIBLE);
                        } else {
                            for (int i = 0; i < imageNameList.size(); i++) {
                                switch (i) {
                                    case 0:
                                        new AQuery(activity).id(holder.ivImageOne).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.no_image);
                                        holder.ivImageOne.setVisibility(View.VISIBLE);
                                        break;
                                    case 1:
                                        new AQuery(activity).id(holder.ivImageTwo).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.no_image);
                                        holder.ivImageTwo.setVisibility(View.VISIBLE);
                                        break;
                                    case 2:
                                        new AQuery(activity).id(holder.ivImageThree).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.no_image);
                                        holder.ivImageThree.setVisibility(View.VISIBLE);
                                        break;
                                    case 3:
                                        new AQuery(activity).id(holder.ivImageFour).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.no_image);
                                        holder.ivImageFour.setVisibility(View.VISIBLE);
                                        break;
                                    case 4:
                                        new AQuery(activity).id(holder.ivImageFive).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.no_image);
                                        holder.ivImageFive.setVisibility(View.VISIBLE);
                                        break;
                                }
                            }
                        }
                        if (imageNameList.size() > 5) {
                            holder.llSquareContainer.setVisibility(View.VISIBLE);
                            holder.tvPhotoNumber.setText((imageNameList.size() - 5) + "+");
                        }
                    }
                }
            }
            if (type == 1) {
                holder.llBottomControl.setVisibility(View.GONE);
                holder.llEditPost.setVisibility(View.GONE);
            } else {
                holder.llBottomControl.setVisibility(View.VISIBLE);
                if (post.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(activity))
                        || ((GroupPostActivity) activity).group.role.equalsIgnoreCase("0")
                        || post.role.trim().equalsIgnoreCase("1")) {
                    holder.llEditPost.setVisibility(View.VISIBLE);
                } else {
                    holder.llEditPost.setVisibility(View.GONE);
                }
            }

            setOnClickListener(holder, post, position, imageNameList);
            holder.tvUserName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  API.openOtherUserProfile(post.userId,context);
                }
            });

        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
       // serViceCall();

//        System.out.println("Post File    "+post.image);
    }

    private void navigateToActivity(String url){
        Log.e("hover", "navigateToActivityx: requested url: "+url );
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(browserIntent);
        }catch (ActivityNotFoundException e){
            Log.e("hover", "navigateToActivityx: please check the link.. its invalid.. there shouldnt be any space or newline or capital letter in https etc " + url );
            Toast.makeText(context,"Invalid link", Toast.LENGTH_SHORT).show();
        }
    }

    //This Method is Created By Arpit Kanda
    //This Method is For Request The Group Comments From Service Of All Users
    private void groupCommentRequest(final LinearLayout container, final ProgressBar progressBar,final Post post,final TextView name,final TextView comment,final int num,final ImageView imageView){
        final String group_comment_url=API.GROUP_POST_COMMENTS + post.post_id + "/" + (0) + "/" + SharedPreferencesMethod.getUserId(context);
        StringRequest postRequest = new StringRequest(Request.Method.GET, group_comment_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       try{
                           System.out.println("Three Comment Response  "+response);
                           progressBar.setVisibility(View.GONE);
                           container.setVisibility(VISIBLE);
                           JSONObject jsonObject = new JSONObject(response);
                           JSONObject results = jsonObject.getJSONArray("group_post_comments").getJSONObject(num);
                           name.setText(results.getString("firstName")+" "+results.getString("lastName"));
                           comment.setText(results.getString("comment"));
                           new AQuery(activity).id(imageView).image(userImageBaseUrl + results.getString("avatar"), true, true, 300, R.drawable.default_profile);
                       }catch (Exception ex){
                           System.out.println("Comment JSON Error   "+ex);
                       }
                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Comment Error  "+error.toString());
            }
        });
        Volley.newRequestQueue(context).add(postRequest);
    }




    //This Method is Created By Arpit Kanda
    //This Method is For Set The Validation Of User Comment This is Only Three Comment For One Time
    private void commentValidation(GroupViewHolder holder,Post post){
        holder.progressBar.setVisibility(VISIBLE);
        try{
            if (post.total_comments==3){
                holder.mainCommmentContainer.setVisibility(View.VISIBLE);
                holder.no_comments_found.setVisibility(View.GONE);
                holder.commentContainer.setVisibility(VISIBLE);
                holder.commentContainer_1.setVisibility(VISIBLE);
                holder.commentContainer_2.setVisibility(VISIBLE);
                groupCommentRequest(holder.mainCommmentContainer_2,holder.progressBar,post,holder.comment_username,holder.user_comments,0,holder.user_comment_img);//Set 1st User Comment
                groupCommentRequest(holder.mainCommmentContainer_2,holder.progressBar,post,holder.comment_username_1,holder.user_comments_1,1,holder.user_comment_img_1);//Set 2nd User Comment
                groupCommentRequest(holder.mainCommmentContainer_2,holder.progressBar,post,holder.comment_username_2,holder.user_comments_2,2,holder.user_comment_img_2);//Set 3rd User Comment
            }else if (post.total_comments==2){
                holder.mainCommmentContainer.setVisibility(View.VISIBLE);
                holder.no_comments_found.setVisibility(View.GONE);
                holder.commentContainer_2.setVisibility(View.GONE);
                holder.viewLine_2.setVisibility(View.GONE);
                holder.commentContainer_1.setVisibility(VISIBLE);
                holder.commentContainer.setVisibility(VISIBLE);
                groupCommentRequest(holder.mainCommmentContainer_2,holder.progressBar,post,holder.comment_username,holder.user_comments,0,holder.user_comment_img);
                groupCommentRequest(holder.mainCommmentContainer_2,holder.progressBar,post,holder.comment_username_1,holder.user_comments_1,1,holder.user_comment_img_1);
            }else if (post.total_comments==1){
                holder.mainCommmentContainer.setVisibility(View.VISIBLE);
                holder.no_comments_found.setVisibility(View.GONE);
                holder.commentContainer_2.setVisibility(View.GONE);
                holder.commentContainer_1.setVisibility(View.GONE);
                holder.viewLine_2.setVisibility(View.GONE);
                holder.viewLine_1.setVisibility(View.GONE);
                holder.commentContainer.setVisibility(VISIBLE);
                groupCommentRequest(holder.mainCommmentContainer_2,holder.progressBar,post,holder.comment_username,holder.user_comments,0,holder.user_comment_img);
            }else if (post.total_comments==0){
                holder.mainCommmentContainer.setVisibility(VISIBLE);
                holder.progressBar.setVisibility(View.GONE);
                holder.no_comments_found.setVisibility(VISIBLE);
                holder.mainCommmentContainer_2.setVisibility(View.GONE);
                holder.more_comments.setVisibility(VISIBLE);
                holder.more_comments.setText(context.getResources().getString(R.string.add_comment_btn_txt));
                holder.viewLine.setVisibility(View.GONE);
                holder.viewLine_1.setVisibility(View.GONE);
                holder.viewLine_2.setVisibility(View.GONE);
            }else{
                holder.mainCommmentContainer.setVisibility(View.VISIBLE);
                holder.no_comments_found.setVisibility(View.GONE);
                groupCommentRequest(holder.mainCommmentContainer_2,holder.progressBar,post,holder.comment_username,holder.user_comments,0,holder.user_comment_img);
                groupCommentRequest(holder.mainCommmentContainer_2,holder.progressBar,post,holder.comment_username_1,holder.user_comments_1,1,holder.user_comment_img_1);
                groupCommentRequest(holder.mainCommmentContainer_2,holder.progressBar,post,holder.comment_username_2,holder.user_comments_2,2,holder.user_comment_img_2);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }






    }









   /* @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {

        if (holder instanceof GroupViewHolder) {

            GroupViewHolder viewHolder = (GroupViewHolder) holder;

            mViewPageStates.put(holder.getAdapterPosition(), viewHolder.slidesPager.getCurrentItem());
            super.onViewRecycled(holder);
        }
    }*/

    private boolean isEllipsized(TextView textview) {
        Layout l = textview.getLayout();
        if (l != null) {
            int lines = l.getLineCount();
            if (lines >= 5) {
                return true;
            }
        }
        return false;
    }


    private void showPopUp(final int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_group_post, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llRemoveUser = dialogView.findViewById(R.id.llRemoveUser);
        final LinearLayout llBlockUser = dialogView.findViewById(R.id.llBlockUser);
        final LinearLayout llDeletePost = dialogView.findViewById(R.id.llDeletePost);
        final LinearLayout llEditPost = dialogView.findViewById(R.id.llEditPost);
        final LinearLayout llReportPost = dialogView.findViewById(R.id.llReportPost);
        final LinearLayout llReportUser = dialogView.findViewById(R.id.llReportUser);
        final LinearLayout llIgnore = dialogView.findViewById(R.id.llIgnore);

        final Post post = posts.get(position);
        if (post.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(activity))) {
            llEditPost.setVisibility(View.VISIBLE);
            llDeletePost.setVisibility(View.VISIBLE);
        }

        if (((GroupPostActivity) activity).group.role.equalsIgnoreCase("0")) {
            llRemoveUser.setVisibility(View.VISIBLE);
            llBlockUser.setVisibility(View.VISIBLE);
            llDeletePost.setVisibility(View.VISIBLE);
            if (post.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(activity))) {
                llRemoveUser.setVisibility(View.GONE);
                llBlockUser.setVisibility(View.GONE);
            }
        }

        if (post.role.trim().equalsIgnoreCase("1") // simple member post
                && !post.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(activity))) {
            if (role.equalsIgnoreCase("0")) {
                llReportPost.setVisibility(View.GONE);
                llReportUser.setVisibility(View.GONE);
            } else {
                llReportPost.setVisibility(View.VISIBLE);
                llReportUser.setVisibility(View.VISIBLE);
            }
        }

        llRemoveUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = API.GROUP_DELETE_MEMBER + post.group_id + "/" + post.userId;
                Log.e("TAG", "" + url);
                System.out.println("Group Delete Member  "+url);

                showPopUp(activity.getResources().getString(R.string.delete_user_dialog_msg), url, "", 0, null);
                /*notifyItemRemoved(position);
                posts.remove(position);
                notifyItemRangeChanged(0, posts.size());*/
                alert.dismiss();
                //hideLayout();
            }
        });
        llBlockUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = API.GROUP_BLOCK_MEMBER + post.group_id + "/" + post.userId;
                System.out.println("Group Block Member   "+url);

                /*notifyItemRemoved(position);
                posts.remove(position);
                notifyItemRangeChanged(0, posts.size());*/

                //hideLayout();
                showPopUp(activity.getResources().getString(R.string.block_user_dialog_msg), url, API.GROUP_BLOCK_MEMBER, 0, null);
                alert.dismiss();
            }
        });
        llDeletePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "";
                url = API.GROUP_POST_DELETE + post.post_id + "/" + SharedPreferencesMethod.getUserId(activity);
                showPopUp(activity.getResources().getString(R.string.delete_post_dialog_msg), url, API.GROUP_POST_DELETE, position, null);
                Log.e("TAG", "" + url);
                System.out.println("Group Post Delete  "+url);
                //API.sendRequestToServerGET(activity, url, API.GROUP_POST_DELETE);


                alert.dismiss();
                //hideLayout();
            }
        });

        llIgnore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "";
                url = API.REPORTED_GROUP_POST_ACTION + post.post_id + "/" + SharedPreferencesMethod.getUserId(activity);
                API.sendRequestToServerGET(activity, url, API.REPORTED_GROUP_POST_ACTION);
                System.out.println("Group Reported Post Action   "+url);
            }
        });

        llEditPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent createPostIntent = new Intent(activity, CreatePostActivity.class);
                createPostIntent.putExtra(ResponseParameters.POST, post);
                createPostIntent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, activity.getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL));
                createPostIntent.putExtra(ResponseParameters.GROUP, ((GroupPostActivity) activity).group);
                activity.startActivity(createPostIntent);
                alert.dismiss();
            }
        });

        llReportPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = API.GROUP_POST_REPORT;
                Log.e("TAG", "" + url);
                System.out.println("Group Post Report  "+url);
                HashMap<String, Object> input = new HashMap<>();
                input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(activity));
                input.put(RequestParameters.GROUPID, "" + ((GroupPostActivity) activity).group.groupId);
                input.put(RequestParameters.POSTID, "" + post.post_id);


                Log.e("INPUT", "" + input);
                System.out.println("Group Post Report  "+input);
                //API.sendRequestToServerGET(activity, url, API.GROUP_POST_DELETE);
                reportPopup(activity.getResources().getString(R.string.post_report_btn_text), url, API.GROUP_POST_REPORT, position, input);
                alert.dismiss();
            }
        });


        llReportUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> input = new HashMap<>();
                input.put(RequestParameters.FROMUSERID, "" + SharedPreferencesMethod.getUserId(activity));
                input.put(RequestParameters.GROUPID, "" + ((GroupPostActivity) activity).group.groupId);
                input.put(RequestParameters.TOUSERID, "" + post.userId);
                //input.put(RequestParameters.REASON, "--");
                Log.e("input", "" + input.toString());
                System.out.println("Group User Report  "+input);
                reportPopup(activity.getResources().getString(R.string.user_report_btn_text), API.GROUP_USER_REPORT, API.GROUP_USER_REPORT, position, input);
                alert.dismiss();
            }
        });


        alert = dialogBuilder.create();
        alert.setCancelable(true);
        alert.show();

    }
    private void showFullPDF(Post post){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + API.group_media+post.image), "text/html");
        activity.startActivity(intent);
    }

    public void setOnClickListener(final GroupViewHolder holder, final Post post, final int position, final ArrayList<String> imageNameList) {
        holder.llImageContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.getExtension(post.image).equalsIgnoreCase("pdf")){
                    showFullPDF(post);
                }else{
                    Intent intent = new Intent(activity, ImagesShowActivity.class);
                    intent.putExtra(ResponseParameters.IMAGE, imageNameList);
                    intent.putExtra(ResponseParameters.POST, post);
                    intent.putExtra(ResponseParameters.IMAGE_URI_LIST, new ArrayList<>());
                    intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, groupImageBaseUrl);
                    intent.putExtra(ResponseParameters.SHOW_CLEAR, false);
                    activity.startActivity(intent);
                }

            }
        });

        holder.llEditPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.llEditPost.startAnimation(Utility.showAnimtion(activity));
                showPopUp(position);
            }
        });

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("TAG", "onClick inside post recyclerview : " + post.content);

                if (post.content.contains("meet.jit.si/")){

                    String[] splitUrl = post.content.split("meet.jit.si/");

                    JitsiMeetConferenceOptions options
                            = new JitsiMeetConferenceOptions.Builder()
                            .setRoom(splitUrl[1])
                            .build();
                    // Launch the new activity with the given options. The launch() method takes care
                    // of creating the required Intent and passing the options.
                    JitsiMeetActivity.launch(activity, options);

                }
                else {

                    if (containsURL(post.content)){
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(post.content));
                        activity.startActivity(browserIntent);
                    }else {
                        Intent intent = new Intent(activity, PostDetailsActivity.class);
                        System.out.println("Send Data    " + post + "    " + ((GroupPostActivity) activity).group + "    " + groupImageBaseUrl + "   " + "  " + type);
                        intent.putExtra(ResponseParameters.POST, post);
                        intent.putExtra(ResponseParameters.GROUP, ((GroupPostActivity) activity).group);
                        intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, groupImageBaseUrl);
                        intent.putExtra(ResponseParameters.TYPE, type);
                        activity.startActivity(intent);
                    }


                }
            }
        };



        //holder.tvreadmore.setOnClickListener(clickListener);

        holder.llLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (post.self_like == 0) {
                    if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                        Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                        return;
                    }

                    String url = API.GROUP_LIKE_POST + posts.get(position).post_id + "/" + posts.get(position).group_id + "/" + SharedPreferencesMethod.getUserId(activity);
                    API.sendRequestToServerGET(activity, url, API.GROUP_LIKE_POST);
                    Utils.logEventGroupLike(activity, post.group_id);
                    System.out.println("Group Like Post  "+url);
                    Drawable img = activity.getResources().getDrawable(R.drawable.ic_thumb_up_liked);
                    holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);


                    holder.tvLike.startAnimation(Utility.showAnimtion(activity));
                    posts.get(position).self_like = 1;
                    posts.get(position).total_likes = posts.get(position).total_likes + posts.get(position).self_like;
                } else {
                    if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                        Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                        return;
                    }

                    String url = API.GROUP_UNLIKE_POST + posts.get(position).post_id + "/" + posts.get(position).group_id + "/" + SharedPreferencesMethod.getUserId(activity);
                    API.sendRequestToServerGET(activity, url, API.GROUP_UNLIKE_POST);
                    System.out.println("Group UnLike Post  "+url);
                    Drawable img = activity.getResources().getDrawable(R.drawable.ic_thumb_up);
                    holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);


                    holder.tvLike.startAnimation(Utility.showAnimtion(activity));
                    posts.get(position).self_like = 0;
                    posts.get(position).total_likes = posts.get(position).total_likes - 1;
                }
                if (posts.get(position).total_likes == 0) {
                    holder.tvLike.setText(activity.getResources().getString(R.string.like_btn_text));
                } else if (posts.get(position).total_likes == 1) {
                    //holder.tvLike.setText(posts.get(position).total_likes + " " + activity.getResources().getString(R.string.like_btn_text));
                } else {
                    //holder.tvLike.setText(posts.get(position).total_likes + " " + activity.getResources().getString(R.string.likes_btn_text));
                }
            }
        });

        holder.llComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              commentValidation(holder,post);
            }
        });

        holder.more_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                holder.llComment.startAnimation(Utility.showAnimtion(activity));
                ((GroupPostActivity) activity).showCommentView(post, position);
                holder.mainCommmentContainer.setVisibility(View.GONE);
            }
        });

        holder.llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.llShare.startAnimation(Utility.showAnimtion(activity));
                Utils.showSharePopUpMenu(holder.llShare, posts.get(position), group_id, activity);
//                Intent intent = new Intent(activity, JoinedGroupListActivity.class);
//                intent.putExtra(ResponseParameters.POST, posts.get(position));
//                activity.startActivity(intent);
            }
        });
    }

    private boolean containsURL(String content){
        String REGEX = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        Pattern p = Pattern.compile(REGEX, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(content);
        if(m.find()) {
            return true;
        }

        return false;
    }


    private void reportPopup(String title, final String url, final String apiUrl, final int position, final HashMap<String, Object> input) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.report_popup, null);
        dialogBuilder.setView(dialogView);
        final EditText etTitle = dialogView.findViewById(R.id.etTitle);
        final TextView Pop_title = dialogView.findViewById(R.id.Pop_title);
        Pop_title.setText(title);
        final TextInputLayout etTitleLayout = dialogView.findViewById(R.id.etTitleLayout);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etTitleLayout.setErrorEnabled(false);
                if (etTitle.getText().toString().trim().isEmpty()) {
                    etTitleLayout.setErrorEnabled(true);
                    etTitleLayout.setError(activity.getResources().getString(R.string.reason_empty_input));
                } else if (Utility.isConnectingToInternet(activity)) {
                    if (apiUrl.equalsIgnoreCase(API.GROUP_USER_REPORT)) {
//                        Toast.makeText(activity, activity.getResources().getString(R.string.user_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(activity, activity.getResources().getString(R.string.user_has_been_reported_msg));
                        input.put(RequestParameters.REASON, "" + etTitle.getText().toString().trim());
                    } else if (apiUrl.equalsIgnoreCase(API.GROUP_POST_REPORT)) {
//                        Toast.makeText(activity, activity.getResources().getString(R.string.post_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(activity, activity.getResources().getString(R.string.post_has_been_reported_msg));
                        input.put(RequestParameters.DESCRIPTION, "" + etTitle.getText().toString().trim());
                    }
                    Log.e("input", "" + input);
                    API.sendRequestToServerPOST_PARAM(activity, url, input);
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                    confirmAlert.dismiss();
                } else {
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                }
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                confirmAlert.dismiss();
            }
        });


        this.confirmAlert = dialogBuilder.create();
        this.confirmAlert.setCancelable(true);
        this.confirmAlert.show();
    }

    public void getJsonData(){
//        try{
//            JSONObject outPut=new JSONObject();
//            JSONArray userList = new JSONArray();
//            userList = outPut.getJSONArray(ResponseParameters.CONNECTIONS);
//            System.out.println("Get Json Array Data"+userList.toString());
//        }catch (Exception ex){
//            ex.printStackTrace();
//        }

    }




    private void serViceCall() {
        if (!Utility.isConnectingToInternet(context)) { //net connection check
            return;
        }
        API.sendRequestToServerGET(context, API.HOME + SharedPreferencesMethod.getUserId(context), API.HOME);// for getting prevCount
        System.out.println("Home Service Call In Group   "+API.HOME + SharedPreferencesMethod.getUserId(context));
        getJsonData();
    }

    private void showPopUp(String title, final String url, final String apiUrl, final int position, final HashMap<String, Object> input) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);
        final TextView tvCancel = dialogView.findViewById(R.id.tvCancel);


        tvTitle.setText(title);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    confirmAlert.dismiss();
                    return;
                }
                if (input == null)
                    API.sendRequestToServerGET(activity, url, apiUrl);
                else
                    API.sendRequestToServerPOST_PARAM(activity, url, input);
                if (apiUrl.equalsIgnoreCase(API.GROUP_POST_DELETE)) {
                    notifyItemRemoved(position);
                    posts.remove(position);
                    notifyItemRangeChanged(0, posts.size());
                    Utils.showPopup(activity, activity.getString(R.string.post_delete_success_msg));
                } else if (apiUrl.equalsIgnoreCase(API.GROUP_USER_REPORT)) {
//                    Toast.makeText(activity, activity.getResources().getString(R.string.user_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                } else if (apiUrl.equalsIgnoreCase(API.GROUP_POST_REPORT)) {
//                    Toast.makeText(activity, activity.getResources().getString(R.string.post_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                }
                confirmAlert.dismiss();
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                confirmAlert.dismiss();
            }
        });

        confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();

    }

   /* private void configurePagerHolder(final GroupViewHolder holder, int position) {

        Post post = posts.get(position);

        String[] splitArray = post.image.split("\\|");

        ArrayList<String> images = new ArrayList<String>(Arrays.asList(splitArray));

        if (images.size() > 1) {
            holder.llPointer.setVisibility(View.VISIBLE);
        } else {
            holder.llPointer.setVisibility(View.GONE);
        }

        ImagePostPagerAdapter adapter = new ImagePostPagerAdapter(images, activity, groupImageBaseUrl);
        holder.slidesPager.setAdapter(adapter);

        holder.slidesPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {*//*empty*//*}

            @Override
            public void onPageSelected(int position) {
                holder.pageIndicatorView.setSelection(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {*//*empty*//*}
        });

        if (mViewPageStates.containsKey(position)) {
            holder.slidesPager.setCurrentItem(mViewPageStates.get(position));
            holder.pageIndicatorView.setSelection(mViewPageStates.get(position));
        }
    }*/


    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvTime, tvPostContent, tvLike, tvComments, tvPhotoNumber, tvCountLike, tvCountComment, tvreadmore;
        TextView no_comments_found,more_comments,comment_username,user_comments,comment_username_1,user_comments_1,comment_username_2,user_comments_2;
        ImageView ivUserImage, ivImageOne, ivImageTwo, ivImageThree, ivImageFour, ivImageFive, ivImageSingle,pdf_image;
        ImageView user_comment_img,user_comment_img_1,user_comment_img_2;
        View viewLine,viewLine_1,viewLine_2;
        CardView cvContainer;
        LinearLayout commentContainer,commentContainer_1,commentContainer_2;
        LinearLayout llEditPost, llLike, llComment, llShare, llSquareContainer, llImageContainer, llBottomControl,mainCommmentContainer,mainCommmentContainer_2;
        ProgressBar progressBar;
        /*SquareViewPager slidesPager;
        PageIndicatorView pageIndicatorView;
        LinearLayout llPointer;*/


        GroupViewHolder(View view) {
            super(view);
            tvUserName = view.findViewById(R.id.tvUserName);
            ivUserImage = view.findViewById(R.id.ivUserImage);
            cvContainer = view.findViewById(R.id.cvContainer);
            tvTime = view.findViewById(R.id.tvTime);
            tvPostContent = view.findViewById(R.id.tvPostContent);
            llEditPost = view.findViewById(R.id.llEditPost);
            llLike = view.findViewById(R.id.llLike);
            llComment = view.findViewById(R.id.llComment);
            llShare = view.findViewById(R.id.llShare);
            tvCountLike = view.findViewById(R.id.count_text_like);
            tvCountComment = view.findViewById(R.id.count_text_comment);
            tvreadmore = view.findViewById(R.id.tvreadmore);
            more_comments=view.findViewById(R.id.more_comments);
            viewLine=view.findViewById(R.id.viewLine);
            viewLine_1=view.findViewById(R.id.viewLine_1);
            viewLine_2=view.findViewById(R.id.viewLine_2);
           /* slidesPager = (SquareViewPager) view.findViewById(R.id.slidesPager);
            pageIndicatorView = (PageIndicatorView) view.findViewById(R.id.pageIndicatorView);
            llPointer = (LinearLayout) view.findViewById(R.id.llPointer);*/
            tvLike = view.findViewById(R.id.tvLike);
            tvComments = view.findViewById(R.id.tvComments);
            ivImageOne = view.findViewById(R.id.ivImageOne);
            ivImageTwo = view.findViewById(R.id.ivImageTwo);
            ivImageThree = view.findViewById(R.id.ivImageThree);
            ivImageFour = view.findViewById(R.id.ivImageFour);
            ivImageFive = view.findViewById(R.id.ivImageFive);
            ivImageSingle = view.findViewById(R.id.ivImageSingle);
            pdf_image = view.findViewById(R.id.pdf_image);
            llSquareContainer = view.findViewById(R.id.llSquareContainer);
            llImageContainer = view.findViewById(R.id.llImageContainer);
            llBottomControl = view.findViewById(R.id.llBottomControl);
            tvPhotoNumber = view.findViewById(R.id.tvPhotoNumber);

            no_comments_found=view.findViewById(R.id.no_comment_found);
            progressBar=view.findViewById(R.id.comment_loader);
            commentContainer=view.findViewById(R.id.user_comment_container);
            commentContainer_1=view.findViewById(R.id.user_comment_container_1);
            commentContainer_2=view.findViewById(R.id.user_comment_container_2);
            user_comments=view.findViewById(R.id.user_comments);
            mainCommmentContainer=view.findViewById(R.id.comment_main_container);
            mainCommmentContainer_2=view.findViewById(R.id.comment_main_container_2);
            comment_username=view.findViewById(R.id.user_comment_name);
            user_comment_img=view.findViewById(R.id.user_comment_img);
            user_comments_1=view.findViewById(R.id.user_comments_1);
            comment_username_1=view.findViewById(R.id.user_comment_name_1);
            user_comment_img_1=view.findViewById(R.id.user_comment_img_1);
            user_comments_2=view.findViewById(R.id.user_comments_2);
            comment_username_2=view.findViewById(R.id.user_comment_name_2);
            user_comment_img_2=view.findViewById(R.id.user_comment_img_2);


        }
    }
}