package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;

import com.codeholic.kotumb.app.BroadcastReciever.SMSReceiver;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.View.OtpEditText;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "SIGNUP";
    private EditText etPassword, etCPassword, etUserName, etUserPhone, etReferal;
    private TextInputLayout userNameLayout, passwordLayout, userPhoneLayout, cPasswordLayout, referralLayout;
    private TextView tvSignIn, tvLangEnglish, tvRegister, tvOtpHint, tvResend, tvPvc, tvTerms, tvText, tvUserNames;
    private LinearLayout llSuggestion;
    private ProgressBar pbLoading, pbUserName;
    private CardView cvRegister;
    private CardView cvOtpSubmit;
    private CheckBox cbAcceptTC;
    private LinearLayout rlOtpContainer, llDialog;
    private OtpEditText etOtp;
    private ImageView ivWrong, ivCorrect;
    private boolean userNameCorrect = false;
    private boolean isUserNameLoading = false;
    private Activity context;
    private String languageToLoad = "";
    private String passOld = "";
    Configuration config = new Configuration();
    Locale locale;
    AlertDialog mobileAvailibilityPopup;
    User user = new User();
    com.codeholic.kotumb.app.View.ProgressBar progressBar;
    HashMap<String, Object> inputUserName = new HashMap<>();
    private View hsv;
    String referralCode;

    private SMSReceiver smsReceiver;
    private int REQ_USER_CONSENT =200;

    //oncreate method for setting content view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Log.e(TAG,"User id is : " + getIntent().getStringExtra("USERID") + " " + "Referrel code is : " + getIntent().getStringExtra("REFCODE"));
      //  referralCode = getIntent().getStringExtra("REFCODE");
        languageToLoad = SharedPreferencesMethod.getDefaultLanguage(this);
        changeLocale(languageToLoad);
       // dynamicLinkCall();
    }

    private void dynamicLinkCall() {
        Log.e(TAG,"Come into dynamic link : ");
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(this.getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();

                            String referLink = deepLink.toString();
                            referLink = referLink.substring(referLink.lastIndexOf("=")+1);
                            Log.e("HOME","Referral link is : " + referLink);
                            String userId = referLink.substring(0,referLink.indexOf("-"));
                            referralCode = referLink.substring(referLink.indexOf("-")+1);

                            if (!referralCode.isEmpty() && !referralCode.equals("null")){
                                etReferal.setText(referralCode);
                            }else {
                                etReferal.setText("");
                            }
//                            if (isFirstLogin){
//                                Log.e("TAG","First time Logged in");
//                            }else {
//                                Log.e("TAG","Not a first Logged in");
//
//                                Invitations invitations = new Invitations(SharedPreferencesMethod.getUserId(HomeScreenActivity.this),userId,referralCode,SharedPreferencesMethod.getString(HomeScreenActivity.this,ResponseParameters.PROFILE_STRENGTH),status);
//                                invitationsMap.put(SharedPreferencesMethod.getUserId(HomeScreenActivity.this),invitations);
//                                childReference.push().setValue(invitationsMap).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                    @Override
//                                    public void onComplete(@NonNull Task<Void> task) {
//                                        if (task.isSuccessful()){
//                                            Log.e("HOME","Data added successfully");
//                                        }else {
//                                            Log.e("HOME","Exception while adding data : " + task.getException());
//                                        }
//
//                                    }
//                                });
//                            }

                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "getDynamicLink:onFailure", e);
                    }
                });

    }

    @Override
    protected void onStart() {
        super.onStart();
        registerBroadcastReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (smsReceiver != null) {
            unregisterReceiver(smsReceiver);
        }
    }


    private void getHintPhoneNumber(){
//        HintRequest hintRequest =
//                new HintRequest.Builder()
//                        .setPhoneNumberIdentifierSupported(true)
//                        .build();
//        PendingIntent mIntent = Auth.CredentialsApi.getHintPickerIntent(mGoogleApiClient, hintRequest);
//        try {
//            startIntentSenderForResult(mIntent.getIntentSender(), MESSAGE_READER, null, 0, 0, 0);
//        } catch (IntentSender.SendIntentException e) {
//            e.printStackTrace();
//        }
    }

    //change locale
    private void changeLocale(String languageToLoad) {
        locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setView();


    }

    //setting content view
    private void setView() {
        setContentView(R.layout.activity_signup);
        initView();
        setClickListener();
        userNameFocusLost(etUserName);
        setTextWatcher(etPassword, passwordLayout);
        setTextWatcher(etUserName, userNameLayout);
        setTextWatcher(etCPassword, cPasswordLayout);
        setTextWatcher(etUserPhone, userPhoneLayout);
        otpTextChangeListener();
    }

    //initializing views
    private void initView() {
        context = this;
        etReferal = findViewById(R.id.etReferCode);
      //  etReferal.setText(referralCode);
        referralLayout = findViewById(R.id.etRefferalLayout);
        etPassword = findViewById(R.id.etPassword);
        etUserName = findViewById(R.id.etUserName);
        etCPassword = findViewById(R.id.etCPassword);
        etUserPhone = findViewById(R.id.etUserPhone);
        etOtp = findViewById(R.id.etOtp);
        userNameLayout = findViewById(R.id.etUserNameLayout);
        Log.e("SIGNUP","Text size is : " + new EditText(this).getTextSize());
        passwordLayout = findViewById(R.id.etPasswordLayout);
        cPasswordLayout = findViewById(R.id.etCPasswordLayout);
        userPhoneLayout = findViewById(R.id.etUserPhoneLayout);
        tvSignIn = findViewById(R.id.tvSignIn);
        tvRegister = findViewById(R.id.tvRegister);
        tvLangEnglish = findViewById(R.id.tv_lan_english);
        if(languageToLoad.equals("hi")){
            tvLangEnglish.setText("हिंदी");
        }
        else if(languageToLoad.equals("kn")){
            tvLangEnglish.setText("ಕನ್ನಡ");
        }
        else
            tvLangEnglish.setText("English");
        tvOtpHint = findViewById(R.id.tvOtpHint);
        cvRegister = findViewById(R.id.cv_register);
        cvOtpSubmit = findViewById(R.id.cv_otp_submit);
        pbLoading = findViewById(R.id.pbLoading);
        pbUserName = findViewById(R.id.pbUserName);
        cbAcceptTC = findViewById(R.id.cbAcceptTC);
        ivWrong = findViewById(R.id.ivWrong);
        ivCorrect = findViewById(R.id.ivCorrect);
        rlOtpContainer = findViewById(R.id.rlOtpContainer);
        tvResend = findViewById(R.id.tv_resend);
        llDialog = findViewById(R.id.llDialog);
        tvPvc = findViewById(R.id.tvPvc);
        tvTerms = findViewById(R.id.tvTerms);
        tvText = findViewById(R.id.tvText);
        tvUserNames = findViewById(R.id.tvUserNames);
        llSuggestion = findViewById(R.id.llSuggestion);
        hsv = findViewById(R.id.hsv);
        tvTerms.setPaintFlags(tvTerms.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvPvc.setPaintFlags(tvPvc.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    //set click listeners on views
    private void setClickListener() {
        cvRegister.setOnClickListener(this);
        cvOtpSubmit.setOnClickListener(this);
        rlOtpContainer.setOnClickListener(this);
        tvLangEnglish.setOnClickListener(this);
        tvResend.setOnClickListener(this);
        tvSignIn.setOnClickListener(this);
        tvPvc.setOnClickListener(this);
        tvTerms.setOnClickListener(this);
        hsv.setOnClickListener(this);
    }





    private void userNameFocusLost(final EditText editText){
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Log.e("onTextChanged", "" + etUserName.getText().toString().trim().length());
                    ivCorrect.setVisibility(GONE);
                    ivWrong.setVisibility(GONE);
                    userNameLayout.setErrorEnabled(false);
                    llSuggestion.setVisibility(GONE);
                    if (editText.getText().toString().length() > 0) {
                        Pattern p = Pattern.compile("^[A-Za-z0-9_.@-]{1,30}$");
                        if (!p.matcher(etUserName.getText().toString().trim()).find()) {
                            //enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_atleast));
                            enableError(userNameLayout,getResources().getString(R.string.username_incorrect_input_special_character));
                        } else if (etUserName.getText().toString().trim().length() < 6) {
                            enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_atleast));
                        } else {
                            if (!Utility.isConnectingToInternet(context)) {
                                return;
                            }
                            if (editText.getText().toString().length() > 5) {
                                pbUserName.setVisibility(VISIBLE);
                                userNameCorrect = false;
                                isUserNameLoading = true;
                                inputUserName.put(RequestParameters.USERNAME, "" + etUserName.getText().toString().trim());
                                Log.e("input", "" + inputUserName);
                                API.sendRequestToServerPOST_PARAM(context, API.CHECKUSERNAME, inputUserName);
                            }
                        }
                    }

                }
            }
        });
    }
    //textwatcher method for all edittext
    private void setTextWatcher(final EditText editText, final TextInputLayout textInputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (editText == etPassword) {
                    passOld = etPassword.getText().toString().trim();
                    Log.e("beforeTextChanged", "" + etPassword.getText().toString().trim());
                }

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    textInputLayout.setErrorEnabled(false);
                }

                Log.e("beforeTextChanged 1", "" + etPassword.getText().toString().trim());

                if (editText == etUserName) {
                    Log.e("onTextChanged", "" + etUserName.getText().toString().trim().length());
                    ivCorrect.setVisibility(GONE);
                    ivWrong.setVisibility(GONE);
                    userNameLayout.setErrorEnabled(false);
                    llSuggestion.setVisibility(GONE);
                    if (s.length() > 0) {
                        Pattern p = Pattern.compile("^[A-Za-z0-9_.@-]{1,30}$");
                        if (!p.matcher(etUserName.getText().toString().trim()).find()) {
                            enableError(userNameLayout,getResources().getString(R.string.username_incorrect_input_special_character));
                           // enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_atleast));
                        } else if (etUserName.getText().toString().trim().length() < 6) {
                            enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_atleast));
                        } else {
                            if (!Utility.isConnectingToInternet(context)) {
                                return;
                            }
                            if (s.length() > 5) {
                                pbUserName.setVisibility(VISIBLE);
                                userNameCorrect = false;
                                isUserNameLoading = true;
                                inputUserName.put(RequestParameters.USERNAME, "" + etUserName.getText().toString().trim());
                                Log.e("input", "" + inputUserName);
                                API.sendRequestToServerPOST_PARAM(context, API.CHECKUSERNAME, inputUserName);
                            }
                        }
                    }
                }

                if (editText == etUserPhone) {
                    userPhoneLayout.setErrorEnabled(false);
                    if (s.length() > 0) {
                        if (isValidLong(etUserPhone.getText().toString())) {
                            if (etUserPhone.getText().toString().length() < 10) {
                                enableError(userPhoneLayout, getResources().getString(R.string.mobile_incorrect_input_minlength));
                            } else if (etUserPhone.getText().toString().length() >= 10) {

                                if (Utility.isConnectingToInternet(context)) { // check net connection
                                    progressBar = new com.codeholic.kotumb.app.View.ProgressBar(context);
                                    progressBar.show(getResources().getString(R.string.app_message_checking_mobile_number));
                                    API.sendRequestToServerGET(context, API.CHECK_MOBILE + etUserPhone.getText().toString().trim(), API.CHECK_MOBILE);// service call for account verification
                                }
                            }
                        } else {
                            enableError(userPhoneLayout, getResources().getString(R.string.mobile_incorrect_input_number));
                        }
                    }
                }

                if (editText == etPassword) {
                    passwordLayout.setErrorEnabled(false);
                    if (s.length() > 0) {
                        if (!etCPassword.getText().toString().trim().isEmpty() && !passOld.equals(etPassword.getText().toString().trim())) {
                            etCPassword.setText("");
                            enableError(cPasswordLayout, getResources().getString(R.string.password_empty_input));
                        }
                        Pattern pass = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z!@#$%^&*\\d]{1,12}");
                        /*boolean userPassMatcher = pass.matcher(etPassword.getText().toString().trim()).find();
                        if (!userPassMatcher) {
                            enableError(passwordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
                        } else */
                        if (etPassword.getText().toString().trim().length() < 6) {
                            enableError(passwordLayout, getResources().getString(R.string.password_incorrect_input_length));
                        }
                    }
                }

                if (editText == etCPassword) {
                    cPasswordLayout.setErrorEnabled(false);
                    if (etCPassword.getText().toString().trim().isEmpty()) {
                        enableError(cPasswordLayout, getResources().getString(R.string.password_empty_input));
                    } else if (!etPassword.getText().toString().trim().equals(etCPassword.getText().toString().trim())) {
                        enableError(cPasswordLayout, getResources().getString(R.string.confirm_password_input_match));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //checking for number is valid or not
    public static Boolean isValidLong(String value) {
        try {
            if (value.length() <= 0)
                return false;
            if (value.startsWith("0"))
                return false;
            Long val = Long.valueOf(value);
            return val != null;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean checkSpecialChar(String input) {
        return !input.contains("!") && !input.contains("@") && !input.contains("#") && !input.contains("$") && !input.contains("%") && !input.contains("^") && !input.contains("&") && !input.contains("*") && !input.contains(".");
    }

    //otp text watcher
    private void otpTextChangeListener() {
        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    if (s.length() >= 6) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(etOtp.getWindowToken(), 0);
                        if (s.toString().equalsIgnoreCase(user.getOtp())) { // checking otp
                            //progressBar = new com.codeholic.kotumb.app.View.ProgressBar(context);
                            //progressBar.show(getResources().getString(R.string.app_please_wait_text));
                            //verifyOtp();
                        } else {
                            final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.otp_incorrect_input), Toast.LENGTH_SHORT);
                            snackbar.show();
                        }
                    }
                    tvOtpHint.setVisibility(GONE);
                } else {
                    tvOtpHint.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //verify otp method
    private void verifyOtp() {
        try {
            if (!Utility.isConnectingToInternet(context)) { // check net connection
                if (progressBar != null) {
                    progressBar.dismiss(); // dismissing progressbar
                }
                final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            HashMap<String, Object> input = new HashMap<>();
            input.put(RequestParameters.USERID, "" + user.getUserId());
            API.sendRequestToServerPOST_PARAM(context, API.VERIFY, input);// service call for account verification

        } catch (Exception e) {
            final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.otp_incorrect_input), Toast.LENGTH_SHORT);
            snackbar.show();
            e.printStackTrace();
        }
    }

    //disabling error of input layout
    void disableError() {
        userNameLayout.setErrorEnabled(false);
        passwordLayout.setErrorEnabled(false);
        userPhoneLayout.setErrorEnabled(false);
        cPasswordLayout.setErrorEnabled(false);
    }

    //enabling error of input layout
    void enableError(TextInputLayout inputLayout, String error) {
        if (inputLayout != null || !error.isEmpty()){
            inputLayout.setErrorEnabled(true);
            inputLayout.setError(error);
        }

    }

    private void clearFocus() {
        etUserName.clearFocus();
        etUserPhone.clearFocus();
        etPassword.clearFocus();
        etCPassword.clearFocus();
    }

    //validation for fields
    private boolean validation() {
        disableError();
        boolean result = true;
        clearFocus();
        Pattern p = Pattern.compile("^[A-Za-z0-9_.@-]{6,30}$");
        Pattern phoneCheck = Pattern.compile("[1-9][0-9]{9,10}");
        //Pattern pass = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z!@#$%^&*\\d]{1,12}");
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etUserPhone.getWindowToken(), 0);

        boolean userNameMatcher = p.matcher(etUserName.getText().toString().trim()).find();
        //boolean userPassMatcher = pass.matcher(etPassword.getText().toString().trim()).find();
        if (etUserPhone.getText().toString().trim().isEmpty()) {
            if (result) {
                etUserPhone.requestFocus();
                imm.showSoftInput(etUserPhone, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(userPhoneLayout, getResources().getString(R.string.mobile_empty_input));
            result = false;
        } else if (etUserPhone.getText().toString().trim().length() < 10) {
            if (result) {
                etUserPhone.requestFocus();
                imm.showSoftInput(etUserPhone, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(userPhoneLayout, getResources().getString(R.string.mobile_incorrect_input_minlength));
            result = false;
        } else if (!phoneCheck.matcher(etUserPhone.getText().toString().trim()).find()) {
            if (result) {
                etUserPhone.requestFocus();
                imm.showSoftInput(etUserPhone, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(userPhoneLayout, getResources().getString(R.string.mobile_incorrect_input_number));
            result = false;
        }
        if (etUserName.getText().toString().trim().isEmpty()) {
            if (result) {
                etUserName.requestFocus();
                imm.showSoftInput(etUserName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(userNameLayout, getResources().getString(R.string.username_empty_input));
            result = false;
        } else if (etUserName.getText().toString().trim().length() < 6) {
            if (result) {
                etUserName.requestFocus();
                imm.showSoftInput(etUserName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_atleast));
            result = false;
        } else if (!userNameCorrect) {
            if (result) {
                etUserName.requestFocus();
                imm.showSoftInput(etUserName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_name_exists));
            result = false;
        } else if (!userNameMatcher) {
            if (result) {
                etUserName.requestFocus();
                imm.showSoftInput(etUserName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_atleast));
            result = false;
        }
        if (etPassword.getText().toString().trim().isEmpty()) {
            if (result) {
                etPassword.requestFocus();
                imm.showSoftInput(etPassword, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(passwordLayout, getResources().getString(R.string.password_empty_input));
            result = false;
        } /*else if (!userPassMatcher) {
            if (result) {
                etPassword.requestFocus();
                imm.showSoftInput(etPassword, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(passwordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
            result = false;
        } */ else if (etPassword.getText().toString().trim().length() < 6) {
            if (result) {
                etPassword.requestFocus();
                imm.showSoftInput(etPassword, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(passwordLayout, getResources().getString(R.string.password_incorrect_input_length));
            result = false;
        }
        if (etCPassword.getText().toString().trim().isEmpty()) {
            if (result) {
                etCPassword.requestFocus();
                imm.showSoftInput(etCPassword, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(cPasswordLayout, getResources().getString(R.string.password_empty_input));
            result = false;
        } else if (!etPassword.getText().toString().trim().equals(etCPassword.getText().toString().trim())) {
            if (result) {
                etCPassword.requestFocus();
                imm.showSoftInput(etCPassword, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(cPasswordLayout, getResources().getString(R.string.confirm_password_input_match));
            result = false;
        }
        if (!cbAcceptTC.isChecked()) {
            final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.term_conditions_accept), Toast.LENGTH_SHORT);
            snackbar.show();
            result = false;
        }
        return result;
    }

    //get response from service
    public void getResponse(JSONObject outPut, int type) {
        try {
            tvRegister.setVisibility(VISIBLE);
            pbLoading.setVisibility(GONE);

            if (type == 0) { // sign up response

                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        final Snackbar snackbar = Snackbar.make(cvRegister, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                        setEnable();
                    } else {
                        if (outPut.has(ResponseParameters.UserId) && outPut.has(ResponseParameters.Otp)) {
                            user = new User();
                            user.setUserId(outPut.getString(ResponseParameters.UserId));
                            user.setOtp(outPut.getString(ResponseParameters.Otp));
                            rlOtpContainer.setVisibility(VISIBLE);
                            final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.verify_otp_desc2_text), Toast.LENGTH_SHORT);
                            snackbar.show();
                           // Log.e("SIGNUP","Otp is : " + outPut.getString(ResponseParameters.Otp));

//                            final Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//
//                                    try {
//                                        etOtp.setText(outPut.getString(ResponseParameters.Otp));
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            }, 1000);

                            startSmsUserConsent();


                        } else {
                            final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                            snackbar.show();
                            setEnable();
                        }
                        //startActivity(new Intent(context, MyProfileActivity.class));
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { // error
                    final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                    setEnable();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { //no network
                    final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                    setEnable();
                }
            } else if (type == 1) { // verify response
                if (progressBar != null)
                    progressBar.dismiss();
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        final Snackbar snackbar = Snackbar.make(cvRegister, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        parseResponse(outPut);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            } else if (type == 2) { // resend otp response
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        final Snackbar snackbar = Snackbar.make(cvRegister, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        user.setOtp(outPut.getString(ResponseParameters.Otp));
                        final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.otp_resend_message), Toast.LENGTH_SHORT);
                        snackbar.show();
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            } else if (type == 3) { //check user name response
                llSuggestion.setVisibility(GONE);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(RequestParameters.USERNAME)) {
                        if (outPut.getString(RequestParameters.USERNAME).equalsIgnoreCase(etUserName.getText().toString().trim())) {
                            if (outPut.has(ResponseParameters.Success)) {
                                ivCorrect.setVisibility(VISIBLE);
                                ivWrong.setVisibility(GONE);
                                pbUserName.setVisibility(GONE);
                                userNameCorrect = true;
                                disableError();
                            } else {
                                ivWrong.setVisibility(VISIBLE);
                                ivCorrect.setVisibility(GONE);
                                pbUserName.setVisibility(GONE);
                                userNameCorrect = false;
                                if (outPut.has(ResponseParameters.SUGGESTIONS)) {
                                    showSuggestions(outPut.getJSONArray(ResponseParameters.SUGGESTIONS));
                                }
                                String error = getResources().getString(R.string.username_incorrect_input_name_exists);
                                error = outPut.getString(ResponseParameters.Errors);
                                if (error.contains("@")) {
                                    enableError(userNameLayout, error);
                                } else {
                                    enableError(userNameLayout,  getResources().getString(R.string.username_incorrect_input_name_exists));
                                }
                            }
                        } else {
                            if (isUserNameLoading) {
                                pbUserName.setVisibility(VISIBLE);
                                ivWrong.setVisibility(GONE);
                                ivCorrect.setVisibility(GONE);
                            }
                            userNameCorrect = false;
                        }
                    } else {
                        ivWrong.setVisibility(VISIBLE);
                        ivCorrect.setVisibility(GONE);
                        pbUserName.setVisibility(GONE);
                        userNameCorrect = false;
                        if (outPut.has(ResponseParameters.SUGGESTIONS)) {
                            showSuggestions(outPut.getJSONArray(ResponseParameters.SUGGESTIONS));
                        }
                        enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_name_exists));
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR") && outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { //error
                    ivWrong.setVisibility(VISIBLE);
                    pbUserName.setVisibility(GONE);
                    ivCorrect.setVisibility(GONE);
                    userNameCorrect = false;
                    if (outPut.has(ResponseParameters.SUGGESTIONS)) {
                        showSuggestions(outPut.getJSONArray(ResponseParameters.SUGGESTIONS));
                    }
                    enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_name_exists));
                } else {
                    ivWrong.setVisibility(VISIBLE);
                    ivCorrect.setVisibility(GONE);
                    pbUserName.setVisibility(GONE);
                    userNameCorrect = false;
                    if (outPut.has(ResponseParameters.SUGGESTIONS)) {
                        showSuggestions(outPut.getJSONArray(ResponseParameters.SUGGESTIONS));
                    }
                    enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_name_exists));
                }
                Pattern p = Pattern.compile("^[A-Za-z0-9_.@-]{6,30}$");
                boolean userNameMatcher = p.matcher(etUserName.getText().toString().trim()).find();

                if (!userNameMatcher) { // user name validation
                    ivWrong.setVisibility(GONE);
                    ivCorrect.setVisibility(GONE);
                    pbUserName.setVisibility(GONE);
                    enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_atleast));
                }
                isUserNameLoading = false;

            } else if (type == 4) { // verify number
                Log.e("TAG", "" + outPut.toString());
                if (progressBar != null)
                    progressBar.dismiss();
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success) && outPut.has(ResponseParameters.AVAILABLE)) {
                        if (outPut.getBoolean(ResponseParameters.AVAILABLE)) {
                            showPopUp(getResources().getString(R.string.mobile_number_exist_message));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startSMSListener() {
        SmsRetrieverClient mClient = SmsRetriever.getClient(this);
        Task<Void> mTask = mClient.startSmsRetriever();
        mTask.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override public void onSuccess(Void aVoid) {
                Toast.makeText(SignupActivity.this, "SMS Retriever starts", Toast.LENGTH_LONG).show();
            }
        });
        mTask.addOnFailureListener(new OnFailureListener() {
            @Override public void onFailure(@NonNull Exception e) {
                Toast.makeText(SignupActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showPopUp(String title) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvCancel = dialogView.findViewById(R.id.tvCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final View view = dialogView.findViewById(R.id.space);
        tvTitle.setText(title);
        tvCancel.setText(getResources().getString(R.string.primary_number_no_btn_text));


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mobileAvailibilityPopup.dismiss();
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etUserPhone.setText("");
                mobileAvailibilityPopup.dismiss();
            }
        });

        mobileAvailibilityPopup = dialogBuilder.create();
        mobileAvailibilityPopup.setCancelable(false);


        mobileAvailibilityPopup.show();

    }

    void showSuggestions(JSONArray suggestions) {
        try {
            if (suggestions.length() > 0) {
                llSuggestion.setVisibility(VISIBLE);
                String temp = "";
                for (int i = 0; i < suggestions.length(); i++) {
                    temp = temp + suggestions.getString(i) + "  ";
                }
                Log.e("TAG", "" + temp);
                if (tvUserNames.getText().toString().isEmpty())
                    tvUserNames.setText(temp.trim());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //enable views
    void setEnable() {
        etPassword.setEnabled(true);
        etUserName.setEnabled(true);
        etCPassword.setEnabled(true);
        etUserPhone.setEnabled(true);
        cvRegister.setClickable(true);
        cbAcceptTC.setClickable(true);
        cbAcceptTC.setEnabled(true);
    }

    //parse json response from server
    private void parseResponse(JSONObject json) {
        try {
            if (json.has(ResponseParameters.Userdata)) { // check rsponse have user data
                JSONObject outPut = json.getJSONObject(ResponseParameters.Userdata);
                user = new User();
                user.setUserName(outPut.getString(ResponseParameters.UserName));
                user.setUserId(outPut.getString(ResponseParameters.UserId));
                user.setMobileNumber(outPut.getString(ResponseParameters.MobileNumber));
                user.setReferenceId(outPut.getString(ResponseParameters.ReferenceId));
                user.setOtp(outPut.getString(ResponseParameters.Otp));
                user.setIsConfirmed(outPut.getString(ResponseParameters.IsConfirmed));
                // user.setDisplayName(outPut.getString(ResponseParameters.DisplayName));
                user.setSet1Questions(json.getString(ResponseParameters.Set1Questions));
                user.setSet2Questions(json.getString(ResponseParameters.Set2Questions));
                Intent intent = new Intent(context, MyProfileActivity.class);
                intent.putExtra(ResponseParameters.Userdata, user);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                SharedPreferencesMethod.setInt(SignupActivity.this, "run", 0);
                startActivity(intent);
                this.finish();
            } else {
                final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.otp_incorrect_input), Toast.LENGTH_SHORT);
                snackbar.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //change keyboard dialog
    void changeKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etPassword.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(etUserName.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(etCPassword.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(etUserPhone.getWindowToken(), 0);
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(getResources().getString(R.string.app_alert_text));
        dialogBuilder.setMessage(getResources().getString(R.string.keyboard_change_message));
        dialogBuilder.setCancelable(true);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.app_select_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                imeManager.showInputMethodPicker();
                dialog.dismiss();
            }
        });

        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                dialog.dismiss();
            }
        });

        dialogBuilder.create().show();
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cv_register: // register button click
                if (validation()) { // validation
                    try {
                        if (!Utility.isConnectingToInternet(context)) { // check net connection
                            final Snackbar snackbar = Snackbar.make(v, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        HashMap<String, Object> input = new HashMap<>();
                        input.put(RequestParameters.USERNAME, "" + etUserName.getText().toString().trim());
                        input.put(RequestParameters.MOBILE, "" + etUserPhone.getText().toString().trim());
                        input.put(RequestParameters.PASSWORD, "" + etPassword.getText().toString().trim());
                        input.put(RequestParameters.CONFIRM_PASSWORD, "" + etCPassword.getText().toString().trim());
                        input.put(RequestParameters.TERMS, "1");
                        input.put("referredBy", etReferal.getText().toString().trim()+"");
                        tvRegister.setVisibility(GONE);
                        pbLoading.setVisibility(VISIBLE);
                        cvRegister.setClickable(false);
                        cbAcceptTC.setClickable(false);
                        etPassword.setEnabled(false);
                        etUserName.setEnabled(false);
                        etCPassword.setEnabled(false);
                        etUserPhone.setEnabled(false);
                        cbAcceptTC.setEnabled(false);
                        Log.e("input", "" + input);
                        API.sendRequestToServerPOST_PARAM(context, API.SIGN_UP, input); // sign up service call
                      //  startSMSListener();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.tv_resend: // resend otp click
                try {
                    if (!Utility.isConnectingToInternet(context)) { //check net connection
                        return;
                    }
                    HashMap<String, Object> input = new HashMap<>();
                    input.put(RequestParameters.MOBILE, "" + etUserPhone.getText().toString().trim());
                    input.put(RequestParameters.USERID, "" + user.getUserId());
                    input.put(RequestParameters.LANG,SharedPreferencesMethod.getDefaultLanguageName(this));
                    API.sendRequestToServerPOST_PARAM(context, API.RESEND_OTP, input); // resend web service call
                } catch (Exception e) {
                    final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                    e.printStackTrace();
                }
                break;
            case R.id.tv_lan_english: // change language english
                languageToLoad = "en"; // your language
                SharedPreferencesMethod.setDefaultLanguage(context, languageToLoad);
//                SharedPreferencesMethod.setString(context, SharedPreferencesMethod.DEFAULT_LANGUAGE, languageToLoad);
                changeLocale(languageToLoad);
                //changeKeyBoard();
                break;
            case R.id.tv_lan_hindi: // change language hindi
                languageToLoad = "hi"; // your language
                SharedPreferencesMethod.setDefaultLanguage(context, languageToLoad);
//                SharedPreferencesMethod.setString(context, SharedPreferencesMethod.DEFAULT_LANGUAGE, languageToLoad);
                changeLocale(languageToLoad);
                changeKeyBoard();
                break;
            case R.id.tv_lan_kannada: // change language hindi
                languageToLoad = "kn"; // your language
                SharedPreferencesMethod.setDefaultLanguage(context, languageToLoad);
//                SharedPreferencesMethod.setString(context, SharedPreferencesMethod.DEFAULT_LANGUAGE, languageToLoad);
                changeLocale(languageToLoad);
                changeKeyBoard();
                break;


            case R.id.tvSignIn: // sign in screen open
                if (!isTaskRoot()) {
                    Intent i = new Intent(context, LoginActivity.class);
                    startActivity(i);
                    finishAffinity();
                } else {
                    Intent i = new Intent(context, LoginActivity.class);
                    startActivity(i);
                    finishAffinity();
                }
                break;
            case R.id.tvTerms: // terms and condition view
                if (SharedPreferencesMethod.getDefaultLanguage(this).trim().equalsIgnoreCase("en"))
                    Utility.prepareCustomTab(this, "" + API.termsUrl + "english");
                else
                    Utility.prepareCustomTab(this, "" + API.termsUrl + "english");

                break;
            case R.id.tvPvc: // privacy view
                /*InputMethodManager immo = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                immo.hideSoftInputFromWindow(etPassword.getWindowToken(), 0);
                immo.hideSoftInputFromWindow(etUserName.getWindowToken(), 0);
                immo.hideSoftInputFromWindow(etCPassword.getWindowToken(), 0);
                immo.hideSoftInputFromWindow(etUserPhone.getWindowToken(), 0);
                tvText.setText(Html.fromHtml(getResources().getString(R.string.privacy_policy_text)));
                llDialog.setVisibility(VISIBLE);*/

                if (SharedPreferencesMethod.getDefaultLanguage(this).trim().equalsIgnoreCase("en"))
                    Utility.prepareCustomTab(this, "" + API.privacyUrl + "english");
                else
                    Utility.prepareCustomTab(this, "" + API.privacyUrl + "english");

                break;

            case R.id.hsv: //language label click
                System.out.println("click====");
                getLang(null);
                break;

            case R.id.cv_otp_submit:
                if(etOtp.getText().length()==6) {
                    verifyOtp();
                }else{
                    final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.verify_otp_six_digit), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
                break;

            case R.id.rlOtpContainer:
                break;
        }
    }

    //backpress method
    @Override
    public void onBackPressed() {
        if (llDialog.getVisibility() == View.VISIBLE) {
            llDialog.setVisibility(GONE);
        } else {
            super.onBackPressed();
        }
    }

    public void getLang(View view) {
        Utils.getLang(this);
    }


    private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        //We can add sender phone number or leave it blank
        // I'm adding null here
        client.startSmsUserConsent(null).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getApplicationContext(), "On Success", Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "On OnFailure", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void registerBroadcastReceiver() {

        smsReceiver = new SMSReceiver();
        smsReceiver.smsBroadcastReceiverListener =
                new SMSReceiver.SmsBroadcastReceiverListener() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, REQ_USER_CONSENT);
                    }
                    @Override
                    public void onFailure() {

                    }
                };
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsReceiver, intentFilter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
                //That gives all message to us.
                // We need to get the code from inside with regex
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
//                textViewMessage.setText(
//                        String.format("%s - %s", getString(R.string.received_message), message));
                getOtpFromMessage(message);
            }
        }
    }
    private void getOtpFromMessage(String message) {
        Log.e("SIGNUP","Message is : " + message);
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            // otpText.setText(matcher.group(0));
            // Log.e("SIGNUP","Message is : " + matcher.group());
            etOtp.setText(matcher.group(0));
        }
    }

}
