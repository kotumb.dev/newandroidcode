package com.codeholic.kotumb.app.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ErrorMessages;
import com.codeholic.kotumb.app.Utility.PermissionsUtils;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.Locale;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    public static String TAG = "SplashActivity";

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */

    @Override
    public void onStart() {
        super.onStart();


        // Branch init
     /*   Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    Log.i("BRANCH SDK", referringParams.toString());
                    // Retrieve deeplink keys from 'referringParams' and evaluate the values to determine where to route the user
                    // Check '+clicked_branch_link' before deciding whether to use your Branch routing logic
                } else {
                    Log.i("BRANCH SDK", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);*/

    }



    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    int count = 0;
    private String languageToLoad = "";
    Configuration config = new Configuration();
    Locale locale;
    TextView tvLoading;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    private void getDynamicLink(){


        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(SplashActivity.this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();

                            String referLink = deepLink.toString();
                            referLink = referLink.substring(referLink.lastIndexOf("=")+1);
                            Log.e("HOME","Referral link is : " + referLink);
                            String userId = referLink.substring(0,referLink.indexOf("-"));
                            String referralCode = referLink.substring(referLink.indexOf("-")+1);

                            if (!referralCode.isEmpty() && !referralCode.equals("null")){
                                Log.e("SPLASH","refer code is :" + referralCode);
                            }else {
                                Log.e("SPLASH","refer code is :" + referralCode);
                            }

                        }else {
                            Log.e("SPLASH","refer code is null :");
                        }
                    }
                })
                .addOnFailureListener(SplashActivity.this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("TAG", "getDynamicLink:onFailure", e);
                    }
                });


//        FirebaseDynamicLinks.getInstance().getDynamicLink(getIntent())
//                .addOnSuccessListener(this, pendingDynamicLinkData -> {
//                    if (pendingDynamicLinkData == null) {
//                        //retrieveLocalIntent();
//                        Log.e("SPLASH","Dynamic link is null");
//                    } else {
//                        Toast.makeText(getApplicationContext(), "Resolving Link, Please Wait...", Toast.LENGTH_LONG).show();
//                        if (pendingDynamicLinkData.getLink().getQueryParameter("route") != null) {
//                            if (Objects.requireNonNull(pendingDynamicLinkData.getLink().getQueryParameter("route")).equalsIgnoreCase("profile")) {
//                                try {
//                                    Uri uri = pendingDynamicLinkData.getLink();
//                                    String permLink = uri.toString().split("\\?")[0];
//
//                                    Log.e("SPLASH","URL params are : " + " userId is :"+ uri.getQueryParameter("userId") + "REfCODe is : " + uri.getQueryParameter("referralcode"));
//                                } catch (NullPointerException e) {
//                                    Toast.makeText(getApplicationContext(), "Unable to View User Profile", Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        } else {
//
//                        }
//                    }
//                }).addOnFailureListener(this, new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.e("TAG", "getDynamicLink:onFailure", e);
//            }
//        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        printHashKey(this);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(SharedPreferencesMethod.POP_UP, false);
        editor.commit();



        isPackageInstalled("com.codeholic.kotumb.app",this.getPackageManager());

       // getDynamicLink();



//        if (!FacebookSdk.isInitialized()) {
//            FacebookSdk.setApplicationId(getResources().getString(R.string.facebook_app_id));
//            FacebookSdk.sdkInitialize(getApplication());
//            AppEventsLogger.activateApp(App.instance);
//        }

        try {
            // Branch logging for debugging
            // Branch object initialization
            /*Calendar cal = Calendar.getInstance();
            cal.add(Calendar.SECOND, 20);

            String date = "2018-01-17 20:42:00";
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = _24HourSDF.parse(date);
            Log.e("current date", ""+_24HourSDF.format(Calendar.getInstance().getTimeInMillis()));
            cal.setTime(date1);
            Intent intent = new Intent(getBaseContext(), NotifyReminder.class);
            intent.putExtra("NOTE", "NOTE");
            PendingIntent pendingIntent =
                    PendingIntent.getBroadcast(getBaseContext(),
                            1, intent, PendingIntent.FLAG_ONE_SHOT);
            AlarmManager alarmManager =
                    (AlarmManager) getSystemService(Context.ALARM_SERVICE);


            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                alarmManager.set(AlarmManager.RTC_WAKEUP,
                        cal.getTimeInMillis(), pendingIntent);
                Toast.makeText(getBaseContext(),
                        "call alarmManager.set()",
                        Toast.LENGTH_LONG).show();
            } else {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP,
                        cal.getTimeInMillis(), pendingIntent);
                Toast.makeText(getBaseContext(),
                        "call alarmManager.setExact()",
                        Toast.LENGTH_LONG).show();

            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }

        languageToLoad = SharedPreferencesMethod.getDefaultLanguage(this);
        changeLocale(languageToLoad);


//        FacebookSdk.sdkInitialize(this);
//        AppEventsLogger.activateApp(getApplicationContext());
//
//        FacebookSdk.setIsDebugEnabled(true);
//        FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
//
//        logViewContentEvent("View Content","01","INR",1200.00);
    }

    private void changeLocale(String languageToLoad) {
        locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setView();
    }

    private void setView() {
        setContentView(R.layout.activity_splash);

        mVisible = true;
        mContentView = findViewById(R.id.fullscreen_content);
        tvLoading = findViewById(R.id.tv_loading);

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        if (Build.VERSION.SDK_INT >= PermissionsUtils.SDK_INT_MARSHMALLOW) { // for permission
            if (PermissionsUtils.getInstance(this).requiredAllPermissionsGranted(this)) {
                runHandler();
            }
        } else {
            runHandler();
            //new MyDialog(context, MyDialog.UPDATE_PROFILE, null).show();
        }

        String lang = "english";
        String url = API.base_api+"services/errors/english";
        new AQuery(this).ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                try {
                    if (json.has("translations")) {
                        JSONArray jsonArray = json.getJSONArray("translations");
                        ErrorMessages.getInstance().loadErrorMessages(jsonArray);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SplashActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }.method(AQuery.METHOD_GET).header("Auth-Key", "KOTUMB__AuTh_keY"));
    }


    public void runHandler() {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                count++;
                if (count == 6) {
                    if (SharedPreferencesMethod.getUserId(SplashActivity.this).trim().isEmpty()) {
                        if (Utility.isFirstTime(getApplicationContext())) {

                            Intent intent = new Intent(SplashActivity.this, SignupActivity.class);
                            startActivity(intent);


//                            FirebaseDynamicLinks.getInstance()
//                                    .getDynamicLink(getIntent())
//                                    .addOnSuccessListener(SplashActivity.this, new OnSuccessListener<PendingDynamicLinkData>() {
//                                        @Override
//                                        public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
//                                            // Get deep link from result (may be null if no link is found)
//                                            Uri deepLink = null;
//                                            if (pendingDynamicLinkData != null) {
//                                                deepLink = pendingDynamicLinkData.getLink();
//
//                                                String referLink = deepLink.toString();
//                                                referLink = referLink.substring(referLink.lastIndexOf("=")+1);
//                                                Log.e("HOME","Referral link is : " + referLink);
//                                                String userId = referLink.substring(0,referLink.indexOf("-"));
//                                                String referralCode = referLink.substring(referLink.indexOf("-")+1);
//
//                                                if (!referralCode.isEmpty() && !referralCode.equals("null")){
//                                                    intent.putExtra("USERID",userId);
//                                                    intent.putExtra("REFCODE",referralCode);
//                                                    startActivity(intent);
//                                                }else {
//                                                    intent.putExtra("USERID","");
//                                                    intent.putExtra("REFCODE","");
//                                                    startActivity(intent);
//                                                }
//
//                                            }else {
//                                                intent.putExtra("USERID","");
//                                                intent.putExtra("REFCODE","");
//                                                startActivity(intent);
//                                            }
//                                        }
//                                    })
//                                    .addOnFailureListener(SplashActivity.this, new OnFailureListener() {
//                                        @Override
//                                        public void onFailure(@NonNull Exception e) {
//                                            Log.e("TAG", "getDynamicLink:onFailure", e);
//                                            startActivity(intent);
//                                        }
//                                    });


                           // Intent intent = new Intent(SplashActivity.this,SignupActivity.class);
                           // startActivity(intent);
                        } else {
                            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                        }
                    } else {
//                        FacebookSdk.sdkInitialize(SplashActivity.this);
//                        AppEventsLogger.activateApp(getApplication());
//
//                        FacebookSdk.setIsDebugEnabled(true);
//                        FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
//
//                        logViewContentEvent("View Content","01","INR",1200.00);
                        Log.e("SPLASH","Home Screen called");
//                        startActivity(new Intent(SplashActivity.this, WebViewActivity.class));
                        startActivity(new Intent(SplashActivity.this, HomeScreenActivity.class));
                        SplashActivity.this.finish();
                    }
                } else {
                    // tvLoading.setText(tvLoading.getText().toString()+".");
                    handler.postDelayed(this, 500);
                }
            }
        };
        handler.post(runnable);
    }

    // onRequestPermissionsResult for geeting permission from user
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        runHandler();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);


        if (AUTO_HIDE) {
            delayedHide(AUTO_HIDE_DELAY_MILLIS);
        }
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }


    public static void printHashKey(Context context) {
        try {
            final PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                final MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                final String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("AppLog", "SHA:" + md + "=");
                Log.i("AppLog", "Hashkey:" + hashKey + "=");
            }
        } catch (Exception e) {
            Log.e("AppLog", "error:", e);
        }
    }

    private boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packageName, 0);
            Log.e(TAG, "isPackageInstalled: True" );
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "isPackageInstalled: False" );
            return false;
        }
    }

}
