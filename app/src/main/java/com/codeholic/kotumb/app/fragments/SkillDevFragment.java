package com.codeholic.kotumb.app.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Activity.OrganizationActivity;
import com.codeholic.kotumb.app.Activity.SearchActivityDisplayUser;
import com.codeholic.kotumb.app.Adapter.MyListAdapter;
import com.codeholic.kotumb.app.Interface.OnGetViewListener;
import com.codeholic.kotumb.app.Model.DisplayUserObject;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * A simple {@link Fragment} subclass.
 */
public class SkillDevFragment extends Fragment {


    public static final String SKILL_DEVS = "skill_devs";
    private static final int ITEM_CLICK = 45;
    private View rootView;
    private View llLoading;
    private View llNotFound;
    private TextView textView;
    private int pagination;
    private ArrayList<DisplayUserObject> displayUserObjects = new ArrayList<>();
    private ListView listView;
    private MyListAdapter adapter;
    private SwipeRefreshLayout swipeLayout;
    private MenuItem searchItem;
    private int itemClicked = -1;
    private boolean flag_loading;
    private View footerView;

    public SkillDevFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_skill_dev, container, false);
        findViews();
        setupListView();
        fetchSkillDevs();
        setHasOptionsMenu(true);
        ((HomeScreenActivity) Objects.requireNonNull(getActivity())).updateToolbarTitle(getString(R.string.skill_dev));
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void findViews() {
        llLoading = rootView.findViewById(R.id.progress_circular);
        llNotFound = rootView.findViewById(R.id.llNotFound);
        textView = rootView.findViewById(R.id.tv);
        swipeLayout = rootView.findViewById(R.id.swipelayout);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                flag_loading = false;
                swipeLayout.setRefreshing(false);
                fetchSkillDevs();
            }
        });
    }

    private void setupListView() {
        listView = rootView.findViewById(R.id.listView);
        adapter = getAdapter();
        listView.setAdapter(adapter);
        setOnLoadMoreListener(listView, new OnGetViewListener() {
            @Override
            public View onGetView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater inflater = getLayoutInflater();
                footerView = inflater.inflate(R.layout.item_loading, viewGroup, false);
                listView.addFooterView(footerView);
                pagination = pagination + 8;
                String url = API.SKILL_DEV + pagination + "/" + SharedPreferencesMethod.getUserId(getActivity());
                API.sendRequestToServerGET_FRAGMENT(getActivity(), SkillDevFragment.this, url, API.SKILL_DEV + 1);
                return null;
            }
        });
    }

    private void setOnLoadMoreListener(final ListView listView, final OnGetViewListener listener) {
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {


            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if (!flag_loading) {
                        flag_loading = true;
                        listener.onGetView(0, null, null);
                    }
                }

                int topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();
                //swipelayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });
    }

    @NonNull
    private MyListAdapter getAdapter() {
        return new MyListAdapter(displayUserObjects, new OnGetViewListener() {
            @Override
            public View onGetView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.skill_dev_item, viewGroup, false);
                ImageView ivSkillDevImage = v.findViewById(R.id.ivSkillDevImage);
                TextView tvSkillDevName = v.findViewById(R.id.tvSkillDevName);
                TextView tvSkillDevCount = v.findViewById(R.id.tvSkillDevCount);
                TextView tvSkillDevSummery = v.findViewById(R.id.tvSkillDevSummery);

                final DisplayUserObject userObject = displayUserObjects.get(i);

                String text = userObject.getFirstName() + " "+userObject.getMiddleName()+" " + userObject.getLastName();
                tvSkillDevName.setText(text.replaceAll("  "," "));
                tvSkillDevSummery.setText(userObject.getCity() + "");
                String likes = getString(R.string.like_btn_text);
                String comments = getString(R.string.comment_btn_text);
                tvSkillDevCount.setText(userObject.getLikes_count() + " " + likes + " · " + userObject.getComments_count() + " " + comments);
                String url = API.imageUrl_displayUsers + userObject.getAvatar().trim();
                new AQuery(getActivity()).id(ivSkillDevImage).image(url, true, true, 300, R.drawable.factory);
                v.setOnClickListener(getItemClickListener(userObject, i));
                return v;
            }
        });
    }

    @NonNull
    private View.OnClickListener getItemClickListener(final DisplayUserObject userObject, final int i) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), OrganizationActivity.class);
                intent.putExtra("data", new Gson().toJson(userObject));
                startActivityForResult(intent, ITEM_CLICK);
                itemClicked = i;
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ITEM_CLICK && resultCode == Activity.RESULT_OK && data != null) {
            String dataString = data.getStringExtra("data");
            DisplayUserObject userObject = new Gson().fromJson(dataString, DisplayUserObject.class);
            displayUserObjects.set(itemClicked, userObject);
            adapter.notifyDataSetChanged();
        }
    }

    public void fetchSkillDevs() {
        try {
            if (!Utility.isConnectingToInternet(getActivity())) {
                final Snackbar snackbar = Snackbar.make(llNotFound, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(View.VISIBLE);
                textView.setText(getResources().getString(R.string.app_no_internet_error));
                return;
            }

            pagination = 0;

            llLoading.setVisibility(VISIBLE);
            llNotFound.setVisibility(View.GONE);
//            cvContainer.setVisibility(View.GONE);

            displayUserObjects.clear();
            String url = API.SKILL_DEV + pagination + "/" + SharedPreferencesMethod.getUserId(getActivity());
            API.sendRequestToServerGET_FRAGMENT(getActivity(), this, url, API.SKILL_DEV);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getResponse(JSONObject outPut, final int i) {
        if (i == 0) {
            llLoading.setVisibility(GONE);
            Log.e("output", outPut.toString());
            if (outPut.has(SKILL_DEVS)) {
                try {
                    JSONArray outPutJSONArray = outPut.getJSONArray(SKILL_DEVS);
                    displayUserObjects.addAll(Utility.getDisplayUserObjects(outPutJSONArray));
                    adapter.notifyDataSetChanged();
                    if(displayUserObjects.size()==0){
                        showErrorUI("Nothing Found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showErrorUI(getString(R.string.something_wrong));
                }
            } else {
                showErrorUI(getString(R.string.something_wrong));
            }
        } else if (i == 1) {
            flag_loading = false;
            listView.removeFooterView(footerView);
            Log.e("output", outPut.toString());
            if (outPut.has(SKILL_DEVS)) {
                try {
                    JSONArray outPutJSONArray = outPut.getJSONArray(SKILL_DEVS);
                    ArrayList<DisplayUserObject> userObjects = Utility.getDisplayUserObjects(outPutJSONArray);
                    this.displayUserObjects.addAll(userObjects);
                    adapter.notifyDataSetChanged();
                    if (userObjects.size() == 0) {
                        flag_loading = true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showErrorUI(getString(R.string.something_wrong));
                }
            } else {
                flag_loading = true;
                // showErrorUI(getString(R.string.something_wrong));
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        inflater.inflate(R.menu.menu_search, menu);
        searchItem = menu.findItem(R.id.search);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(getActivity(), SearchActivityDisplayUser.class));
                // mSearchView.display();
                //openKeyboard();
                return true;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void showErrorUI(String errorText) {
        llNotFound.setVisibility(VISIBLE);
        textView.setText(errorText);
    }

}
