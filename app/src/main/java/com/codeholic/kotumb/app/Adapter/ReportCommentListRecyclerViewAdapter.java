package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.ImagesShowActivity;
import com.codeholic.kotumb.app.Activity.OtherUserProfileActivity;
import com.codeholic.kotumb.app.Activity.ReportedCommentActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.Model.PostComments;
import com.codeholic.kotumb.app.Model.ReportPost;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class ReportCommentListRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ReportPost> posts;
    User user;
    Activity activity;
    Context context;
    AlertDialog alert;
    AlertDialog confirmAlert;
    private HashMap<Integer, Integer> mViewPageStates = new HashMap<>();


    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;

    int type = 0;

    public void setType(int type) {
        this.type = type;
    }

    public void setUserImageBaseUrl(String base_url) {
        this.userImageBaseUrl = base_url;
    }

    String userImageBaseUrl = "";

    public void setGroupImageBaseUrl(String base_url) {
        this.groupImageBaseUrl = base_url;
    }

    String groupImageBaseUrl = "";

    public ReportCommentListRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<ReportPost> posts,User user) {
        this.posts = posts;
        this.user=user;
        this.activity = activity;
        context = activity.getBaseContext();

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }


    @Override
    public int getItemViewType(int position) {
        return posts.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.group_report_comment_item, parent, false);
            GroupViewHolder GroupViewHolder = new GroupViewHolder(view);
            return GroupViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, final int position) {
        Post post = this.posts.get(position).post_info;
        ReportPost reportPost = this.posts.get(position);
        PostComments postComments = this.posts.get(position).group_comment_info;
        if (viewholder instanceof GroupViewHolder) {
            ArrayList<String> imageNameList = new ArrayList<>();
            GroupViewHolder holder = (GroupViewHolder) viewholder;
            String text = "" + post.firstName.trim() + " " + post.middleName + " " + post.lastName.trim();
            holder.tvUserName.setText(text.replaceAll("  "," "));
            holder.tvUserFromName.setText(reportPost.firstName +" " + " " + reportPost.lastName);
            if (!post.content.trim().isEmpty()) {
                holder.tvPostContent.setVisibility(View.VISIBLE);
                holder.tvPostContent.setText(post.content);
            } else {
                holder.tvPostContent.setVisibility(View.GONE);
            }

            holder.tvReportContent.setText("" + reportPost.description.trim());
            holder.tvCommentUserName.setText("" + postComments.firstName.trim() + " " + postComments.lastName.trim());
            holder.tvCommentContent.setText("" + postComments.comment.trim());
            holder.tvTime.setText(post.duration.trim());
            new AQuery(activity).id(holder.ivUserImage).image(userImageBaseUrl + post.avatar.trim(), true, true, 300, R.drawable.ic_user);
            new AQuery(activity).id(holder.ivUserFromImage).image(userImageBaseUrl + reportPost.avatar, true, true, 300, R.drawable.ic_user);

            new AQuery(activity).id(holder.ivCommentUserImage).image(userImageBaseUrl + postComments.avatar, true, true, 300, R.drawable.ic_user);


            Log.e("TAG", "image " + post.image);
            if (!post.image.trim().isEmpty()) {
                String[] splitArray = post.image.split("\\|");
                imageNameList = new ArrayList<String>(Arrays.asList(splitArray));
            }
            holder.ivImageOne.setVisibility(View.GONE);
            holder.ivImageTwo.setVisibility(View.GONE);
            holder.ivImageThree.setVisibility(View.GONE);
            holder.ivImageFour.setVisibility(View.GONE);
            holder.ivImageFive.setVisibility(View.GONE);
            holder.llSquareContainer.setVisibility(View.GONE);
            holder.ivImageSingle.setVisibility(View.GONE);

            if (imageNameList.size() == 0) {
                holder.llImageContainer.setVisibility(View.GONE);
            } else {
                holder.llImageContainer.setVisibility(View.VISIBLE);
                if (imageNameList.size() == 1) {
                    new AQuery(activity).id(holder.ivImageSingle).image(groupImageBaseUrl + imageNameList.get(0), true, true, 300, R.drawable.ic_user);
                    holder.ivImageSingle.setVisibility(View.VISIBLE);
                } else if (imageNameList.size() == 3) {
                    new AQuery(activity).id(holder.ivImageThree).image(groupImageBaseUrl + imageNameList.get(0), true, true, 300, R.drawable.ic_user);
                    holder.ivImageThree.setVisibility(View.VISIBLE);
                    new AQuery(activity).id(holder.ivImageFour).image(groupImageBaseUrl + imageNameList.get(1), true, true, 300, R.drawable.ic_user);
                    holder.ivImageFour.setVisibility(View.VISIBLE);
                    new AQuery(activity).id(holder.ivImageFive).image(groupImageBaseUrl + imageNameList.get(2), true, true, 300, R.drawable.ic_user);
                    holder.ivImageFive.setVisibility(View.VISIBLE);
                } else {
                    for (int i = 0; i < imageNameList.size(); i++) {
                        switch (i) {
                            case 0:
                                new AQuery(activity).id(holder.ivImageOne).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.ic_user);
                                holder.ivImageOne.setVisibility(View.VISIBLE);
                                break;
                            case 1:
                                new AQuery(activity).id(holder.ivImageTwo).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.ic_user);
                                holder.ivImageTwo.setVisibility(View.VISIBLE);
                                break;
                            case 2:
                                new AQuery(activity).id(holder.ivImageThree).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.ic_user);
                                holder.ivImageThree.setVisibility(View.VISIBLE);
                                break;
                            case 3:
                                new AQuery(activity).id(holder.ivImageFour).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.ic_user);
                                holder.ivImageFour.setVisibility(View.VISIBLE);
                                break;
                            case 4:
                                new AQuery(activity).id(holder.ivImageFive).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.ic_user);
                                holder.ivImageFive.setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                }
                if (imageNameList.size() > 5) {
                    holder.llSquareContainer.setVisibility(View.VISIBLE);
                    holder.tvPhotoNumber.setText((imageNameList.size() - 5) + "+");
                }
            }


            setOnClickListener(holder, post, position, imageNameList);
            openProfileVolley(holder,postComments.userCommentedBy);
            openProfile(holder);
        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }




    public void openProfileVolley(final GroupViewHolder holder,final String id){
        holder.tvUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                API.openOtherUserProfile(id,context);
            }
        });
    }



    private void openProfile(GroupViewHolder holder){
        holder.tvUserFromName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OtherUserProfileActivity.class);
                intent.putExtra("USER", user);
                context.startActivity(intent);
            }
        });
    }


   /* @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {

        if (holder instanceof GroupViewHolder) {

            GroupViewHolder viewHolder = (GroupViewHolder) holder;

            mViewPageStates.put(holder.getAdapterPosition(), viewHolder.slidesPager.getCurrentItem());
            super.onViewRecycled(holder);
        }
    }*/


    public void setOnClickListener(final GroupViewHolder holder, final Post post, final int position, final ArrayList<String> imageNameList) {
        holder.llImageContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ImagesShowActivity.class);
                intent.putExtra(ResponseParameters.IMAGE, imageNameList);
                intent.putExtra(ResponseParameters.IMAGE_URI_LIST, new ArrayList<>());
                intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, groupImageBaseUrl);
                intent.putExtra(ResponseParameters.SHOW_CLEAR, false);
                activity.startActivity(intent);
            }
        });

        holder.llEditPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.llEditPost.startAnimation(Utility.showAnimtion(activity));
                showPopUp(activity.getResources().getString(R.string.delete_comment_dialog_msg), position);
            }
        });
    }

    private void showPopUp(String title, final int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = (LinearLayout) dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = (LinearLayout) dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = (TextView) dialogView.findViewById(R.id.title);
        final TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        final TextView tvCancel = (TextView) dialogView.findViewById(R.id.tvCancel);
        tvCancel.setText(activity.getResources().getString(R.string.post_report_ignore_btn_text));


        tvTitle.setText(title);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    confirmAlert.dismiss();
                    return;
                }


                API.sendRequestToServerGET(activity, API.REPORTED_GROUP_POST_COMMENT_ACTION + posts.get(position).id + "/" + posts.get(position).comment_id + "/1", API.REPORTED_GROUP_POST_COMMENT_ACTION);

                notifyItemRemoved(position);
                posts.remove(position);
                notifyItemRangeChanged(0, posts.size());
                hideLayout();
                confirmAlert.dismiss();
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    confirmAlert.dismiss();
                    return;
                }

                API.sendRequestToServerGET(activity, API.REPORTED_GROUP_POST_COMMENT_ACTION + posts.get(position).id + "/" + posts.get(position).comment_id + "/0", API.REPORTED_GROUP_POST_COMMENT_ACTION);

                notifyItemRemoved(position);
                posts.remove(position);
                notifyItemRangeChanged(0, posts.size());
                hideLayout();
                confirmAlert.dismiss();
            }
        });

        confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();

    }

    void hideLayout() {
        ((ReportedCommentActivity) activity).hideLayout();
    }


    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvTime, tvPostContent, tvPhotoNumber, tvUserFromName, tvCommentUserName, tvCommentContent, tvReportContent;
        ImageView ivUserImage, ivImageOne, ivImageTwo, ivImageThree, ivImageFour, ivImageFive, ivImageSingle, ivUserFromImage, ivCommentUserImage;
        CardView cvContainer;
        LinearLayout llEditPost, llLike, llComment, llSquareContainer, llImageContainer;


        GroupViewHolder(View view) {
            super(view);
            tvUserName = (TextView) view.findViewById(R.id.tvUserName);
            tvReportContent = (TextView) view.findViewById(R.id.tvReportContent);
            tvUserFromName = (TextView) view.findViewById(R.id.tvUserFromName);
            tvCommentUserName = (TextView) view.findViewById(R.id.tvCommentUserName);
            tvCommentContent = (TextView) view.findViewById(R.id.tvCommentContent);
            ivUserImage = (ImageView) view.findViewById(R.id.ivUserImage);
            ivUserFromImage = (ImageView) view.findViewById(R.id.ivUserFromImage);
            ivCommentUserImage = (ImageView) view.findViewById(R.id.ivCommentUserImage);
            cvContainer = (CardView) view.findViewById(R.id.cvContainer);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            tvPostContent = (TextView) view.findViewById(R.id.tvPostContent);
            llEditPost = (LinearLayout) view.findViewById(R.id.llEditPost);
            llLike = (LinearLayout) view.findViewById(R.id.llLike);
            llComment = (LinearLayout) view.findViewById(R.id.llComment);
            ivImageOne = (ImageView) view.findViewById(R.id.ivImageOne);
            ivImageTwo = (ImageView) view.findViewById(R.id.ivImageTwo);
            ivImageThree = (ImageView) view.findViewById(R.id.ivImageThree);
            ivImageFour = (ImageView) view.findViewById(R.id.ivImageFour);
            ivImageFive = (ImageView) view.findViewById(R.id.ivImageFive);
            ivImageSingle = (ImageView) view.findViewById(R.id.ivImageSingle);
            llSquareContainer = (LinearLayout) view.findViewById(R.id.llSquareContainer);
            llImageContainer = (LinearLayout) view.findViewById(R.id.llImageContainer);
            tvPhotoNumber = (TextView) view.findViewById(R.id.tvPhotoNumber);
        }
    }
}