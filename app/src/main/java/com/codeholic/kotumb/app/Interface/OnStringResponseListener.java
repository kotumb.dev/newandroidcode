package com.codeholic.kotumb.app.Interface;

public interface OnStringResponseListener {
    void onGotResponse(String response);
}