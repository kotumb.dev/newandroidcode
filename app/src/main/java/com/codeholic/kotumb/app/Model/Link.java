package com.codeholic.kotumb.app.Model;

/**
 * Created by DELL on 11/1/2017.
 */

public class Link {
    String link = "userdata";
    String linkName = "userId";
    String linkId = "referenceId";

    public Link() {

    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

}
