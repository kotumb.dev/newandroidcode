package com.codeholic.kotumb.app.Utility;

import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Activity.AddNoteActivity;
import com.codeholic.kotumb.app.Activity.ChatActivity;
import com.codeholic.kotumb.app.Activity.CompleteProfileActivity;
import com.codeholic.kotumb.app.Activity.ConnectionsActivity;
import com.codeholic.kotumb.app.Activity.ConnectionsMessageActivity;
import com.codeholic.kotumb.app.Activity.ContactFeedbackActivity;
import com.codeholic.kotumb.app.Activity.CreateGroupActivity;
import com.codeholic.kotumb.app.Activity.CreatePostActivity;
import com.codeholic.kotumb.app.Activity.EducationActivity;
import com.codeholic.kotumb.app.Activity.ForgetPasswordActivity;
import com.codeholic.kotumb.app.Activity.GroupInfoActivity;
import com.codeholic.kotumb.app.Activity.GroupInviteActivity;
import com.codeholic.kotumb.app.Activity.GroupPostActivity;
import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Activity.InvitationDetails;
import com.codeholic.kotumb.app.Activity.JoinedGroupListActivity;
import com.codeholic.kotumb.app.Activity.LinksActivity;
import com.codeholic.kotumb.app.Activity.LoginActivity;
import com.codeholic.kotumb.app.Activity.MessageActivity;
import com.codeholic.kotumb.app.Activity.MyProfileActivity;
import com.codeholic.kotumb.app.Activity.NotepadActivity;
import com.codeholic.kotumb.app.Activity.NotificationActivity;
import com.codeholic.kotumb.app.Activity.OrganizationActivity;
import com.codeholic.kotumb.app.Activity.OtherUserProfileActivity;
import com.codeholic.kotumb.app.Activity.PostDetailsActivity;
import com.codeholic.kotumb.app.Activity.PostLikesActivity;
import com.codeholic.kotumb.app.Activity.ProfessionActivity;
import com.codeholic.kotumb.app.Activity.ReportedCommentActivity;
import com.codeholic.kotumb.app.Activity.ReportedPostActivity;
import com.codeholic.kotumb.app.Activity.ReportedUserActivity;
import com.codeholic.kotumb.app.Activity.ResumeTemplateActivity;
import com.codeholic.kotumb.app.Activity.RewardHistoryActivty;
import com.codeholic.kotumb.app.Activity.SearchActivity;
import com.codeholic.kotumb.app.Activity.SearchActivityDisplayUser;
import com.codeholic.kotumb.app.Activity.SearchConnectionActivtiy;
import com.codeholic.kotumb.app.Activity.ShareResumeActivity;
import com.codeholic.kotumb.app.Activity.ShowVideos;
import com.codeholic.kotumb.app.Activity.SignupActivity;
import com.codeholic.kotumb.app.Activity.UnomerActivity;
import com.codeholic.kotumb.app.Activity.UpdateLoginInfoActivity;
import com.codeholic.kotumb.app.Activity.UpdatePasswordActivity;
import com.codeholic.kotumb.app.Activity.UploadVideoActivity;
import com.codeholic.kotumb.app.Activity.UserVideoListActivity;
import com.codeholic.kotumb.app.Activity.VideoDetailsActivity;
import com.codeholic.kotumb.app.Activity.VideoLikesListActivity;
import com.codeholic.kotumb.app.Activity.WhoViewActivity;
import com.codeholic.kotumb.app.Interface.OnResponseListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.Services.GetAddService;
import com.codeholic.kotumb.app.Services.GetPreLoadDataService;
import com.codeholic.kotumb.app.fragments.BaseFragment;
import com.codeholic.kotumb.app.fragments.ConnectionFragment;
import com.codeholic.kotumb.app.fragments.CurrentRewardFragment;
import com.codeholic.kotumb.app.fragments.ExpiredRewardFragment;
import com.codeholic.kotumb.app.fragments.FavrtLinksFragment;
import com.codeholic.kotumb.app.fragments.GroupFragment;
import com.codeholic.kotumb.app.fragments.HomeFragment;
import com.codeholic.kotumb.app.fragments.InviteFragment;
import com.codeholic.kotumb.app.fragments.LinksFragment;
import com.codeholic.kotumb.app.fragments.MeFragment;
import com.codeholic.kotumb.app.fragments.MembersFragment;
import com.codeholic.kotumb.app.fragments.NotepadFragment;
import com.codeholic.kotumb.app.fragments.NotificationFragment;
import com.codeholic.kotumb.app.fragments.RedeemRewardFragment;
import com.codeholic.kotumb.app.fragments.SkillDevFragment;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class API {

    private static String TAG ="API";

    //Development Server APIs

    /*public static String base_api = "http://dev.codeholic.in/mamta/kotumb/api/v3/";
    public static String share_post_link = "http://dev.codeholic.in/mamta/kotumb/";
    public static String get_mediaLink = "http://dev.codeholic.in/mamta/kotumb/uploads/notifications/";
    public static String downloadPDF = "http://dev.codeholic.in/mamta/kotumb/uploads/messages/";
    public static String imageUrl = "http://dev.codeholic.in/mamta/kotumb/uploads/users/";
    public static String imageAdUrl = "http://dev.codeholic.in/mamta/kotumb/uploads/advertises/";
    public static String imageUrl_displayUsers="http://dev.codeholic.in/mamta/kotumb/uploads/display-users/";
    public static String termsUrl = "http://dev.codeholic.in/mamta/kotumb/terms";
    public static String privacyUrl = "http://dev.codeholic.in/mamta/kotumb/api/v1/services/privacy/";
    public static String contactUsLink = "http://dev.codeholic.in/mamta/kotumb/ajax/support/";
    public static String contactUsOpenChat = "http://dev.codeholic.in/mamta/kotumb/ajax/singleQuery/";
    public static String defaultAdUrl = "http://dev.codeholic.in/mamta/kotumb/info/advertise";
    public static String gallary_media="http://dev.codeholic.in/mamta/kotumb/uploads/display-users/";
    public static String group_media="http://dev.codeholic.in/mamta/kotumb/uploads/groups/";
    public static String user_videos="http://dev.codeholic.in/mamta/kotumb/uploads/user_videos/";*/



    //Testing Server Server APIs
    //  public static String base_api = "http://www.kotumb.com/testserver1/api/v3/";
    //public static String base_api = "http://ec2-13-235-38-213.ap-south-1.compute.amazonaws.com/kotumb_testserver/testserver1/api/v3/";
    // public static String base_api = "http://www.kotumb.com/testserver1/api/v3/";
    //public static String base_api = "http://ec2-13-235-38-213.ap-south-1.compute.amazonaws.com/kotumb_testserver/testserver1/api/v3/";
    /*public static String share_post_link = "http://kotumb.com/testserver1/";
    public static String get_mediaLink = "http://s3.ap-south-1.amazonaws.com/kotumb-store-testing/uploads/notifications/";
    public static String downloadPDF = "http://s3.ap-south-1.amazonaws.com/kotumb-store-testing/uploads/messages/";
    public static String imageUrl = "http://s3.ap-south-1.amazonaws.com/kotumb-store-testing/uploads/users/";
    public static String imageUrl_displayUsers="http://s3.ap-south-1.amazonaws.com/kotumb-store-testing/uploads/display-users/";
    public static String imageAdUrl = "http://kotumb.com/testserver1/uploads/advertises/";
    public static String termsUrl = "http://kotumb.com/testserver1/welcome/tnc/";
    public static String privacyUrl = "http://kotumb.com/testserver1/welcome/privacy-policy/";
    public static String contactUsLink = "http://kotumb.com/testserver1/ajax/support/";
    public static String contactUsOpenChat = "http://kotumb.com/testserver1/ajax/singleQuery/";
    public static String defaultAdUrl = "http://kotumb.com/testserver1/info/advertise";
    public static String gallary_media="http://s3.ap-south-1.amazonaws.com/kotumb-store-testing/uploads/display-users/";
    public static String group_media="http://s3.ap-south-1.amazonaws.com/kotumb-store-testing/uploads/groups/";
    public static String user_videos="http://s3.ap-south-1.amazonaws.com/kotumb-store-testing/uploads/user_videos/";

    //prod
   public static String base_api = "https://kotumb.in/api/v3/";*/


    //Production Server APIservices/add_group_post/
    //public static String base_api = "https://www.kotumb.in/api/v3/";


    public static String base_api = "https://qa.kotumb.in/index.php/api/v3/";
    public static String share_post_link = "https://qa.kotumb.in/";
    public static String get_mediaLink = "https://s3.ap-south-1.amazonaws.com/kotumb-store-1/uploads/notifications/";
    public static String downloadPDF = "https://s3.ap-south-1.amazonaws.com/kotumb-store-1/uploads/messages/";
    public static String imageUrl = "https://s3.ap-south-1.amazonaws.com/kotumb-store-1/uploads/users/";
    public static String imageUrl_displayUsers="https://s3.ap-south-1.amazonaws.com/kotumb-store-1/uploads/display-users/";
    public static String imageAdUrl = "http://qa.kotumb.in/uploads/advertises/";
    public static String termsUrl = "https://qa.kotumb.in/welcome/tnc/";
    public static String privacyUrl = "https://qa.kotumb.in/welcome/privacy-policy/";
    public static String group_media="https://s3.ap-south-1.amazonaws.com/kotumb-store-1/uploads/groups/";
    public static String contactUsLink = "https://qa.kotumb.in/ajax/support/";
    public static String contactUsOpenChat = "https://qa.kotumb.in/ajax/singleQuery/";
    public static String defaultAdUrl = "https://qa.kotumb.in/info/advertise";
    public static String gallary_media="https://s3.ap-south-1.amazonaws.com/kotumb-store-1/uploads/display-users/";
    public static String user_videos="https://s3.ap-south-1.amazonaws.com/kotumb-store-1/uploads/user_videos/";

    public static final String GET_AD_FOR_NOTI = "adnoti";
    public static final String GET_MEMBERS = base_api + "services/groups-members/";
    public static final String DELETE_ACC = base_api + "services/delete_account/";
    public static final String MARK_AS_READ = base_api + "services/message_marked_as_read/";
    public static final String SKILL_DEV_LIKES = base_api + "services/skill_dev_likes/";
    public static final String VIDEO_UPLOAD=base_api+"services/upload_video/";
    public static final String VIDEO_DELETE=base_api+"services/delete_video/";
    public static final String MY_VIDEO_GET=base_api+"services/my_videos/";
    public static final String VIDEO_PUBLISH=base_api+"services/user_published_video/";
    public static final String VIDEO_LIMITS=base_api+"services/upload_video_limits";
    public static final String VIDEO_LIKES=base_api+"services/like_unlike_video/";
    public static final String VIDEO_LIKES_LIST=base_api+"services/video_likes/";
    public static final String USER_VIDEO_GET=base_api+"services/get_user_videos/";
    public static final String SKILL_DEV_ADD_COMMENT = base_api + "services/dpu_comment/";
    public static final String SKILL_DEV_LIKE_UNLIKE = base_api + "services/dpu_like_unlike/";
    public static final String SKILL_DEV_COMMENTS = base_api + "services/skill_dev_comments/";
    public static final String SKILL_DEV = base_api + "services/skill-devs/";
    public static final String REWARD_HISTORY = base_api + "services/award_points/";
    public static final String COMPLETE_SURVEY = base_api + "services/complete_survey/";
    public static final String MARK_NOTIFICATION_READ = base_api + "services/mark_notification_read/";
    public static final String ADD_TOKEN = base_api + "services/add_token/";
    public static String CHANGE_LANG = base_api + "services/change_language/";
    public static String SIGN_IN = base_api + "services/login";
    public static String SIGN_UP = base_api + "services/register";
    public static String VERIFY = base_api + "services/verify";
    public static String CHANGE_MOBILE = base_api + "services/change_mobile/";
    public static String SEND_OTP = base_api + "services/send_otp_only/";
    public static String RESEND_OTP = base_api + "services/send_otp";
    public static String RESET_PASSWORD = base_api + "services/reset_password";
    public static String CHANGE_LOGIN_INFO = base_api + "services/change_login_info";
    public static String PERSONAL_INFO = base_api + "services/personalinfo";
    public static String UPDATE_PERSONAL_INFO = base_api + "services/update_personalinfo";
    public static String WORK_EXPERIANCE = base_api + "services/workexperience";
    public static String ADHAR_VERIFICATION = base_api + "services/aadhar_verification";
    public static String EDUCATION = base_api + "services/education";
    public static String EDUCATION_UPDATE = base_api + "services/update_education";
    public static String EDUCATION_DELETE = base_api + "services/delete_education/";
    public static String PROFILE = base_api + "services/profile";
    public static String UPDATE_WORK_EXPERIANCE = base_api + "services/update_workexperience";
    public static String DELETE_ORGANISATION = base_api + "services/delete_experience/";
    public static String UPDATE_PROFILE_PIC = base_api + "services/upload";
    public static String FORGET_PASSWORD = base_api + "services/forget_password";
    public static String JOBROLES = base_api + "services/jobroles/";
    public static String ORGANIZATION = base_api + "services/organization/";
    public static String SCHOOLS = base_api + "services/schools/";
    public static String COURSES = base_api + "services/courses/";
    public static String ZIPCODES = base_api + "services/zipcodes/";
    public static String RESUME_TEMPLATE = base_api + "services/resume_templates";
    public static String UPDATE_RESUME=base_api+"services/update_resumeinfo/";
    public static String GET_RESUME_INFO=base_api+"services/get_resume_info/";
    public static String PROFILE_VIEWS = base_api + "services/profileviewers/";
    public static String PROFILE_VIEWS_UPDATE = base_api + "services/profileviewers_update/";
    public static String GET_SKILLS = base_api + "services/skills/";
    public static String GET_INDUSTRIES = base_api + "services/get_industries/";
    public static String GET_LANGUAGE = base_api + "services/all_languages/";
    public static String GET_APP_LANGUAGE = base_api + "services/get_languages/";
    public static String LOAD_GALLERY=base_api+"services/display_user_gallery/";
    public static String ADD_SKILLS = base_api + "services/add_skill_info";
    public static String ADD_DSKILLS = base_api + "services/add_skill_desired";
    public static String LOGOUT = base_api + "services/logout_remove_token";
    public static String ZIPCODE = base_api + "services/zipcode/";
    public static String ZIPCODE_CLICKED = base_api + "services/zipcode_click/";
    public static String SHARE_RESUME = base_api + "services/resume_pdf/";
    public static String SHARE_RESUME_MESSAGE = base_api + "services/share_resume_message";
    public static String SET_DEFAULT_RESUME = base_api + "services/set_default_template/";
    public static String SEARCH_USERS = base_api + "services/search/";
    public static String SEARCH_USERS_UPDATE = base_api + "services/search_update/";
    public static String SUGGESTION_USERS = base_api + "services/people_may_know/";
    public static String SUGGESTION_USERS_UPDATE = base_api + "services/people_may_know_update/";
    public static String NOTIFICATION = base_api + "services/notifications/";
    public static String NOTIFICATION_UPDATE = base_api + "services/notifications_update/";
    public static String CONNECT = base_api + "services/connect";
    public static String CONNECTECTIONS = base_api + "services/connections/";
    public static String CONNECTECTIONS_UPDATE = base_api + "services/connections_update/";
    public static String CONNECTECTIONS_SEARCH = base_api + "services/connections_search/";
    public static String CONNECTECTIONS_SEARCH_UPDATE = base_api + "services/connections_search_update/";
    public static String MESSAGES_LIST = base_api + "services/conversations/";
    public static String MESSAGES_LIST_UPDATE = base_api + "services/conversations_update/";
    public static String CHAT_LIST = base_api + "services/get_messages/";
    public static String DELETE_CONVERSATION = base_api + "services/delete_msgs/";
    public static String CHAT_LIST_UPDATE = base_api + "services/get_messages_update/";
    public static String SEND_MESSAGE = base_api + "services/send_message/";
    public static String MESSAGES_LIST_SEARCH = base_api + "services/conversations_search/";
    public static String MESSAGES_LIST_SEARCH_UPDATE = base_api + "services/conversations_search_update/";
    public static String INVITATIONS = base_api + "services/invitations/";
    public static String CHECKUSERNAME = base_api + "services/check_username/";
    public static String INVITATIONS_SENT = base_api + "services/invitations_sent/";
    public static String INVITATIONS_RECEIVED = base_api + "services/invitations_received/";
    public static String REMINDER_LIST = base_api + "services/note_list/";
    public static String INVITATIONS_SENT_UPDATE = base_api + "services/invitations_sent_update/";
    public static String INVITATIONS_RECEIVED_UPDATE = base_api + "services/invitations_received_update/";
    public static String INVITATIONS_SENT_UPDATE_ = base_api + "services/invitations_sent_update_/";
    public static String INVITATIONS_RECEIVED_UPDATE_ = base_api + "services/invitations_received_update_/";
    public static String CREATE_NOTE = base_api + "services/create_note/";
    public static String UPDATE_NOTE = base_api + "services/update_note";
    public static String HOME = base_api + "services/home/";
    public static String DELETE_NOTE = base_api + "services/delete_note/";
    public static String ASK_FOR_RECOM = base_api + "services/ask_for_recommendation";
    public static String RECOM = base_api + "services/recommend";
    public static String RECOM_STATUS_CHANGE = base_api + "services/recommendation_status_to_profile";
    public static String GET_ADD = base_api + "services/get_ad/";
    public static String GET_ADS = base_api + "services/get_ads/";
    public static String FAVOURITES_LINKS = base_api + "services/user_favourites/";
    public static String GOV_LINKS = base_api + "services/govt_links/";
    public static String FAVOURITES_LINKS_UPDATE = base_api + "services/user_favourites_update/";
    public static String GOV_LINKS_UPDATE = base_api + "services/govt_links_update/";
    public static String FEEDBACK = base_api + "services/feedback/";
    public static String CONTACT = base_api + "services/contact/";
    public static String ADD_FAV = base_api + "services/add_fav/";
    public static String FAVOURITES_LINKS_ADMIN = base_api + "services/favourites/";
    public static String FAVOURITES_LINKS_UPDATE_ADMIN = base_api + "services/favourites_update/";
    public static String DELETE_FAV = base_api + "services/delete_fav/";
    public static String CHANGE_PASSWORD = base_api + "services/change_password/";
    public static String JOINED_GROUPS = base_api + "services/joined_groups/";
    public static String JOINED_GROUPS_UPDATE = base_api + "services/joined_groups_update/";
    public static String PUBLIC_GROUPS = base_api + "services/groups/";
    public static String PUBLIC_GROUPS_UPDATE = base_api + "services/groups_update/";
    public static String GROUP_CATEGORIES = base_api + "services/group_categories/";
    public static String CREATE_GROUP = base_api + "services/create_group/";
    public static String GROUP_INFO = base_api + "services/group_info/";
    public static String EDIT_GROUP = base_api + "services/edit_group/";
    public static String FIND_MEMBERS_TO_JOIN_GROUP = base_api + "services/find_members_to_join_group/";
    public static String FIND_MEMBERS_TO_JOIN_GROUP_UPDATE = base_api + "services/find_members_to_join_group_update/";
    public static String GROUP_INVITE_MEMBER = base_api + "services/invite_member/";
    public static String GROUP_DELETE_MEMBER = base_api + "services/delete_group_member/";
    public static String GROUP_BLOCK_MEMBER = base_api + "services/block_group_member/";
    public static String GROUP_ACCEPT_MEMBER = base_api + "services/accept_group_member_request/";
    public static String GROUP_CHANGE_MEMBER = base_api + "services/group_member_action/";
    public static String GROUP_JOIN_REQUEST = base_api + "services/request_group_to_join/";
    public static String GROUP_ACCEPT_REQUEST = base_api + "services/accept_group_member_request/";
    public static String GROUP_POSTS = base_api + "services/group_posts/";
    public static String GROUP_LIKE_POST = base_api + "services/group_post_like/";
    public static String GROUP_UNLIKE_POST = base_api + "services/group_post_unlike/";
    public static String GROUP_SHARE_POST = base_api + "services/share_group_post/";
    public static String GROUP_ADD_POST = base_api + "services/add_group_post/";
    public static String GROUP_UPDATE_POST = base_api + "services/update_group_post/";
    public static String GROUP_ADD_COMMENT = base_api + "services/group_post_comment/";
    public static String GROUP_POST_COMMENTS = base_api + "services/group_post_comments/";
    public static String GROUP_POST_COMMENTS_UPDATE = base_api + "services/group_post_comments_update/";
    public static String GROUP_POST_LIKES = base_api + "services/group_post_likes/";
    public static String GROUP_POST_LIKES_UPDATE = base_api + "services/group_post_likes_update/";
    public static String GROUP_POST_DELETE = base_api + "services/delete_group_post/";
    public static String GROUP_POST_COMMENT_DELETE = base_api + "services/delete_group_post_comment/";
    public static String GROUP_POST_COMMENT_EDIT = base_api + "services/edit_comment/";
    public static String GET_COMMENT_REPLY = base_api + "services/load_comments_replies/";
    public static String GROUP_LATEST_POST = base_api + "services/group_latest_posts/";
    public static String CHECK_MOBILE = base_api + "services/check_mobile/";
    public static String DELETE_GROUP = base_api + "services/delete_group/";
    public static String SHARE_GROUP_POST = base_api + "services/share_group_post/";
    public static String GROUP_POST_REPORT = base_api + "services/group_post_report/";
    public static String REPORTED_GROUP_POST = base_api + "services/reported_group_posts/";
    public static String REPORTED_GROUP_POST_UPDATE = base_api + "services/reported_group_posts_update/";
    public static String REPORTED_GROUP_POST_ACTION = base_api + "services/group_post_report_action/";
    public static String REPORTED_GROUP_POST_COMMENT_ACTION = base_api + "services/group_post_report_comment_action/";
    public static String GROUP_USER_REPORT = base_api + "services/report_group_user/";
    public static String USER_REPORT = base_api + "services/report_user/";
    public static String GROUP_USER_REPORT_LIST = base_api + "services/group_user_reports/";
    public static String GROUP_USER_REPORT_LIST_UPDATE = base_api + "services/group_user_reports_update/";
    public static String GROUP_COMMENT_REPORT_LIST = base_api + "services/reported_group_post_comments/";
    public static String GROUP_COMMENT_REPORT_LIST_UPDATE = base_api + "services/reported_group_post_comments_update/";
    public static String GROUP_USER_REPORT_ACTION = base_api + "services/user_report_action/";
    public static String GROUP_POST_COMMENT_REPORT = base_api + "services/group_post_comment_report/";
    public static String GET_RSS_LIST = base_api + "services/rss/";
    public static String GET_RSS_LIST_UPDATE = base_api + "services/rss_update/";
    public static String REDEEEM_POINTS = base_api + "services/redeem_reward";
    public static String REFERREL_DETAILS = base_api + "services/referral_profile";

    public static String USER_REFER_REWARD = base_api + "services/user_referred_reward";
    public static String USER_EXPIRED_SUMMATION = base_api + "services/user_expired_summation";
    public static String POPUP_READ = base_api + "services/popupread/";

    private static OnResponseListener listener;



    public static void sendRequestToServerGET(final Context context, String url, final String input) {
        try {
            AQuery aq = new AQuery(context);
            //Log.d("Url ", url);
            if (url.equalsIgnoreCase(API.ORGANIZATION) || url.equalsIgnoreCase(API.SCHOOLS)
                    || url.equalsIgnoreCase(API.COURSES) || url.equalsIgnoreCase(API.JOBROLES)
                    || url.equalsIgnoreCase(API.GET_SKILLS) || url.equalsIgnoreCase(API.GET_INDUSTRIES ) || url.equalsIgnoreCase(API.GET_LANGUAGE)) {
                url = url + "?lang-code=" + SharedPreferencesMethod.getDefaultLanguage(context);
            }


            aq.ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {
                        //Log.e(context.getClass().getName(), json.toString());
                        if (json != null) {
                            //
                            json.put("RP_MESSAGE", "ALL_OKAY");
                            callApiCallback(context, input, json);
                            if (input.equalsIgnoreCase(GROUP_ACCEPT_MEMBER) && listener != null) {
                                listener.onGotResponse(json);
                            }
                        } else {
                            JSONObject output = new JSONObject();
                            if (status.getCode() == AjaxStatus.NETWORK_ERROR) {
                                output.put("RP_MESSAGE", "NO NETWORK");
                            } else {
                                output.put("RP_MESSAGE", "ERROR");
                            }
                            callApiCallback(context, input, output);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        callApiCallback(context, input, null);
                    }
                }
            }.method(AQuery.METHOD_GET).header("Auth-Key", "KOTUMB__AuTh_keY"));


        } catch (Exception e) {
            e.printStackTrace();
            callApiCallback(context, url, null);
        }
    }



    public static  void openOtherUserProfile(final String id, final Context context){
        final List<User> users = new ArrayList<>();
        User user = SharedPreferencesMethod.getUserInfo(context);
        System.out.println("User Info   "+user.getUserInfo());
        String url;
        url = API.PROFILE;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            System.out.println("Main Response  "+response);
                            JSONObject jsonResponse = new JSONObject(response).getJSONObject("profile");
                            JSONObject json2 = jsonResponse.getJSONObject("personal_info");
                            System.out.println("Json Response  "+ json2.toString());
                            User user = new User();
                            user.setFirstName(json2.getString("firstName"));
                            user.setMiddleName(json2.getString("middleName"));
                            user.setMiddleName(json2.getString("lastName"));
                            user.setAvatar(json2.getString("avatar"));
                            user.setUserId(json2.getString("userId"));
                            user.setUserInfo(json2.toString());
                            users.add(user);
                            Intent intent = new Intent(context, OtherUserProfileActivity.class);
                            intent.putExtra("USER", user);
                            System.out.println("User Object Data "+ user.getUserInfo());
                            context.startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  input = new HashMap<>();
                // the POST parameters:
                input.put(RequestParameters.VIEWERID, "" + SharedPreferencesMethod.getUserId(context));
                input.put(RequestParameters.USERID, "" + id);
                return input;
            }
        };
        Volley.newRequestQueue(context).add(postRequest);
    }





    public static void sendRequestToServerGET_FRAGMENT(final Context context, final Fragment fragment, String url, final String input) {
        Log.e("INVITE","Update input is : " + input);
        Log.e("URL","Update input is : " + url);
        try {
            AQuery aq = new AQuery(context);
            //Log.d("Url ", url);
            aq.ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {
                        Log.e(context.getClass().getName(), "" + json);
                        if (json != null) {
                            //Log.e(context.getClass().getName(), json.toString());
                            json.put("RP_MESSAGE", "ALL_OKAY");
                            callApiCallbackFragment(context,fragment, input, json);
                        } else {
                            JSONObject output = new JSONObject();
                            if (status.getCode() == AjaxStatus.NETWORK_ERROR) {
                                output.put("RP_MESSAGE", "NO NETWORK");
                            } else {
                                output.put("RP_MESSAGE", "ERROR");
                            }
                            callApiCallbackFragment(context,fragment, input, output);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        callApiCallbackFragment(context,fragment, input, null);
                    }
                }
            }.method(AQuery.METHOD_GET).header("Auth-Key", "KOTUMB__AuTh_keY"));


        } catch (Exception e) {
            e.printStackTrace();
            callApiCallback(context, url, null);
        }
    }

    public static void sendRequestToServerPOST(final Context context, String url, JSONObject input) {
        try {
            AQuery aq = new AQuery(context);
            Log.d("input ", input.toString());
            Log.d("Url ", url);
            aq.post(url, input, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {
                        if (json != null) {
                            Log.d("API_OUTPUT", json.toString());
                            json.put("RP_MESSAGE", "ALL_OKAY");
                            callApiCallback(context, url, json);
                        } else {
                            JSONObject output = new JSONObject();
                            if (status.getCode() == AjaxStatus.NETWORK_ERROR) {
                                output.put("RP_MESSAGE", "NO NETWORK");
                            } else {
                                output.put("RP_MESSAGE", "ERROR");
                            }
                            callApiCallback(context, url, output);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        callApiCallback(context, url, null);
                    }
                }
            }.header("Auth-Key", "KOTUMB__AuTh_keY"));

        } catch (Exception e) {
            e.printStackTrace();
            callApiCallback(context, url, null);
        }
    }

    public static void sendRequestToServerPOST_PARAM(final Context context, String url, HashMap<String, Object> input) {
        try {
            AQuery aq = new AQuery(context);
            //Log.d("Url ", url);
            aq.ajax(url, input, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {

                        Log.d("API_OUTPUT", "" + json);

                        if (json != null) {

                            json.put("RP_MESSAGE", "ALL_OKAY");
                            callApiCallback(context, url, json);
                        } else {
                            JSONObject output = new JSONObject();
                            if (status.getCode() == AjaxStatus.NETWORK_ERROR) {
                                output.put("RP_MESSAGE", "NO NETWORK");
                            } else {
                                Toast.makeText(context,"Second",Toast.LENGTH_SHORT).show();
                                output.put("RP_MESSAGE", "ERROR");
                            }
                            callApiCallback(context, url, output);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        callApiCallback(context, url, null);
                    }
                }
            }.header("Auth-Key", "KOTUMB__AuTh_keY"));

        } catch (Exception e) {
            e.printStackTrace();
            callApiCallback(context, url, null);
        }
    }

    public static void sendRequestToServerPOST_PARAM_FRAGMENT(final Context context, final BaseFragment fragment, String url, HashMap<String, Object> input) {
        try {
            AQuery aq = new AQuery(context);
            Log.d("API ", url+" INPUT = "+input);
            aq.ajax(url, input, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {
                        if (json != null) {
                            Log.d("API_OUTPUT", json.toString());
                            json.put("RP_MESSAGE", "ALL_OKAY");
                            callApiCallbackFragment(context,fragment, url, json);
                        } else {
                            JSONObject output = new JSONObject();
                            if (status.getCode() == AjaxStatus.NETWORK_ERROR) {
                                output.put("RP_MESSAGE", "NO NETWORK");
                            } else {
                                output.put("RP_MESSAGE", "ERROR");
                            }
                            callApiCallbackFragment(context,fragment, url, output);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        callApiCallbackFragment(context,fragment, url, null);
                    }
                }
            }.header("Auth-Key", "KOTUMB__AuTh_keY"));

        } catch (Exception e) {
            e.printStackTrace();
            callApiCallback(context, url, null);
        }
    }

    public static void sendRequestToServerPOST_SECURE(final Context context, String url, JSONObject input) {
        try {
            AQuery aq = new AQuery(context);
            Log.d("Url ", url);
            aq.post(url, input, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {

                        // Log.e("Header_token", SharedPreferencesMethod.getString(context, SharedPreferencesMethod.USER_TOKEN) +" "+status.getError()+" "+status.getMessage());
                        if (json != null) {
                            Log.d("API_OUTPUT", json.toString());
                            json.put("RP_MESSAGE", "ALL_OKAY");
                            callApiCallback(context, url, json);
                        } else {
                            JSONObject output = new JSONObject();
                            if (status.getCode() == AjaxStatus.NETWORK_ERROR) {
                                output.put("RP_MESSAGE", "NO NETWORK");
                            } else {
                                output.put("RP_MESSAGE", "ERROR");
                            }
                            callApiCallback(context, url, output);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        callApiCallback(context, url, null);
                    }
                }
            }.method(AQuery.METHOD_POST).header("Auth-Key", "KOTUMB__AuTh_keY"));

        } catch (Exception e) {
            e.printStackTrace();
            callApiCallback(context, url, null);
        }
    }

    public static void callApiCallback(Context context, String api, JSONObject output) {

        Log.e(TAG, "callApiCallback: context"+context+"\n"+"API ="+api+"\n"+"OUTPUT"+output.toString() );

        if (api.equals(SIGN_IN)) {
            ((LoginActivity) context).getResponse(output, 0);
        } else if (api.equals(SIGN_UP)) {
            ((SignupActivity) context).getResponse(output, 0);
        } else if (api.equals(CHECK_MOBILE)) {
            ((SignupActivity) context).getResponse(output, 4);
        } else if (api.equals(VERIFY)) {
            if (context instanceof LoginActivity) {
                ((LoginActivity) context).getResponse(output, 1);
            } else if (context instanceof SignupActivity) {
                ((SignupActivity) context).getResponse(output, 1);
            }
        } else if (api.equals(RESEND_OTP)) {
            if (context instanceof LoginActivity) {
                ((LoginActivity) context).getResponse(output, 2);
            } else if (context instanceof SignupActivity) {
                ((SignupActivity) context).getResponse(output, 2);
            }
        } else if (api.equals(SEND_OTP)) {
            if (context instanceof HomeScreenActivity) {
                ((HomeScreenActivity) context).getResponse(output, 2);
            } else if (context instanceof CompleteProfileActivity) {
                ((CompleteProfileActivity) context).getResponse(output, 8);
            } else if (context instanceof ForgetPasswordActivity) {
                ((ForgetPasswordActivity) context).getResponse(output, 2);
            }
        } else if (api.equals(CHANGE_MOBILE)) {
            if (context instanceof HomeScreenActivity) {
                ((HomeScreenActivity) context).getResponse(output, 3);
            } else if (context instanceof CompleteProfileActivity) {
                ((CompleteProfileActivity) context).getResponse(output, 9);
            }
        } else if (api.equals(CHANGE_LOGIN_INFO)) {
            ((UpdateLoginInfoActivity) context).getResponse(output, 0);
        } else if (api.equals(CHANGE_PASSWORD)) {
            ((UpdatePasswordActivity) context).getResponse(output, 0);
        } else if (api.equals(PERSONAL_INFO)) {
            ((MyProfileActivity) context).getResponse(output, 0);
        } else if (api.equals(UPDATE_PERSONAL_INFO)) {
            ((CompleteProfileActivity) context).getResponse(output, 2);
        } else if (api.equals(WORK_EXPERIANCE) || api.equals(UPDATE_WORK_EXPERIANCE)) {
            ((ProfessionActivity) context).getResponse(output, 0);
        } else if (api.equals(ADHAR_VERIFICATION)) {
            ((CompleteProfileActivity) context).getResponse(output, 1);
        } else if (api.equals(EDUCATION) || api.equals(EDUCATION_UPDATE)) {
            ((EducationActivity) context).getResponse(output, 0);
        } else if (api.equals(UPDATE_PROFILE_PIC)) {
            ((CompleteProfileActivity) context).getResponse(output, 0);
        } else if (api.equals(FORGET_PASSWORD)) {
            ((ForgetPasswordActivity) context).getResponse(output, 0);
        } else if (api.equals(RESET_PASSWORD)) {
            ((ForgetPasswordActivity) context).getResponse(output, 1);
        } else if (api.equals(JOBROLES)) {
            if (context instanceof ProfessionActivity)
                ((ProfessionActivity) context).getResponse(output, 1);
            if (context instanceof GetPreLoadDataService)
                ((GetPreLoadDataService) context).getResponse(output, 3);
        } else if (api.equals(ORGANIZATION)) {
            if (context instanceof ProfessionActivity)
                ((ProfessionActivity) context).getResponse(output, 2);
            if (context instanceof GetPreLoadDataService)
                ((GetPreLoadDataService) context).getResponse(output, 0);
        } else if (api.equals(COURSES)) {
            if (context instanceof EducationActivity)
                ((EducationActivity) context).getResponse(output, 1);
            if (context instanceof GetPreLoadDataService)
                ((GetPreLoadDataService) context).getResponse(output, 2);
        } else if (api.equals(SCHOOLS)) {
            if (context instanceof EducationActivity)
                ((EducationActivity) context).getResponse(output, 2);
            if (context instanceof GetPreLoadDataService)
                ((GetPreLoadDataService) context).getResponse(output, 1);
        } else if (api.equals(ZIPCODES)) {
            if (context instanceof MyProfileActivity)
                ((MyProfileActivity) context).getResponse(output, 1);
            if (context instanceof CompleteProfileActivity)
                ((CompleteProfileActivity) context).getResponse(output, 3);

        } else if (api.equals(PROFILE_VIEWS)) {
            ((WhoViewActivity) context).getResponse(output, 0);
        } else if (api.equals(PROFILE_VIEWS_UPDATE)) {
            ((WhoViewActivity) context).getResponse(output, 1);
        } else if (api.equals(SEARCH_USERS)) {
            ((SearchActivity) context).getResponse(output, 0);
        } else if (api.equals(SEARCH_USERS_UPDATE)) {
            ((SearchActivity) context).getResponse(output, 1);
        } else if (api.equals(GET_SKILLS)) {
            if (context instanceof CompleteProfileActivity)
                ((CompleteProfileActivity) context).getResponse(output, 4);
            if (context instanceof GetPreLoadDataService)
                ((GetPreLoadDataService) context).getResponse(output, 4);
        } else if (api.equals(GET_INDUSTRIES)) {
            if (context instanceof CompleteProfileActivity)
                ((CompleteProfileActivity) context).getResponse(output, 40);
            if (context instanceof GetPreLoadDataService)
                ((GetPreLoadDataService) context).getResponse(output, 40);
        } else if (api.equals(ADD_SKILLS)) {
            ((CompleteProfileActivity) context).getResponse(output, 5);
        } else if (api.equals(ADD_DSKILLS)) {
            ((CompleteProfileActivity) context).getResponse(output, 11);
        } else if (api.equals(LOGOUT)) {
            if (context instanceof HomeScreenActivity) {
                ((HomeScreenActivity) context).getResponse(output, 0);
            } else if (context instanceof CompleteProfileActivity) {
                ((CompleteProfileActivity) context).getResponse(output, 20);
            } else if (context instanceof HomeScreenActivity) {
                ((HomeScreenActivity) context).getResponse(output, 20);
            }
        } else if (api.equals(ZIPCODE)) {
            if (context instanceof CompleteProfileActivity)
                ((CompleteProfileActivity) context).getResponse(output, 6);
            if (context instanceof MyProfileActivity)
                ((MyProfileActivity) context).getResponse(output, 2);

        } else if (api.equals(ZIPCODE_CLICKED)) {
            if (context instanceof CompleteProfileActivity)
                ((CompleteProfileActivity) context).getResponse(output, 12);
            if (context instanceof MyProfileActivity)
                ((MyProfileActivity) context).getResponse(output, 4);

        } else if (api.equals(SHARE_RESUME)) {
            ((ShareResumeActivity) context).getResponse(output, 0);
            SHARE_RESUME = base_api + "services/resume_pdf/";
        } else if (api.equals(PROFILE)) {
            if (context instanceof CompleteProfileActivity)
                ((CompleteProfileActivity) context).getResponse(output, 7);
            else if (context instanceof OtherUserProfileActivity)
                ((OtherUserProfileActivity) context).getResponse(output, 0);
        } else if (api.equals(CONNECTECTIONS)) {
            if (context instanceof ConnectionsActivity) {
                ((ConnectionsActivity) context).getResponse(output, 0);
            } else if (context instanceof ConnectionsMessageActivity) {
                ((ConnectionsMessageActivity) context).getResponse(output, 0);
            }
        } else if (api.equals(CONNECTECTIONS_UPDATE)) {
            if (context instanceof ConnectionsActivity) {
                ((ConnectionsActivity) context).getResponse(output, 1);
            } else if (context instanceof ConnectionsMessageActivity) {
                ((ConnectionsMessageActivity) context).getResponse(output, 1);
            }
        } else if (api.equals(MESSAGES_LIST)) {
            ((MessageActivity) context).getResponse(output, 0);
        } else if (api.equals(MESSAGES_LIST_UPDATE)) {
            ((MessageActivity) context).getResponse(output, 1);
        } else if (api.equals(MESSAGES_LIST_SEARCH)) {
            ((MessageActivity) context).getResponse(output, 2);
        } else if (api.equals(MESSAGES_LIST_SEARCH_UPDATE)) {
            ((MessageActivity) context).getResponse(output, 3);
        } else if (api.equals(CONNECTECTIONS_SEARCH)) {
            ((ConnectionsMessageActivity) context).getResponse(output, 2);
        } else if (api.equals(CONNECTECTIONS_SEARCH_UPDATE)) {
            ((ConnectionsMessageActivity) context).getResponse(output, 3);
        } else if (api.equals(CHAT_LIST)) {
            ((ChatActivity) context).getResponse(output, 0);
        } else if (api.equals(CHAT_LIST_UPDATE)) {
            ((ChatActivity) context).getResponse(output, 1);
        } else if (api.equals(SEND_MESSAGE)) {
            ((ChatActivity) context).getResponse(output, 2);
        } else if (api.equals(DELETE_CONVERSATION)) {
            ((ChatActivity) context).getResponse(output, 3);
        } else if (api.equals(DELETE_CONVERSATION + 2)) {
            ((ChatActivity) context).getResponse(output, 6);
        } else if (api.equals(SUGGESTION_USERS)) {
            ((ConnectionsActivity) context).getResponse(output, 0);
        } else if (api.equals(SUGGESTION_USERS_UPDATE)) {
            ((ConnectionsActivity) context).getResponse(output, 1);
        } else if (api.equals(CHECKUSERNAME)) {
            if (context instanceof SignupActivity)
                ((SignupActivity) context).getResponse(output, 3);
            else if (context instanceof UpdateLoginInfoActivity)
                ((UpdateLoginInfoActivity) context).getResponse(output, 1);
        } else if (api.equals(HOME)) {
            ((HomeScreenActivity) context).getResponse(output, 1);
        } else if (api.equals(SET_DEFAULT_RESUME)) {
            ((ResumeTemplateActivity) context).getResponse(output, 0);
        } else if (api.equals(CREATE_NOTE) || api.equals(UPDATE_NOTE)) {
            ((AddNoteActivity) context).getResponse(output, 0);
        } else if (api.equals(REMINDER_LIST)) {
            ((NotepadActivity) context).getResponse(output, 0);
        } else if (api.equals(NOTIFICATION)) {
            ((NotificationActivity) context).getResponse(output, 0);
        } else if (api.equals(FAVOURITES_LINKS) || api.equals(GOV_LINKS)) {
            ((LinksActivity) context).getResponse(output, 0);
        } else if (api.equals(FAVOURITES_LINKS_UPDATE) || api.equals(GOV_LINKS_UPDATE)) {
            ((LinksActivity) context).getResponse(output, 1);
        } else if (api.equals(GET_ADD)) {
            ((GetAddService) context).getResponse(output, 0);
        } else if (api.equals(SHARE_RESUME_MESSAGE)) {
            ((ShareResumeActivity) context).getResponse(output, 1);
        } else if (api.equals(GET_LANGUAGE)) {
            if (context instanceof GetPreLoadDataService)
                ((GetPreLoadDataService) context).getResponse(output, 5);
            else if (context instanceof CompleteProfileActivity)
                ((CompleteProfileActivity) context).getResponse(output, 10);
            else if (context instanceof MyProfileActivity)
                ((MyProfileActivity) context).getResponse(output, 3);
        } else if (api.equals(FEEDBACK) || api.equals(CONTACT)) {
            ((ContactFeedbackActivity) context).getResponse(output, 0);
        } else if (api.equals(GROUP_CATEGORIES)) {
            ((CreateGroupActivity) context).getResponse(output, 0);
        } else if (api.equals(CREATE_GROUP)) {
            ((CreateGroupActivity) context).getResponse(output, 1);
        } else if (api.equals(GROUP_INFO)) {
            ((GroupPostActivity) context).getResponse(output, 0);
        } else if (api.equals(EDIT_GROUP)) {
            if (context instanceof CreateGroupActivity) {
                ((CreateGroupActivity) context).getResponse(output, 1);
            } else if (context instanceof GroupInfoActivity) {
                ((GroupInfoActivity) context).getResponse(output, 0);
            }
        } else if (api.equals(FIND_MEMBERS_TO_JOIN_GROUP)) {
            ((GroupInviteActivity) context).getResponse(output, 0);
        } else if (api.equals(FIND_MEMBERS_TO_JOIN_GROUP_UPDATE)) {
            ((GroupInviteActivity) context).getResponse(output, 1);
        } else if (api.equals(GROUP_USER_REPORT_LIST)) {
            ((ReportedUserActivity) context).getResponse(output, 0);
        } else if (api.equals(GROUP_USER_REPORT_LIST_UPDATE)) {
            ((ReportedUserActivity) context).getResponse(output, 1);
        } else if (api.equals(GROUP_COMMENT_REPORT_LIST)) {
            ((ReportedCommentActivity) context).getResponse(output, 0);
        } else if (api.equals(GROUP_COMMENT_REPORT_LIST_UPDATE)) {
            ((ReportedCommentActivity) context).getResponse(output, 1);
        } else if (api.equals(GROUP_INVITE_MEMBER)) {
            ((GroupInviteActivity) context).getResponse(output, 2);
        } else if (api.equals(GROUP_POSTS)) {
            ((GroupPostActivity) context).getResponse(output, 1);
        } else if (api.equals(GROUP_ADD_POST) || api.equals(GROUP_UPDATE_POST)) {
            ((CreatePostActivity) context).getResponse(output, 0);
        } else if (api.equals(GROUP_POST_COMMENTS)) {
            if (context instanceof GroupPostActivity)
                ((GroupPostActivity) context).getResponse(output, 2);
            else if (context instanceof PostDetailsActivity)
                ((PostDetailsActivity) context).getResponse(output, 2);
        } else if (api.equals(GROUP_POST_COMMENTS_UPDATE)) {
            if (context instanceof GroupPostActivity)
                ((GroupPostActivity) context).getResponse(output, 5);
            else if (context instanceof PostDetailsActivity)
                ((PostDetailsActivity) context).getResponse(output, 5);
        } else if (api.equals(GROUP_LATEST_POST)) {
            ((GroupPostActivity) context).getResponse(output, 3);
        } else if (api.equals(GROUP_POST_LIKES)) {
            ((PostLikesActivity) context).getResponse(output, 0);
        } else if (api.equals(GROUP_POST_LIKES_UPDATE)) {
            ((PostLikesActivity) context).getResponse(output, 1);
        } else if (api.equals(DELETE_GROUP) || api.equals("FINISH")) { // for closing screen
            if (context instanceof GroupPostActivity)
                ((GroupPostActivity) context).getResponse(output, 4);
        } else if (api.equals(JOINED_GROUPS)) {
            ((JoinedGroupListActivity) context).getResponse(output, 0);
        } else if (api.equals(JOINED_GROUPS_UPDATE)) {
            ((JoinedGroupListActivity) context).getResponse(output, 1);
        } else if (api.equals(REPORTED_GROUP_POST)) {
            ((ReportedPostActivity) context).getResponse(output, 0);
        } else if (api.equals(REPORTED_GROUP_POST_UPDATE)) {
            ((ReportedPostActivity) context).getResponse(output, 1);
        } else if (api.equals(SKILL_DEV_COMMENTS)) {
            ((OrganizationActivity) context).getResponse(output, 1);
        } else if (api.equals(SKILL_DEV_COMMENTS + 1)) {
            ((OrganizationActivity) context).getResponse(output, 3);
        } else if (api.equals(SKILL_DEV_LIKE_UNLIKE)) {
            ((OrganizationActivity) context).getResponse(output, 0);
        } else if (api.equals(SKILL_DEV_ADD_COMMENT)) {
            ((OrganizationActivity) context).getResponse(output, 2);
        } else if (api.equals(SKILL_DEV)) {
            ((SearchActivityDisplayUser) context).getResponse(output, 0);
        } else if (api.equals(SKILL_DEV + 1)) {
            ((SearchActivityDisplayUser) context).getResponse(output, 1);
        } else if (api.equals(SKILL_DEV_LIKES)) {
            ((OrganizationActivity) context).getResponse(output, 4);
        } else if (api.equals(SKILL_DEV_LIKES + 1)) {
            ((OrganizationActivity) context).getResponse(output, 5);
        } else if (api.equals(SKILL_DEV + 2)) {
            ((OrganizationActivity) context).getResponse(output, 6);
        } else if (api.equals(CONNECTECTIONS + 2)) {
            ((SearchConnectionActivtiy) context).getResponse(output, 0);
        } else if (api.equals(CONNECTECTIONS_UPDATE + 2)) {
            ((SearchConnectionActivtiy) context).getResponse(output, 1);
        } else if (api.equals(GROUP_POSTS + 2)) {
            ((PostDetailsActivity) context).getResponse(output, 10);
        } else if (api.equals(LOGOUT)) {
            ((CompleteProfileActivity) context).getResponse(output, 20);
        } else if (api.contains(DELETE_ACC)) {
            ((HomeScreenActivity) context).getResponse(output, 30);
        } else if (api.contains(PROFILE)) {
            ((OtherUserProfileActivity) context).getResponse(output, 0);
        } else if (api.equalsIgnoreCase(REWARD_HISTORY)) {
            ((RewardHistoryActivty) context).getResponse(output, 0);
        } else if (api.equalsIgnoreCase(REWARD_HISTORY + 1)) {
            ((RewardHistoryActivty) context).getResponse(output, 1);
        } else if (api.equalsIgnoreCase(GROUP_JOIN_REQUEST)) {
            ((GroupInfoActivity) context).getResponse(output, 1);
        } else if (api.equalsIgnoreCase(GROUP_DELETE_MEMBER)) {
            ((GroupInfoActivity) context).getResponse(output, 2);
        } else if (api.equalsIgnoreCase(COMPLETE_SURVEY)) {
            if (context instanceof UnomerActivity){
                ((UnomerActivity) context).getResponse(output, 0);
            }else if (context instanceof HomeScreenActivity){
                ((HomeScreenActivity) context).getResponse(output, 12);
            }
        } else if (api.equalsIgnoreCase(MY_VIDEO_GET)) {
            ((UserVideoListActivity) context).getResponse(output, 0);
        }else if (api.equalsIgnoreCase(VIDEO_LIMITS)) {
            if(context instanceof ShowVideos){
                ((ShowVideos)context).getResponse(output,3);
            }
            else
                ((UploadVideoActivity) context).getResponse(output, 3);
        }else if (api.equalsIgnoreCase(USER_VIDEO_GET)) {
            ((UserVideoListActivity) context).getResponse(output, 1);
        }else if (api.equalsIgnoreCase(VIDEO_LIKES_LIST)) {
            ((VideoLikesListActivity) context).getResponse(output, 1);
        }else if (api.equalsIgnoreCase(VIDEO_PUBLISH)) {
            ((UserVideoListActivity) context).getResponse(output, 4);
        }else if (api.equalsIgnoreCase(VIDEO_LIKES)) {
            if (context instanceof UserVideoListActivity){
                ((UserVideoListActivity) context).getResponse(output, 2);
            }else if (context instanceof VideoDetailsActivity){
                ((VideoDetailsActivity) context).getResponse(output, 0);
            }
        }else if (api.contains(REFERREL_DETAILS)) {
            ((InvitationDetails) context).getResponse(output);
        }
    }

    public static void callApiCallbackFragment(Context context,Fragment fragment, String api, JSONObject output) {
        Log.e("API","Fragment is : " +fragment.toString());
        if (api.equals(PROFILE)) {
            ((MeFragment) fragment).getResponse(output, 0);
        } else if (api.equals(CONNECTECTIONS)) {
            ((ConnectionFragment) fragment).getResponse(output, 0);
        } else if (api.equals(CONNECTECTIONS_UPDATE)) {
            ((ConnectionFragment) fragment).getResponse(output, 1);
        } else if (api.equals(INVITATIONS_SENT)) {
            ((InviteFragment.PlaceholderFragment) fragment).getResponse(output, 1);
        } else if (api.equals(INVITATIONS_RECEIVED)) {
            ((InviteFragment.PlaceholderFragment) fragment).getResponse(output, 0);
        } else if (api.equals(INVITATIONS_SENT_UPDATE)) {
            ((InviteFragment.PlaceholderFragment) fragment).getResponse(output, 2);
        } else if (api.equals(INVITATIONS_RECEIVED_UPDATE)) {
            ((InviteFragment.PlaceholderFragment) fragment).getResponse(output, 3);
        } else if (api.equals(INVITATIONS_SENT_UPDATE_)) {
            ((InviteFragment.PlaceholderFragment) fragment).getResponse(output, 4);
        } else if (api.equals(INVITATIONS_RECEIVED_UPDATE_)) {
            ((InviteFragment.PlaceholderFragment) fragment).getResponse(output, 5);
        } else if (api.equals(SUGGESTION_USERS)) {
            if (fragment instanceof HomeFragment)
                ((HomeFragment) fragment).getResponse(output, 0);
            if (fragment instanceof InviteFragment.PlaceholderFragment)
                ((InviteFragment.PlaceholderFragment) fragment).getResponse(output, 6);
        } else if (api.equals(SUGGESTION_USERS_UPDATE)) {
            ((InviteFragment.PlaceholderFragment) fragment).getResponse(output, 7);
        } else if (api.equals(NOTIFICATION)) {
            ((NotificationFragment) fragment).getResponse(output, 0);
        } else if (api.equals(NOTIFICATION_UPDATE)) {
            ((NotificationFragment) fragment).getResponse(output, 1);
        } else if (api.equals(FAVOURITES_LINKS)) {
            ((FavrtLinksFragment.ListLinkFragment) fragment).getResponse(output, 0);
        } else if (api.equals(FAVOURITES_LINKS_UPDATE)) {
            ((FavrtLinksFragment.ListLinkFragment) fragment).getResponse(output, 1);
        } else if (api.equals(FAVOURITES_LINKS_ADMIN)) {
            ((FavrtLinksFragment.FavLinkFragment) fragment).getResponse(output, 0);
        } else if (api.equals(FAVOURITES_LINKS_UPDATE_ADMIN)) {
            ((FavrtLinksFragment.FavLinkFragment) fragment).getResponse(output, 1);
        } else if (api.equals(ADD_FAV)) {
            ((FavrtLinksFragment.ListLinkFragment) fragment).getResponse(output, 2);
        } else if (api.equals(GOV_LINKS)) {
            ((LinksFragment) fragment).getResponse(output, 0);
        } else if (api.equals(GOV_LINKS_UPDATE)) {
            ((LinksFragment) fragment).getResponse(output, 1);
        } else if (api.equals(GET_RSS_LIST)) {
            ((LinksFragment) fragment).getResponse(output, 0);
        } else if (api.equals(GET_RSS_LIST_UPDATE)) {
            ((LinksFragment) fragment).getResponse(output, 1);
        } else if (api.equals(REMINDER_LIST)) {
            ((NotepadFragment) fragment).getResponse(output, 0);
        } else if (api.equals(JOINED_GROUPS)) {
            ((GroupFragment.PlaceholderFragment) fragment).getResponse(output, 0);
        } else if (api.equals(PUBLIC_GROUPS)) {
            ((GroupFragment.PlaceholderFragment) fragment).getResponse(output, 1);
        } else if (api.equals(PUBLIC_GROUPS_UPDATE)) {
            ((GroupFragment.PlaceholderFragment) fragment).getResponse(output, 2);
        } else if (api.equals(SKILL_DEV)) {
            ((SkillDevFragment) fragment).getResponse(output, 0);
        } else if (api.equals(SKILL_DEV + 1)) {
            ((SkillDevFragment) fragment).getResponse(output, 1);
        } else if (api.contains(GET_AD_FOR_NOTI)) {
            ((NotificationFragment) fragment).getResponse(output, 100);
        } else if (api.equalsIgnoreCase(GET_MEMBERS)) {
            ((MembersFragment) fragment).getResponse(output, 0);
        }else if (api.contains(REDEEEM_POINTS)) {
            if (fragment instanceof MeFragment) {
                ((MeFragment) fragment).getResponse(output, 1);
            } else if (fragment instanceof CurrentRewardFragment) {
                ((CurrentRewardFragment) fragment).getResponse(output);
            } else if (fragment instanceof RedeemRewardFragment) {
                ((RedeemRewardFragment) fragment).getResponse(output);
            }
            else if (fragment instanceof ExpiredRewardFragment) {
                ((ExpiredRewardFragment) fragment).getResponse(output);
            }

        }else if (api.contains(USER_REFER_REWARD)) {
            if (fragment instanceof MeFragment)
                ((MeFragment) fragment).getResponse(output,2);

        }else if (api.contains(USER_EXPIRED_SUMMATION)) {
            ((MeFragment) fragment).getResponse(output,3);
        }else if(api.contains(REWARD_HISTORY)) {
            if (fragment instanceof HomeFragment)
                ((HomeFragment) fragment).getResponse(output, 1);
        }else if(api.contains(POPUP_READ)){
            if (fragment instanceof HomeFragment)
                ((HomeFragment) fragment).getResponse(output, 2);
        }
    }


    public static void setResponseListener(OnResponseListener listener) {
        API.listener = listener;
    }
}
