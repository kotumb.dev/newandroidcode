package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.CompleteProfileActivity;
import com.codeholic.kotumb.app.Activity.OtherUserProfileActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Recommendations;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import java.util.HashMap;
import java.util.List;


public class RecomRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Recommendations> recommendationsList;
    Activity activity;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;

    public void setShowSwitch(boolean showSwitch) {
        this.showSwitch = showSwitch;
    }

    private boolean showSwitch = false;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;


    public RecomRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<Recommendations> recommendationsList) {
        this.recommendationsList = recommendationsList;
        this.activity = activity;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return recommendationsList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return recommendationsList.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recom_list_item, parent, false);
            UserViewHolder UserViewHolder = new UserViewHolder(view);
            return UserViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {
        User user = recommendationsList.get(position).getUser();

        if (viewholder instanceof UserViewHolder) {
            UserViewHolder holder = (UserViewHolder) viewholder;
            holder.tvUserName.setText("" + user.getFirstName() + " " + user.getLastName());
            holder.tvNotification.setText(recommendationsList.get(position).getDescription());
            if (position < recommendationsList.size() - 1) {
                holder.line.setVisibility(View.VISIBLE);
            } else {
                holder.line.setVisibility(View.GONE);
            }

            if (showSwitch) {
                holder.sVisible.setVisibility(View.VISIBLE);
                if (recommendationsList.get(position).getStatus().equalsIgnoreCase("1")) {
                    holder.sVisible.setChecked(true);
                } else {
                    holder.sVisible.setChecked(false);
                }
            } else {
                holder.sVisible.setVisibility(View.GONE);
            }
            new AQuery(activity).id(holder.ivUserImage).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.ic_user);
            setOnClickListener(holder, recommendationsList.get(position), position);

        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    public void setOnClickListener(final UserViewHolder holder, final Recommendations recommendations, final int position) {
       /* holder.ivSelectedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });*/
        final User user = recommendations.getUser();

        holder.sVisible.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("checked", "" + isChecked);
                if (!Utility.isConnectingToInternet(activity)) { // check net connection
                    holder.sVisible.setChecked(!isChecked);
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity,activity. getResources().getString(R.string.app_no_internet_error));
                    return;
                }

                HashMap<String, Object> input = new HashMap<>();
                input.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(activity));
                input.put(RequestParameters.RECOMMEND_ID, "" + recommendations.getRecomId());
                if (isChecked) {
                    input.put(RequestParameters.STATUS, "1");
                } else {
                    input.put(RequestParameters.STATUS, "0");
                }

                recommendationsList.get(position).setStatus(""+input.get(RequestParameters.STATUS));
                //notifyItemChanged(position);
                Log.e("request", "" + input);
                API.sendRequestToServerPOST_PARAM(activity, API.RECOM_STATUS_CHANGE, input);
            }
        });
        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(user.getUserId().trim())) {
                    Intent intent = new Intent(activity, OtherUserProfileActivity.class);
                    intent.putExtra("USER", user);
                    activity.startActivity(intent);
                } else {
                    Intent intent = new Intent(activity, CompleteProfileActivity.class);
                    intent.putExtra(ResponseParameters.Id, R.id.ll_personal_details);
                    //intent.putExtra("DATA", new JSONObject().toString());
                    activity.startActivity(intent);
                }

            }
        });


    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvNotification;
        ImageView ivUserImage;
        CardView cvContainer;
        Switch sVisible;
        View line;

        UserViewHolder(View view) {
            super(view);
            tvUserName = (TextView) view.findViewById(R.id.tvUserName);
            tvNotification = (TextView) view.findViewById(R.id.tvNotification);
            ivUserImage = (ImageView) view.findViewById(R.id.ivUserImage);
            cvContainer = (CardView) view.findViewById(R.id.cvContainer);
            sVisible = (Switch) view.findViewById(R.id.sVisible);
            line = view.findViewById(R.id.view);
        }
    }
}