package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.OtherUserProfileActivity;
import com.codeholic.kotumb.app.Activity.ReportedUserActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.ReportedUser;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import java.util.List;


public class UserReportedListRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ReportedUser> users;
    Activity activity;
    AlertDialog confirmAlert;
    User user;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;
    String BASE_URL = "";

    public void setBASE_URL(String BASE_URL) {
        this.BASE_URL = BASE_URL;
    }

    private boolean suggestion = false;

    public UserReportedListRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<ReportedUser> users, User user) {
        this.users = users;
        this.user=user;
        this.activity = activity;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return users.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.reported_user_item, parent, false);
            UserViewHolder UserViewHolder = new UserViewHolder(view);
            return UserViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {

            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);

        }

        return null;
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {
        ReportedUser user = users.get(position);

        if (viewholder instanceof UserViewHolder) {
            UserViewHolder holder = (UserViewHolder) viewholder;
            holder.tvUserFromName.setText("" + user.fromName);
            holder.tvUserToName.setText("" + user.toName);
            holder.tvPostContent.setText(user.reason);
            new AQuery(activity).id(holder.ivUserFromImage).image(BASE_URL + user.fromAvatar, true, true, 300, R.drawable.ic_user);
            new AQuery(activity).id(holder.ivUserToImage).image(BASE_URL + user.toAvatar, true, true, 300, R.drawable.ic_user);
            setOnClickListener(holder, user, position);
            openProfileVolley(holder,user.toUserId);
            openProfile(holder.tvUserFromName);
        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }


    private void openProfile(TextView textView){
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, OtherUserProfileActivity.class);
                intent.putExtra("USER", user);
                activity.startActivity(intent);
            }
        });
    }
    private void openProfileVolley(UserViewHolder holder, final String id){
        holder.tvUserToName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               API.openOtherUserProfile(id,activity);
            }
        });
    }



    public void setOnClickListener(final UserViewHolder holder, final ReportedUser user, final int position) {

        holder.llEditPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.e("TAG", "inside if");
                showPopUp(activity.getResources().getString(R.string.block_user_dialog_msg), position);
            }
        });


    }


    private void showPopUp(String title, final int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.title);

        final TextView tvCancel = dialogView.findViewById(R.id.tvCancel);
        tvCancel.setText(activity.getResources().getString(R.string.post_report_ignore_btn_text));


        tvTitle.setText(title);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    confirmAlert.dismiss();
                    return;
                }


                API.sendRequestToServerGET(activity, API.GROUP_USER_REPORT_ACTION + users.get(position).id + "/" + users.get(position).group_id + "/" + users.get(position).toUserId + "/1", API.GROUP_USER_REPORT_ACTION);

                notifyItemRemoved(position);
                users.remove(position);
                notifyItemRangeChanged(0, users.size());
                hideLayout();
                confirmAlert.dismiss();
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    confirmAlert.dismiss();
                    return;
                }

                API.sendRequestToServerGET(activity, API.GROUP_USER_REPORT_ACTION + users.get(position).id + "/" + users.get(position).group_id + "/" + users.get(position).toUserId + "/0", API.GROUP_USER_REPORT_ACTION);

                notifyItemRemoved(position);
                users.remove(position);
                notifyItemRangeChanged(0, users.size());
                hideLayout();
                confirmAlert.dismiss();
            }
        });

        confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();

    }

    void hideLayout() {
        ((ReportedUserActivity) activity).hideLayout();
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserFromName, tvUserToName, tvPostContent;
        ImageView ivUserFromImage, ivUserToImage;
        CardView cvContainer;
        LinearLayout llEditPost;


        UserViewHolder(View view) {
            super(view);
            tvUserFromName = view.findViewById(R.id.tvUserFromName);
            tvUserToName = view.findViewById(R.id.tvUserToName);
            ivUserFromImage = view.findViewById(R.id.ivUserFromImage);
            ivUserToImage = view.findViewById(R.id.ivUserToImage);
            cvContainer = view.findViewById(R.id.cvContainer);
            llEditPost = view.findViewById(R.id.llEditPost);
            tvPostContent = view.findViewById(R.id.tvPostContent);
        }
    }
}