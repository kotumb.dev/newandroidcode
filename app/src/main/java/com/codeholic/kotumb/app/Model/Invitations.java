package com.codeholic.kotumb.app.Model;

public class Invitations {

    private String userName,user2ID,referralCode,profilePercentage,status,createdAt;

    public Invitations(String userName, String user2ID, String profilePercentage, String createdAt) {
        this.userName = userName;
        this.user2ID = user2ID;
        this.profilePercentage = profilePercentage;
        this.createdAt = createdAt;

    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String user1ID) {
        this.userName = user1ID;
    }


    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getProfilePercentage() {
        return profilePercentage;
    }

    public void setProfilePercentage(String profilePercentage) {
        this.profilePercentage = profilePercentage;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
