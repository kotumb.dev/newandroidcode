package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import androidx.browser.customtabs.CustomTabsClient;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.browser.customtabs.CustomTabsServiceConnection;
import androidx.browser.customtabs.CustomTabsSession;
import androidx.core.util.Pair;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codeholic.kotumb.app.Activity.ChatActivity;
import com.codeholic.kotumb.app.Activity.VideoDetailsActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Message;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.EmojiMapUtil;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;


public class ChatRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final User user;
    List<Message> messages;
    Activity activity;

    private final int VIEW_TYPE_MESSAGE_ME = 0;
    private final int VIEW_TYPE_MESSAGE_OTHER = 2;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;
    CustomTabsServiceConnection mCustomTabsServiceConnection;
    CustomTabsClient mClient;
    CustomTabsSession mCustomTabsSession;
    CustomTabsIntent.Builder builder;
    boolean warmedUp = false;
    private boolean selectionMode;
    private ArrayList<Pair<Integer, Message>> selection = new ArrayList<>();
    private ArrayList<UserViewHolder> selectedViews = new ArrayList<>();

    public ChatRecyclerViewAdapter(RecyclerView recyclerView, final Activity activity, List<Message> messages, User user) {
        this.messages = messages;
        this.activity = activity;
        this.user = user;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && lastVisibleItem >= (getItemCount() - 1)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }

                if (linearLayoutManager.findFirstVisibleItemPosition() > 0) {
                    ((ChatActivity) activity).hideShowFab(false);
                } else {
                    ((ChatActivity) activity).hideShowFab(true);
                }

            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        if (messages.get(position) == null) {
            return VIEW_TYPE_LOADING;
        } else if (messages.get(position).getUserId().equals(SharedPreferencesMethod.getUserId(activity))) {
            return VIEW_TYPE_MESSAGE_ME;
        } else {
            return VIEW_TYPE_MESSAGE_OTHER;
        }
    }


    @Override
    public int getItemCount() {
        return messages.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_MESSAGE_ME) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_me, parent, false);
            UserViewHolder UserViewHolder = new UserViewHolder(view);
            return UserViewHolder;
        } else if (viewType == VIEW_TYPE_MESSAGE_OTHER) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_other, parent, false);
            UserViewHolder UserViewHolder = new UserViewHolder(view);
            return UserViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    public void clearSelection() {
        selection.clear();
        selectionMode = false;
        for (UserViewHolder v : selectedViews) {
            v.itemView.setBackgroundColor(Color.parseColor("#00000000"));
        }
    }

    public ArrayList<Pair<Integer, Message>> getSelectedItems() {
        return selection;
    }

    public void deleteSelection() {
        String ids = "";
        for (Pair<Integer, Message> message : selection) {
            ids = ids + message.second.getId() + ",";
            int index = getIndexOf(message);
            if (index > -1) {
                messages.remove(index);
            }
        }
        notifyDataSetChanged();
        ids = ids.substring(0, ids.length() - 1);
        try {
            Utility.showLoading(activity, "Deleting...");
            String cid = new JSONObject(user.getUserInfo()).getString("cid");
            String userId = SharedPreferencesMethod.getUserId(activity);
            String url = API.DELETE_CONVERSATION + "/" + cid + "/" + userId + "/" + ids;
            API.sendRequestToServerGET(activity, url, API.DELETE_CONVERSATION + 2);// service call for getting messages
        } catch (JSONException e) {
            e.printStackTrace();
        }
        clearSelection();
    }

    private int getIndexOf(Pair<Integer, Message> message) {
        int counter = 0;
        for (Message msg : messages) {
            if (message.second.getId().equalsIgnoreCase(msg.getId())) {
                return counter;
            }
            counter++;
        }
        return -1;
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewholder, final int position) {
        final Message message = messages.get(position);
        try {
            if (viewholder instanceof UserViewHolder) {
                final UserViewHolder holder = (UserViewHolder) viewholder;
                String reply = EmojiMapUtil.replaceCheatSheetEmojis("" + message.getReply());
                String refMSG = EmojiMapUtil.replaceCheatSheetEmojis("" + message.getRefrenceMessage());

                CharSequence sequence = Html.fromHtml(reply);
                SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
                URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
                for (URLSpan span : urls) {
                    makeLinkClickable(strBuilder, span);
                }
                holder.reply_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        viewholder.getAdapterPosition();

                    }
                });

                CharSequence refsequence = Html.fromHtml(refMSG);
                SpannableStringBuilder refstrBuilder = new SpannableStringBuilder(refsequence);
                URLSpan[] refurls = refstrBuilder.getSpans(0, refsequence.length(), URLSpan.class);
                for (URLSpan refspan : refurls) {
                    makeLinkClickable(refstrBuilder, refspan);
                }

                System.out.println("The Get Message "+  strBuilder);
                holder.tvMessage.setText(strBuilder);
                holder.tvMessage.setMovementMethod(LinkMovementMethod.getInstance());
                if (message.getRefrenceMessage().isEmpty()){
                    holder.reply_container.setVisibility(View.GONE);

                }else{
                    holder.reply_container.setVisibility(View.VISIBLE);
                    holder.reply_text.setText(refstrBuilder);
                    System.out.println("Real Time MSG  "+message.getRefrenceMessage());
                }

                //holder.tvMessage.setText(Html.fromHtml(reply));
                holder.tvTime.setText("" + message.getMessageAT());
                if (holder.sent_icon != null) {
                    if (message.getReadStatus().equalsIgnoreCase("-1") && !message.isSent()) {
                        holder.sent_icon.setImageResource(R.drawable.clock);
                        holder.sent_icon.setPadding(4, 4, 4, 4);
                        holder.sent_icon.setColorFilter(activity.getResources().getColor(R.color.tick));
                    }
                    if (message.getReadStatus().equalsIgnoreCase("-1") && message.isSent()) {
                        holder.sent_icon.setImageResource(R.drawable.tick);
                        holder.sent_icon.setPadding(0, 0, 0, 0);
                        holder.sent_icon.setColorFilter(activity.getResources().getColor(R.color.tick));
                    }
                    if (message.getReadStatus().equalsIgnoreCase("0")) {
                        holder.sent_icon.setImageResource(R.drawable.double_tick);
                        holder.sent_icon.setPadding(0, 0, 0, 0);
                        holder.sent_icon.setColorFilter(activity.getResources().getColor(R.color.tick));
                    }
                    if (message.getReadStatus().equalsIgnoreCase("1")) {
                        if (Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP) {
                            holder.sent_icon.setImageResource(R.drawable.double_tick);
                            holder.sent_icon.setColorFilter(activity.getResources().getColor(R.color.blue_tick));
                        } else {
                            holder.sent_icon.setImageResource(R.drawable.double_tick_blue);
                        }
                        holder.sent_icon.setPadding(0, 0, 0, 0);
                    }
                }

                setSelected(holder, isContains(position, message));

                holder.cvContainer.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (!selectionMode) {
                            selectionMode = true;
                            setSelected(holder, true);
                            selection.add(new Pair<>(position, message));
                            ((ChatActivity) activity).showContextMenu();
                        } else {
                            selectionMode = false;
                            ((ChatActivity) activity).hideContextMenu();
                        }
                        return true;
                    }
                });

                holder.cvContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (selectionMode) {
                            if (!isContains(position, message)) {
                                setSelected(holder, true);
                                selection.add(new Pair<>(position, message));
                                ((ChatActivity) activity).showContextMenu();
                            } else {
                                setSelected(holder, false);
                                removeByPos(position);
                                ((ChatActivity) activity).showContextMenu();
                            }
                        }
                    }
                });

                containerclick(holder,holder.tvMessage,message,position);
                containerclick(holder,holder.reply_text,message,position);
                holder.tvMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try{
                            String currentString = holder.tvMessage.getText().toString().trim();
                            String[] separated = currentString.split("~");
                            System.out.println("Video More   "+separated[0]+""+separated[0]);
                            if (separated[9].equalsIgnoreCase(" Click Here For More Details")){
                                Intent intent=new Intent(activity,VideoDetailsActivity.class);
                                intent.putExtra("video_user",separated[0]);
                                intent.putExtra("video_title",separated[2]);
                                intent.putExtra("video_desp",separated[3]);
                                intent.putExtra("video_id",separated[4]);
                                intent.putExtra("video_userid",separated[5]);
                                intent.putExtra("video_like_count",separated[6]);
                                intent.putExtra("video_date_time",separated[7]);
                                intent.putExtra("video_uri",separated[8]);
                                intent.putExtra("video_avatar",user.getAvatar());
                                activity.startActivity(intent);
                            }
                        }catch (Exception ex){
                            ex.printStackTrace();
                            System.out.println("Text Error  "+ex.getMessage());
                        }
                    }
                });


                pdfAttachInMessage(holder,message);//set and Download PDF on Message Container
            } else if (viewholder instanceof LoadingViewHolder) {
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
                loadingViewHolder.progressBar.setIndeterminate(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void termsConditionPopup(Activity activity){
        androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.activity_video_details, null);
        dialogBuilder.setView(dialogView);
        final androidx.appcompat.app.AlertDialog confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();
    }



    public void containerclick(final UserViewHolder holder, final TextView textView, final Message message, final int position){
        textView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!selectionMode) {
                    selectionMode = true;
                    setSelected(holder, true);
                    selection.add(new Pair<>(position, message));
                    ((ChatActivity) activity).showContextMenu();
                } else {
                    selectionMode = false;
                    ((ChatActivity) activity).hideContextMenu();
                }
                return true;
            }
        });


//        textView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (selectionMode) {
//                    if (!isContains(position, message)) {
//                        setSelected(holder, true);
//                        selection.add(new Pair<>(position, message));
//                        ((ChatActivity) activity).showContextMenu();
//                    } else {
//                        setSelected(holder, false);
//                        removeByPos(position);
//                        ((ChatActivity) activity).showContextMenu();
//                    }
//                }
//            }
//        });
    }








    //This Method is Made By Arpit Kanda
    //For Show PDF in Full Screen
    public void showFullPDF(Message message){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + API.downloadPDF+message.getMessageMedia()), "text/html");
        activity.startActivity(intent);
    }


    //
    //This Method is Create By Arpit Kanda
    //For Attach PDF And Download PDF and View Also
    public void pdfAttachInMessage(UserViewHolder holder,final Message message){
        if (message.getMessageMediaType().equalsIgnoreCase("pdf")){
            holder.pdfIcon.setVisibility(View.VISIBLE);
            holder.tvMessage.setText("PDF ");
            holder.pdfIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    new DownloadPDF.DownloadFile().execute(API.downloadPDF+message.getMessageMedia(),message.getMessageMedia());
                    showFullPDF(message);
                }
            });
        }else{
            holder.pdfIcon.setVisibility(View.GONE);
        }
    }

//




    private void removeByPos(int position) {
        int indexToRemove = -1;
        int index = 0;
        for (Pair<Integer, Message> pair : selection) {
            if (pair.first == position) {
                indexToRemove = index;
            }
            index++;
        }
        if (indexToRemove >= 0) {
            selection.remove(indexToRemove);
        }
    }

    private boolean isContains(int position, Message message) {
        for (Pair<Integer, Message> pair : selection) {
            if (pair.first == position) {
                return true;
            }
        }
        return false;
    }




    private void setSingleClickListener(final int position, final Message message, final UserViewHolder holder) {
        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showDeletePopUpMenu(holder.cvContainer, activity, new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case 0:
                                deleteSelectedMsg(position, message);
                                break;
                            case 1:
                                //   Utils.showShareExternalOptions(message, activity);
                                break;
                        }
                        return false;
                    }
                });
            }
        });
    }

    private void deleteSelectedMsg(int position, Message message) {
        try {
            messages.remove(position);
            String cid = new JSONObject(user.getUserInfo()).getString("cid");
            String url = API.DELETE_CONVERSATION + "/" + cid + "/" + SharedPreferencesMethod.getUserId(activity) + "/" + message.getId();
            API.sendRequestToServerGET(activity, url, API.DELETE_CONVERSATION + 2);// service call for getting messages
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setSelected(UserViewHolder holder, boolean b) {
        if (b) {
            holder.itemView.setBackgroundColor(Color.parseColor("#81d4fa"));
            selectedViews.add(holder);
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#00000000"));
            selectedViews.remove(holder);
        }
    }

    private void showPopup(Message message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        CharSequence[] items = {"Delete"};
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    protected void makeLinkClickable(final SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                Utility.prepareCustomTab(activity, span.getURL());
                // Do something with span.getURL() to handle the link click...
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

    /*private void prepareCustomTab(final String url) {

        builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        // builder.setSecondaryToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
        builder.setShowTitle(true);
        builder.addDefaultShareMenuItem();

        Bitmap icon = getBitmapFromDrawable(activity, R.drawable.ic_arrow_back);
        PendingIntent pendingIntent = createPendingShareIntent();

        final Bitmap backButton = getBitmapFromDrawable(activity, R.drawable.ic_arrow_back);
        builder.setCloseButtonIcon(backButton);

        builder.setStartAnimations(activity, R.anim.slide_in_right, R.anim.slide_out_left);
        builder.setExitAnimations(activity, R.anim.slide_in_left, R.anim.slide_out_right);
        //builder.setActionButton(icon, activity.getString(R.string.app_name), pendingIntent, true);
        // builder.addMenuItem("Share this page", createPendingShareIntent());
        mCustomTabsServiceConnection = new CustomTabsServiceConnection() {
            @Override
            public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
                //Pre-warming
                mClient = customTabsClient;
                mClient.warmup(0L);
                mCustomTabsSession = mClient.newSession(null);
                mCustomTabsSession.mayLaunchUrl(Uri.parse(url), null, null);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mClient = null;
            }
        };
        warmedUp = CustomTabsClient.bindCustomTabsService(activity, "com.android.chrome", mCustomTabsServiceConnection);
        if (url.isEmpty())
            return;

        builder.build().launchUrl(activity, Uri.parse(url));
    }

    private Bitmap getBitmapFromDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof VectorDrawable) {
            return getBitmapFromVectorDrawable((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("Unable to convert to bitmap");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private Bitmap getBitmapFromVectorDrawable(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    private PendingIntent createPendingShareIntent() {
        Intent actionIntent = new Intent(Intent.ACTION_SEND);
        actionIntent.setType("text/plain");
        actionIntent.putExtra(Intent.EXTRA_TEXT, "Share text");
        return PendingIntent.getActivity(
                activity, 0, actionIntent, 0);
    }*/

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvTime;
        EmojiconTextView tvMessage;
        CardView cvContainer;
        ImageView sent_icon;
        ImageView pdfIcon;
        CardView reply_container;
        TextView reply_text;

        UserViewHolder(View view) {
            super(view);
            cvContainer = view.findViewById(R.id.cvContainer);
            tvTime = view.findViewById(R.id.tvTime);
            tvMessage = view.findViewById(R.id.tvMessage);
            sent_icon = view.findViewById(R.id.sent_icon);
            pdfIcon=view.findViewById(R.id.pdf_icon);
            reply_container=view.findViewById(R.id.chat_reply_container);
            reply_text=view.findViewById(R.id.reply_txt_me);
        }
    }

}