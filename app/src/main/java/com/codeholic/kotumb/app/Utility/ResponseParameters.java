package com.codeholic.kotumb.app.Utility;

/**
 * Created by DELL on 10/26/2017.
 */

public interface ResponseParameters {
    public String ErrorKey = "errorKey";
    public String Errors = "errors";
    public String Error = "error";
    public boolean flag=true;
    public String Success = "success";
    public String LIKED_AT="likedAt";
    public String LIKE_ID="likeId";
    public String VIDEO_ID="videoId";
    public String MESSAGE = "message";
    public String CREATED_AT="createdAt";
    public String Userdata = "userdata";
    public String UserId = "userId";
    public String User_Id = "user_id";
    public String ViewerId = "viewerId";
    public String ReferenceId = "referenceId";
    public String UserName = "userName";
    public String MobileNumber = "mobileNumber";
    public String Otp = "otp";
    public String IsConfirmed = "isConfirmed";
    public String DisplayName = "displayName";
    public String ChangeLoginInfo = "change_login_info";
    public String FirstLogin = "first_login";
    public String Set1Questions = "set1_questions";
    public String Set2Questions = "set2_questions";
    public String Question = "question";
    public String Name="name";
    public String QuestionOne = "question1";
    public String QuestionTwo = "question2";
    public String AnswerOne = "answer1";
    public String AnswerTwo = "answer2";

    public String PROFILE_SUMMERY = "profileSummary";
    public String PROFILE_HEADLINE = "profileHeadline";

    public String Id = "id";
    public String PersonalInfo = "personal_info";
    public String PersonalInformation = "personal_information";
    public String FirstName = "firstName";
    public String LastName = "lastName";
    public String DateOfBirth = "dateOfBirth";
    public String Gender = "gender";
    public String Email = "email";
    public String Avatar = "avatar";
    public String Address = "address";
    public String City = "city";
    public String State = "state";
    public String ZIP = "zipcode";
    public String EMAIL_VISIBILITY = "emailVisibility";
    public String MOBILE_VISIBILITY = "mobileVisibility";
    public String ADDRESS_VISIBILITY = "addressVisibility";
    public String ZIPCODES = "zipcodes";
    public String DISTRICT = "district";
    public String CONNECTIONS_COUNT = "connections_count";
    public String CONNECTIONS_DATE = "connectionDate";
    public String INVITATIONS_COUNT = "invitations_count";
    public String SHOW_POPUP_PROFESSION = "show_popup_profession";
    public String SHOW_POPUP_PRIMARY_NUMBER = "show_popup_primary_no";
    public String CONNECTIONS = "connections";
    public String ISCONNECTED = "is_connected";


    public String PROFILE = "profile";
    public String recommendations = "recommendations";
    public String PROFILE_STRENGTH = "profileStrength";
    public String CHECK_PERSONAL_INFO = "check_personal_info";
    public String EDUCATION_DETAIL_CHECK = "education_details_check";
    public String EDUCATION_DETAIL = "education_details";
    public String EDUCATION = "education";
    public String WORK_EXPERIANCE_CHECK = "work_experience_check";
    public String USER_SKILLS_CHECK = "user_cskills_check";
    public String USER_DKILLS_CHECK = "user_dskills_check";
    public String USER_SKILLS = "user_cskills";
    public String USER_DSKILLS = "user_dskills";
    public String CHECK_ADHAR_INFO = "check_adhaar_info";

    public String WORK_EXPERIANCE = "work_experience";
    public String EXPERIANCE = "experience";
    public String ORGANIZATION_NAME = "organizationName";
    public String ORGANIZATION_ROLE = "organizationRole";
    public String YEARFROM = "yearFrom";
    public String YEARTO = "yearTo";
    public String DESCRIPTION = "description";
    public String ISCURRENT = "isCurrent";
    public String AADHAR_INFO = "adhaar_info";
    public String AADHARINFO = "aadhaarinfo";
    public String ADHAR_NUMBER = "aadharNumber";
    public String NAME_ON_ADHAR = "nameOnAadhar";
    public String FATHER_NAME = "fatherName";
    public String VERIFICATION_STATUS = "verificationStatus";
    public String AlternateMobile = "alternateMobile";
    public String QUESTIONS = "ques_ans";
    public String JOBROLES = "jobroles";
    public String ORGANISATIONS = "organizations";
    public String ORGANISATION_NAME = "organizationName";
    public String ROLENAME = "roleName";
    public String COURSES = "courses";
    public String COURSESNAME = "courseName";
    public String SCHOOLS = "schools";
    public String SCHOOLNAME = "schoolName";

    public String VIEWERS = "viewers";
    public String SKILLS = "skills";
    public String INDUSTRIES = "industries";
    public String SKILL = "skill";
    public String SkillDescription = "skillDescription";
    public String Interests = "interests";
    public String RESULTS = "results";

    public String CONNECTION_FLAG = "connection_flag";
    public String CONNECTION_INFO = "connect_info";
    public String STATUS = "status";
    public String Approved="isApproved";
    public String Publish="isPublished";
    public String ACTIONUSERID = "actionUserId";

    public String NOTES = "notes";
    public String NOTE = "note";

    public String INVITATIONS = "invitations";
    public String TOTAL_ROWS = "total_rows";
    String resumeid = "resumeid";
    String resumeId = "resumeId";
    String TITLE = "title";
    public String Date = "date";
    String TIME = "time";
    String ACTIVITY_TYPE = "activityType";
    String MEDIA_TYPE="mediaType";
    String THUMBNAIL="thumbnail";
    String MEDIA="media";
    String NOTIFICATIONS = "notifications";
    String NOTELIST = "note_list";
    String REQUESTED = "requested";
    String REQUESTACCEPTED = "requestaccepted";
    String REQUESTDELETED = "requestdeleted";
    String ASKED_RECOM = "asked_recommend";
    String PROFILEVIEWED = "profileviewed";
    String Role = "role";
    String NORMAL_ROLE = "KU1";
    String USER_NOTES = "user_notes";
    String NOTE_UPDATED = "note_updated";
    String NOTE_CREATED = "note_created";
    String NOTE_DELETED = "note_deleted";
    String UPDATE_USER = "UPDATE_USER";

    String REQUEST_RECEIVED = "REQUEST_RECEIVED";
    String REQUEST_ACCEPTED = "REQUEST_ACCEPTED";
    String REQUEST_SEND = "REQUEST_SEND";
    String ADD_RECEIVED = "ADD_RECEIVED";

    String LAST_MSG_FLAG = "last_msg_flag";
    String LAST_MSG_INFO = "last_msg_info";
    String MESSAGEAT = "messagedAT";
    String REPLY = "reply";
    String UNREAD_MESSAGE_COUNT = "un_read_msg_count";
    String MESSAGES = "messages";
    String MESSAGES_INFO = "message_info";
    String UPDATE_CHAT = "UPDATE_CHAT";
    String MESSAGE_SENT = "message_sent";
    String MESSAGE_RECEIVED = "message_received";
    String SUPPORT_CHAT = "support_chat";
    String BROADCAST = "broadcast";
    String MSG = "msg";

    String MESSAGE_COUNT = "message_count";
    String ACTIVITY_TIME = "activityTime";
    String URL = "url";
    String LINKS = "links";
    String SECTOR = "sector";
    String ORGANIZATION_INDUSTRY = "organizationIndustry";
    String AD = "ad";
    String IMAGE = "image";
    String SHARERESUME = "SHARERESUME";
    String SUGGESTIONS = "suggestion";
    String ALL_LANGUAGES = "all_languages";
    String LANGUAGE = "language";
    String LANGUAGES = "languages";
    String FAV_LINK = "fav_link";
    String IMAGE_BASE_URL = "img_base_url";
    String AD_IMAGE_BASE_URL = "ad_img_url";
    String GROUPS = "groups";
    String GROUP_IMG_BASE_URL = "group_img_base_url";
    String GROUP_CATEGORIES = "group_categories";
    String CATEGORY = "category";
    String GROUP = "group";
    String GROUP_INFO = "groupInfo";
    String GROUP_MEMBERS = "group_members";
    String BUNDLE = "BUNDLE";
    String GROUP_MEMBERS_REQUESTED = "group_members_requested";
    String GROUP_MEMBERS_BLOCKED = "group_members_blocked";
    String GROUP_MEMBERS_INVITED = "group_members_invited";
    String USERS = "users";
    String CREATE_GROUP = "create_group";
    String GROUP_POSTS = "group_posts";
    String POST = "post";
    String COMMENT = "comment";
    String GROUP_POSTS_COMMENTS = "group_post_comments";
    String POST_LIKES = "post_likes";
    String GROUP_POST_LIKES = "group_post_likes";
    String IMAGE_URI_LIST = "IMAGE_URI_LIST";
    String DOB_VISIBILITY = "dobVisibility";
    String GENDER_VISIBILITY = "genderVisibility";
    String AVAILABLE = "available";
    String MiddleName = "middleName";
    String SHOW_CLEAR = "show_clear";
    String TYPE = "type";
    String LIKEINFO = "likeinfo";
    String REPORTED_USERS = "reported_users";
    String POSTS_REPORTS = "post_reports";
    String COMMENT_REPORTS = "comment_reports";
    String RSSFEEDS = "rssFeeds";
    String ITEMS = "items";
    String LINK = "link";
    String UPDATE = "update";
    String REQUIRED = "required";
    String END_DATE="endDate";
    String VERSION = "version";
    String NOTIFICATION_COUNT = "notification_count";
    String OPEN_FRAGMENT = "open_fragment";
    String OPEN_GROUP = "open_fragment";

    /*broadcast activity type*/
    String GROUP_CREATED = "group_created";
    String GROUP_UPDATED = "group_updated";
    String GROUP_INVITED = "group_invite";
    String GROUP_MEMBER_DELETE = "group_member_delete";
    String GROUP_MEMBER_ROLE_UPDATE = "group_member_role_update";
    String GROUP_MEMBER_ROLE_UPDATE_NEW = "group_member_role_update_new";
    String GROUP_MEMBER_ACCEPT = "group_member_accept";
    String GROUP_MEMBER_BLOCK = "group_member_block";
    String GROUP_DELETED = "group_deleted";
    String GROUP_DELETED_ALL = "all_group_deleted";
    String GROUP_INVITATION = "groupinvitation";
    String GROUP_EDIT_POST = "group_edit_post";
    String GROUP_DELETE_POST = "group_delete_post";
    String GROUP_POST_LIKE = "group_post_like";
    String GROUP_POST_UNLIKE = "group_post_unlike";
    String GROUP_COMMENT_CREATED = "group_comment_created";
    String GROUP_MEMBER_UNBLOCK = "group_member_unblock";
    String GROUP_COMMENT_DELETED = "group_comment_deleted";
    String LOGOUT = "logout";
    String GROUP_REQUEST = "group_request";
    String GROUP_REQUESTED = "grouprequested";
    String GROUP_REQUEST_ACCEPTED = "grouprequestaccept";
    String GROUP_MEMBER_DELETED = "groupmemberdeleted";
    String GROUP_REPORTED_POST = "report_group_post";
    String GROUP_REPORTED_USER = "report_group_user";
    String GROUP_REPORTED_COMMENTS = "report_group_comment";
    String GROUP_POST_COMMENT = "group_post_comment";
    String GROUP_POST_SHARE = "groupsharepost";
    String OPEN_COMMENT_VIEW = "openCommentView";
    String IS_BLOCKED = "isBlocked";
    String IS_DELETED = "isDeleted";
    String GROUP_DELETE_ADMIN = "group_deleted_by_admin";
    String MAKE_GROUP_ADMIN = "make_group_admin";
    String MAKE_GROUP_MEMBER = "make_group_member";
    //    public static final String GROUP_IMG_BASE_URL = "group_img_base_url";
    public static final String GROUP_NAME = "groupName";
    public static final String GROUP_ID = "groupId";
    String LOGO = "logo";
    String GROUP_POST_COMMENT_NOTIFICATION = "group_post_comment";
    String GROUP_POST_COMMENT_OTHERS_NOTIFICATION = "group_post_comment_others";
    String GROUP_POST_LIKE_NOTIFICATION = "group_post_like";
    String GROUP_POST_SHARE_NOTIFICATION = "group_post_share";
    String SHARE_COUNT = "share_count";
    String LIKE_COUNT = "like_count";
    String COMMENT_COUNT = "comment_count";
    String FIELD_ID = "field_id";
    String POST_ID = "post_id";
    String POST_INFO = "postInfo";
    String VIDEO_INFO="new_video";
    String VIDEOS="videos";
    String VIDEO_APPROVE="videoapproved";
    String VIDEO_LIKES="videolikes";
    String NEW_VIDEO_NOTIFICATION="newvideonotification";
    String GROUPID = "group_id";
    String GROUP_POST_ADD = "group_post_add";
    String GROUP_SHARED_POST = "group_shared_post";
    String GROUP_POST_SHARED = "group_post_shared";
    String AD_IMG_URL = "ad_img_url";
    String WELCOME = "welcome";
    String SHARE_TEXT = "post_share_txt";
    String PUBLIC_GROUP = "publicgroup";
    String IS_CHALLENGED = "isChallenged";
    String WHAT_TO_DO = "what_to_do";
    String MESSAGE_READ = "message_read";
    String REWARD_POINTS = "rewardPoints";
    String SURVEY_TIME = "survey_time";
    String REWARDS = "awards";
    String WELCOME_MSG = "welcome_msg";
    String GROUP_USER_REPORT_NOTIFICATION = "group_user_report";
    String GROUP_POST_REPORT_NOTIFICATION = "group_post_report";
    String GROUP_POST_COMMENT_REPORT_NOTIFICATION = "group_post_comment_report";
    boolean PROFILE_SUMMERY_POP_SHOW=true;

    String CURRENT_REWARD = "currentreward";
    String POINTS = "points";
    String REWARDTYPE = "rewardType";
    String REDEEM_REWARD = "redeemreward";
    String EXPIRED_REWARD = "expiredreward";
    String REFERREL = "referral";
    String PROFILESTRENGTH = "profileStrength";
    String MIN_REDEEM_POINTS = "minimumPointtoRedeem";
    String EXPIREDATE = "expirationTime";
    String PAISEPERPOINT = "paiseperpoint";
    String SUMMATION_OF_POINT_EXPIRE = "sumofpointsexpiring";
     String ROCKSTAR = "namerockstar";
     String SUPERSTAR = "namesuperstar";
     String POWERSTAR = "namepowerstar";
     String MIN_POINT_ROCKSTAR = "minimumPointtoRockstar";
     String MIN_POINT_SUPERSTAR = "minimumPointtoSuperstar";
     String MIN_POINT_POWERSTAR = "minimumPointtoPowerstar";



//    public static final String COMMENT = "comment";






/*group_member_unblock //
group_add_post //
group_comment_updated//
group_delete_comment//*/
}
