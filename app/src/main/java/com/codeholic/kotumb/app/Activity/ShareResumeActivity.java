package com.codeholic.kotumb.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.EventNames;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShareResumeActivity extends AppCompatActivity {
    AlertDialog f21b;
    @BindView(R.id.header)
    TextView header;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    com.codeholic.kotumb.app.View.ProgressBar progressBar = null;
    String resumeid = "1";
    @BindView(R.id.wv)
    WebView wv;

    //custom clicklistener class
    class onClickListener implements OnClickListener {
        onClickListener() {
        }

        public void onClick(View v) {
            ShareResumeActivity.this.onBackPressed();
        }
    }

    //AsyncTask for getting html for webview
    private class DownloadWebPageTask extends AsyncTask<String, Void, String> {

        class webChromeClient extends WebChromeClient {
            webChromeClient() {
            }
        }

        class onLongClickListener implements OnLongClickListener {
            onLongClickListener() {
            }

            public boolean onLongClick(View v) {
                return true;
            }
        }

        private DownloadWebPageTask() {
        }

        protected String doInBackground(String... urls) {
            try {
                if (Utility.isConnectingToInternet(ShareResumeActivity.this)) { //check internet connection
                    String response = "";
                    for (String url : urls) {
                        Log.e("url", "" + url);
                        try {
                            BufferedReader buffer = new BufferedReader(new InputStreamReader(new DefaultHttpClient().execute(new HttpGet(url)).getEntity().getContent()));
                            String str = "";
                            while (true) {
                                str = buffer.readLine();
                                if (str == null) {
                                    break;
                                }
                                response = response + str;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return response; // html string
                }
                Snackbar.make(ShareResumeActivity.this.wv, ShareResumeActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                return "";
            } catch (Exception e2) {
                e2.printStackTrace();
                return "";
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setLanguage();
        }

        protected void onPostExecute(String result) {
            ShareResumeActivity.this.pbLoading.setVisibility(View.GONE);
            if (!result.isEmpty()) {
                //setting webview

                ShareResumeActivity.this.wv.getSettings().setUseWideViewPort(true);
                //ShareResumeActivity.this.wv.getSettings().setJavaScriptEnabled(true);
                ShareResumeActivity.this.wv.setWebChromeClient(new webChromeClient());
                ShareResumeActivity.this.wv.getSettings().setLoadWithOverviewMode(true);
                ShareResumeActivity.this.wv.setOnLongClickListener(new onLongClickListener());
                ShareResumeActivity.this.wv.setLongClickable(false);
                Log.e("result", "" + result);
                //load html string to webview
                ShareResumeActivity.this.wv.loadData(result, "text/html", "UTF-8");
                ShareResumeActivity.this.wv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

                setLanguage();
            }
        }
    }

    private void setLanguage() {
        String defaultLanguage = SharedPreferencesMethod.getDefaultLanguage(ShareResumeActivity.this);
        Locale locale = new Locale(defaultLanguage, "IN");
        setLocale(locale, ShareResumeActivity.this);
    }

    public static void setLocale(Locale locale, Context context) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        Locale.setDefault(locale);
        configuration.setLocale(locale);

        if (Build.VERSION.SDK_INT >= 25) {
            context = context.getApplicationContext().createConfigurationContext(configuration);
            context = context.createConfigurationContext(configuration);
        }

        context.getResources().updateConfiguration(configuration,
                resources.getDisplayMetrics());
    }

    //oncreate method for setting content view
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        {
            setLanguage();
        }
        setContentView(R.layout.activity_share_resume);
        ButterKnife.bind(this);
        initToolbar();
        initwebView();
        findViewById(R.id.fab).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isConnectingToInternet(ShareResumeActivity.this)) { // check net connection
                    EmailPopup();
                } else {
                    Snackbar.make(v, ShareResumeActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                }
            }
        });


        setLanguage();

    }

    //initialize web view show resume
    private void initwebView() {
        String url = API.RESUME_TEMPLATE + "/" + this.resumeid + "/" + SharedPreferencesMethod.getUserId(this);
        new DownloadWebPageTask().execute(url);
    }


    public  void NotePopup(final EditText etEmail, final TextInputLayout etEmailLayout,String NoteTitle) {
        final Builder dialogBuilder = new Builder(this);
        final View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_resume_note, null);
        dialogBuilder.setView(dialogView);
        final EditText resume_note = dialogView.findViewById(R.id.resume_note);
        final TextView cancel = dialogView.findViewById(R.id.cancel);
        final TextView note = dialogView.findViewById(R.id.share_note);
        note.setText(R.string.home_share_resume_link);
        if (NoteTitle.equalsIgnoreCase("Send via Mail")){
            note.setOnClickListener(getEmailListener(resume_note,etEmail,etEmailLayout));
        }else{

        }
        final AlertDialog confirmAlert = dialogBuilder.create();

        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });
        confirmAlert.show();
    }
    //email popup 
    private void EmailPopup() {
        Builder dialogBuilder = new Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.email_popup_redesign, null);
        dialogBuilder.setView(dialogView);
        final EditText etEmail = dialogView.findViewById(R.id.etEmail);
        final TextInputLayout etEmailLayout = dialogView.findViewById(R.id.etEmailLayout);
        final TextView share=dialogView.findViewById(R.id.share_resume);
        final TextView shareContract=dialogView.findViewById(R.id.share_contacts);
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+.[a-z]+";
        share.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etEmail.getText().toString().isEmpty()||!etEmail.getText().toString().matches(emailPattern)){
                    enableError(etEmailLayout, getResources().getString(R.string.email_incorrect_input_format));
                }else{
                    NotePopup(etEmail,etEmailLayout,share.getText().toString());
                    etEmailLayout.setErrorEnabled(false);
                }

            }
        });

        shareContract.setOnClickListener(getContactsClickListener());
        this.f21b = dialogBuilder.create();
        this.f21b.setCancelable(false);
        this.f21b.show();
        Window window = this.f21b.getWindow();
        if (window != null) {
            View decorView = window.getDecorView();
            decorView.findViewById(R.id.cancel).setOnClickListener(getDismissListener());
        }

    }




    private void enableError(TextInputLayout inputLayout, String error) {
        try {
            inputLayout.setErrorEnabled(true);
            inputLayout.setError(error);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }






    @NonNull
    private OnClickListener getContactsClickListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShareResumeActivity.this, ConnectionsMessageActivity.class);
                intent.putExtra(ResponseParameters.SHARERESUME, "");
//                intent.putExtra(ResponseParameters.NOTE,Note.getText().toString());
                startActivityForResult(intent, 1);
            }
        };
    }

    @NonNull
    private OnClickListener getDismissListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                f21b.dismiss();
            }
        };
    }

    @NonNull
    private OnClickListener getEmailListener(final EditText Note,final EditText etEmail, final TextInputLayout etEmailLayout) {
        return new OnClickListener() {
            public void onClick(View v) {
                if (Boolean.valueOf(false).booleanValue()) {
                    ShareResumeActivity.this.f21b.dismiss();
                }
                etEmailLayout.setErrorEnabled(false);
                if (etEmail.getText().toString().trim().isEmpty()) {
                    etEmailLayout.setErrorEnabled(true);
                    etEmailLayout.setError(ShareResumeActivity.this.getResources().getString(R.string.email_empty_input));
                } else if (!Utility.isEmailIdValid(etEmail.getText().toString().trim())) {
                    etEmailLayout.setErrorEnabled(true);
                    etEmailLayout.setError(ShareResumeActivity.this.getResources().getString(R.string.email_incorrect_input_format));
                } else if (Utility.isConnectingToInternet(ShareResumeActivity.this)) {
                    HashMap<String, Object> input = new HashMap();
                    input.put("email", "" + etEmail.getText().toString().trim());
                    input.put(RequestParameters.NOTE, "" +Note.getText().toString().trim());
                    System.out.println("Resume Note   "+input);
                    ShareResumeActivity.this.progressBar = new com.codeholic.kotumb.app.View.ProgressBar(ShareResumeActivity.this);
                    ShareResumeActivity.this.progressBar.show(ShareResumeActivity.this.getResources().getString(R.string.app_please_wait_text));
                    API.SHARE_RESUME += SharedPreferencesMethod.getUserId(ShareResumeActivity.this) + "/" + ShareResumeActivity.this.resumeid;
                    Log.e("API.SHARE_RESUME", "" + API.SHARE_RESUME);
                    API.sendRequestToServerPOST_PARAM(ShareResumeActivity.this, API.SHARE_RESUME, input);// service call for share resume
                    ShareResumeActivity.this.f21b.dismiss();
                } else {
                    Snackbar.make(etEmailLayout, ShareResumeActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                }
            }
        };
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && data != null) {
            ShareResumeActivity.this.progressBar = new com.codeholic.kotumb.app.View.ProgressBar(ShareResumeActivity.this);
            ShareResumeActivity.this.progressBar.show(ShareResumeActivity.this.getResources().getString(R.string.app_please_wait_text));
            HashMap<String, Object> input = new HashMap();
            input.put(RequestParameters.FROM_USERID, "" + SharedPreferencesMethod.getUserId(ShareResumeActivity.this));
            input.put(RequestParameters.TO_USERID, "" + data.getStringExtra(ResponseParameters.UserId));
            input.put(RequestParameters.NOTE, "" + data.getStringExtra(ResponseParameters.NOTE));
            System.out.println("Contact Input    "+input);
            API.sendRequestToServerPOST_PARAM(ShareResumeActivity.this, API.SHARE_RESUME_MESSAGE, input);// service call for share resume
        }
    }

    //setting toolbar and header title
    private void initToolbar() {
        setSupportActionBar(this.mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        this.header.setText(getResources().getString(R.string.share_resume_title_text));
        this.mToolbar.setNavigationOnClickListener(new onClickListener());
        if (!SharedPreferencesMethod.getString(this, "" + ResponseParameters.resumeid).isEmpty()) {
            this.resumeid = SharedPreferencesMethod.getString(this, "" + ResponseParameters.resumeid);
        }
    }

    //get response from server
    public void getResponse(JSONObject outPut, int i) {
        try {
            if (this.progressBar != null) {
                this.progressBar.dismiss();
            }
            Log.e("output", "" + outPut);
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (i == 0) {
                    if (outPut.has(ResponseParameters.MESSAGE)) {
                        Snackbar.make(this.mToolbar, outPut.getString(ResponseParameters.MESSAGE), Toast.LENGTH_SHORT).show();
                        if (outPut.getString(ResponseParameters.MESSAGE).equalsIgnoreCase(ResponseParameters.Success)) {
//                            Toast.makeText(this, getResources().getString(R.string.resume_email_sent_success), Toast.LENGTH_SHORT).show();

                            Bundle bundle = new Bundle();
                            bundle.putString("UserId", SharedPreferencesMethod.getUserId(this));
                            bundle.putString("SharedVia", "Email");
                            Utils.logEvent(EventNames.SHARE_RESUME, bundle, this);

                            Utils.showPopup(ShareResumeActivity.this, getResources().getString(R.string.resume_email_sent_success), new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                }
                            });

                        }
                    }
                } else if (i == 1) {
                    if (outPut.has(ResponseParameters.Success)) {
                        Snackbar.make(this.mToolbar, outPut.getString(ResponseParameters.Success), Toast.LENGTH_SHORT).show();
                        if (outPut.getString(ResponseParameters.Success).equalsIgnoreCase(ResponseParameters.Success)) {

                            Bundle bundle = new Bundle();
                            bundle.putString("UserId", SharedPreferencesMethod.getUserId(this));
                            bundle.putString("SharedVia", "Message");
                            Utils.logEvent(EventNames.SHARE_RESUME, bundle, this);
//                            Toast.makeText(this, getResources().getString(R.string.resume_message_sent_success), Toast.LENGTH_SHORT).show();
                            Utils.showPopup(ShareResumeActivity.this, getResources().getString(R.string.resume_message_sent_success), new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                }
                            });

                        }
                    }
                }
            } else if (outPut.has(ResponseParameters.Errors)) { //error
                Snackbar.make(this.mToolbar, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT).show();
            } else { //error
                Snackbar.make(this.mToolbar, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
        }
    }
}
