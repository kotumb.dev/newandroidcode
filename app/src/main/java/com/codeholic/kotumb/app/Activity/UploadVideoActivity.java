package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utils;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSink;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;
import io.github.lizhangqu.coreprogress.ProgressHelper;
import io.github.lizhangqu.coreprogress.ProgressUIListener;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;


public class UploadVideoActivity extends AppCompatActivity implements Player.EventListener{
    private ProgressBar uploadProgress;
    private TextView txtPercentage,terms_error,header,tv_video_title,tv_terms_condtion,dialog_termsContionsHeading,dialog_termsConditionText;
    private TextInputLayout  et_videotitle_layout,et_video_description_layout;
    private File videofile;
    Toolbar mToolbar;
    CardView cv_recordVideo,cv_Upload;
    ImageView video_thumbnail,removeVideo,play_icon,dismiss_dialog,iv_fullscreen;
    RelativeLayout play_video_container;
    EditText etVideoTitle,etVideoDescription;
    String thumbnailPath="";
    CardView videoCard;
    private RelativeLayout rl_videoContainer,dialog_headerLayout;
    VideoView videoview;
    SimpleExoPlayer player;
    CheckBox termsConditions;
    WebView dialog_web_view;
    FrameLayout frameLayout,videoThumbnailFrame;
    LinearLayout progressLayout,dialog_termsContionsLayout,dialog_llLoading;
    private PlayerView playerView;
    FrameLayout addFullScreen;
    PlayerView fullscreeplay;
    SimpleExoPlayer fullplayer;
    Bitmap thumb;
    RecyclerView rv_like_list;
    ProgressDialog progressDialog;
    ProgressBar progressBar;
    MenuItem videoList, otherVideoList;
    int VIDEO_REQUEST_CODE=121;
    String termsCondtionMessage="";
    String Video_DIRECTORY = "";
    TelephonyManager mgr;
    PhoneStateListener phoneStateListener;
    IntentFilter intentFilter;



    private String key = "";
    BroadcastReceiver receiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            System.out.println("videoPath=="+intent.getStringExtra("path"));
            handleVideo(intent.getStringExtra("path"));
            iv_fullscreen.setVisibility(View.VISIBLE);
            key=intent.getStringExtra("path");
            iv_fullscreen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                    //videoview.pause();
                    if(player!=null){
                        player.setPlayWhenReady(false);
                    }
                    rl_videoContainer.setVisibility(View.VISIBLE);
                    addFullScreen.setVisibility(View.GONE);
                    showfullScreenPopup(key);
                }
            });
        }
    };

    boolean onCall=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_video);
        findViews();
        initToolbar();
      //  ObjectGraph.create(new DaggerDependencyModule(this)).inject(this);
        removeVideo.setVisibility(View.GONE);
        terms_error.setVisibility(View.GONE);
        play_video_container.setVisibility(View.VISIBLE);
        videoThumbnailFrame.setVisibility(View.VISIBLE);
        play_icon.setVisibility(View.VISIBLE);

        if(getIntent().hasExtra("path")){
            key=getIntent().getStringExtra("path");
            System.out.println("path=="+key);
            handleVideo(key);
            iv_fullscreen.setVisibility(View.VISIBLE);
            iv_fullscreen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                    //videoview.pause();
                    if(player!=null){
                        player.setPlayWhenReady(false);
                    }
                    rl_videoContainer.setVisibility(View.VISIBLE);
                    addFullScreen.setVisibility(View.GONE);
                    showfullScreenPopup(key);
                }
            });
        }
        termsConditionCheck();
        setOnClick();
        API.sendRequestToServerGET(UploadVideoActivity.this, API.VIDEO_LIMITS+"/"+SharedPreferencesMethod.getUserId(UploadVideoActivity.this), API.VIDEO_LIMITS);
        textWatcher(etVideoTitle,et_videotitle_layout,"Please Fill Video Title");
        intentFilter=new IntentFilter();
        intentFilter.addAction("videoPath");
        registerReceiver(receiver,intentFilter);

        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    //Incoming call: Pause music
                    if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M) {
                        System.out.println("onstop======11");
                        onCall=true;
                    }
                } else if(state == TelephonyManager.CALL_STATE_IDLE) {
                    //Not in call: Play music
                    onCall=false;
                } else if(state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    //A call is dialing, active or on hold
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        onCall=true;
                        System.out.println("onstop======12");
                    }
                }
                super.onCallStateChanged(state, incomingNumber);
            }
        };
        mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if(mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }

    }




    private void findViews(){
        cv_recordVideo=findViewById(R.id.cv_record);
        cv_Upload=findViewById(R.id.cv_upload);
        video_thumbnail=findViewById(R.id.video_thumbnail);
        removeVideo=findViewById(R.id.remove_video);
        videoCard = findViewById(R.id.video_card);
        playerView=findViewById(R.id.play_video);
        videoview = findViewById(R.id.videoview);
        iv_fullscreen=findViewById(R.id.iv_allow_fullscreen);
        addFullScreen=findViewById(R.id.main_media_frame);
        rl_videoContainer=findViewById(R.id.play_video_container);
        play_icon=findViewById(R.id.play_icon);
        etVideoDescription=findViewById(R.id.et_video_description);
        etVideoTitle=findViewById(R.id.et_videtitle);
        SpannableStringBuilder builder = new SpannableStringBuilder();

        builder.append("Video Title");
        int start = builder.length();
        builder.append("*");
        int end = builder.length();

        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        etVideoTitle.setHint(builder);
        termsConditions=findViewById(R.id.cb_terms_conditions);

        txtPercentage = findViewById(R.id.txtPercentage);
        uploadProgress = findViewById(R.id.upload_progress);
        progressLayout=findViewById(R.id.video_upload_progress);
        terms_error=findViewById(R.id.tv_terms_errror);
        mToolbar=findViewById(R.id.toolbar_actionbar);
        header=findViewById(R.id.header);
        et_videotitle_layout=findViewById(R.id.et_videotitle_layout);
        et_video_description_layout=findViewById(R.id.et_video_description_layout);

        tv_terms_condtion=findViewById(R.id.tv_terms_condtion);
        SpannableString content = new SpannableString(getResources().getString(R.string.terms_and_conditions_text));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tv_terms_condtion.setText(content);
        play_video_container=findViewById(R.id.play_video_container);
        videoThumbnailFrame=findViewById(R.id.videoThumbnailFrame);
        System.out.println("Upload URL  "+API.VIDEO_UPLOAD);
    }

    private void showfullScreenPopup(String path) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.video_terms_condition_layout, null);
        dialogBuilder.setView(dialogView);
        dialogfindViews(dialogView);
        dialog_termsContionsLayout.setVisibility(View.GONE);
        dialog_termsContionsHeading.setVisibility(View.GONE);
        dialog_termsConditionText.setVisibility(View.GONE);
        dialog_headerLayout.setVisibility(View.GONE);
        rv_like_list.setVisibility(View.GONE);
        dialog_llLoading.setVisibility(View.GONE);
        dialog_web_view.setVisibility(View.GONE);
        setFullplayer(Uri.parse(path));

        //activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        final AlertDialog confirmAlert = dialogBuilder.create();
        confirmAlert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                addFullScreen.setVisibility(View.VISIBLE);
                videoThumbnailFrame.setVisibility(View.VISIBLE);
                rl_videoContainer.setVisibility(View.VISIBLE);
                if(fullplayer!=null){
                    fullplayer.stop();
                    fullplayer=null;
                }
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();
    }

    private void setFullplayer(Uri url) {
        new File(getCacheDir(), "media").delete();
        fullplayer=null;
        if (fullplayer == null) {
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            fullplayer= ExoPlayerFactory.newSimpleInstance(this, trackSelector);
            MediaSource audioSource = new ExtractorMediaSource(url,
                    new CacheDataSourceFactory(this, 100 * 1024 * 1024, 5 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
            fullplayer.setPlayWhenReady(true);
            fullplayer.prepare(audioSource);
            fullscreeplay.setPlayer(fullplayer);
            fullplayer.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
//                    holder.player.stop();
                }

                @Override
                public void onLoadingChanged(boolean isLoading) {
                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    switch (playbackState) {
                        case Player.STATE_BUFFERING:
                            break;
                        case Player.STATE_ENDED:

                            break;
                        case Player.STATE_IDLE:

                            break;
                        case Player.STATE_READY:
                            break;
                        default:
                            break;
                    }

                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {
                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {
                }
            });
        }
    }

    public class CacheDataSourceFactory implements DataSource.Factory {
        private final Context context;
        private final DefaultDataSourceFactory defaultDatasourceFactory;
        private final long maxFileSize, maxCacheSize;

        CacheDataSourceFactory(Context context, long maxCacheSize, long maxFileSize) {
            super();
            this.context = context;
            this.maxCacheSize = maxCacheSize;
            this.maxFileSize = maxFileSize;
            String userAgent = Util.getUserAgent(context, context.getString(R.string.app_name));
            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            defaultDatasourceFactory = new DefaultDataSourceFactory(this.context,
                    bandwidthMeter,
                    new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter));
        }


        @Override
        public DataSource createDataSource() {
            LeastRecentlyUsedCacheEvictor evictor = new LeastRecentlyUsedCacheEvictor(maxCacheSize);
            SimpleCache simpleCache = new SimpleCache(new File(context.getCacheDir(), "media"), evictor);
            return new CacheDataSource(simpleCache, defaultDatasourceFactory.createDataSource(),
                    new FileDataSource(), new CacheDataSink(simpleCache, maxFileSize),
                    CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR, null);
        }
    }


    private void dialogfindViews(View dialogView){
        dialog_web_view = dialogView.findViewById(R.id.web_view);
        dialog_termsContionsLayout = dialogView.findViewById(R.id.condition_message_layout);
        dialog_termsContionsHeading = dialogView.findViewById(R.id.condtion_heading);
        dialog_termsConditionText=dialogView.findViewById(R.id.condtion_text);
        dialog_headerLayout=dialogView.findViewById(R.id.header_layout);
        dismiss_dialog=dialogView.findViewById(R.id.dismiss_dialog);
        rv_like_list=dialogView.findViewById(R.id.rv_user_like_list);
        dialog_llLoading=dialogView.findViewById(R.id.llLoading);
        frameLayout=dialogView.findViewById(R.id.frameLayout);
        fullscreeplay=dialogView.findViewById(R.id.fullscreeplay);
        rv_like_list=dialogView.findViewById(R.id.rv_user_like_list);
        progressBar=dialogView.findViewById(R.id.video_loader);
    }


    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.upload_videos_text));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }




    @Override
    public void onBackPressed()
    {super.onBackPressed();
        // code here to show dialog
        finish();
         // optional depending on your needs
    }






    private void textWatcher(final EditText text,final TextInputLayout textInputLayout,final String error){
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               /* if (!text.getText().toString().isEmpty()){
                    textInputLayout.setErrorEnabled(false);
                }else{
                   // textInputLayout.setErrorEnabled(false);
                    //textInputLayout.setError(error);
                    //tv_video_title.setText(getResources().getString(R.string.video_title_heading));
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

String terms="",video1="",video2="";
    int usedLimit=0,availableLimit=0,totalLimit_used=0,totalLimit=0;
    public void getResponse(final JSONObject outPut, int i) {
        System.out.println("Video Limits Response   "+outPut.toString());
        if (i==3){
            try{
                termsCondtionMessage=outPut.getString("video_terms_conditions");
                usedLimit=Integer.valueOf(outPut.getString("user_uplopad_video_limit_used"));
                availableLimit=Integer.valueOf(outPut.getString("user_upload_video_limit"));
                totalLimit_used=Integer.valueOf(outPut.getString("total_user_upload_video_limit_used"));
                totalLimit=Integer.valueOf(outPut.getString("total_user_upload_video_limit"));
                System.out.println("LIMIT==="+usedLimit+"==="+availableLimit+"===="+totalLimit);
                terms=outPut.getString("video_terms_conditions");
                SharedPreferencesMethod.setString(this,"INSIDE_SHARE",outPut.getString("inside_message_on_video_approved_and_published"));
                SharedPreferencesMethod.setString(this,"OUTSIDE_SHARE",outPut.getString("outside_message_on_video_approved_and_published"));
                video1=outPut.getString("video_upload_limit_exceed_popup_message");
                video2=outPut.getString("user_video_upload_limit_exceed_popup_message");
                if (totalLimit==0){
                    Utils.showPopup(UploadVideoActivity.this,outPut.getString("video_upload_limit_exceed_popup_message"));
                }else{
                    cv_recordVideo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (usedLimit < availableLimit){
                                /*Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                takeVideoIntent.putExtra(android.provider.MediaStore.EXTRA_VIDEO_QUALITY, 1);
                                takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
                                takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Environment.getExternalStorageState());
                                if(takeVideoIntent.resolveActivity(getPackageManager())!=null){
                                    startActivityForResult(takeVideoIntent, VIDEO_REQUEST_CODE);
                                }*/
                                if(!onCall) {
                                    Intent intent = new Intent(UploadVideoActivity.this, CameraActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                else {
                                    Utils.showPopup(UploadVideoActivity.this,"Your call is currently active.");
                                }
                            }
                            else if(totalLimit==totalLimit_used){
                               try {
                                    Utils.showPopup(UploadVideoActivity.this,outPut.getString("video_upload_limit_exceed_popup_message"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            else{
                                try {
                                    Utils.showPopup(UploadVideoActivity.this,outPut.getString("user_video_upload_limit_exceed_popup_message"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });



                    play_icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (key.length() > 0) {
                                playVideo(Uri.parse(key));
                                play_video_container.setVisibility(View.VISIBLE);
                                playerView.setVisibility(View.VISIBLE);
                                play_icon.setVisibility(View.GONE);
                                video_thumbnail.setVisibility(View.GONE);
                            } else {
                                if (usedLimit < availableLimit){
                                    /*Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                    takeVideoIntent.putExtra(android.provider.MediaStore.EXTRA_VIDEO_QUALITY, 1);
                                    takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
                                    takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Environment.getExternalStorageState());
                                    if(takeVideoIntent.resolveActivity(getPackageManager())!=null){
                                        startActivityForResult(takeVideoIntent, VIDEO_REQUEST_CODE);
                                    }*/
                                    if(!onCall) {
                                        Intent intent = new Intent(UploadVideoActivity.this, CameraActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                    else {
                                        Utils.showPopup(UploadVideoActivity.this,"Your call is currently active.");
                                    }
                                }
                                else if(totalLimit==totalLimit_used){

                                    try {
                                        Utils.showPopup(UploadVideoActivity.this,outPut.getString("video_upload_limit_exceed_popup_message"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                else{
                                    try {
                                        Utils.showPopup(UploadVideoActivity.this,outPut.getString("user_video_upload_limit_exceed_popup_message"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    });
                }
            }catch (Exception ex){
                ex.getMessage();
                ex.printStackTrace();
            }
        }
    }


    private void termsConditionCheck(){
        termsConditions.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    terms_error.setVisibility(View.GONE);
                }
            }
        });
    }





    private void setOnClick(){
        cv_Upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(etVideoTitle.getText().toString().isEmpty()||etVideoTitle.getText().toString().equalsIgnoreCase("")) {
                    et_videotitle_layout.setErrorEnabled(true);
                    et_videotitle_layout.setError(getResources().getString(R.string.video_title_error_text));
                }else if (!termsConditions.isChecked()){
                    terms_error.setVisibility(View.VISIBLE);
                    Utils.showPopup(UploadVideoActivity.this,"Please check the Terms and condition");
                }else{

                        terms_error.setVisibility(View.GONE);
                        etVideoTitle.setError(null);
                        et_videotitle_layout.setError(null);
                        et_videotitle_layout.setErrorEnabled(false);
                        et_videotitle_layout.setErrorEnabled(false);
                        et_video_description_layout.setErrorEnabled(false);
                        uploadVideoServer();

                }

            }
        });
        removeVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Inside remove icon--------------------------------------------");
                videoview.setVisibility(View.GONE);
                if(player!=null){
                    player.stop();
                    player.release();
                }
                play_video_container.setVisibility(View.GONE);
                playerView.setVisibility(View.GONE);
                videoThumbnailFrame.setVisibility(View.VISIBLE);
                play_icon.setVisibility(View.VISIBLE);
                iv_fullscreen.setVisibility(View.GONE);
                removeVideo.setVisibility(View.GONE);
                key="";
                cv_Upload.setVisibility(View.GONE);
                video_thumbnail.setImageBitmap(null);
                cv_recordVideo.setVisibility(View.VISIBLE);
                etVideoDescription.setText("");
                etVideoTitle.setText("");
            }
        });

        tv_terms_condtion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              termsConditionPopup(UploadVideoActivity.this);
            }
        });
    }









    private void termsConditionPopup(Activity activity){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.video_terms_condition_layout, null);
        dialogBuilder.setView(dialogView);
        dialogfindViews(dialogView);
        dialog_termsContionsLayout.setVisibility(View.VISIBLE);
        dialog_termsContionsHeading.setVisibility(View.VISIBLE);
        dialog_termsConditionText.setVisibility(View.VISIBLE);
        rv_like_list.setVisibility(View.GONE);
        dialog_web_view.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
        dialog_termsConditionText.setText(Html.fromHtml(terms));
        final AlertDialog confirmAlert = dialogBuilder.create();
        dismiss_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();
        dialog_llLoading.setVisibility(View.GONE);
    }




    private void handleVideo(final String filePath) {

        videofile = new File(filePath);
        DisplayMetrics dm;
        DecimalFormat decimalFormat=new DecimalFormat("0.00");
        double size=(double) videofile.length()/(1024*1024);
        Utils.showPopup(this,"Video size: "+decimalFormat.format(size)+" MB");

        //videoview.setVisibility(View.VISIBLE);
        video_thumbnail.setVisibility(View.VISIBLE);
        videoCard.setVisibility(View.VISIBLE);
        cv_recordVideo.setVisibility(View.GONE);
        play_icon.setVisibility(View.VISIBLE);
        cv_Upload.setVisibility(View.VISIBLE);
        removeVideo.setVisibility(View.VISIBLE);
        videoThumbnailFrame.setVisibility(View.VISIBLE);
        final Uri uri = Uri.fromFile(videofile);
        System.out.println("filepath=="+filePath+"==="+size+"==="+uri+"=="+videofile.getAbsolutePath());
        //videoview.setVideoURI(uri);
        MediaController mc = new MediaController(this);
        videoview.setMediaController(mc);
        videoview.setVisibility(View.GONE);
        thumbnailPath=filePath;
        Glide.with(this).asBitmap()
                .load(filePath)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        thumb=resource;
                        video_thumbnail.setImageBitmap(thumb);
                    }
                });
        //thumb = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
        video_thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //videoview.setVisibility(View.VISIBLE);
                //videoview.start();
                playVideo(uri);
                play_video_container.setVisibility(View.VISIBLE);
                playerView.setVisibility(View.VISIBLE);
                play_icon.setVisibility(View.GONE);
                video_thumbnail.setVisibility(View.GONE);
            }
        });
    }











    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }
        if(progressDialog!=null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
       if(videoview!=null){
           videoview=null;
       }
       if(fullplayer!=null){
           fullplayer.stop();
       }
       unregisterReceiver(receiver);
    }


    private void uploadVideoServer(){

        if(isNetworkAvailable()) {
            progressLayout.setVisibility(View.VISIBLE);
            cv_Upload.setVisibility(View.GONE);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            thumb.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
            System.out.println("BASE64==" + encoded);
            cv_recordVideo.setVisibility(View.GONE);
            try {
                System.out.println("videopath == " + videofile.getPath() + "name  " + videofile.getName());
                OkHttpClient client = new OkHttpClient.Builder().connectTimeout(1600, TimeUnit.SECONDS).writeTimeout(1600, TimeUnit.SECONDS).readTimeout(1600, TimeUnit.SECONDS).build();
                MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
                builder.addFormDataPart(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(this));
                builder.addFormDataPart(RequestParameters.TITLE, "" + etVideoTitle.getText().toString().trim());
                builder.addFormDataPart(RequestParameters.DESCRIPTION, "" + etVideoDescription.getText().toString().trim());
                builder.addFormDataPart(ResponseParameters.MEDIA_TYPE, "" + Utils.getExtension(videofile.getPath()));

                final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
                builder.addFormDataPart(ResponseParameters.THUMBNAIL, encoded);
                builder.addFormDataPart(ResponseParameters.MEDIA, videofile.getName(), RequestBody.create(MediaType.parse("video/mp4"), videofile));
                MultipartBody build = builder.build();
                Log.e("USER Id is : ", "" + SharedPreferencesMethod.getUserId(this) + "MEDIAType : " + Utils.getExtension(videofile.getPath()) + "THUMBNAIL is :  " + encoded + "MEDIA is : " +videofile );

                RequestBody requestBody = ProgressHelper.withProgress(build, new ProgressUIListener() {
                    @Override
                    public void onUIProgressStart(long totalBytes) {
                        super.onUIProgressStart(totalBytes);

                        Log.e("UPLOAD","Request is :" + build.toString());
                    }

                    @Override
                    public void onUIProgressChanged(long numBytes, long totalBytes, float percent, float speed) {
                        uploadProgress.setProgress((int) (100 * percent));
                        txtPercentage.setText("Uploading...");
                    }

                    @Override
                    public void onUIProgressFinish() {
                        super.onUIProgressFinish();
                        txtPercentage.setText(getResources().getString(R.string.please_wait_text) + "...");
                        progressDialog = new ProgressDialog(UploadVideoActivity.this);
                        progressDialog.setMessage(getResources().getString(R.string.video_upload_ready_text));
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                    }
                });
                okhttp3.Request request = new okhttp3.Request.Builder()
                        .url(API.VIDEO_UPLOAD)
                        .post(requestBody)
                        .build();
                System.out.println("Input Data  " + request.body().toString() + "   " + "  " + build.parts() + "  " + build.toString());
                client.newCall(request).enqueue(new Callback() {
                    Handler mainHandler = new Handler(UploadVideoActivity.this.getMainLooper());

                    @Override
                    public void onFailure(Call call, final IOException e) {
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                System.out.println("Video Upload Error  " + e.getMessage());
                                Utils.showPopup(UploadVideoActivity.this, getResources().getString(R.string.video_server_error_text));
                                cv_Upload.setVisibility(View.VISIBLE);
                                progressLayout.setVisibility(View.GONE);
                                progressDialog.dismiss();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        System.out.println("Upload URL  " + response);
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (!response.isSuccessful()) {
                                    Utils.showPopup(UploadVideoActivity.this, getResources().getString(R.string.something_wrong));
                                    progressLayout.setVisibility(View.GONE);
                                    cv_Upload.setVisibility(View.VISIBLE);
                                    cv_recordVideo.setVisibility(View.GONE);
                                    txtPercentage.setText(getResources().getString(R.string.video_upload_faild_text));
                                    progressDialog.dismiss();
                                    removeVideo.setVisibility(View.GONE);
                                } else {

                                    try {
                                        String resStr = response.body().toString();
                                        System.out.println("VideoResponse== " + response.body().toString());
                                        JSONObject json = new JSONObject(resStr);

                                        if (response.code() == 200){

                                            txtPercentage.setText(getResources().getString(R.string.video_upload_complete_text));
                                            progressLayout.setVisibility(View.GONE);
                                            cv_Upload.setVisibility(View.GONE);
                                            et_videotitle_layout.setError(null);
                                            key = "";
                                            removeVideo.setVisibility(View.GONE);
                                            et_videotitle_layout.setErrorEnabled(false);
                                            et_video_description_layout.setErrorEnabled(false);
                                            cv_recordVideo.setVisibility(View.VISIBLE);
                                            progressDialog.dismiss();
                                       /* if(!SharedPreferencesMethod.getBoolean(UploadVideoActivity.this,"FirstTimeCamera")){
                                            SharedPreferencesMethod.setBoolean(UploadVideoActivity.this,"FirstTimeCamera",true);
                                        }*/
                                            videoview.setVisibility(View.GONE);
                                            Utils.showPopup(UploadVideoActivity.this, getResources().getString(R.string.video_success_message) + "\n" + getResources().getString(R.string.video_approval_message), new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Intent intent = new Intent();
                                                    intent.setAction("refresh_myVideos");
                                                    intent.putExtra("refresh", "refresh_myVideos");
                                                    sendBroadcast(intent);
                                                    finish();
                                                }
                                            });
                                            tv_terms_condtion.setClickable(false);
                                            etVideoDescription.setText("");
                                            etVideoTitle.setText("");
                                            //tv_video_title.setText(getResources().getString(R.string.video_title_heading));
                                            video_thumbnail.setVisibility(View.GONE);
                                            play_icon.setVisibility(View.VISIBLE);
                                            if (player != null) {
                                                player.stop();
                                                player.release();
                                            }
                                            play_video_container.setVisibility(View.GONE);
                                            playerView.setVisibility(View.GONE);
                                            videoThumbnailFrame.setVisibility(View.VISIBLE);
                                            play_icon.setVisibility(View.VISIBLE);
                                            iv_fullscreen.setVisibility(View.GONE);
                                            removeVideo.setVisibility(View.GONE);
                                            key = "";
                                            cv_Upload.setVisibility(View.GONE);
                                            video_thumbnail.setImageBitmap(null);
                                            cv_recordVideo.setVisibility(View.VISIBLE);
                                            etVideoDescription.setText("");
                                            etVideoTitle.setText("");
                                            API.sendRequestToServerGET(UploadVideoActivity.this, API.VIDEO_LIMITS + "/" + SharedPreferencesMethod.getUserId(UploadVideoActivity.this), API.VIDEO_LIMITS);

                                        }


//                                        if (json.has(ResponseParameters.Success)) {
//                                            txtPercentage.setText(getResources().getString(R.string.video_upload_complete_text));
//                                            progressLayout.setVisibility(View.GONE);
//                                            cv_Upload.setVisibility(View.GONE);
//                                            et_videotitle_layout.setError(null);
//                                            key = "";
//                                            removeVideo.setVisibility(View.GONE);
//                                            et_videotitle_layout.setErrorEnabled(false);
//                                            et_video_description_layout.setErrorEnabled(false);
//                                            cv_recordVideo.setVisibility(View.VISIBLE);
//                                            progressDialog.dismiss();
//                                       /* if(!SharedPreferencesMethod.getBoolean(UploadVideoActivity.this,"FirstTimeCamera")){
//                                            SharedPreferencesMethod.setBoolean(UploadVideoActivity.this,"FirstTimeCamera",true);
//                                        }*/
//                                            videoview.setVisibility(View.GONE);
//                                            Utils.showPopup(UploadVideoActivity.this, getResources().getString(R.string.video_success_message) + "\n" + getResources().getString(R.string.video_approval_message), new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//                                                    Intent intent = new Intent();
//                                                    intent.setAction("refresh_myVideos");
//                                                    intent.putExtra("refresh", "refresh_myVideos");
//                                                    sendBroadcast(intent);
//                                                    finish();
//                                                }
//                                            });
//                                            tv_terms_condtion.setClickable(false);
//                                            etVideoDescription.setText("");
//                                            etVideoTitle.setText("");
//                                            //tv_video_title.setText(getResources().getString(R.string.video_title_heading));
//                                            video_thumbnail.setVisibility(View.GONE);
//                                            play_icon.setVisibility(View.VISIBLE);
//                                            if (player != null) {
//                                                player.stop();
//                                                player.release();
//                                            }
//                                            play_video_container.setVisibility(View.GONE);
//                                            playerView.setVisibility(View.GONE);
//                                            videoThumbnailFrame.setVisibility(View.VISIBLE);
//                                            play_icon.setVisibility(View.VISIBLE);
//                                            iv_fullscreen.setVisibility(View.GONE);
//                                            removeVideo.setVisibility(View.GONE);
//                                            key = "";
//                                            cv_Upload.setVisibility(View.GONE);
//                                            video_thumbnail.setImageBitmap(null);
//                                            cv_recordVideo.setVisibility(View.VISIBLE);
//                                            etVideoDescription.setText("");
//                                            etVideoTitle.setText("");
//                                            API.sendRequestToServerGET(UploadVideoActivity.this, API.VIDEO_LIMITS + "/" + SharedPreferencesMethod.getUserId(UploadVideoActivity.this), API.VIDEO_LIMITS);
//                                        }
                                        else {
                                            progressLayout.setVisibility(View.GONE);
                                            cv_Upload.setVisibility(View.VISIBLE);
                                            cv_recordVideo.setVisibility(View.GONE);
                                            progressDialog.dismiss();
                                            new AlertDialog.Builder(UploadVideoActivity.this).setMessage(response.code())
                                                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            uploadVideoServer();
                                                            dialog.dismiss();
                                                        }
                                                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            }).show();
                                        }
                                    } catch (Exception ex) {
                                        System.out.println("Video wrong  " + ex.getMessage() + "  " + ex.toString());
                                        //  Utils.showPopup(UploadVideoActivity.this,"Some error occur while uploading");
                                        progressLayout.setVisibility(View.GONE);
                                        cv_Upload.setVisibility(View.GONE);
                                        cv_recordVideo.setVisibility(View.VISIBLE);
                                        progressDialog.dismiss();
                                        tv_terms_condtion.setClickable(false);
                                        etVideoDescription.setText("");
                                        key = "";
                                        etVideoTitle.setText("");
//                                    tv_video_title.setText(getResources().getString(R.string.video_title_heading));
                                        video_thumbnail.setVisibility(View.GONE);
                                        play_icon.setVisibility(View.VISIBLE);
                                    }

                                }
                            }
                        });

                    }
                });
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        else{
            Utils.showPopup(this,"Please check your internet connect");
        }

    }


    private void initializePlayer() {
        player=null;
        if (player == null) {
            // 1. Create a default TrackSelector
            LoadControl loadControl = new DefaultLoadControl(
                    new DefaultAllocator(true, 20),
                    3000,
                    5000,
                    1500,
                    5000, -1, true);

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);
            // 2. Create the player
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this), trackSelector, loadControl);
            playerView.setPlayer(player);
            //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN |View.SYSTEM_UI_FLAG_IMMERSIVE);
        }
    }



    private void playVideo(Uri videoUri) {
        initializePlayer();
        if (videoUri == null) {
            return;
        }
        buildMediaSource(videoUri);

    }


    private void buildMediaSource(Uri mUri) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getResources().getString(R.string.app_name)), bandwidthMeter);
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);
        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        player.addListener(this);
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {

            case Player.STATE_BUFFERING:
                break;
            case Player.STATE_ENDED:
                break;
            case Player.STATE_IDLE:

                break;
            case Player.STATE_READY:

                break;
            default:
                break;
        }
        if (playWhenReady && playbackState == Player.STATE_READY) {
            // media actually playing
            play_icon.setVisibility(View.GONE);
        } else if (playWhenReady) {
            // might be idle (plays after prepare()),
            // buffering (plays when data available)
            // or ended (plays when seek away from end)
        } else {
            // player paused in any state
        }
    }





    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
