package com.codeholic.kotumb.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Adapter.UserVideoListAdapter;
import com.codeholic.kotumb.app.Model.VideoData;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;


public class UserVideoListActivity extends AppCompatActivity {
    List<VideoData> videoDataList=new ArrayList<>();
    Context context;
    private Toolbar mToolbar;
    private TextView header,tv_video_notfound;
    int pagination = 0;
    boolean loading=true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private RecyclerView rv_video_list;
    private LinearLayoutManager linearLayoutManager = null;
    private UserVideoListAdapter userVideoListAdapter;
    private LinearLayout llLoading,mainLayout,llDataNotFound;
    private ProgressBar pagination_loader;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String URL="";
    public int video_position=-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_video_list);
        findViews();
        initStartups();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        userVideoListAdapter.stopVideo();
    }

    @Override
    protected void onStop() {
        super.onStop();
        userVideoListAdapter.pauseAllVideo();
    }


    private void initStartups(){
        context=this;
        Intent intent = getIntent();
//        URL=  API.MY_VIDEO_GET+ SharedPreferencesMethod.getUserId(context);
//        API.sendRequestToServerGET(context, URL+"/"+pagination, API.MY_VIDEO_GET);
//        initToolbar("My Video List");
//        onRefresh(URL,API.MY_VIDEO_GET);
        if (intent.hasExtra("my_video")){
            URL=  API.MY_VIDEO_GET+ SharedPreferencesMethod.getUserId(context);
            API.sendRequestToServerGET(context, URL+"/"+pagination, API.MY_VIDEO_GET);
            initToolbar("My Video List");
            onRefresh(URL,API.MY_VIDEO_GET);
        }else if(intent.hasExtra("other_video")){
            API.sendRequestToServerGET(context, API.VIDEO_PUBLISH,API.VIDEO_PUBLISH);
            initToolbar("My Publish Video List");
            onRefresh(API.VIDEO_PUBLISH,API.VIDEO_PUBLISH);
        }

    }


    private void onRefresh(final String URL, final String input){
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                videoDataList.clear();
                pagination=0;
                userVideoListAdapter.notifyDataSetChanged();
                rv_video_list.refreshDrawableState();
                rv_video_list.invalidate();
                API.sendRequestToServerGET(context, URL+"/"+pagination,input);
            }
        });
    }


    private void findViews(){
        mToolbar=findViewById(R.id.toolbar_actionbar);
        header=findViewById(R.id.header);
        rv_video_list=findViewById(R.id.rv_video_list);
        swipeRefreshLayout=findViewById(R.id.swipelayout);
        mainLayout=findViewById(R.id.mainLayout);
        llLoading=findViewById(R.id.llLoading);
        llDataNotFound=findViewById(R.id.llDataNotFound);
        tv_video_notfound=findViewById(R.id.tv_video_notfound);
        pagination_loader=findViewById(R.id.pagination_loader);
    }



    public void getResponse(JSONObject outPut, int i){
        if (i==0){
            System.out.println("Video List Response   "+outPut.toString());
            if (outPut.has(ResponseParameters.Success)){
                llLoading.setVisibility(View.GONE);
                mainLayout.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                try{
                    if (outPut.getJSONArray("videos").length()>0){
                        for (int j=0; j<outPut.getJSONArray("videos").length(); j++){
                            JSONObject videoObj = outPut.getJSONArray("videos").getJSONObject(j);
                            VideoData videoData=new VideoData();
                            System.out.println("Video Array Response    "+videoObj.getString(ResponseParameters.TITLE)+"=="+videoObj.getString(ResponseParameters.Approved));
                            videoData.setUserFirstName(videoObj.getString(ResponseParameters.FirstName));
                            videoData.setUserLastName(videoObj.getString(ResponseParameters.LastName));
                            videoData.setVideoTitle(videoObj.getString(ResponseParameters.TITLE));
                            videoData.setVideoDescription(videoObj.getString(ResponseParameters.DESCRIPTION));
                            videoData.setVideoName(videoObj.getString(ResponseParameters.MEDIA));
                            videoData.setVideoType(videoObj.getString(ResponseParameters.MEDIA_TYPE));
                            videoData.setUserAvatar(videoObj.getString(ResponseParameters.Avatar));
                            videoData.setVideoUploadedDate(videoObj.getString(ResponseParameters.CREATED_AT));
                            videoData.setVideoEndDate(videoObj.getString(ResponseParameters.END_DATE));
                            videoData.setVideoId(videoObj.getString(ResponseParameters.Id));
                            videoData.setUserId(videoObj.getString(ResponseParameters.UserId));
                            videoData.setVideoLikeCount(videoObj.getString(ResponseParameters.LIKE_COUNT));
                            videoData.setVideoStatus(videoObj.getString(ResponseParameters.STATUS));
                            videoData.setVideoApproved(videoObj.getString(ResponseParameters.Approved));
                            videoData.setVideoPublish(videoObj.getString(ResponseParameters.Publish));
                            videoData.setVideoThumbnail(videoObj.getString(ResponseParameters.THUMBNAIL));
                            videoDataList.add(videoData);
                        }

                    }
                    if(outPut.getJSONArray("archived_videos").length()>0){
                        for (int j=0; j<outPut.getJSONArray("archived_videos").length(); j++){
                            JSONObject videoObj = outPut.getJSONArray("archived_videos").getJSONObject(j);
                            VideoData videoData=new VideoData();
                            System.out.println("Video Archived Response    "+videoObj.getString(ResponseParameters.TITLE));
                            videoData.setUserFirstName(videoObj.getString(ResponseParameters.FirstName));
                            videoData.setUserLastName(videoObj.getString(ResponseParameters.LastName));
                            videoData.setVideoTitle(videoObj.getString(ResponseParameters.TITLE));
                            videoData.setVideoDescription(videoObj.getString(ResponseParameters.DESCRIPTION));
                            videoData.setVideoName(videoObj.getString(ResponseParameters.MEDIA));
                            videoData.setVideoType(videoObj.getString(ResponseParameters.MEDIA_TYPE));
                            videoData.setUserAvatar(videoObj.getString(ResponseParameters.Avatar));
                            videoData.setVideoUploadedDate(videoObj.getString(ResponseParameters.CREATED_AT));
                            videoData.setVideoEndDate(videoObj.getString(ResponseParameters.END_DATE));
                            videoData.setVideoId(videoObj.getString(ResponseParameters.Id));
                            videoData.setUserId(videoObj.getString(ResponseParameters.UserId));
                            videoData.setVideoLikeCount(videoObj.getString(ResponseParameters.LIKE_COUNT));
                            videoData.setVideoStatus(videoObj.getString(ResponseParameters.STATUS));
                            videoData.setVideoApproved(videoObj.getString(ResponseParameters.Approved));
                            videoData.setVideoPublish(videoObj.getString(ResponseParameters.Publish));
                            videoData.setVideoThumbnail(videoObj.getString(ResponseParameters.THUMBNAIL));
                            videoDataList.add(videoData);
                        }
                    }
                    if(outPut.getJSONArray("videos").length()==0 && outPut.getJSONArray("archived_videos").length()==0){
                        llLoading.setVisibility(View.GONE);
                        llDataNotFound.setVisibility(View.VISIBLE);
                        String s=outPut.getString("message");
                        tv_video_notfound.setText(s);
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }
                linearLayoutManager = new LinearLayoutManager(context);
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                rv_video_list.setLayoutManager(linearLayoutManager);
                userVideoListAdapter = new UserVideoListAdapter(this, videoDataList);
                userVideoListAdapter.notifyDataSetChanged();
                rv_video_list.setAdapter(userVideoListAdapter);
                rv_video_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);

                    }

                    @Override
                    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        if (loading) {
                            visibleItemCount = linearLayoutManager.getChildCount();
                            totalItemCount = linearLayoutManager.getItemCount();
                            pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                loading = false;
                                pagination = pagination + 10;
                                pagination_loader.setVisibility(View.VISIBLE);
                                fetchData(URL+"/"+pagination);
                            }
                        }
                    }
                });
            }else{
                llLoading.setVisibility(View.GONE);
                llDataNotFound.setVisibility(View.VISIBLE);
                tv_video_notfound.setText(getResources().getString(R.string.video_server_error_text));
                System.out.println("Video List Response Error  "+outPut.toString());
            }
        }


        if (i==4){
            llLoading.setVisibility(View.GONE);
            mainLayout.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setRefreshing(false);
//            {"success":"success","published_videos":[],"RP_MESSAGE":"ALL_OKAY"}
            System.out.println("Publish Video Response   "+outPut.toString());
            if (outPut.has(ResponseParameters.Success)){
                llLoading.setVisibility(View.GONE);
                mainLayout.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                try{
                    if (outPut.getJSONArray("published_videos").getJSONObject(0).has("")){
                        tv_video_notfound.setVisibility(View.VISIBLE);
                        tv_video_notfound.setText("No Publish Videos Found");
                    }else{

                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                    ex.getMessage();
                }
            }
        }

    }









    private void fetchData(final String URL){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                LoadMoreData(URL);
            }
        },500);

    }





    private void LoadMoreData(String URL){
        loading=true;
        new AQuery(UserVideoListActivity.this).ajax(URL,JSONObject.class,new AjaxCallback<JSONObject>()
                {
                    @Override
                    public void callback(String url, JSONObject outPut, AjaxStatus status) {
                        super.callback(url, outPut, status);
                        try{
                            pagination_loader.setVisibility(View.GONE);
                            for (int j=0; j<outPut.getJSONArray("videos").length(); j++){
                                JSONObject videoObj = outPut.getJSONArray("videos").getJSONObject(j);
                                VideoData videoData=new VideoData();
                                System.out.println("Video Array Response    "+videoObj.getString(ResponseParameters.TITLE));
                                videoData.setUserFirstName(videoObj.getString(ResponseParameters.FirstName));
                                videoData.setUserLastName(videoObj.getString(ResponseParameters.LastName));
                                videoData.setVideoTitle(videoObj.getString(ResponseParameters.TITLE));
                                videoData.setVideoDescription(videoObj.getString(ResponseParameters.DESCRIPTION));
                                videoData.setVideoName(videoObj.getString(ResponseParameters.MEDIA));
                                videoData.setVideoType(videoObj.getString(ResponseParameters.MEDIA_TYPE));
                                videoData.setUserAvatar(videoObj.getString(ResponseParameters.Avatar));
                                videoData.setVideoUploadedDate(videoObj.getString(ResponseParameters.CREATED_AT));
                                videoData.setVideoEndDate(videoObj.getString(ResponseParameters.END_DATE));
                                videoData.setVideoId(videoObj.getString(ResponseParameters.Id));
                                videoData.setUserId(videoObj.getString(ResponseParameters.UserId));
                                videoData.setVideoLikeCount(videoObj.getString(ResponseParameters.LIKE_COUNT));
                                videoData.setVideoStatus(videoObj.getString(ResponseParameters.STATUS));
                                videoData.setVideoApproved(videoObj.getString(ResponseParameters.Approved));
                                videoData.setVideoPublish(videoObj.getString(ResponseParameters.Publish));
                                videoData.setVideoThumbnail(videoObj.getString(ResponseParameters.THUMBNAIL));
                                videoDataList.add(videoData);
                            }
                            userVideoListAdapter.notifyDataSetChanged();
                        }catch (Exception ex){
                            ex.getMessage();
                            ex.printStackTrace();
                        }
                    }
                });
    }






    private void initToolbar(String text) {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(text);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
