package com.codeholic.kotumb.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class GalleryData implements Parcelable {

    String galleryId="";
    String galleryUserId="";
    String galleryMedia="";
    String galleryMediaCreated="";
    String galleryMediaUpdated="";


    public GalleryData(){

    }


    protected GalleryData(Parcel in) {
        galleryId = in.readString();
        galleryUserId = in.readString();
        galleryMedia = in.readString();
        galleryMediaCreated = in.readString();
        galleryMediaUpdated = in.readString();
    }

    public static final Creator<GalleryData> CREATOR = new Creator<GalleryData>() {
        @Override
        public GalleryData createFromParcel(Parcel in) {
            return new GalleryData(in);
        }

        @Override
        public GalleryData[] newArray(int size) {
            return new GalleryData[size];
        }
    };

    public void setGalleryId(String galleryId){
        this.galleryId=galleryId;
    }
     public String getGalleryId(){
        return galleryId;
     }



    public void setGalleryUserId(String galleryUserId){
        this.galleryUserId=galleryUserId;
    }
    public String getGalleryUserId(){
        return galleryUserId;
    }




    public void setGalleryMedia(String galleryMedia){
        this.galleryMedia=galleryMedia;
    }
    public String getGalleryMedia(){
        return galleryMedia;
    }



    public void setGalleryMediaCreated(String galleryMediaCreated){
        this.galleryMediaCreated=galleryMediaCreated;
    }
    public String getGalleryMediaCreated(){
        return galleryMediaCreated;
    }



    public void setGalleryMediaUpdated(String galleryMediaUpdated){
        this.galleryMediaUpdated=galleryMediaUpdated;
    }
    public String getGalleryMediaUpdated(){
        return galleryMediaUpdated;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(galleryId);
        dest.writeString(galleryUserId);
        dest.writeString(galleryMedia);
        dest.writeString(galleryMediaCreated);
        dest.writeString(galleryMediaUpdated);
    }
}
