package com.codeholic.kotumb.app.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Adapter.EducationListAdapter;
import com.codeholic.kotumb.app.Adapter.ProfessionListAdapter;
import com.codeholic.kotumb.app.Adapter.RecomRecyclerViewAdapter;
import com.codeholic.kotumb.app.Adapter.UserRecyclerViewAdapter;
import com.codeholic.kotumb.app.Model.Education;
import com.codeholic.kotumb.app.Model.Profession;
import com.codeholic.kotumb.app.Model.Recommendations;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.EventNames;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.github.florent37.viewtooltip.ViewTooltip;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.github.florent37.viewtooltip.ViewTooltip.ALIGN.START;
import static com.github.florent37.viewtooltip.ViewTooltip.Position.TOP;

public class OtherUserProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private static UserRecyclerViewAdapter.UserViewHolder holder;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.tvdob)
    TextView tvdob;

    @BindView(R.id.tvgender)
    TextView tvgender;

    @BindView(R.id.tvchallenged)
    TextView tvchallenged;

    @BindView(R.id.llgender)
    LinearLayout llgender;

    @BindView(R.id.lldob)
    LinearLayout lldob;

    @BindView(R.id.llchallenged)
    LinearLayout llchallenged;

    @BindView(R.id.cvPersonalDetails)
    CardView cvPersonalDetails;

    @BindView(R.id.tvUserName)
    TextView tvUserName;

    @BindView(R.id.tvCity)
    TextView tvCity;

    @BindView(R.id.tvConnectionsLabel)
    TextView tvConnectionsLabel;

    @BindView(R.id.tvAdharVerified)
    TextView tvAdharVerified;

    @BindView(R.id.tvConnectionCount)
    TextView tvConnectionCount;

    @BindView(R.id.llConnections)
    LinearLayout llConnections;

    @BindView(R.id.tvProfession)
    TextView tvProfession;

    @BindView(R.id.tvAbout)
    TextView tvAbout;

    @BindView(R.id.ivUser)
    ImageView ivUser;

    @BindView(R.id.cvAboutMe)
    CardView cvAboutMe;

    @BindView(R.id.cvProfession)
    CardView cvProfession;

    @BindView(R.id.cvEducation)
    CardView cvEducation;

    @BindView(R.id.cvProfessionDetails)
    CardView cvProfessionDetails;

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    @BindView(R.id.llContainer)
    LinearLayout llContainer;

    @BindView(R.id.rvOrganisationList)
    RecyclerView rvOrganisationList;

    @BindView(R.id.rvRecommendations)
    RecyclerView rvRecommendations;

    @BindView(R.id.rvEducationList)
    RecyclerView rvEducationList;

    @BindView(R.id.llNoProfession)
    LinearLayout llNoProfession;

    @BindView(R.id.llNoEducation)
    LinearLayout llNoEducation;

    @BindView(R.id.llNoSkills)
    LinearLayout llNoSkills;

    @BindView(R.id.hlSkills)
    HorizontalScrollView hlSkills;

    @BindView(R.id.tvSkills)
    TextView tvSkills;

    @BindView(R.id.llNoSkillsDes)
    LinearLayout llNoSkillsDes;

    @BindView(R.id.llNoDSkillsDes)
    LinearLayout llNoDSkillsDes;

    @BindView(R.id.hlSkillsContainer)
    LinearLayout hlSkillsContainer;

    @BindView(R.id.llNoDSkills)
    LinearLayout llNoDSkills;

    @BindView(R.id.hlDSkills)
    HorizontalScrollView hlDSkills;

    @BindView(R.id.tvDSkills)
    TextView tvDSkills;

    @BindView(R.id.llDSkillsContainer)
    LinearLayout llDSkillsContainer;

    @BindView(R.id.cvSkills)
    CardView cvSkills;

    @BindView(R.id.cvSkillsDes)
    CardView cvSkillsDes;

    @BindView(R.id.tvSkillsDes)
    TextView tvSkillsDes;

    @BindView(R.id.cvDSkills)
    CardView cvDSkills;

    @BindView(R.id.cvDSkillsDes)
    CardView cvDSkillsDes;

    @BindView(R.id.tvDSkillsDes)
    TextView tvDSkillsDes;

    @BindView(R.id.llDecline)
    LinearLayout llDecline;

    @BindView(R.id.llMessage)
    LinearLayout llMessage;

    @BindView(R.id.cvDecline)
    CardView cvDecline;

    @BindView(R.id.cvMessage)
    CardView cvMessage;

    @BindView(R.id.llConnectAccept)
    LinearLayout llConnectAccept;

    @BindView(R.id.cvConnectAccept)
    CardView cvConnectAccept;

    @BindView(R.id.cvRecom)
    CardView cvRecom;

    @BindView(R.id.tvConnectAccept)
    TextView tvConnectAccept;

    @BindView(R.id.cvLanguageKnown)
    CardView cvLanguageKnown;

    @BindView(R.id.tvLanguage)
    TextView tvLanguage;

    @BindView(R.id.adView)
    ImageView adView;

    @BindView(R.id.rlAdView)
    RelativeLayout rlAdView;

    @BindView(R.id.cvEmail)
    CardView cvEmail;

    @BindView(R.id.tvEmail)
    TextView tvEmail;

    @BindView(R.id.cvMobile)
    CardView cvMobile;

    @BindView(R.id.tvMobile)
    TextView tvMobile;

    @BindView(R.id.cvAddress)
    CardView cvAddress;

    @BindView(R.id.tvAddress)
    TextView tvAddress;

    boolean connectionFlag = false;
    String status = "";
    String ACTION_USER_ID = "";
    private User user;
    private JSONObject profile = new JSONObject();
    private LinearLayoutManager linearLayoutManager;
    private ProfessionListAdapter professionListAdapter;
    private RecomRecyclerViewAdapter recomRecyclerViewAdapter;
    private EducationListAdapter educationListAdapter;
    AlertDialog recomDialog, askRecomDialog;
    MenuItem askRecom, recom, report;
    AlertDialog confirmAlert;
    com.codeholic.kotumb.app.View.ProgressBar progressBar;
    ViewTooltip v;
    private boolean deepLink;

    public static void setHolder(UserRecyclerViewAdapter.UserViewHolder holder) {
        OtherUserProfileActivity.holder = holder;
    }

    //oncreate method for setting content view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_user_profile);
        ButterKnife.bind(this);
        initToolbar();
        init();

        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();

        if (data != null) {
            deepLink = true;
            user = new User();
            String url = data.toString();
            int lastIndexOf = url.lastIndexOf("/");
            String userId = url.substring(lastIndexOf + 1, url.length());
            user.setUserId(userId);
        }

      /*  if(!user.getRole().equalsIgnoreCase("KR1")){
            finish();
            return;
        }*/
        //initConnectionFlag();
        setupViews();
        serviceCall();
        setClickListener();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.UPDATE_USER);
        filter.addAction(ResponseParameters.REQUEST_SEND);
        registerReceiver(receiver, filter);

        IntentFilter filterAdd = new IntentFilter();
        filterAdd.addAction(ResponseParameters.ADD_RECEIVED);
        registerReceiver(getAd, filterAdd);


    }











    //setting toolbar and header title
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.header_main_menu_option2));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (!isTaskRoot()) {
            super.onBackPressed();
        } else {
            startActivity(new Intent(this, SplashActivity.class));
            finish();
        }
    }

    private BroadcastReceiver getAd = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            try {
//                HomeScreenActivity.setUpAdImage(intent.getStringExtra(ResponseParameters.ADD_RECEIVED), rlAdView, adView, OtherUserProfileActivity.this, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void setUpAdImage(String adResponse) {
        hideAdView();
        try {
            if (!adResponse.isEmpty()) {
                final JSONObject ad = new JSONObject(adResponse);
                if (ad.has(ResponseParameters.AD)) {
                    if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.IMAGE)) {
                        if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE).isEmpty()) {
                            rlAdView.setVisibility(VISIBLE);
                            new AQuery(this).id(adView).image(API.imageAdUrl + ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE), true, true, 300, R.drawable.ic_user);
                            adView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.URL)) {
                                            if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL).isEmpty()) {
                                                Utility.prepareCustomTab(OtherUserProfileActivity.this, ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL));
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideAdView() {
        rlAdView.setVisibility(GONE);
    }

    //init values and show user details
    private void init() {

        Intent intent = getIntent();
        user = getIntent().getParcelableExtra("USER");

//        System.out.println("User Parcible Data  "+ user);
    }

    private void initConnectionFlag() {
        try {
            JSONObject outPut = new JSONObject(user.getUserInfo());
            if (outPut.has(ResponseParameters.CONNECTION_FLAG)) {
                llConnectAccept.setVisibility(VISIBLE);
                llDecline.setVisibility(GONE);
                llMessage.setVisibility(GONE);
                askRecom.setVisible(false);
                recom.setVisible(false);
                connectionFlag = outPut.getBoolean(ResponseParameters.CONNECTION_FLAG);
                if (outPut.getBoolean(ResponseParameters.CONNECTION_FLAG)) {
                    if (outPut.has(ResponseParameters.CONNECTION_INFO)) {
                        JSONObject CONNECTION_INFO = outPut.getJSONObject(ResponseParameters.CONNECTION_INFO);
                        status = CONNECTION_INFO.getString(ResponseParameters.STATUS);
                        ACTION_USER_ID = CONNECTION_INFO.getString(ResponseParameters.ACTIONUSERID);
                        if (status.trim().equalsIgnoreCase("0")) {
                            if (SharedPreferencesMethod.getUserId(this).trim().equalsIgnoreCase(ACTION_USER_ID.trim())) {
                                tvConnectAccept.setText(getResources().getString(R.string.request_cancel_btn_text));
                            } else {
                                tvConnectAccept.setText(getResources().getString(R.string.request_accept_btn_text));
                                llDecline.setVisibility(VISIBLE);
                            }
                        } else if (status.trim().equalsIgnoreCase("1")) {
                            tvConnectAccept.setText(getResources().getString(R.string.unfriend_btn_text));
                            llMessage.setVisibility(VISIBLE);
                            askRecom.setVisible(true);
                            recom.setVisible(true);
                        }
                    }
                } else {
                    tvConnectAccept.setText(getResources().getString(R.string.connect_btn_text));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //show user details
    private void setupViews() {
        if (user!=null) {
            tvUserName.setText("" + user.getFirstName() + " " + user.getLastName());

            Log.e("TAG", "" + user.getMobileVisibility() + " " + user.getAddressVisibility() + " " + user.getEmailVisibility());

            if (user.getProfileHeadline() != null && !user.getProfileHeadline().trim().equalsIgnoreCase("null") && !user.getProfileHeadline().trim().isEmpty()) {
                tvProfession.setText("" + user.getProfileHeadline());
//            System.out.println("Heading   "+user.getProfileHeadline());
                cvProfession.setVisibility(View.VISIBLE);
            } else {
                cvProfession.setVisibility(View.GONE);
            }

            if (user.getProfileSummary() != null && !user.getProfileSummary().trim().equalsIgnoreCase("null") && !user.getProfileSummary().trim().isEmpty()) {
                tvAbout.setText("" + user.getProfileSummary());
                cvAboutMe.setVisibility(View.VISIBLE);
            } else {
                cvAboutMe.setVisibility(View.GONE);
            }

            if (user.getAadhaarInfo().equalsIgnoreCase("true")) {
                tvAdharVerified.setVisibility(View.VISIBLE);
            } else {
                tvAdharVerified.setVisibility(View.GONE);
            }
            tvCity.setText(user.getCity() + ", " + user.getState());
            new AQuery(this).id(ivUser).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.default_profile);
        }else {

            Intent intent = new Intent(OtherUserProfileActivity.this,HomeScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);

        }

    }

    //sevice call for getting user details
    private void serviceCall() {
        try {
            if (!Utility.isConnectingToInternet(this)) { // check net connection
                final Snackbar snackbar = Snackbar.make(ivUser, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }


            String url;
            HashMap<String, Object> input = new HashMap<>();
            if (!deepLink) {
                url = API.PROFILE;
                input.put(RequestParameters.VIEWERID, "" + SharedPreferencesMethod.getUserId(this));
                input.put(RequestParameters.USERID, "" + user.getUserId());
//                System.out.println("Get User id  "+user.getUserId());
            } else {
                url = API.PROFILE + "/" + user.getUserId();
                input.put(RequestParameters.VIEWERID, 0);
                input.put(RequestParameters.USERID, 0);
//                System.out.println("Get User id  2 "+user.getUserId());
            }
            Log.e("input", "" + input);
            API.sendRequestToServerPOST_PARAM(this, url, input); // service call for user profile data
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //set click listener on views
    private void setClickListener() {
        cvDecline.setOnClickListener(this);
        cvConnectAccept.setOnClickListener(this);
        llConnections.setOnClickListener(this);
        cvMessage.setOnClickListener(this);
    }

    //getresponse from server
    public void getResponse(JSONObject outPut, int i) {
        try {
            System.out.println("Ohter Profile Response  "+outPut);
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Errors)) { // error
                    final Snackbar snackbar = Snackbar.make(ivUser, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else {
                    parseResponse(outPut);
                    Bundle bundle = new Bundle();
                    bundle.putString("UserId", user.getUserId());
                    bundle.putString("FullName", user.getFirstName() + " " + user.getLastName());
                    Utils.logEvent(EventNames.USER_PROFILE_VIEW, bundle, this);
                }
            } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { //error
                final Snackbar snackbar = Snackbar.make(ivUser, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                snackbar.show();
            } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { //no network
                final Snackbar snackbar = Snackbar.make(ivUser, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //parse response from server
    private void parseResponse(JSONObject outPut) {
        try {
            Log.e("output", "" + outPut);
            pbLoading.setVisibility(View.GONE);
            llContainer.setVisibility(View.VISIBLE);
            user.setUserInfo(outPut.toString());
            initConnectionFlag();

            if (outPut.has(ResponseParameters.LANGUAGES)) {
                String langauge = "";
                for (int j = 0; j < outPut.getJSONArray(ResponseParameters.LANGUAGES).length(); j++) {
                    langauge = langauge + outPut.getJSONArray(ResponseParameters.LANGUAGES).getJSONObject(j).getString(ResponseParameters.LANGUAGE).trim() + ", ";
                }
                if (!langauge.trim().isEmpty()) {
                    cvLanguageKnown.setVisibility(VISIBLE);
                    tvLanguage.setText(langauge.trim().substring(0, langauge.trim().length() - 1));
                }
            }

            if (outPut.has(ResponseParameters.PROFILE)) { // check output have profile
                profile = outPut.getJSONObject(ResponseParameters.PROFILE);
                if (profile.has(ResponseParameters.CONNECTIONS_COUNT)) { // connection count
                    tvConnectionCount.setText(profile.getString(ResponseParameters.CONNECTIONS_COUNT));
                }

                if (profile.has(ResponseParameters.Userdata)) {
                    JSONObject USERDATA = profile.getJSONObject(ResponseParameters.Userdata);

                    if (USERDATA.has(ResponseParameters.IS_BLOCKED)) {
                        if (USERDATA.getString(ResponseParameters.IS_BLOCKED).equalsIgnoreCase("1")) {
                            showBlockedUI("This user has been blocked by admin. This profile is no longer accessible.");
                            return;
                        }
                    }

                    if (USERDATA.has(ResponseParameters.IS_DELETED)) {
                        if (USERDATA.getString(ResponseParameters.IS_DELETED).equalsIgnoreCase("1")) {
                            showBlockedUI("This user has been deleted. This profile is no longer accessible.");
                            return;
                        }
                    }

                    if (USERDATA.has(ResponseParameters.Role)) {
                        if (!USERDATA.getString(ResponseParameters.Role).equals(ResponseParameters.NORMAL_ROLE)) {
                            cvSkills.setVisibility(GONE);
                            cvSkillsDes.setVisibility(GONE);
                            cvDSkills.setVisibility(GONE);
                            cvDSkillsDes.setVisibility(GONE);
                        }
                    }
                    if (USERDATA.has(ResponseParameters.MobileNumber)) {
                        user.setMobileNumber(USERDATA.getString(ResponseParameters.MobileNumber).trim());
                    }
                }

                if (profile.has(ResponseParameters.USER_SKILLS)) { // user skills
                    if (profile.getJSONArray(ResponseParameters.USER_SKILLS).length() <= 0) { //if user have skills length more then 0
                        hlSkills.setVisibility(GONE);
                        tvSkills.setVisibility(GONE);
                        llNoSkills.setVisibility(VISIBLE);
                    } else {
                        hlSkills.setVisibility(GONE);
                        llNoSkills.setVisibility(GONE);
                        tvSkills.setVisibility(VISIBLE);
                        String text = "";
                        for (int i = 0; i < profile.getJSONArray(ResponseParameters.USER_SKILLS).length(); i++) {
                            if (!profile.getJSONArray(ResponseParameters.USER_SKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim().isEmpty()) {
                                /*LayoutInflater inflater = LayoutInflater.from(this);
                                View v = inflater.inflate(R.layout.skill_tv, null, false);
                                TextView textView = (TextView) v.findViewById(R.id.tv);
                                textView.setText(profile.getJSONArray(ResponseParameters.USER_SKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim());
                                hlSkillsContainer.addView(v);*/

                                if (text.isEmpty())
                                    text = text.trim() + profile.getJSONArray(ResponseParameters.USER_SKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim();
                                else
                                    text = text + ", " + profile.getJSONArray(ResponseParameters.USER_SKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim();
                            }
                        }

                        tvSkills.setText("" + text);
                        if (text.isEmpty()) {
                            hlSkills.setVisibility(GONE);
                            llNoSkills.setVisibility(VISIBLE);
                            tvSkills.setVisibility(GONE);
                        }
                    }
                } else {
                    hlSkills.setVisibility(GONE);
                    llNoSkills.setVisibility(VISIBLE);
                    tvSkills.setVisibility(GONE);
                }

                if (profile.has(ResponseParameters.USER_DSKILLS)) { // user desired skills
                    if (profile.getJSONArray(ResponseParameters.USER_DSKILLS).length() <= 0) { //if user have desired skills length more then 0
                        hlDSkills.setVisibility(GONE);
                        tvDSkills.setVisibility(GONE);
                        llNoDSkills.setVisibility(VISIBLE);
                    } else {
                        hlDSkills.setVisibility(GONE);
                        tvDSkills.setVisibility(VISIBLE);
                        llNoDSkills.setVisibility(GONE);
                        String text = "";
                        for (int i = 0; i < profile.getJSONArray(ResponseParameters.USER_DSKILLS).length(); i++) {
                            if (!profile.getJSONArray(ResponseParameters.USER_DSKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim().isEmpty()) {
                                /*LayoutInflater inflater = LayoutInflater.from(this);
                                View v = inflater.inflate(R.layout.skill_tv, null, false);
                                TextView textView = (TextView) v.findViewById(R.id.tv);
                                textView.setText(profile.getJSONArray(ResponseParameters.USER_DSKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim());
                                llDSkillsContainer.addView(v);*/
                                if (text.isEmpty())
                                    text = text.trim() + profile.getJSONArray(ResponseParameters.USER_DSKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim();
                                else
                                    text = text + ", " + profile.getJSONArray(ResponseParameters.USER_DSKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim();
                            }
                        }
                        tvDSkills.setText("" + text);
                        if (text.isEmpty()) {
                            hlDSkills.setVisibility(GONE);
                            tvDSkills.setVisibility(GONE);
                            llNoDSkills.setVisibility(VISIBLE);
                        }
                    }
                } else {
                    hlDSkills.setVisibility(GONE);
                    tvDSkills.setVisibility(GONE);
                    llNoDSkills.setVisibility(VISIBLE);
                }

                if (profile.has(ResponseParameters.PersonalInfo)) { // check personal info
                    JSONObject PERSONALINFO = profile.getJSONObject(ResponseParameters.PersonalInfo);

                    if (PERSONALINFO.has(ResponseParameters.FirstName) && PERSONALINFO.has(ResponseParameters.LastName) && PERSONALINFO.has(ResponseParameters.MiddleName)) {
                        user.setFirstName(PERSONALINFO.getString(ResponseParameters.FirstName));
                        user.setMiddleName(PERSONALINFO.getString(ResponseParameters.MiddleName));
                        user.setLastName(PERSONALINFO.getString(ResponseParameters.LastName));
                    }
                    if (PERSONALINFO.has(ResponseParameters.Avatar)) {
                        user.setAvatar(PERSONALINFO.getString(ResponseParameters.Avatar));
                    }
                    if (PERSONALINFO.has(ResponseParameters.PROFILE_HEADLINE)) {
                        user.setProfileHeadline(PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE));
                    }
                    if (PERSONALINFO.has(ResponseParameters.PROFILE_SUMMERY)) {
                        user.setProfileSummary(PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY));
                    }
                    if (PERSONALINFO.has(ResponseParameters.AADHARINFO)) {
                        user.setAadhaarInfo(PERSONALINFO.getString(ResponseParameters.AADHARINFO));
                    }
                    if (PERSONALINFO.has(ResponseParameters.City)) {
                        user.setCity(PERSONALINFO.getString(ResponseParameters.City));
                    }
                    if (PERSONALINFO.has(ResponseParameters.State)) {
                        user.setState(PERSONALINFO.getString(ResponseParameters.State));
                    }

                    if (PERSONALINFO.has(ResponseParameters.Gender)) {
                        user.setGender(PERSONALINFO.getString(ResponseParameters.Gender));
                    }

                    if (PERSONALINFO.has(ResponseParameters.DateOfBirth)) {
                        user.setDOB(PERSONALINFO.getString(ResponseParameters.DateOfBirth));
                    }

                    if (PERSONALINFO.has(ResponseParameters.IS_CHALLENGED)) {
                        user.setIsChallenged(PERSONALINFO.getString(ResponseParameters.IS_CHALLENGED));
                    }

                    if (PERSONALINFO.has(ResponseParameters.GENDER_VISIBILITY)) {
                        user.setGenderVisibility(PERSONALINFO.getString(ResponseParameters.GENDER_VISIBILITY));
                    }

                    if (PERSONALINFO.has(ResponseParameters.DOB_VISIBILITY)) {
                        user.setDobVisibility(PERSONALINFO.getString(ResponseParameters.DOB_VISIBILITY));
                    }


                    setupViews();

                    if (PERSONALINFO.has(ResponseParameters.EMAIL_VISIBILITY)) {
                        user.setEmail(PERSONALINFO.getString(ResponseParameters.Email));
                        user.setEmailVisibility(PERSONALINFO.getString(ResponseParameters.EMAIL_VISIBILITY));
                    }
                    if (PERSONALINFO.has(ResponseParameters.MOBILE_VISIBILITY)) {
                        user.setMobileVisibility(PERSONALINFO.getString(ResponseParameters.MOBILE_VISIBILITY));
                    }
                    if (PERSONALINFO.has(ResponseParameters.ADDRESS_VISIBILITY)) {
                        user.setAddress(PERSONALINFO.getString(ResponseParameters.Address));
                        user.setAddressVisibility(PERSONALINFO.getString(ResponseParameters.ADDRESS_VISIBILITY));
                    }

                    setUpVisibilityViews();


                    if (PERSONALINFO.getString(ResponseParameters.SkillDescription) != null && !PERSONALINFO.getString(ResponseParameters.SkillDescription).equalsIgnoreCase("null") && !PERSONALINFO.getString(ResponseParameters.SkillDescription).trim().isEmpty()) {
                        tvSkillsDes.setText("" + PERSONALINFO.getString(ResponseParameters.SkillDescription));
                        System.out.println("Des    "+PERSONALINFO.getString(ResponseParameters.SkillDescription));
                        llNoSkillsDes.setVisibility(GONE);
                    } else {
                        llNoSkillsDes.setVisibility(VISIBLE);
                        tvSkillsDes.setVisibility(GONE);
                    }
                    if (PERSONALINFO.getString(ResponseParameters.Interests) != null && !PERSONALINFO.getString(ResponseParameters.Interests).equalsIgnoreCase("null") && !PERSONALINFO.getString(ResponseParameters.Interests).trim().isEmpty()) {
                        tvDSkillsDes.setText("" + PERSONALINFO.getString(ResponseParameters.Interests));
                        llNoDSkillsDes.setVisibility(GONE);
                    } else {
                        llNoDSkillsDes.setVisibility(VISIBLE);
                        tvDSkillsDes.setVisibility(GONE);
                    }
                }
                setWorkRecyclerView();
                setEducationRecyclerView();
            }

            if (outPut.has(ResponseParameters.recommendations)) {
                setRecomRecyclerView(outPut.getJSONArray(ResponseParameters.recommendations));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showBlockedUI(String text) {
        cvLanguageKnown.setVisibility(GONE);
        cvProfession.setVisibility(GONE);
        cvSkills.setVisibility(GONE);
        cvSkillsDes.setVisibility(GONE);
        cvDSkills.setVisibility(GONE);
        cvDSkillsDes.setVisibility(GONE);
        cvRecom.setVisibility(GONE);
        cvAboutMe.setVisibility(GONE);
        cvProfession.setVisibility(GONE);
        cvEducation.setVisibility(GONE);
        cvProfessionDetails.setVisibility(GONE);
        llConnections.setVisibility(GONE);
        llDecline.setVisibility(GONE);
        llMessage.setVisibility(GONE);
        tvConnectionCount.setVisibility(GONE);
        tvAddress.setVisibility(GONE);
        tvCity.setVisibility(GONE);
        tvConnectAccept.setVisibility(GONE);
        recom.setVisible(false);
        report.setVisible(false);
        askRecom.setVisible(false);
        tvConnectionsLabel.setText(text);
        //Toast.makeText(this, "User is blocked by admin", Toast.LENGTH_SHORT).show();
        return;
    }

    private void setUpVisibilityViews() {
        try {
            if (user.getEmailVisibility().trim().equalsIgnoreCase("0") && !user.getEmail().trim().isEmpty()) {
                cvEmail.setVisibility(GONE);
                tvEmail.setText(user.getEmail().trim());
            } else {
                cvEmail.setVisibility(GONE);
            }

            if (user.getAddressVisibility().trim().equalsIgnoreCase("0") && !user.getAddress().trim().isEmpty()) {
                cvAddress.setVisibility(VISIBLE);
                tvAddress.setText(user.getAddress().trim());
            } else {
                cvAddress.setVisibility(GONE);
            }


            if (user.getMobileVisibility().trim().equalsIgnoreCase("0") && !user.getMobileNumber().trim().isEmpty()) {
                cvMobile.setVisibility(GONE);
                tvMobile.setText(user.getMobileNumber().trim());
            } else {
                cvMobile.setVisibility(GONE);
            }

            if (user.getGenderVisibility().equalsIgnoreCase("0")
                    || user.getDobVisibility().equalsIgnoreCase("0")) {
                cvPersonalDetails.setVisibility(VISIBLE);
                if (user.getGenderVisibility().equalsIgnoreCase("0")) {
                    llgender.setVisibility(VISIBLE);
                    tvgender.setText(user.getGender().equalsIgnoreCase("M") ? "Male" : "Female");
                }
                if (user.getDobVisibility().equalsIgnoreCase("0")) {
                    lldob.setVisibility(VISIBLE);
                    String dob = user.getDOB();
                    try {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        SimpleDateFormat userFormat = new SimpleDateFormat("dd MMMM, yyyy");
                        Date parse = format.parse(dob);
                        tvdob.setText(userFormat.format(parse));
                    } catch (ParseException e) {
                        e.printStackTrace();
                        tvdob.setText(dob);
                    }
                }
                if (user.getIsChallenged().equalsIgnoreCase("1")) {
                    llchallenged.setVisibility(VISIBLE);
                    tvchallenged.setText("Yes");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //setting profession list
    private void setWorkRecyclerView() {
        try {
            //set up recycler view
            linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvOrganisationList.setLayoutManager(linearLayoutManager);
            rvOrganisationList.setNestedScrollingEnabled(false);
            // rvOrganisationList.setHasFixedSize(true);
            if (profile.has(ResponseParameters.WORK_EXPERIANCE)) { // check work experience
                List<Profession> professions = new ArrayList<>();
                for (int i = 0; i < profile.getJSONArray(ResponseParameters.WORK_EXPERIANCE).length(); i++) {
                    JSONObject jsonObject = profile.getJSONArray(ResponseParameters.WORK_EXPERIANCE).getJSONObject(i);
                    Profession profession = new Profession();
                    profession.setId(jsonObject.getString(ResponseParameters.Id));
                    profession.setCompanyName(jsonObject.getString(ResponseParameters.ORGANIZATION_NAME));
                    profession.setProfessionName(jsonObject.getString(ResponseParameters.ORGANIZATION_ROLE));
                    profession.setYearFrom(jsonObject.getString(ResponseParameters.YEARFROM));
                    profession.setYearTo(jsonObject.getString(ResponseParameters.YEARTO));
                    profession.setDescription(jsonObject.getString(ResponseParameters.DESCRIPTION));
                    profession.setIsCurrent(jsonObject.getString(ResponseParameters.ISCURRENT));
                    professions.add(profession);
                    System.out.println("Descriptions  "+jsonObject.getString(ResponseParameters.DESCRIPTION));
                }

                professionListAdapter = new ProfessionListAdapter(this, professions);
                professionListAdapter.menu = false;
                rvOrganisationList.setAdapter(professionListAdapter);

                if (profile.getJSONArray(ResponseParameters.WORK_EXPERIANCE).length() <= 0) {
                    rvOrganisationList.setVisibility(GONE);
                    llNoProfession.setVisibility(VISIBLE);
                } else {
                    rvOrganisationList.setVisibility(VISIBLE);
                    llNoProfession.setVisibility(GONE);
                }
            } else {
                rvOrganisationList.setVisibility(GONE);
                llNoProfession.setVisibility(VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //setting recommendations list
    private void setRecomRecyclerView(JSONArray recommendations) {
        try {
            //set up recycler view
            linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvRecommendations.setLayoutManager(linearLayoutManager);
            rvRecommendations.setNestedScrollingEnabled(false);
            // rvOrganisationList.setHasFixedSize(true);
            List<Recommendations> recommendationsList = new ArrayList<>();
            for (int i = 0; i < recommendations.length(); i++) {
                JSONObject jsonObject = recommendations.getJSONObject(i);
                Recommendations recommendations1 = new Recommendations();
                User user = new User();
                user.setFirstName(jsonObject.getString(ResponseParameters.FirstName));
                user.setMiddleName(jsonObject.getString(ResponseParameters.MiddleName));
                user.setLastName(jsonObject.getString(ResponseParameters.LastName));
                user.setAvatar(jsonObject.getString(ResponseParameters.Avatar));
                user.setUserId(jsonObject.getString(ResponseParameters.UserId));
                user.setProfileHeadline(jsonObject.getString(ResponseParameters.PROFILE_HEADLINE));
                user.setProfileSummary(jsonObject.getString(ResponseParameters.PROFILE_SUMMERY));
                //user.setAadhaarInfo(results.getString(ResponseParameters.AADHARINFO));
                user.setCity(jsonObject.getString(ResponseParameters.City));
                user.setState(jsonObject.getString(ResponseParameters.State));
                user.setUserInfo(jsonObject.toString());
                recommendations1.setUser(user);
                recommendations1.setRecomId(jsonObject.getString(ResponseParameters.Id));
                recommendations1.setDescription(jsonObject.getString(ResponseParameters.DESCRIPTION));
                recommendations1.setStatus(jsonObject.getString(ResponseParameters.STATUS));
                recommendationsList.add(recommendations1);
            }

            ArrayList<Recommendations> toRemove = new ArrayList<>();
            for (Recommendations recom:recommendationsList) {
                if(recom.getStatus().equalsIgnoreCase("0")){
                    toRemove.add(recom);
                }
            }
            recommendationsList.removeAll(toRemove);

            recomRecyclerViewAdapter = new RecomRecyclerViewAdapter(rvRecommendations, this, recommendationsList);
            rvRecommendations.setAdapter(recomRecyclerViewAdapter);

            if (recommendations.length() <= 0) {
                cvRecom.setVisibility(GONE);
            } else {
                cvRecom.setVisibility(VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //setting Education list
    private void setEducationRecyclerView() {
        try {
            //setup recycler view
            linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvEducationList.setLayoutManager(linearLayoutManager);
            rvEducationList.setNestedScrollingEnabled(false);
            // rvEducationList.setHasFixedSize(true);
            if (profile.has(ResponseParameters.EDUCATION_DETAIL)) { // check education details
                List<Education> educations = new ArrayList<>();
                for (int i = 0; i < profile.getJSONArray(ResponseParameters.EDUCATION_DETAIL).length(); i++) {
                    JSONObject jsonObject = profile.getJSONArray(ResponseParameters.EDUCATION_DETAIL).getJSONObject(i);
                    Education education = new Education();
                    education.setId(jsonObject.getString(ResponseParameters.Id));
                    education.setCourse(jsonObject.getString(RequestParameters.COURSE));
                    education.setSchool(jsonObject.getString(RequestParameters.SCHOOL));
                    education.setYearOfPassing(jsonObject.getString(RequestParameters.YEAR));
                    education.setDetails(jsonObject.getString(RequestParameters.DESCRIPTION));
                    educations.add(education);
                }
                educationListAdapter = new EducationListAdapter(this, educations);
                Log.e("profile", "" + educations.size());
                educationListAdapter.menu = false;
                rvEducationList.setAdapter(educationListAdapter);

                if (profile.getJSONArray(ResponseParameters.EDUCATION_DETAIL).length() <= 0) {
                    rvEducationList.setVisibility(GONE);
                    llNoEducation.setVisibility(VISIBLE);
                } else {
                    rvEducationList.setVisibility(VISIBLE);
                    llNoEducation.setVisibility(GONE);
                }
            } else {
                rvEducationList.setVisibility(GONE);
                llNoEducation.setVisibility(VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            //user_id_1, user_id_2, action_user_id, status(0 =>connect, 1=Accept, 2=Declined, 3=Blocked, 4= Delete)
            case R.id.cvConnectAccept: // click connect
                try {
                    if (!Utility.isConnectingToInternet(this)) { // check net connection
                        final Snackbar snackbar = Snackbar.make(ivUser, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }
                    final HashMap<String, Object> input = new HashMap<>();
                    input.put(RequestParameters.USERID1, "" + SharedPreferencesMethod.getUserId(this));
                    input.put(RequestParameters.USERID2, "" + user.getUserId());
                    input.put(RequestParameters.ACTIONUSERID, "" + SharedPreferencesMethod.getUserId(this));
                    if (connectionFlag) {
                        if (status.trim().equalsIgnoreCase("0")) {
                            if (SharedPreferencesMethod.getUserId(this).trim().equalsIgnoreCase(ACTION_USER_ID.trim())) {
                                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                                dialogBuilder.setTitle(getResources().getString(R.string.app_alert_text));
                                dialogBuilder.setMessage(getResources().getString(R.string.confirm_cancel_request_message));
                                dialogBuilder.setCancelable(true);
                                dialogBuilder.setPositiveButton(getResources().getString(R.string.primary_number_yes_btn_text), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        try {
                                            if (!Utility.isConnectingToInternet(OtherUserProfileActivity.this)) { // check net connection
                                                final Snackbar snackbar = Snackbar.make(cvConnectAccept, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                                                snackbar.show();
                                                return;
                                            }
                                            input.put(RequestParameters.STATUS, "2");
                                            connectionFlag = false;
                                            tvConnectAccept.setText(getResources().getString(R.string.connect_btn_text));
                                            llDecline.setVisibility(GONE);
                                            llMessage.setVisibility(GONE);
                                            askRecom.setVisible(false);
                                            recom.setVisible(false);
                                            status = input.get(RequestParameters.STATUS).toString();


                                            JSONObject jsonObject = new JSONObject(user.getUserInfo());
                                            jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "2");

                                            Log.e("CONNECTION_INFO", "" + jsonObject.toString());
                                            jsonObject.put(ResponseParameters.CONNECTION_FLAG, connectionFlag);
                                            user.setUserInfo(jsonObject.toString());


                                            Intent broadCast = new Intent();
                                            broadCast.setAction(ResponseParameters.UPDATE_USER);
                                            broadCast.putExtra("USER", user);
                                            sendBroadcast(broadCast);

                                            API.sendRequestToServerPOST_PARAM(OtherUserProfileActivity.this, API.CONNECT, input); // service call for cancel request
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        dialog.dismiss();
                                    }
                                });

                                dialogBuilder.setNegativeButton(getResources().getString(R.string.primary_number_no_btn_text), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                    }
                                });

                                dialogBuilder.create().show();


                            } else {
                                if (!Utility.isConnectingToInternet(OtherUserProfileActivity.this)) { // check net connection
                                    final Snackbar snackbar = Snackbar.make(cvConnectAccept, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                                    snackbar.show();
                                    return;
                                }
                                connectionFlag = true;
                                input.put(RequestParameters.STATUS, "1");
                                llDecline.setVisibility(GONE);
                                tvConnectAccept.setText(getResources().getString(R.string.unfriend_btn_text));
                                llMessage.setVisibility(VISIBLE);
                                askRecom.setVisible(true);
                                recom.setVisible(true);
                                status = input.get(RequestParameters.STATUS).toString();
                                API.sendRequestToServerPOST_PARAM(this, API.CONNECT, input);// service call for connect

                                //for updating connection list
                                JSONObject jsonObject = new JSONObject(user.getUserInfo());
                                jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(RequestParameters.STATUS, "1");
                                user.setUserInfo(jsonObject.toString());

                                Intent requestAccepted = new Intent();
                                requestAccepted.setAction(ResponseParameters.REQUEST_ACCEPTED);
                                requestAccepted.putExtra("USER", user);
                                sendBroadcast(requestAccepted);

                            }
                        } else if (status.trim().equalsIgnoreCase("1")) {
                            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                            dialogBuilder.setTitle(getResources().getString(R.string.app_alert_text));
                            dialogBuilder.setMessage(getResources().getString(R.string.confirm_unfriend_message));
                            dialogBuilder.setCancelable(true);
                            dialogBuilder.setPositiveButton(getResources().getString(R.string.done_btn_text), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    try {
                                        if (!Utility.isConnectingToInternet(OtherUserProfileActivity.this)) { // check net connection
                                            final Snackbar snackbar = Snackbar.make(cvConnectAccept, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                                            snackbar.show();
                                            return;
                                        }
                                        input.put(RequestParameters.STATUS, "4");
                                        connectionFlag = false;
                                        tvConnectAccept.setText(getResources().getString(R.string.connect_btn_text));
                                        llDecline.setVisibility(GONE);
                                        llMessage.setVisibility(GONE);
                                        askRecom.setVisible(false);
                                        recom.setVisible(false);
                                        status = input.get(RequestParameters.STATUS).toString();

                                        JSONObject jsonObject = new JSONObject(user.getUserInfo());
                                        jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "" + status);
                                        jsonObject.put(ResponseParameters.CONNECTION_FLAG, connectionFlag);
                                        user.setUserInfo(jsonObject.toString());

                                        Intent broadCast = new Intent();
                                        broadCast.setAction(ResponseParameters.UPDATE_USER);
                                        broadCast.putExtra("USER", user);
                                        sendBroadcast(broadCast);

                                        API.sendRequestToServerPOST_PARAM(OtherUserProfileActivity.this, API.CONNECT, input);// service call for unfriend
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    dialog.dismiss();
                                }
                            });

                            dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.dismiss();
                                }
                            });
                            dialogBuilder.create().show();
                        }
                    } else {
                        if (!Utility.isConnectingToInternet(OtherUserProfileActivity.this)) { // check net connection
                            final Snackbar snackbar = Snackbar.make(cvConnectAccept, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        input.put(RequestParameters.STATUS, "0");
                        llDecline.setVisibility(GONE);
                        connectionFlag = true;
                        ACTION_USER_ID = SharedPreferencesMethod.getUserId(this).trim();
                        tvConnectAccept.setText(getResources().getString(R.string.request_cancel_btn_text));
                        status = input.get(RequestParameters.STATUS).toString();


                        //broad cast for updating request send

                        JSONObject jsonObject = new JSONObject(user.getUserInfo());
                        jsonObject.put(ResponseParameters.CONNECTION_FLAG, connectionFlag);
                        JSONObject CONNECTION_INFO = new JSONObject();
                        CONNECTION_INFO.put(ResponseParameters.STATUS, "" + status);
                        CONNECTION_INFO.put(ResponseParameters.ACTIONUSERID, "" + ACTION_USER_ID);
                        jsonObject.put(ResponseParameters.CONNECTION_INFO, CONNECTION_INFO);
                        System.out.println("Other User Request    "+jsonObject.toString());
                        user.setUserInfo(jsonObject.toString());

                        Intent broadCast = new Intent();
                        broadCast.setAction(ResponseParameters.REQUEST_SEND);
                        broadCast.putExtra("USER", user);
                        sendBroadcast(broadCast);

                        if (holder != null) {
                            holder.llAcceptConnect.performClick();
                        }
                        API.sendRequestToServerPOST_PARAM(this, API.CONNECT, input); // service call for send request
                        Utils.logEventUserConnectRequest(OtherUserProfileActivity.this, user.getUserId());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;


            case R.id.cvDecline: // decline request
                try {


                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                    dialogBuilder.setTitle(getResources().getString(R.string.app_alert_text));
                    dialogBuilder.setMessage(getResources().getString(R.string.confirm_decline_request_message));
                    dialogBuilder.setCancelable(true);
                    dialogBuilder.setPositiveButton(getResources().getString(R.string.primary_number_yes_btn_text), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            try {
                                if (!Utility.isConnectingToInternet(OtherUserProfileActivity.this)) { // check net connection
                                    final Snackbar snackbar = Snackbar.make(cvDecline, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                                    snackbar.show();
                                    return;
                                }
                                HashMap<String, Object> input = new HashMap<>();
                                input.put(RequestParameters.USERID1, "" + SharedPreferencesMethod.getUserId(OtherUserProfileActivity.this));
                                input.put(RequestParameters.USERID2, "" + user.getUserId());
                                input.put(RequestParameters.ACTIONUSERID, "" + SharedPreferencesMethod.getUserId(OtherUserProfileActivity.this));
                                input.put(RequestParameters.STATUS, "2");
                                connectionFlag = false;
                                Log.e("input", "" + input);
                                tvConnectAccept.setText(getResources().getString(R.string.connect_btn_text));
                                llDecline.setVisibility(GONE);

                                JSONObject jsonObject = new JSONObject(user.getUserInfo());
                                jsonObject.put(ResponseParameters.CONNECTION_FLAG, connectionFlag);
                                user.setUserInfo(jsonObject.toString());

                                Intent broadCast = new Intent();
                                broadCast.setAction(ResponseParameters.UPDATE_USER);
                                broadCast.putExtra("USER", user);
                                sendBroadcast(broadCast);

                                API.sendRequestToServerPOST_PARAM(OtherUserProfileActivity.this, API.CONNECT, input); // service call for decline request
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            dialog.dismiss();
                        }
                    });

                    dialogBuilder.setNegativeButton(getResources().getString(R.string.primary_number_no_btn_text), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                        }
                    });

                    dialogBuilder.create().show();


                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.llConnections: // show connections of user
                Intent intent = new Intent(this, ConnectionsActivity.class);
                intent.putExtra("USER", user);
                startActivity(intent);
                break;
            case R.id.cvMessage:
                Intent intentChat = new Intent(this, ChatActivity.class);
                intentChat.putExtra("USER", user);
                intentChat.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentChat);
                break;
            case R.id.ivMobile:
                showToolTip(v, getResources().getString(R.string.mobile_visibility_helptext));
                break;
            case R.id.ivAddress:
                showToolTip(v, getResources().getString(R.string.address_visibility_helptext));
                break;
            case R.id.ivEmail:
                showToolTip(v, getResources().getString(R.string.Email_visibility_helptext));
                break;
        }
    }

    void showToolTip(View view, String toolTipTExt) {
        if (v != null) {
            v.close();
        }
        v = ViewTooltip
                .on(view)
                .autoHide(true, 20000)
                .clickToHide(true)
                .align(START)
                .position(TOP)
                .text(toolTipTExt)
                .textColor(Color.WHITE)
                .color(getResources().getColor(R.color.colorPrimary))
                .corner(10)
                .onHide(new ViewTooltip.ListenerHide() {
                    @Override
                    public void onHide(View view) {
                        v = null;
                    }
                });
        v.show();
    }

    private void recomUser() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.recommendation_popup, null);
        dialogBuilder.setView(dialogView);
        final EditText etRecommend = (EditText) dialogView.findViewById(R.id.etRecommend);
        final TextView tvRecommendText = (TextView) dialogView.findViewById(R.id.tvRecommendText);
        //tvRecommendText.setText(getResources().getString(R.string.recommendation_desc, user.getFirstName() + " " + user.getLastName()));
        tvRecommendText.setText(getResources().getString(R.string.recommendation_desc));
        final TextInputLayout etRecommendLayout = (TextInputLayout) dialogView.findViewById(R.id.etRecommendLayout);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.share_resume_send_btn), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        this.recomDialog = dialogBuilder.create();
        this.recomDialog.setCancelable(false);
        this.recomDialog.show();
        this.recomDialog.getButton(-1).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Boolean.valueOf(false).booleanValue()) {
                    recomDialog.dismiss();
                }
                if (etRecommend.getText().toString().isEmpty()) {
                    etRecommendLayout.setErrorEnabled(true);
                    etRecommendLayout.setError(getString(R.string.recom_description_empty_input));
                } else {
                    etRecommendLayout.setErrorEnabled(false);
                    if (!Utility.isConnectingToInternet(OtherUserProfileActivity.this)) { // check net connection
                        final Snackbar snackbar = Snackbar.make(header, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }

                    recomDialog.dismiss();
                    HashMap<String, Object> input = new HashMap<>();
                    input.put(RequestParameters.FROM_USERID, SharedPreferencesMethod.getUserId(OtherUserProfileActivity.this));
                    input.put(RequestParameters.TO_USERID, "" + user.getUserId());
                    input.put(RequestParameters.DESCRIPTION, "" + etRecommend.getText().toString().trim());

                    Log.e("request", "" + input);

                    API.sendRequestToServerPOST_PARAM(OtherUserProfileActivity.this, API.RECOM, input);

                    Bundle bundle = new Bundle();
                    bundle.putString("RecommendedTo", user.getUserId());
                    bundle.putString("RecommendedBy", SharedPreferencesMethod.getUserId(OtherUserProfileActivity.this));
                    Utils.logEvent(EventNames.GIVE_RECOMMENDATION, bundle, OtherUserProfileActivity.this);
                }
            }
        });
    }

    private void askRecomUser() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.ask_recommendation_popup, null);
        dialogBuilder.setView(dialogView);
        final EditText etRecommend = (EditText) dialogView.findViewById(R.id.etRecommend);
        final TextInputLayout etRecommendLayout = (TextInputLayout) dialogView.findViewById(R.id.etRecommendLayout);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.share_resume_send_btn), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        this.askRecomDialog = dialogBuilder.create();
        this.askRecomDialog.setCancelable(false);
        this.askRecomDialog.show();
        this.askRecomDialog.getButton(-1).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Boolean.valueOf(false).booleanValue()) {
                    askRecomDialog.dismiss();
                }

                if (etRecommend.getText().toString().trim().isEmpty()) {
                    etRecommendLayout.setErrorEnabled(true);
                    etRecommendLayout.setError(getString(R.string.recom_description_empty_input));
                } else {
                    etRecommendLayout.setErrorEnabled(false);

                    if (!Utility.isConnectingToInternet(OtherUserProfileActivity.this)) { // check net connection
                        final Snackbar snackbar = Snackbar.make(header, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }

                    askRecomDialog.dismiss();
                    HashMap<String, Object> input = new HashMap<>();
                    input.put(RequestParameters.FROM_USERID, SharedPreferencesMethod.getUserId(OtherUserProfileActivity.this));
                    input.put(RequestParameters.TO_USERID, "" + user.getUserId());
                    input.put(RequestParameters.DESCRIPTION, "" + etRecommend.getText().toString().trim());

                    Log.e("request", "" + input);

                    API.sendRequestToServerPOST_PARAM(OtherUserProfileActivity.this, API.ASK_FOR_RECOM, input);
                }
            }
        });
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            User updatedUser = intent.getParcelableExtra("USER");
            if (updatedUser.getUserId().equals(user.getUserId())) {
                try {
                    JSONObject updatedJson = new JSONObject(updatedUser.getUserInfo());
                    JSONObject json = new JSONObject(user.getUserInfo());
                    if (updatedJson.has(ResponseParameters.CONNECTION_FLAG)) {
                        json.put(ResponseParameters.CONNECTION_FLAG, updatedJson.getBoolean(ResponseParameters.CONNECTION_FLAG));
                    }
                    if (updatedJson.has(ResponseParameters.CONNECTION_INFO)) {
                        json.put(ResponseParameters.CONNECTION_INFO, updatedJson.getJSONObject(ResponseParameters.CONNECTION_INFO));
                    }
                    user.setUserInfo(json.toString());
                    initConnectionFlag();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        try {
            if (getAd != null) {
                unregisterReceiver(getAd);
                getAd = null;
            }
            if (receiver != null) {
                unregisterReceiver(receiver);
                receiver = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_other_user_profile, menu);
        askRecom = menu.findItem(R.id.ask_recom);
        recom = menu.findItem(R.id.recom);
        report = menu.findItem(R.id.report);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ask_recom:
                askRecomUser();
                break;
            case R.id.recom:
                recomUser();
                break;
            case R.id.report:
                HashMap<String, Object> input = new HashMap<>();
                input.put(RequestParameters.FROMUSERID, "" + SharedPreferencesMethod.getUserId(this));
                input.put(RequestParameters.TOUSERID, "" + user.getUserId());
                Log.e("input", "" + input.toString());
                reportPopup(getResources().getString(R.string.user_report_btn_text), API.USER_REPORT, input);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void reportPopup(String title, final String url, final HashMap<String, Object> input) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.report_popup, null);
        dialogBuilder.setView(dialogView);
        final EditText etTitle = (EditText) dialogView.findViewById(R.id.etTitle);
        final TextView Pop_title = dialogView.findViewById(R.id.Pop_title);
        Pop_title.setText(title);
        final TextInputLayout etTitleLayout = (TextInputLayout) dialogView.findViewById(R.id.etTitleLayout);
        final LinearLayout llYes = (LinearLayout) dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = (LinearLayout) dialogView.findViewById(R.id.llCancel);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etTitleLayout.setErrorEnabled(false);
                if (etTitle.getText().toString().trim().isEmpty()) {
                    etTitleLayout.setErrorEnabled(true);
                    etTitleLayout.setError(getResources().getString(R.string.reason_empty_input));
                } else if (Utility.isConnectingToInternet(OtherUserProfileActivity.this)) {
                    input.put(RequestParameters.REASON, "" + etTitle.getText().toString().trim());
                    Log.e("input", "" + input);
                    Utils.showPopup(OtherUserProfileActivity.this, getResources().getString(R.string.user_has_been_reported_msg));
//                    Toast.makeText(OtherUserProfileActivity.this, getResources().getString(R.string.user_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                    API.sendRequestToServerPOST_PARAM(OtherUserProfileActivity.this, url, input);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                    confirmAlert.dismiss();
                } else {
                    Utils.showPopup(OtherUserProfileActivity.this, getResources().getString(R.string.app_no_internet_error));
//                    Toast.makeText(OtherUserProfileActivity.this, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                }
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                confirmAlert.dismiss();
            }
        });


        this.confirmAlert = dialogBuilder.create();
        this.confirmAlert.setCancelable(true);
        this.confirmAlert.show();
    }
}
