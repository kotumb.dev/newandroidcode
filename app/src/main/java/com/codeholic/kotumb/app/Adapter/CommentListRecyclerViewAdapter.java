package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.GroupPostActivity;
import com.codeholic.kotumb.app.Activity.PostDetailsActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.PostComments;
import com.codeholic.kotumb.app.Model.ReplyComment;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommentListRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<PostComments> comments;
    Activity activity;
    List<ReplyComment>reply_comments=new ArrayList<>();
    AlertDialog confirmAlert;
    AlertDialog editCommentAlert;
    AlertDialog replyComments;
    Group group;
    int pagination=0;
    int pastVisiblesItems;
    LinearLayoutManager linearLayoutManager;
    boolean loading = true;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount,visibleItemCountReply;
    private int lastVisibleItem, totalItemCount,totalItemCountReply;
    CommentReplyAdapter commentReplyAdapter;

    public void setUserImageBaseUrl(String base_url) {
        this.userImageBaseUrl = base_url;
    }

    String userImageBaseUrl = "";

    public void setGroupImageBaseUrl(String base_url) {
        this.groupImageBaseUrl = base_url;
    }

    String groupImageBaseUrl = "";

    public CommentListRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<PostComments> comments) {
        this.comments = comments;
        this.activity = activity;
        if (activity instanceof GroupPostActivity)
            group = ((GroupPostActivity) activity).group;
        else if (activity instanceof PostDetailsActivity)
            group = ((PostDetailsActivity) activity).group;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && lastVisibleItem >= (getItemCount() - 1)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }


    @Override
    public int getItemViewType(int position) {
        return comments.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.post_comment_item, parent, false);
            GroupViewHolder GroupViewHolder = new GroupViewHolder(view);
            return GroupViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading_comments, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, final int position) {
        final PostComments comments = this.comments.get(position);
        if (viewholder instanceof GroupViewHolder) {
            final GroupViewHolder holder = (GroupViewHolder) viewholder;
            String text = "" + comments.firstName.trim() + " " + comments.middleName + " " + comments.lastName.trim();
            holder.tvUserName.setText(text.replaceAll("  ", " "));
            if (!comments.comment.trim().isEmpty()) {
                holder.tvCommentContent.setVisibility(View.VISIBLE);
                holder.tvCommentContent.setText(Utils.decode(comments.comment));
            } else {
                holder.tvCommentContent.setVisibility(View.GONE);
            }

            if (position == 0) {
                holder.viewLine.setVisibility(View.GONE);
            } else {
                holder.viewLine.setVisibility(View.VISIBLE);
            }
            new AQuery(activity).id(holder.ivUserImage).image(userImageBaseUrl + comments.avatar.trim(), true, true, 300, R.drawable.ic_user);


            if (comments.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(activity)) ||
                    group.role.equalsIgnoreCase("0")
                    || comments.role.equalsIgnoreCase("1")) {
                holder.llEditPost.setVisibility(View.VISIBLE);
            } else {
                if (group.isMember.equalsIgnoreCase("0")) {
                    holder.llEditPost.setVisibility(View.GONE);
                } else {
                    if (comments.role.equalsIgnoreCase("0")) {
                        holder.llEditPost.setVisibility(View.GONE);
                    } else {
                        holder.llEditPost.setVisibility(View.VISIBLE);
                    }
                }
            }

            setOnClickListener(holder, comments, position);
            holder.tvUserName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 API.openOtherUserProfile(comments.userId,activity);
                }
            });
            final String URL=API.GET_COMMENT_REPLY+comments.post_id+"/"+comments.comment_id+"/";
            holder.comment_reply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.comment_reply_container.getVisibility()==View.VISIBLE){
                        holder.comment_reply_container.setVisibility(View.GONE);
                        reply_comments.clear();
                        pagination=0;
                    }else{
                        holder.comment_reply_loader.setVisibility(View.VISIBLE);
                        getAllReplies(comments,holder,URL);
                    }

                }
            });
            holder.comment_reply_recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }
                @Override
                public void onScrolled( RecyclerView recyclerView, int dx, int dy) {
                    if (loading){
                        System.out.println("Pagination  "+pagination+  " Reply Comment Size  "+reply_comments.size());
                            System.out.println("Pagination Inside  "+pagination+  " Reply Comment Size In  "+reply_comments.size());
//                        if (pagination < reply_comments.size()) //check for scroll down
//                        {
                            visibleItemCountReply = linearLayoutManager.getChildCount();
                            totalItemCountReply = linearLayoutManager.getItemCount();
                            pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                            System.out.println("Visible Count "+visibleItemCountReply);
                        System.out.println("TotalCountReply "+totalItemCountReply);
                        System.out.println("Past Count "+pastVisiblesItems);
                            if ((visibleItemCountReply + pastVisiblesItems) == totalItemCountReply) {
                                loading = false;
                                pagination = pagination + 5;
                               fetchData(URL+pagination,holder);
                            }else{
                            }
//                        }else{
//                            Toast.makeText(activity, "Not In", Toast.LENGTH_SHORT).show();
//                        }
                    }
                }
            });

        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    public void setOnClickListener(final GroupViewHolder holder, final PostComments comment, final int position) {
        holder.llEditPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(activity, holder.llEditPost);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_comments, popup.getMenu());


                if (comment.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(activity)) ||
                        group.role.equalsIgnoreCase("0")
                        || comment.role.equalsIgnoreCase("1") || comment.role.equalsIgnoreCase("0")) {
                    // Log.e("TAG", "inside if");
                    if (group.role.equalsIgnoreCase("0") && comment.role.equalsIgnoreCase("0")) {
                        popup.getMenu().findItem(R.id.action_delete).setVisible(true);
                    }
                    if (comment.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(activity))) {
                        popup.getMenu().findItem(R.id.action_edit).setVisible(true);
                        popup.getMenu().findItem(R.id.action_reply).setVisible(true);
                        popup.getMenu().findItem(R.id.action_delete).setVisible(true);
                    }

                    boolean isMine = comment.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(activity));
                    if (comment.role.equalsIgnoreCase("1") && !isMine) {
                        if (group.role.equalsIgnoreCase("1")) {
                            popup.getMenu().findItem(R.id.action_report).setVisible(true);
                        } else {
                        popup.getMenu().findItem(R.id.action_delete).setVisible(true);
                        }
                    }

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_edit:
                                    editCommentPopup(position);
                                    break;
                                case R.id.action_delete:
                                    String url = API.GROUP_POST_COMMENT_DELETE + comments.get(position).comment_id + "/" + SharedPreferencesMethod.getUserId(activity);
                                    Log.e("TAG", "" + url);
                                    showPopUp(activity.getResources().getString(R.string.delete_comment_dialog_msg), url, API.GROUP_POST_COMMENT_DELETE, position, null);
                                    break;
                                case R.id.action_report:
                                    HashMap<String, Object> input = new HashMap<>();
                                    input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(activity));
                                    input.put(RequestParameters.GROUPID, "" + group.groupId);
                                    input.put(RequestParameters.POSTID, "" + comment.post_id);
                                    input.put(RequestParameters.COMMENTID, "" + comment.comment_id);
                                    //input.put(RequestParameters.DESCRIPTION, "");
                                    reportPopup(activity.getResources().getString(R.string.report_comment_btn_text), API.GROUP_POST_COMMENT_REPORT, API.GROUP_POST_COMMENT_REPORT, position, input);
                                    break;
                                case R.id.action_reply:
                                    commentReplyPopup(comment,position,holder);
                                    break;
                            }
                            return false;
                        }
                    });

                    popup.show();
                }


            }
        });

       /* holder.tvCommentContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("TAG", "LINE COUNT " + holder.tvCommentContent.getLineCount());
            }
        });*/

    }

    private void editCommentPopup(final int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.change_group_name, null);
        dialogBuilder.setView(dialogView);
        final EditText etTitle = dialogView.findViewById(R.id.etTitle);
        final TextView Pop_title = dialogView.findViewById(R.id.Pop_title);
        Pop_title.setText(activity.getResources().getString(R.string.edit_commment_text_hint));
        etTitle.setHint(activity.getResources().getString(R.string.edit_commment_text_hint));
        etTitle.setText(Utils.decode(comments.get(position).comment));
        try {
            etTitle.setSelection(comments.get(position).comment.trim().length());
        } catch (Exception e) {
            e.printStackTrace();
        }
        final TextInputLayout etTitleLayout = dialogView.findViewById(R.id.etTitleLayout);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                etTitleLayout.setErrorEnabled(false);
                String commentText = etTitle.getText().toString().trim();
                if (commentText.isEmpty()) {
                    etTitleLayout.setErrorEnabled(true);
                    etTitleLayout.setError(activity.getResources().getString(R.string.enter_comment_error_text));
                } else if (Utility.isConnectingToInternet(activity)) {
                    HashMap<String, Object> input = new HashMap();
                    input.put(RequestParameters.COMMENT_ID, "" + comments.get(position).comment_id);
                    input.put(RequestParameters.COMMENT, "" + Utils.encode(commentText));
                    Log.e("input", "" + input);
                    API.sendRequestToServerPOST_PARAM(activity, API.GROUP_POST_COMMENT_EDIT, input);
                    comments.get(position).comment = commentText;
                    notifyItemChanged(position);
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                    editCommentAlert.dismiss();
                } else {
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    editCommentAlert.dismiss();
                }
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                editCommentAlert.dismiss();
            }
        });


        this.editCommentAlert = dialogBuilder.create();
        this.editCommentAlert.setCancelable(false);
        this.editCommentAlert.show();
    }



    private void commentReplyPopup(final PostComments postComments, int position, final GroupViewHolder holder){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.change_group_name, null);
        dialogBuilder.setView(dialogView);
        final EditText etTitle = dialogView.findViewById(R.id.etTitle);
        final TextView Pop_title = dialogView.findViewById(R.id.Pop_title);
        Pop_title.setText(activity.getResources().getString(R.string.reply_commment_text_hint));
        etTitle.setHint(activity.getResources().getString(R.string.reply_commment_text_hint));
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        final TextInputLayout etTitleLayout = dialogView.findViewById(R.id.etTitleLayout);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addReplies(postComments,etTitle);
                replyComments.dismiss();
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                replyComments.dismiss();
            }
        });
        this.replyComments = dialogBuilder.create();
        this.replyComments.setCancelable(false);
        this.replyComments.show();
    }





    private void addReplies(PostComments postComments,EditText editText){
        HashMap<String, Object> input = new HashMap();
        input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(activity));
        input.put(RequestParameters.GROUPID, "" + group.groupId);
        input.put(RequestParameters.POSTID, "" +postComments.post_id);
        input.put(RequestParameters.COMMENT, "" + Utils.encode(editText.getText().toString().trim()));
        input.put(RequestParameters.REPLYID,""+postComments.comment_id);
        System.out.println("Reply Add  "+input);
        API.sendRequestToServerPOST_PARAM(activity, API.GROUP_ADD_COMMENT, input);
        Utils.logEventGroupCommented(activity, group.groupId);
//        editText.setText("");
    }



    private void getAllReplies(final PostComments comments, final GroupViewHolder holder,String URL){
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, URL,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println("Comment Reply Response  "+response);
                        holder.comment_reply_container.setVisibility(View.VISIBLE);
                        holder.comment_reply_loader.setVisibility(View.GONE);
                        holder.comment_reply_list.setVisibility(View.VISIBLE);
                        try {
//                            Gson gson = new Gson(); // creates a Gson instance
//                            Type listType = new TypeToken<List<PostComments>>() {
//                            }.getType();
//                            List<ReplyComment> updatedReply = gson.fromJson(response.getString("commentReplies"), listType);
//                            System.out.println("Update Reply  "+updatedReply.size());
                            JSONArray jsonArray = new JSONArray(response.getString("commentReplies"));
                            for (int i=0; i<jsonArray.length(); i++){
                                JSONObject result=jsonArray.getJSONObject(i);
                                System.out.println("Reply Array  "+result.getString("comment"));
                                ReplyComment replyComment=new ReplyComment();
                                replyComment.setCommentId(result.getString("comment_id"));
                                replyComment.setReplyId(result.getString("reply_id"));
                                replyComment.setPostId(result.getString("post_id"));
                                replyComment.setReplyComment(result.getString("comment"));
                                replyComment.setFirstName(result.getString("firstName"));
                                replyComment.setLastName(result.getString("lastName"));
                                replyComment.setGroupID(result.getString("group_id"));
                                replyComment.setReplyAvatar(result.getString("avatar"));
                                reply_comments.add(replyComment);
                                linearLayoutManager = new LinearLayoutManager(activity);
                                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                holder.comment_reply_recyclerview.setLayoutManager(linearLayoutManager);
                                commentReplyAdapter = new CommentReplyAdapter(activity,reply_comments);
                                commentReplyAdapter.notifyDataSetChanged();
                                holder.comment_reply_recyclerview.setHasFixedSize(true);
                                holder.comment_reply_recyclerview.setAdapter(commentReplyAdapter);
                                holder.comment_reply_recyclerview.smoothScrollToPosition(0);
                                System.out.println("Reply Comment Size First    "+reply_comments.size());
                            }
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Comment Reply Response Error "+error.toString());
            }
        });
        Volley.newRequestQueue(activity).add(postRequest);
    }





    private void LoadMoreScroll(final String URL, final GroupViewHolder holder){
        loading = true;
        holder.pagination_loader.setVisibility(View.VISIBLE);
        JsonObjectRequest paginationRequest = new JsonObjectRequest(Request.Method.GET, URL,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println("Second Main Response  "+response+  "URL  "+URL);
                        try{
                            JSONArray jsonArray = new JSONArray(response.getString("commentReplies"));
                            System.out.println("JSON ARRAY RESPONSE  "+jsonArray.toString());
                            for (int i=0; i<jsonArray.length(); i++) {
                                JSONObject result = jsonArray.getJSONObject(i);
                                System.out.println("Second Array  " + result.toString());
                                ReplyComment replyComment = new ReplyComment();
                                replyComment.setCommentId(result.getString("comment_id"));
                                replyComment.setReplyId(result.getString("reply_id"));
                                replyComment.setPostId(result.getString("post_id"));
                                replyComment.setReplyComment(result.getString("comment"));
                                replyComment.setFirstName(result.getString("firstName"));
                                replyComment.setLastName(result.getString("lastName"));
                                replyComment.setGroupID(result.getString("group_id"));
                                replyComment.setReplyAvatar(result.getString("avatar"));
                                reply_comments.add(replyComment);
                                commentReplyAdapter.notifyDataSetChanged();
                                System.out.println("Pagination Second  "+pagination+  " Reply Comment Size Second  "+reply_comments.size());
                            }
                            }catch (Exception ex){
                            System.out.println("Pagination Error "+ex.toString());
                        }
//
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Pagination Volley Error "+error.toString());
            }
        });
        Volley.newRequestQueue(activity).add(paginationRequest);
    }


    private void fetchData(final String URL, final GroupViewHolder holder){
        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                LoadMoreScroll(URL,holder);
                                holder.comment_reply_loader.setVisibility(View.GONE);
                            }
                        },500);
    }





    private void reportPopup(String title, final String url, final String apiUrl, final int position, final HashMap<String, Object> input) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.report_popup, null);
        dialogBuilder.setView(dialogView);
        final EditText etTitle = dialogView.findViewById(R.id.etTitle);
        final TextView Pop_title = dialogView.findViewById(R.id.Pop_title);
        Pop_title.setText(title);
        final TextInputLayout etTitleLayout = dialogView.findViewById(R.id.etTitleLayout);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etTitleLayout.setErrorEnabled(false);
                if (etTitle.getText().toString().trim().isEmpty()) {
                    etTitleLayout.setErrorEnabled(true);
                    etTitleLayout.setError(activity.getResources().getString(R.string.reason_empty_input));
                } else if (Utility.isConnectingToInternet(activity)) {
                    input.put(RequestParameters.DESCRIPTION, "" + etTitle.getText().toString().trim());
//                    Toast.makeText(activity, activity.getResources().getString(R.string.comment_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.comment_has_been_reported_msg));
                    Log.e("input", "" + input);
                    API.sendRequestToServerPOST_PARAM(activity, url, input);
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                    confirmAlert.dismiss();
                } else {
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                }
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                confirmAlert.dismiss();
            }
        });


        this.confirmAlert = dialogBuilder.create();
        this.confirmAlert.setCancelable(true);
        this.confirmAlert.show();
    }

    private void showPopUp(String title, final String url, final String apiUrl, final int position, final HashMap<String, Object> input) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);
        final TextView tvCancel = dialogView.findViewById(R.id.tvCancel);


        tvTitle.setText(title);
        /*if(!showYes){
            view.setVisibility(View.GONE);
            llYes.setVisibility(View.GONE);
        }

        if(!showCancel){
            view.setVisibility(View.GONE);
            llCancel.setVisibility(View.GONE);
        }*/

        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    confirmAlert.dismiss();
                    return;
                }
                if (input == null) {
                    API.sendRequestToServerGET(activity, url, apiUrl);
                    notifyItemRemoved(position);
                    comments.remove(position);
                    notifyItemRangeChanged(0, comments.size());
                    confirmAlert.dismiss();
                } else {
                    API.sendRequestToServerPOST_PARAM(activity, url, input);
//                    Toast.makeText(activity, activity.getResources().getString(R.string.comment_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    confirmAlert.dismiss();
                }
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                confirmAlert.dismiss();
            }
        });

        confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();

    }


    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        View more;
        TextView tvUserName, tvTime, tvCommentContent,comment_reply;
        ImageView ivUserImage;
        LinearLayout cvContainer, llEditPost,comment_reply_container,comment_reply_list;
        RecyclerView comment_reply_recyclerview;
        ProgressBar comment_reply_loader,pagination_loader;
        View viewLine;


        GroupViewHolder(View view) {
            super(view);
            tvUserName = view.findViewById(R.id.tvUserName);
            ivUserImage = view.findViewById(R.id.ivUserImage);
            cvContainer = view.findViewById(R.id.cvContainer);
            tvTime = view.findViewById(R.id.tvTime);
            comment_reply = view.findViewById(R.id.comment_reply);
            tvCommentContent = view.findViewById(R.id.tvPostContent);
            viewLine = view.findViewById(R.id.viewLine);
            llEditPost = view.findViewById(R.id.llEditPost);
            more = view.findViewById(R.id.more);
            pagination_loader=view.findViewById(R.id.pagination_loader);
            comment_reply_container=view.findViewById(R.id.comment_reply_container);
            comment_reply_list=view.findViewById(R.id.comment_reply_list);
            comment_reply_recyclerview=view.findViewById(R.id.comment_reply_recyclerview);
            comment_reply_loader=view.findViewById(R.id.comment_reply_loader);
        }
    }





}