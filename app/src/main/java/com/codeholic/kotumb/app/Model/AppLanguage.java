package com.codeholic.kotumb.app.Model;

import org.json.JSONException;
import org.json.JSONObject;

public class AppLanguage {
    private String id;
    private String language;
    private String lang_code;
    private String status;

    public AppLanguage(JSONObject jsonObject) throws JSONException {
        this.id = jsonObject.getString("id");
        this.language = jsonObject.getString("language");
        this.lang_code = jsonObject.getString("lang_code");
        this.status = jsonObject.getString("status");
        System.out.println("Get All Language  "+ jsonObject.getString("language")  +"  "+ "Code  "+jsonObject.getString("lang_code"));
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLang_code() {
        return lang_code;
    }

    public void setLang_code(String lang_code) {
        this.lang_code = lang_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
