package com.codeholic.kotumb.app.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ImageView;

import com.codeholic.kotumb.app.Adapter.CategoryAdapter;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ShowVideos extends AppCompatActivity {

    public interface VideoInterface{
        void StopAllVideos();
        void PauseAllVideos();
    }


    public TabLayout tabLayout;
    ViewPager viewPager;
    List<VideoInterface> list;
    FloatingActionButton camera;
    Toolbar mToolbar;
    TelephonyManager mgr;
    PhoneStateListener phoneStateListener;
    boolean onCall=false;
    public String selectTab="";
    boolean condition=false;

    public synchronized void registerData(int a,VideoInterface data){
        if (data != null){
            list.add(a,data);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_videos);

        if(isNetworkAvailable()){

            initStartup();
            condition=true;

        }
        else{

            Utils.showPopup(this, getResources().getString(R.string.app_no_internet_error), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(isNetworkAvailable())
                                handler.postDelayed(this, 1000);
                            else {
                                // do actions
                                initStartup();
                                condition = true;
                            }
                        }
                    }, 1000);
                }
            });



         }
    }

    void initStartup(){

        list=new ArrayList<>(2);

        tabLayout=findViewById(R.id.tabs);
        viewPager=findViewById(R.id.viewpager);
        mToolbar=findViewById(R.id.toolbar_actionbar);
        camera=findViewById(R.id.camera_video);


        CategoryAdapter categoryAdapter=new CategoryAdapter(this,getSupportFragmentManager());
        viewPager.setAdapter(categoryAdapter);
        tabLayout.setupWithViewPager(viewPager);

        if(getIntent().hasExtra("tab")){
            if(getIntent().getStringExtra("tab").equals("0")){
                selectTab="0";
            }
            else{
                selectTab="1";
            }
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition()==0){
                    list.get(1).PauseAllVideos();
                }else {
                    list.get(0).PauseAllVideos();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        API.sendRequestToServerGET(ShowVideos.this, API.VIDEO_LIMITS+"/"+ SharedPreferencesMethod.getUserId(ShowVideos.this), API.VIDEO_LIMITS);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.videos_title_text));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    //Incoming call: Pause music
                    if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M) {
                        System.out.println("onstop======11");
                        onCall=true;
                    }
                } else if(state == TelephonyManager.CALL_STATE_IDLE) {
                    //Not in call: Play music
                    onCall=false;
                } else if(state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    //A call is dialing, active or on hold
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        onCall=true;
                        System.out.println("onstop======12");
                    }
                }
                super.onCallStateChanged(state, incomingNumber);
            }
        };
        mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if(mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }
    }

    @Override
    protected void onDestroy() {
        list.get(0).StopAllVideos();
        list.get(1).StopAllVideos();
        if (mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }
        super.onDestroy();

    }

    public void showpop(AlertDialog.Builder dialogBuilder, ImageView dismiss_dialog){
        final AlertDialog confirmAlert = dialogBuilder.create();
        dismiss_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();
    }

    public void RefereshLimit(){
        API.sendRequestToServerGET(ShowVideos.this, API.VIDEO_LIMITS+"/"+ SharedPreferencesMethod.getUserId(ShowVideos.this), API.VIDEO_LIMITS);
    }

    int usedLimit=0,availableLimit=0,totalLimit_used=0,totalLimit=0;
    public void getResponse(final JSONObject outPut, int i) {
        System.out.println("Video Limits Response   " + outPut.toString());
        if (i == 3) {
            try {
                usedLimit = Integer.valueOf(outPut.getString("user_uplopad_video_limit_used"));
                availableLimit = Integer.valueOf(outPut.getString("user_upload_video_limit"));
                totalLimit_used = Integer.valueOf(outPut.getString("total_user_upload_video_limit_used"));
                totalLimit = Integer.valueOf(outPut.getString("total_user_upload_video_limit"));
                System.out.println("LIMIT===" + usedLimit + "===" + availableLimit + "====" + totalLimit);

                SharedPreferencesMethod.setString(this, "INSIDE_SHARE", outPut.getString("inside_message_on_video_approved_and_published"));
                SharedPreferencesMethod.setString(this, "OUTSIDE_SHARE", outPut.getString("outside_message_on_video_approved_and_published"));
                if (totalLimit == 0) {
                    Utils.showPopup(ShowVideos.this, outPut.getString("video_upload_limit_exceed_popup_message"));
                } else {
                    camera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (usedLimit < availableLimit) {
                                /*Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                takeVideoIntent.putExtra(android.provider.MediaStore.EXTRA_VIDEO_QUALITY, 1);
                                takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
                                takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Environment.getExternalStorageState());
                                if(takeVideoIntent.resolveActivity(getPackageManager())!=null){
                                    startActivityForResult(takeVideoIntent, VIDEO_REQUEST_CODE);
                                }*/
                                if (!onCall) {
                                  //  if(SharedPreferencesMethod.getBoolean(ShowVideos.this,"FirstTimeCamera")) {
                                      //  Intent intent = new Intent(ShowVideos.this, CameraActivity.class);
                                  //      startActivity(intent);
                                //    }
                              //      else{
                                    new AlertDialog.Builder(ShowVideos.this).setTitle("Instructions")
                                            .setMessage(getResources().getString(R.string.camera_instruction))
                                            .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent intent = new Intent(ShowVideos.this, CameraActivity.class);
                                                    startActivity(intent);
                                                }
                                            }).show();
                                        /*Utils.showPopup(ShowVideos.this, getResources().getString(R.string.camera_instruction), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent intent = new Intent(ShowVideos.this, CameraActivity.class);
                                                startActivity(intent);
                                            }
                                        });*/
                              //      }
                                } else {
                                    Utils.showPopup(ShowVideos.this, "Your call is currently active.");
                                }
                            } else if (totalLimit == totalLimit_used) {
                                try {
                                    Utils.showPopup(ShowVideos.this, outPut.getString("video_upload_limit_exceed_popup_message"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    Utils.showPopup(ShowVideos.this, outPut.getString("user_video_upload_limit_exceed_popup_message"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    public void SetTab(int posi){
        tabLayout.getTabAt(posi).select();
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
