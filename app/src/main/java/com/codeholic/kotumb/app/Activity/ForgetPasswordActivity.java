package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.View.OtpEditText;
import com.github.florent37.viewtooltip.ViewTooltip;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.github.florent37.viewtooltip.ViewTooltip.ALIGN.START;
import static com.github.florent37.viewtooltip.ViewTooltip.Position.TOP;

public class ForgetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.llUserNameContainer)
    LinearLayout llUserNameContainer;

    @BindView(R.id.llQuestionsContainer)
    LinearLayout llQuestionsContainer;

    @BindView(R.id.llPasswordContainer)
    LinearLayout llPasswordContainer;

    @BindView(R.id.etUserNameLayout)
    TextInputLayout etUserNameLayout;

    @BindView(R.id.etUserName)
    EditText etUserName;

    @BindView(R.id.cvSubmit)
    CardView cvSubmit;

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    @BindView(R.id.tvSubmit)
    TextView tvSubmit;

    @BindView(R.id.tvQuestionOne)
    TextView tvQuestionOne;

    @BindView(R.id.etQuestionOneLayout)
    TextInputLayout etQuestionOneLayout;

    @BindView(R.id.etQuestionOne)
    EditText etQuestionOne;

    @BindView(R.id.tvQuestionTwo)
    TextView tvQuestionTwo;

    @BindView(R.id.etQuestionTwoLayout)
    TextInputLayout etQuestionTwoLayout;

    @BindView(R.id.etQuestionTwo)
    EditText etQuestionTwo;

    @BindView(R.id.cvQuestionSubmit)
    CardView cvQuestionSubmit;

    @BindView(R.id.etPasswordLayout)
    TextInputLayout etPasswordLayout;

    @BindView(R.id.etCPasswordLayout)
    TextInputLayout etCPasswordLayout;

    @BindView(R.id.etCPassword)
    EditText etCPassword;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.cvUpdate)
    CardView cvUpdate;

    @BindView(R.id.pbChangeLoading)
    ProgressBar pbChangeLoading;

    @BindView(R.id.tvUpdate)
    TextView tvUpdate;

    @BindView(R.id.ivUserName)
    ImageView ivUserName;

    @BindView(R.id.ivQuestionOne)
    ImageView ivQuestionOne;

    @BindView(R.id.ivQuestionTwo)
    ImageView ivQuestionTwo;


    Activity context;
    JSONObject Questions = new JSONObject();
    ViewTooltip v;

    com.codeholic.kotumb.app.View.ProgressBar progressBar;
    AlertDialog otpAlertDialog;
    HashMap<String, Object> otpRequest = new HashMap<>();
    private String otp = "";

    //oncreate method for initializing content view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        setToolbar();
        setClickListeners();
        init();
    }

    //for initializing
    private void init() {
        context = this;
        setTextWatcher(etUserName, etUserNameLayout);
        setTextWatcher(etQuestionOne, etQuestionOneLayout);
        setTextWatcher(etQuestionTwo, etQuestionTwoLayout);
        setTextWatcher(etPassword, etPasswordLayout);
        setTextWatcher(etCPassword, etCPasswordLayout);
    }

    //setting textwatcher for every textbox
    private void setTextWatcher(EditText editText, final TextInputLayout textInputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    textInputLayout.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //setting toolbar and header title
    private void setToolbar() {

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        TextView header = (TextView) findViewById(R.id.header);

        header.setText(getResources().getString(R.string.login_forget_pwd));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    //user name validation
    private boolean forgetPasswordValidation() {
        setErrorDisable();
        if (etUserName.getText().toString().trim().isEmpty()) {
            etUserNameLayout.setErrorEnabled(true);
            etUserNameLayout.setError(getResources().getString(R.string.username_empty_input));
            return false;
        }
        return true;
    }

    //password fields validation
    private boolean passwordValidation() {
        setErrorDisable();

        //Pattern p = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z!@#$%^&*\\d]{8,12}");
        //boolean userPassMatcher = p.matcher(etPassword.getText().toString().trim()).find();

        if (etPassword.getText().toString().trim().isEmpty()) {
            etPasswordLayout.setErrorEnabled(true);
            etPasswordLayout.setError(getResources().getString(R.string.password_empty_input));
            return false;
        } else if (etPassword.getText().toString().trim().length() < 6) {
            etPasswordLayout.setErrorEnabled(true);
            etPasswordLayout.setError(getResources().getString(R.string.password_incorrect_input_atleast));
            return false;
        } /*else if (!userPassMatcher) {
            etPasswordLayout.setErrorEnabled(true);
            etPasswordLayout.setError(getResources().getString(R.string.password_incorrect_input_atleast));
            return false;
        } */ else if (!etPassword.getText().toString().trim().equalsIgnoreCase(etCPassword.getText().toString().trim())) {
            etCPasswordLayout.setErrorEnabled(true);
            etCPasswordLayout.setError(getResources().getString(R.string.confirm_password_input_match));
            return false;
        }
        return true;
    }

    //Questions fields validation
    private boolean QuestionsValidation() {
        setErrorDisable();
        try {
            /*if (etQuestionOne.getText().toString().trim().isEmpty()) {
                etQuestionOneLayout.setErrorEnabled(true);
                etQuestionOneLayout.setError(getResources().getString(R.string.security_answer_empty_input));
                return false;
            }
            if (!etQuestionOne.getText().toString().trim().equalsIgnoreCase(Questions.getString(ResponseParameters.AnswerOne))) {
                etQuestionOneLayout.setErrorEnabled(true);
                etQuestionOneLayout.setError(getResources().getString(R.string.security_answer_incorrect_input));
                return false;
            }*/
            if (etQuestionTwo.getText().toString().trim().isEmpty()) {
                etQuestionTwoLayout.setErrorEnabled(true);
                etQuestionTwoLayout.setError(getResources().getString(R.string.security_answer_empty_input));
                return false;
            }
            if (!etQuestionTwo.getText().toString().trim().equalsIgnoreCase(Questions.getString(ResponseParameters.AnswerTwo))) {
                etQuestionTwoLayout.setErrorEnabled(true);
                etQuestionTwoLayout.setError(getResources().getString(R.string.security_answer_incorrect_input));
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    //disabling errors form InputLayout
    private void setErrorDisable() {
        etQuestionOneLayout.setErrorEnabled(false);
        etUserNameLayout.setErrorEnabled(false);
        etQuestionTwoLayout.setErrorEnabled(false);
        etPasswordLayout.setErrorEnabled(false);
        etCPasswordLayout.setErrorEnabled(false);
    }

    //setting click listener on views
    private void setClickListeners() {
        cvSubmit.setOnClickListener(this);
        cvQuestionSubmit.setOnClickListener(this);
        cvUpdate.setOnClickListener(this);
        ivUserName.setOnClickListener(this);
        ivQuestionTwo.setOnClickListener(this);
        ivQuestionOne.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cvSubmit: // submiting user name
                if (forgetPasswordValidation()) { // validation
                    if (!Utility.isConnectingToInternet(context)) { // checking net connection
                        final Snackbar snackbar = Snackbar.make(v, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }

                    HashMap<String, Object> input = new HashMap<>();
                    input.put(RequestParameters.USERNAME, "" + etUserName.getText().toString().trim());
                    input.put(RequestParameters.LANG, SharedPreferencesMethod.getDefaultLanguageName(this));
                    Log.e("input", API.FORGET_PASSWORD + " " + input);
                    tvSubmit.setVisibility(GONE);
                    pbLoading.setVisibility(VISIBLE);
                    cvSubmit.setClickable(false);
                    etUserName.setEnabled(false);
                    API.sendRequestToServerPOST_PARAM(context, API.FORGET_PASSWORD, input); // forget password service call
                }
                break;
            case R.id.cvQuestionSubmit: // questions submit click
                if (QuestionsValidation()) { // validation

                    try {
                        if (!Utility.isConnectingToInternet(context)) { // check net connection
                            final Snackbar snackbar = Snackbar.make(v, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        progressBar = new com.codeholic.kotumb.app.View.ProgressBar(ForgetPasswordActivity.this);
                        progressBar.show(getResources().getString(R.string.app_please_wait_text));
                        long time = System.currentTimeMillis();
                        SharedPreferencesMethod.setLongSharedPreference(ForgetPasswordActivity.this,"otp_time",time);
                        API.sendRequestToServerPOST_PARAM(ForgetPasswordActivity.this, API.SEND_OTP, otpRequest); // resend web service call
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
            case R.id.cvUpdate: // update password
                if (passwordValidation()) { // validation
                    try {
                        if (!Utility.isConnectingToInternet(context)) { // check net connection
                            final Snackbar snackbar = Snackbar.make(v, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        HashMap<String, Object> input = new HashMap<>();
                        input.put(RequestParameters.USERID, "" + Questions.getString(ResponseParameters.UserId));
                        input.put(RequestParameters.PASSWORD, "" + etPassword.getText().toString().trim());
                        input.put(RequestParameters.CONFIRM_PASSWORD, "" + etCPassword.getText().toString().trim());
                        tvUpdate.setVisibility(GONE);
                        pbChangeLoading.setVisibility(VISIBLE);
                        cvUpdate.setClickable(false);
                        etPassword.setEnabled(false);
                        etCPassword.setEnabled(false);
                        API.sendRequestToServerPOST_PARAM(context, API.RESET_PASSWORD, input); // service call for resetting password
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.ivUserName:
                showToolTip(v, getResources().getString(R.string.forget_pwd_username_helptext));
                break;

            case R.id.ivQuestionOne:
            case R.id.ivQuestionTwo:
                showToolTip(v, getResources().getString(R.string.forget_pwd_squestion_helptext));
                break;
        }
    }

    private void OtpPopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.otp_popup, null);
        dialogBuilder.setView(dialogView);
        final OtpEditText etOtp = (OtpEditText) dialogView.findViewById(R.id.etOtp);
        final TextView tvOtpHint = (TextView) dialogView.findViewById(R.id.tvOtpHint);
        final TextView tvResend = (TextView) dialogView.findViewById(R.id.tv_resend);
        final TextView tv_timer=(TextView)dialogView.findViewById(R.id.tv_otp_timer);
        tv_timer.setVisibility(VISIBLE);
        tvResend.setVisibility(GONE);
        final CountDownTimer countDownTimer=new CountDownTimer(30000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tv_timer.setText(String.valueOf(getResources().getString(R.string.otp_timer_text_1)+" "+(int)millisUntilFinished/1000)+" sec. "+getResources().getString(R.string.otp_timer_text_2));
            }
            @Override
            public void onFinish() {
                tv_timer.setVisibility(View.GONE);
                tvResend.setVisibility(View.VISIBLE);
            }
        };
        //timer for otp resend start
        countDownTimer.start();
        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    if (s.length() >= 6) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(etOtp.getWindowToken(), 0);
                        if (s.toString().equalsIgnoreCase(otp)) { // checking otp
                            verifyOtp();
                            otpAlertDialog.dismiss();
                        } else {
                            Utils.showPopup(ForgetPasswordActivity.this, getResources().getString(R.string.otp_incorrect_input));
//                            Toast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.otp_incorrect_input), Toast.LENGTH_SHORT).show();
                        }
                    }
                    tvOtpHint.setVisibility(GONE);
                } else {
                    tvOtpHint.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!Utility.isConnectingToInternet(ForgetPasswordActivity.this)) { //check net connection
                        return;
                    }
                    progressBar = new com.codeholic.kotumb.app.View.ProgressBar(ForgetPasswordActivity.this);
                    progressBar.show(getResources().getString(R.string.app_please_wait_text));
                    otpAlertDialog.dismiss();
                    long time=System.currentTimeMillis();
                    if(time-SharedPreferencesMethod.getLongSharedPreferences(ForgetPasswordActivity.this,"otp_time")>=30000) {
                        API.sendRequestToServerPOST_PARAM(ForgetPasswordActivity.this, API.SEND_OTP, otpRequest); // resend web service call}
                        SharedPreferencesMethod.setLongSharedPreference(ForgetPasswordActivity.this,"otp_time",time);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tvResend.setVisibility(View.GONE);
                tv_timer.setVisibility(View.VISIBLE);
                countDownTimer.start();
            }
        });
        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                otpAlertDialog.dismiss();
            }
        });
        otpAlertDialog = dialogBuilder.create();
        otpAlertDialog.setCancelable(false);
        otpAlertDialog.show();
    }

    private void verifyOtp() {
        llQuestionsContainer.setVisibility(GONE);
        llPasswordContainer.setVisibility(VISIBLE);
    }

    void showToolTip(View view, String toolTipText) {
        if (v != null) {
            v.close();
        }
        v = ViewTooltip
                .on(view)
                .autoHide(true, 20000)
                .clickToHide(true)
                .align(START)
                .position(TOP)
                .text(toolTipText)
                .textColor(Color.WHITE)
                .color(getResources().getColor(R.color.colorPrimary))
                .corner(10)
                .onHide(new ViewTooltip.ListenerHide() {
                    @Override
                    public void onHide(View view) {
                        v = null;
                    }
                });
        v.show();
    }

    //enabling fields
    void setEnable() {
        cvSubmit.setClickable(true);
        etUserName.setEnabled(true);
        tvSubmit.setVisibility(VISIBLE);
        pbLoading.setVisibility(GONE);

        tvUpdate.setVisibility(VISIBLE);
        pbChangeLoading.setVisibility(GONE);
        cvUpdate.setClickable(true);
        etPassword.setEnabled(true);
        etCPassword.setEnabled(true);
    }

    //getting response from service
    public void getResponse(JSONObject outPut, int i) {
        if (progressBar != null) {
            progressBar.dismiss(); // dismissing progressbar
        }
        try {

            Log.e("op", "" + outPut);
            if (i == 0) { // response for forget password
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors) || !outPut.has(ResponseParameters.QUESTIONS)) {
                        final Snackbar snackbar = Snackbar.make(cvSubmit, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                        setEnable();
                    } else {
                        //parsing response
                        parseResponse(outPut, i);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error
                    setEnable();
                    final Snackbar snackbar = Snackbar.make(cvSubmit, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net
                    setEnable();
                    final Snackbar snackbar = Snackbar.make(cvSubmit, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            }
            if (i == 1) { // resetting password
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        //error
                        final Snackbar snackbar = Snackbar.make(cvSubmit,Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                        setEnable();
                    } else if (outPut.has(ResponseParameters.Success)) {
                        //success
//                        Toast.makeText(context, getResources().getString(R.string.password_reset_success), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(ForgetPasswordActivity.this, getResources().getString(R.string.password_reset_success));
                        finish();
                    } else {
                        //error
                        final Snackbar snackbar = Snackbar.make(cvSubmit, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                        snackbar.show();
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error
                    setEnable();
                    final Snackbar snackbar = Snackbar.make(cvSubmit, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net
                    setEnable();
                    final Snackbar snackbar = Snackbar.make(cvSubmit, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            } else if (i == 2) {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(cvSubmit, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        otp = outPut.getString(ResponseParameters.Otp).trim();// updating otp
                        OtpPopup();
                        /*final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.verify_otp), Toast.LENGTH_SHORT);
                        snackbar.show();*/
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { // error
                    final Snackbar snackbar = Snackbar.make(cvSubmit, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { // no net connection
                    final Snackbar snackbar = Snackbar.make(cvSubmit, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //parsing response from server
    private void parseResponse(JSONObject outPut, int i) {
        try {
            if (i == 0) {
                Questions = outPut.getJSONObject(ResponseParameters.QUESTIONS);
                llUserNameContainer.setVisibility(GONE);
                llQuestionsContainer.setVisibility(VISIBLE);
                tvQuestionOne.setText(Questions.getString(ResponseParameters.QuestionOne));
                tvQuestionTwo.setText(Questions.getString(ResponseParameters.QuestionTwo));

                otpRequest = new HashMap<>();
                otpRequest.put(RequestParameters.LANG, SharedPreferencesMethod.getDefaultLanguageName(this));
                otpRequest.put(RequestParameters.MOBILE, "" + outPut.getString(ResponseParameters.MobileNumber));
                otpRequest.put(RequestParameters.USERID, "" + Questions.getString(ResponseParameters.UserId));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
