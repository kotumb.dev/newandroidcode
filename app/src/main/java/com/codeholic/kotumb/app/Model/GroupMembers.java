package com.codeholic.kotumb.app.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DELL on 11/1/2017.
 */

public class GroupMembers implements Serializable {

    @SerializedName("id")
    public String id = "";

    @SerializedName("isBlocked")
    public String isBlocked = "";

    @SerializedName(value="groupId", alternate={"group_id"})
    public String groupId = "";

    @SerializedName("userId")
    public String userId = "";

    @SerializedName("role")
    public String role = "";

    @SerializedName("owner")
    public String owner = "";

    @SerializedName("status")
    public String status = "";

    @SerializedName("createdAt")
    public String createdAt = "";

    @SerializedName("updatedAt")
    public String updatedAt = "";

    @SerializedName("firstName")
    public String firstName = "";

    @SerializedName("middleName")
    public String middleName = "";

    @SerializedName("lastName")
    public String lastName = "";

    @SerializedName("avatar")
    public String avatar = "";

    @SerializedName("profileHeadline")
    public String profileHeadline = "";



}
