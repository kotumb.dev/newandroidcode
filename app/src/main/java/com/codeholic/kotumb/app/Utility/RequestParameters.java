package com.codeholic.kotumb.app.Utility;

/**
 * Created by DELL on 10/26/2017.
 */

public interface RequestParameters {
    public String USERNAME = "username";
    public String DISPLAYNAME = "displayname";
    public String PASSWORD = "password";
    public String OLD_PASSWORD = "oldpassword";
    public String CONFIRM_PASSWORD = "password_confirm";
    public String TERMS = "terms";
    public String MOBILE = "mobile";
    public String MOBILE_NUMBER = "mobile";
    public String USERID = "userid";
    public String VIEWERID = "viewerid";
    public String TOKEN = "token";

    public String REDEEMPOINTS = "redeem_points";
    public String FIRSTNAME = "firstname";
    public String LASTNAME = "lastname";
    public String DATEOFBIRTH = "dateofbirth";
    public String DATE = "date";
    public String GENDER = "gender";
    public String EMAIL = "email";
    public String CITY = "city";
    public String STATE = "state";
    public String ZIP = "zip";
    public String PROFILE_SUMMERY = "profile_summary";
    public String PROFILE_HEADLINE = "profile_headline";
    public String ALTERNATE_NUMBER = "alternate_number";
    public String QUESTION_ID1 = "question_id1";
    public String ANSWER1 = "answer1";
    public String QUESTION_ID2 = "question_id2";
    public String ANSWER2 = "answer2";



    public String SHOW_EMAIL = "show_email";
    public String SHOW_MOBILE = "show_mobile";
    public String SHOW_ADDRESS = "show_address";


    public String ORGANISATION = "organization";
    public String ROLE = "role";
    public String FROMYEAR = "fromyear";
    public String TOYEAR = "toyear";
    public String CURRENTORG = "currentorg";
    public String EXPID = "expid";

    public String ADHAR_NUMBER = "aadhar_number";
    public String CONFIRM_ADHAR_NUMBER = "confirm_aadhar_number";
    public String NAME_ADHAR= "name_on_aadhar";
    public String FATHER_NAME = "father_name";
    public String PINCODE = "pincode";
    public String VERIFIED = "verified";

    public String COURSE_NAME = "coursename";
    public String SCHOOL_NAME = "schoolname";
    public String PASSING_YEAR = "passingyear";
    public String DESCRIPTION = "description";
    public String EDUCATIONID = "educationid";

    public String COURSE = "course";
    public String SCHOOL = "school";
    public String YEAR = "year";
    public String AVATAR = "avatar";

    public String CURRENT_SKILLS = "current_skills";
    public String CURRENT_CUSTOM_SKILLS = "current_custom_skills_";
    public String DESIRED_SKILLS = "desired_skills";
    public String INTEREST = "interests";
    public String RESUMEID = "resumeid";
    public String ADDRESS = "address";


    public String USERID1 = "user_id_1";
    public String USERID2 = "user_id_2";
    public String ACTIONUSERID = "action_user_id";
    public String STATUS = "status";

    String TITLE = "title";
    String TIME = "time";
    String NOTE = "note";
    String NOTEID = "noteid";
    String TO_USERID = "to_user";
    String FROM_USERID = "from_user";
    String MESSAGE = "message";
    String MESSAGE_MEDIA = "media";
    String MESSAGE_MEDIA_TYPE = "mediaType";
    String RECOMMEND_ID = "recommend_id";
    String SECTOR = "sector";
    String LANGUAGE = "languages";
    String LANG = "language";
    String CURRENT_INDUSTRIES = "current_industries_";
    String NAME = "name";
    String SUBJECT = "subject";
    String URL = "url";
    String PROFILE_URL = "profileUrl";
    String COUNTRY = "country";
    String GROUP_NAME = "groupName";
    String GROUP_SUMMARY = "groupSummary";
    String LOGO = "logo";
    String CATEGORY = "category";
    String PRIVACY = "privacy";
    String ADMINS = "admins";
    String MEMBERS = "members";
    String GROUPID = "groupid";
    String CONTENT = "content";
    String IMAGE = "image";
    String POSTID = "post_id";
    String REPLYID = "reply_id";
    String COMMENT = "comment";
    String COMMENT_ID = "comment_id";
    String OLD_IMAGES = "old_images";
    String SHOW_DOB = "show_dob";
    String SHOW_GENDER = "show_gender";
    String MIDDLENAME = "middlename";
    String SHAREDPOSTID = "sharedPostId";
    String SHAREDPOSTGROUPID = "sharedPostGroupId";
    String SHAREDPOSTUSERID = "sharedPostUserId";
    String FROMUSERID = "fromUserId";
    String TOUSERID = "toUserId";
    String REASON = "reason";
    String COMMENTID = "commentid";
    String CATEGORY_CUSTOM ="custom" ;
    String SUPPORT_CHAT ="support_chat" ;
    String IS_CHALLENGED = "isChallenged";
    boolean PROFILE_SUMMERY_POP_SHOW=true;
}
