package com.codeholic.kotumb.app.Activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactActivity extends AppCompatActivity {
    @BindView(R.id.header)
    TextView header;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    com.codeholic.kotumb.app.View.ProgressBar progressBar = null;
    @BindView(R.id.wv)
    WebView webView;


    //custom clicklistener class
    class onClickListener implements OnClickListener {
        onClickListener() {
        }

        public void onClick(View v) {
            ContactActivity.this.onBackPressed();
        }
    }

    private void startWebView(String url) {

        //Create new webview Client to show progress dialog
        //When opening a url or click on link

        webView.setWebViewClient(new WebViewClient() {

            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource (WebView view, String url) {
                //pbLoading.setVisibility(View.VISIBLE);
            }
            public void onPageFinished(WebView view, String url) {
                Log.e("TAG", "onPageFinished");
                try{
                    Log.e("TAG 1", "onPageFinished");
                    pbLoading.setVisibility(View.GONE);
                }catch(Exception exception){
                    exception.printStackTrace();
                }
            }
        });

        webView.setWebChromeClient(new WebChromeClient());

        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);

        // Other webview options

        webView.getSettings().setLoadWithOverviewMode(true);
        /*webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.getSettings().setBuiltInZoomControls(true);
        */



        //Load url in webview
        webView.loadUrl(url);


    }

    // Open previous opened link from history on webview when back button pressed

    @Override
    // Detect when the back button is pressed
    public void onBackPressed() {
        if(webView.canGoBack()) {
            webView.goBack();
        } else {
            // Let the system handle the back button
            super.onBackPressed();
        }
    }

    //oncreate method for setting content view
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_resume);
        ButterKnife.bind(this);
        initToolbar();
        initwebView();
        findViewById(R.id.fab).setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);

    }

    //initialize web view show resume
    private void initwebView() {
        String url = API.contactUsLink + SharedPreferencesMethod.getUserId(this);
        startWebView(url);
    }






    //setting toolbar and header title
    private void initToolbar() {
        setSupportActionBar(this.mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        this.header.setText(getResources().getString(R.string.feedback_title));
        this.mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

}
