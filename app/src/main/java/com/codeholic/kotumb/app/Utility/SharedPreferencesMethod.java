package com.codeholic.kotumb.app.Utility;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.codeholic.kotumb.app.BroadcastReciever.NotifyReminder;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.Services.GetPreLoadDataService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Set;

/**
 * Created by sumesh on 12/6/2017.
 */
public class SharedPreferencesMethod {


    public static final String USER_ABOUT = "USER_ABOUT";
    public static final String USERID = "USER_ID";
    private static final String DEFAULT_LANGUAGE_NAME = "default_language_name";
    public static Context appContext;
    private static String PREFERENCE = "kotumb_prefrences";
    public static String MODE = "MODE";
    public static final String ORGANISATIONS = "ORGANISATIONS";
    public static final String DEFAULT_LANGUAGE = "default_language";
    public static final String SCHOOLS = "SCHOOLS";
    public static final String COURSES = "COURSES";
    public static final String JOBROLES = "JOBROLES";
    public static final String SKILLS = "SKILLS";
    public static final String INDUSTRIES = "INDUSTRIES";
    public static final String AD = "AD";

    public static final String CURRENT_COUNTRY = "CURRENT_COUNTRY";
    public static final String USER_REGION = "USER_REGION";
    public static final String USER_CITY = "USER_CITY";
    public static final String USER_INTEREST = "USER_INTEREST";
    public static final String USER_ADDRESS = "USER_ADDRESS";
    public static final String USER_DOB = "USER_DOB";
    public static final String USER_PIC = "user_profile_pic";
    //  public static final String USER_PIC = "USER_PIC";
    public static final String USER_DEVICE_TOKEN = "USER_DEVICE_TOKEN";
    public static final String FCM_REGISTER_ID = "FCM_REGISTER_ID";
    public static final String DEVICE_ID = "DEVICE_ID";
    public static final String DEVICE_LATITUDE = "DEVICE_LATITUDE";
    public static final String DEVICE_LONGITUDE = "DEVICE_LONGITUDE";
    public static final String APK_VERSION = "APK_VERSION";
    public static final String CITY_ID = "CITY_ID";
    public static final String CITY_IMAGE = "CITY_IMAGE";
    public static final String CITY_AUDIO_URL = "CITY_AUDIO_URL";
    public static final String CITY_INTRO = "CITY_INTRO";
    public static final String IS_NOTIFICATION_DISABLED = "IS_NOTIFICATION_DISABLED";
    public static final String NO_OF_NOTIFICATION = "NO_OF_NOTIFICATION";
    public static final String IS_NOTIFICATION_SOUND_DISABLED = "IS_NOTIFICATION_SOUND_DISABLED";
    public static final String IS_NOTIFICATION_VIBRATIONS_DISABLED = "IS_NOTIFICATION_VIBRATIONS_DISABLED";
    public static final String LOCATION_SERVICE_MODE = "LOCATION_SERVICE_MODE";
    public static final String INTEREST_A = "INTEREST_A";
    public static final String INTEREST_B = "INTEREST_B";
    public static final String INTEREST_C = "INTEREST_C";
    public static final String DOWNLOAD_FILE_ID = "DOWNLOAD_FILE_ID";
    public static final String DOWNLOAD_WALK_ID = "DOWNLOAD_WALK_ID";
    public static final String LOCATION_DIALOG_NEVER = "LOCATION_DIALOG_NEVER";
    public static final String OFFLINE_DIALOG_NEVER = "OFFLINE_DIALOG_NEVER";
    public static final String IS_DOWNLOADING = "IS_DOWNLOADING";
    public static final String AUTO_PLAY_AUDIO = "AUTO_PLAY_AUDIO";
    public static final String CITY_LAT = "city_latitude";
    public static final String CITY_LONG = "city_longitude";
    public static String ALL_LANGUAGES = "ALL_LANGUAGES";
    public static String POP_UP="firstTime";
    public static String POP_UP_2="stopTime";
    public static String LANGUAGES = "languages";
    public static String REMEMBER_USERNAME = "userName";
    public static String REMEMBER_PASSWORD = "password";
    public static String ONLYONCE_PROFILE_COMPLETION = "onlyonce_profile_completion";
    public static String REFRAL_USER_COUNT = "refral_user_count";


    public static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        return editor;
    }

    public static SharedPreferences getSharedPreference(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        return sharedpreferences;
    }

    public static boolean getBoolean(Context context, boolean name) {
        SharedPreferences sharedPreferences = getSharedPreference(context);
        return (boolean) sharedPreferences.getBoolean(String.valueOf(name), false);
    }
    public static boolean getBoolean(Context context, String name) {
        SharedPreferences sharedPreferences = getSharedPreference(context);
        return (boolean) sharedPreferences.getBoolean(name, false);
    }

    public static boolean getBooleanDefaultTrue(Context context, String name) {
        SharedPreferences sharedPreferences = getSharedPreference(context);
        return sharedPreferences.getBoolean(name, true);
    }

    public static void setStringSharedPreferencehistory(Context context, String name, Set<String> value) {
        appContext = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        // editor.clear();
        editor.putStringSet(name, value);
        editor.commit();
    }

    public static Set<String> getStringSharedPreferenceshistory(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return settings.getStringSet(name, null);
    }

    public static void setBoolean(Context context, String name, boolean value) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(name, value);
        editor.commit();
    }

    public static String getString(Context context, String name) {
        SharedPreferences sharedPreferences = getSharedPreference(context);
        return sharedPreferences.getString(name, "");
    }

    public static void setString(Context context, String name, String value) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putString(name, value);
        editor.commit();
    }

    public static void setUserDetails(Context context, User user) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putString(ResponseParameters.UserName, user.getUserName());
        editor.putString(ResponseParameters.UserId, user.getUserId());
        editor.putString(ResponseParameters.MobileNumber, user.getMobileNumber());
        editor.putString(ResponseParameters.ReferenceId, user.getReferenceId());
        editor.putString(ResponseParameters.Otp, user.getOtp());
        editor.putString(ResponseParameters.IsConfirmed, user.getIsConfirmed());
        editor.putString(ResponseParameters.DisplayName, user.getDisplayName());
        editor.commit();
        setUserInfo(context, user);
        setUpdateData(context);
    }

    public static void setUpdateData(Context context) {
        GetPreLoadDataService.startActionGetNameOrg(context);
        GetPreLoadDataService.startActionGetSchools(context);
        GetPreLoadDataService.startActionGetCourses(context);
        GetPreLoadDataService.startActionGetRoles(context);
        GetPreLoadDataService.startActionGetSkills(context);
        GetPreLoadDataService.startActionGetIndustries(context);
        GetPreLoadDataService.startActionGetLanguage(context);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        Intent notifyReminder = new Intent(context, NotifyReminder.class);
        notifyReminder.setAction(NotifyReminder.SERVICE_CALL);
        PendingIntent pi = PendingIntent.getBroadcast(context, NotifyReminder.CALLSERVICE,
                notifyReminder, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pi);
    }

    public static void setUserInfo(Context context, User user) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putString(ResponseParameters.FirstName, user.getOnlyFirstName());
        editor.putString(ResponseParameters.LastName, user.getLastName());
        editor.putString(ResponseParameters.DateOfBirth, user.getDOB());
        editor.putString(ResponseParameters.Gender, user.getGender());
        editor.putString(ResponseParameters.Email, user.getEmail());
        editor.putString(ResponseParameters.Avatar, user.getAvatar());
        editor.putString(ResponseParameters.Address, user.getAddress());
        editor.putString(ResponseParameters.City, user.getCity());
        editor.putString(ResponseParameters.State, user.getState());
        editor.putString(ResponseParameters.ZIP, user.getZipCode());
        editor.putString(ResponseParameters.AlternateMobile, user.getAlternateMobile());
        editor.putString(ResponseParameters.PROFILE_HEADLINE, user.getProfileHeadline());
        editor.putString(ResponseParameters.PROFILE_SUMMERY, user.getProfileSummary());
        editor.putString(ResponseParameters.EMAIL_VISIBILITY, user.getEmailVisibility());
        editor.putString(ResponseParameters.MOBILE_VISIBILITY, user.getMobileVisibility());
        editor.putString(ResponseParameters.ADDRESS_VISIBILITY, user.getAddressVisibility());
        editor.putString(ResponseParameters.DOB_VISIBILITY, user.getDobVisibility());
        editor.putString(ResponseParameters.GENDER_VISIBILITY, user.getGenderVisibility());
        editor.putString(ResponseParameters.MiddleName, user.getMiddleName());
        editor.putString(ResponseParameters.UserName, user.getUserName());
        editor.putString(ResponseParameters.IS_CHALLENGED, user.getIsChallenged());
        editor.commit();
    }

    public static int getInt(Context context, String name) {
        SharedPreferences sharedPreferences = getSharedPreference(context);
        return sharedPreferences.getInt(name, 0);
    }

    public static void setInt(Context context, String name, int value) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putInt(name, value);
        editor.commit();
    }

    // for username string preferences
    public static void setDoubleSharedPreference(Context context, String name, double value) {
        appContext = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(name, (float) value);
        editor.commit();
    }

    public static Double getDoubleSharedPreferences(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return (double) settings.getFloat(name, 0.0f);
    }

    public static void setLongSharedPreference(Context context, String name, long value) {
        appContext = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(name, value);
        editor.commit();
    }

    public static long getLongSharedPreferences(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return settings.getLong(name, 0l);
    }

    public static void clear(Context context) {
        try {
            SharedPreferences.Editor editor = getEditor(context);
            String languageToLoad = getDefaultLanguage(context);
            clearNotes(context);
            clearSerViceCall(context);
            editor.remove(SharedPreferencesMethod.PREFERENCE);
            editor.clear();
            editor.commit();
            context.getSharedPreferences(SharedPreferencesMethod.PREFERENCE, 0).edit().clear().commit();
            SharedPreferencesMethod.setString(context, SharedPreferencesMethod.DEFAULT_LANGUAGE, languageToLoad);
            NotificationManager notificationManager = (NotificationManager) context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //setBoolean(context, SharedPreferencesMethod.FIRST_TIME, true);
    }

    public static void clearSerViceCall(Context context) {
        Intent notifyReminder = new Intent(context, NotifyReminder.class);
        notifyReminder.setAction(NotifyReminder.SERVICE_CALL);
        PendingIntent pi = PendingIntent.getBroadcast(context, NotifyReminder.CALLSERVICE,
                notifyReminder, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.cancel(pi);
    }

    public static void clearNotes(Context context) {
        try {
            JSONArray NOTELIST = new JSONArray();
            if (!getHasString(context, ResponseParameters.NOTELIST).isEmpty()) {
                NOTELIST = new JSONArray(getHasString(context, ResponseParameters.NOTELIST));
            }
            for (int i = 0; i < NOTELIST.length(); i++) {
                JSONObject noteList = NOTELIST.getJSONObject(i);
                Intent intent = new Intent(context, NotifyReminder.class);
                intent.setAction(noteList.getString(ResponseParameters.Id));
                PendingIntent pendingIntent =
                        PendingIntent.getBroadcast(context,
                                Integer.parseInt(noteList.getString(ResponseParameters.Id)), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager alarmManager =
                        (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(pendingIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getUserId(Context context) {
        return getString(context, ResponseParameters.UserId);
    }

    public static User getUserInfo(Context context) {
        User user = new User();
        user.setUserId(getUserId(context));
        user.setFirstName(getString(context, ResponseParameters.FirstName));
        user.setMiddleName(getString(context, ResponseParameters.MiddleName));
        user.setLastName(getString(context, ResponseParameters.LastName));
        user.setDOB(getString(context, ResponseParameters.DateOfBirth));
        user.setMobileNumber(getString(context, ResponseParameters.MobileNumber));
        user.setGender(getString(context, ResponseParameters.Gender));
        user.setEmail(getString(context, ResponseParameters.Email));
        user.setAvatar(getString(context, ResponseParameters.Avatar));
        user.setAddress(getString(context, ResponseParameters.Address));
        user.setCity(getString(context, ResponseParameters.City));
        user.setState(getString(context, ResponseParameters.State));
        user.setZipCode(getString(context, ResponseParameters.ZIP));
        user.setAlternateMobile(getString(context, ResponseParameters.AlternateMobile));
        user.setProfileHeadline(getString(context, ResponseParameters.PROFILE_HEADLINE));
        user.setProfileSummary(getString(context, ResponseParameters.PROFILE_SUMMERY));
        user.setMobileVisibility(getString(context, ResponseParameters.MOBILE_VISIBILITY));
        user.setEmailVisibility(getString(context, ResponseParameters.EMAIL_VISIBILITY));
        user.setAddressVisibility(getString(context, ResponseParameters.ADDRESS_VISIBILITY));
        user.setUserName(getString(context,ResponseParameters.UserName));
        user.setDobVisibility(getString(context, ResponseParameters.DOB_VISIBILITY));
        user.setGenderVisibility(getString(context, ResponseParameters.GENDER_VISIBILITY));
        user.setMiddleName(getString(context, ResponseParameters.MiddleName));
        user.setIsChallenged(getString(context, ResponseParameters.IS_CHALLENGED));
        return user;
    }

    public static String getDefaultLanguage(Context context) {
        SharedPreferences sharedPreferences = getSharedPreference(context);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            Log.e("locale", ""+context.getResources().getConfiguration().getLocales().get(0));
        } else{
            Log.e("locale else", ""+context.getResources().getConfiguration().locale);
        }*/
        return sharedPreferences.getString(SharedPreferencesMethod.DEFAULT_LANGUAGE, "en");
    }

    public static void setDefaultLanguage(Context context, String languageToLoad) {
//        SharedPreferences sharedPreferences = getSharedPreference(context);
        setString(context, SharedPreferencesMethod.DEFAULT_LANGUAGE, languageToLoad);
        GetPreLoadDataService.startActionGetNameOrg(context);
        GetPreLoadDataService.startActionGetSchools(context);
        GetPreLoadDataService.startActionGetCourses(context);
        GetPreLoadDataService.startActionGetRoles(context);
        GetPreLoadDataService.startActionGetSkills(context);
        GetPreLoadDataService.startActionGetIndustries(context);
        GetPreLoadDataService.startActionGetLanguage(context);
    }


    public static String getHasString(Context context, String name) {
        SharedPreferences sharedPreferences = getSharedPreference(context);
        if (sharedPreferences.contains(name)) {
            return sharedPreferences.getString(name, "");
        } else {
            return "";
        }

    }

    public static String getArray(Context context, String name) {
        SharedPreferences sharedPreferences = getSharedPreference(context);
        if (sharedPreferences.contains(name)) {
            return sharedPreferences.getString(name, "");
        } else {
            return "[]";
        }

    }

    public static void setDefaultLanguageName(Activity activity, String language) {
        setString(activity, SharedPreferencesMethod.DEFAULT_LANGUAGE_NAME, language);
    }

    public static String getDefaultLanguageName(Activity activity) {
        String string = getString(activity, SharedPreferencesMethod.DEFAULT_LANGUAGE_NAME);
        if(string==null){
            string="english";
        }
        if(string.isEmpty()){
            string="english";
        }
        return string;
    }
}
