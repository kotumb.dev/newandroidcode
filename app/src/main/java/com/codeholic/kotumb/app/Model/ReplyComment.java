package com.codeholic.kotumb.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ReplyComment implements Parcelable {

    String commentId;
    String postId;
    String replyId;
    String replyComment;
    String firstName;
    String lastName;
    String replyAvatar;
    String groupID;
    String userID;


    protected ReplyComment(Parcel in) {
        commentId = in.readString();
        postId = in.readString();
        replyId = in.readString();
        replyComment = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        userID = in.readString();
        groupID = in.readString();
        replyAvatar = in.readString();
    }


    public ReplyComment(){

    }
    public static final Creator<ReplyComment> CREATOR = new Creator<ReplyComment>() {
        @Override
        public ReplyComment createFromParcel(Parcel in) {
            return new ReplyComment(in);
        }

        @Override
        public ReplyComment[] newArray(int size) {
            return new ReplyComment[size];
        }
    };

    public void setCommentId(String commentId){
        this.commentId=commentId;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setPostId(String postId){
        this.postId=postId;
    }

    public String getPostId() {
        return postId;
    }

    public void setReplyId(String replyId){
        this.replyId=replyId;
    }

    public String getReplyId() {
        return replyId;
    }


    public void setUserID(String userID){
        this.userID=userID;
    }

    public String getUserID() {
        return userID;
    }



    public void setGroupID(String groupID){
        this.groupID=groupID;
    }

    public String getGroupID() {
        return groupID;
    }






    public void setReplyAvatar(String replyAvatar){
        this.replyAvatar=replyAvatar;
    }

    public String getReplyAvatar() {
        return replyAvatar;
    }



    public void setReplyComment(String replyComment){
        this.replyComment=replyComment;
    }


    public String getReplyComment() {
        return replyComment;
    }


    public void setFirstName(String firstName){
        this.firstName=firstName;
    }

    public String getFirstName() {
        return firstName;
    }


    public void setLastName(String lastName){
        this.lastName=lastName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(commentId);
        dest.writeString(postId);
        dest.writeString(replyId);
        dest.writeString(replyComment);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(userID);
        dest.writeString(groupID);
        dest.writeString(replyAvatar);
    }
}
