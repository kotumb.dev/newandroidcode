package com.codeholic.kotumb.app.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.core.util.Pair;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Adapter.MyPagerAdapter;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.GroupMembers;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ConvertBitmap;
import com.codeholic.kotumb.app.Utility.ImgeCroper.Crop;
import com.codeholic.kotumb.app.Utility.PermissionsUtils;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.RotateImageCode;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.fragments.MembersFragment;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupInfoActivity extends AppCompatActivity implements View.OnClickListener, IPickResult {
    public static GroupInfoActivity groupInfoActivity;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    PopupWindow popupWindow;

    @BindView(R.id.ivGroupImage)
    ImageView ivGroupImage;

    @BindView(R.id.lladdMembers)
    LinearLayout lladdMembers;

    @BindView(R.id.llJoinGroup)
    RelativeLayout llJoinGroup;

    @BindView(R.id.join_title)
    TextView join_title;

    @BindView(R.id.leave_group_btn)
    TextView leave_group_btn;

    /*@BindView(R.id.line)
    LinearLayout line;*/

    @BindView(R.id.fabChangeImage)
    FloatingActionButton fabChangeImage;

    @BindView(R.id.tvGroupName)
    TextView tvGroupName;

    @BindView(R.id.tvGroupMembers)
    TextView tvGroupMembers;

    @BindView(R.id.tvGroupSummary)
    TextView tvGroupSummary;

//    @BindView(R.id.rvGroupMembers)
//    RecyclerView rvGroupMembers;

//    @BindView(R.id.rvGroupRequestedList)
//    RecyclerView rvGroupRequestedList;
//
//    @BindView(R.id.rvGroupBlockedList)
//    RecyclerView rvGroupBlockededList;
//
//    @BindView(R.id.rvGroupInvitedList)
//    RecyclerView rvGroupInvitedList;

//    @BindView(R.id.llInviteRequests)
//    LinearLayout llInviteRequests;
//
//    @BindView(R.id.llRequestedRequests)
//    LinearLayout llRequestedRequests;
//
//    @BindView(R.id.llBlockRequests)
//    LinearLayout llBlockRequests;
//
//    @BindView(R.id.llRequests)
//    LinearLayout llRequests;

    @BindView(R.id.tab)
    TabLayout tab;

    @BindView(R.id.pager)
    ViewPager pager;

    @BindView(R.id.ivEditGroupAbout)
    ImageView ivEditGroupAbout;

    @BindView(R.id.llGroupAbout)
    LinearLayout llGroupAbout;

    @BindView(R.id.tvGroupCategory)
    TextView tvGroupCategory;

    private LinearLayoutManager linearLayoutManager;
//    MemberListRecyclerViewAdapter memberListRecyclerViewAdapter;
//    MemberListRecyclerViewAdapter invitedMemberListRecyclerViewAdapter = null;
//    MemberListRecyclerViewAdapter blockedMemberListRecyclerViewAdapter;
//    MemberListRecyclerViewAdapter requestMemberListRecyclerViewAdapter;


    AlertDialog titleAlert;
    AlertDialog changeGroupTitle;
    AlertDialog alert;
    com.codeholic.kotumb.app.View.ProgressBar progressBar = null;
    Context context;
    public static final int REQUEST_CROP = 6709;
    String logo = "";
    public Group group = new Group();
    private BroadcastReceiver receiver;
    Bundle bundle;
    public static List<GroupMembers> members = new ArrayList<>();
    public static List<GroupMembers> member_requested = new ArrayList<>();
    public static List<GroupMembers> member_invited = new ArrayList<>();
    public static List<GroupMembers> member_blocked = new ArrayList<>();
    MenuItem editGroup = null;
    int type = 0;
    private boolean ignore;
    public MembersFragment membersFragment;
    public MembersFragment invitedMembersFragment;
    public MembersFragment requestedMembersFragment;
    public MembersFragment blockedMembersFragment;
    ArrayList<Pair<String, Fragment>> fragments = new ArrayList<>();
    // oncreate method initializing view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_info);
        ButterKnife.bind(this);
        initToolbar();
        init();
//        setupRecyclerView();
        setClickListener();
        setBroadCast();
        //setTextWatcher(etNote, etNoteLayout);

        //getCategories();

        setupViewPager();

    }


    /**
     * Status-
     * 1 = pending
     * 2 = approved
     * 3 = blocked
     * 4 = invited
     *
     * Role -
     * 0 = Admin
     * 1 = Member
     */
    private void setupViewPager() {
        GroupInfoActivity.groupInfoActivity = this;


        membersFragment = new MembersFragment();
        setArguments(membersFragment, 2);
        fragments.add(new Pair<String, Fragment>("Group Members", membersFragment));

        invitedMembersFragment = new MembersFragment();
        setArguments(invitedMembersFragment, 4);
        fragments.add(new Pair<String, Fragment>("Invited Members", invitedMembersFragment));

        requestedMembersFragment = new MembersFragment();
        setArguments(requestedMembersFragment, 1);
        fragments.add(new Pair<String, Fragment>("Group Requests", requestedMembersFragment));

        blockedMembersFragment = new MembersFragment();
        setArguments(blockedMembersFragment, 3);
        fragments.add(new Pair<String, Fragment>("Blocked Members", blockedMembersFragment));

        MyPagerAdapter adapter = new MyPagerAdapter(fragments, getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(10);
        tab.setupWithViewPager(pager);
        if (group.role.equalsIgnoreCase("1")){
            setLimitations(adapter);//method-Arpit Kanda
        }else {
            pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }
                @Override
                public void onPageSelected(int i) {
                    if (i == 0) {
                        membersFragment.refresh();
                    }
                    if (i == 1) {
                        invitedMembersFragment.refresh();
                    }
                    if (i == 2) {
                        requestedMembersFragment.refresh();
                    }
                    if (i == 3) {
                        blockedMembersFragment.refresh();
                    }
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
        }
    }

//method created by- Arpit Kanda set user and admin Limitation
    public void setLimitations( MyPagerAdapter adapter){
            tab.removeTabAt(1);
            tab.removeTabAt(2);
            tab.removeTabAt(1);
            fragments.remove(1);
            fragments.remove(2);
            fragments.remove(1);
            pager.setAdapter(adapter);
            pager.setOffscreenPageLimit(1);
            tab.setupWithViewPager(pager);
            tab.setSelectedTabIndicatorColor(Color.rgb(245,106,49));
    }


    private void setArguments(MembersFragment membersFragment, int i) {
        String baseUrl = bundle.getString(ResponseParameters.IMAGE_BASE_URL);
        Bundle args = new Bundle();
        args.putInt("status", i);
        args.putString("role", group.role);
        args.putString("group_id", group.groupId);
        args.putString("baseUrl", baseUrl);
        membersFragment.setArguments(args);
    }

    private void init() {
        context = this;
        bundle = getIntent().getBundleExtra(ResponseParameters.BUNDLE);
        type = bundle.getInt(ResponseParameters.TYPE, 0);
        members = (ArrayList<GroupMembers>) bundle.getSerializable(ResponseParameters.GROUP_MEMBERS);
        member_requested = (ArrayList<GroupMembers>) bundle.getSerializable(ResponseParameters.GROUP_MEMBERS_REQUESTED);
        member_blocked = (ArrayList<GroupMembers>) bundle.getSerializable(ResponseParameters.GROUP_MEMBERS_BLOCKED);
        member_invited = (ArrayList<GroupMembers>) bundle.getSerializable(ResponseParameters.GROUP_MEMBERS_INVITED);

        Log.e("members list", "" + members.size());

        if (!ignore) {
            group = (Group) bundle.getSerializable(ResponseParameters.GROUP);
        }

        setImageText();
        if (group.role.trim().equalsIgnoreCase("0")) {
            fabChangeImage.show();
            lladdMembers.setVisibility(View.VISIBLE);
//            llRequests.setVisibility(View.VISIBLE);
            ivEditGroupAbout.setVisibility(View.VISIBLE);
            llGroupAbout.setOnClickListener(this);
        }else{
            lladdMembers.setVisibility(View.GONE);
//            llRequests.setVisibility(View.VISIBLE);
            ivEditGroupAbout.setVisibility(View.GONE);
//            llGroupAbout.setOnClickListener(this);
        }
        //llRequests.setVisibility(View.VISIBLE);

        if (!ignore) {
            for (GroupMembers member : member_requested) {
                if (member.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(this))) {
                    group.status = "1";
                }
            }

            for (GroupMembers member : member_invited) {
                if (member.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(this))) {
                    group.status = "4";
                }
            }
        }

        if (group.isMember.equalsIgnoreCase("0")) {
            llJoinGroup.setVisibility(View.VISIBLE);
            llJoinGroup.setOnClickListener(this);
            if (group.status.trim().equalsIgnoreCase("1")) {
                join_title.setText(getResources().getString(R.string.group_requested_text));
                leave_group_btn.setVisibility(View.VISIBLE);
                leave_group_btn.setOnClickListener(this);
            } else if (group.status.trim().equalsIgnoreCase("4")) {
                join_title.setText(getResources().getString(R.string.group_invited_text));
                leave_group_btn.setVisibility(View.VISIBLE);
                leave_group_btn.setText("Decline Request");
                leave_group_btn.setOnClickListener(this);
            } else if (group.status.trim().equalsIgnoreCase("0") || group.status.trim().isEmpty()) {
                join_title.setText(getResources().getString(R.string.group_join_text_long));
                leave_group_btn.setVisibility(View.GONE);
            }
        }else{
            llJoinGroup.setVisibility(View.GONE);
        }
    }

    void setImageText() {
        new AQuery(this).id(ivGroupImage).image(bundle.getString(ResponseParameters.GROUP_IMG_BASE_URL) + group.logo, true, true, 300, R.drawable.ic_user);
        tvGroupName.setText(group.groupName.trim());
        tvGroupSummary.setText(group.groupSummary.trim());
        tvGroupMembers.setText(group.members_count + " " + getResources().getString(R.string.group_member_text));
        tvGroupCategory.setText(group.category_name);
    }

    //broadcast for update user list and user disconnection
    private void setBroadCast() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //do something based on the intent's action
                String trim = intent.getAction().trim();
                if (trim.equalsIgnoreCase(ResponseParameters.GROUP_UPDATED)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.logo = updatedGroup.logo;
                        group.groupName = updatedGroup.groupName;
                        group.groupSummary = updatedGroup.groupSummary;
                        group.category = updatedGroup.category;
                        group.category_name = updatedGroup.category_name;
                        group.privacy = updatedGroup.privacy;
                        group.members_count = updatedGroup.members_count;
                        group.admins_count = updatedGroup.admins_count;
                        setImageText();
                    }
                } else if (trim.equalsIgnoreCase(ResponseParameters.GROUP_INVITED)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        tvGroupMembers.setText(group.members_count + " " + getResources().getString(R.string.group_member_text));
                        List<GroupMembers> invited_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                        int positionStart = member_invited.size();
                        member_invited.addAll(invited_members);

//                        if (invitedMemberListRecyclerViewAdapter != null) {
//                            invitedMemberListRecyclerViewAdapter.notifyItemRangeInserted(positionStart, invited_members.size());
//                            invitedMemberListRecyclerViewAdapter.notifyItemRangeChanged(0, member_invited.size());
//                            Log.e("test if", "test " + member_invited.size());
//                        }
                        if (member_invited.size() > 0) {
//                            llInviteRequests.setVisibility(View.VISIBLE);
                        } else {
//                            llInviteRequests.setVisibility(View.GONE);
                        }

                    }
                } else if (trim.equalsIgnoreCase(ResponseParameters.GROUP_REQUEST)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        tvGroupMembers.setText(group.members_count + " " + getResources().getString(R.string.group_member_text));
                        List<GroupMembers> requested_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                        int positionStart = member_requested.size();
                        member_requested.addAll(requested_members);

//                        if (requestMemberListRecyclerViewAdapter != null) {
//                            requestMemberListRecyclerViewAdapter.notifyItemRangeInserted(positionStart, requested_members.size());
//                            requestMemberListRecyclerViewAdapter.notifyItemRangeChanged(0, member_requested.size());
//                            Log.e("test if", "test " + member_requested.size());
//                        }
                        if (member_requested.size() > 0) {
//                            llRequestedRequests.setVisibility(View.VISIBLE);
                        } else {
//                            llRequestedRequests.setVisibility(View.GONE);
                        }

                    }
                } else if (trim.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_DELETE)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        tvGroupMembers.setText(group.members_count + " " + getResources().getString(R.string.group_member_text));
                        String status = intent.getStringExtra(ResponseParameters.STATUS);
                        String userId = intent.getStringExtra(ResponseParameters.UserId);

                        if (userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(GroupInfoActivity.this))) {
                            finish();
                            return;
                        }

                        List<GroupMembers> selectedList = new ArrayList<>();
                        //1=pending,2=approved,3=blocked, 4 = invited
                        if (status.trim().equalsIgnoreCase("2")) {
                            selectedList = members;
                        } else if (status.trim().equalsIgnoreCase("3")) {
                            selectedList = member_blocked;
                        } else if (status.trim().equalsIgnoreCase("4")) {
                            selectedList = member_invited;
                        } else if (status.trim().equalsIgnoreCase("1")) {
                            selectedList = member_requested;
                        }

                        for (int i = 0; i < selectedList.size(); i++) {
                            if (selectedList.get(i).userId.trim().equalsIgnoreCase(userId.trim())) {
                                selectedList.remove(i);
                                if (status.trim().equalsIgnoreCase("2")) {
//                                    memberListRecyclerViewAdapter.notifyItemRemoved(i);
//                                    memberListRecyclerViewAdapter.notifyItemRangeChanged(0, selectedList.size());
                                } else if (status.trim().equalsIgnoreCase("3")) {
//                                    blockedMemberListRecyclerViewAdapter.notifyItemRemoved(i);
//                                    blockedMemberListRecyclerViewAdapter.notifyItemRangeChanged(0, selectedList.size());
                                    if (selectedList.size() <= 0) {
                                        hideBlockLayout();
                                    }
                                } else if (status.trim().equalsIgnoreCase("4")) {
//                                    invitedMemberListRecyclerViewAdapter.notifyItemRemoved(i);
//                                    invitedMemberListRecyclerViewAdapter.notifyItemRangeChanged(0, selectedList.size());
                                    if (selectedList.size() <= 0) {
                                        hideInviteLayout();
                                    }
                                } else if (status.trim().equalsIgnoreCase("1")) {
//                                    requestMemberListRecyclerViewAdapter.notifyItemRemoved(i);
//                                    requestMemberListRecyclerViewAdapter.notifyItemRangeChanged(0, selectedList.size());
                                    if (selectedList.size() <= 0) {
                                        hideRequestLayout();
                                    }
                                }
                                break;
                            }
                        }

                    }
                } else if (trim.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_ROLE_UPDATE)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        tvGroupMembers.setText(group.members_count + " " + getResources().getString(R.string.group_member_text));
                        List<GroupMembers> group_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                        for (int i = 0; i < members.size(); i++) {
                            /// add role
                            if (group_members.get(0).userId.trim().equalsIgnoreCase(members.get(i).userId.trim())) {
                                if (group_members.get(0).userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(GroupInfoActivity.this).trim())) {
                                    group.role = group_members.get(0).role;
                                    if (group.role.trim().equalsIgnoreCase("0")) {
                                        fabChangeImage.show();
                                        lladdMembers.setVisibility(View.VISIBLE);
//                                        llRequests.setVisibility(View.VISIBLE);
                                        ivEditGroupAbout.setVisibility(View.VISIBLE);
                                        if (editGroup != null)
                                            editGroup.setVisible(true);
                                    }
                                }
                                members.remove(i);
                                members.add(i, group_members.get(0));
//                                if (memberListRecyclerViewAdapter != null) {
//                                    memberListRecyclerViewAdapter.notifyItemChanged(i);
//                                }
                                break;
                            }
                        }

                        if (group_members.get(0).userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(context))) {
                            if (member_blocked.size() > 0) {
//                                llBlockRequests.setVisibility(View.VISIBLE);
                            } else {
//                                llBlockRequests.setVisibility(View.GONE);
                            }

                            if (member_invited.size() > 0) {
//                                llInviteRequests.setVisibility(View.VISIBLE);
                            } else {
//                                llInviteRequests.setVisibility(View.GONE);
                            }

                            if (member_requested.size() > 0) {
//                                llRequestedRequests.setVisibility(View.VISIBLE);
                            } else {
//                                llRequestedRequests.setVisibility(View.GONE);
                            }
                        }
                    }
                } else if (trim.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_ACCEPT)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        tvGroupMembers.setText(group.members_count + " " + getResources().getString(R.string.group_member_text));
                        Log.e("test", "test");

                        List<GroupMembers> group_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                        boolean removed = false;
                        for (int i = 0; i < member_requested.size(); i++) {
                            if (group_members.get(0).userId.trim().equalsIgnoreCase(member_requested.get(i).userId.trim())) {
                                member_requested.remove(i);
//                                requestMemberListRecyclerViewAdapter.notifyItemRemoved(i);
//                                requestMemberListRecyclerViewAdapter.notifyItemRangeChanged(0, member_requested.size());
                                if (member_requested.size() <= 0) {
                                    hideRequestLayout();
                                }
                                removed = true;
                                break;
                            }
                        }
                        if (!removed) {
                            if (member_blocked.size() == 0) {
                                hideBlockLayout();
                            }
                            for (int i = 0; i < member_blocked.size(); i++) {
                                if (group_members.get(0).userId.trim().equalsIgnoreCase(member_blocked.get(i).userId.trim())) {
                                    member_blocked.remove(i);
                                    try{
//                                        blockedMemberListRecyclerViewAdapter.notifyItemRemoved(i);
//                                        blockedMemberListRecyclerViewAdapter.notifyItemRangeChanged(0, member_blocked.size());
                                    }catch (Exception ex){
                                        ex.printStackTrace();
                                    }

                                    if (member_blocked.size() <= 0) {
                                        hideBlockLayout();
                                    }
                                    removed = true;
                                    break;
                                }
                            }
                        }
                        if (!removed)
                            for (int i = 0; i < member_invited.size(); i++) {
                                if (group_members.get(0).userId.trim().equalsIgnoreCase(member_invited.get(i).userId.trim())) {
                                    member_invited.remove(i);
//                                    invitedMemberListRecyclerViewAdapter.notifyItemRemoved(i);
//                                    invitedMemberListRecyclerViewAdapter.notifyItemRangeChanged(0, member_invited.size());
                                    if (member_invited.size() <= 0) {
                                        hideInviteLayout();
                                    }
                                    break;
                                }
                            }

                        members.addAll(group_members);
//                        if (memberListRecyclerViewAdapter != null) {
//                            // memberListRecyclerViewAdapter.notifyDataSetChanged();
//                            memberListRecyclerViewAdapter.notifyItemRangeInserted(members.size() - group_members.size(), group_members.size());
//                            memberListRecyclerViewAdapter.notifyItemRangeChanged(0, members.size());
//                        }
                    }
                } else if (trim.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_BLOCK)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        tvGroupMembers.setText(group.members_count + " " + getResources().getString(R.string.group_member_text));
                        List<GroupMembers> group_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                        for (int i = 0; i < members.size(); i++) {
                            if (group_members.get(0).userId.trim().equalsIgnoreCase(members.get(i).userId.trim())) {
//                                Toast.makeText(context, String.valueOf(members.get(i)), Toast.LENGTH_SHORT).show();
                                members.remove(i);
//                                memberListRecyclerViewAdapter.notifyItemRemoved(i);
//                                memberListRecyclerViewAdapter.notifyItemRangeChanged(0, members.size());
                                break;
                            }
                        }

                        if (group_members.get(0).userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(GroupInfoActivity.this))) {
                            finish();
                        }

//                        Log.e("TAG", "" + blockedMemberListRecyclerViewAdapter.getItemCount());
                        member_blocked.addAll(group_members);

//                        if (blockedMemberListRecyclerViewAdapter != null) {
//                            blockedMemberListRecyclerViewAdapter.notifyItemRangeInserted(member_blocked.size() - group_members.size(), group_members.size());
//                            blockedMemberListRecyclerViewAdapter.notifyItemRangeChanged(0, member_blocked.size());
//                        }else{
//                            Toast.makeText(context, "Value Is Null", Toast.LENGTH_SHORT).show();
//                        }
                        if (member_blocked.size() > 0) {
//                            llBlockRequests.setVisibility(View.VISIBLE);
                        } else {
//                            llBlockRequests.setVisibility(View.GONE);
                        }
                    }
                } else if (trim.equalsIgnoreCase(ResponseParameters.GROUP_DELETED)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        tvGroupMembers.setText(group.members_count + " " + getResources().getString(R.string.group_member_text));
                        finish();
                        return;
                    }
                } else if (trim.equalsIgnoreCase(ResponseParameters.GROUP_DELETED_ALL)) {
                    if (group.category.equalsIgnoreCase(intent.getStringExtra(ResponseParameters.CATEGORY))) {
//                        Toast.makeText(context, R.string.this_group_was_deleted, Toast.LENGTH_SHORT).show();
                        Utils.showPopup(GroupInfoActivity.this, getResources().getString(R.string.this_group_was_deleted));
                        finish();
                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.GROUP_UPDATED);
        filter.addAction(ResponseParameters.GROUP_INVITED);
        filter.addAction(ResponseParameters.GROUP_MEMBER_DELETE);
        filter.addAction(ResponseParameters.GROUP_MEMBER_ROLE_UPDATE);
        filter.addAction(ResponseParameters.GROUP_MEMBER_ACCEPT);
        filter.addAction(ResponseParameters.GROUP_MEMBER_BLOCK);
        filter.addAction(ResponseParameters.GROUP_DELETED);
        filter.addAction(ResponseParameters.GROUP_REQUEST);
        filter.addAction(ResponseParameters.GROUP_DELETED_ALL);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {

        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        GroupInviteActivity.clearSelection();
        super.onDestroy();
    }

//    private void setupRecyclerView() {
//        linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        rvGroupMembers.setLayoutManager(linearLayoutManager);
//        memberListRecyclerViewAdapter = new MemberListRecyclerViewAdapter(rvGroupMembers, this, members);
//        memberListRecyclerViewAdapter.setImageBaseUrl(bundle.getString(ResponseParameters.IMAGE_BASE_URL));
//        memberListRecyclerViewAdapter.setType(type);
//        memberListRecyclerViewAdapter.isMember = true;
//        memberListRecyclerViewAdapter.myRole = group.role;
//        rvGroupMembers.setNestedScrollingEnabled(true);
//        rvGroupMembers.addItemDecoration(
//                new DividerItemDecoration(this, R.drawable.divider));
//        rvGroupMembers.setAdapter(memberListRecyclerViewAdapter);
//
//
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        rvGroupRequestedList.setLayoutManager(linearLayoutManager);
//        rvGroupRequestedList.setNestedScrollingEnabled(true);
//        requestMemberListRecyclerViewAdapter = new MemberListRecyclerViewAdapter(rvGroupRequestedList, this, member_requested);
//        requestMemberListRecyclerViewAdapter.isRequest = true;
//        requestMemberListRecyclerViewAdapter.setImageBaseUrl(bundle.getString(ResponseParameters.IMAGE_BASE_URL));
//        rvGroupRequestedList.addItemDecoration(
//                new DividerItemDecoration(this, R.drawable.divider));
//        rvGroupRequestedList.setAdapter(requestMemberListRecyclerViewAdapter);
//        if (member_requested.size() > 0) {
//            llRequestedRequests.setVisibility(View.VISIBLE);
//        } else {
//            llRequestedRequests.setVisibility(View.GONE);
//        }
//
//
//        LinearLayoutManager linearLayoutManagerInvited = new LinearLayoutManager(this);
//        linearLayoutManagerInvited.setOrientation(LinearLayoutManager.VERTICAL);
//        rvGroupInvitedList.setLayoutManager(linearLayoutManagerInvited);
//        rvGroupInvitedList.setNestedScrollingEnabled(true);
//        invitedMemberListRecyclerViewAdapter = new MemberListRecyclerViewAdapter(rvGroupInvitedList, this, member_invited);
//        invitedMemberListRecyclerViewAdapter.isInvited = true;
//        invitedMemberListRecyclerViewAdapter.setImageBaseUrl(bundle.getString(ResponseParameters.IMAGE_BASE_URL));
//        rvGroupInvitedList.addItemDecoration(
//                new DividerItemDecoration(this, R.drawable.divider));
//        rvGroupInvitedList.setAdapter(invitedMemberListRecyclerViewAdapter);
//
//        if (member_invited.size() > 0) {
//            llInviteRequests.setVisibility(View.VISIBLE);
//        } else {
//            llInviteRequests.setVisibility(View.GONE);
//        }
//
//
//        LinearLayoutManager linearLayoutManagerBlocked = new LinearLayoutManager(this);
//        linearLayoutManagerBlocked.setOrientation(LinearLayoutManager.VERTICAL);
////        rvGroupBlockededList.setLayoutManager(linearLayoutManagerBlocked);
////        rvGroupBlockededList.setNestedScrollingEnabled(true);
////        blockedMemberListRecyclerViewAdapter = new MemberListRecyclerViewAdapter(rvGroupBlockededList, this, member_blocked);
////        blockedMemberListRecyclerViewAdapter.setImageBaseUrl(bundle.getString(ResponseParameters.IMAGE_BASE_URL));
////        blockedMemberListRecyclerViewAdapter.isBlocked = true;
////        rvGroupBlockededList.addItemDecoration(
//                new DividerItemDecoration(this, R.drawable.divider));
////        rvGroupBlockededList.setAdapter(blockedMemberListRecyclerViewAdapter);
//        if (member_blocked.size() > 0) {
////            llBlockRequests.setVisibility(View.VISIBLE);
//        } else {
////            llBlockRequests.setVisibility(View.GONE);
//        }
//    }


    private void setClickListener() {
        lladdMembers.setOnClickListener(this);
        fabChangeImage.setOnClickListener(this);
    }

    public void hideBlockLayout() {
        try{
//            member_blocked.clear();
//            blockedMemberListRecyclerViewAdapter.notifyDataSetChanged();
        }catch(Exception ex){
//            AlertDialog alertDialog = new AlertDialog.Builder(GroupInfoActivity.this).create();
//            alertDialog.setTitle("Request Accept");
//            alertDialog.setMessage("Something went Wrong");
//            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                        }
//                    });
//            alertDialog.show();
            ex.printStackTrace();

        }

//        llBlockRequests.setVisibility(View.GONE);
    }

    public void hideRequestLayout() {
        member_requested.clear();

//        if(requestMemberListRecyclerViewAdapter != null)
//        requestMemberListRecyclerViewAdapter.notifyDataSetChanged();
//        llRequestedRequests.setVisibility(View.GONE);
    }

    public void hideInviteLayout() {
        member_invited.clear();
//        invitedMemberListRecyclerViewAdapter.notifyDataSetChanged();
//        llInviteRequests.setVisibility(View.GONE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_edit, menu);
        if (group.role.trim().equalsIgnoreCase("0")) {
            editGroup = menu.findItem(R.id.action_edit);
            editGroup.setVisible(true);
        }/*else if(group.isMember.equalsIgnoreCase("0")){
            joinGroup = menu.findItem(R.id.action_join);
            joinGroup.setVisible(true);
        }*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_edit) {
            //ChangeNamePopup();

            Intent intent = new Intent(this, CreateGroupActivity.class);
            intent.putExtra(ResponseParameters.GROUP, group);
            intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, bundle.getString(ResponseParameters.GROUP_IMG_BASE_URL));
            startActivity(intent);


            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void ChangeSummeryPopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.change_group_name, null);
        dialogBuilder.setView(dialogView);
        final EditText etTitle = dialogView.findViewById(R.id.etTitle);
        etTitle.setText(group.groupSummary);
        etTitle.setSelection(etTitle.getText().toString().trim().length());
        final TextInputLayout etTitleLayout = dialogView.findViewById(R.id.etTitleLayout);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.Pop_title);
        tvTitle.setText(getResources().getString(R.string.create_group_edittext_group_about_hint));
        etTitle.setHint(getResources().getString(R.string.create_group_edittext_group_about_hint));
        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                etTitleLayout.setErrorEnabled(false);
                if (etTitle.getText().toString().trim().isEmpty()) {
                    etTitleLayout.setErrorEnabled(true);
                    etTitleLayout.setError(GroupInfoActivity.this.getResources().getString(R.string.select_group_summary_error_text));
                } else if (Utility.isConnectingToInternet(GroupInfoActivity.this)) {
                    HashMap<String, Object> input = new HashMap();
                    input.put(RequestParameters.GROUPID, "" + group.groupId);
                    input.put(RequestParameters.GROUP_SUMMARY, "" + etTitle.getText().toString().trim());
                    input.put(RequestParameters.PRIVACY, group.privacy);
                    Log.e("input", "" + input);
                    serviceCall(input);
                    tvGroupSummary.setText(etTitle.getText().toString().trim());
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                    changeGroupTitle.dismiss();
                } else {
                    Snackbar.make(etTitleLayout, GroupInfoActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                }


            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                changeGroupTitle.dismiss();
            }
        });


        this.changeGroupTitle = dialogBuilder.create();
        this.changeGroupTitle.setCancelable(true);
        this.changeGroupTitle.show();
    }

    private void serviceCall(HashMap<String, Object> input) {
        input.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(this));
        progressBar = new com.codeholic.kotumb.app.View.ProgressBar(GroupInfoActivity.this);
        progressBar.show(getResources().getString(R.string.app_please_wait_text));
        API.sendRequestToServerPOST_PARAM(GroupInfoActivity.this, API.EDIT_GROUP, input);// service call for share resume
    }


    //override method of picking image
    @Override
    public void onPickResult(PickResult pickResult) {
        Log.e("TAG", "Result ");
        if (pickResult.getError() == null) {
            //If you want the Uri.
            //Mandatory to refresh image from Uri.
            //getImageView().setImageURI(null);

            //Setting the real returned image.
            //getImageView().setImageURI(r.getUri());

            //If you want the Bitmap.

            //imvUserPic.setImageBitmap(pickResult.getBitmap());
            beginCrop(pickResult.getUri());
            //r.getPath();
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, pickResult.getError().getMessage(), Toast.LENGTH_LONG).show();
            Utils.showPopup(GroupInfoActivity.this, pickResult.getError().getMessage());
        }
    }

    //for cropping image
    private void beginCrop(Uri source) {
        try {
            Uri destination = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/kotumb_group.jpeg"));
            Crop.of(source, destination).asSquare().start(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // initToolbar method initializing Toolbar
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        // setting title
        //header.setText(getResources().getString(R.string.header_main_create_group_title));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    //onActivityResult
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CROP) {// for request crop of image
            Uri destination = Crop.getOutput(data);

            try {
                Bitmap bitmap1 = MediaStore.Images.Media.getBitmap(context.getContentResolver(), destination);
                bitmap1 = ConvertBitmap.Mytransform(bitmap1);
                bitmap1 = RotateImageCode.rotateImage(bitmap1, new File(Environment.getExternalStorageDirectory() + "/appy_celebration_temp.jpeg"));
                ByteArrayOutputStream datasecond = new ByteArrayOutputStream();
                bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, datasecond);
                byte[] bitmapdata = datasecond.toByteArray();

                File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temp2.jpeg");
                if (f.exists())
                    f.delete();
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bitmapdata);
                fo.close();
                String PicturePath = Environment.getExternalStorageDirectory() + File.separator + "temp2.jpeg";

                bitmap1 = BitmapFactory.decodeFile(PicturePath);
                //picByteArray = bitmapdata;


                if (bitmap1 != null) {
                    try {

                        logo = "data:image/png;base64," + Base64.encodeToString(bitmapdata, Base64.NO_WRAP);
                        if (Utility.isConnectingToInternet(GroupInfoActivity.this)) {
                            HashMap<String, Object> input = new HashMap();
                            input.put(RequestParameters.GROUPID, "" + group.groupId);
                            input.put(RequestParameters.LOGO, "" + logo);
                            input.put(RequestParameters.PRIVACY, group.privacy);
                            Log.e("input", "" + input);
                            serviceCall(input);

                            ivGroupImage.setImageBitmap(bitmap1);
                            ivGroupImage.setVisibility(View.VISIBLE);
                        } else {
                            Snackbar.make(ivGroupImage, GroupInfoActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                        }


                        /*HashMap<String, Object> input = new HashMap<>();

                        input.put(RequestParameters.AVATAR,  "data:image/png;base64,"+ Base64.encodeToString(bitmapdata, Base64.NO_WRAP));
                        input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(context));
                        Log.e("input", "" + input);
                        API.sendRequestToServerPOST_PARAM(context, API.UPDATE_PROFILE_PIC, input);
                        pbImageLoading.setVisibility(View.VISIBLE);
                        cvEdit.setVisibility(View.GONE);*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lladdMembers:
                Intent intent = new Intent(this, GroupInviteActivity.class);
                intent.putExtra(ResponseParameters.GROUP, group);
                startActivity(intent);

                break;

            case R.id.llGroupAbout:
                ChangeSummeryPopup();
                break;

            case R.id.llJoinGroup:
                if (group.status.equalsIgnoreCase("0")
                        || group.status.equalsIgnoreCase("")) {
                    joinGroup();
                }
                break;

            case R.id.leave_group_btn:
                leaveGroup();
                break;


            case R.id.fabChangeImage:
                GelleryOrCameraPopup();
                break;
        }
    }

    private void joinGroup() {
        HashMap<String, Object> input = new HashMap();
        input.put(RequestParameters.GROUPID, "" + group.groupId);
        input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(this));
        input.put(RequestParameters.ROLE, "1");
        API.sendRequestToServerPOST_PARAM(this, API.GROUP_JOIN_REQUEST, input);// service call for getting connection
        Utils.logEventGroupJoinRequest(this, group.groupId);
        Utility.showLoading(this, "Please wait");
    }

    private void leaveGroup() {
        HashMap<String, Object> input = new HashMap();
        input.put(RequestParameters.GROUPID, "" + group.groupId);
        input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(this));
        input.put(RequestParameters.ROLE, "0");
        String url = API.GROUP_DELETE_MEMBER + group.groupId + "/" + SharedPreferencesMethod.getUserId(this);
        API.sendRequestToServerGET(this, url, API.GROUP_DELETE_MEMBER);// service call for getting connection
        Utility.showLoading(this, "Please wait");
    }

    /*private void createGroup() {
        try {
            if (!Utility.isConnectingToInternet(context)) { // check net connection
                final Snackbar snackbar = Snackbar.make(lladdMembers, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            HashMap<String, Object> input = new HashMap();
            *//*input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(GroupInfoActivity.this));
            input.put(RequestParameters.GROUP_NAME, "" + etGroupName.getText().toString().trim());
            input.put(RequestParameters.GROUP_SUMMARY, "" + etGroupSum.getText().toString().trim());
            input.put(RequestParameters.LOGO, "" + logo);
            input.put(RequestParameters.CATEGORY, "" + group_categories.getJSONObject(spGroupCategory.getSelectedItemPosition()).getString(ResponseParameters.Id));
            input.put(RequestParameters.PRIVACY, "" + spGroupType.getSelectedItemPosition());
            input.put(RequestParameters.ADMINS, "");
            input.put(RequestParameters.MEMBERS, "");*//*

            Log.e("input", "" + input);
            progressBar = new com.codeholic.kotumb.app.View.ProgressBar(GroupInfoActivity.this);
            progressBar.show(getResources().getString(R.string.app_please_wait_text));
            API.sendRequestToServerPOST_PARAM(GroupInfoActivity.this, API.CREATE_GROUP, input);// service call for share resume
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    //galleryOrCameraPopup for selecting image
    public void GelleryOrCameraPopup() {
        PickSetup pickSetup = new PickSetup().setTitle(getResources().getString(R.string.select_option_message_for_selecting_group_image));
        pickSetup.setTitleColor(getResources().getColor(R.color.textColor));
        pickSetup.setProgressText(getResources().getString(R.string.app_please_wait_text));
        pickSetup.setProgressTextColor(getResources().getColor(R.color.textColor));
        pickSetup.setCancelText(getResources().getString(R.string.cancel_btn_text));
        pickSetup.setCancelTextColor(getResources().getColor(R.color.colorPrimary));
        pickSetup.setButtonTextColor(getResources().getColor(R.color.textColor));
        pickSetup.setDimAmount(0.3f);
        pickSetup.setFlip(true);
        pickSetup.setMaxSize(500);
        pickSetup.setPickTypes(EPickType.GALLERY, EPickType.CAMERA);
        pickSetup.setCameraButtonText(getResources().getString(R.string.app_camera_btn_text));
        pickSetup.setGalleryButtonText(getResources().getString(R.string.app_gallery_btn_text));
        pickSetup.setButtonOrientationInt(LinearLayoutCompat.HORIZONTAL);
        pickSetup.setSystemDialog(false);
        pickSetup.setGalleryIcon(R.mipmap.gallery_colored);
        pickSetup.setCameraIcon(R.mipmap.camera_colored);
        pickSetup.setIconGravityInt(Gravity.LEFT);

        if (Build.VERSION.SDK_INT >= PermissionsUtils.SDK_INT_MARSHMALLOW) { // for permission
            if (PermissionsUtils.getInstance(context).requiredPermissionsGranted(context)) {
                PickImageDialog.build(pickSetup)
                        .setOnPickResult(this).show(this);
            }else{
                PickImageDialog.build(pickSetup)
                        .setOnPickResult(this).show(this);
            }
        } else {
            PickImageDialog.build(pickSetup)
                    .setOnPickResult(this).show(this);
            //new MyDialog(context, MyDialog.UPDATE_PROFILE, null).show();
        }
    }

    // onRequestPermissionsResult for geeting permission from user
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermissionsUtils.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                if (PermissionsUtils.getInstance(context).areAllPermissionsGranted(grantResults)) {
                    GelleryOrCameraPopup();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /*private void showPopUp(String title, boolean showYes, boolean showCancel) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = (LinearLayout) dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = (LinearLayout) dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = (TextView) dialogView.findViewById(R.id.title);
        final View view = dialogView.findViewById(R.id.space);
        tvTitle.setText(title);
        if (!showYes) {
            view.setVisibility(View.GONE);
            llYes.setVisibility(View.GONE);
        }

        if (!showCancel) {
            view.setVisibility(View.GONE);
            llCancel.setVisibility(View.GONE);
        }

        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileAvailibilityPopup.dismiss();
            }
        });

        mobileAvailibilityPopup = dialogBuilder.create();
        mobileAvailibilityPopup.setCancelable(false);
        mobileAvailibilityPopup.show();

    }


    private void showErrorMessagePopup(final String title, String message) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);


        dialogBuilder.setTitle(title);
        dialogBuilder.setMessage(message);
        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (title.equalsIgnoreCase(ResponseParameters.Success)) {
                    finish();
                }
                GroupInfoActivity.this.titleAlert.dismiss();
            }
        });
        this.titleAlert = dialogBuilder.create();
        this.titleAlert.setCancelable(true);
        this.titleAlert.show();
    }
*/

    public void getResponse(JSONObject outPut, int i) {
        try {
            Utility.hideLoading();
            if (progressBar != null)
                progressBar.dismiss();
            Log.e("output", "" + outPut);
            System.out.println("Group Info Response   "+outPut.toString());
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Success)) {
                    if (i == 0) {
                        Utils.showPopup(GroupInfoActivity.this, getString(R.string.group_update_success_msg));
                    } else if (i == 2) {
                        group.status = "0";//change to requested
                        ignore = true;
                        init();
                        ignore = false;
                    } else if (i == 1) {
                        group.status = "1";//change to requested
                        init();
                    }
                } else if (outPut.has(ResponseParameters.Errors)) {
                    Snackbar.make(lladdMembers, Utils.getErrorMessage(outPut), 0).show();
                } else {
                    Snackbar.make(lladdMembers, GroupInfoActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                }
            } else {
                Snackbar.make(lladdMembers, GroupInfoActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
            }

        } catch (Exception e) {

        }
    }


}
