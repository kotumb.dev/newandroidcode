package com.codeholic.kotumb.app.Activity;

import android.content.Context;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ContactFeedbackActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.etName)
    EditText etName;

    @BindView(R.id.etNameLayout)
    TextInputLayout etNameLayout;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etEmailLayout)
    TextInputLayout etEmailLayout;

    @BindView(R.id.etPhone)
    EditText etPhone;

    @BindView(R.id.etPhoneLayout)
    TextInputLayout etPhoneLayout;

    @BindView(R.id.etSubject)
    EditText etSubject;

    @BindView(R.id.etSubjectLayout)
    TextInputLayout etSubjectLayout;

    @BindView(R.id.etMessage)
    EditText etMessage;

    @BindView(R.id.etMessageLayout)
    TextInputLayout etMessageLayout;

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    @BindView(R.id.cvSave)
    CardView cvSave;

    @BindView(R.id.tvSave)
    TextView tvSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_feedback);
        ButterKnife.bind(this);
        initToolbar();
        setClickListener();
        setTextWatcher(etName, etNameLayout);
        setTextWatcher(etEmail, etEmailLayout);
        setTextWatcher(etPhone, etPhoneLayout);
        setTextWatcher(etSubject, etSubjectLayout);
        setTextWatcher(etMessage, etMessageLayout);
        if (getIntent().hasExtra("FEEDBACK"))
            fillValues();
    }

    //set click listeners on views
    private void setClickListener() {
        cvSave.setOnClickListener(this);
    }

    //textwatcher method for all edittext
    private void setTextWatcher(final EditText editText, final TextInputLayout textInputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    textInputLayout.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        //setting screen title
        if (getIntent().hasExtra("FEEDBACK"))
            header.setText(getResources().getString(R.string.feedback_title));
        else
            header.setText(getResources().getString(R.string.contact_us_title));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void fillValues() {
        User user = SharedPreferencesMethod.getUserInfo(this);
        etEmail.setText(user.getEmail());
        etPhone.setText(user.getMobileNumber());
        etName.setText(user.getFirstName() + " " + user.getLastName());
    }

    //checking for number is valid or not
    public static Boolean isValidLong(String value) {
        try {
            if (value.length() <= 0)
                return false;
            if (value.startsWith("0"))
                return false;
            Long val = Long.valueOf(value);
            if (val != null)
                return true;
            else
                return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    //disabling error of input layout
    void disableError() {
        etNameLayout.setErrorEnabled(false);
        etEmailLayout.setErrorEnabled(false);
        etPhoneLayout.setErrorEnabled(false);
        etSubjectLayout.setErrorEnabled(false);
        etMessageLayout.setErrorEnabled(false);
    }

    //enabling error of input layout
    void enableError(TextInputLayout inputLayout, String error) {
        inputLayout.setErrorEnabled(true);
        inputLayout.setError(error);
    }

    //validation for fields
    private boolean validation() {
        disableError();
        boolean result = true;
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etName.getWindowToken(), 0);
        Pattern phoneCheck = Pattern.compile("[1-9][0-9]{9,10}");
        if (etName.getText().toString().trim().isEmpty()) {
            if (result) {
                etName.requestFocus();
                imm.showSoftInput(etName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etNameLayout, getResources().getString(R.string.name_empty_input));
            result = false;
        }
        if (etEmail.getText().toString().trim().isEmpty()) {
            if (result) {
                etEmail.requestFocus();
                imm.showSoftInput(etEmail, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etEmailLayout, getResources().getString(R.string.email_empty_input));
            result = false;
        } else if (!Utility.isEmailIdValid(etEmail.getText().toString().trim())) {
            if (result) {
                etEmail.requestFocus();
                imm.showSoftInput(etEmail, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etEmailLayout, getResources().getString(R.string.email_incorrect_input_format));
            result = false;
        }


        if (etPhone.getText().toString().trim().isEmpty()) {
            if (result) {
                etPhone.requestFocus();
                imm.showSoftInput(etPhone, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etPhoneLayout, getResources().getString(R.string.mobile_empty_input));
            result = false;
        } else if (etPhone.getText().toString().trim().length() < 10) {
            if (result) {
                etPhone.requestFocus();
                imm.showSoftInput(etPhone, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etPhoneLayout, getResources().getString(R.string.mobile_incorrect_input_minlength));
            result = false;
        } else if (!phoneCheck.matcher(etPhone.getText().toString().trim()).find()) {
            if (result) {
                etPhone.requestFocus();
                imm.showSoftInput(etPhone, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etPhoneLayout, getResources().getString(R.string.mobile_incorrect_input_number));
            result = false;
        }

        if (etSubject.getText().toString().trim().isEmpty()) {
            if (result) {
                etSubject.requestFocus();
                imm.showSoftInput(etSubject, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etSubjectLayout, getResources().getString(R.string.subject_empty_input));
            result = false;
        }
        if (etMessage.getText().toString().trim().isEmpty()) {
            if (result) {
                etMessage.requestFocus();
                imm.showSoftInput(etMessage, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etMessageLayout, getResources().getString(R.string.message_empty_input));
            result = false;
        }
        return result;
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cvSave: // register button click
                if (validation()) { // validation
                    try {
                        if (!Utility.isConnectingToInternet(this)) { // check net connection
                            final Snackbar snackbar = Snackbar.make(v, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        HashMap<String, Object> input = new HashMap<>();
                        input.put(RequestParameters.NAME, "" + etName.getText().toString().trim());
                        input.put(RequestParameters.MOBILE, "" + etPhone.getText().toString().trim());
                        input.put(RequestParameters.EMAIL, "" + etEmail.getText().toString().trim());
                        input.put(RequestParameters.SUBJECT, "" + etSubject.getText().toString().trim());
                        input.put(RequestParameters.MESSAGE, "" + etMessage.getText().toString().trim());
                        tvSave.setVisibility(GONE);
                        pbLoading.setVisibility(VISIBLE);
                        cvSave.setClickable(false);
                        etName.setEnabled(false);
                        etPhone.setEnabled(false);
                        etEmail.setEnabled(false);
                        etSubject.setEnabled(false);
                        etMessage.setEnabled(false);
                        Log.e("input", "" + input);
                        if (getIntent().hasExtra("FEEDBACK"))
                            API.sendRequestToServerPOST_PARAM(this, API.FEEDBACK, input); // service call
                        else
                            API.sendRequestToServerPOST_PARAM(this, API.CONTACT, input); // service call
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    //enable views
    void setEnable() {
        tvSave.setVisibility(VISIBLE);
        pbLoading.setVisibility(GONE);
        cvSave.setClickable(true);
        etName.setEnabled(true);
        etPhone.setEnabled(true);
        etEmail.setEnabled(true);
        etSubject.setEnabled(true);
        etMessage.setEnabled(true);
    }

    //get response from service
    public void getResponse(JSONObject outPut, int type) {
        try {
            setEnable();
            if (type == 0) { // sign up response
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        final Snackbar snackbar = Snackbar.make(cvSave, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else if (outPut.has(ResponseParameters.Success)) {
                        finish();
                    } else {
                        final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                        snackbar.show();
                        setEnable();
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { // error
                    final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                    setEnable();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { //no network
                    final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                    setEnable();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
