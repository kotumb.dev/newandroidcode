package com.codeholic.kotumb.app.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PostComments implements Serializable {

    @SerializedName("comment_id")
    public String comment_id = "";

    @SerializedName("reply_id")
    public String reply_id = "";

    @SerializedName("userCommentedBy")
    public String userCommentedBy = "";

    @SerializedName("d_comment_id")
    public String d_comment_id = "";

    @SerializedName("group_id")
    public String group_id = "";

    @SerializedName("comment")
    public String comment = "";

    @SerializedName("post_id")
    public String post_id = "";

    @SerializedName("id")
    public String id = "";

    @SerializedName("userId")
    public String userId = "";

    @SerializedName("liked")
    public String liked = "";

    @SerializedName("createdAt")
    public String createdAt = "";

    @SerializedName("updatedAt")
    public String updatedAt = "";

    @SerializedName("firstName")
    public String firstName = "";

    @SerializedName("middleName")
    public String middleName = "";

    @SerializedName("lastName")
    public String lastName = "";

    @SerializedName("avatar")
    public String avatar = "";

    @SerializedName("profileHeadline")
    public String profileHeadline = "";

    @SerializedName("role")
    public String role = "";

}
