package com.codeholic.kotumb.app.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codeholic.kotumb.app.Adapter.MemberListRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.GroupMembers;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MembersFragment extends Fragment {


    private View rootView;
    private android.widget.ProgressBar progressBar;
    private TextView empty_text;
    private int status;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private String baseUrl = "";
    private String group_id = "";
    private int pagination = 0;
    private List<GroupMembers> memberObjects = new ArrayList<>();
    private MemberListRecyclerViewAdapter adapter;
    private String role;

    public MembersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_members, container, false);
        findViews();
        Bundle arguments = getArguments();
        if (arguments != null) {
            status = arguments.getInt("status", 0);
            baseUrl = arguments.getString("baseUrl", "");
            group_id = arguments.getString("group_id", "");
            role = arguments.getString("role", "1");
        }
        setupRecycleView();
        fetchMembers();

        return rootView;
    }

    private void fetchMembers() {
        String url = API.GET_MEMBERS;
        String groupId = this.group_id;
        String status = this.status + "";
        url = url + groupId + "/" + status + "/" + pagination;// + "?role=" + role;
        API.sendRequestToServerGET_FRAGMENT(getContext(), this, url, API.GET_MEMBERS);
    }

    private void findViews() {
        progressBar = rootView.findViewById(R.id.progressBar);
        empty_text = rootView.findViewById(R.id.empty_text);
        recyclerView = rootView.findViewById(R.id.recyclerView);
    }

    /**
     * Status-
     * 1 = pending
     * 2 = approved
     * 3 = blocked
     * 4 = invited
     *
     * Role -
     * 0 = Admin
     * 1 = Member
     */

    private void setupRecycleView() {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new MemberListRecyclerViewAdapter(recyclerView, getActivity(), memberObjects);
        recyclerView.setAdapter(adapter);
        adapter.setImageBaseUrl(baseUrl);
        adapter.isMember = status==2;
        adapter.isRequest = status==1;
        adapter.isInvited = status==4;
        adapter.isBlocked = status==3;
        adapter.myRole = role;
        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                memberObjects.add(null);
                pagination += 10;
                fetchMembers();
            }
        });
    }

    public void getResponse(JSONObject outPut, int i) {
        if (i == 0) {
            adapter.isLoading =false;
            progressBar.setVisibility(GONE);
            android.util.Log.e("output", outPut.toString());
            try {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        empty_text.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) {
                        handleSuccessResponse(outPut);
                    } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                        empty_text.setVisibility(View.VISIBLE);
                    } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                        empty_text.setVisibility(View.VISIBLE);
                        empty_text.setText(R.string.app_no_internet_error);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleSuccessResponse(JSONObject outPut) {
        try {
            //memberObjects.removeAll(Collections.singletonList(null));

            JSONArray jsonArray = outPut.getJSONArray("group_members");
            System.out.println("Group Members   "+jsonArray.toString());
            ArrayList<GroupMembers> groupMemberObjects = Utility.getGroupMemberObjects(jsonArray);
            if (groupMemberObjects.size() == 0) {
                adapter.isLoading = true;
            }

            if(pagination == 0) {

                //memberObjects = new ArrayList<>(groupMemberObjects);

           /* memberObjects.addAll(groupMemberObjects);
            adapter.notifyDataSetChanged();*/

                memberObjects.clear();
                memberObjects.addAll(groupMemberObjects);
            /*adapter = new MemberListRecyclerViewAdapter(recyclerView, getActivity(), memberObjects);
            recyclerView.setAdapter(adapter);*/

                setupRecycleView();

                if (memberObjects.size() == 0) {
                    empty_text.setVisibility(View.VISIBLE);
                } else {
                    empty_text.setVisibility(View.GONE);
                }
            } else {
                memberObjects.remove(memberObjects.size() - 1);
                adapter.notifyItemRemoved(memberObjects.size());
                adapter.notifyItemRangeChanged(0, memberObjects.size() - 1);

                memberObjects.addAll(groupMemberObjects);

//                adapter.notifyDataSetChanged();
                //adapter.setLoaded();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void refresh() {
        pagination=0;
        memberObjects.clear();
        fetchMembers();
    }


}
