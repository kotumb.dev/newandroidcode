package com.codeholic.kotumb.app.Activity;

import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Adapter.NotificationRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Notification;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;

public class NotificationActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    List<Notification> notifications;
    NotificationRecyclerViewAdapter notificationRecyclerViewAdapter;
    int pagination = 0;
    private LinearLayoutManager linearLayoutManager;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        initToolbar();
        setView();
    }

    //init toolbar function
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.home_notification_title));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //setview for getting user notification
    private void setView() {
        try {
            if (!Utility.isConnectingToInternet(this)) { // checking net connection
                llNotFound.setVisibility(View.VISIBLE);
                final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }

            String userId = SharedPreferencesMethod.getUserId(this);

            API.sendRequestToServerGET(this, API.NOTIFICATION + userId, API.NOTIFICATION); // service call for user list
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //getResponse for getting list of notification
    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        Log.e("response", "" + outPut);
        try {
            if (i == 0) { // for getting urlImages
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        JSONArray notificationList = new JSONArray();
                        if (outPut.has(ResponseParameters.NOTIFICATIONS)) {
                            notificationList = outPut.getJSONArray(ResponseParameters.NOTIFICATIONS);

                        }
                        notifications = new ArrayList<>();
                        for (int j = 0; j < notificationList.length(); j++) {
                            JSONObject results = notificationList.getJSONObject(j);
                            Notification notification = new Notification();
                            User user = new User();
                            user.setFirstName(results.getString(ResponseParameters.FirstName));
                            user.setMiddleName(results.getString(ResponseParameters.MiddleName));
                            user.setLastName(results.getString(ResponseParameters.LastName));
                            user.setAvatar(results.getString(ResponseParameters.Avatar));
                            user.setUserId(results.getString(ResponseParameters.UserId));
                            user.setProfileHeadline(results.getString(ResponseParameters.PROFILE_HEADLINE));
                            user.setProfileSummary(results.getString(ResponseParameters.PROFILE_SUMMERY));
                            //user.setAadhaarInfo(results.getString(ResponseParameters.AADHARINFO));
                            user.setCity(results.getString(ResponseParameters.City));
                            user.setState(results.getString(ResponseParameters.State));
                            user.setUserInfo(results.toString());
                            notification.setUser(user);
                            notification.setStatus(results.getString(ResponseParameters.STATUS));
                            notification.setNotificationId(results.getString(ResponseParameters.Id));
                            notification.setComment(results.getString(ResponseParameters.COMMENT));
                            notification.setNotificationType(results.getString(ResponseParameters.ACTIVITY_TYPE));
                            notification.setMedia(results.getString(ResponseParameters.MEDIA));
                            notification.setMediaType(results.getString(ResponseParameters.MEDIA_TYPE));
                            notification.setNotifyUserFirstName(results.getString(ResponseParameters.FirstName));
                            notification.setNotifyUserLasrName(results.getString(ResponseParameters.LastName));
//                            notification.setUserFrom(results.getString(ResponseParameters.USER_FROM));
                            notifications.add(notification);
                            System.out.println("Get Media  "+notification.getNotificationType());
//                            Toast.makeText(this, notification.getNotificationType(), Toast.LENGTH_SHORT).show();
                        }

                        //setting options for recycler adapter
                        linearLayoutManager = new LinearLayoutManager(this);
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserList.setLayoutManager(linearLayoutManager);
                        notificationRecyclerViewAdapter = new NotificationRecyclerViewAdapter(rvUserList, this,null, notifications);
                        rvUserList.setAdapter(notificationRecyclerViewAdapter);
                        //load more setup for recycler adapter
                        notificationRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                if (notifications.size() >= 6) {
                                    /*if (!getIntent().hasExtra("SUGGESTION")) {
                                        rvGroupList.post(new Runnable() {
                                            public void run() {
                                                urlImages.add(null);
                                                notificationRecyclerViewAdapter.notifyItemInserted(urlImages.size() - 1);
                                            }
                                        });
                                        API.sendRequestToServerGET(ConnectionsActivity.this, API.CONNECTECTIONS + user.getLinkName() + "/" + SharedPreferencesMethod.getLinkName(ConnectionsActivity.this) + "/?offset=" + (pagination + 6), API.CONNECTECTIONS_UPDATE);
                                    }*/
                                }
                            }
                        });

                    } else {
                        //no userfound
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } else if (i == 1) {// for updating list
                notifications.remove(notifications.size() - 1); // removing of loading item
                notificationRecyclerViewAdapter.notifyItemRemoved(notifications.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            for (int j = 0; j < outPut.getJSONArray(ResponseParameters.CONNECTIONS).length(); j++) {
                                JSONObject searchResults = outPut.getJSONArray(ResponseParameters.CONNECTIONS).getJSONObject(j);
                                Notification notification = new Notification();
                                User user = new User();
                                user.setFirstName(searchResults.getString(ResponseParameters.FirstName));    user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                                user.setLastName(searchResults.getString(ResponseParameters.LastName));
                                user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                                user.setUserId(searchResults.getString(ResponseParameters.UserId));
                                user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                                user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                                user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                                user.setCity(searchResults.getString(ResponseParameters.City));
                                user.setState(searchResults.getString(ResponseParameters.State));
                                user.setUserInfo(searchResults.toString());
                                notification.setUser(user);
                                notification.setStatus(searchResults.getString(ResponseParameters.STATUS));
                                notification.setNotificationId(searchResults.getString(ResponseParameters.Id));
                                notification.setComment(searchResults.getString(ResponseParameters.COMMENT));
                                notification.setNotificationType(searchResults.getString(ResponseParameters.ACTIVITY_TYPE));
                                notification.setMedia(searchResults.getString(ResponseParameters.MEDIA));
                                notification.setMediaType(searchResults.getString(ResponseParameters.MEDIA_TYPE));
                                notification.setNotifyUserFirstName(searchResults.getString(ResponseParameters.FirstName));
                                notification.setNotifyUserLasrName(searchResults.getString(ResponseParameters.LastName));
//                                notification.setUserFrom(searchResults.getString(ResponseParameters.USER_FROM));
                                notifications.add(notification);
                            }
                            pagination = pagination + 6;
                            //updating recycler view
                            notificationRecyclerViewAdapter.notifyDataSetChanged();
                            notificationRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            }

        } catch (Exception e) {
            //mo user found
            llNotFound.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }

}
