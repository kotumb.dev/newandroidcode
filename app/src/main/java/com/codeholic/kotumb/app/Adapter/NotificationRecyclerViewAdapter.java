package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

//import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.CompleteProfileActivity;
import com.codeholic.kotumb.app.Activity.ContactActivity;
import com.codeholic.kotumb.app.Activity.GroupPostActivity;
import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Activity.OtherUserProfileActivity;
import com.codeholic.kotumb.app.Activity.PostDetailsActivity;
import com.codeholic.kotumb.app.Activity.ShowVideos;
import com.codeholic.kotumb.app.Activity.VideoDetailsActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Interface.OnResponseListener;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.Notification;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.Model.VideoData;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.fragments.NotificationFragment;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Future;

import static android.text.Spannable.SPAN_EXCLUSIVE_EXCLUSIVE;
import static com.codeholic.kotumb.app.Utility.Utils.showPopup;


public class NotificationRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final Fragment fragment;
    private List<Notification> notifications;
    Activity activity;
    Integer pageNumber = 0;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private final int VIEW_TYPE_ADS = 2;
    private boolean isLoading;
    private boolean flag;
    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;
    private AlertDialog confirmAlert;
    private OnResponseListener refreshListener;
    Future<File> downloading;
    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;
    private ArrayList<Integer> loadedIds = new ArrayList<>();


    public NotificationRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, Fragment fragment, List<Notification> notifications) {
        this.notifications = notifications;
        this.activity = activity;
        this.fragment = fragment;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        int i = notifications.get(position) == null ? VIEW_TYPE_LOADING : (notifications.get(position).getId().isEmpty()) ? VIEW_TYPE_ADS : VIEW_TYPE_ITEM;
        return i;
    }


    @Override
    public int getItemCount() {
        return notifications.size();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.notification_list_item, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        } else if (viewType == VIEW_TYPE_ADS) {
            View view = LayoutInflater.from(activity).inflate(R.layout.ad_layout_new, parent, false);
            return new AdsViewHolder(view);
        }
        flag=true;
        return null;
    }

    public void setRefreshListener(OnResponseListener refreshListener) {
        this.refreshListener = refreshListener;
    }



    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    public class AdsViewHolder extends RecyclerView.ViewHolder {
        public ImageView adView;
        public RelativeLayout rlAdView;
        public int id;

        AdsViewHolder(View view) {
            super(view);
            adView = view.findViewById(R.id.adView);
            rlAdView = view.findViewById(R.id.rlAdView);
            id = new Random().nextInt(10000);
        }
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewholder, int position) {
        boolean alreadySet = false;
        if (viewholder instanceof UserViewHolder) {
            final Notification notification = notifications.get(position);
            System.out.println("Notification Type   "+ notification.getMedia() +  "    Media   "+notification.getMedia());
            final User user = notification.getUser();
            UserViewHolder holder = (UserViewHolder) viewholder;
            System.out.println("User DOB "+ user.getDOB());
            holder.tvUserName.setText("" + user.getFirstName() + " " + user.getLastName());
            SpannableString spannableName = new SpannableString("" + user.getFirstName().trim() + " " + user.getLastName().trim() + " ");
            spannableName.setSpan(new ForegroundColorSpan(Color.parseColor("#30445D")),
                    0, spannableName.length(), SPAN_EXCLUSIVE_EXCLUSIVE);
            SpannableString spannableMessage = new SpannableString("");
            final String notificationType = notification.getNotificationType();
            Resources resources = activity.getResources();
            if (notificationType.equalsIgnoreCase(ResponseParameters.REQUESTED)) {
                spannableMessage = new SpannableString(resources.getString(R.string.notification_sent_request));
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.REQUESTACCEPTED)) {
                spannableMessage = new SpannableString(resources.getString(R.string.notification_accept_request));
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.PROFILEVIEWED)) {
                spannableMessage = new SpannableString(resources.getString(R.string.notification_viewed_profile));
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.ASKED_RECOM)) {
                spannableMessage = new SpannableString(resources.getString(R.string.notification_ask_recommendation));
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_INVITATION)) {
                spannableMessage = new SpannableString(resources.getString(R.string.notification_ask_join_group));
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_REQUESTED)) {
                spannableMessage = new SpannableString(resources.getString(R.string.notification_request_join_group));
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_REQUEST_ACCEPTED)) {
                spannableMessage = new SpannableString(resources.getString(R.string.notification_request_accepted_join_group));
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_DELETE_ADMIN)) {
                Group group = notification.getGroup();
                holder.tvUserName.setText("" + group.groupName);
                spannableName = new SpannableString(group.groupName);
                spannableMessage = new SpannableString(" " + resources.getString(R.string.notification_group_delete));
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.MAKE_GROUP_ADMIN)) {
                Group group = notification.getGroup();
                spannableName = new SpannableString(group.groupName);

                holder.tvUserName.setText("" + group.groupName);
                spannableMessage = new SpannableString(" " + activity.getString(R.string.added_as_admin_msg));
                setOnClickListener(holder, group, notification.getGroupImageURL(), notification, 0);
                alreadySet = true;
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.MAKE_GROUP_MEMBER)) {
                Group group = notification.getGroup();
                spannableName = new SpannableString(group.groupName);
                holder.tvUserName.setText("" + group.groupName);
                spannableMessage = new SpannableString(" " + activity.getString(R.string.remove_as_admin_msg));
                setOnClickListener(holder, group, notification.getGroupImageURL(), notification, 0);
                alreadySet = true;
            }
            spannableMessage.setSpan(new RelativeSizeSpan(.7f),
                    0, spannableMessage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_INVITATION)
                    || notificationType.equalsIgnoreCase(ResponseParameters.GROUP_REQUESTED)
                    || notificationType.equalsIgnoreCase(ResponseParameters.GROUP_REQUEST_ACCEPTED)) {
                SpannableString spannableGroupName = new SpannableString("");

                if (!notification.getNotificationDes().trim().isEmpty()) {
                    String[] splitArray = notification.getNotificationDes().trim().split("\\|");
                    if (splitArray.length >= 2) {
                        spannableGroupName = new SpannableString(" " + splitArray[1]);
                        spannableGroupName.setSpan(new ForegroundColorSpan(Color.parseColor("#30445D")),
                                0, spannableGroupName.length(), SPAN_EXCLUSIVE_EXCLUSIVE);
                        spannableGroupName.setSpan(new StyleSpan(Typeface.BOLD), 0, spannableGroupName.length(), SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                }

                holder.tvNotification.setText(TextUtils.concat(spannableName, spannableMessage, spannableGroupName));
            } else if (notificationType.equalsIgnoreCase(
                    ResponseParameters.GROUP_POST_COMMENT_NOTIFICATION) ||
                    notificationType.equalsIgnoreCase(
                            ResponseParameters.GROUP_POST_COMMENT_OTHERS_NOTIFICATION)) {
                String source;
                if (notificationType.equalsIgnoreCase(
                        ResponseParameters.GROUP_POST_COMMENT_NOTIFICATION)) {
                    source = activity.getString(R.string.commented_on_your_post);
                } else {
                    source = " also commented on a post you engaged with.";
                }
                int count = notification.getCount();
                if (count > 1) {
                    spannableName = new SpannableString(spannableName + activity.getString(R.string.bell_noti_and) + " " + (count - 1) + " " + activity.getString(R.string.bell_noti_other) + " ");
                }
                spannableMessage = new SpannableString(source);
                spannableMessage.setSpan(new RelativeSizeSpan(.7f),
                        0, spannableMessage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tvNotification.setText(TextUtils.concat(spannableName, spannableMessage));
                Post post = notification.getPost();
                setOnClickListener(holder, post, notification.getGroup(), notification.getGroupImageURL(), true, notification,user);
                alreadySet = true;
            } else if (notificationType.equalsIgnoreCase(
                    ResponseParameters.GROUP_POST_LIKE_NOTIFICATION)) {
                Post post = notification.getPost();
                String source = notification.getMessage();
                holder.tvNotification.setText(source);
                setOnClickListener(holder, post, notification.getGroup(), notification.getGroupImageURL(), false, notification,user);
                alreadySet = true;
            } else if (notificationType.equalsIgnoreCase(
                    ResponseParameters.GROUP_POST_SHARE_NOTIFICATION)) {
                String source = activity.getString(R.string.bell_noti_shared_ur_post);
                int count = notification.getCount();
                if (count > 1) {
                    String and = activity.getString(R.string.bell_noti_and);
                    String other = activity.getString(R.string.bell_noti_other);
                    spannableName = new SpannableString(spannableName + and + " " + (count - 1) + " " + other + " ");
                }
                spannableMessage = new SpannableString(source);
                spannableMessage.setSpan(new RelativeSizeSpan(.7f),
                        0, spannableMessage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tvNotification.setText(TextUtils.concat(spannableName, spannableMessage));
                Post post = notification.getPost();
                setOnClickListener(holder, post, notification.getGroup(), notification.getGroupImageURL(), false, notification,user);
                alreadySet = true;
            } else if (notificationType.equalsIgnoreCase(
                    ResponseParameters.GROUP_POST_ADD) || notificationType.equalsIgnoreCase(
                    ResponseParameters.GROUP_POST_SHARED)) {
                String source;
                String comment = notification.getComment();
                if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_POST_ADD)) {
                    source = activity.getString(R.string.bell_noti_posted_in) + " " + comment + " " + activity.getString(R.string.bell_noti_group);
                } else {
                    String s = activity.getString(R.string.bell_noti_shared_post_in);
                    source = s + " " + comment + " " + activity.getString(R.string.bell_noti_group);
                }
                spannableMessage = new SpannableString(source);
                spannableMessage.setSpan(new RelativeSizeSpan(.7f),
                        0, spannableMessage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tvNotification.setText(TextUtils.concat(spannableName, spannableMessage));
                Post post = notification.getPost();

                setOnClickListener(holder, post, notification.getGroup(), notification.getGroupImageURL(), false, notification,user);
                alreadySet = true;
            } else if (notificationType.equalsIgnoreCase(
                    ResponseParameters.SUPPORT_CHAT)) {
                String source = activity.getString(R.string.bell_noti_new_support_msg);
                spannableMessage = new SpannableString(source);
                spannableMessage.setSpan(new RelativeSizeSpan(1.0f),
                        0, spannableMessage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                holder.tvNotification.setText(TextUtils.concat(spannableName, spannableMessage));
                holder.tvNotification.setText(spannableMessage);
                Post post = notification.getPost();
                holder.cvContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        activity.startActivity(new Intent(activity, ContactActivity.class));
                        markAsRead(notification);
                    }
                });
//                 setOnClickListener(holder, post, notification.getGroup(),notification.getGroupImageURL(), false);
                alreadySet = true;
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.WELCOME)
                    || notificationType.equalsIgnoreCase(ResponseParameters.WELCOME_MSG)
                    || notificationType.equalsIgnoreCase(ResponseParameters.WHAT_TO_DO)) {
                String des = notification.getNotificationDes();
                spannableName = new SpannableString(des);
                holder.tvUserName.setText(spannableName);
//                String message;
//                String link = null;
//                getMediaType(holder);
//                else if (notificationType.equalsIgnoreCase(ResponseParameters.WELCOME)
//                        || notificationType.equalsIgnoreCase(ResponseParameters.WELCOME_MSG)) {
////                    message = "Welcome to Kotumb";
//                } else {
////                    message = "Things to do in Kotumb";
////                    link = "";
//                }
////                spannableMessage = new SpannableString(message);
                holder.tvNotification.setText(R.string.new_message_kotumb_team);
//                spannableMessage = new SpannableString(message);
//                holder.tvNotification.setText(TextUtils.concat(notification.getNotificationDes()));

                setOnClickListener(holder, notification, "");

                alreadySet = true;
            }
            else if(notificationType.equalsIgnoreCase("videoapproved")){
                spannableMessage = new SpannableString(user.getFirstName()+" "+user.getLastName());
                spannableMessage.setSpan(new RelativeSizeSpan(.7f),
                        0, spannableMessage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                final String message=notification.getMessage();
                holder.tvNotification.setText(message);
                holder.tvTime.setText("" + notification.getNotificationTime());

                holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video_notification));
                System.out.println("inApproved16=========="+message);
                final String media=notification.getMedia();
                holder.cvContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        markAsRead(notification);
                        Intent intent=new Intent(activity, VideoDetailsActivity.class);
                        intent.putExtra("video_uri",media);
                        intent.putExtra("like_status",String.valueOf(notification.getLike()));
                        intent.putExtra("activitytype","videoapproved");
                        intent.putExtra("video_title",notification.getTitle());
                        intent.putExtra("id",notification.getId());
                        intent.putExtra("isPublished",notification.getIsPublished());
                        intent.putExtra("video_id",String.valueOf(notification.getFieldId()));
                        intent.putExtra("video_avatar",notification.getThumbnail());
                        intent.putExtra("video_date_time",notification.getVideo_date_time());
                        // intent.putExtra("videoDescription",notification.getNotificationDes());
                        intent.putExtra("video_desp",notification.getNotificationDes());
                        intent.putExtra("videoStatus",notification.getStatus());
                        intent.putExtra("fname",notification.getNotifyUserFirstName());
                        intent.putExtra("lanme",notification.getNotifyUserLasrName());
                        intent.putExtra("user",user);
                        activity.startActivity(intent);
                    }
                });
                alreadySet = true;

            }    else if(notificationType.equalsIgnoreCase("publishedvideonotification")){
                spannableMessage = new SpannableString(user.getFirstName()+" "+user.getLastName());
                spannableMessage.setSpan(new RelativeSizeSpan(.7f),
                        0, spannableMessage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                final String message=notification.getMessage();
                holder.tvNotification.setText(message);
                holder.tvTime.setText("" + notification.getNotificationTime());
                System.out.println("check=="+notification.getMedia()+"=="+notification.getNotificationDes()+"=="+String.valueOf(notification.getFieldId())+
                        "=="+notification.getNotifyUserFirstName()+"=="+notification.getNotifyUserLasrName());
                holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video_notification));
                System.out.println("inApproved15=========="+holder.ivUserImage.getId());
                final String media=notification.getMedia();
                holder.cvContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        markAsRead(notification);

                        Intent intent=new Intent(activity, VideoDetailsActivity.class);
                        intent.putExtra("video_uri",media);
                        //intent.putExtra("created")
                        intent.putExtra("video_id",String.valueOf(notification.getFieldId()));
                        intent.putExtra("like_count",notification.getLikeCount());
                        intent.putExtra("video_title",notification.getTitle());
                        intent.putExtra("id",notification.getId());
                        intent.putExtra("video_date_time",notification.getVideo_date_time());
                        intent.putExtra("like_status",String.valueOf(notification.getLike()));
                        intent.putExtra("activitytype","publishedvideonotification");
                        intent.putExtra("video_desp",notification.getNotificationDes());
                        intent.putExtra("video_avatar",notification.getThumbnail());
                        //ntent.putExtra("videoId",String.valueOf(notification.getFieldId()));
                        intent.putExtra("videoStatus",notification.getStatus());
                        intent.putExtra("fname",notification.getNotifyUserFirstName());
                        intent.putExtra("lanme",notification.getNotifyUserLasrName());
                        intent.putExtra("user",user);
                        activity.startActivity(intent);
                    }
                });
                alreadySet = true;
            }else if(notificationType.equalsIgnoreCase("videorejected")){
                holder.tvNotification.setText(notification.getMessage());
                holder.tvTime.setText("" + notification.getNotificationTime());
                holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video_notification));
                holder.cvContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        markAsRead(notification);
                        //Toast.makeText(activity,"click",Toast.LENGTH_SHORT).show();
                        Utils.showPopup(activity,notification.getMessage());
                    }
                });
                alreadySet = true;
            }
            else if(notificationType.equalsIgnoreCase("newvideonotification")){
                spannableMessage = new SpannableString(user.getFirstName()+" "+user.getLastName());
                spannableMessage.setSpan(new RelativeSizeSpan(.7f),
                        0, spannableMessage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tvNotification.setText(notification.getMessage());
                holder.tvTime.setText("" + notification.getNotificationTime());
                holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video_notification));
                System.out.println("inApproved14=========="+holder.ivUserImage.getId());
                holder.cvContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        markAsRead(notification);
                        Intent intent=new Intent(activity, ShowVideos.class);
                        intent.putExtra("videoUri",notification.getMedia());
                        intent.putExtra("tab","1");
                        intent.putExtra("activitytype","newvideonotification");
                        intent.putExtra("videoDescription",notification.getNotificationDes());
                        intent.putExtra("videoId",String.valueOf(notification.getFieldId()));
                        intent.putExtra("videoStatus",notification.getStatus());
                        intent.putExtra("fname",notification.getNotifyUserFirstName());
                        intent.putExtra("lanme",notification.getNotifyUserLasrName());
                        intent.putExtra("user",user);
                        activity.startActivity(intent);
                    }
                });
                alreadySet = true;

            }else if(notificationType.equalsIgnoreCase(ResponseParameters.VIDEO_LIKES)){
                final VideoData videoData=notification.getVideoData();
                spannableMessage = new SpannableString(user.getFirstName()+" "+user.getLastName());
                spannableMessage.setSpan(new RelativeSizeSpan(.7f),
                        0, spannableMessage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                String msg=notification.getMessage();
                System.out.println("videolikemsg=="+msg);
                holder.tvNotification.setText(msg);
                holder.tvTime.setText("" + notification.getNotificationTime());
                new AQuery(activity).id(holder.ivUserImage).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.default_profile);
                System.out.println("inApproved13=========="+holder.ivUserImage.getId());
                holder.cvContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        markAsRead(notification);
                        Intent intent=new Intent(activity,VideoDetailsActivity.class);
                        intent.putExtra("video_uri",notification.getMedia());
                        //intent.putExtra("created")
                        intent.putExtra("like_count",notification.getLikeCount());
                        intent.putExtra("video_title",notification.getTitle());
                        intent.putExtra("id",notification.getId());
                        intent.putExtra("video_id",String.valueOf(notification.getFieldId()));
                        intent.putExtra("video_date_time",notification.getVideo_date_time());
                        intent.putExtra("like_status",String.valueOf(notification.getLike()));
                        intent.putExtra("activitytype","publishedvideonotification");
                        intent.putExtra("video_desp",notification.getNotificationDes());
                        intent.putExtra("video_avatar",notification.getThumbnail());
                        //ntent.putExtra("videoId",String.valueOf(notification.getFieldId()));
                        intent.putExtra("videoStatus",notification.getStatus());
                        intent.putExtra("fname",notification.getNotifyUserFirstName());
                        intent.putExtra("lanme",notification.getNotifyUserLasrName());
                        intent.putExtra("user",user);
                        activity.startActivity(intent);
                    }
                });
                alreadySet = true;
            }

//            else if(notification.getNotificationType().equalsIgnoreCase("groupvideoshare")){
//                String s = "Video Share in Group";
//                spannableMessage = new SpannableString(s);
//                spannableMessage.setSpan(new RelativeSizeSpan(0.8f),
//                        s.length(), s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                holder.tvNotification.setText(notification.getGroup().groupName+" "+TextUtils.concat(spannableMessage));
//                holder.tvTime.setText("" + notification.getNotificationTime());
//                holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video_notification));
//                holder.cvContainer.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        markAsRead(notification);
//                        Intent intent=new Intent(activity,VideoDetailsActivity.class);
//                        intent.putExtra("videoUri",notification.getMedia());
//                        intent.putExtra("videoDescription",notification.getNotificationDes());
//                        intent.putExtra("videoId",String.valueOf(notification.getFieldId()));
//                        intent.putExtra("videoStatus",notification.getStatus());
//                        intent.putExtra("fname",notification.getNotifyUserFirstName());
//                        intent.putExtra("lanme",notification.getNotifyUserLasrName());
//                        intent.putExtra("user",user);
//                        activity.startActivity(intent);
//                    }
//                });
//                alreadySet = true;
//            }

            else if (notificationType.equalsIgnoreCase(ResponseParameters.PUBLIC_GROUP)) {
                Group group = notification.getGroup();
                String s = "New Group Added";
                String source = s + "\n" + group.groupName;
                spannableMessage = new SpannableString(source);
                spannableMessage.setSpan(new RelativeSizeSpan(1.0f),
                        s.length(), source.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tvNotification.setText(TextUtils.concat(spannableMessage));
                setOnClickListener(holder, group, notification.getGroupImageURL(), notification, 1);
                alreadySet = true;
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_USER_REPORT_NOTIFICATION)) {
                Group group = notification.getGroup();
                String s = "Group member reported a user in ";
                String source = s + " " + group.groupName;
                spannableMessage = new SpannableString(source);
                spannableMessage.setSpan(new RelativeSizeSpan(0.8f),
                        s.length(), source.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tvNotification.setText(TextUtils.concat(spannableMessage));
                setOnClickListenerForReportedNotifications(user,holder, group, notification.getGroupImageURL(), notification, ResponseParameters.GROUP_USER_REPORT_NOTIFICATION);
                alreadySet = true;
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_POST_REPORT_NOTIFICATION)) {
                Group group = notification.getGroup();
                String s = "Group member reported a post in ";
                String source = s + " " + group.groupName;
                spannableMessage = new SpannableString(source);
                spannableMessage.setSpan(new RelativeSizeSpan(0.8f),
                        s.length(), source.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tvNotification.setText(TextUtils.concat(spannableMessage));
                setOnClickListenerForReportedNotifications(user,holder, group, notification.getGroupImageURL(), notification, ResponseParameters.GROUP_POST_REPORT_NOTIFICATION);
                alreadySet = true;
            } else if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_POST_COMMENT_REPORT_NOTIFICATION)) {
                Group group = notification.getGroup();
                String s = "Group member reported a comment in ";
                String source = s + " " + group.groupName;
                spannableMessage = new SpannableString(source);
                spannableMessage.setSpan(new RelativeSizeSpan(0.8f),
                        s.length(), source.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.tvNotification.setText(TextUtils.concat(spannableMessage));
                setOnClickListenerForReportedNotifications(user,holder, group, notification.getGroupImageURL(), notification, ResponseParameters.GROUP_POST_COMMENT_REPORT_NOTIFICATION);
                alreadySet = true;
            }  else {
                holder.tvNotification.setText(TextUtils.concat(spannableName, spannableMessage));
                setVisbiltyinMedia(holder);
            }



            //Calling Broadcast Method To Set All Media and Broadcast Mesaages
            if(!notificationType.equals(ResponseParameters.VIDEO_APPROVE) && !notificationType.equals(ResponseParameters.VIDEO_LIKES) && !notificationType.equals("newvideonotification") && !notificationType.equals("publishedvideonotification") && !notificationType.equals("videorejected"))
            setMediaItemValidation(alreadySet,user, holder, notificationType, notification.getMediaType(), notification.getMedia(), notification);



            if (!alreadySet) {
                VideoData videoData=notification.getVideoData();
                setOnClickListener(holder, user, position, notification,videoData);
            }


            String notificationStatus = notification.getStatus();
            if (notificationStatus.equalsIgnoreCase("0")) {
                holder.tvNotification.setTypeface(null, Typeface.BOLD);
                holder.bg.setBackgroundColor(Color.parseColor("#eeeeee"));
            } else {
                holder.bg.setBackgroundColor(Color.parseColor("#00fff9c4"));
                holder.tvNotification.setTypeface(null, Typeface.NORMAL);
            }


        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        } else if (viewholder instanceof AdsViewHolder) {
            AdsViewHolder adsViewHolder = (AdsViewHolder) viewholder;
            NotificationFragment fragment = (NotificationFragment) this.fragment;
//            if (!loadedIds.contains(adsViewHolder.id)) {
            fragment.loadAdForThisView(adsViewHolder, position);
            loadedIds.add(adsViewHolder.id);
//            adsViewHolder.setIsRecyclable(false);
//            }else{
//                loadingViewHolder.adView.setImageResource(R.drawable.ic_user);
//            }
        }
    }

    private void setOnClickListener(UserViewHolder holder, final Notification notification, final String imgUrl) {
        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAsRead(notification);
                if (imgUrl == null) {
                    showPopup(activity, notification.getNotificationDes());

                } else {
                    showPopup(activity, notification.getNotificationDes());
//                    Utils.showPopupWithImage(notification.getNotificationDes(), "", activity);
                }
            }
        });
    }

    private void markAsRead(Notification notification) {
        notification.setStatus("1");
        String url = API.MARK_NOTIFICATION_READ + notification.getId();
        System.out.println("Get Notify URL  "+  url);
        API.sendRequestToServerGET(activity, url, "");
        notifyDataSetChanged();
       /* new AQuery(activity).ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                notifyDataSetChanged();
                Toast.makeText(activity, object + "", Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    private void setOnClickListener(UserViewHolder holder, final Post post, final Group group, final String base_url, final boolean openCommentView, final Notification notification,final User user) {
        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAsRead(notification);
                if (post != null) {
                    Intent intent = new Intent(activity, PostDetailsActivity.class);
                    intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, base_url);
                    intent.putExtra(ResponseParameters.GROUP, group);
                    intent.putExtra(ResponseParameters.POST, post);
                    intent.putExtra("USER", user);
                    intent.putExtra("back", true);
                    intent.putExtra(ResponseParameters.OPEN_COMMENT_VIEW, openCommentView);
                    activity.startActivity(intent);
                } else {
                    showPopUp(activity.getString(R.string.post_has_been_deleted));
                }
            }
        });
    }

    public void setOnClickListener(final UserViewHolder holder, final Group group, final String base_url, final Notification notification, final int type_value) {
        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
            @Override
            public void onClick(View v) {
                markAsRead(notification);
                if (!group.groupId.trim().isEmpty()) {
                    Intent intent = new Intent(activity, GroupPostActivity.class);
                    intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, base_url);
                    intent.putExtra(ResponseParameters.GROUP, group);
                    intent.putExtra(ResponseParameters.TYPE, type_value);
                    if (type_value == 1) {
                        intent.putExtra(ResponseParameters.GROUP_INFO, true);
                    }
                    activity.startActivity(intent);
                } else {
                    showPopUp(activity.getString(R.string.tile_noti_group_deleted));
                }
            }
        });
    }

    private void setOnClickListenerForReportedNotifications(final User user,final UserViewHolder holder, final Group group, final String base_url, final Notification notification, final String type_value) {
        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAsRead(notification);
                if (!group.groupId.trim().isEmpty()) {
                    Intent intent = new Intent(activity, GroupPostActivity.class);
                    intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, base_url);
                    intent.putExtra(ResponseParameters.GROUP, group);
                    intent.putExtra(ResponseParameters.TYPE, type_value);
                    intent.putExtra("USER", user);
                    intent.putExtra("report", type_value);
                    activity.startActivity(intent);
                } else {
                    showPopUp(activity.getString(R.string.tile_noti_group_deleted));
                }
            }
        });
    }

    public void setOnClickListener(final UserViewHolder holder, final Post post, final String base_url, final Notification notification) {
        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAsRead(notification);
                Intent intent = new Intent(activity, GroupPostActivity.class);
                intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, base_url);
                intent.putExtra(ResponseParameters.GROUP, post);
                intent.putExtra(ResponseParameters.TYPE, 0);
                activity.startActivity(intent);
            }
        });
    }

    public void setOnClickListener(final UserViewHolder holder, final User user,
                                   final int position, final Notification notification,final VideoData videoData) {
        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAsRead(notification);
                String notificationType = notifications.get(position).getNotificationType();
                System.out.println("Activity Type   "+notification.getMedia());
                if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_INVITATION)) {
                    String[] splitArray = notifications.get(position).getNotificationDes().trim().split("\\|");
                    if (splitArray.length >= 2) {
                        showPopUp(activity.getResources().getString(R.string.group_request_user_dialog_msg), position, splitArray[0]);
                    }
                } else if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_REQUESTED)) {
                    String[] splitArray = notifications.get(position).getNotificationDes().trim().split("\\|");
                    if (splitArray.length >= 2) {
                        showPopUp(activity.getResources().getString(R.string.group_request_accept_dialog_msg), position, splitArray[0]);
                    }
                } else if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_REQUEST_ACCEPTED)) {
                    ((HomeScreenActivity) activity).openHomeFragment();
                } else if (notificationType.equalsIgnoreCase(ResponseParameters.GROUP_DELETE_ADMIN)) {
                    showPopUp(notifications.get(position).getNotificationDes());
                } else {
                    if (!SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(user.getUserId().trim())) {
                        if (!user.getFirstName().equalsIgnoreCase("Super")){
                                Intent intent = new Intent(activity, OtherUserProfileActivity.class);
                                System.out.println("User Info    "+user.getUserInfo());
                                System.out.println("Activity Type   "+notification.getNotificationType());
//                                Toast.makeText(activity, notification.getNotificationType(), Toast.LENGTH_SHORT).show();
                                intent.putExtra("USER", user);
                                activity.startActivity(intent);
                        }else{
                            if (notificationType.equalsIgnoreCase(ResponseParameters.BROADCAST)&&notification.getMedia().equalsIgnoreCase("")){
                                Utils.showPopup(activity,notification.getNotificationDes());
                            }else if(notificationType.equalsIgnoreCase(ResponseParameters.BROADCAST)&&!notification.getMedia().equalsIgnoreCase("")){
                                mediaShowPopUp(notification.getMediaType(),notificationType,notification,holder);
                            }else if(notificationType.equalsIgnoreCase("birthday")&&!notification.getMedia().equalsIgnoreCase("")){
                                mediaShowPopUp(notification.getMediaType(),notificationType,notification,holder);
                            }
//                            else if (notificationType.equalsIgnoreCase("videoapproved")){
//                                Intent intent=new Intent(activity, VideoDetailsActivity.class);
//                                intent.putExtra("videoUri",notification.getMedia());
//                                intent.putExtra("videoDescription",notification.getNotificationDes());
//                                intent.putExtra("videoId",String.valueOf(notification.getFieldId()));
//                                intent.putExtra("videoStatus",notification.getStatus());
//                                intent.putExtra("user",user);
//                                activity.startActivity(intent);
//                            }
                        }
                    } else {
                        Intent intent = new Intent(activity, CompleteProfileActivity.class);
                        intent.putExtra(ResponseParameters.Id, R.id.ll_personal_details);
                        //intent.putExtra("DATA", new JSONObject().toString());
                        activity.startActivity(intent);
                    }
                }
            }
        });
    }



    private void showPopUp(String title, final int position, final String groupId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);
        final TextView tvCancel = dialogView.findViewById(R.id.tvCancel);


        tvTitle.setText(title);
        tvYes.setText(activity.getResources().getString(R.string.request_accept_btn_text));
        tvCancel.setText(activity.getResources().getString(R.string.request_decline_btn_text));


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    confirmAlert.dismiss();
                    return;
                }
                String userId = "";

                if (notifications.get(position).getNotificationType().equalsIgnoreCase(ResponseParameters.GROUP_INVITATION)) {
                    userId = SharedPreferencesMethod.getUserId(activity);
                } else if (notifications.get(position).getNotificationType().equalsIgnoreCase(ResponseParameters.GROUP_REQUESTED)) {
                    userId = notifications.get(position).getUser().getUserId();

                }
                confirmAlert.dismiss();
                String url = API.GROUP_ACCEPT_MEMBER + groupId + "/" + userId + "/" + SharedPreferencesMethod.getUserId(activity);
                API.setResponseListener(new OnResponseListener() {
                    @Override
                    public void onGotResponse(JSONObject response) {
                        if (response.has("errors")) {
                            try {
                                showPopUp(response.getString("errors"));
                            } catch (JSONException e) {
                                showPopUp(activity.getString(R.string.bell_noti_group_limit_exceeded));
                                e.printStackTrace();
                            }
                        } else {
                            System.out.println("Group Accept Member   "+response.toString());
                            notifyItemRemoved(position);
                            notifications.remove(position);
                            notifyItemRangeChanged(0, notifications.size());
                        }
                    }
                });
                System.out.println("Group Accept Member  "+url);
                API.sendRequestToServerGET(activity, url, API.GROUP_ACCEPT_MEMBER);

            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    confirmAlert.dismiss();
                    return;
                }

                String userId = "";

                if (notifications.get(position).getNotificationType().equalsIgnoreCase(ResponseParameters.GROUP_INVITATION)) {
                    userId = SharedPreferencesMethod.getUserId(activity);
                } else if (notifications.get(position).getNotificationType().equalsIgnoreCase(ResponseParameters.GROUP_REQUESTED)) {
                    userId = notifications.get(position).getUser().getUserId();
                }

                String url = API.GROUP_DELETE_MEMBER + groupId + "/" + userId;
                System.out.println("Group Delete Member   "+url);
                API.sendRequestToServerGET(activity, url, API.GROUP_DELETE_MEMBER);
                notifyItemRemoved(position);
                NotificationFragment.notificationToRemove.add(notifications.get(position).getId());
                notifications.remove(position);
                notifyItemRangeChanged(0, notifications.size());
                confirmAlert.dismiss();
                if (NotificationRecyclerViewAdapter.this.refreshListener != null) {
                    //NotificationRecyclerViewAdapter.this.refreshListener.onGotResponse(null);
                }
            }
        });

        confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();

    }

    private void showPopUp(String title) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_normal, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);

        tvTitle.setText(title);
        //tvYes.setText(activity.getResources().getString(R.string.request_accept_btn_text));

        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });

        confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();

    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*
    This Code  Section is Written By Arpit Kanda
    For Showing and Downloading For Media Items
    Starts From Line 848-1428
     */


    //This Method is Created By Arpit Kanda
    //All Media Popups Validations
    public void setMediaItemValidation(boolean alreadySet,final User user,final UserViewHolder holder,String notifyType,String mediaType,String media,final Notification notification){
        User user1 = SharedPreferencesMethod.getUserInfo(activity);
        String DOB=user1.getDOB().substring(5);
        System.out.println("Notifcation Id   "+notification.getNotificationId()  +"    Broadcast Type   "+notification.getNotificationType()  + "  Broadcast Media   "+media);



        //Broadcast YoutubeActivity Thumbnail Set
         if (mediaType.equalsIgnoreCase("videoLink")&& notifyType.equalsIgnoreCase(ResponseParameters.BROADCAST)){
            holder.tvNotification.setText(activity.getResources().getString(R.string.new_message_kotumb_team));
            holder.ivUserImage.setVisibility(View.GONE);
            holder.videoIcon.setVisibility(View.VISIBLE);
            holder.squarLayout.setVisibility(View.VISIBLE);
            final String getMedia=notification.getMedia();
            String videoId=getMedia.substring(17);
            getThumbnailFromYoutube(videoId,holder);
            holder.tvTime.setText("" + notification.getNotificationTime());




            //Broadcast Zip Set
        }else if (mediaType.equalsIgnoreCase("zip")&&notifyType.equalsIgnoreCase(ResponseParameters.BROADCAST)){
            setVisbiltyinMedia(holder);
            holder.tvNotification.setText(activity.getResources().getString(R.string.new_message_kotumb_team));
            holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.zip_notification));
            holder.tvTime.setText("" + notification.getNotificationTime());





            //Broadcast MP4 Video Set
        }else if(mediaType.equalsIgnoreCase("mp4")&&notifyType.equalsIgnoreCase(ResponseParameters.BROADCAST)){
            setVisbiltyinMedia(holder);
            holder.tvNotification.setText(activity.getResources().getString(R.string.new_message_kotumb_team));
            holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video_notification));
            holder.tvTime.setText("" + notification.getNotificationTime());





            //Broadcast .JPG Set
        }else if (mediaType.equalsIgnoreCase("jpg")&&notifyType.equalsIgnoreCase(ResponseParameters.BROADCAST)){
            setVisbiltyinMedia(holder);
            holder.tvNotification.setText(activity.getResources().getString(R.string.new_message_kotumb_team));
            holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.image_notification));
            holder.tvTime.setText("" + notification.getNotificationTime());




            //Broadcast PDF Set
        }else if (notification.getMediaType().equalsIgnoreCase("pdf")&&notifyType.equalsIgnoreCase(ResponseParameters.BROADCAST)){
            setVisbiltyinMedia(holder);
            holder.tvNotification.setText(R.string.new_message_kotumb_team);
            holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.pdf_notification));
            holder.tvTime.setText("" + notification.getNotificationTime());




            //Broadcast MKV Video Set
        }else if(notification.getMediaType().equalsIgnoreCase("mkv")&&notifyType.equalsIgnoreCase(ResponseParameters.BROADCAST)){
            setVisbiltyinMedia(holder);
            holder.tvNotification.setText(activity.getResources().getString(R.string.new_message_kotumb_team));
            holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video_notification));
            holder.tvTime.setText("" + notification.getNotificationTime());





            //Broadcast 3GP Video Set
        }else if(notification.getMediaType().equalsIgnoreCase("3gp")&&notifyType.equalsIgnoreCase(ResponseParameters.BROADCAST)) {
            setVisbiltyinMedia(holder);
            holder.tvNotification.setText(activity.getResources().getString(R.string.new_message_kotumb_team));
            holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video_notification));
            holder.tvTime.setText("" + notification.getNotificationTime());





            //Broadcast FLV Video Set
        }else if(notification.getMediaType().equalsIgnoreCase("flv")&&notifyType.equalsIgnoreCase(ResponseParameters.BROADCAST)){
                setVisbiltyinMedia(holder);
            holder.tvNotification.setText(activity.getResources().getString(R.string.new_message_kotumb_team));
            holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video_notification));
            holder.tvTime.setText("" + notification.getNotificationTime());





            //Broadcast .PNG Set
        }else if(notification.getMediaType().equalsIgnoreCase("png")&&notifyType.equalsIgnoreCase(ResponseParameters.BROADCAST)) {
            setVisbiltyinMedia(holder);
            holder.tvNotification.setText(activity.getResources().getString(R.string.new_message_kotumb_team));
            holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.image_notification));
            holder.tvTime.setText("" + notification.getNotificationTime());





            //Broadcast .JPG Set
        }else if(notification.getMediaType().equalsIgnoreCase("jpeg")&&notifyType.equalsIgnoreCase(ResponseParameters.BROADCAST)) {
            setVisbiltyinMedia(holder);
            holder.tvNotification.setText(activity.getResources().getString(R.string.new_message_kotumb_team));
            holder.squarLayout.setVisibility(View.GONE);
            new AQuery(activity).id(holder.ivUserImage).image(API.get_mediaLink + notification.getMedia(), true, true, 300, R.drawable.default_profile);
            holder.tvTime.setText("" + notification.getNotificationTime());




            //Broadcast Birthday Set
        } else if(DOB.equalsIgnoreCase(getDateAndTime())&&notifyType.equalsIgnoreCase("birthday")){
            setVisbiltyinMedia(holder);
            holder.cvContainer.setClickable(true);
            new AQuery(activity).id(holder.ivUserImage).image(API.imageUrl + user1.getAvatar(), true, true, 300, R.drawable.default_profile);
            holder.tvNotification.setText(R.string.birthday_wish_text);
            holder.tvTime.setText("" + notification.getNotificationTime());
            holder.cvContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((HomeScreenActivity) activity).openMeFragment();
                }
            });



        }else if(notification.getNotificationType().equalsIgnoreCase("birthday")&&!DOB.equalsIgnoreCase(getDateAndTime())){
            setVisbiltyinMedia(holder);
            new AQuery(activity).id(holder.ivUserImage).image(API.imageUrl + user1.getAvatar(), true, true, 300, R.drawable.default_profile);
            holder.tvNotification.setText(activity.getResources().getString(R.string.birthday_after_text)  +user1.getDOB());
            holder.tvTime.setText("" + notification.getNotificationTime());

        }
        else if (notification.getNotificationType().equalsIgnoreCase(ResponseParameters.BROADCAST)&&notification.getMedia().equalsIgnoreCase("")){
            holder.tvNotification.setText(activity.getResources().getString(R.string.new_message_kotumb_team));
            holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.text_notification));
            holder.tvTime.setText("" + notification.getNotificationTime());
        }
//        else if(notification.getNotificationType().equalsIgnoreCase("videoapproved")){
//            holder.tvNotification.setText(notification.getUser().getFirstName()+" "+notification.getUser().getLastName()+"  Your Video is Apporove");
//            holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video_notification));
//            holder.tvTime.setText("" + notification.getNotificationTime());
//
//        }
        else {
            setVisbiltyinMedia(holder);
            if(notification.getMediaType().equals("mp4")){
                holder.ivUserImage.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.video_notification));
            }
            else
            new AQuery(activity).id(holder.ivUserImage).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.default_profile);
            holder.tvTime.setText("" + notification.getNotificationTime());

        }
    }

    //This Method Made By Arpit Kanda
    //set Image Visibilty
    public void setVisbiltyinMedia(UserViewHolder holder){
        holder.squarLayout.setVisibility(View.GONE);
        holder.ivUserImage.setVisibility(View.VISIBLE);
    }



    private String getDateAndTime() {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = format.format(today);
        String DOB=dateToStr.substring(5);
        System.out.println("Current Date   "+DOB);
        return DOB;
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }
    }

    //This is Method Is Created By Arpit Kanda
    //All Popups For All Media-Video,YoutubeActivity,Zip,Image
    private void mediaShowPopUp(final String media,final String notifyTpe,final Notification notification,final  UserViewHolder holder) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_media, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final TextView tvDownload = dialogView.findViewById(R.id.download_file);
        final LinearLayout linearLayout = dialogView.findViewById(R.id.download);
        final TextView discriptions = dialogView.findViewById(R.id.txt_description);
        final ImageView imageView=dialogView.findViewById(R.id.media_imageVIew);
        final ProgressBar progressBar=dialogView.findViewById(R.id.pbLoading);
        final WebView wv = dialogView.findViewById(R.id.youtube_webview);
        final TextView youtubeTitle=dialogView.findViewById(R.id.txt_title);
        wv.getSettings().setJavaScriptEnabled(true);



        //Broadcast PDF View
        if (media.equalsIgnoreCase("pdf")&&notifyTpe.equalsIgnoreCase("broadcast")){
            youtubeTitle.setVisibility(View.GONE);
            wv.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            tvDownload.setText("Download");
            imageView.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.pdf_notification));
            discriptions.setText(notification.getNotificationDes());
            tvDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    downloadUri(notification);
                }
            });
            progressBar.setVisibility(View.GONE);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showFullPDF(notification);
                }
            });
            holder.bg.setBackgroundColor(Color.parseColor("#00fff9c4"));




            //Broadcast ZIP View
        } else  if (notification.getMediaType().equalsIgnoreCase("zip")&&notifyTpe.equalsIgnoreCase("broadcast")){
            youtubeTitle.setVisibility(View.GONE);
            wv.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            imageView.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.zip_notification));
            tvDownload.setText(activity.getResources().getString(R.string.download_btn));
            discriptions.setText(notification.getNotificationDes());
            tvDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressBar.setVisibility(View.VISIBLE);
                    downloadUri(notification);
                    progressBar.setVisibility(View.GONE);
                }
            });
            holder.bg.setBackgroundColor(Color.parseColor("#00fff9c4"));
            holder.tvNotification.setTypeface(null, Typeface.NORMAL);






            //Broadcast MP4 View Set
        }else  if (notification.getMediaType().equalsIgnoreCase("mp4")&&notifyTpe.equalsIgnoreCase("broadcast")) {
            imageView.setVisibility(View.GONE);
            youtubeTitle.setVisibility(View.GONE);
            wv.setVisibility(View.VISIBLE);
            System.out.println("URL==="+notification.getMedia());
            tvDownload.setText(activity.getResources().getString(R.string.full_screen_btn));
            //linearLayout.setVisibility(View.GONE);
            videoPlayFromServer(tvDownload,imageView,notification,discriptions,progressBar,wv);






            //Broadcast YoutubeActivity Video View
        } else if (notification.getMediaType().equalsIgnoreCase("videoLink")&&notifyTpe.equalsIgnoreCase("broadcast")){
            imageView.setVisibility(View.GONE);
            final String getMedia=notification.getMedia();
            final String videoId=getMedia.substring(17);
            tvDownload.setText(activity.getResources().getString(R.string.full_screen_btn));
            youtubeTitle.setVisibility(View.VISIBLE);
            //linearLayout.setVisibility(View.GONE);
            //holder.videoIcon.setVisibility(View.VISIBLE);
            setYoututbeDetails(notification,tvDownload,wv,videoId,youtubeTitle,progressBar);
            discriptions.setText(notification.getNotificationDes());


            //Broadcast MKV Video View
        }else if  (notification.getMediaType().equalsIgnoreCase("mkv")&&notifyTpe.equalsIgnoreCase("broadcast")) {
            youtubeTitle.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
            videoPlayFromServer(tvDownload,imageView,notification,discriptions,progressBar,wv);


            //Broadcast FLV Video VIew
        }else if  (notification.getMediaType().equalsIgnoreCase("flv")&&notifyTpe.equalsIgnoreCase("broadcast")){
            youtubeTitle.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
            videoPlayFromServer(tvDownload,imageView,notification,discriptions,progressBar,wv);



            //Broadcast 3GP Video View
        }else if(notification.getMediaType().equalsIgnoreCase("3gp")&&notifyTpe.equalsIgnoreCase("broadcast")){
            youtubeTitle.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
            videoPlayFromServer(tvDownload,imageView,notification,discriptions,progressBar,wv);



            //Broadcast .JPG Set
        }else if(notification.getMediaType().equalsIgnoreCase("jpg")&&notifyTpe.equalsIgnoreCase("broadcast")){
            youtubeTitle.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
            setImageFromServer(tvDownload,notification,imageView,wv,progressBar,discriptions);


            //Broadcast .JPEG Set
        }else if(notification.getMediaType().equalsIgnoreCase("jpeg")&&notifyTpe.equalsIgnoreCase("broadcast")){
            youtubeTitle.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
            setImageFromServer(tvDownload,notification,imageView,wv,progressBar,discriptions);

         //Broadcast .PNG Set
        }else if(notification.getMediaType().equalsIgnoreCase("png")&&notifyTpe.equalsIgnoreCase("broadcast")){
            youtubeTitle.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
            setImageFromServer(tvDownload,notification,imageView,wv,progressBar,discriptions);
        }
        else{
            linearLayout.setVisibility(View.GONE);
        }

        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });
        confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();
    }


    public void videoPlayFromServer(TextView download,ImageView imageView,final Notification notification,TextView discriptions,final ProgressBar progressBar,WebView wv){
        imageView.setVisibility(View.GONE);
        final String url=API.get_mediaLink+notification.getMedia();
        System.out.println("link==="+url);
        discriptions.setText(notification.getNotificationDes());
        playServerVideo(progressBar,wv,url);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                intent.putExtra("force_fullscreen",true);
                activity.startActivity(intent);
            }
        });
    }


    public void setImageFromServer(TextView textView,final Notification notification,ImageView imageView,WebView wv,final ProgressBar progressBar,TextView discriptions){
        imageView.setVisibility(View.VISIBLE);
        wv.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        discriptions.setText(notification.getNotificationDes());
        new AQuery(activity).id(imageView).image(API.get_mediaLink+notification.getMedia(), true, true, 300, R.drawable.default_profile);
        downloadClick(textView,notification,progressBar);
    }

    public void playServerVideo(ProgressBar progressBar,WebView webview,String url){
        webview.setWebViewClient(new myWebClient());
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webview.setWebChromeClient(new WebChromeClient());
        activity.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        webview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        String url123="<video controls=\"\" width=\"100%\" height=\"200\"><source src=\""+url+"\" type=\"video/mp4\" id=\"video_src\"></video>";
        webview.loadDataWithBaseURL("",url123,mimeType,encoding,"");
        progressBar.setVisibility(View.GONE);
    }






    //This Method is Created By Arpit Kanda
    //Set Youtube Details-Title Description and Thumbnail
    private void setYoututbeDetails(final Notification notification,final TextView play,final WebView wv,String videoId,final TextView name,final ProgressBar progressBar){
        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        String youtubeURL ="https://www.youtube.com/watch?v="+videoId;
        String youtubeURLJson = "http://www.youtube.com/oembed?url=" + youtubeURL + "&format=json";
        final String html ="<iframe class=\"youtube-player\" style=\"border: 0; width: 100%; height: 85%; padding:0px; margin:0px\" id=\"ytplayer\" type=\"text/html\" src=\"http://www.youtube.com/embed/"
                + videoId
                + "?fs=0\" frameborder=\"0\">\n"
                + "</iframe>\n";
        activity.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        wv.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        StringRequest strReq = new StringRequest(Request.Method.GET, youtubeURLJson, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                wv.setWebChromeClient(new WebChromeClient());
                wv.loadDataWithBaseURL("", html, mimeType, encoding, "");
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String text= jsonObject.getString("title");
                    name.setText(text);
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("Volly Error   "+e.getMessage());
                }

                play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(notification.getMedia()));
                        System.out.println("URL==="+Uri.parse(notification.getMedia()));
                        intent.putExtra("force_fullscreen",true);
                        activity.startActivity(intent);
                    }});
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Json  Volly Error   "+error.getMessage());
            }
        }) {
        };
        Volley.newRequestQueue(activity).add(strReq);
    }





    //This Method is Created By Arpit Kanda
    //Download and View Method
    public void downloadClick(TextView download,final Notification notification,final ProgressBar progressBar){
//        new DownloadFile().execute(API.get_mediaLink+notification.getMedia(),notification.getMedia());
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadUri(notification);
            }
        });
    }







AlertDialog alertDialog;
    String downloaded="";
    String total="";
public void downloadUri(Notification notification){
        System.out.println("ClickOn---------------------------------------------------------------------");
//    Uri uri = Uri.parse("file://" + destination);
    File uri=new File(Environment.getExternalStorageDirectory() + "/KotumbMedia/" + notification.getMedia());
    String url=API.get_mediaLink+notification.getMedia();
//Set up download manager request
    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url))
    .setDescription("Downloading " + notification.getMedia())
    .setTitle("Download Files")
    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
    .setDestinationUri(Uri.fromFile(uri)); //URI is valid

//Start the download
    final DownloadManager manager = (DownloadManager)activity
            .getSystemService(Context.DOWNLOAD_SERVICE);
    final long downloadId = manager.enqueue(request);

    final int UPDATE_PROGRESS = 5020;

    alertDialog=new AlertDialog.Builder(activity).setMessage("Downloading...").setCancelable(false).show();
    final Handler handler=new Handler(){
        @Override
        public void handleMessage(android.os.Message msg) {

            if(msg.what==UPDATE_PROGRESS){
                 downloaded = String.format("%.2f MB", (double)((msg.arg1)/1024)/1024);
                 total = String.format("%.2f MB", (double)( (msg.arg2)/1024)/1024);
                String status = downloaded + " / " + total;

            }
            else{
                if(alertDialog.isShowing()){
                    alertDialog.dismiss();
                }
            }
        }
    };
    new Thread(new Runnable() {
        @Override
        public void run() {
            boolean downloading = true;
            while (downloading) {
                DownloadManager.Query q = new DownloadManager.Query();
                q.setFilterById(downloadId);
                Cursor cursor = manager.query(q);
                cursor.moveToFirst();
                int bytes_downloaded = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                    downloading = false;
                    if(alertDialog.isShowing()){
                        alertDialog.dismiss();
                    }
                }
                //Post message to UI Thread
                Message   msg = handler.obtainMessage();
                msg.what = UPDATE_PROGRESS;
                //msg.obj = statusMessage(cursor);
                msg.arg1 = bytes_downloaded;
                msg.arg2 = bytes_total;
                handler.sendMessage(msg);
                cursor.close();
            }
        }
    }).start();
}


    public void showFullPDF(Notification notification){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + API.get_mediaLink+notification.getMedia()), "text/html");
        activity.startActivity(intent);
    }





    //This Method is Written By Arpit Kanda
    //Get Thumbnail From YoutubeActivity URL
    public void getThumbnailFromYoutube(String videoId,UserViewHolder holder){
    System.out.println("youtube_thumb");
        try{
            String img_url="http://img.youtube.com/vi/"+videoId+"/0.jpg"; // this is link which will give u thumnail image of that video
            new AQuery(activity).id(holder.ivUserImageSquar).image(img_url, true, true, 300, R.drawable.ic_user);
        }catch (Exception ex){
            System.out.println("Video Exception  "+ex);
        }
    }










    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvNotification;
        ImageView ivUserImage;
        ImageView ivUserImageSquar;
        ImageView videoIcon;
        CardView cvContainer;
        TextView tvTime;
        LinearLayout bg;
        FrameLayout squarLayout;

        UserViewHolder(View view) {
            super(view);
            tvUserName = view.findViewById(R.id.tvUserName);
            tvNotification = view.findViewById(R.id.tvNotification);
            ivUserImage = view.findViewById(R.id.ivUserImage);
            ivUserImageSquar = view.findViewById(R.id.ivUserImageSquar);
            videoIcon=view.findViewById(R.id.mainVideoIcon);
            cvContainer = view.findViewById(R.id.cvContainer);
            squarLayout=view.findViewById(R.id.ivFramelayout);
            tvTime = view.findViewById(R.id.tvTime);
            bg = view.findViewById(R.id.bg);
        }
    }
}