package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Adapter.VideoLikeListAdapter;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.Model.VideoData;
import com.codeholic.kotumb.app.Model.VideoLikes;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.View.DividerItemDecoration;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSink;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/*This VideoDetailsActivity Made By Arpit Kanda*/

public class VideoDetailsActivity extends AppCompatActivity  implements Player.EventListener{
    private TextView tvUserName,tvTime,tvVideoDescription,tvLike,header,tv_video_title,ls_video_status,tv_likes_count,dialog_termsContionsHeading,dialog_termsConditionText,publishVideo;
    private ImageView ivUserAvatar,ivDefaulticon,ivThumbnail,ivCross,dismiss_dialog,videoStatus_logo,play_icon,publishLogo,iv_allow_fullscreen;
    private RelativeLayout rl_videoContainer,dialog_headerLayout;
    private VideoView videoview;
    private WebView dialog_web_view;
    private SimpleExoPlayer player;
    private PlayerView playerView;
    FrameLayout addFullScreen;
    PlayerView fullscreeplay;
    SimpleExoPlayer fullplayer;
    private ProgressBar spinnerVideoDetails;
    private LinearLayout video_like,dialog_termsContionsLayout,dialog_llLoading,video_share,llEditPost;
    private RecyclerView rv_like_list;
    private FrameLayout videoThumbnailFrame;
    private  Toolbar mToolbar;
    boolean clicked=true;
    ProgressBar progressBar;
    String activityType="";
    private User user;
    LinearLayoutManager linearLayoutManager;
    VideoLikeListAdapter videoLikeListAdapter;
    List<VideoLikes> videoLikesList=new ArrayList<VideoLikes>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_details);
        findViews();
        initToolbar();
        setVisibilty();
        initStartup();
        addFullScreen=findViewById(R.id.main_media_frame);
        addFullScreen.setVisibility(View.VISIBLE);
        // ATTENTION: This was auto-generated to handle app links.

    }

    private void dialogfindViews(View dialogView){
        dialog_web_view = dialogView.findViewById(R.id.web_view);
        dialog_termsContionsLayout = dialogView.findViewById(R.id.condition_message_layout);
        dialog_termsContionsHeading = dialogView.findViewById(R.id.condtion_heading);
        dialog_termsConditionText=dialogView.findViewById(R.id.condtion_text);
        dialog_headerLayout=dialogView.findViewById(R.id.header_layout);
        dismiss_dialog=dialogView.findViewById(R.id.dismiss_dialog);
        fullscreeplay=dialogView.findViewById(R.id.fullscreeplay);
        rv_like_list=dialogView.findViewById(R.id.rv_user_like_list);
        progressBar=dialogView.findViewById(R.id.video_loader);
        dialog_llLoading=dialogView.findViewById(R.id.llLoading);
    }



    private void findViews(){
        videoview=findViewById(R.id.videoview);
        ivThumbnail=findViewById(R.id.video_thumbnail);
        tv_video_title=findViewById(R.id.ls_video_title);
        addFullScreen=findViewById(R.id.main_media_frame);
        iv_allow_fullscreen=findViewById(R.id.iv_allow_fullscreen);
        ivDefaulticon=findViewById(R.id.default_icon);
        rl_videoContainer=findViewById(R.id.play_video_container);
        playerView=findViewById(R.id.play_video);
        publishVideo=findViewById(R.id.ls_video_publish_status);
        publishLogo=findViewById(R.id.publishVideoStatus_logo);
        spinnerVideoDetails=findViewById(R.id.video_loader);
        tvVideoDescription=findViewById(R.id.ls_video_description);
        tvUserName=findViewById(R.id.tvUserName);
        ivUserAvatar=findViewById(R.id.ivUserImage);
        ivCross=findViewById(R.id.remove_video);
        tvLike=findViewById(R.id.tvLike);
        video_like=findViewById(R.id.video_like);
        mToolbar=findViewById(R.id.toolbar_actionbar);
        videoStatus_logo=findViewById(R.id.videoStatus_logo);
        header=findViewById(R.id.header);
        ls_video_status=findViewById(R.id.ls_video_status);
        tv_likes_count=findViewById(R.id.tv_likes_count);
        tvTime=findViewById(R.id.tvTime);
        video_share=findViewById(R.id.video_share);
        llEditPost=findViewById(R.id.llEditPost);
        videoThumbnailFrame=findViewById(R.id.videoThumbnailFrame);
        play_icon=findViewById(R.id.play_icon);
    }


    private void setVisibilty(){
        ivDefaulticon.setVisibility(View.GONE);
        ivCross.setVisibility(View.GONE);
        rl_videoContainer.setVisibility(View.VISIBLE);
      //  addFullScreen.setVisibility(View.VISIBLE);
        iv_allow_fullscreen.setVisibility(View.VISIBLE);
        spinnerVideoDetails.setVisibility(View.VISIBLE);
        publishLogo.setVisibility(View.GONE);
        play_icon.setVisibility(View.GONE);
        publishVideo.setVisibility(View.GONE);
        ls_video_status.setVisibility(View.GONE);
        videoStatus_logo.setVisibility(View.GONE);
        llEditPost.setVisibility(View.GONE);
      //  tv_likes_count.setVisibility(View.GONE);
    }



    private void initStartup(){
        //tv_video_title.setVisibility(View.VISIBLE);

        Intent intent = getIntent();
            VideoData videoData=new VideoData();
        System.out.println("awsm=="+intent.getStringExtra("video_desp"));
            String video_id=intent.getStringExtra("video_id");
            String video_title=intent.getStringExtra("video_title");
            String video_desp="";
            String user_id="";
            user_id=intent.getStringExtra("id");
            videoData.setUserId(user_id);
             video_desp=intent.getStringExtra("video_desp");
            String video_like_count="",video_user="";
            if(intent.hasExtra("video_like_count")) {
                video_like_count = intent.getStringExtra("video_like_count");
            }
            String video_date_time=intent.getStringExtra("video_date_time");
            if(intent.hasExtra("video_user")) {
                video_user = intent.getStringExtra("video_user");
            }
            tv_video_title.setText("Title: "+video_title);
            String user_avatar=intent.getStringExtra("video_avatar");
            String video_uri=intent.getStringExtra("video_uri");
            videoData.setUserFirstName(video_user);
            videoData.setVideoUploadedDate(video_date_time);
            videoData.setVideoTitle(video_title);
            videoData.setVideoLikeCount(video_like_count);
            videoData.setVideoId(video_id);
            videoData.setVideoDescription(video_desp);
            videoData.setVideoName(video_uri);
            videoData.setUserAvatar(user_avatar);
            if (!video_desp.isEmpty() && !video_desp.equals("null")){
                tvVideoDescription.setText(getResources().getString(R.string.video_description_text_1) + " " + video_desp);
            }

            else tvVideoDescription.setVisibility(View.GONE);
            tvTime.setText(video_date_time);
            tv_likes_count.setText(video_like_count);

            tvUserName.setText(SharedPreferencesMethod.getUserInfo(this).getFirstName()+" "+SharedPreferencesMethod.getUserInfo(this).getLastName());
            if(intent.hasExtra("fullname"))
                tvUserName.setText(intent.getStringExtra("fullname"));
            System.out.println("awsm=="+intent.getStringExtra("activitytype")+"=="+intent.getStringExtra("fullname"));
            if(intent.hasExtra("activitytype")){
                if(intent.hasExtra("fullname"))
                tvUserName.setText(intent.getStringExtra("fullname"));
                new AQuery(this).id(ivUserAvatar).image(API.imageUrl +intent.getStringExtra("avatar"), true, true, 300, R.drawable.default_profile);
                if(intent.getStringExtra("activitytype").equals("videoapproved")){
                    activityType="videoapproved";
                    video_like.setVisibility(View.GONE);
                    tv_likes_count.setVisibility(View.GONE);
                    System.out.println("Inside video approve");
                    new AQuery(this).id(ivUserAvatar).image(API.imageUrl + SharedPreferencesMethod.getUserInfo(this).getAvatar(), true, true, 300, R.drawable.default_profile);
                    publishLogo.setVisibility(View.VISIBLE);
                    publishVideo.setVisibility(View.VISIBLE);
                    ls_video_status.setVisibility(View.VISIBLE);
                    videoStatus_logo.setVisibility(View.VISIBLE);
                    ls_video_status.setText(getResources().getString(R.string.video_status_text)+" "+getResources().getString(R.string.video_approved_text));
                    videoStatus_logo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check_mark));
                    if(intent.getStringExtra("isPublished").equalsIgnoreCase("2")){
                        publishVideo.setText(getResources().getString(R.string.video_publish_status)+" "+getResources().getString(R.string.video_published));
                        publishLogo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check_mark));
                    }
                    else {
                        publishVideo.setText(getResources().getString(R.string.video_publish_status)+" " + getResources().getString(R.string.pending_text));
                        publishLogo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.pending));
                    }
                }
                else if(intent.getStringExtra("activitytype").equals("publishedvideonotification") || intent.getStringExtra("activitytype").equals("videolikes")){
                    activityType="publishedvideonotification";
                    tv_likes_count.setVisibility(View.VISIBLE);
                    System.out.println("Inside video publish");
                    video_like.setVisibility(View.VISIBLE);
                    if(intent.getStringExtra("like_status").equals("1")){
                        Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up_liked);
                        tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                        tvLike.startAnimation(Utility.showAnimtion(VideoDetailsActivity.this));
                        tvLike.setText(getResources().getString(R.string.video_unlike));

                    }
                    if(intent.hasExtra("like_count")){
                        if(Integer.parseInt(intent.getStringExtra("like_count"))>0)
                        tv_likes_count.setText(intent.getStringExtra("like_count")+" "+getResources().getString(R.string.video_likes));
                        else tv_likes_count.setVisibility(View.GONE);
                    }
                    new AQuery(this).id(ivUserAvatar).image(API.imageUrl + SharedPreferencesMethod.getUserInfo(this).getAvatar(), true, true, 300, R.drawable.default_profile);
                    String url=API.VIDEO_LIKES+video_id+"/"+ SharedPreferencesMethod.getUserId(VideoDetailsActivity.this)+"/"+0;
                  /*  new AQuery(this).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
                        @Override
                        public void callback(String url, JSONObject object, AjaxStatus status) {
                            super.callback(url, object, status);
                            System.out.println("Video Like Response  "+object.toString());
                            try{
                                if (object.has(ResponseParameters.Success)){
                                    object.getString("like_count");
                                    int totalLikes=Integer.parseInt(object.getString("like_count"));
                                    tv_likes_count.setText("Likes: "+String.valueOf(totalLikes));
                                }
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }

                        }
                    });*/
                }
            }
            else{
                new AQuery(this).id(ivUserAvatar).image(API.imageUrl +user_avatar, true, true, 300, R.drawable.default_profile);
            }
           // tv_video_title.setText(video_title);

            playVideo(Uri.parse(API.user_videos+video_uri));
      //  Toast.makeText(VideoDetailsActivity.this,video_id,Toast.LENGTH_LONG).show();
            String url=API.VIDEO_LIKES+video_id+"/"+ SharedPreferencesMethod.getUserId(VideoDetailsActivity.this)+"/"+1;
            System.out.println("Video Like URL   "+url);
            videoLikes(video_id);
            setOnClick(video_id,videoData);
       /* }
        else {
            System.out.println("awsm== not found");
        }*/
        /*else if (intent.hasExtra("video_Uri")){
            VideoData videoData=new VideoData();
            user = intent.getParcelableExtra("user");
            String uri=intent.getStringExtra("videoUri");
            String description=intent.getStringExtra("videoDescription");
            String video_id=intent.getStringExtra("videoId");
            String userName=intent.getStringExtra("video_userName");
            videoData.setUserFirstName(SharedPreferencesMethod.getUserInfo(this).getFirstName());
            videoData.setUserLastName(SharedPreferencesMethod.getUserInfo(this).getLastName());
            videoData.setVideoId(video_id);
            videoData.setVideoDescription(getResources().getString(R.string.video_description_text_1)+" "+description);
            videoData.setVideoName(uri);
            tvUserName.setText(SharedPreferencesMethod.getUserInfo(this).getFirstName()+" "+SharedPreferencesMethod.getUserInfo(this).getLastName());
            tvVideoDescription.setText(description);
            new AQuery(this).id(ivUserAvatar).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.default_profile);
            playVideo(Uri.parse(API.user_videos+uri));
            String url=API.VIDEO_LIKES+video_id+"/"+ SharedPreferencesMethod.getUserId(VideoDetailsActivity.this)+"/"+1;
            System.out.println("Video Like URL   "+url);
            videoLikes(video_id);
            setOnClick(video_id,videoData);
        }else if (intent.hasExtra("videoUri")){
            videoGetData(intent);
        }*/
    }




    private void videoGetData(Intent intent){
        VideoData videoData=new VideoData();
        user = intent.getParcelableExtra("user");
        String uri=intent.getStringExtra("videoUri");
        String description=intent.getStringExtra("videoDescription");
        String video_id=intent.getStringExtra("videoId");
        String userName=intent.getStringExtra("video_userName");
        String fname=intent.getStringExtra("fname");
        String lname=intent.getStringExtra("laname");
        videoData.setUserFirstName(user.getFirstName());
        videoData.setUserLastName(user.getLastName());
        videoData.setVideoId(video_id);
        videoData.setVideoDescription(getResources().getString(R.string.video_description_text_1)+" "+description);
        videoData.setVideoName(uri);
        System.out.println("check==="+user.getFirstName()+" "+user.getLastName()+"=="+user.getAvatar());
        tvUserName.setText(user.getFirstName()+" "+user.getLastName());
        tvVideoDescription.setText(description);
        new AQuery(this).id(ivUserAvatar).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.default_profile);
        playVideo(Uri.parse(API.user_videos+uri));
        String url=API.VIDEO_LIKES+video_id+"/"+ SharedPreferencesMethod.getUserId(VideoDetailsActivity.this)+"/"+0;
        /*new AQuery(this).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                System.out.println("Video Like Response  "+object.toString());
                try{
                    if (object.has(ResponseParameters.Success)){
                        object.getString("like_count");
                        int totalLikes=Integer.parseInt(object.getString("like_count"));
                        tv_likes_count.setText("Likes: "+String.valueOf(totalLikes));
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }

            }
        });*/
        System.out.println("Video Like URL   "+url);
        videoLikes(video_id);
        setOnClick(video_id,videoData);
    }





    private void LikePopup(final Activity activity,String videoId){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.video_terms_condition_layout, null);
        dialogBuilder.setView(dialogView);
        dialogfindViews(dialogView);
        FrameLayout frameLayout=dialogView.findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);
        dialog_web_view.setVisibility(View.GONE);
        dialog_headerLayout.setVisibility(View.VISIBLE);
        dialog_termsConditionText.setVisibility(View.GONE);
        dialog_termsContionsLayout.setVisibility(View.VISIBLE);
        dialog_termsContionsHeading.setText("User Like List");
        rv_like_list.setVisibility(View.VISIBLE);
        userLikesListServiceCall(videoId,activity);
        final AlertDialog confirmAlert = dialogBuilder.create();
        dismiss_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();
    }



    private void userLikesListServiceCall(final String videoId, final Activity activity){
        dialog_llLoading.setVisibility(View.VISIBLE);
        videoLikesList.clear();
        String url=API.VIDEO_LIKES_LIST+videoId;
        System.out.println("Video Delete URL   "+url);
        new AQuery(activity).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject outPut, AjaxStatus status) {
                        super.callback(url, outPut, status);
                        dialog_llLoading.setVisibility(View.GONE);
                        System.out.println("Video Like List Response   "+outPut.toString());
                        try{
                            for (int j=0; j<outPut.getJSONArray("likes").length(); j++){
                                JSONObject likeObj = outPut.getJSONArray("likes").getJSONObject(j);
                                VideoLikes videoLikes=new VideoLikes();
                                videoLikes.setLike_date(likeObj.getString(ResponseParameters.LIKED_AT));
                                videoLikes.setLike_id(likeObj.getString(ResponseParameters.LIKE_ID));
                                videoLikes.setUser_id(likeObj.getString("userId"));
                                videoLikes.setVideo_avatar(likeObj.getString(ResponseParameters.Avatar));
                                videoLikes.setVideo_first_name(likeObj.getString(ResponseParameters.FirstName));
                                videoLikes.setVideo_last_name(likeObj.getString(ResponseParameters.LastName));
                                videoLikesList.add(videoLikes);
                            }
                            linearLayoutManager = new LinearLayoutManager(activity);
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rv_like_list.setLayoutManager(linearLayoutManager);
                            videoLikeListAdapter = new VideoLikeListAdapter(rv_like_list, activity, videoLikesList);
                            rv_like_list.addItemDecoration(
                                    new DividerItemDecoration(activity, R.drawable.divider));
                            rv_like_list.setAdapter(videoLikeListAdapter);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }
                }
        );
    }





    private void setOnClick(final String videoId, final VideoData videoData){
        tv_likes_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LikePopup(VideoDetailsActivity.this,videoId);
            }
        });


        iv_allow_fullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                player.setPlayWhenReady(false);
                rl_videoContainer.setVisibility(View.VISIBLE);
                addFullScreen.setVisibility(View.GONE);
                showfullScreenPopup(videoData);
                play_icon.setVisibility(View.GONE);
            }
        });


        video_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                video_share.startAnimation(Utility.showAnimtion(VideoDetailsActivity.this));
                Utils.showVideoSharePopUpMenu(video_share,videoId, videoData, videoId, VideoDetailsActivity.this);
            }
        });
    }

    private void showfullScreenPopup(final VideoData videoData) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.video_terms_condition_layout, null);
        dialogBuilder.setView(dialogView);
        dialogfindViews(dialogView);
        dialog_termsContionsLayout.setVisibility(View.GONE);
        dialog_termsContionsHeading.setVisibility(View.GONE);
        dialog_termsConditionText.setVisibility(View.GONE);
        dialog_headerLayout.setVisibility(View.GONE);
        rv_like_list.setVisibility(View.GONE);
        dialog_llLoading.setVisibility(View.GONE);
        dialog_web_view.setVisibility(View.GONE);
        setFullplayer(Uri.parse(API.user_videos+videoData.getVideoName()));

        //activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        final AlertDialog confirmAlert = dialogBuilder.create();
        confirmAlert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
               addFullScreen.setVisibility(View.VISIBLE);
                videoThumbnailFrame.setVisibility(View.VISIBLE);
                rl_videoContainer.setVisibility(View.VISIBLE);
                if(fullplayer!=null){
                    fullplayer.stop();
                    fullplayer=null;
                }
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();
    }



    public class CacheDataSourceFactory implements DataSource.Factory {
        private final Context context;
        private final DefaultDataSourceFactory defaultDatasourceFactory;
        private final long maxFileSize, maxCacheSize;

        CacheDataSourceFactory(Context context, long maxCacheSize, long maxFileSize) {
            super();
            this.context = context;
            this.maxCacheSize = maxCacheSize;
            this.maxFileSize = maxFileSize;
            String userAgent = Util.getUserAgent(context, context.getString(R.string.app_name));
            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            defaultDatasourceFactory = new DefaultDataSourceFactory(this.context,
                    bandwidthMeter,
                    new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter));
        }


        @Override
        public DataSource createDataSource() {
            LeastRecentlyUsedCacheEvictor evictor = new LeastRecentlyUsedCacheEvictor(maxCacheSize);
            SimpleCache simpleCache = new SimpleCache(new File(context.getCacheDir(), "media"), evictor);
            return new CacheDataSource(simpleCache, defaultDatasourceFactory.createDataSource(),
                    new FileDataSource(), new CacheDataSink(simpleCache, maxFileSize),
                    CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR, null);
        }
    }

    private void setFullplayer(Uri url) {
        new File(getCacheDir(), "media").delete();
        fullplayer=null;
        if (fullplayer == null) {
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            fullplayer= ExoPlayerFactory.newSimpleInstance(this, trackSelector);
            MediaSource audioSource = new ExtractorMediaSource(url,
                    new CacheDataSourceFactory(this, 100 * 1024 * 1024, 5 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
            fullplayer.setPlayWhenReady(true);
            fullplayer.prepare(audioSource);
            fullscreeplay.setPlayer(fullplayer);
            fullplayer.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
//                    holder.player.stop();
                }

                @Override
                public void onLoadingChanged(boolean isLoading) {
                    if(isLoading){
                        progressBar.setVisibility(View.VISIBLE);
                    }
                    else progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    switch (playbackState) {
                        case Player.STATE_BUFFERING:
                           spinnerVideoDetails.setVisibility(View.VISIBLE);
                            break;
                        case Player.STATE_ENDED:

                            break;
                        case Player.STATE_IDLE:

                            break;
                        case Player.STATE_READY:
                            spinnerVideoDetails.setVisibility(View.GONE);
                            break;
                        default:
                            break;
                    }

                    if (playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED ||
                            !playWhenReady) {

                        fullscreeplay.setKeepScreenOn(false);
                    } else { // STATE_IDLE, STATE_ENDED
                        // This prevents the screen from getting dim/lock
                        fullscreeplay.setKeepScreenOn(true);
                    }

                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {
                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {
                }
            });
        }
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText("Video Details");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(!SharedPreferencesMethod.getBoolean(VideoDetailsActivity.this,"HomeScreenActivity")){
                   finish();
               }
               else{
                   Intent intent=new Intent(VideoDetailsActivity.this,HomeScreenActivity.class);
                   startActivity(intent);
                   finish();
               }
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        if(!SharedPreferencesMethod.getBoolean(VideoDetailsActivity.this,"HomeScreenActivity")){
            finish();
        }
        else{
            Intent intent=new Intent(VideoDetailsActivity.this,HomeScreenActivity.class);
            startActivity(intent);
            finish();
        }
        super.onBackPressed();  // optional depending on your needs
    }


    private void videoLikes(final String video_id){
        video_like.setClickable(true);
        video_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setAction(ResponseParameters.NOTIFICATIONS);
                intent.putExtra("refresh","");
                sendBroadcast(intent);
                if (tvLike.getText().toString().equalsIgnoreCase("Like")){
                    String url=API.VIDEO_LIKES+video_id+"/"+ SharedPreferencesMethod.getUserId(VideoDetailsActivity.this)+"/"+1;
                    System.out.println("Video Like URL   "+url);
                    Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up_liked);
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                    tvLike.startAnimation(Utility.showAnimtion(VideoDetailsActivity.this));
                    API.sendRequestToServerGET(VideoDetailsActivity.this, url, API.VIDEO_LIKES);
                    tvLike.setText("Unlike");
                }else{
                    String url=API.VIDEO_LIKES+video_id+"/"+ SharedPreferencesMethod.getUserId(VideoDetailsActivity.this)+"/"+0;
                    System.out.println("Video Like URL   "+url);
                    Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up);
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                    tvLike.startAnimation(Utility.showAnimtion(VideoDetailsActivity.this));
                    API.sendRequestToServerGET(VideoDetailsActivity.this, url, API.VIDEO_LIKES);
                    tvLike.setText("Like");
                }

            }
        });
    }



    public void getResponse(JSONObject outPut, int i) {
        if (i==0){
            System.out.println("Video Likes Response  "+outPut.toString());
            try{
                if (outPut.has(ResponseParameters.Success)){
                    outPut.getString("like_count");
                    int totalLikes=Integer.parseInt(outPut.getString("like_count"));
                    if(totalLikes>0) {
                        tv_likes_count.setVisibility(View.VISIBLE);
                        tv_likes_count.setText(String.valueOf(totalLikes) + " Likes");
                    }
                    else tv_likes_count.setVisibility(View.GONE);
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

    }




    private void initializePlayer() {
        if (player == null) {
            // 1. Create a default TrackSelector
            LoadControl loadControl = new DefaultLoadControl(
                    new DefaultAllocator(true, 20),
                    3000,
                    5000,
                    1500,
                    5000, -1, true);

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);
            // 2. Create the player
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this), trackSelector, loadControl);
            playerView.setPlayer(player);
            //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN |View.SYSTEM_UI_FLAG_IMMERSIVE);
        }
    }



    private void playVideo(Uri videoUri) {
        initializePlayer();
        if (videoUri == null) {
            return;
        }
        buildMediaSource(videoUri);

    }


    private void buildMediaSource(Uri mUri) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getResources().getString(R.string.app_name)), bandwidthMeter);
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);
        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        player.addListener(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(player!=null)
            player.setPlayWhenReady(false);
        if(fullplayer!=null)
            fullplayer.setPlayWhenReady(false);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.stop();
        if(fullplayer!=null){
            fullplayer.stop();
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        if(player!=null){
            player.setPlayWhenReady(false);
        }
        if(fullplayer!=null){
            fullplayer.setPlayWhenReady(false);
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {

            case Player.STATE_BUFFERING:
                spinnerVideoDetails.setVisibility(View.VISIBLE);
                break;
            case Player.STATE_ENDED:
                break;
            case Player.STATE_IDLE:

                break;
            case Player.STATE_READY:
                spinnerVideoDetails.setVisibility(View.GONE);

                break;
            default:
                break;
        }
        if (playWhenReady && playbackState == Player.STATE_READY) {
            // media actually playing
            play_icon.setVisibility(View.GONE);
        } else if (playWhenReady) {
            // might be idle (plays after prepare()),
            // buffering (plays when data available)
            // or ended (plays when seek away from end)
        } else {
            // player paused in any state
        }

        if (playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED ||
                !playWhenReady) {

            playerView.setKeepScreenOn(false);
        } else { // STATE_IDLE, STATE_ENDED
            // This prevents the screen from getting dim/lock
            playerView.setKeepScreenOn(true);
        }
    }





    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }



}
