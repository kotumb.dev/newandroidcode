package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Pattern;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class UpdateLoginInfoActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etPassword, etCPassword, etUserName, etOldPassword;
    private TextInputLayout userNameLayout, passwordLayout, cPasswordLayout, etOldPasswordLayout;
    private TextView tvRegister, tvUserNames;
    private LinearLayout llSuggestion;
    private ProgressBar pbLoading, pbUserName;
    private CardView cvRegister;
    private Activity context;
    private ImageView ivWrong, ivCorrect;
    private boolean userNameCorrect = false;
    private boolean isUserNameLoading = false;
    private String languageToLoad = "";
    private String passOld = "";
    Configuration config = new Configuration();
    Locale locale;
    User user = new User();

    //oncreate method for setting content view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languageToLoad = SharedPreferencesMethod.getDefaultLanguage(this);
        changeLocale(languageToLoad);
    }

    //change locale
    private void changeLocale(String languageToLoad) {
        locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setView();
    }

    //setting content view
    private void setView() {
        setContentView(R.layout.activity_change_info);
        initView();
        setClickListener();
        setTextWatcher(etPassword, passwordLayout);
        setTextWatcher(etUserName, userNameLayout);
        setTextWatcher(etCPassword, cPasswordLayout);
        setTextWatcher(etOldPassword, etOldPasswordLayout);
        if (!getIntent().getStringExtra(RequestParameters.USERNAME).isEmpty() && !getIntent().getStringExtra(RequestParameters.USERNAME).equals("null")) {
            etUserName.setText(getIntent().getStringExtra(RequestParameters.USERNAME).trim());
            int selection=getIntent().getStringExtra(RequestParameters.USERNAME).trim().length();
            etUserName.setSelection(selection);
        }

    }

    //initializing views
    private void initView() {
        context = this;
        user = getIntent().getParcelableExtra(ResponseParameters.Userdata);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etUserName = (EditText) findViewById(R.id.etUserName);
        etCPassword = (EditText) findViewById(R.id.etCPassword);
        etOldPassword = (EditText) findViewById(R.id.etOldPassword);
        etOldPasswordLayout = (TextInputLayout) findViewById(R.id.etOldPasswordLayout);
        userNameLayout = (TextInputLayout) findViewById(R.id.etUserNameLayout);
        passwordLayout = (TextInputLayout) findViewById(R.id.etPasswordLayout);
        cPasswordLayout = (TextInputLayout) findViewById(R.id.etCPasswordLayout);
        tvRegister = (TextView) findViewById(R.id.tvRegister);
        cvRegister = (CardView) findViewById(R.id.cv_register);
        pbLoading = (ProgressBar) findViewById(R.id.pbLoading);
        ivWrong = (ImageView) findViewById(R.id.ivWrong);
        ivCorrect = (ImageView) findViewById(R.id.ivCorrect);
        pbUserName = (ProgressBar) findViewById(R.id.pbUserName);
        ivCorrect.setVisibility(VISIBLE);
        tvUserNames = (TextView) findViewById(R.id.tvUserNames);
        llSuggestion = (LinearLayout) findViewById(R.id.llSuggestion);
    }

    //set click listeners on views
    private void setClickListener() {
        cvRegister.setOnClickListener(this);
    }

    //textwatcher method for all edittext
    private void setTextWatcher(final EditText editText, final TextInputLayout textInputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (editText == etPassword) {
                    passOld = etPassword.getText().toString().trim();
                    Log.e("beforeTextChanged", "" + etPassword.getText().toString().trim());
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    textInputLayout.setErrorEnabled(false);
                }
                if (editText == etUserName) {
                    llSuggestion.setVisibility(GONE);
                    if (etUserName.getText().toString().trim().equalsIgnoreCase(getIntent().getStringExtra(RequestParameters.USERNAME))) {
                        ivCorrect.setVisibility(VISIBLE);
                        ivWrong.setVisibility(GONE);
                        userNameCorrect = true;
                    } else {
                        ivCorrect.setVisibility(GONE);
                        ivWrong.setVisibility(GONE);
                        userNameCorrect = true;
                    }
                    pbUserName.setVisibility(GONE);
                    userNameLayout.setErrorEnabled(false);
                    if (!etUserName.getText().toString().trim().equalsIgnoreCase(getIntent().getStringExtra(RequestParameters.USERNAME)) && s.length() > 0) {

                        Pattern p = Pattern.compile("^[A-Za-z0-9_.-]{6,30}$");
                        boolean userNameMatcher = p.matcher(etUserName.getText().toString().trim()).find();

                        if (!userNameMatcher) {
                            enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_atleast));
                        } else if (etUserName.getText().toString().trim().length() < 6) {
                            enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_atleast));
                        } else {
                            if (!Utility.isConnectingToInternet(context)) {
                                return;
                            }
                            if (s.length() > 5) {
                                pbUserName.setVisibility(VISIBLE);
                                userNameCorrect = false;
                                isUserNameLoading = true;
                                HashMap<String, Object> input = new HashMap<>();
                                input.put(RequestParameters.USERNAME, "" + etUserName.getText().toString().trim());
                                Log.e("input", "" + input);
                                API.sendRequestToServerPOST_PARAM(context, API.CHECKUSERNAME, input);
                            }
                        }
                    } else {
                        userNameCorrect = true;
                    }
                }
                if (editText == etPassword) {
                    passwordLayout.setErrorEnabled(false);
                    if (s.length() > 0) {
                        if (!etCPassword.getText().toString().trim().isEmpty() && !passOld.equals(etPassword.getText().toString().trim())) {
                            etCPassword.setText("");
                            enableError(cPasswordLayout, getResources().getString(R.string.password_empty_input));
                        }
                        Pattern pass = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z!@#$%^&*\\d]{1,12}");
                        /*boolean userPassMatcher = pass.matcher(etPassword.getText().toString().trim()).find();
                        if (!userPassMatcher) {
                            enableError(passwordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
                        } else*/ if (etPassword.getText().toString().trim().length() < 6) {
                            enableError(passwordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
                        }
                    }
                }

                if (editText == etCPassword) {
                    cPasswordLayout.setErrorEnabled(false);
                    if (etCPassword.getText().toString().trim().isEmpty()) {
                        enableError(cPasswordLayout, getResources().getString(R.string.password_empty_input));
                    } else if (!etPassword.getText().toString().trim().equals(etCPassword.getText().toString().trim())) {
                        enableError(cPasswordLayout, getResources().getString(R.string.confirm_password_input_match));
                    }
                }

                if (editText == etOldPassword) {
                    etOldPasswordLayout.setErrorEnabled(false);
                    if (s.length() > 0) {
                        if (!etOldPassword.getText().toString().trim().equalsIgnoreCase(getIntent().getStringExtra(RequestParameters.PASSWORD))) {
                            enableError(etOldPasswordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //disabling error of input layout
    void disableError() {
        userNameLayout.setErrorEnabled(false);
        passwordLayout.setErrorEnabled(false);
        etOldPasswordLayout.setErrorEnabled(false);
        cPasswordLayout.setErrorEnabled(false);
    }

    //enabling error of input layout
    void enableError(TextInputLayout inputLayout, String error) {
        inputLayout.setErrorEnabled(true);
        inputLayout.setError(error);
    }

    //validation for fields
    private boolean validation() {
        disableError();
        boolean result = true;
        Pattern p = Pattern.compile("^[A-Za-z0-9_.-]{6,30}$");
        //Pattern pass = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z!@#$%^&*\\d]{1,12}");
        //Pattern oldpass = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z!@#$%^&*\\d]{8,12}");


        boolean userNameMatcher = p.matcher(etUserName.getText().toString().trim()).find();
        //boolean userPassMatcher = pass.matcher(etPassword.getText().toString().trim()).find();
        // boolean oldPassMatcher = oldpass.matcher(etOldPassword.getText().toString().trim()).find();

        if (etUserName.getText().toString().trim().isEmpty()) {
            enableError(userNameLayout, getResources().getString(R.string.username_empty_input));
            result = false;
        } else if (etUserName.getText().toString().trim().length() < 6) {
            enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_atleast));
            result = false;
        } else if (!userNameCorrect) {
            enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_name_exists));
            result = false;
        } else if (!userNameMatcher) {
            enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_atleast));
            result = false;
        }
        if (etOldPassword.getText().toString().trim().isEmpty()) {
            enableError(etOldPasswordLayout, getResources().getString(R.string.old_password_empty_input));
            result = false;
        } else if (!etOldPassword.getText().toString().trim().equalsIgnoreCase(getIntent().getStringExtra(RequestParameters.PASSWORD))) {
            enableError(etOldPasswordLayout, getResources().getString(R.string.old_password_input_match));
            result = false;
        }
        if (etPassword.getText().toString().trim().isEmpty()) {
            enableError(passwordLayout, getResources().getString(R.string.password_empty_input));
            result = false;
        } /*else if (!userPassMatcher) {
            enableError(passwordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
            result = false;
        } */else if (etPassword.getText().toString().trim().length() < 6) {
            enableError(passwordLayout, getResources().getString(R.string.password_incorrect_input_atleast));
            result = false;
        }
        if (etCPassword.getText().toString().trim().isEmpty()) {
            enableError(cPasswordLayout, getResources().getString(R.string.password_empty_input));
            result = false;
        } else if (!etPassword.getText().toString().trim().equals(etCPassword.getText().toString().trim())) {
            enableError(cPasswordLayout, getResources().getString(R.string.confirm_password_input_match));
            result = false;
        }
        return result;
    }

    //get response from service
    public void getResponse(JSONObject outPut, int type) {
        try {
            tvRegister.setVisibility(VISIBLE);
            pbLoading.setVisibility(GONE);
            setVisible();
            if (type == 0) { // change login info response
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(cvRegister, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        if (outPut.has(ResponseParameters.Success)) {
                            if (outPut.getString(ResponseParameters.Success).equalsIgnoreCase("success")) {
                                //SharedPreferencesMethod.setUserDetails(this, user);
                                Intent intent = new Intent(context, MyProfileActivity.class);// open first login info screen
                                intent.putExtra(ResponseParameters.Userdata, user);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                this.finish();
                            } else {
                                final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                                snackbar.show();
                            }
                        } else {
                            final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                            snackbar.show();
                        }
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { //error
                    final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { //no network
                    final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            } else if (type == 1) { // check user name service response
                llSuggestion.setVisibility(GONE);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(RequestParameters.USERNAME)) {
                        if (outPut.getString(RequestParameters.USERNAME).trim().equalsIgnoreCase(getIntent().getStringExtra(RequestParameters.USERNAME))) {
                            //check user name is same as old
                            userNameLayout.setErrorEnabled(false);
                            ivWrong.setVisibility(GONE);
                            ivCorrect.setVisibility(VISIBLE);
                            pbUserName.setVisibility(GONE);
                            userNameCorrect = true;
                        } else {
                            if (outPut.getString(RequestParameters.USERNAME).equalsIgnoreCase(etUserName.getText().toString().trim())) {
                                //check if username response is same as entered text then handle response
                                if (outPut.has(ResponseParameters.Success)) {
                                    ivCorrect.setVisibility(VISIBLE);
                                    ivWrong.setVisibility(GONE);
                                    pbUserName.setVisibility(GONE);
                                    userNameCorrect = true;
                                    userNameLayout.setErrorEnabled(false);
                                } else {
                                    ivWrong.setVisibility(VISIBLE);
                                    ivCorrect.setVisibility(GONE);
                                    pbUserName.setVisibility(GONE);
                                    userNameCorrect = false;
                                    if (outPut.has(ResponseParameters.SUGGESTIONS)) {
                                        showSuggestions(outPut.getJSONArray(ResponseParameters.SUGGESTIONS));
                                    }
                                    enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_name_exists));
                                }
                            } else {
                                if (isUserNameLoading) {
                                    pbUserName.setVisibility(VISIBLE);
                                    ivWrong.setVisibility(GONE);
                                    ivCorrect.setVisibility(GONE);
                                }
                                userNameCorrect = false;
                            }
                        }
                    } else { // error response
                        ivWrong.setVisibility(VISIBLE);
                        ivCorrect.setVisibility(GONE);
                        pbUserName.setVisibility(GONE);
                        userNameCorrect = false;
                        if (outPut.has(ResponseParameters.SUGGESTIONS)) {
                            showSuggestions(outPut.getJSONArray(ResponseParameters.SUGGESTIONS));
                        }
                        enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_name_exists));
                    }
                } else { // error response
                    ivWrong.setVisibility(VISIBLE);
                    ivCorrect.setVisibility(GONE);
                    pbUserName.setVisibility(GONE);
                    userNameCorrect = false;
                    if (outPut.has(ResponseParameters.SUGGESTIONS)) {
                        showSuggestions(outPut.getJSONArray(ResponseParameters.SUGGESTIONS));
                    }
                    enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_name_exists));
                }
                Pattern p = Pattern.compile("^[A-Za-z0-9_.-]{6,30}$");
                boolean userNameMatcher = p.matcher(etUserName.getText().toString().trim()).find();
                //regular expression check
                if (!userNameMatcher) {
                    ivWrong.setVisibility(VISIBLE);
                    ivCorrect.setVisibility(GONE);
                    pbUserName.setVisibility(GONE);
                    userNameCorrect = false;
                    enableError(userNameLayout, getResources().getString(R.string.username_incorrect_input_atleast));
                }


                isUserNameLoading = false;
                if (etUserName.getText().toString().trim().equalsIgnoreCase(getIntent().getStringExtra(RequestParameters.USERNAME))) {
                    llSuggestion.setVisibility(GONE);
                    userNameCorrect = true;
                    userNameLayout.setErrorEnabled(false);
                    ivWrong.setVisibility(GONE);
                    ivCorrect.setVisibility(VISIBLE);
                    pbUserName.setVisibility(GONE);
                }
            }
        } catch (Exception e) {
            final Snackbar snackbar = Snackbar.make(cvRegister, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
            snackbar.show();
            e.printStackTrace();
        }
    }

    //enable all edit text
    void setVisible() {
        etPassword.setEnabled(true);
        etUserName.setEnabled(true);
        etCPassword.setEnabled(true);
        etOldPassword.setEnabled(true);
        cvRegister.setClickable(true);
    }

    void showSuggestions(JSONArray suggestions) {
        try {
            if (suggestions.length() > 0) {
                llSuggestion.setVisibility(VISIBLE);
                String temp = "";
                for (int i = 0; i < suggestions.length(); i++) {
                    temp = temp + suggestions.getString(i) + "  ";
                }
                Log.e("TAG", "" + temp);
                if (tvUserNames.getText().toString().isEmpty())
                    tvUserNames.setText(temp.trim());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cv_register: // update click
                if (validation()) { // check validation
                    try {
                        if (!Utility.isConnectingToInternet(context)) { // check net connection
                            final Snackbar snackbar = Snackbar.make(v, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        HashMap<String, Object> input = new HashMap<>();
                        input.put(RequestParameters.USERID, "" + user.getUserId());
                        input.put(RequestParameters.USERNAME, "" + etUserName.getText().toString().trim());
                        input.put(RequestParameters.OLD_PASSWORD, "" + etOldPassword.getText().toString().trim());
                        input.put(RequestParameters.PASSWORD, "" + etPassword.getText().toString().trim());
                        input.put(RequestParameters.CONFIRM_PASSWORD, "" + etPassword.getText().toString().trim());
                        tvRegister.setVisibility(GONE);
                        pbLoading.setVisibility(VISIBLE);
                        cvRegister.setClickable(false);
                        etPassword.setEnabled(false);
                        etUserName.setEnabled(false);
                        etOldPassword.setEnabled(false);
                        API.sendRequestToServerPOST_PARAM(context, API.CHANGE_LOGIN_INFO, input); // service call for update info
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }
}
