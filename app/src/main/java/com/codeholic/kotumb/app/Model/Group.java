package com.codeholic.kotumb.app.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DELL on 11/1/2017.
 */

public class Group implements Serializable {
    @SerializedName("groupId")
    public String groupId = "";

    @SerializedName("isMember")
    public String isMember = "";

    @SerializedName("userId")
    public String userId = "";

    @SerializedName("groupName")
    public String groupName = "";

    @SerializedName("groupSummary")
    public String groupSummary = "";

    @SerializedName("delete_status")
    public String delete_status = "";

    @SerializedName("owner_type")
    public String owner_type = "";

    @SerializedName("privacy")
    public String privacy = "";

    @SerializedName("createdAt")
    public String createdAt = "";

    @SerializedName("updatedAt")
    public String updatedAt = "";

    @SerializedName("category")
    public String category = "";

    @SerializedName("category_name")
    public String category_name = "";

    @SerializedName("role")
    public String role = "";

    @SerializedName("status")
    public String status = "";

    @SerializedName("members_count")
    public int members_count = 0;

    @SerializedName("admins_count")
    public int admins_count = 1;

    @SerializedName("logo")
    public String logo = "";

}
