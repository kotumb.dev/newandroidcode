package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bumptech.glide.Glide;
import com.codeholic.kotumb.app.Activity.OtherUserProfileActivity;
import com.codeholic.kotumb.app.Activity.ShowVideos;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.Model.VideoData;
import com.codeholic.kotumb.app.Model.VideoLikes;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.View.DividerItemDecoration;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSink;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.codeholic.kotumb.app.Utility.API.imageUrl;

public class PublishVideosAdapter extends RecyclerView.Adapter<PublishVideosAdapter.VideoViewHolder> {
    List<VideoData> videoDataList;
    Activity activity;
    Context context;
    AlertDialog alert;
    TextView dialog_termsContionsHeading,dialog_termsConditionText;
    WebView dialog_web_view;
    RelativeLayout dialog_headerLayout;
    ImageView dismiss_dialog;
    LinearLayout dialog_llLoading,dialog_termsContionsLayout;
    RecyclerView rv_like_list;
    RecyclerView.ViewHolder viewHolder1;
    FrameLayout frameLayout;
    PlayerView fullscreeplay;
    SimpleExoPlayer fullplayer;
    ProgressBar progressBar;
    String id="";
    public ArrayList<VideoViewHolder> arrayList=new ArrayList<>();


    public  PublishVideosAdapter(Activity activity, List<VideoData> videoDataList,Context context) {
        this.activity = activity;
        this.videoDataList = videoDataList;
        this.context = context;
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull VideoViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if(holder instanceof VideoViewHolder){
            VideoViewHolder holder1 = (VideoViewHolder)holder;
            if(holder1.player!=null){
                holder1.player.release();
                holder1.player=null;
                holder1.videoThumbnailFrame.setVisibility(View.VISIBLE);
                holder1.play_icon.setVisibility(View.VISIBLE);
            }
        }
    }


    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_publish_videos, parent, false);
     //   PublishVideosAdapter.VideoViewHolder GroupViewHolder = new PublishVideosAdapter.VideoViewHolder(view);
        return new VideoViewHolder(view);
    }



    @Override
    public void onBindViewHolder(VideoViewHolder viewHolder, final int position) {
        final VideoData videoData = this.videoDataList.get(position);
        viewHolder1=viewHolder;
        if (viewHolder instanceof VideoViewHolder) {
            final VideoViewHolder holder = (VideoViewHolder) viewHolder;
            System.out.println("Video List Details   "+videoData.getVideoName()+"   "+videoData.getVideoTitle()+"   "+videoData.getVideoDescription()+"   "+videoData.getVideoStatus());
            setVisibility(holder);//set all visibilty
            holder.tv_userName.setText(videoData.getUserFirstName()+" "+videoData.getUserLastName());
            //holder.tv_videoTitle.setText(videoData.getVideoTitle());
            holder.tv_likes_count.setText(videoData.getVideoLikeCount());
            holder.video_id=videoData.getVideoId();
            holder.tv_videoTitle.setText(context.getResources().getString(R.string.video_title_heading)+": "+videoData.getVideoTitle());
            if(videoData.getVideoDescription().length()>0) {
                holder.tv_videoDescription.setText(context.getResources().getString(R.string.video_description_text_1) + " " + videoData.getVideoDescription());
            }
            else holder.tv_videoDescription.setVisibility(View.GONE);
            String path=API.user_videos+videoData.getVideoThumbnail();
            System.out.println("Path123======="+path);
            path=path.trim().toString();
            Glide.with(context).load(path).into(holder.videoThumbnail);
            holder.llEditPost.setVisibility(View.GONE);
            //Bitmap thumb = ThumbnailUtils.createVideoThumbnail(videoData.getVideoThumbnail(), MediaStore.Video.Thumbnails.MINI_KIND);
            // holder.videoThumbnail.setImageBitmap(thumb);
            //new AQuery(context).id(holder.userAvatar).image(API.imageUrl + videoData.getUserAvatar(), true, true, 300, R.drawable.default_profile);

            setOnClick(holder,videoData,position);//all set onClick listners
            if(Integer.parseInt(videoData.getVideoLikeCount())>0)
            holder.tv_likes_count.setText(videoData.getVideoLikeCount()+" "+context.getResources().getString(R.string.video_likes));
            else holder.tv_likes_count.setVisibility(View.GONE);

            if(videoData.getLike().equals("1")){
                Drawable img = context.getResources().getDrawable(R.drawable.ic_thumb_up_liked);
                holder.tv_Like.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                holder.tv_Like.startAnimation(Utility.showAnimtion(context));
                holder.tv_Like.setText(context.getResources().getString(R.string.video_unlike));
            }

         /*   final String url=API.VIDEO_LIKES+videoData.getVideoId()+"/"+SharedPreferencesMethod.getUserId(activity)+"/"+0;
            new AQuery(context).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
                @Override
                public void callback(String url, JSONObject object, AjaxStatus status) {
                    super.callback(url, object, status);
                    System.out.println("Video Like Response  "+object.toString());
                    try{
                        if (object.has(ResponseParameters.Success)){
                            object.getString("like_count");
                            int totalLikes=Integer.parseInt(object.getString("like_count"));
                            holder.tv_likes_count.setText("Likes: "+String.valueOf(totalLikes));
                        }
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }

                }
            });*/
        }


    }




    private void setVisibility(VideoViewHolder holder){
        holder.videoContainer.setVisibility(View.GONE);
        holder.videoFullScreenPlayer.setVisibility(View.VISIBLE);
        holder.playerContainer.setVisibility(View.GONE);
        holder.cross_icon.setVisibility(View.GONE);
        holder.publish_status.setVisibility(View.GONE);
        holder.tv_video_status.setVisibility(View.GONE);
        holder.videoStatusLogo.setVisibility(View.GONE);
        holder.publishVideologo.setVisibility(View.GONE);
       // holder.video_like.setVisibility(View.GONE);
//        holder.tv_videoTitle.setVisibility(View.VISIBLE);
        holder.videoThumbnailFrame.setVisibility(View.VISIBLE);
    }

    private void setFullplayer(Uri url) {
        new File(context.getCacheDir(), "media").delete();
        fullplayer=null;
        if (fullplayer == null) {
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            fullplayer= ExoPlayerFactory.newSimpleInstance(activity, trackSelector);
            MediaSource audioSource = new ExtractorMediaSource(url,
                    new PublishVideosAdapter.CacheDataSourceFactory(context, 100 * 1024 * 1024, 5 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
            fullplayer.setPlayWhenReady(true);
            fullplayer.prepare(audioSource);
            fullscreeplay.setPlayer(fullplayer);
            fullplayer.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
//                    holder.player.stop();
                }

                @Override
                public void onLoadingChanged(boolean isLoading) {

                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    switch (playbackState) {
                        case Player.STATE_BUFFERING:
                            progressBar.setVisibility(View.VISIBLE);
                            break;
                        case Player.STATE_ENDED:

                            break;
                        case Player.STATE_IDLE:

                            break;
                        case Player.STATE_READY:
                            progressBar.setVisibility(View.GONE);
                            break;
                        default:
                            break;
                    }
                    if (playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED ||
                            !playWhenReady) {

                        fullscreeplay.setKeepScreenOn(false);
                    } else { // STATE_IDLE, STATE_ENDED
                        // This prevents the screen from getting dim/lock
                        fullscreeplay.setKeepScreenOn(true);
                    }
                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {
                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {
                }
            });
        }
    }







    private void setOnClick(final VideoViewHolder holder, final VideoData videoData, final int position){

/*
        if (videoData.getVideoStatus().equalsIgnoreCase("2")){
            holder.llEditPost.setVisibility(View.GONE);
            holder.publish_status.setText("Publish Status: "+context.getResources().getString(R.string.reject_text));
            holder.tv_video_status.setText(context.getResources().getString(R.string.video_status_text)+context.getResources().getString(R.string.reject_text));
            holder.videoStatusLogo.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_cross));
            holder.publishVideologo.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_cross));
        }
        else if(videoData.getVideoApproved().equals("1")){
            holder.tv_video_status.setText(context.getResources().getString(R.string.video_status_text)+" "+context.getResources().getString(R.string.video_approved_text));
            holder.publish_status.setText("Publish Status: "+context.getResources().getString(R.string.pending_text));
            holder.videoStatusLogo.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_check_mark));
            holder.publishVideologo.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.pending));
            holder.llEditPost.setVisibility(View.GONE);
            holder.video_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.video_share.startAnimation(Utility.showAnimtion(activity));
                    SharedPreferencesMethod.setString(activity,"VID_ID",videoData.getVideoId());
                    Utils.showVideoSharePopUpMenu(holder.video_share,videoData.getVideoId(), videoDataList.get(position), videoData.getVideoId(), activity);
                }
            });
        }
        else {
            holder.llEditPost.setVisibility(View.VISIBLE);
            holder.publishVideologo.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.pending));
            holder.publish_status.setText("Publish Status: "+context.getResources().getString(R.string.pending_text));
            holder.tv_video_status.setText(context.getResources().getString(R.string.video_status_text)+context.getResources().getString(R.string.pending_text));
            holder.videoStatusLogo.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.pending));
            holder.llEditPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.llEditPost.startAnimation(Utility.showAnimtion(activity));
                    showPopup(videoData,position);
                }
            });
        }

        if(videoData.getVideoPublish().equals("2")){
            holder.publishVideologo.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_check_mark));
            holder.publish_status.setText("Publish Status:  Video Published");
        }
*/

        holder.video_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.video_share.startAnimation(Utility.showAnimtion(activity));
                SharedPreferencesMethod.setString(activity,"VID_ID",videoData.getVideoId());
                Utils.showVideoSharePopUpMenu(holder.video_share,videoData.getVideoId(), videoDataList.get(position), videoData.getVideoId(), activity);
            }
        });
        // new AQuery(context).id(holder.userAvatar).image(imageUrl + videoData.getUserAvatar(), true, true, 300, R.drawable.default_profile);
        // new AQuery(context).id(holder.userAvatar).image(user_videos + videoData.getVideoThumbnail(), true, true, 300, R.drawable.default_profile);

        Glide.with(context).load(imageUrl + videoData.getUserAvatar()).into(holder.userAvatar);

        holder.tvUploadTime.setText(videoData.getVideoUploadedDate());
        final Uri uri=Uri.parse(API.user_videos+videoData.getVideoName());
        holder.videoThumbnailFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.videoFullScreenPlayer.setVisibility(View.VISIBLE);
                pauseVideo();
                holder.playerContainer.setVisibility(View.VISIBLE);
                System.out.println("Video URI  "+uri);
                playVideo(uri,holder);
                holder.play_icon.setVisibility(View.GONE);
                holder.iv_allow_fullscreen.setVisibility(View.VISIBLE);
                holder.videoThumbnailFrame.setVisibility(View.GONE);
            }
        });


        holder.tv_likes_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LikePopup(context,videoData.getVideoId());
            }
        });

        holder.tv_userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user=new User();
                user.setFirstName(videoData.getUserFirstName());
                user.setLastName(videoData.getUserFirstName());
                user.setUserId(videoData.getUserId());
                Intent intent=new Intent(activity, OtherUserProfileActivity.class);
                intent.putExtra("USER",user);
                activity.startActivity(intent);
            }
        });


        holder.video_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(activity,videoData.getUserId()+"==="+SharedPreferencesMethod.getUserInfo(activity).getUserId(),Toast.LENGTH_LONG).show();
                if(videoData.getUserId().equals(SharedPreferencesMethod.getUserInfo(activity).getUserId())){
                  //  Toast.makeText(activity,"Equal======",Toast.LENGTH_LONG).show();
                    Intent intent=new Intent();
                    intent.setAction(ResponseParameters.NOTIFICATIONS);
                    intent.putExtra("refresh","");
                    activity.sendBroadcast(intent);
                }
                 if (holder.tv_Like.getText().toString().equalsIgnoreCase("Like")){
                    String url=API.VIDEO_LIKES+videoData.getVideoId()+"/"+ SharedPreferencesMethod.getUserId(context)+"/"+1;
                    System.out.println("Video Like URL   "+url);
                    Drawable img = context.getResources().getDrawable(R.drawable.ic_thumb_up_liked);
                     holder.tv_Like.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                     holder.tv_Like.startAnimation(Utility.showAnimtion(context));
                     new AQuery(context).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
                         @Override
                         public void callback(String url, JSONObject object, AjaxStatus status) {
                             super.callback(url, object, status);
                             System.out.println("Video Like Response  "+object.toString());
                             try{
                                 if (object.has(ResponseParameters.Success)){
                                     object.getString("like_count");
                                     int totalLikes=Integer.parseInt(object.getString("like_count"));
                                     if(totalLikes>0) {
                                         holder.tv_likes_count.setVisibility(View.VISIBLE);
                                         holder.tv_likes_count.setText( String.valueOf(totalLikes)+" Likes");
                                     }
                                     else holder.tv_likes_count.setVisibility(View.GONE);
                                 }
                             }catch (Exception ex){
                                 ex.printStackTrace();
                             }

                         }
                     });
                     holder.tv_Like.setText(context.getResources().getString(R.string.video_unlike));
                }else{
                    String url=API.VIDEO_LIKES+videoData.getVideoId()+"/"+ SharedPreferencesMethod.getUserId(context)+"/"+0;
                    System.out.println("Video Like URL   "+url);
                    Drawable img = context.getResources().getDrawable(R.drawable.ic_thumb_up);
                     holder.tv_Like.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                     holder.tv_Like.startAnimation(Utility.showAnimtion(context));
                     new AQuery(context).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
                         @Override
                         public void callback(String url, JSONObject object, AjaxStatus status) {
                             super.callback(url, object, status);
                             System.out.println("Video Like Response  "+object.toString());
                             try{
                                 if (object.has(ResponseParameters.Success)){
                                     object.getString("like_count");
                                     int totalLikes=Integer.parseInt(object.getString("like_count"));
                                     if(totalLikes>0) {
                                         holder.tv_likes_count.setVisibility(View.VISIBLE);
                                         holder.tv_likes_count.setText( String.valueOf(totalLikes)+" Likes");
                                     }
                                     else holder.tv_likes_count.setVisibility(View.GONE);
                                 }
                             }catch (Exception ex){
                                 ex.printStackTrace();
                             }

                         }
                     });
                     holder.tv_Like.setText(context.getResources().getString(R.string.video_like));
                }
             /*   final String url=API.VIDEO_LIKES+videoData.getVideoId()+"/"+SharedPreferencesMethod.getUserId(activity)+"/"+1;
                new AQuery(context).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        super.callback(url, object, status);
                        System.out.println("Video Like Response  "+object.toString());
                        try{
                            if (object.has(ResponseParameters.Success)){
                                object.getString("like_count");
                                int totalLikes=Integer.parseInt(object.getString("like_count"))+Integer.parseInt(videoData.getVideoLikeCount());
                                holder.tv_likes_count.setText(String.valueOf(totalLikes));
                            }
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }

                    }
                });*/
            }
        });




        holder.iv_allow_fullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                holder.playerContainer.setVisibility(View.VISIBLE);
                holder.addFullScreen.setVisibility(View.GONE);
                showfullScreenPopup(videoData,holder);
                holder.play_icon.setVisibility(View.GONE);
            }
        });


    }


    private void LikePopup(final Context activity,String videoId){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.video_terms_condition_layout, null);
        dialogBuilder.setView(dialogView);
        dialogfindViews(dialogView);
        FrameLayout frameLayout=dialogView.findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);
        dialog_web_view.setVisibility(View.GONE);
        dialog_headerLayout.setVisibility(View.VISIBLE);
        dialog_termsConditionText.setVisibility(View.GONE);
        dialog_termsContionsLayout.setVisibility(View.VISIBLE);
        dialog_termsContionsHeading.setText("User Like List");
        rv_like_list.setVisibility(View.VISIBLE);
        userLikesListServiceCall(videoId,activity);
        if(context instanceof  ShowVideos)
        ((ShowVideos)context).showpop(dialogBuilder,dismiss_dialog);
        /*final AlertDialog confirmAlert = dialogBuilder.create();
        dismiss_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();*/
    }

    List<VideoLikes> videoLikesList=new ArrayList<VideoLikes>();
    VideoLikeListAdapter videoLikeListAdapter;

    private void userLikesListServiceCall(final String videoId, final Context activity){


        dialog_llLoading.setVisibility(View.VISIBLE);
        videoLikesList.clear();
        String url=API.VIDEO_LIKES_LIST+videoId;
        System.out.println("Video Delete URL   "+url);
        new AQuery(activity).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject outPut, AjaxStatus status) {
                        super.callback(url, outPut, status);
                        dialog_llLoading.setVisibility(View.GONE);
                        System.out.println("Video Like List Response   "+outPut.toString());
                        try{
                            for (int j=0; j<outPut.getJSONArray("likes").length(); j++){
                                JSONObject likeObj = outPut.getJSONArray("likes").getJSONObject(j);
                                VideoLikes videoLikes=new VideoLikes();
                                videoLikes.setLike_date(likeObj.getString(ResponseParameters.LIKED_AT));
                                videoLikes.setLike_id(likeObj.getString(ResponseParameters.LIKE_ID));
                                videoLikes.setUser_id(likeObj.getString("userId"));
                                videoLikes.setVideo_avatar(likeObj.getString(ResponseParameters.Avatar));
                                videoLikes.setVideo_first_name(likeObj.getString(ResponseParameters.FirstName));
                                videoLikes.setVideo_last_name(likeObj.getString(ResponseParameters.LastName));
                                videoLikesList.add(videoLikes);
                            }
                            LinearLayoutManager linearLayoutManager;
                            linearLayoutManager = new LinearLayoutManager(activity);
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rv_like_list.setLayoutManager(linearLayoutManager);
                            videoLikeListAdapter = new VideoLikeListAdapter(rv_like_list, activity, videoLikesList);
                            rv_like_list.addItemDecoration(
                                    new DividerItemDecoration(activity, R.drawable.divider));
                            rv_like_list.setAdapter(videoLikeListAdapter);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }
                }
        );
    }


    public void stopVideo(){
        if(fullplayer!=null){
            fullplayer.release();
        }
        for(int i=0;i<arrayList.size();i++){
            if(arrayList.get(i).player!=null)
                arrayList.get(i).player.stop();
        }
    }

    public void pauseVideo(){
        for(int i=0;i<arrayList.size();i++){
            if(arrayList.get(i).player!=null)
                arrayList.get(i).player.setPlayWhenReady(false);
        }
    }
    public void pauseAllVideo(){
        if(fullplayer!=null){
            fullplayer.setPlayWhenReady(false);
        }
        for(int i=0;i<arrayList.size();i++){
            if(arrayList.get(i).player!=null)
                arrayList.get(i).player.setPlayWhenReady(false);
        }
    }





    @Override
    public int getItemCount () {
        return videoDataList.size();
    }

    private void initializePlayer(final VideoViewHolder holder, Uri url) {
        new File(context.getCacheDir(), "media").delete();

        if (holder.player == null) {

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            holder.player = ExoPlayerFactory.newSimpleInstance(activity, trackSelector);
            MediaSource audioSource = new ExtractorMediaSource(url,
                    new PublishVideosAdapter.CacheDataSourceFactory(context, 100 * 1024 * 1024, 5 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
            holder.player.setPlayWhenReady(true);
            holder.player.prepare(audioSource);

            holder.videoFullScreenPlayer.setPlayer(holder.player);
            holder.player.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
//                    holder.player.stop();
                }

                @Override
                public void onLoadingChanged(boolean isLoading) {
                    if(isLoading){
                        holder.pbLoadVdo.setVisibility(View.VISIBLE);
                    }
                    else holder.pbLoadVdo.setVisibility(View.GONE);
                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    switch (playbackState) {
                        case Player.STATE_BUFFERING:
                            holder.pbLoadVdo.setVisibility(View.VISIBLE);
                            break;
                        case Player.STATE_ENDED:

                            break;
                        case Player.STATE_IDLE:

                            break;
                        case Player.STATE_READY:
                            holder.pbLoadVdo.setVisibility(View.GONE);


                            break;
                        default:
                            break;
                    }
                    if(playWhenReady){
                        id=holder.video_id;
                        for(int i=0;i<arrayList.size();i++){
                            if(!id.equals(arrayList.get(i).video_id)){
                                System.out.println("id==="+arrayList.get(i).video_id);
                                if(arrayList.get(i).player!=null)
                                    arrayList.get(i).player.setPlayWhenReady(false);
                            }
                        }
                    }
                    if (playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED ||
                            !playWhenReady) {

                        holder.videoFullScreenPlayer.setKeepScreenOn(false);
                    } else { // STATE_IDLE, STATE_ENDED
                        // This prevents the screen from getting dim/lock
                        holder.videoFullScreenPlayer.setKeepScreenOn(true);
                    }
                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {
                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {
                }
            });

        }
    }



    private void playVideo(Uri videoUri, VideoViewHolder holder) {
        arrayList.add(holder);
        initializePlayer(holder,videoUri);
        if (videoUri == null) {
            return;
        }

    }


    private void showPopup(final VideoData videoData, final int position){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_group_post, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llDeletePost = dialogView.findViewById(R.id.llDeletePost);
        llDeletePost.setVisibility(View.VISIBLE);
        llDeletePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUpDelete("Are You Sure You Want To Delete The Video",position,videoData);
            }
        });
        alert = dialogBuilder.create();
        alert.setCancelable(true);
        alert.show();
    }



    private void deleteServiceCall(VideoData video,final int position){
        String url=API.VIDEO_DELETE+"/"+ SharedPreferencesMethod.getUserId(activity)+"/"+video.getVideoId();
        System.out.println("Video Delete URL   "+url);
        new AQuery(context).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject object, AjaxStatus status) {
                        super.callback(url, object, status);
                        System.out.println("Delete Video Response   "+object.toString());
                        notifyItemRemoved(position);
                        videoDataList.remove(position);
                        notifyItemRangeChanged(0, videoDataList.size());
                        Utils.showPopup(activity, activity.getString(R.string.post_delete_success_msg));
                    }
                }
        );
    }



    private void dialogfindViews(View dialogView){
        dialog_web_view = dialogView.findViewById(R.id.web_view);
        dialog_termsContionsLayout = dialogView.findViewById(R.id.condition_message_layout);
        dialog_termsContionsHeading = dialogView.findViewById(R.id.condtion_heading);
        dialog_termsConditionText=dialogView.findViewById(R.id.condtion_text);
        dialog_headerLayout=dialogView.findViewById(R.id.header_layout);
        dismiss_dialog=dialogView.findViewById(R.id.dismiss_dialog);
        rv_like_list=dialogView.findViewById(R.id.rv_user_like_list);
        dialog_llLoading=dialogView.findViewById(R.id.llLoading);
        fullscreeplay=dialogView.findViewById(R.id.fullscreeplay);
        frameLayout=dialogView.findViewById(R.id.frameLayout);
        progressBar=dialogView.findViewById(R.id.video_loader);
    }

    private void showfullScreenPopup(final VideoData videoData, final VideoViewHolder holder) {
        pauseVideo();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.video_terms_condition_layout, null);
        dialogBuilder.setView(dialogView);
        dialogfindViews(dialogView);
        dialog_termsContionsLayout.setVisibility(View.GONE);
        dialog_termsContionsHeading.setVisibility(View.GONE);
        dialog_termsConditionText.setVisibility(View.GONE);
        dialog_headerLayout.setVisibility(View.GONE);
        rv_like_list.setVisibility(View.GONE);
        dialog_llLoading.setVisibility(View.GONE);
        dialog_web_view.setVisibility(View.GONE);
        setFullplayer(Uri.parse(API.user_videos+videoData.getVideoName()));

        //activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        final AlertDialog confirmAlert = dialogBuilder.create();
        confirmAlert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                holder.addFullScreen.setVisibility(View.VISIBLE);
                holder.videoThumbnailFrame.setVisibility(View.VISIBLE);
                holder.playerContainer.setVisibility(View.VISIBLE);
                if(fullplayer!=null){
                    fullplayer.stop();
                }
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();
    }








    public class CacheDataSourceFactory implements DataSource.Factory {
        private final Context context;
        private final DefaultDataSourceFactory defaultDatasourceFactory;
        private final long maxFileSize, maxCacheSize;

        CacheDataSourceFactory(Context context, long maxCacheSize, long maxFileSize) {
            super();
            this.context = context;
            this.maxCacheSize = maxCacheSize;
            this.maxFileSize = maxFileSize;
            String userAgent = Util.getUserAgent(context, context.getString(R.string.app_name));
            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            defaultDatasourceFactory = new DefaultDataSourceFactory(this.context,
                    bandwidthMeter,
                    new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter));
        }


        @Override
        public DataSource createDataSource() {
            LeastRecentlyUsedCacheEvictor evictor = new LeastRecentlyUsedCacheEvictor(maxCacheSize);
            SimpleCache simpleCache = new SimpleCache(new File(context.getCacheDir(), "media"), evictor);
            return new CacheDataSource(simpleCache, defaultDatasourceFactory.createDataSource(),
                    new FileDataSource(), new CacheDataSink(simpleCache, maxFileSize),
                    CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR, null);
        }
    }


    private void showPopUpDelete(String title, final int position, final VideoData videoData) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        tvTitle.setText(title);
        final AlertDialog deleteAlret = dialogBuilder.create();
        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteServiceCall(videoData,position);
                deleteAlret.dismiss();
                alert.dismiss();
            }
        });
        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlret.dismiss();
                alert.dismiss();
            }
        });
        deleteAlret.setCancelable(true);
        deleteAlret.show();
    }




    public static class VideoViewHolder extends RecyclerView.ViewHolder {
        TextView tv_videoDescription, tv_videoTitle, tv_userName,tvUploadTime,tv_video_status,publish_status,tv_likes_count,tv_Like;
        VideoView videoContainer;
        FrameLayout addFullScreen,videoThumbnailFrame;
        PlayerView videoFullScreenPlayer;
        SimpleExoPlayer player;
        ProgressBar pbLoadVdo;
        RelativeLayout playerContainer;
        LinearLayout video_like,lv_container,video_share;
        ImageView userAvatar,videoThumbnail,play_icon,cross_icon,iv_allow_fullscreen,videoStatusLogo,publishVideologo;
        LinearLayout llEditPost;
        String video_id="";
        View video_card;


        public VideoViewHolder(View view) {
            super(view);
            tv_videoDescription = view.findViewById(R.id.ls_video_description);
            videoContainer = view.findViewById(R.id.videoview);
            tv_Like=view.findViewById(R.id.tvLike);
            tv_videoTitle=view.findViewById(R.id.ls_video_title);
            userAvatar = view.findViewById(R.id.ivUserImage);
            videoThumbnail = view.findViewById(R.id.video_thumbnail);
            play_icon = view.findViewById(R.id.play_icon);
            cross_icon = view.findViewById(R.id.remove_video);
            tv_userName = view.findViewById(R.id.tvUserName);
            video_card=view.findViewById(R.id.video_card);
            videoFullScreenPlayer=view.findViewById(R.id.play_video);
            tv_video_status=view.findViewById(R.id.ls_video_status);
            pbLoadVdo=view.findViewById(R.id.video_loader);
            playerContainer=view.findViewById(R.id.play_video_container);
            tvUploadTime=view.findViewById(R.id.tvTime);
            addFullScreen=view.findViewById(R.id.main_media_frame);
            llEditPost=view.findViewById(R.id.llEditPost);
            video_share=view.findViewById(R.id.video_share);
            video_like=view.findViewById(R.id.video_like);
            iv_allow_fullscreen=view.findViewById(R.id.iv_allow_fullscreen);
            lv_container=view.findViewById(R.id.lv_container);
            tv_likes_count=view.findViewById(R.id.tv_likes_count);
            videoStatusLogo=view.findViewById(R.id.videoStatus_logo);
            publishVideologo=view.findViewById(R.id.publishVideoStatus_logo);
            publish_status=view.findViewById(R.id.ls_video_publish_status);
            videoThumbnailFrame=view.findViewById(R.id.videoThumbnailFrame);
        }
    }

}















