package com.codeholic.kotumb.app.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Adapter.ReportPostListRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.GroupMembers;
import com.codeholic.kotumb.app.Model.ReportPost;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ReportedPostActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;


    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.container)
    CoordinatorLayout container;

    @BindView(R.id.rvPostList)
    RecyclerView rvPostList;


    @BindView(R.id.cvPost)
    CardView cvPost;


    Context context;
    public Group group;
    User user;

    private BroadcastReceiver receiver;

    List<GroupMembers> members = new ArrayList<>();
    List<GroupMembers> member_requested = new ArrayList<>();
    List<GroupMembers> member_invited = new ArrayList<>();
    List<GroupMembers> member_blocked = new ArrayList<>();
    List<ReportPost> posts = new ArrayList<>();
    String USER_IMAGE_BASE_URL = "";
    int pagination = 0;
    private LinearLayoutManager linearLayoutManager = null;
    ReportPostListRecyclerViewAdapter reportPostListRecyclerViewAdapter;
    com.codeholic.kotumb.app.View.ProgressBar progressBar = null;
    int type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_reported_post);
        ButterKnife.bind(this);
        init();
        initToolbar();
        getGroupInfo();
        setClickListener();
    }

    private void getGroupInfo() {
        try {
            if (!Utility.isConnectingToInternet(context)) { // check net connection
                final Snackbar snackbar = Snackbar.make(rvPostList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(VISIBLE);
                return;
            }
            /*progressBar = new com.codeholic.kotumb.app.View.ProgressBar(this);
            progressBar.show(getResources().getString(R.string.app_please_wait_text));*/
            pagination = 0;
            API.sendRequestToServerGET(context, API.REPORTED_GROUP_POST + "/" + group.groupId + "/" + pagination, API.REPORTED_GROUP_POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickListener() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    private void init() {
        context = this;
        group = (Group) getIntent().getSerializableExtra(ResponseParameters.GROUP);
        user = getIntent().getParcelableExtra("USER");
        /*swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getAllPost();
            }
        });*/
        if (getIntent().hasExtra(ResponseParameters.TYPE)) {
            if (getIntent().getIntExtra(ResponseParameters.TYPE, 0) == 3) { // from public group tab
                cvPost.setVisibility(GONE);
                type = 3;
            } else {
                cvPost.setVisibility(VISIBLE);
            }
        }
    }

    private void getAllPost() {
        try {
            if (!Utility.isConnectingToInternet(context)) { // check net connection
                final Snackbar snackbar = Snackbar.make(rvPostList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }

            llLoading.setVisibility(VISIBLE);
            llNotFound.setVisibility(GONE);
            getGroupInfo();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //clear notification of message
    }

    public void hideLayout() {
        if (posts.size() <= 0)
            llNotFound.setVisibility(View.VISIBLE);
    }

    // initToolbar method initializing view
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.reported_post_list_title));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //getResponse for getting list of urlImages
    public void getResponse(JSONObject outPut, int i) {
        Log.e("response", "" + outPut);
        try {

            if (progressBar != null) {
                progressBar.dismiss(); // dismissing progressbar
            }

            if (i == 0) { // for getting urlImages message
                llLoading.setVisibility(GONE);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response


                        if (outPut.has(ResponseParameters.IMAGE_BASE_URL)) {
                            USER_IMAGE_BASE_URL = outPut.getString(ResponseParameters.IMAGE_BASE_URL);
                        }

                        if (outPut.has(ResponseParameters.POSTS_REPORTS)) {
                            Gson gson = new Gson(); // creates a Gson instance
                            Type listType = new TypeToken<List<ReportPost>>() {
                            }.getType();
                            posts = gson.fromJson(outPut.getString(ResponseParameters.POSTS_REPORTS), listType);

                           /* for(int j = 0; j <outPut.getJSONArray(ResponseParameters.GROUP_POSTS).length(); j++){
                                Log.e("TAG", ""+outPut.getJSONArray(ResponseParameters.GROUP_POSTS).getJSONObject(j).toString());
                            }*/


                            linearLayoutManager = new LinearLayoutManager(this);
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rvPostList.setLayoutManager(linearLayoutManager);
                            reportPostListRecyclerViewAdapter = new ReportPostListRecyclerViewAdapter(rvPostList, this, posts,user);
                            reportPostListRecyclerViewAdapter.setUserImageBaseUrl(USER_IMAGE_BASE_URL);
                            reportPostListRecyclerViewAdapter.setType(type);
                            reportPostListRecyclerViewAdapter.setGroupImageBaseUrl(getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL));
                            //rvPostList.setHasFixedSize(true);

                            rvPostList.setAdapter(reportPostListRecyclerViewAdapter);

                            reportPostListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                @Override
                                public void onLoadMore() {
                                    Log.e("load more", "load moe");
                                    if (posts.size() >= 5) {
                                        rvPostList.post(new Runnable() {
                                            public void run() {
                                                posts.add(null);
                                                reportPostListRecyclerViewAdapter.notifyItemInserted(posts.size() - 1);
                                            }
                                        });
                                        API.sendRequestToServerGET(ReportedPostActivity.this, API.REPORTED_GROUP_POST + group.groupId + "/" + (pagination + 5), API.REPORTED_GROUP_POST_UPDATE);// service call for getting messages
                                    }
                                }
                            });


                            if (posts.size() <= 0) {
                                llNotFound.setVisibility(View.VISIBLE);
                            }
                        } else {
                            llNotFound.setVisibility(View.VISIBLE);
                        }

                    } else {
                        //no userfound
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } else if (i == 1) { // for send message
                Log.e("output", "" + outPut);
                posts.remove(posts.size() - 1); // removing of loading item
                reportPostListRecyclerViewAdapter.notifyItemRemoved(posts.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                    } else if (outPut.has(ResponseParameters.Success) && outPut.has(ResponseParameters.POSTS_REPORTS)) { // success response
                        if (outPut.has(ResponseParameters.POSTS_REPORTS)) {
                            Gson gson = new Gson(); // creates a Gson instance
                            Type listType = new TypeToken<List<ReportPost>>() {
                            }.getType();
                            List<ReportPost> updatedPosts = gson.fromJson(outPut.getString(ResponseParameters.POSTS_REPORTS), listType);
                            posts.addAll(updatedPosts);

                            if (updatedPosts.size() > 0) {
                                pagination = pagination + 5;
                                //updating recycler view
                                reportPostListRecyclerViewAdapter.notifyDataSetChanged();
                                reportPostListRecyclerViewAdapter.setLoaded();
                            }


                        }
                    }
                }
            }

        } catch (Exception e) {
            //mo user found
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    protected void onDestroy() {

        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
