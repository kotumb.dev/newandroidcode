package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import com.google.android.material.snackbar.Snackbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Activity.ProfessionActivity;
import com.codeholic.kotumb.app.Model.Profession;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by DELL on 11/17/2017.
 */

public class ProfessionListAdapter extends RecyclerView.Adapter<ProfessionListAdapter.ProfessionViewHolder> {
    List<Profession> professions;
    Activity activity;
    public boolean menu = true;


    public ProfessionListAdapter(Activity activity, List<Profession> professions) {
        this.professions = professions;
        this.activity = activity;
    }

    @Override
    public int getItemCount() {
        return professions.size();
    }

    @Override
    public ProfessionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.professional_list_item, parent, false);
        ProfessionViewHolder professionViewHolder = new ProfessionViewHolder(view);
        return professionViewHolder;
    }


    @Override
    public void onBindViewHolder(ProfessionViewHolder holder, int position) {
        try {

            Profession profession = professions.get(position);
            if (!profession.getYearFrom().trim().equalsIgnoreCase("0000-00-00")) {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");
                if (profession.getIsCurrent().trim().equalsIgnoreCase("1")) {
                    holder.tvTime.setText(df_.format(df.parse(profession.getYearFrom().trim())) + " to " + activity.getResources().getString(R.string.profile_present_text).trim());
                } else {
                    holder.tvTime.setText(df_.format(df.parse(profession.getYearFrom().trim())) + " to " + df_.format(df.parse(profession.getYearTo().trim())));
                }
            } else {
                holder.tvTime.setText("");
            }
            makeTextViewResizable(profession,holder.tvProfessionDes, 5, "..Read More", true);
            holder.tvProfessionDes.setText(profession.getDescription().trim());
            holder.tvCompanyName.setText(profession.getCompanyName().trim());
            holder.tvProfessionName.setText(profession.getProfessionName().trim());
            System.out.println("Pro Details  "+profession.getDescription().trim());
            if (!profession.getDescription().equalsIgnoreCase("")){
                holder.tvProfessionDes.setText(profession.getDescription().trim());
            }

            if (menu) {
                holder.tvOptionMenu.setVisibility(View.VISIBLE);
                setOnClickListener(holder, profession, position);
            } else {
                holder.tvOptionMenu.setVisibility(View.GONE);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }



    public static class MySpannable extends ClickableSpan {

        private boolean isUnderline = true;

        /**
         * Constructor
         */
        public MySpannable(boolean isUnderline) {
            this.isUnderline = isUnderline;
        }

        @Override
        public void updateDrawState(TextPaint ds) {

            ds.setUnderlineText(isUnderline);
            ds.setColor(Color.rgb(150,52,78));

        }

        @Override
        public void onClick(View widget) {

        }
    }



    public void setOnClickListener(final ProfessionViewHolder holder, final Profession profession, final int position) {
        holder.tvOptionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(activity, holder.tvOptionMenu);
                //inflating menu from xml resource
                popup.inflate(R.menu.options_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.itemEdit:
                                Constants.profession_position = position;
                                Intent intent = new Intent(activity, ProfessionActivity.class);
                                intent.putExtra(RequestParameters.ORGANISATION, profession);
                                activity.startActivity(intent);
                                break;
                            case R.id.itemDelete:
                                onClickDelete(holder, position, profession);
                                break;

                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();
            }
        });
        holder.cvMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.profession_position = position;
                Intent intent = new Intent(activity, ProfessionActivity.class);
                intent.putExtra(RequestParameters.ORGANISATION, profession);
                activity.startActivity(intent);
            }
        });
    }

    private void onClickDelete(final ProfessionViewHolder holder, final int position, final Profession profession) {
        Utils.showPopup(activity, activity.getString(R.string.remove_item), activity.getString(R.string.delete_btn_text), activity.getString(R.string.cancel_btn_text), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delete(holder, position, profession);
            }
        }, null);
    }

    private void delete(ProfessionViewHolder holder, int position, Profession profession) {
        if (!Utility.isConnectingToInternet(activity)) {
            final Snackbar snackbar = Snackbar.make(holder.tvOptionMenu, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
            snackbar.show();
            return;
        }
        professions.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, professions.size());
        sendRequest(activity, API.DELETE_ORGANISATION + "" + SharedPreferencesMethod.getUserId(activity) + "/" + profession.getId());
    }


    public void sendRequest(final Activity context, String url) {
        try {
            AQuery aq = new AQuery(context);
            //Log.d("Url ", url);
            aq.ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {
                        if (json != null) {
                            Log.e(context.getClass().getName(), json.toString());
                            json.put("RP_MESSAGE", "ALL_OKAY");
                            getResponse(json);
                        } else {
                            JSONObject output = new JSONObject();
                            if (status.getCode() == AjaxStatus.NETWORK_ERROR) {
                                output.put("RP_MESSAGE", "NO NETWORK");
                            } else {
                                output.put("RP_MESSAGE", "ERROR");
                            }
                            getResponse(output);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        getResponse(new JSONObject());
                    }
                }
            }.method(AQuery.METHOD_GET).header("Auth-Key","KOTUMB__AuTh_keY"));


        } catch (Exception e) {
            e.printStackTrace();
            getResponse(new JSONObject());
        }
    }


    public void getResponse(JSONObject output) {
        try {
            if (output.has(ResponseParameters.Success)) {
//                Toast.makeText(activity, output.getString(ResponseParameters.Success), Toast.LENGTH_SHORT).show();
                Utils.showPopup(activity, output.getString(ResponseParameters.Success));
            } else {
//                Toast.makeText(activity, activity.getResources().getString(R.string.error_text), Toast.LENGTH_SHORT).show();
                Utils.showPopup(activity, activity.getResources().getString(R.string.error_text));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void makeTextViewResizable(final Profession profession,final TextView tv,final int maxLine, final String expandText, final boolean viewMore) {
        tv.setText(profession.getDescription());
        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0,
                            lineEndIndex - expandText.length() + 1)
                            + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(profession,tv.getText()
                                            .toString(), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0,
                            lineEndIndex - expandText.length() + 1)
                            + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(profession,tv.getText()
                                            .toString(), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(
                            tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex)
                            + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(profession,tv.getText()
                                            .toString(), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(
            final Profession profession,final String strSpanned, final TextView tv, final int maxLine,
            final String spanableText, final boolean viewMore) {
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);
        if (strSpanned.contains(spanableText)) {
            ssb.setSpan(
                    new ClickableSpan() {

                        @Override
                        public void onClick(View widget) {

                            if (viewMore) {
                                tv.setLayoutParams(tv.getLayoutParams());
                                tv.setText(tv.getTag().toString(),
                                        TextView.BufferType.SPANNABLE);
                                tv.invalidate();
                                makeTextViewResizable(profession,tv, -5, "..Read Less",
                                        false);
                                tv.setTextColor(Color.BLACK);
                                tv.setText(profession.getDescription());
                            } else {
                                tv.setLayoutParams(tv.getLayoutParams());
                                tv.setText(tv.getTag().toString(),
                                        TextView.BufferType.SPANNABLE);
                                tv.invalidate();

                                makeTextViewResizable(profession,tv, 5, "..Read More",
                                        true);
                                tv.setTextColor(Color.BLACK);
                                tv.setText(profession.getDescription());
                            }
                            tv.setText(profession.getDescription());
                        }
                    }, strSpanned.indexOf(spanableText),
                    strSpanned.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }






    public static class ProfessionViewHolder extends RecyclerView.ViewHolder {
        TextView tvProfessionName, tvCompanyName, tvTime, tvProfessionDes;
        ImageView ivImage;
        CardView cvMain;
        LinearLayout tvOptionMenu;

        ProfessionViewHolder(View view) {
            super(view);
            cvMain = (CardView) view.findViewById(R.id.cvMain);
            ivImage = (ImageView) view.findViewById(R.id.ivImage);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            tvProfessionName = (TextView) view.findViewById(R.id.tvProfessionName);
            tvCompanyName = (TextView) view.findViewById(R.id.tvCompanyName);
            tvProfessionDes = (TextView) view.findViewById(R.id.tvProfessionDes);
            tvOptionMenu = (LinearLayout) view.findViewById(R.id.tvOptionMenu);
        }
    }
}
