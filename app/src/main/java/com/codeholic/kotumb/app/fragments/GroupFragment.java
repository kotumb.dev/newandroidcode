package com.codeholic.kotumb.app.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Activity.CreateGroupActivity;
import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Adapter.GroupListRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.GroupMembers;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.View.DividerItemDecoration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shahroz.svlibrary.interfaces.onSearchListener;
import com.shahroz.svlibrary.interfaces.onSimpleSearchActionsListener;
import com.shahroz.svlibrary.widgets.MaterialSearchView;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class GroupFragment extends BaseFragment implements View.OnClickListener, onSimpleSearchActionsListener, onSearchListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;


    @BindView(R.id.faCreateGroup)
    FloatingActionButton faCreateGroup;

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private static SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    @BindView(R.id.container)
    ViewPager mViewPager;

    static TabLayout tabLayout;
    private boolean mSearchViewAdded = false;
    private MaterialSearchView mSearchView;
    private WindowManager mWindowManager;
    private MenuItem searchItem;
    private boolean searchActive = false;
    private boolean search = true;
    private String queryText = "";
    private PlaceholderFragment myGroup = null;
    private PlaceholderFragment publicGroup = null;
    private String latestQuery;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mWindowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        mSearchView = new MaterialSearchView(getActivity());
        mSearchView.setOnSearchListener(this);
        mSearchView.setSearchResultsListener(this);
        mSearchView.setHintText(getResources().getString(R.string.invitations_search));
        Log.e("onCreate", "InviteFragment");

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.groups_tab, container, false);
        ButterKnife.bind(this, view);
        mToolbar = getActivity().findViewById(R.id.toolbar_actionbar);

        tabLayout = view.findViewById(R.id.tabs);
        ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.header_main_group_title));
        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        mViewPager.setOffscreenPageLimit(3);
        Log.e("onResume", "InviteFragment");
        mViewPager.setAdapter(mSectionsPagerAdapter);
        faCreateGroup.setOnClickListener(this);
        tabLayout.setupWithViewPager(mViewPager);
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        tab = tabLayout.getTabAt(1);


        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (searchActive) {
                    searchActive = false;
                    mSearchView.hide();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if (mToolbar != null) {
            // Delay adding SearchView until Toolbar has finished loading
            mToolbar.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!mSearchViewAdded && mWindowManager != null) {
                            mWindowManager.addView(mSearchView,
                                    MaterialSearchView.getSearchViewLayoutParams(getActivity()));
                            mSearchViewAdded = true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }


        HomeScreenActivity activity = (HomeScreenActivity) getActivity();
        if (activity != null && activity.openPublicGroup) {
            mViewPager.setCurrentItem(1);
            activity.openPublicGroup = false;
        }

        return view;
    }


    @Override
    public void onSearch(String query) {
        Log.e("onSearch", "onSearch " + searchActive);
        if (search) {
            this.latestQuery = query;
            last_text_edit = System.currentTimeMillis();
            h.postDelayed(input_finish_checker, idle_min);
//            performSearch(query);
        }
    }

    long idle_min = 1000; // 4 seconds after user stops typing
    long last_text_edit = 0;
    Handler h = new Handler();
    boolean already_queried = false;

    private Runnable input_finish_checker = new Runnable() {
        public void run() {
            if (System.currentTimeMillis() > (last_text_edit + idle_min - 500)) {
                // user hasn't changed the EditText for longer than
                // the min delay (with half second buffer window)
                if (!already_queried) { // don't do this stuff twice.
                    already_queried = true;
                    performSearch(latestQuery); // your queries
                }
            }
        }
    };

    private void performSearch(String query) {
        try {
            queryText = query;
            if (tabLayout.getSelectedTabPosition() == 0) {
                myGroup.setView(0, query);
            } else {
                publicGroup.setView(1, query);
            }
            already_queried=false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void searchViewOpened() {
        Log.e("searchViewOpened", "searchViewOpened " + searchActive);
        searchActive = true;
        search = true;
        getActivity().findViewById(R.id.bottom_tab_layout).setVisibility(GONE);
        tabLayout.setVisibility(GONE);
    }

    @Override
    public void searchViewClosed() {
        Log.e("searchViewClosed", "searchViewClosed " + queryText);
        getActivity().findViewById(R.id.bottom_tab_layout).setVisibility(VISIBLE);
        tabLayout.setVisibility(VISIBLE);
        if (!queryText.trim().isEmpty()) {
            Log.e("inside if", "inside if");
            onSearch("");
        }
        search = false;
    }

    @Override
    public void onItemClicked(String item) {

    }

    @Override
    public void onScroll() {

    }

    @Override
    public void error(String localizedMessage) {

    }

    @Override
    public void onCancelSearch() {
        Log.e("onCancelSearch", "onCancelSearch");
        searchActive = false;
        mSearchView.hide();
    }


    private void openKeyboard() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mSearchView.getSearchView().dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                mSearchView.getSearchView().dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
            }
        }, 200);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        inflater.inflate(R.menu.menu_search, menu);
        searchItem = menu.findItem(R.id.search);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                mSearchView.display();
                openKeyboard();
                return true;
            }
        });
        if (searchActive)
            mSearchView.display();
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.faCreateGroup:
                startActivity(new Intent(getActivity(), CreateGroupActivity.class));
                break;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private LinearLayoutManager linearLayoutManager = null;
        RecyclerView rvUserList;
        LinearLayout llLoading;
        List<Group> groups = new ArrayList<>();
        GroupListRecyclerViewAdapter groupListRecyclerViewAdapter;
        int pagination = 0;

        private String queryText = "";

        LinearLayout llNotFound;
        CardView cvContainer;
        private static final String ARG_SECTION_NUMBER = "section_number";
        TextView textView;
        Context _context;
        private BroadcastReceiver receiver;
        private BroadcastReceiver requestReceiver;
        private SwipeRefreshLayout swipelayout;


        @Override
        public void onResume() {
            super.onResume();
        }

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            Bundle args = getArguments();
            Log.e("args", "" + args.getInt(ARG_SECTION_NUMBER, 0));
            final int index = args.getInt(ARG_SECTION_NUMBER, 0);

            View rootView = inflater.inflate(R.layout.fragment_group_list, container, false);
            rvUserList = rootView.findViewById(R.id.rvGroupList);
            llLoading = rootView.findViewById(R.id.llLoading);
            llNotFound = rootView.findViewById(R.id.llNotFound);
            textView = rootView.findViewById(R.id.tv);
            cvContainer = rootView.findViewById(R.id.cvContainer);
            textView.setText(getResources().getString(R.string.msg_no_group_found));
            swipelayout = rootView.findViewById(R.id.swipelayout);

            if (index == 0) {
                setBroadCast();
            }
            setReceivedBroadCast(index);
            setView(index, "");

            swipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    setView(index, "");
                    swipelayout.setRefreshing(false);
                }
            });

            return rootView;
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            _context = context;
        }

        @Override
        public void onDestroyView() {
            if (receiver != null) {
                _context.unregisterReceiver(receiver);
                receiver = null;
            }
            if (requestReceiver != null) {
                _context.unregisterReceiver(requestReceiver);
                requestReceiver = null;
            }
            super.onDestroyView();
        }


        public void setView(int index, String query) {
            try {
                if (!Utility.isConnectingToInternet(getActivity())) {
                    final Snackbar snackbar = Snackbar.make(llNotFound, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                    llLoading.setVisibility(GONE);
                    llNotFound.setVisibility(View.VISIBLE);
                    textView.setText(getResources().getString(R.string.app_no_internet_error));
                    return;
                }

                pagination = 0;
                queryText = query;

                llLoading.setVisibility(VISIBLE);
                llNotFound.setVisibility(View.GONE);
                cvContainer.setVisibility(View.GONE);

                groups.clear();
                linearLayoutManager = new LinearLayoutManager(getActivity());
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                rvUserList.setLayoutManager(linearLayoutManager);
                groupListRecyclerViewAdapter = new GroupListRecyclerViewAdapter(rvUserList, getActivity(), groups, this);
                groupListRecyclerViewAdapter.setType(index);
                rvUserList.addItemDecoration(
                        new DividerItemDecoration(getActivity(), R.drawable.divider));
                rvUserList.setHasFixedSize(true);
                rvUserList.setAdapter(groupListRecyclerViewAdapter);


                if (index == 1) {
                    String url = API.PUBLIC_GROUPS + SharedPreferencesMethod.getUserId(getActivity()) + "/" + pagination + "/" + query;
                    API.sendRequestToServerGET_FRAGMENT(getActivity(), this, url, API.PUBLIC_GROUPS);
                } else if (index == 0) {
                    String url = API.JOINED_GROUPS + SharedPreferencesMethod.getUserId(getActivity()) + "/" + pagination + "/" + query;
                    API.sendRequestToServerGET_FRAGMENT(getActivity(), this, url, API.JOINED_GROUPS);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        //broadcast for update group list and group creation
        private void setBroadCast() {
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    //do something based on the intent's action
                    if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_CREATED)) {
                        Group group = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                        groups.add(0, group);
                        if (linearLayoutManager != null && groupListRecyclerViewAdapter != null) {
                            groupListRecyclerViewAdapter.notifyItemRangeInserted(0, 1);
                            groupListRecyclerViewAdapter.notifyItemRangeChanged(0, groups.size());
                            rvUserList.scrollToPosition(0);
                            groupListRecyclerViewAdapter.notifyDataSetChanged();
                            cvContainer.setVisibility(VISIBLE);
                            llNotFound.setVisibility(GONE);
                        }
                    } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_UPDATED)) {
                        Group group = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                        for (int i = 0; i < groups.size(); i++) {
                            if (groups.get(i).groupId.trim().equalsIgnoreCase(group.groupId.trim())) {
                               /* groups.remove(i);
                                groups.add(i, group);*/
                                groups.get(i).logo = group.logo;
                                groups.get(i).groupName = group.groupName;
                                groups.get(i).groupSummary = group.groupSummary;
                                groups.get(i).category = group.category;
                                groups.get(i).category_name = group.category_name;
                                groups.get(i).privacy = group.privacy;
                                groups.get(i).members_count = group.members_count;
                                groups.get(i).admins_count = group.admins_count;
                                if (linearLayoutManager != null && groupListRecyclerViewAdapter != null) {
                                    groupListRecyclerViewAdapter.notifyItemChanged(i);
                                }
                                break;
                            }
                        }
                    } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_DELETE)) {
                        Group group = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                        String userId = intent.getStringExtra(ResponseParameters.UserId);
                        for (int i = 0; i < groups.size(); i++) {
                            if (groups.get(i).groupId.trim().equalsIgnoreCase(group.groupId.trim())) {
                                if (linearLayoutManager != null && groupListRecyclerViewAdapter != null) {
                                    if (userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(_context))) {
                                        groupListRecyclerViewAdapter.notifyItemRemoved(i);
                                        groups.remove(i);
                                        groupListRecyclerViewAdapter.notifyItemRangeChanged(0, groups.size());
                                    } else {
                                        groups.get(i).members_count = group.members_count;
                                        groupListRecyclerViewAdapter.notifyItemChanged(i);
                                    }
                                }
                                break;
                            }
                        }
                    } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_DELETED)) {
                        Group group = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                        for (int i = 0; i < groups.size(); i++) {
                            if (groups.get(i).groupId.trim().equalsIgnoreCase(group.groupId.trim())) {
                                if (linearLayoutManager != null && groupListRecyclerViewAdapter != null) {
                                    groupListRecyclerViewAdapter.notifyItemRemoved(i);
                                    groups.remove(i);
                                    groupListRecyclerViewAdapter.notifyItemRangeChanged(0, groups.size());
                                }
                                break;
                            }
                        }
                    } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_DELETED_ALL)) {
                        String categoryId = intent.getStringExtra(ResponseParameters.CATEGORY);
                        for (int i = 0; i < groups.size(); i++) {
                            String groupID = groups.get(i).category.trim();
                            if (groupID.equalsIgnoreCase(categoryId)) {
                                if (linearLayoutManager != null && groupListRecyclerViewAdapter != null) {
                                    groupListRecyclerViewAdapter.notifyItemRemoved(i);
                                    groups.remove(i);
                                    groupListRecyclerViewAdapter.notifyItemRangeChanged(0, groups.size());
                                }
                                break;
                            }
                        }
                    } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_BLOCK)) {
                        Group group = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                        for (int i = 0; i < groups.size(); i++) {
                            if (groups.get(i).groupId.trim().equalsIgnoreCase(group.groupId.trim())) {
                                List<GroupMembers> group_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                                if (linearLayoutManager != null && groupListRecyclerViewAdapter != null) {
                                    if (group_members.get(0).userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(getActivity()))) {
                                        groupListRecyclerViewAdapter.notifyItemRemoved(i);
                                        groups.remove(i);
                                        groupListRecyclerViewAdapter.notifyDataSetChanged();
                                        //groupListRecyclerViewAdapter.notifyItemRangeChanged(0, groups.size());
                                    } else {
                                        groups.get(i).members_count = group.members_count;
                                        groupListRecyclerViewAdapter.notifyItemChanged(i);
                                    }
                                }
                                break;
                            }
                        }
                    } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_UNBLOCK)) {
                        Group group = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                        List<GroupMembers> group_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                        if (linearLayoutManager != null && groupListRecyclerViewAdapter != null &&
                                group_members.get(0).userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(getActivity()))) {
                            groups.add(0, group);
                            groupListRecyclerViewAdapter.notifyItemRangeInserted(0, 1);
                            groupListRecyclerViewAdapter.notifyItemRangeChanged(0, groups.size());
                        }
                    } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_ACCEPT)) {
                        Group group = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                        List<GroupMembers> group_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                        if (linearLayoutManager != null && groupListRecyclerViewAdapter != null &&
                                group_members.get(0).userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(getActivity()))) {
                            groups.add(0, group);
                            groupListRecyclerViewAdapter.notifyItemRangeInserted(0, 1);
                            groupListRecyclerViewAdapter.notifyItemRangeChanged(0, groups.size());
                            rvUserList.scrollToPosition(0);
                        } else {
                            for (int i = 0; i < groups.size(); i++) {
                                if (groups.get(i).groupId.trim().equalsIgnoreCase(group.groupId.trim())) {
                                    groups.get(i).members_count = group.members_count;
                                    if (linearLayoutManager != null && groupListRecyclerViewAdapter != null) {
                                        groupListRecyclerViewAdapter.notifyItemChanged(i);
                                    }
                                    break;
                                }
                            }
                        }
                    } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_ROLE_UPDATE)) {
                        Group group = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                        for (int i = 0; i < groups.size(); i++) {
                            if (groups.get(i).groupId.trim().equalsIgnoreCase(group.groupId.trim())) {
                               /* groups.remove(i);
                                groups.add(i, group);*/
//                                    groups.get(i).logo = group.logo;
//                                    groups.get(i).groupName = group.groupName;
//                                    groups.get(i).groupSummary = group.groupSummary;
//                                    groups.get(i).category = group.category;
//                                    groups.get(i).privacy = group.privacy;
                                groups.get(i).members_count = group.members_count;
                                groups.get(i).admins_count = group.admins_count;
                                if (linearLayoutManager != null && groupListRecyclerViewAdapter != null) {
                                    groupListRecyclerViewAdapter.notifyItemChanged(i);

                                }
                                break;
                            }
                        }
                    }
                    Log.e("test", "test");
                }
            };

            IntentFilter filter = new IntentFilter();
            filter.addAction(ResponseParameters.GROUP_CREATED);
            filter.addAction(ResponseParameters.GROUP_UPDATED);
            filter.addAction(ResponseParameters.GROUP_MEMBER_DELETE);
            filter.addAction(ResponseParameters.GROUP_DELETED);
            filter.addAction(ResponseParameters.GROUP_DELETED_ALL);
            filter.addAction(ResponseParameters.GROUP_MEMBER_BLOCK);
            filter.addAction(ResponseParameters.GROUP_MEMBER_ACCEPT);
            filter.addAction(ResponseParameters.GROUP_MEMBER_UNBLOCK);
            filter.addAction(ResponseParameters.GROUP_MEMBER_ROLE_UPDATE);
            _context.registerReceiver(receiver, filter);
        }


        //broadcast for request received and request send (for change on list)
        private void setReceivedBroadCast(final int index) {
            requestReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    //do something based on the intent's action


                }
            };

            IntentFilter filter = new IntentFilter();
            filter.addAction(ResponseParameters.REQUEST_RECEIVED);
            filter.addAction(ResponseParameters.REQUEST_SEND);
            _context.registerReceiver(requestReceiver, filter);
        }

        public void getResponse(JSONObject outPut, final int i) {
            llLoading.setVisibility(GONE);
            Log.e("output", outPut.toString());
            System.out.println("Group Fragment Response    "+outPut.toString());
            try {
                if (i == 0 || i == 1) {
                    Log.e("output", outPut.toString());
                    if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                        if (outPut.has(ResponseParameters.Errors)) {
                            groups.clear();
                            groupListRecyclerViewAdapter.notifyDataSetChanged();
                            llNotFound.setVisibility(View.VISIBLE);
                            cvContainer.setVisibility(View.GONE);
                        } else if (outPut.has(ResponseParameters.Success)) {
                            if (outPut.has(ResponseParameters.GROUPS)) {
                                llNotFound.setVisibility(View.GONE);
                                groups = new ArrayList<>();
                                Gson gson = new Gson(); // creates a Gson instance
                                Type listType = new TypeToken<List<Group>>() {
                                }.getType();

                                groups = gson.fromJson(outPut.getString(ResponseParameters.GROUPS), listType); //parses the JSON string into an Cloth object
                                if (groups.size() > 0) {
                                    cvContainer.setVisibility(View.VISIBLE);
                                } else {
                                    llNotFound.setVisibility(View.VISIBLE);
                                }
                                linearLayoutManager = new LinearLayoutManager(getActivity());
                                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                rvUserList.setLayoutManager(linearLayoutManager);
                                groupListRecyclerViewAdapter = new GroupListRecyclerViewAdapter(rvUserList, getActivity(), groups, this);
                                if (outPut.has(ResponseParameters.GROUP_IMG_BASE_URL))
                                    groupListRecyclerViewAdapter.setImageBaseUrl(outPut.getString(ResponseParameters.GROUP_IMG_BASE_URL));
                                groupListRecyclerViewAdapter.setType(i);
                                rvUserList.addItemDecoration(
                                        new DividerItemDecoration(getActivity(), R.drawable.divider));
                                rvUserList.setHasFixedSize(true);
                                rvUserList.setAdapter(groupListRecyclerViewAdapter);

                                groupListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                    @Override
                                    public void onLoadMore() {
                                        if (groups.size() >= 10) {
                                            Log.e("inside", "inside");
                                            rvUserList.post(new Runnable() {
                                                public void run() {
                                                    groups.add(null);
                                                    groupListRecyclerViewAdapter.notifyItemInserted(groups.size() - 1);
                                                }
                                            });
                                            if (i == 1)
                                                API.sendRequestToServerGET_FRAGMENT(getActivity(), PlaceholderFragment.this, API.PUBLIC_GROUPS + SharedPreferencesMethod.getUserId(getActivity()) + "/" + (pagination + 10) + "/" + queryText, API.PUBLIC_GROUPS_UPDATE);
                                            else
                                                API.sendRequestToServerGET_FRAGMENT(getActivity(), PlaceholderFragment.this, API.JOINED_GROUPS + SharedPreferencesMethod.getUserId(getActivity()) + "/" + (pagination + 10) + "/" + queryText, API.PUBLIC_GROUPS_UPDATE);
                                        }
                                    }
                                });

                            }
                        } else {
                            llNotFound.setVisibility(View.VISIBLE);
                        }
                    } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                }
                if (i == 2) {
                    groups.remove(groups.size() - 1);
                    groupListRecyclerViewAdapter.notifyItemRemoved(groups.size());
                    if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                        if (outPut.has(ResponseParameters.Success)) {
                            if (outPut.has(ResponseParameters.GROUPS)) {
                                llNotFound.setVisibility(View.GONE);
                                List<Group> groups_updated = new ArrayList<>();
                                Gson gson = new Gson(); // creates a Gson instance
                                Type listType = new TypeToken<List<Group>>() {
                                }.getType();

                                groups_updated = gson.fromJson(outPut.getString(ResponseParameters.GROUPS), listType); //parses the JSON string into an Cloth object
                                groups.addAll(groups_updated);

                                if (groups_updated.size() > 0) {
                                    groupListRecyclerViewAdapter.notifyDataSetChanged();
                                    pagination = pagination + 10;
                                    groupListRecyclerViewAdapter.setLoaded();
                                }
                            }
                        }
                    }
                }


            } catch (Exception e) {
                llNotFound.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }

        public void showNoUser() {
            llNotFound.setVisibility(View.VISIBLE);
        }


    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a FavLinkFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    myGroup = PlaceholderFragment.newInstance(position);
                    return myGroup;
                case 1:
                    publicGroup = PlaceholderFragment.newInstance(position);
                    return publicGroup;
                default:
                    myGroup = PlaceholderFragment.newInstance(position);
                    return myGroup;
            }

        }

       /* public View getTabView(String title, String count) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(getActivity()).inflate(R.layout.invite_tab, null);
            TextView tv = (TextView) v.findViewById(R.id.tv);
            tv.setText(title);

            TextView tvCount = (TextView) v.findViewById(R.id.tvCount);
            if (count.length() > 2) {
                count = "99+";
            }
            tvCount.setText(count);
            if (!count.isEmpty()) {
                tvCount.setVisibility(View.VISIBLE);
            }

            return v;
        }*/

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.header_main_mygroup_title);
                case 1:
                    return getResources().getString(R.string.header_main_publicgroup_title);
            }
            return null;
        }
    }
}
