package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.codeholic.kotumb.app.Adapter.ImagesRecyclerViewAdapter;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.Utility;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagesShowActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.rvUrlImages)
    RecyclerView rvUrlImages;

    @BindView(R.id.rvUriImages)
    RecyclerView rvUriImages;

    @BindView(R.id.pdf_show)
    WebView pdf_show;

    @BindView(R.id.image_show)
    NestedScrollView image_show;

    Context context;

    Post post;
    ArrayList<String> urlImages = new ArrayList<>();
    List<byte[]> imagesEncodedList = new ArrayList<>();
    ArrayList<Uri> uriList = new ArrayList<>();
    String groupImageBaseUrl = "";
    ImagesRecyclerViewAdapter imagesUrlRecyclerViewAdapter;

    ImagesRecyclerViewAdapter imagesByteRecyclerViewAdapter;
    private LinearLayoutManager urlImageLinearLayoutManager;
    private LinearLayoutManager byteLinearLayoutManager;

    // onCreate method initializing view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images_show);
        ButterKnife.bind(this);
        init();
        initToolbar();
        setView();
    }








    //init data of user
    private void init() {
        context = this;
    }

    //init toolbar function
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.image_title_activity));

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //setview for getting user connection
    private void setView() {
        try {
            urlImages = getIntent().getStringArrayListExtra(ResponseParameters.IMAGE);
            System.out.println("Image URL  "+groupImageBaseUrl);
            uriList = getIntent().getParcelableArrayListExtra(ResponseParameters.IMAGE_URI_LIST);
            imagesEncodedList = Utility.convertURIToByteArray(uriList, context);
            groupImageBaseUrl = getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL);
            urlImageLinearLayoutManager = new LinearLayoutManager(this);
            urlImageLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvUrlImages.setLayoutManager(urlImageLinearLayoutManager);
            imagesUrlRecyclerViewAdapter = new ImagesRecyclerViewAdapter(rvUrlImages, this, urlImages);
            if (getIntent().hasExtra(ResponseParameters.SHOW_CLEAR)){
                imagesUrlRecyclerViewAdapter.setShowCancel(false);
            }
            imagesUrlRecyclerViewAdapter.setGroupImageBaseUrl(groupImageBaseUrl);
            rvUrlImages.setNestedScrollingEnabled(false);
            rvUrlImages.setAdapter(imagesUrlRecyclerViewAdapter);


            byteLinearLayoutManager = new LinearLayoutManager(this);
            byteLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvUriImages.setLayoutManager(byteLinearLayoutManager);
            rvUriImages.setNestedScrollingEnabled(false);
            imagesByteRecyclerViewAdapter = new ImagesRecyclerViewAdapter(this, imagesEncodedList, uriList);
            if (getIntent().hasExtra(ResponseParameters.SHOW_CLEAR)){
                imagesByteRecyclerViewAdapter.setShowCancel(false);
            }
            rvUriImages.setAdapter(imagesByteRecyclerViewAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideView() {
        if (urlImages.size() == 0 && imagesEncodedList.size() == 0) {
            onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (!getIntent().hasExtra(ResponseParameters.SHOW_CLEAR)) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(ResponseParameters.IMAGE, urlImages);
            returnIntent.putExtra(ResponseParameters.IMAGE_URI_LIST, uriList);
            setResult(Activity.RESULT_OK, returnIntent);
        }
        finish();
        Log.e("TAG", "length " + urlImages.size());
    }
}
