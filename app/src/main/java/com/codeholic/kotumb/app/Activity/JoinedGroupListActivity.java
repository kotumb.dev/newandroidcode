package com.codeholic.kotumb.app.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Adapter.GroupListRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.View.DividerItemDecoration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shahroz.svlibrary.interfaces.onSearchListener;
import com.shahroz.svlibrary.interfaces.onSimpleSearchActionsListener;
import com.shahroz.svlibrary.widgets.MaterialSearchView;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class JoinedGroupListActivity extends AppCompatActivity implements onSimpleSearchActionsListener, onSearchListener {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.tvNouser)
    TextView tvNoGroup;

    @BindView(R.id.rvUserList)
    RecyclerView rvGroupList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.adView)
    ImageView adView;
    @BindView(R.id.rlAdView)
    RelativeLayout rlAdView;
    Post post;

    int pagination = 0;
    private LinearLayoutManager linearLayoutManager;
    private BroadcastReceiver receiver;
    List<Group> groups = new ArrayList<>();
    GroupListRecyclerViewAdapter groupListRecyclerViewAdapter;

    private boolean mSearchViewAdded = false;
    private MaterialSearchView mSearchView;
    private WindowManager mWindowManager;
    private MenuItem searchItem;
    private boolean searchActive = false;
    private boolean search = true;
    private String queryText = "";

    // onCreate method initializing view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_groups);
        ButterKnife.bind(this);
        init();
        initToolbar();
        //setView();
        onSearch("");
        setBroadCast();

        IntentFilter filterAdd = new IntentFilter();
        filterAdd.addAction(ResponseParameters.ADD_RECEIVED);
        registerReceiver(getAd, filterAdd);
    }

    private BroadcastReceiver getAd = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            try {
//                HomeScreenActivity.setUpAdImage(intent.getStringExtra(ResponseParameters.ADD_RECEIVED), rlAdView, adView, JoinedGroupListActivity.this, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void setUpAdImage(String adResponse) {
        hideAdView();
        try {
            if (!adResponse.isEmpty()) {
                final JSONObject ad = new JSONObject(adResponse);
                if (ad.has(ResponseParameters.AD)) {
                    if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.IMAGE)) {
                        if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE).isEmpty()) {
                            rlAdView.setVisibility(VISIBLE);
                            new AQuery(this).id(adView).image(API.imageAdUrl + ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE), true, true, 300, R.drawable.default_ad);
                            adView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.URL)) {
                                            if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL).isEmpty()) {
                                                Utility.prepareCustomTab(JoinedGroupListActivity.this, ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL));
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideAdView() {
        rlAdView.setVisibility(GONE);
    }

    //init data of user
    private void init() {
        post = (Post) getIntent().getSerializableExtra(ResponseParameters.POST);
    }

    //init toolbar function
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.header_main_mygroup_title));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        mSearchView = new MaterialSearchView(this);
        mSearchView.setOnSearchListener(this);
        mSearchView.setSearchResultsListener(this);
        mSearchView.setHintText(getResources().getString(R.string.invitations_search));


        if (mToolbar != null) {
            // Delay adding SearchView until Toolbar has finished loading
            mToolbar.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!mSearchViewAdded && mWindowManager != null) {
                            mWindowManager.addView(mSearchView,
                                    MaterialSearchView.getSearchViewLayoutParams(JoinedGroupListActivity.this));
                            mSearchViewAdded = true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    @Override
    public void onSearch(String query) {
        Log.e("onSearch", "onSearch ");
        if (search) {
            try {
                if (!Utility.isConnectingToInternet(this)) { // checking net connection
                    llLoading.setVisibility(GONE);
                    if (query.trim().isEmpty() || groups.size() <= 0) {
                        llNotFound.setVisibility(View.VISIBLE);
                        tvNoGroup.setText(getResources().getString(R.string.app_no_internet_error));
                    }
                    final Snackbar snackbar = Snackbar.make(rvGroupList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }

                queryText = query;
                pagination = 0;
                llLoading.setVisibility(VISIBLE);
                llNotFound.setVisibility(View.GONE);
                groups.clear();
                linearLayoutManager = new LinearLayoutManager(JoinedGroupListActivity.this);
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                rvGroupList.setLayoutManager(linearLayoutManager);
                groupListRecyclerViewAdapter = new GroupListRecyclerViewAdapter(rvGroupList, JoinedGroupListActivity.this, groups, null);
                groupListRecyclerViewAdapter.setSharePost(true, post);
                groupListRecyclerViewAdapter.setType(0);
                rvGroupList.addItemDecoration(
                        new DividerItemDecoration(JoinedGroupListActivity.this, R.drawable.divider));
                rvGroupList.setHasFixedSize(true);
                rvGroupList.setAdapter(groupListRecyclerViewAdapter);

                API.sendRequestToServerGET(JoinedGroupListActivity.this, API.JOINED_GROUPS + SharedPreferencesMethod.getUserId(JoinedGroupListActivity.this) + "/" + (pagination) + "/" + query, API.JOINED_GROUPS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void searchViewOpened() {
        Log.e("searchViewOpened", "searchViewOpened " + searchActive);
        searchActive = true;
        search = true;
    }

    @Override
    public void searchViewClosed() {
        Log.e("searchViewClosed", "searchViewClosed");
        if (!queryText.trim().isEmpty()) {
            onSearch("");
        }
        search = false;
        //Util.showSnackBarMessage(fab,"Search View Closed");
    }

    @Override
    public void onItemClicked(String item) {

    }

    @Override
    public void onScroll() {

    }

    @Override
    public void error(String localizedMessage) {

    }

    @Override
    public void onCancelSearch() {
        Log.e("onCancelSearch", "onCancelSearch");
        searchActive = false;
        mSearchView.hide();
    }

    //setview for getting user connection
    private void setView() {
        try {
            if (!Utility.isConnectingToInternet(this)) { // checking net connection
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(View.VISIBLE);
                tvNoGroup.setText(getResources().getString(R.string.app_no_internet_error));
                final Snackbar snackbar = Snackbar.make(rvGroupList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }


            API.sendRequestToServerGET(JoinedGroupListActivity.this, API.JOINED_GROUPS + SharedPreferencesMethod.getUserId(JoinedGroupListActivity.this) + "/" + (pagination) + "/" + queryText, API.JOINED_GROUPS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //getResponse for getting list of urlImages
    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        Log.e("response", "" + outPut);
        try {
            if (i == 0) { // for getting urlImages
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        groups.clear();
                        groupListRecyclerViewAdapter.notifyDataSetChanged();
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        if (outPut.has(ResponseParameters.GROUPS)) {
                            llNotFound.setVisibility(View.GONE);
                            groups = new ArrayList<>();
                            Gson gson = new Gson(); // creates a Gson instance
                            Type listType = new TypeToken<List<Group>>() {
                            }.getType();

                            groups = gson.fromJson(outPut.getString(ResponseParameters.GROUPS), listType); //parses the JSON string into an Cloth object
                            if (groups.size() <= 0) {
                                llNotFound.setVisibility(View.VISIBLE);
                            }


                            linearLayoutManager = new LinearLayoutManager(JoinedGroupListActivity.this);
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rvGroupList.setLayoutManager(linearLayoutManager);
                            groupListRecyclerViewAdapter = new GroupListRecyclerViewAdapter(rvGroupList, JoinedGroupListActivity.this, groups, null);
                            groupListRecyclerViewAdapter.setSharePost(true, post);
                            if (outPut.has(ResponseParameters.GROUP_IMG_BASE_URL))
                                groupListRecyclerViewAdapter.setImageBaseUrl(outPut.getString(ResponseParameters.GROUP_IMG_BASE_URL));
                            groupListRecyclerViewAdapter.setType(i);
                            rvGroupList.addItemDecoration(
                                    new DividerItemDecoration(JoinedGroupListActivity.this, R.drawable.divider));
                            rvGroupList.setHasFixedSize(true);
                            rvGroupList.setAdapter(groupListRecyclerViewAdapter);

                            groupListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                @Override
                                public void onLoadMore() {
                                    if (groups.size() >= 10) {
                                        Log.e("inside", "inside");
                                        rvGroupList.post(new Runnable() {
                                            public void run() {
                                                groups.add(null);
                                                groupListRecyclerViewAdapter.notifyItemInserted(groups.size() - 1);
                                            }
                                        });
                                        String userId = SharedPreferencesMethod.getUserId(JoinedGroupListActivity.this);
                                        String url = API.JOINED_GROUPS + userId + "/" + (pagination + 10) + "/" + queryText;
                                        API.sendRequestToServerGET(JoinedGroupListActivity.this,
                                                url, API.JOINED_GROUPS_UPDATE);
                                    }
                                }
                            });


                            if (groups.size() > 0) {
                                llNotFound.setVisibility(View.GONE);
                            } else {
                                llNotFound.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        //no userfound
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } else if (i == 1) {// for updating list
                groups.remove(groups.size() - 1);
                groupListRecyclerViewAdapter.notifyItemRemoved(groups.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) {
                        if (outPut.has(ResponseParameters.GROUPS)) {
                            llNotFound.setVisibility(View.GONE);
                            List<Group> groups_updated = new ArrayList<>();
                            Gson gson = new Gson(); // creates a Gson instance
                            Type listType = new TypeToken<List<Group>>() {
                            }.getType();

                            groups_updated = gson.fromJson(outPut.getString(ResponseParameters.GROUPS), listType); //parses the JSON string into an Cloth object
                            groups.addAll(groups_updated);

                            if (groups_updated.size() > 0) {
                                groupListRecyclerViewAdapter.notifyDataSetChanged();
                                pagination = pagination + 10;
                                groupListRecyclerViewAdapter.setLoaded();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            //mo user found
            llNotFound.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        searchItem = menu.findItem(R.id.search);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                mSearchView.display();
                openKeyboard();
                return true;
            }
        });
        if (searchActive)
            mSearchView.display();
        return true;
    }

    private void openKeyboard() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mSearchView.getSearchView().dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                mSearchView.getSearchView().dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
            }
        }, 200);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }


    //broadcast for update group list and group creation
    private void setBroadCast() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //do something based on the intent's action
                if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_CREATED)) {
                    Group group = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    groups.add(0, group);
                    if (linearLayoutManager != null && groupListRecyclerViewAdapter != null) {
                        groupListRecyclerViewAdapter.notifyItemRangeInserted(0, 1);
                        groupListRecyclerViewAdapter.notifyItemRangeChanged(0, groups.size());
                    }
                } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_UPDATED)) {
                    Group group = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    for (int i = 0; i < groups.size(); i++) {
                        if (groups.get(i).groupId.trim().equalsIgnoreCase(group.groupId.trim())) {
                            groups.remove(i);
                            groups.add(i, group);
                            if (linearLayoutManager != null && groupListRecyclerViewAdapter != null) {
                                groupListRecyclerViewAdapter.notifyItemChanged(i);
                            }
                            break;
                        }
                    }
                } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_DELETE)) {
                    Group group = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    String userId = intent.getStringExtra(ResponseParameters.UserId);
                    if (userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(JoinedGroupListActivity.this))) {
                        for (int i = 0; i < groups.size(); i++) {
                            if (groups.get(i).groupId.trim().equalsIgnoreCase(group.groupId.trim())) {
                                if (linearLayoutManager != null && groupListRecyclerViewAdapter != null) {
                                    groupListRecyclerViewAdapter.notifyItemRemoved(i);
                                    groups.remove(i);
                                    groupListRecyclerViewAdapter.notifyItemRangeChanged(0, groups.size());
                                }
                                break;
                            }
                        }
                    }
                } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_DELETED)) {
                    Group group = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    for (int i = 0; i < groups.size(); i++) {
                        if (groups.get(i).groupId.trim().equalsIgnoreCase(group.groupId.trim())) {
                            if (linearLayoutManager != null && groupListRecyclerViewAdapter != null) {
                                groupListRecyclerViewAdapter.notifyItemRemoved(i);
                                groups.remove(i);
                                groupListRecyclerViewAdapter.notifyItemRangeChanged(0, groups.size());
                            }
                            break;
                        }
                    }
                }
                Log.e("test", "test");
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.GROUP_CREATED);
        filter.addAction(ResponseParameters.GROUP_UPDATED);
        filter.addAction(ResponseParameters.GROUP_MEMBER_DELETE);
        filter.addAction(ResponseParameters.GROUP_DELETED);
        registerReceiver(receiver, filter);
    }


    @Override
    protected void onDestroy() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        if (getAd != null) {
            unregisterReceiver(getAd);
            getAd = null;
        }
        super.onDestroy();
    }
}
