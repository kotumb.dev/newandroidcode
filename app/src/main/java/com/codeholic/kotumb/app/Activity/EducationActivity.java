package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Model.Education;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.shagi.materialdatepicker.date.DatePickerFragmentDialog;

//import com.tsongkha.spinnerdatepicker.DatePickerDialog;
//import com.tsongkha.spinnerdatepicker.DatePickerDialog;


import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class EducationActivity extends AppCompatActivity implements View.OnClickListener
{

    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.etDegreeLayout)
    TextInputLayout etDegreeLayout;

    @BindView(R.id.etDegree)
    AutoCompleteTextView etDegree;

    @BindView(R.id.etCollegeLayout)
    TextInputLayout etCollegeLayout;

    @BindView(R.id.etCollege)
    AutoCompleteTextView etCollege;

    @BindView(R.id.etYearOfPassingLayout)
    TextInputLayout etYearOfPassingLayout;

    @BindView(R.id.etYearOfPassing)
    EditText etYearOfPassing;

    @BindView(R.id.etCollegeCity)
    EditText etCollegeCity;

    @BindView(R.id.etEduDesLayout)
    TextInputLayout etEduDesLayout;

    @BindView(R.id.etEduDes)
    EditText etEduDes;

    @BindView(R.id.cvEduSave)
    CardView cvEduSave;

    @BindView(R.id.tvEduSave)
    TextView tvEduSave;

    @BindView(R.id.pbEduLoading)
    ProgressBar pbEduLoading;

    @BindView(R.id.pbDegree)
    ProgressBar pbDegree;

    @BindView(R.id.pbCollege)
    ProgressBar pbCollege;

    @BindView(R.id.llCityContainer)
    LinearLayout llCityContainer;

    Activity context;
    Education education;

    private Calendar calendar;
    private int year, month, day;
    private String userId="";

    List<String> courses = new ArrayList<>();
    List<String> schools = new ArrayList<>();

    boolean clg = true, degree = true;
    boolean opened = false;
    boolean clearFocus = true;

    DatePickerDialog.OnDateSetListener mDateSetListener = null;
//    private DatePickerDialog.OnDateSetListener mDateSetListener;

    //on create method for initializing layout
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_education);
        ButterKnife.bind(this);
        init();
        initToolbar();
        setClickListener();
        setEditTextWatcher();
    }

    //initilizing toolbar and setting title
    private void initToolbar()
    {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.profile_education_title));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });
    }

    //initilizing for variables
    private void init()
    {
        context = this;
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        if (getIntent().hasExtra("DATA"))
        {
            education = getIntent().getParcelableExtra("DATA");
            fillView();
        }


        etDegree.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                Log.e("onFocusChange", "onFocusChange " + opened + " " + clearFocus);
                if (hasFocus && opened && (clearFocus || etDegree.getText().toString().trim().isEmpty()))
                    etDegree.performClick();
                opened = true;
            }
        });

        etDegree.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS).isEmpty())
                    {
                        if (!Utility.isConnectingToInternet(context))
                        {
                            return;
                        }
                        API.sendRequestToServerGET(context, API.COURSES, API.COURSES);
                    } else
                    {
                        getResponse(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.COURSES)), 1);
                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
                etDegree.showDropDown();
            }
        });


        etCollege.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if (hasFocus)
                    etCollege.performClick();
            }
        });

        etCollege.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS).isEmpty())
                    {
                        if (!Utility.isConnectingToInternet(context))
                        {
                            return;
                        }
                        API.sendRequestToServerGET(context, API.SCHOOLS, API.SCHOOLS);
                    } else
                    {
                        getResponse(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SCHOOLS)), 2);
                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
                etCollege.showDropDown();
            }
        });

    }

    //fill values if any
    private void fillView()
    {
        clg = false;
        degree = false;
        etDegree.setText(education.getCourse());
        etCollege.setText(education.getSchool());
        etYearOfPassing.setText(education.getYearOfPassing());
        etEduDes.setText(education.getDetails());
    }

    //setting click listeners to views
    private void setClickListener()
    {
        cvEduSave.setOnClickListener(this);
        etYearOfPassing.setOnClickListener(this);
    }

    //validation for fields
    private boolean eduValidation()
    {
        disableEduError();
        boolean result = true;
        clearFocus = false;
        clearFocus();
        clearFocus = true;
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etDegree.getWindowToken(), 0);
        if (etDegree.getText().toString().trim().isEmpty())
        {
            if (result)
            {
                etDegree.requestFocus();
                imm.showSoftInput(etDegree, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etDegreeLayout, getResources().getString(R.string.course_name_empty_input));
            result = false;
        }
        if (etCollege.getText().toString().trim().isEmpty())
        {
            if (result)
            {
                etCollege.requestFocus();
                imm.showSoftInput(etCollege, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etCollegeLayout, getResources().getString(R.string.school_name_empty_input));
            result = false;
        }
        Object tag = etCollege.getTag();
        if (tag != null)
        {
            if (!(boolean) tag)
            {
                enableError(etCollegeLayout, getString(R.string.select_from_dropdown_text));
                result = false;
            }
        }
        Object tag2 = etDegree.getTag();
        if (tag2 != null)
        {
            if (!(boolean) tag2)
            {
                enableError(etDegreeLayout, getString(R.string.select_from_dropdown_text));
                result = false;
            }
        }
        if (etYearOfPassing.getText().toString().trim().isEmpty())
        {
            if (result)
            {
                etYearOfPassingLayout.getParent().requestChildFocus(etYearOfPassingLayout, etYearOfPassingLayout);
            }
            enableError(etYearOfPassingLayout, getResources().getString(R.string.passing_year_empty_select));
            result = false;
        }
        return result;
    }

    private void clearFocus()
    {
        etCollege.clearFocus();
        etCollegeCity.clearFocus();
        Log.e("enabled", "enabled 1");
        etDegree.clearFocus();
        Log.e("enabled", "enabled 2");
        etEduDes.clearFocus();
        etYearOfPassing.clearFocus();
    }

    //disabling error from layout
    private void disableEduError()
    {
        etDegreeLayout.setErrorEnabled(false);
        etCollegeLayout.setErrorEnabled(false);
        etYearOfPassingLayout.setErrorEnabled(false);
    }

    //enabling errom from TextInputLayout
    private void enableError(TextInputLayout inputLayout, String error)
    {
        inputLayout.setErrorEnabled(true);
        inputLayout.setError(error);
    }

    //setting tex watcher to edittexts
    private void setEditTextWatcher()
    {
        setTextWatcher(etDegree, etDegreeLayout);
        setTextWatcher(etCollege, etCollegeLayout);
        setTextWatcher(etYearOfPassing, etYearOfPassingLayout);
    }

    //setting textwatcher for edittext
    private void setTextWatcher(final EditText editText, final TextInputLayout textInputLayout)
    {
        editText.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0)
                {
                    textInputLayout.setErrorEnabled(false);
                }

                if (editText == etCollege)
                {
                    llCityContainer.setVisibility(GONE);
                }
                if (s.length() >= 1)
                {
                    try
                    {
                        if (editText == etDegree)
                        {
                            if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.COURSES).isEmpty())
                            {
                                if (!Utility.isConnectingToInternet(context))
                                {
                                    return;
                                }
                                if (degree)
                                {
                                    pbDegree.setVisibility(VISIBLE);
                                    API.sendRequestToServerGET(context, API.COURSES, API.COURSES);
                                }
                                degree = true;
                            } else
                            {
                                if (clg)
                                {
                                    getResponse(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.COURSES)), 1);
                                }
                                clg = true;
                            }
                        }
                        if (editText == etCollege)
                        {
                            if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SCHOOLS).isEmpty())
                            {
                                if (!Utility.isConnectingToInternet(context))
                                {
                                    return;
                                }
                                if (clg)
                                {
                                    pbCollege.setVisibility(VISIBLE);
                                    API.sendRequestToServerGET(context, API.SCHOOLS, API.SCHOOLS);
                                }
                                clg = true;
                            } else
                            {
                                if (clg)
                                {
                                    getResponse(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SCHOOLS)), 2);
                                }
                                clg = true;
                            }
                        }

                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (editText == etCollege)
                {
                    Log.e("TAG", "maSkills");
                    if (etCollege.getText().toString().trim().isEmpty())
                    {
                        Log.e("TAG", "if");
                        etCollege.performClick();
                    }
                    etCollege.setTag(false);
                    //Toast.makeText(context, etCollege.getTag() + "", Toast.LENGTH_SHORT).show();
                }

                if (editText == etDegree)
                {
                    etDegree.setTag(false);
                    if (etDegree.getText().toString().trim().isEmpty())
                    {
                        etDegree.performClick();
                    }
                }
            }
        });
    }

    //getting response from service

    public void getResponse(JSONObject output, int i)
    {
        try
        {
            if (i == 0)
            { // response of saving education details
                if (output.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY"))
                {
                    if (output.has(ResponseParameters.Errors))
                    {
                        final Snackbar snackbar = Snackbar.make(etCollegeLayout, Utils.getErrorMessage(output), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else if (output.has(ResponseParameters.Success))
                    {
                        final Snackbar snackbar = Snackbar.make(etCollegeLayout, output.getString(ResponseParameters.Success), Toast.LENGTH_SHORT);
                        snackbar.show();
                        /*if (Constants.completeProfileActivity != null) {
                            Constants.completeProfileActivity.finish();
                            Constants.completeProfileActivity = null;
                        }*/
                        JSONObject jsonObject = output.getJSONObject(ResponseParameters.EDUCATION);
                        Constants.education = new Education();
                        Constants.education.setId(jsonObject.getString(ResponseParameters.Id));
                        Constants.education.setCourse(jsonObject.getString(RequestParameters.COURSE));
                        Constants.education.setSchool(jsonObject.getString(RequestParameters.SCHOOL));
                        Constants.education.setYearOfPassing(jsonObject.getString(RequestParameters.YEAR));
                        Constants.education.setDetails(jsonObject.getString(RequestParameters.DESCRIPTION));

                        Constants.completeProfileActivity = true;
                        Utils.showPopup(EducationActivity.this, getString(R.string.success_text), new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View view)
                            {
                                finish();
                            }
                        });
                    } else
                    {
//                        Toast.makeText(context, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(EducationActivity.this, getResources().getString(R.string.error_text));
                    }
                    tvEduSave.setVisibility(VISIBLE);
                    pbEduLoading.setVisibility(GONE);
                    cvEduSave.setClickable(true);
                    etDegree.setEnabled(true);
                    etCollege.setEnabled(true);
                    etYearOfPassing.setEnabled(true);
                    etEduDes.setEnabled(true);
                }
            }
            if (i == 1)
            { // getting degree list
                pbDegree.setVisibility(GONE);
                Log.e("API.COURSES", API.COURSES + "  " + output);
                if (output.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY"))
                {
                    if (output.has(ResponseParameters.Success))
                    {
                        if (output.has(ResponseParameters.COURSES))
                        {
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.COURSES, output.toString());
                            courses = new ArrayList<>();
                            for (int j = 0; j < output.getJSONArray(ResponseParameters.COURSES).length(); j++)
                            {
                                courses.add(output.getJSONArray(ResponseParameters.COURSES).getJSONObject(j).getString(ResponseParameters.COURSESNAME));
                            }
                            courses.add("Other");
                            String[] data = courses.toArray(new String[courses.size()]);
                            ArrayAdapter<?> adapter = new ArrayAdapter<Object>(this, R.layout.autocomplete_item, data);
                            etDegree.setAdapter(adapter);
                            etDegree.setThreshold(1);
                            Utils.addOtherOptionHandler(etDegree, courses, EducationActivity.this);
                            if (!etDegree.getText().toString().trim().isEmpty() && !courses.contains(etDegree.getText().toString().trim()))
                                etDegree.showDropDown();
                        }
                    }
                }
            }
            if (i == 2)
            { // getting college list
                pbCollege.setVisibility(GONE);
                Log.e("API.SCHOOLS", API.SCHOOLS + "  " + output);
                if (output.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY"))
                {
                    if (output.has(ResponseParameters.Success))
                    {
                        if (output.has(ResponseParameters.SCHOOLS))
                        {
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.SCHOOLS, output.toString());
                            schools = new ArrayList<>();
                            for (int j = 0; j < output.getJSONArray(ResponseParameters.SCHOOLS).length(); j++)
                            {
                                schools.add(output.getJSONArray(ResponseParameters.SCHOOLS).getJSONObject(j).getString(ResponseParameters.SCHOOLNAME));
                            }
                            schools.add("Other");
                            String[] data = schools.toArray(new String[schools.size()]);
                            ArrayAdapter<?> adapter = new ArrayAdapter<Object>(this, R.layout.autocomplete_item, data);
                            etCollege.setAdapter(adapter);
                            etCollege.setThreshold(1);
                            Utils.addOtherOptionHandler(etCollege, schools, EducationActivity.this);
                            if (!etCollege.getText().toString().trim().isEmpty() && !schools.contains(etCollege.getText().toString().trim()))
                                etCollege.showDropDown();
                            if (schools.contains(etCollege.getText().toString().trim()))
                            {
                                llCityContainer.setVisibility(GONE);
                            } else
                            {
                                llCityContainer.setVisibility(VISIBLE);
                            }
                        }
                    }
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(final View v)
    {
        switch (v.getId())
        {
            case R.id.cvEduSave: // save button click
                try
                {
                    if (eduValidation())
                    { // checking validation
                        if (!Utility.isConnectingToInternet(context))
                        { // checking net connection
                            final Snackbar snackbar = Snackbar.make(v, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        HashMap<String, Object> input = new HashMap<>();
                        if (getIntent().hasExtra("DATA"))
                        {
                            input.put(RequestParameters.EDUCATIONID, education.getId());

                        }
                        userId = SharedPreferencesMethod.getUserId(this);
                        if(getIntent().hasExtra("USERID"))
                        {
                            if(userId.isEmpty())
                            {
                                userId = getIntent().getStringExtra("USERID");
                            }
                        }
                        input.put(RequestParameters.USERID, userId);
                        input.put(RequestParameters.COURSE_NAME, "" + etDegree.getText().toString().trim());
                        if (llCityContainer.getVisibility() == VISIBLE)
                        {
                            input.put(RequestParameters.SCHOOL_NAME, "" + etCollege.getText().toString().trim() + " " + etCollegeCity.getText().toString().trim());
                        } else
                        {
                            input.put(RequestParameters.SCHOOL_NAME, "" + etCollege.getText().toString().trim());
                        }
                        input.put(RequestParameters.PASSING_YEAR, "" + etYearOfPassing.getText().toString().trim());
                        input.put(RequestParameters.DESCRIPTION, "" + etEduDes.getText().toString().trim());
                        tvEduSave.setVisibility(GONE);
                        pbEduLoading.setVisibility(VISIBLE);
                        cvEduSave.setClickable(false);
                        etDegree.setEnabled(false);
                        etCollege.setEnabled(false);
                        etYearOfPassing.setEnabled(false);
                        etEduDes.setEnabled(false);

                        if (getIntent().hasExtra("DATA"))
                        {
                            API.sendRequestToServerPOST_PARAM(context, API.EDUCATION_UPDATE, input); // for updating existing education info
                        } else
                        {
                            API.sendRequestToServerPOST_PARAM(context, API.EDUCATION, input); // for adding education info
                        }

                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            case R.id.etYearOfPassing: // for selecting passing year
                showDatePopup(context);
                break;
        }
    }

    //    private void showCal2(final EditText v) {
//        Calendar calendar = Calendar.getInstance();
//        new SpinnerDatePickerDialogBuilder()
//                .context(EducationActivity.this)
//                .callback(new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        v.setText("" + year);
//                    }
//                })
//                .spinnerTheme(R.style.NumberPickerStyle)
//                .showTitle(true)
//                .showDaySpinner(true)
//                .defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
//                //  .maxDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
//                .minDate(calendar.get(Calendar.YEAR) - 100, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
//                .build()
//                .show();
//    }
    private void showCal(final EditText v)
    {
//        Calendar calendar = Calendar.getInstance();

        DatePickerFragmentDialog datePickerDialog = DatePickerFragmentDialog.newInstance(new DatePickerFragmentDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePickerFragmentDialog view, int year, int monthOfYear, int dayOfMonth)
            {
                v.setText("" + year);
            }
        }, 2014, 12 - 1, 16);


        try
        {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date date = df.parse("1947-01-01");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            datePickerDialog.setMinDate(calendar.getTimeInMillis());
            datePickerDialog.setMaxDate(Calendar.getInstance().getTimeInMillis());
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        datePickerDialog.setTitle("");
        datePickerDialog.show(getSupportFragmentManager(), "");
    }


    public void showDatePopup(Activity activity)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.date_picker_dialog, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final Spinner year = dialogView.findViewById(R.id.year);
        final AlertDialog confirmAlert = dialogBuilder.create();
        llYes.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String years = year.getSelectedItem().toString().trim();
                confirmAlert.dismiss();
                etYearOfPassing.setText(years);
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public void getDateShow(final EditText v)
    {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(
                EducationActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                mDateSetListener,
                year, month, day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        int daySpinnerId = Resources.getSystem().getIdentifier("day", "id", "android");
        View daySpinner = dialog.findViewById(daySpinnerId);
        try
        {
            daySpinner.setVisibility(View.GONE);
        } catch (Exception ex)
        {
            showDatePopup(context);
        }

        dialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
            {
                v.setText(String.valueOf(year));
            }
        });
    }


}