package com.codeholic.kotumb.app.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ErrorMessages {
    private HashMap<String, String> errorMessages;
    private static ErrorMessages instance;


    private ErrorMessages() {
        errorMessages = new HashMap<>();
//        errorMessages.put("serErr_regFail", "आपका नया खाता बनाने में कोई समस्या थी। कृपया पुन: प्रयास करें।");
//        errorMessages.put("serErr_verifyNotFound", "अमान्य उपयोगकर्ता");
//        errorMessages.put("serErr_loginRoleInvalid", "अमान्य उपयोगकर्ता नाम या पासवर्ड");
//        errorMessages.put("serErr_loginInvalid", "अमान्य उपयोगकर्ता नाम या पासवर्ड");
    }

    public static ErrorMessages getInstance() {
        if (instance == null) {
            instance = new ErrorMessages();
        }
        return instance;
    }

    public void loadErrorMessages(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                errorMessages.put(jsonObject.getString("keyword"), jsonObject.getString("translation"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getMessageFor(String key) {
        return errorMessages.get(key);
    }
}
