package com.codeholic.kotumb.app.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Adapter.ReminderListAdapter;
import com.codeholic.kotumb.app.Model.Note;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class NotepadActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
   /* @BindView(R.id.container)
    ViewPager mViewPager;*/

   /* @BindView(R.id.tabs)
    TabLayout tabLayout;*/

    private LinearLayoutManager linearLayoutManager;
    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;
    @BindView(R.id.llLoading)
    LinearLayout llLoading;
    @BindView(R.id.llAddNote)
    CardView llAddNote;
//    @BindView(R.id.adView)
//    ImageView adView;
    @BindView(R.id.rlAdView)
    RelativeLayout rlAdView;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    List<Note> notes = new ArrayList<>();
    ReminderListAdapter reminderListAdapter;

    //oncreate method for setting content view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notepad);
        ButterKnife.bind(this);
        initToolbar();
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        /*mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);*/
        setClickListener();
        setView();

        IntentFilter filterAdd = new IntentFilter();
        filterAdd.addAction(ResponseParameters.ADD_RECEIVED);
        registerReceiver(getAd, filterAdd);
    }

    private BroadcastReceiver getAd = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            try{
//                HomeScreenActivity.setUpAdImage(intent.getStringExtra(ResponseParameters.ADD_RECEIVED), rlAdView, adView, NotepadActivity.this, true);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    private void setUpAdImage(String adResponse) {
        hideAdView();
        try {
            if (!adResponse.isEmpty()) {
                final JSONObject ad = new JSONObject(adResponse);
                if (ad.has(ResponseParameters.AD)) {
                    if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.IMAGE)) {
                        if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE).isEmpty()) {
                            rlAdView.setVisibility(VISIBLE);
//                            new AQuery(this).id(adView).image(API.imageAdUrl + ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE), true, true, 300, R.drawable.ic_user);
//                            adView.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    try {
//                                        if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.URL)) {
//                                            if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL).isEmpty()) {
//                                                Utility.prepareCustomTab(NotepadActivity.this, ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL));
//                                            }
//                                        }
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            });
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideAdView() {
        rlAdView.setVisibility(GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.REMINDER != -1 && Constants.note != null) {
            notes.remove(Constants.REMINDER);
            notes.add(Constants.REMINDER, Constants.note);
            reminderListAdapter.notifyDataSetChanged();
        } else if (Constants.note != null) {
            if (notes.size() > 0) {
                notes.add(Constants.note);
                reminderListAdapter.notifyDataSetChanged();
            } else {
                notes.add(Constants.note);
                linearLayoutManager = new LinearLayoutManager(this);
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                rvUserList.setLayoutManager(linearLayoutManager);
                /*rvGroupList.setHasFixedSize(true);
                rvGroupList.setNestedScrollingEnabled(false);*/
                reminderListAdapter = new ReminderListAdapter(this, notes);
                rvUserList.setAdapter(reminderListAdapter);
                llNotFound.setVisibility(View.GONE);
            }
        }
        Constants.REMINDER = -1;
        Constants.note = null;
    }

    private void setClickListener() {
        llAddNote.setOnClickListener(this);
    }

    private void setView() {
        try {
            if (!Utility.isConnectingToInternet(this)) {
                final Snackbar snackbar = Snackbar.make(llAddNote, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(View.VISIBLE);
                return;
            }
            API.sendRequestToServerGET(this, API.REMINDER_LIST + SharedPreferencesMethod.getUserId(this), API.REMINDER_LIST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //initializing toolbar and setting header title
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.header_main_menu_option8));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        try {
            Log.e("output", outPut.toString());
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Errors)) {
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.has(ResponseParameters.Success)) {
                    if (outPut.has(ResponseParameters.NOTES)) {
                        notes = new ArrayList<>();
                        for (int j = 0; j < outPut.getJSONArray(ResponseParameters.NOTES).length(); j++) {
                            JSONObject noteList = outPut.getJSONArray(ResponseParameters.NOTES).getJSONObject(j);
                            Note note = new Note();
                            note.setNoteID(noteList.getString(ResponseParameters.Id));
                            note.setNoteTitle(noteList.getString(ResponseParameters.TITLE));
                            note.setNoteDes(noteList.getString(ResponseParameters.DESCRIPTION));
                            note.setNoteDate(noteList.getString(ResponseParameters.Date));
                            note.setNoteTime(noteList.getString(ResponseParameters.TIME));
                            notes.add(note);
                        }
                        linearLayoutManager = new LinearLayoutManager(this);
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserList.setLayoutManager(linearLayoutManager);
                        //rvGroupList.setHasFixedSize(true);
                        //rvGroupList.setNestedScrollingEnabled(false);
                        reminderListAdapter = new ReminderListAdapter(this, notes);
                        rvUserList.setAdapter(reminderListAdapter);
                    }
                } else {
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                llNotFound.setVisibility(View.VISIBLE);
            } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            llNotFound.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }

    public void showNotFound() {
        llNotFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llAddNote:
                startActivity(new Intent(this, AddNoteActivity.class));
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_invites, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        if (getAd != null) {
            unregisterReceiver(getAd);
            getAd = null;
        }
        super.onDestroy();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements View.OnClickListener {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private LinearLayoutManager linearLayoutManager;
        RecyclerView rvUserList;
        LinearLayout llLoading;
        CardView llAddNote;

        LinearLayout llNotFound;
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        @Override
        public void onResume() {
            super.onResume();

        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_reminder, container, false);
            rvUserList = (RecyclerView) rootView.findViewById(R.id.rvUserList);
            llLoading = (LinearLayout) rootView.findViewById(R.id.llLoading);
            llNotFound = (LinearLayout) rootView.findViewById(R.id.llNotFound);
            llAddNote = (CardView) rootView.findViewById(R.id.llAddNote);
            llAddNote.setOnClickListener(this);
            Bundle args = getArguments();
            int index = args.getInt(ARG_SECTION_NUMBER, 0);
            setView(rootView, index);
            return rootView;
        }

        private void setView(View rootView, int index) {
            try {
                if (!Utility.isConnectingToInternet(getActivity())) {
                    final Snackbar snackbar = Snackbar.make(rootView, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                    llLoading.setVisibility(GONE);
                    llNotFound.setVisibility(View.VISIBLE);
                    return;
                }
                if (index == 1)
                    API.sendRequestToServerGET_FRAGMENT(getActivity(), this, API.REMINDER_LIST + SharedPreferencesMethod.getUserId(getActivity()), API.REMINDER_LIST);
                else if (index == 0) {
                    llLoading.setVisibility(GONE);
                    llNotFound.setVisibility(View.VISIBLE);
                    //API.sendRequestToServerGET_FRAGMENT(getActivity(), this, API.INVITATIONS_RECEIVED + SharedPreferencesMethod.getLinkName(getActivity()), API.INVITATIONS_RECEIVED);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void showNotFound() {
            llNotFound.setVisibility(View.VISIBLE);
        }

        public void getResponse(JSONObject outPut, int i) {
            llLoading.setVisibility(GONE);
            try {
                Log.e("output", outPut.toString());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) {
                        if (outPut.has(ResponseParameters.NOTES)) {
                            List<Note> notes = new ArrayList<>();
                            for (int j = 0; j < outPut.getJSONArray(ResponseParameters.NOTES).length(); j++) {
                                JSONObject noteList = outPut.getJSONArray(ResponseParameters.NOTES).getJSONObject(j);
                                Note note = new Note();
                                note.setNoteID(noteList.getString(ResponseParameters.Id));
                                note.setNoteTitle(noteList.getString(ResponseParameters.TITLE));
                                note.setNoteDes(noteList.getString(ResponseParameters.DESCRIPTION));
                                note.setNoteDate(noteList.getString(ResponseParameters.Date));
                                note.setNoteTime(noteList.getString(ResponseParameters.TIME));
                                notes.add(note);
                            }
                            linearLayoutManager = new LinearLayoutManager(getActivity());
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rvUserList.setLayoutManager(linearLayoutManager);
                            rvUserList.setHasFixedSize(true);
                            rvUserList.setNestedScrollingEnabled(false);
                            //rvGroupList.setAdapter(new ReminderListAdapter(getActivity(), notes, this));
                        }
                    } else {
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                llNotFound.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.llAddNote:
                    getActivity().startActivity(new Intent(getActivity(), AddNoteActivity.class));
                    break;
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a FavLinkFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "";
                case 1:
                    return getResources().getString(R.string.app_reminder_text);
            }
            return null;
        }
    }
}
