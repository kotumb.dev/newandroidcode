package com.codeholic.kotumb.app;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Activity.SignupActivity;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;

public class vidphon extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(SharedPreferencesMethod.getUserId(vidphon.this).length() == 0){
            startActivity(new Intent(this, SignupActivity.class));
            finish();
        }
        setContentView(R.layout.activity_vidphon);
        WebView myWebView = findViewById(R.id.webview);
        myWebView.loadUrl("file:///android_asset/filename.html");

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(false);
        webSettings.setMediaPlaybackRequiresUserGesture(false);
        webSettings.setDisplayZoomControls(false);
        webSettings.setCacheMode(webSettings.LOAD_NO_CACHE);
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabaseEnabled(true);


        myWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onPermissionRequest(PermissionRequest request) {
                request.grant(request.getResources());
            }

            public boolean onConsoleMessage(ConsoleMessage cm) {
                Log.e("MyApplication", cm.message());
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {

        Intent intent=new Intent(this, HomeScreenActivity.class);
        startActivity(intent);
        finish();

        super.onBackPressed();
    }
}
