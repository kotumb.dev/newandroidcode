package com.codeholic.kotumb.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonArray;

public class DisplayUserObject implements Parcelable {

    private String userId;
    private String username;
    private String firstName;
    private String middleName;
    private String lastName;
    private String dateOfBirth;
    private String email;
    private String avatar;
    private String address;
    private String city;
    private String state;
    private String zipcode;
    private String likes_count;
    private String comments_count;
    private String mobileNumber;
    private String isLiked;
    private JsonArray isCommented;
    private String profileSummary;

    public DisplayUserObject() {
    }

    protected DisplayUserObject(Parcel in) {
        userId = in.readString();
        username = in.readString();
        firstName = in.readString();
        middleName = in.readString();
        lastName = in.readString();
        dateOfBirth = in.readString();
        email = in.readString();
        avatar = in.readString();
        address = in.readString();
        city = in.readString();
        state = in.readString();
        zipcode = in.readString();
        likes_count = in.readString();
        comments_count = in.readString();
        mobileNumber = in.readString();
        isLiked = in.readString();
        profileSummary = in.readString();
    }

    public static final Creator<DisplayUserObject> CREATOR = new Creator<DisplayUserObject>() {
        @Override
        public DisplayUserObject createFromParcel(Parcel in) {
            return new DisplayUserObject(in);
        }

        @Override
        public DisplayUserObject[] newArray(int size) {
            return new DisplayUserObject[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(String likes_count) {
        this.likes_count = likes_count;
    }

    public String getComments_count() {
        return comments_count;
    }

    public void setComments_count(String comments_count) {
        this.comments_count = comments_count;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(String isLiked) {
        this.isLiked = isLiked;
    }

    public JsonArray getIsCommented() {
        return isCommented;
    }

    public void setIsCommented(JsonArray isCommented) {
        this.isCommented = isCommented;
    }

    public String getProfileSummary() {
        return profileSummary;
    }

    public void setProfileSummary(String profileSummary) {
        this.profileSummary = profileSummary;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(username);
        dest.writeString(firstName);
        dest.writeString(middleName);
        dest.writeString(lastName);
        dest.writeString(dateOfBirth);
        dest.writeString(email);
        dest.writeString(avatar);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(zipcode);
        dest.writeString(likes_count);
        dest.writeString(comments_count);
        dest.writeString(mobileNumber);
        dest.writeString(isLiked);
        dest.writeString(profileSummary);
    }
}
