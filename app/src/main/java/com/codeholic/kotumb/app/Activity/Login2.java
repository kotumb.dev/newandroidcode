package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.codeholic.kotumb.app.BroadcastReciever.NotifyReminder;
import com.codeholic.kotumb.app.Model.Note;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.View.OtpEditText;
import com.codeholic.kotumb.app.View.ProgressBar;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.github.florent37.viewtooltip.ViewTooltip.ALIGN.START;
import static com.github.florent37.viewtooltip.ViewTooltip.Position.TOP;

public class Login2 extends AppCompatActivity implements View.OnClickListener {
    private EditText etPassword, etUserName;
    private TextInputLayout userNameLayout, passwordLayout;
    private TextView tvRegistration, tvRegistrationText, tvForgetPwd, tvLangEnglish, tvLangHindi,tvLogin,tvLangKanand,tvOtpHint, tvResend, tvPvc, tvTerms, tvText;
    private LinearLayout rlOtpContainer, llDialog;
    private OtpEditText etOtp;
    private android.widget.ProgressBar pbLoading;
    private CardView cvLogin;
    private Activity context;
    private Button click;
    private CheckBox rememberMe;
    RelativeLayout relativeLayout;
    private String languageToLoad = "";
    Configuration config = new Configuration();
    Locale locale;
    User user = new User();
    ProgressBar progressBar;
    ImageView ivUserName, ivForgetPwd, ivRegistration;
    ViewTooltip v;
    View hsv;
    private boolean delete;
    private boolean popup;
    private String deleteAccountReason;

    //oncreate method for setting layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        languageToLoad = SharedPreferencesMethod.getDefaultLanguage(this);
        System.out.println("Language To Load  "+ languageToLoad);
        delete = getIntent().getBooleanExtra("delete", false);
        deleteAccountReason = getIntent().getStringExtra("reason");
        popup = getIntent().getBooleanExtra("popup", false);
        changeLocale(languageToLoad);

        if (popup) {
            Utils.showPopup(this, "Account Deleted Successfully");
        }
        rememberUserPasswordGet();//get and set username and password in remember me feilds
      /*  relativeLayout=findViewById(R.id.relative_layout);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("click==========");
                Utils.getLang(LoginActivity.this);
            }
        });*/
    }

    //onresume method
    @Override
    protected void onResume() {
        super.onResume();
        if (!languageToLoad.equalsIgnoreCase(SharedPreferencesMethod.getDefaultLanguage(this))) {
            languageToLoad = SharedPreferencesMethod.getDefaultLanguage(this);
            changeLocale(languageToLoad);
        }
    }

    LinearLayout linearLayout;
    //setting content view
    private void setView() {
        setContentView(R.layout.activity_login);
        initView();
        setClickListener();
        setTextWatcher(etUserName, userNameLayout);
        setTextWatcher(etPassword, passwordLayout);
        otpTextChangeListener();

        if(delete){
            findViewById(R.id.bar).setVisibility(GONE);
            findViewById(R.id.hsv).setVisibility(GONE);
            // relativeLayout.setVisibility(GONE);
            findViewById(R.id.select_lang_text).setVisibility(GONE);
            findViewById(R.id.tv_Registration).setVisibility(GONE);
            findViewById(R.id.ivRegistration).setVisibility(GONE);
            findViewById(R.id.tvForgetPwd).setVisibility(GONE);
            findViewById(R.id.ivForgetPwd).setVisibility(GONE);
            findViewById(R.id.tvTerms).setVisibility(GONE);
            findViewById(R.id.tnc).setVisibility(GONE);
            findViewById(R.id.ivUserName).setVisibility(GONE);
            findViewById(R.id.delete_text).setVisibility(VISIBLE);
            TextView username = findViewById(R.id.etUserName);
            TextView tvLogin = findViewById(R.id.tvLogin);
            tvLogin.setText(getString(R.string.confirm_text));
            username.setFocusable(false);
            username.setText(SharedPreferencesMethod.getUserInfo(this).getUserName());
            username.setVisibility(GONE);
        }
    }

    //initializing views
    private void initView() {
        context = this;
        etPassword = findViewById(R.id.etPassword);
        linearLayout=findViewById(R.id.linear_layout);


        etUserName = findViewById(R.id.etUserName);
        userNameLayout = findViewById(R.id.etUserNameLayout);
        passwordLayout = findViewById(R.id.etPasswordLayout);
        tvRegistration = findViewById(R.id.tvRegistration);
        tvLogin = findViewById(R.id.tvLogin);
        tvRegistrationText = findViewById(R.id.tv_Registration);
        tvForgetPwd = findViewById(R.id.tvForgetPwd);
        tvLangEnglish = findViewById(R.id.tv_lan_english);
        tvLangHindi = findViewById(R.id.tv_lan_hindi);
        tvLangKanand = findViewById(R.id.tv_lan_kannada);
        tvRegistrationText = findViewById(R.id.tv_Registration);
        cvLogin = findViewById(R.id.cv_login);
        pbLoading = findViewById(R.id.pbLoading);
        rlOtpContainer = findViewById(R.id.rlOtpContainer);
        llDialog = findViewById(R.id.llDialog);
        etOtp = findViewById(R.id.etOtp);
        tvOtpHint = findViewById(R.id.tvOtpHint);
        tvResend = findViewById(R.id.tv_resend);
        tvPvc = findViewById(R.id.tvPvc);
        tvTerms = findViewById(R.id.tvTerms);
        tvTerms.setPaintFlags(tvTerms.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvPvc.setPaintFlags(tvPvc.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvText = findViewById(R.id.tvText);
        ivUserName = findViewById(R.id.ivUserName);
        ivForgetPwd = findViewById(R.id.ivForgetPwd);
        hsv = findViewById(R.id.hsv);
        ivRegistration = findViewById(R.id.ivRegistration);
        rememberMe=findViewById(R.id.rememberMe);
    }

    //otp text watcher
    private void otpTextChangeListener() {
        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    if (s.length() >= 6) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(etOtp.getWindowToken(), 0);
                        if (s.toString().equalsIgnoreCase(user.getOtp())) { // checking otp
                            progressBar = new ProgressBar(context);
                            progressBar.show(getResources().getString(R.string.app_please_wait_text));
                            verifyOtp();
                        } else {
                            final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.otp_incorrect_input), Toast.LENGTH_SHORT);
                            snackbar.show();
                        }
                    }
                    tvOtpHint.setVisibility(GONE);
                } else {
                    tvOtpHint.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //verify otp method
    private void verifyOtp() {
        try {
            if (!Utility.isConnectingToInternet(context)) { // check net connection
                if (progressBar != null) {
                    progressBar.dismiss(); // dismissing progressbar
                }
                final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            HashMap<String, Object> input = new HashMap<>();
            input.put(RequestParameters.USERID, "" + user.getUserId());
            API.sendRequestToServerPOST_PARAM(context, API.VERIFY, input); // service call for account verification

        } catch (Exception e) {
            final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.otp_incorrect_input), Toast.LENGTH_SHORT);
            snackbar.show();
            e.printStackTrace();
        }
    }




    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
//This Methods is Written By Arpit Kanda
// For Save Remember Always UserName and UserPassword During Login

    private void rememberUserPasswordSave(String userName,String password){
        SharedPreferences settings = context.getSharedPreferences("RememberMe", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("REMEMBER_USERNAME", userName);
        editor.putString("REMEMBER_PASSWORD",password);
        editor.commit();
    }


    private void rememberUserPasswordGet(){
        SharedPreferences settings = context.getSharedPreferences("RememberMe", Context.MODE_PRIVATE);
        String user_name= settings.getString("REMEMBER_USERNAME", "");
        String user_password= settings.getString("REMEMBER_PASSWORD", "");
        if (user_name.isEmpty()||user_password.isEmpty()){
            etUserName.setText("");
            etPassword.setText("");
            rememberMe.setChecked(false);
        }else{
            rememberMe.setChecked(true);
            etUserName.setText(user_name);
            etPassword.setText(user_password);
        }
    }


    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/




    //set click listeners on views
    private void setClickListener() {
        tvRegistration.setOnClickListener(this);
        tvRegistrationText.setOnClickListener(this);
        linearLayout.setOnClickListener(this);



        tvForgetPwd.setOnClickListener(this);
        cvLogin.setOnClickListener(this);
        tvLangEnglish.setOnClickListener(this);
        tvLangHindi.setOnClickListener(this);
//        tvLangKanand.setOnClickListener(this);
        tvResend.setOnClickListener(this);
        tvPvc.setOnClickListener(this);
        tvTerms.setOnClickListener(this);
        ivUserName.setOnClickListener(this);
        ivForgetPwd.setOnClickListener(this);
        ivRegistration.setOnClickListener(this);
        hsv.setOnClickListener(this);
    }

    //textwatcher method for all edittext
    private void setTextWatcher(EditText editText, final TextInputLayout textInputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    textInputLayout.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //getting response from server
    public void getResponse(JSONObject outPut, int type) {
        try {
            tvLogin.setVisibility(VISIBLE);
            pbLoading.setVisibility(GONE);
            if (type == 0) { // response for login service call

                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(cvLogin, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                        setEnable();
                    } else {
                        if (!delete) {
                            parseResponse(outPut, type);
                        } else {
                            Intent mIntent = new Intent();
                            mIntent.putExtra("reason", deleteAccountReason);
                            setResult(RESULT_OK,mIntent);
                            finish();
                        }
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { // error
                    setEnable();
                    final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { // no network
                    setEnable();
                    final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            } else if (type == 1) {// response for verify service call
                if (progressBar != null)
                    progressBar.dismiss();
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(cvLogin, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        parseResponse(outPut, type);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { // error
                    final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { // no network
                    final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            } else if (type == 2) { // response for resend otp
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(cvLogin, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        user.setOtp(outPut.getString(ResponseParameters.Otp)); // updating otp
                        //SharedPreferencesMethod.setUserDetails(this, user);
                        final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.verify_otp_desc2_text), Toast.LENGTH_SHORT);
                        snackbar.show();
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { // error
                    final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { // no net connection
                    final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        } catch (Exception e) {
            setEnable();
            e.printStackTrace();
        }
    }


    //parse response
    private void parseResponse(JSONObject json, int type) {
        try {
            if (json.has(ResponseParameters.Userdata)) { // checking response have user data
                user = new User();
                JSONObject USERDATA = json.getJSONObject(ResponseParameters.Userdata);

                if (USERDATA.has(ResponseParameters.IS_BLOCKED)) {
                    boolean blocked = USERDATA.getString(ResponseParameters.IS_BLOCKED).equalsIgnoreCase("1");
                    if (blocked) {
                        setEnable();
                        final Snackbar snackbar = Snackbar.make(cvLogin, "User is blocked by admin", Toast.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }
                }

                if (USERDATA.has(ResponseParameters.IS_DELETED)) {
                    boolean deleted = USERDATA.getString(ResponseParameters.IS_DELETED).equalsIgnoreCase("1");
                    if (deleted) {
                        setEnable();
                        final Snackbar snackbar = Snackbar.make(cvLogin, "User Account is deleted", Toast.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }
                }

                user.setUserName(USERDATA.getString(ResponseParameters.UserName));
                user.setUserId(USERDATA.getString(ResponseParameters.UserId));
                user.setMobileNumber(USERDATA.getString(ResponseParameters.MobileNumber));
                user.setReferenceId(USERDATA.getString(ResponseParameters.ReferenceId));
                user.setOtp(USERDATA.getString(ResponseParameters.Otp));
                user.setIsConfirmed(USERDATA.getString(ResponseParameters.IsConfirmed));
                user.setSet1Questions(json.getString(ResponseParameters.Set1Questions));
                user.setSet2Questions(json.getString(ResponseParameters.Set2Questions));

                if (json.has(ResponseParameters.PersonalInfo)) { // checking user have personal info
                    try {
                        JSONObject PERSONALINFO = json.getJSONObject(ResponseParameters.PersonalInfo);
                        user.setFirstName(PERSONALINFO.getString(ResponseParameters.FirstName));  user.setMiddleName(PERSONALINFO.getString(ResponseParameters.MiddleName));
                        user.setMiddleName(PERSONALINFO.getString(ResponseParameters.MiddleName));
                        user.setLastName(PERSONALINFO.getString(ResponseParameters.LastName));
                        user.setDOB(PERSONALINFO.getString(ResponseParameters.DateOfBirth));
                        user.setGender(PERSONALINFO.getString(ResponseParameters.Gender));
                        user.setEmail(PERSONALINFO.getString(ResponseParameters.Email));
                        user.setAvatar(PERSONALINFO.getString(ResponseParameters.Avatar));
                        user.setAddress(PERSONALINFO.getString(ResponseParameters.Address));
                        user.setCity(PERSONALINFO.getString(ResponseParameters.City));
                        user.setState(PERSONALINFO.getString(ResponseParameters.State));
                        user.setZipCode(PERSONALINFO.getString(ResponseParameters.ZIP));
                        user.setEmailVisibility(PERSONALINFO.getString(ResponseParameters.EMAIL_VISIBILITY));
                        user.setMobileVisibility(PERSONALINFO.getString(ResponseParameters.MOBILE_VISIBILITY));
                        user.setAddressVisibility(PERSONALINFO.getString(ResponseParameters.ADDRESS_VISIBILITY));

                        user.setDobVisibility(PERSONALINFO.getString(ResponseParameters.DOB_VISIBILITY));
                        user.setGenderVisibility(PERSONALINFO.getString(ResponseParameters.GENDER_VISIBILITY));
                        if (PERSONALINFO.has(ResponseParameters.AlternateMobile))
                            if (!PERSONALINFO.getString(ResponseParameters.AlternateMobile).equalsIgnoreCase("null") && PERSONALINFO.getString(ResponseParameters.AlternateMobile) != null)
                                user.setAlternateMobile(PERSONALINFO.getString(ResponseParameters.AlternateMobile));
                        if (PERSONALINFO.has(ResponseParameters.PROFILE_SUMMERY))
                            if (!PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY).equalsIgnoreCase("null") && PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY) != null)
                                user.setProfileSummary(PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY));

                        if (PERSONALINFO.has(ResponseParameters.PROFILE_HEADLINE))
                            if (!PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE).equalsIgnoreCase("null") && PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE) != null)
                                user.setProfileHeadline(PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                if (user.getIsConfirmed().equalsIgnoreCase("0") && !json.getBoolean(ResponseParameters.ChangeLoginInfo)) {// checking user is verified and change login info flag
                    rlOtpContainer.setVisibility(VISIBLE);
                    final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.verify_otp_desc2_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else {
                    Intent intent;
                    if (json.has(ResponseParameters.LANGUAGES)) {
                        String langauge = "";
                        for (int j = 0; j < json.getJSONArray(ResponseParameters.LANGUAGES).length(); j++) {
                            langauge = langauge + json.getJSONArray(ResponseParameters.LANGUAGES).getJSONObject(j).getString(ResponseParameters.LANGUAGE).trim() + ", ";
                        }
                        SharedPreferencesMethod.setString(this, SharedPreferencesMethod.LANGUAGES, langauge.trim());
                    }

                    if (json.getBoolean(ResponseParameters.ChangeLoginInfo)) { // checking check login info
                        intent = new Intent(context, UpdateLoginInfoActivity.class);
                        intent.putExtra(ResponseParameters.Userdata, user);
                        intent.putExtra(RequestParameters.USERNAME, etUserName.getText().toString().trim());
                        intent.putExtra(RequestParameters.PASSWORD, etPassword.getText().toString().trim());
                    } else if (json.getBoolean(ResponseParameters.FirstLogin)) { // checking first login flag
                        intent = new Intent(context, MyProfileActivity.class);
                        intent.putExtra(ResponseParameters.Userdata, user);
                    } else { // redirect to home screen

                        if (json.has(ResponseParameters.USER_NOTES)) {
                            //for saving note list
                            JSONArray NOTELIST = new JSONArray();
                            for (int noteCount = 0; noteCount < json.getJSONArray(ResponseParameters.USER_NOTES).length(); noteCount++) {
                                try {
                                    JSONObject noteList = json.getJSONArray(ResponseParameters.USER_NOTES).getJSONObject(noteCount);
                                    Note note = new Note();
                                    note.setNoteID(noteList.getString(ResponseParameters.Id));
                                    note.setNoteTitle(noteList.getString(ResponseParameters.TITLE));
                                    note.setNoteDes(noteList.getString(ResponseParameters.DESCRIPTION));
                                    note.setNoteDate(noteList.getString(ResponseParameters.Date));
                                    note.setNoteTime(noteList.getString(ResponseParameters.TIME));
                                    Calendar cal = Calendar.getInstance();
                                    String date = note.getNoteDate() + " " + note.getNoteTime();
                                    SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    cal.setTime(_24HourSDF.parse(date));
                                    Intent intentNote = new Intent(getBaseContext(), NotifyReminder.class);
                                    //intent.putExtra("NOTE", note);

                                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                    ObjectOutputStream out = null;
                                    try {
                                        out = new ObjectOutputStream(bos);
                                        out.writeObject(note);
                                        out.flush();
                                        byte[] data = bos.toByteArray();
                                        intentNote.putExtra("NOTE", data);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } finally {
                                        try {
                                            bos.close();
                                        } catch (IOException ex) {
                                            ex.printStackTrace();
                                        }
                                    }

                                    intentNote.setAction(note.getNoteID());
                                    PendingIntent pendingIntent =
                                            PendingIntent.getBroadcast(this,
                                                    Integer.parseInt(note.getNoteID()), intentNote, PendingIntent.FLAG_UPDATE_CURRENT);
                                    AlarmManager alarmManager =
                                            (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                                    //note list
                                    JSONObject noteObject = new JSONObject();
                                    noteObject.put(ResponseParameters.Id, note.getNoteID());
                                    NOTELIST.put(noteObject);


                                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                        alarmManager.set(AlarmManager.RTC_WAKEUP,
                                                cal.getTimeInMillis(), pendingIntent);

                                    } else {
                                        alarmManager.setExact(AlarmManager.RTC_WAKEUP,
                                                cal.getTimeInMillis(), pendingIntent);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            //setting note list
                            SharedPreferencesMethod.setString(this, ResponseParameters.NOTELIST, NOTELIST.toString());
                        }


                        SharedPreferencesMethod.setUserDetails(this, user);
                        intent = new Intent(context, HomeScreenActivity.class);

                        //Log.e("mobile number", "number "+user.getMobileNumber());

                        /*intent = new Intent(context, UpdateLoginInfoActivity.class);
                        intent.putExtra(ResponseParameters.Userdata, user);
                        intent.putExtra(RequestParameters.USERNAME, etUserName.getText().toString().trim());
                        intent.putExtra(RequestParameters.PASSWORD, etPassword.getText().toString().trim());*/
                    }
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    SharedPreferencesMethod.setInt(getApplicationContext(), "run", 5);
                    startActivity(intent);
                    this.finish();
                }
            } else {
                if (type == 0) {
                    setEnable();
                    final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (type == 1) {
                    final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.otp_incorrect_input), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //enable all views
    void setEnable() {
        cvLogin.setClickable(true);
        etPassword.setEnabled(true);
        etUserName.setEnabled(true);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()) {
            case R.id.tv_Registration:
            case R.id.tvRegistration: // open registration screen
                //i = new Intent(context, SignupActivity.class);
               // startActivity(i);
                Utils.getLang(this);
                break;
            case R.id.tvForgetPwd: // open forget password screen
                i = new Intent(context, ForgetPasswordActivity.class);
                startActivity(i);
                break;
            case R.id.cv_login: // submit login credentials
                if (validation()) { // validation
                    try {
                        if (!Utility.isConnectingToInternet(context)) { // check net connection
                            final Snackbar snackbar = Snackbar.make(v, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }

                        HashMap<String, Object> input = new HashMap<>();
                        input.put(RequestParameters.PASSWORD, "" + etPassword.getText().toString().trim());
                        input.put(RequestParameters.USERNAME, "" + etUserName.getText().toString().trim());
                        String token = FirebaseInstanceId.getInstance().getToken();
                        input.put(RequestParameters.TOKEN, "" + token);


                        Log.e("input", "" + input);
                        tvLogin.setVisibility(GONE);
                        pbLoading.setVisibility(VISIBLE);
                        cvLogin.setClickable(false);
                        etPassword.setEnabled(false);
                        etUserName.setEnabled(false);
                        API.sendRequestToServerPOST_PARAM(context, API.SIGN_IN, input); // service call for login
                        if (rememberMe.isChecked()){
                            rememberUserPasswordSave(etUserName.getText().toString().trim(),etPassword.getText().toString().trim());//remember me username and password
                        }else{
                            rememberUserPasswordSave("","");//not remember username and password
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.tv_resend: // resend otp
                try {
                    if (!Utility.isConnectingToInternet(context)) { // checking net connections
                        return;
                    }
                    HashMap<String, Object> input = new HashMap<>();
                    input.put(RequestParameters.MOBILE, "" + user.getMobileNumber());
                    input.put(RequestParameters.USERID, "" + user.getUserId());
                    input.put(RequestParameters.LANG, SharedPreferencesMethod.getDefaultLanguageName(this));
                    API.sendRequestToServerPOST_PARAM(context, API.RESEND_OTP, input); // service call for resend otp
                } catch (Exception e) {
                    final Snackbar snackbar = Snackbar.make(cvLogin, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                    e.printStackTrace();
                }
                break;
            case R.id.tv_lan_english: // change language to english
                languageToLoad = "en"; // your language
                SharedPreferencesMethod.setDefaultLanguage(getApplicationContext(), languageToLoad);
//                System.out.println("Language To Load  "+SharedPreferencesMethod.getDefaultLanguageName(this));
//                SharedPreferencesMethod.setString(context, SharedPreferencesMethod.DEFAULT_LANGUAGE, languageToLoad);
                changeLocale(languageToLoad);
//                changeKeyBoard();
                break;
            case R.id.tv_lan_hindi:// change language to hindi
                languageToLoad = "hi"; // your language
                SharedPreferencesMethod.setDefaultLanguage(getApplicationContext(), languageToLoad);
//                SharedPreferencesMethod.setString(context, SharedPreferencesMethod.DEFAULT_LANGUAGE, languageToLoad);
                changeLocale(languageToLoad);
                changeKeyBoard();

            case R.id.tv_lan_kannada:// change language to hindi
                languageToLoad = "kn"; // your language
                SharedPreferencesMethod.setDefaultLanguage(getApplicationContext(), languageToLoad);
//                SharedPreferencesMethod.setString(context, SharedPreferencesMethod.DEFAULT_LANGUAGE, languageToLoad);
                changeLocale(languageToLoad);
                changeKeyBoard();
                break;
            case R.id.tvTerms: // terms and condition view
                if (SharedPreferencesMethod.getDefaultLanguage(this).trim().equalsIgnoreCase("en"))
                    Utility.prepareCustomTab(this, "" + API.termsUrl + "english");
                else
                    Utility.prepareCustomTab(this, "" + API.termsUrl + "english");

                break;
            case R.id.tvPvc: // privacy view
                /*InputMethodManager immo = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                immo.hideSoftInputFromWindow(etPassword.getWindowToken(), 0);
                immo.hideSoftInputFromWindow(etUserName.getWindowToken(), 0);
                immo.hideSoftInputFromWindow(etCPassword.getWindowToken(), 0);
                immo.hideSoftInputFromWindow(etUserPhone.getWindowToken(), 0);
                tvText.setText(Html.fromHtml(getResources().getString(R.string.privacy_policy_text)));
                llDialog.setVisibility(VISIBLE);*/

                if (SharedPreferencesMethod.getDefaultLanguage(this).trim().equalsIgnoreCase("en"))
                    Utility.prepareCustomTab(this, "" + API.privacyUrl + "english");
                else
                    Utility.prepareCustomTab(this, "" + API.privacyUrl + "english");

                break;
            case R.id.ivUserName:
                showToolTip(v, getResources().getString(R.string.login_username_helptext));
                break;
            case R.id.ivForgetPwd:
                showToolTip(v, getResources().getString(R.string.login_forget_pwd_helptext));
                break;
            case R.id.ivRegistration:
                showToolTip(v, getResources().getString(R.string.register_new_account_help_text));
                break;

            case R.id.linear_layout:
                System.out.println("click========");
                Utils.getLang(this);
                break;
        }
    }

    void showToolTip(View view, String toolTipText) {
        if (v != null) {
            v.close();
        }
        v = ViewTooltip
                .on(view)
                .autoHide(true, 20000)
                .clickToHide(true)
                .align(START)
                .position(TOP)
                .text(toolTipText)
                .textColor(Color.WHITE)
                .color(getResources().getColor(R.color.colorPrimary))
                .corner(10)
                .onHide(new ViewTooltip.ListenerHide() {
                    @Override
                    public void onHide(View view) {
                        v = null;
                    }
                });
        v.show();
    }

    //show change keyboard dialog
    void changeKeyBoard() {
        InputMethodManager immo = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        immo.hideSoftInputFromWindow(etPassword.getWindowToken(), 0);
        immo.hideSoftInputFromWindow(etUserName.getWindowToken(), 0);
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(getResources().getString(R.string.app_alert_text));
        dialogBuilder.setMessage(getResources().getString(R.string.keyboard_change_message));
        dialogBuilder.setCancelable(true);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.app_select_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                imeManager.showInputMethodPicker();
                dialog.dismiss();
            }
        });

        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                dialog.dismiss();
            }
        });

        dialogBuilder.create().show();
    }

    //backpress method
    @Override
    public void onBackPressed() {
        if (llDialog.getVisibility() == View.VISIBLE) {
            llDialog.setVisibility(GONE);
        } else {
            super.onBackPressed();
        }
    }

    //validation method
    private boolean validation() {
        /*if (etUserName.getText().toString().trim().isEmpty() && etPassword.getText().toString().trim().isEmpty()) {
            userNameLayout.setErrorEnabled(true);
            passwordLayout.setErrorEnabled(true);
            userNameLayout.setError(getResources().getString(R.string.user_name_error));
            passwordLayout.setError(getResources().getString(R.string.password_error));
            return false;
        } else */
        boolean result = true;
        passwordLayout.setErrorEnabled(true);
        userNameLayout.setErrorEnabled(true);
        if (etUserName.getText().toString().trim().isEmpty()) {
            userNameLayout.setError(getResources().getString(R.string.username_empty_input));
            result = false;
        } else {
            userNameLayout.setErrorEnabled(false);
        }
        if (etPassword.getText().toString().trim().isEmpty()) {
            passwordLayout.setError(getResources().getString(R.string.password_empty_input));
            result = false;
        } /*else if (etPassword.getText().toString().trim().length() < 8) {
            passwordLayout.setError(getResources().getString(R.string.password_error_length));
            result = false;
        } */ else {
            passwordLayout.setErrorEnabled(false);
        }
        if (result) {
            userNameLayout.setErrorEnabled(false);
            passwordLayout.setErrorEnabled(false);
            result = true;
        }
        return result;
    }

    //change locale
    private void changeLocale(String languageToLoad) {
        locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setView();

    }

     public void getLang(View view) {
        Utils.getLang(this);
     }
}
