package com.codeholic.kotumb.app.View;

import android.app.ProgressDialog;
import android.content.Context;

import com.codeholic.kotumb.app.R;

/**
 * Created by hp on 06/08/2016.
 */
public class ProgressBar {
    private Context context;
    private ProgressDialog progressDialog;

    public ProgressBar(Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(context, R.style.progress);
        progressDialog.setCancelable(false);
        /*TextView view = (TextView) progressDialog.findViewById(android.R.id.message);
        Typeface face = Typeface.createFromAsset(context.getAssets(),
                IConstants.TYPEFACE);
        view.setTypeface(face);*/

    }

    public void show(String message) {
        if (progressDialog != null && !progressDialog.isShowing()) {

            progressDialog.setMessage(message);
            progressDialog.show();

        }
    }

    public void dismiss() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

}
