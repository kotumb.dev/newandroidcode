package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Adapter.MyListAdapter;
import com.codeholic.kotumb.app.Interface.OnGetViewListener;
import com.codeholic.kotumb.app.Model.DisplayUserObject;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.EventNames;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivityDisplayUser extends AppCompatActivity {

    public static final String SKILL_DEVS = "skill_devs";
    private static String newText = "";
    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.searchview)
    SearchView searchview;

    @BindView(R.id.rvUserList)
    ListView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.no_data)
    TextView no_data;

    int pagination = 0;

    Activity context;
    ArrayList<DisplayUserObject> userObjects = new ArrayList<>();
    private boolean flag_loading;
    private View footerView;
    private MyListAdapter adapter;
    private boolean logged;
    //UserRecyclerViewAdapter userRecyclerViewAdapter;

    //oncreate method for setting content view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_display_user);
        ButterKnife.bind(this);
        init();
        initToolbar();
        searchViewSetUp();

        setOnLoadMoreListener(rvUserList, new OnGetViewListener() {
            @Override
            public View onGetView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater inflater = getLayoutInflater();
                footerView = inflater.inflate(R.layout.item_loading, viewGroup, false);
                //rvUserList.addFooterView(footerView);
                pagination = pagination + 8;
                SearchActivityDisplayUser context = SearchActivityDisplayUser.this;
                String userId = SharedPreferencesMethod.getUserId(context);
                try{
                    String result=newText.substring(0,1);
                    if (result.equalsIgnoreCase("#")){
                        String newString =newText.startsWith("#") ? newText.substring(1) : newText;
                        String url = API.SKILL_DEV + pagination + "/" + userId + "/"+"?tag="+newString;
                        System.out.println("Skill Url#  "+url);
                        API.sendRequestToServerGET(context, url, API.SKILL_DEV + 1);
                    }else{
                        String url = API.SKILL_DEV + pagination + "/" + userId + "/"+newText;
                        System.out.println("Skill Url  "+url);
                        API.sendRequestToServerGET(context, url, API.SKILL_DEV + 1);
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }
                return null;
            }
        });
    }

    private void setOnLoadMoreListener(final ListView listView, final OnGetViewListener listener) {
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {


            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if (!flag_loading) {
                        flag_loading = true;
                        listener.onGetView(0, null, null);
                    }
                }

//                int topRowVerticalPosition = (listView== null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();
//                //swipelayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });
    }

    //setting toolbar and header title
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //init values and show user details
    private void init() {
        context = this;
    }

    //search view setup
    private void searchViewSetUp() {
        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                flag_loading = false;
                pagination = 0;
                rvUserList.setAdapter(getAdapter());
                SearchActivityDisplayUser.newText = newText;
                boolean b = !newText.isEmpty();
//                b = true;

                if (b) {
                    try{
                        String result=newText.substring(0,1);
                        String newString = newText.startsWith("#") ? newText.substring(1) : newText;
                        System.out.println("Search Text 2   "+newText);
                        if (result.equalsIgnoreCase("#")){
                            String url = API.SKILL_DEV + pagination + "/" + SharedPreferencesMethod.getUserId(SearchActivityDisplayUser.this) + "/" +"?tag="+ newString;
                            API.sendRequestToServerGET(context, url, API.SKILL_DEV); // service call for user list
                            System.out.println("Skill Url2#   "+url);
                            no_data.setVisibility(View.GONE);
                            llLoading.setVisibility(View.VISIBLE);
                            userObjects.clear();
                        }else{
                            String url = API.SKILL_DEV + pagination + "/" + SharedPreferencesMethod.getUserId(SearchActivityDisplayUser.this) + "/"+ newText;
                            API.sendRequestToServerGET(context, url, API.SKILL_DEV); // service call for user list
                            System.out.println("Skill Url2   "+url);
                        }

                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                    no_data.setVisibility(View.GONE);
                    llLoading.setVisibility(View.VISIBLE);
                    userObjects.clear();
                }
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    //getting response from server
    public void getResponse(JSONObject outPut, int i) {
        try {
            Log.e("response", outPut.toString());
            if (i == 0) { // get search user list
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(rvUserList, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else if (outPut.has(ResponseParameters.Success)) {

                        //Logging FireBase Events
                        if (!logged) {
                            Utils.logEvent(EventNames.SKILL_DEV_SEARCH, new Bundle(), this);
                            logged = true;
                        }

                        if (outPut.has(SKILL_DEVS)) {
                            JSONArray outPutJSONArray = outPut.getJSONArray(SKILL_DEVS);
                            userObjects.clear();
                            userObjects.addAll(Utility.getDisplayUserObjects(outPutJSONArray));
                            llLoading.setVisibility(View.GONE);
                            adapter = getAdapter();
                            rvUserList.setAdapter(adapter);
                            if (userObjects.size() == 0) {
                                no_data.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        final Snackbar snackbar = Snackbar.make(rvUserList, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    }
                }
            }
            if (i == 1) { // load more response
                flag_loading = false;
                rvUserList.removeFooterView(footerView);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(rvUserList, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else if (outPut.has(ResponseParameters.Success)) {
                        if (outPut.has(SKILL_DEVS)) {
                            JSONArray outPutJSONArray = outPut.getJSONArray(SKILL_DEVS);
                            ArrayList<DisplayUserObject> displayUserObjects = Utility.getDisplayUserObjects(outPutJSONArray);
                            userObjects.addAll(displayUserObjects);
                            llLoading.setVisibility(View.GONE);
                            adapter.notifyDataSetChanged();
                            if (displayUserObjects.size() == 0) {
                                flag_loading = true;
                            }
                        }
                    } else {
                        final Snackbar snackbar = Snackbar.make(rvUserList, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    private MyListAdapter getAdapter() {
        return new MyListAdapter(userObjects, new OnGetViewListener() {
            @Override
            public View onGetView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.skill_dev_item, viewGroup, false);
                ImageView ivSkillDevImage = v.findViewById(R.id.ivSkillDevImage);
                TextView tvSkillDevName = v.findViewById(R.id.tvSkillDevName);
                TextView tvSkillDevCount = v.findViewById(R.id.tvSkillDevCount);
                TextView tvSkillDevSummery = v.findViewById(R.id.tvSkillDevSummery);

                final DisplayUserObject userObject = userObjects.get(i);

                String text = userObject.getFirstName() + " " + userObject.getMiddleName() + " " + userObject.getLastName();
                tvSkillDevName.setText(text.replaceAll("  ", " "));
                tvSkillDevSummery.setText(userObject.getCity() + "");
                String likes = getString(R.string.like_btn_text);
                String comments = getString(R.string.comment_btn_text);
                tvSkillDevCount.setText(userObject.getLikes_count() + " " + likes + " · " + userObject.getComments_count() + " " + comments);
                String url = API.imageUrl + userObject.getAvatar().trim();
                new AQuery(SearchActivityDisplayUser.this).id(ivSkillDevImage).image(url, true, true, 300, R.drawable.ic_user);
                v.setOnClickListener(getItemClickListener(userObject));
                return v;
            }
        });
    }

    @NonNull
    private View.OnClickListener getItemClickListener(final DisplayUserObject userObject) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchActivityDisplayUser.this, OrganizationActivity.class);
                intent.putExtra("data", new Gson().toJson(userObject));
                startActivity(intent);
            }
        };
    }
}
