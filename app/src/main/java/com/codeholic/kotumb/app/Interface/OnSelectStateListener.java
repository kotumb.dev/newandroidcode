package com.codeholic.kotumb.app.Interface;

public interface OnSelectStateListener<T> {
    void OnSelectStateChanged(boolean state, T file);
}
