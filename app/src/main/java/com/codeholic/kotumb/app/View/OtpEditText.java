package com.codeholic.kotumb.app.View;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Editable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import com.codeholic.kotumb.app.R;

public class OtpEditText extends EditText {
    public static final String XML_NAMESPACE_ANDROID = "http://schemas.android.com/apk/res/android";
    private float mCharSize;
    private OnClickListener mClickListener;
    int[][] mStates = new int[][]{
            new int[]{android.R.attr.state_selected}, // selected
            new int[]{android.R.attr.state_focused}, // focused
            new int[]{-android.R.attr.state_focused}, // unfocused
    };
    int[] mColors = new int[]{
            Color.GREEN,
            Color.BLACK,
            Color.GRAY
    };
    ColorStateList mColorStates = new ColorStateList(mStates, mColors);

    private float mLineSpacing = 8.0f;
    private float mLineStroke = 1.0f;
    private float mLineStrokeSelected = 2.0f;
    private Paint mLinesPaint;
    private int mMaxLength = 6;
    private float mNumChars = 4.0f;
    private float mSpace = 10.0f;

    class C04891 implements Callback {
        C04891() {
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public void onDestroyActionMode(ActionMode mode) {
        }

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            return false;
        }
    }

    class C04902 implements OnClickListener {
        C04902() {
        }

        public void onClick(View v) {
            OtpEditText.this.setSelection(OtpEditText.this.getText().length());
            if (OtpEditText.this.mClickListener != null) {
                OtpEditText.this.mClickListener.onClick(v);
            }
        }
    }

    public OtpEditText(Context context) {
        super(context);
    }

    public OtpEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public OtpEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(21)
    public OtpEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        float multi = context.getResources().getDisplayMetrics().density;
        this.mLineStroke *= multi;
        this.mLineStrokeSelected *= multi;
        this.mLinesPaint = new Paint(getPaint());
        this.mLinesPaint.setStrokeWidth(this.mLineStroke);
        if (!isInEditMode()) {
            TypedValue outValue = new TypedValue();
            context.getTheme().resolveAttribute(R.attr.colorPrimaryDark, outValue, true);
            this.mColors[0] = outValue.data;
        }
        setBackgroundResource(0);
        this.mSpace *= multi;
        this.mLineSpacing *= multi;
        this.mMaxLength = attrs.getAttributeIntValue("http://schemas.android.com/apk/res/android", "maxLength", 4);
        this.mNumChars = (float) this.mMaxLength;
        super.setCustomSelectionActionModeCallback(new C04891());
        super.setOnClickListener(new C04902());
    }

    public void setOnClickListener(OnClickListener l) {
        this.mClickListener = l;
    }

    public void setCustomSelectionActionModeCallback(Callback actionModeCallback) {
        throw new RuntimeException("setCustomSelectionActionModeCallback() not supported.");
    }

    protected void onDraw(Canvas canvas) {
        int availableWidth = (getWidth() - getPaddingRight()) - getPaddingLeft();
        if (this.mSpace < 0.0f) {
            this.mCharSize = ((float) availableWidth) / ((this.mNumChars * 2.0f) - 1.0f);
        } else {
            this.mCharSize = (((float) availableWidth) - (this.mSpace * (this.mNumChars - 1.0f))) / this.mNumChars;
        }
        int startX = getPaddingLeft();
        int bottom = getHeight() - getPaddingBottom();
        Editable text = getText();
        int textLength = text.length();
        float[] textWidths = new float[textLength];
        getPaint().getTextWidths(getText(), 0, textLength, textWidths);
        int i = 0;
        while (((float) i) < this.mNumChars) {
            updateColorForLines(i == textLength);
            canvas.drawLine((float) startX, (float) bottom, this.mCharSize + ((float) startX), (float) bottom, this.mLinesPaint);
            if (getText().length() > i) {
                canvas.drawText(text, i, i + 1, (((float) startX) + (this.mCharSize / 2.0f)) - (textWidths[0] / 2.0f), ((float) bottom) - this.mLineSpacing, getPaint());
            }
            if (this.mSpace < 0.0f) {
                startX = (int) (((float) startX) + (this.mCharSize * 2.0f));
            } else {
                startX = (int) (((float) startX) + (this.mCharSize + this.mSpace));
            }
            i++;
        }
    }

    private int getColorForState(int... states) {
        return this.mColorStates.getColorForState(states, -7829368);
    }

    private void updateColorForLines(boolean next) {
        if (isFocused()) {
            this.mLinesPaint.setStrokeWidth(this.mLineStrokeSelected);
            this.mLinesPaint.setColor(getColorForState(16842908));
            if (next) {
                this.mLinesPaint.setColor(getColorForState(16842913));
                return;
            }
            return;
        }
        this.mLinesPaint.setStrokeWidth(this.mLineStroke);
        this.mLinesPaint.setColor(getColorForState(-16842908));
    }
}
