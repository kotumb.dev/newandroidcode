package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Model.Invitations;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.DateUtils;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.View.CircleImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InvitationDetails extends AppCompatActivity {

    @BindView(R.id.invitationRecyclerview)
    RecyclerView invitationRecycler;
    @BindView(R.id.progressLayout)
    ConstraintLayout progressLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.header)
    TextView header;

    InvitationAdapter invitationAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitations);
        ButterKnife.bind(this);
        initToolbar();

        progressLayout.setVisibility(View.VISIBLE);
        API.sendRequestToServerGET(this,API.REFERREL_DETAILS+"/"+SharedPreferencesMethod.getUserId(this),API.REFERREL_DETAILS);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        invitationRecycler.setLayoutManager(linearLayoutManager);

        invitationAdapter = new InvitationAdapter(this,new ArrayList<>());
        invitationRecycler.setAdapter(invitationAdapter);
    }

    private void initToolbar(){
        setSupportActionBar(mToolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.invitation_details));

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void getResponse(JSONObject output){
        progressLayout.setVisibility(View.GONE);
        if (output.has(ResponseParameters.Success)){
            parseResponse(output);
        }else {
            Toast.makeText(this, "No data found", Toast.LENGTH_LONG).show();
        }
    }

    private void parseResponse(JSONObject jsonObject){

        if (jsonObject.has(ResponseParameters.REFERREL)){
            try {
                JSONArray parent = jsonObject.getJSONArray(ResponseParameters.REFERREL);
                for (int i=0;i<parent.length();i++){
                    JSONObject child = parent.getJSONObject(i);
                    Log.e("INVITATION","Created at is : " + child.getString(ResponseParameters.CREATED_AT));
                    Invitations invitations = new Invitations(child.getString(ResponseParameters.UserName),child.getString(ResponseParameters.UserId),child.getString(ResponseParameters.PROFILE_STRENGTH),child.getString(ResponseParameters.CREATED_AT));
                    invitationAdapter.invitationsList.add(invitations);
                    invitationAdapter.notifyDataSetChanged();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private class InvitationAdapter extends RecyclerView.Adapter<InvitationAdapter.ViewHolder>{

        List<Invitations> invitationsList = new ArrayList<>();
        Activity activity;

        public InvitationAdapter(Activity activity, List<Invitations> invitationsList) {
            this.invitationsList = invitationsList;
            this.activity = activity;
        }

        @NonNull
        @Override
        public InvitationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem= layoutInflater.inflate(R.layout.invitation_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull InvitationAdapter.ViewHolder holder, int position) {

            final Invitations invitations = invitationsList.get(position);
            holder.name.setText(invitations.getUserName());
            new AQuery(activity).id(holder.profileImage).image(API.imageUrl + "default-profile.png", true, true, 300, R.drawable.ic_user);
            // holder.createdDate.setText(invitations.getCreatedAt());

            if (invitations.getProfilePercentage()!=null && !invitations.getProfilePercentage().isEmpty()){
                if (Integer.parseInt(invitations.getProfilePercentage()) >= 75){
                    holder.status.setText("Completed");
                }else {
                    holder.status.setText("Pending");
                }
            }

            String mDateTime = null;
            try {
                mDateTime = DateUtils.formatDateFromDateString(DateUtils.DATE_FORMAT_8, DateUtils.DATE_FORMAT_5+" "+"hh:mm a", invitations.getCreatedAt());
                holder.createdDate.setText(mDateTime);
            } catch (ParseException e) {
                Log.e("INIVITATION","Time & date exception is : " + e.getMessage());
                e.printStackTrace();
            }

           // new AQuery(activity).id(holder.profileImage).image(API.imageUrl + )

        }

        @Override
        public int getItemCount() {
            return invitationsList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            TextView name,createdDate,status;
            CircleImageView profileImage;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);

                profileImage = itemView.findViewById(R.id.ivUserImage);
                name = itemView.findViewById(R.id.tvUserName);
                createdDate = itemView.findViewById(R.id.tvDateText);
                status = itemView.findViewById(R.id.statusText);

            }
        }
    }

}
