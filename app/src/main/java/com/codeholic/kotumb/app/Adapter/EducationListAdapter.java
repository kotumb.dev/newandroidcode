package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Activity.EducationActivity;
import com.codeholic.kotumb.app.Model.Education;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by DELL on 11/17/2017.
 */

public class EducationListAdapter extends RecyclerView.Adapter<EducationListAdapter.educationViewHolder> {
    List<Education> educations;
    Activity activity;
    public boolean menu = true;


    public EducationListAdapter(Activity activity, List<Education> educations) {
        this.educations = educations;
        this.activity = activity;
    }

    @Override
    public int getItemCount() {
        return educations.size();
    }

    @Override
    public educationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.education_list_item, parent, false);
        educationViewHolder professionViewHolder = new educationViewHolder(view);
        return professionViewHolder;
    }


    @Override
    public void onBindViewHolder(educationViewHolder holder, int position) {
        Education education = educations.get(position);
        holder.tvProfessionName.setText(education.getCourse());
        holder.tvCompanyName.setText(education.getSchool());
        holder.tvTime.setText(activity.getResources().getString(R.string.education_passing_year_text) + "-" + education.getYearOfPassing());
        holder.tvProfessionDes.setText(education.getDetails());
        if (menu) {
            holder.tvOptionMenu.setVisibility(View.VISIBLE);
            setOnClickListener(holder, education, position);
        } else {
            holder.tvOptionMenu.setVisibility(View.GONE);
        }
    }

    public void setOnClickListener(final educationViewHolder holder, final Education education, final int position) {
        holder.tvOptionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(activity, holder.tvOptionMenu);
                //inflating menu from xml resource
                popup.inflate(R.menu.options_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.itemEdit:
                                Constants.education_position = position;
                                Intent intent = new Intent(activity, EducationActivity.class);
                                intent.putExtra("DATA", education);
                                activity.startActivity(intent);
                                break;
                            case R.id.itemDelete:
                                onClickDelete(holder, position, education);
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();
            }
        });
        holder.cvMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.education_position = position;
                Intent intent = new Intent(activity, EducationActivity.class);
                intent.putExtra("DATA", education);
                activity.startActivity(intent);
            }
        });
    }

    private void onClickDelete(final educationViewHolder holder, final int position, final Education education) {
        Utils.showPopup(activity, activity.getString(R.string.remove_item), activity.getString(R.string.delete_btn_text), activity.getString(R.string.cancel_btn_text), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delete(holder, position, education);
            }
        }, null);
    }

    private void delete(educationViewHolder holder, int position, Education education) {
        if (!Utility.isConnectingToInternet(activity)) {
            final Snackbar snackbar = Snackbar.make(holder.tvOptionMenu, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
            snackbar.show();
            return;
        }
        educations.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, educations.size());
        sendRequest(activity, API.EDUCATION_DELETE + "" + SharedPreferencesMethod.getUserId(activity) + "/" + education.getId());
    }

    public void sendRequest(final Activity context, String url) {
        try {
            AQuery aq = new AQuery(context);
            //Log.d("Url ", url);
            aq.ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {
                        if (json != null) {
                            Log.e(context.getClass().getName(), json.toString());
                            json.put("RP_MESSAGE", "ALL_OKAY");
                            getResponse(json);
                        } else {
                            JSONObject output = new JSONObject();
                            if (status.getCode() == AjaxStatus.NETWORK_ERROR) {
                                output.put("RP_MESSAGE", "NO NETWORK");
                            } else {
                                output.put("RP_MESSAGE", "ERROR");
                            }
                            getResponse(output);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        getResponse(new JSONObject());
                    }
                }
            }.method(AQuery.METHOD_GET).header("Auth-Key", "KOTUMB__AuTh_keY"));


        } catch (Exception e) {
            e.printStackTrace();
            getResponse(new JSONObject());
        }
    }


    public void getResponse(JSONObject output) {
        try {
            if (output.has(ResponseParameters.Success)) {
//                Toast.makeText(activity, output.getString(ResponseParameters.Success), Toast.LENGTH_SHORT).show();
                Utils.showPopup(activity, output.getString(ResponseParameters.Success));
            } else {
//                Toast.makeText(activity, activity.getResources().getString(R.string.error_text), Toast.LENGTH_SHORT).show();
                Utils.showPopup(activity, output.getString(ResponseParameters.Success));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class educationViewHolder extends RecyclerView.ViewHolder {
        TextView tvProfessionName, tvCompanyName, tvTime, tvProfessionDes;
        ImageView ivImage;
        CardView cvMain;

        LinearLayout tvOptionMenu;

        educationViewHolder(View view) {
            super(view);
            cvMain = (CardView) view.findViewById(R.id.cvMain);
            ivImage = (ImageView) view.findViewById(R.id.ivImage);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            tvProfessionName = (TextView) view.findViewById(R.id.tvProfessionName);
            tvCompanyName = (TextView) view.findViewById(R.id.tvCompanyName);
            tvProfessionDes = (TextView) view.findViewById(R.id.tvProfessionDes);
            tvOptionMenu = (LinearLayout) view.findViewById(R.id.tvOptionMenu);
        }
    }
}
