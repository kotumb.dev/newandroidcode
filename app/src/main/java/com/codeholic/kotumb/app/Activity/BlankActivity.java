package com.codeholic.kotumb.app.Activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

public class BlankActivity extends AppCompatActivity {

    @Override
    public void onStart() {
        super.onStart();
        Branch branch=Branch.getInstance();
        // Branch init
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    Log.i("BRANCH SDK", referringParams.toString());
                    System.out.println("Branchdata=="+referringParams.toString());
                    // Retrieve deeplink keys from 'referringParams' and evaluate the values to determine where to route the user
                    // Check '+clicked_branch_link' before deciding whether to use your Branch routing logic
                    try {
                        if(referringParams.getBoolean("+clicked_branch_link")){
                            Utils.showPopup(BlankActivity.this,"Invited link: "+ referringParams.getString("~referring_link"), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent=new Intent(BlankActivity.this,SplashActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            //if(referringParams.getBoolean("+is_first_session")){

                            //}
                        }
                        else{
                            Intent intent=new Intent(BlankActivity.this,SplashActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.i("BRANCH SDK", error.getMessage());
                    Intent intent=new Intent(BlankActivity.this,SplashActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, this.getIntent().getData(), this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blank);
        /*Intent intent=new Intent(BlankActivity.this,SplashActivity.class);
        startActivity(intent);
        finish();*/
    }
}
