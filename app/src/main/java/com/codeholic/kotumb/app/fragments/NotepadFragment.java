package com.codeholic.kotumb.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Activity.AddNoteActivity;
import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Adapter.ReminderListAdapter;
import com.codeholic.kotumb.app.Model.Note;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;

public class NotepadFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;




    private LinearLayoutManager linearLayoutManager;
    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;
    @BindView(R.id.llLoading)
    LinearLayout llLoading;
    @BindView(R.id.llAddNote)
    CardView llAddNote;
    @BindView(R.id.adView)
    ImageView adView;
    @BindView(R.id.rlAdView)
    RelativeLayout rlAdView;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    List<Note> notes = new ArrayList<>();
    ReminderListAdapter reminderListAdapter;

    //oncreate method for setting content view
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.activity_notepad, container, false);
        ButterKnife.bind(this, view);
        ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.header_main_menu_option8));
        setClickListener();
        setView();
        return view;
    }




    @Override
    public void onResume() {
        super.onResume();
        if (Constants.REMINDER != -1 && Constants.note != null) {
            notes.remove(Constants.REMINDER);
            notes.add(Constants.REMINDER, Constants.note);
            reminderListAdapter.notifyDataSetChanged();
        } else if (Constants.note != null) {
            if (notes.size() > 0) {
                notes.add(Constants.note);
                reminderListAdapter.notifyDataSetChanged();
            } else {
                notes.add(Constants.note);
                linearLayoutManager = new LinearLayoutManager(getActivity());
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                rvUserList.setLayoutManager(linearLayoutManager);
                /*rvUserList.setHasFixedSize(true);
                rvUserList.setNestedScrollingEnabled(false);*/
                reminderListAdapter = new ReminderListAdapter(getActivity(), notes);
                reminderListAdapter.setFragment(this);
                rvUserList.setAdapter(reminderListAdapter);
                llNotFound.setVisibility(View.GONE);
            }
        }
        Constants.REMINDER = -1;
        Constants.note = null;
    }

    private void setClickListener() {
        llAddNote.setOnClickListener(this);
    }

    private void setView() {
        try {
            if (!Utility.isConnectingToInternet(getActivity())) {
                final Snackbar snackbar = Snackbar.make(llAddNote, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(View.VISIBLE);
                return;
            }
            API.sendRequestToServerGET_FRAGMENT(getActivity(), this, API.REMINDER_LIST + SharedPreferencesMethod.getUserId(getActivity()), API.REMINDER_LIST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //initializing toolbar and setting header title
   /* private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.header_main_menu_option8));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }*/

    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        try {
            Log.e("output", outPut.toString());
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Errors)) {
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.has(ResponseParameters.Success)) {
                    if (outPut.has(ResponseParameters.NOTES)) {
                        notes = new ArrayList<>();
                        for (int j = 0; j < outPut.getJSONArray(ResponseParameters.NOTES).length(); j++) {
                            JSONObject noteList = outPut.getJSONArray(ResponseParameters.NOTES).getJSONObject(j);
                            Note note = new Note();
                            note.setNoteID(noteList.getString(ResponseParameters.Id));
                            note.setNoteTitle(noteList.getString(ResponseParameters.TITLE));
                            note.setNoteDes(noteList.getString(ResponseParameters.DESCRIPTION));
                            note.setNoteDate(noteList.getString(ResponseParameters.Date));
                            note.setNoteTime(noteList.getString(ResponseParameters.TIME));
                            notes.add(note);
                        }
                        linearLayoutManager = new LinearLayoutManager(getActivity());
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserList.setLayoutManager(linearLayoutManager);
                        //rvUserList.setHasFixedSize(true);
                        //rvUserList.setNestedScrollingEnabled(false);
                        reminderListAdapter = new ReminderListAdapter(getActivity(), notes);
                        reminderListAdapter.setFragment(this);
                        rvUserList.setAdapter(reminderListAdapter);
                    }
                } else {
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                llNotFound.setVisibility(View.VISIBLE);
            } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            llNotFound.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }

    public void showNotFound() {
        llNotFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llAddNote:
                startActivity(new Intent(getActivity(), AddNoteActivity.class));
                break;
        }
    }
}
