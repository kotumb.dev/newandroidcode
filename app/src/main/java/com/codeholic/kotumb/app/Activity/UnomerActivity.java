package com.codeholic.kotumb.app.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Interface.OnGetViewListener;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.EventNames;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utils;
import com.unomer.sdk.Unomer;
import com.unomer.sdk.UnomerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class UnomerActivity extends AppCompatActivity {


    //new App id and Public ID
   // public static final String PUB_ID = "fzwT6LXmQBgzHH9+qJP1dA==";
   // public static final String APP_ID = "CSAtYF4ep8zQEjmKiNOh1MuWG2DbXg";

        public static final String APP_ID = "u03XqAQpEFMdTthHKwgafjobRJzyxI";

    //Old App id and Public ID
    public static final String PUB_ID = "9OyNihZH3F2xauIia9gaYw==";
  //  public static final String APP_ID = "8ZqrxBtg04OlMKmLT73HWbukXYEpPn";

      //    public static final String APP_ID = "u03XqAQpEFMdTthHKwgafjobRJzyxI";
    Unomer survey;
    UnomerListener uListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unomer_activty);

        uListener = getuListener();
        survey = new Unomer(getApplicationContext(), PUB_ID, APP_ID, uListener);
        findViewById(R.id.msg).setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        survey.destroy();
    }

    public void fetchSurvey() {
        String eParam = "";
        Toast.makeText(this, "Fetch Survey", Toast.LENGTH_SHORT).show();
        survey.fetchSurvey(getApplicationContext(), uListener, eParam);
        survey.showSurvey(this, uListener);
    }

    @NonNull
    private UnomerListener getuListener() {
        return new UnomerListener() {
            @Override
            public void unomerSurveyFetchSuccess(boolean isRewardEnabledOnThisSurvey, int reward, String
                    rewardCurrency, int noOfQuestions, String eParam) {
                // Called when a survey has been fetched/downloaded
                //  Toast.makeText(UnomerActivity.this, "Fetch Successful", Toast.LENGTH_SHORT).show();
                survey.showSurvey(UnomerActivity.this, this);
            }

            @Override
            public void unomerSurveyFetchStarted() {
                // Called when survey fetching/downloading has just started
                // Toast.makeText(UnomerActivity.this, "Fetch Started", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void unomerSurveyFetchFailed(String eMsg, String eParam) {
                // Called when a survey fetch/download request failed
//                if (eMsg.contains("104")) {
                Log.e("unomer", eMsg);
                String no_survey_available = "No Survey Available";
//                String no_survey_available = "No Survey Available+\n["+eMsg+"]";
                Utils.showPopup(UnomerActivity.this, no_survey_available, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        survey.destroy();
                        finish();
                    }
                });
//                } else {
//                    Toast.makeText(UnomerActivity.this, "Fetch failed :: " + eMsg, Toast.LENGTH_SHORT).show();
//                    survey.destroy();
//                    finish();
//                }
            }

            @Override
            public void unomerSurveyDisplayed() {
                // Called when a survey is displayed
                // Toast.makeText(UnomerActivity.this, "Survey Displayed ", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void unomerUploadComplete(int reward, String rewardCurrency, String responseCode,
                                             boolean isRewarded, String eParam) {
                // Called when a survey has been uploaded to Unomer server
                //Toast.makeText(UnomerActivity.this, "Survey Upload Complete ", Toast.LENGTH_SHORT).show();
                String url = API.COMPLETE_SURVEY + SharedPreferencesMethod.getUserId(UnomerActivity.this);
                if (isRewarded) {
                    API.sendRequestToServerGET(UnomerActivity.this, url, API.COMPLETE_SURVEY);
                } else {
                    Utils.showPopup(UnomerActivity.this, "No reward points earned", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                }
                survey.destroy();
                Utils.tookSurvey = true;

                Bundle bundle = new Bundle();
                bundle.putString("UserId", SharedPreferencesMethod.getUserId(UnomerActivity.this));
                Utils.logEvent(EventNames.SURVEY_GIVEN, bundle, UnomerActivity.this);
//                finish();
            }

            @Override
            public void unomerSurveySubmittedForUpload(int reward, String rewardCurrency, String responseCode,
                                                       boolean isRewarded) {
                // Called when a survey has been submitted to upload
                //  Toast.makeText(getApplicationContext(), "SURVEY SUBMITTED FOR UPLOAD ( NOT UPLOADED YET ) ::", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void unomerSurveyClosed() {
                // Called when a survey is closed ( via closed button or back press )
                //   Toast.makeText(UnomerActivity.this, "Survey Displayed ", Toast.LENGTH_SHORT).show();
                survey.destroy();
                finish();
            }

            @Override
            public void unomerSurveyConfirmationDeclined() {
                // Called when user declines an offer to take a survey
                // Note : UnomerActivity.this will happen only if you have NOT disabled the confirmation already
                Toast.makeText(UnomerActivity.this, "Confirmation declined", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void unomerUploadFailed() {
                // Called when uploading of survey response fails ( due to network or other reasons )
                Toast.makeText(UnomerActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                survey.destroy();
                finish();
            }

            @Override
            // Called when user gets disqualified
            public void unomerUserDisqualified(int reward, String rewardCurrency, String responseCode,
                                               boolean isRewarded) {
                System.out.println("DEBUGG :: UNOMER :: user was disqualified");
                survey.destroy();
            }
        };
    }

    public void getResponse(JSONObject outPut, int i) {
        Log.e("output", "" + outPut);
        System.out.println("Survey Response   "+outPut.toString());
        try {
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Success)) {
                    if (outPut.has("survey_reward")){

                        //reward alert
                        if (!outPut.getString("survey_reward").equals("0")){
                        //showPointsEarnDialog(outPut.getString("survey_reward"));
                        }else {
                            finish();
                        }
//                        finish();
                    }
                    if (i == 0) {
                        finish();
                    }
                } else if (outPut.has(ResponseParameters.Errors)) {
//                    Snackbar.make(lladdMembers, Utils.getErrorMessage(outPut), 0).show();
                    Utils.showPopup(UnomerActivity.this, Utils.getErrorMessage(outPut), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                } else {
//                    Snackbar.make(lladdMembers, UnomerActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                    Utils.showPopup(UnomerActivity.this, getResources().getString(R.string.app_no_internet_error), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                }
            } else {
//                Snackbar.make(lladdMembers, UnomerActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                Utils.showPopup(UnomerActivity.this, getResources().getString(R.string.app_no_internet_error), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onClickSurvey(View view) {
//        Utils.surveyTimeInSeconds = 15;
//        Utils.tookSurvey = true;
        if (!Utils.tookSurvey && getTimeElapsed() > Utils.surveyTimeInSeconds) {
            findViewById(R.id.msg).setVisibility(View.GONE);
            fetchSurvey();
        } else {
            showWaitDialog();
        }
    }

    private long getTimeElapsed() {
        long diffInMillis = Calendar.getInstance().getTimeInMillis() - Utils.appStartTime.getTimeInMillis();
        return diffInMillis / 1000;
    }

    private void showWaitDialog() {
        Toast.makeText(this, "Dialog", Toast.LENGTH_SHORT).show();
        final Handler handler = new Handler();
        Utils.showPopupInTextView(UnomerActivity.this, getTimeString(), new OnGetViewListener() {
            @Override
            public View onGetView(int i, final View view, ViewGroup viewGroup) {
                final Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        final String string = getTimeString();
                        final TextView tv = (TextView) view;
                        UnomerActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv.setText(string);
                            }
                        });
                        long timeElapsed = getTimeElapsed();
                        if (Utils.surveyTimeInSeconds - timeElapsed > 0) {
                            handler.postDelayed(this, 1000);
                        }
                    }
                };
                handler.postDelayed(runnable, 1000);
                return null;
            }
        }, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacksAndMessages(null);
            }
        });
    }

    @NonNull
    private String getTimeString() {
        int time = (int) (Utils.surveyTimeInSeconds - getTimeElapsed());
        if (!Utils.tookSurvey) {
            if (time>0) {
                int sec = time % 60;
                int min = time / 60;
                String minStr = min < 10 ? "0" + min : min + "";
                String secStr = sec < 10 ? "0" + sec : sec + "";
                return "Wait for " + minStr + ":" + secStr + " min before you can take survey";
            } else {
                return "You can now take Survey";
            }
        } else {
            return "You already took survey";
        }
    }

    private void showPointsEarnDialog(String pointValue) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.alert_reward_points, viewGroup, false);
        TextView tv_points = dialogView.findViewById(R.id.tv_points);
        TextView tv_ok = dialogView.findViewById(R.id.tv_ok);
        tv_points.setText(getString(R.string.reward_str_pre)+" "+pointValue+" "+getString(R.string.reward_str_post));

        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
