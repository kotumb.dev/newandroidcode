package com.codeholic.kotumb.app.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Activity.ShowVideos;
import com.codeholic.kotumb.app.Adapter.UserVideoListAdapter;
import com.codeholic.kotumb.app.Model.VideoData;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyVideosFragment extends Fragment implements ShowVideos.VideoInterface {

    List<VideoData> videoDataList=new ArrayList<>();
    private Toolbar mToolbar;
    private TextView header,tv_video_notfound;
    int pagination = 0;
    boolean loading=true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private RecyclerView rv_video_list;
    private LinearLayoutManager linearLayoutManager = null;
    private UserVideoListAdapter userVideoListAdapter;
    private LinearLayout llLoading,mainLayout,llDataNotFound;
    private ProgressBar pagination_loader;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String URL="";
    IntentFilter intentFilter;
    View view;
    public int video_position=-1;
    BroadcastReceiver receiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.hasExtra("refresh")){
                videoDataList.clear();
                pagination=0;
                userVideoListAdapter.notifyDataSetChanged();
                rv_video_list.refreshDrawableState();
                rv_video_list.invalidate();
                getResponse();
                ((ShowVideos)getActivity()).SetTab(1);
                ((ShowVideos)getActivity()).RefereshLimit();
            }
        }
    };


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((ShowVideos)getActivity()).registerData(1,(ShowVideos.VideoInterface) this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_user_video_list, container, false);
        findViews();

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_video_list.setLayoutManager(linearLayoutManager);
        userVideoListAdapter = new UserVideoListAdapter(getActivity(), new ArrayList<>());
        userVideoListAdapter.notifyDataSetChanged();
        rv_video_list.setAdapter(userVideoListAdapter);

        if(isNetworkAvailable()) {
            getResponse();
        }
        else{
            Utils.showPopup(getActivity(),getActivity().getResources().getString(R.string.app_no_internet_error));
        }

        intentFilter=new IntentFilter();
        intentFilter.addAction("refresh_myVideos");
        getActivity().registerReceiver(receiver,intentFilter);
        return view;
    }

    private void getResponse() {
        String url= API.MY_VIDEO_GET+ SharedPreferencesMethod.getUserId(getActivity());
        onRefresh(URL,API.MY_VIDEO_GET);
        Log.e("MYVID","json obj is : " + url);
        new AQuery(getActivity()).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject outPut, AjaxStatus status) {
                super.callback(url, outPut, status);
                Log.e("MYVID","json obj is : " + outPut);
                if (outPut == null){
                    llLoading.setVisibility(View.GONE);
                    llDataNotFound.setVisibility(View.VISIBLE);
                    tv_video_notfound.setText(getResources().getString(R.string.video_server_error_text));
                }else {
                    //  JSONObject outPut=object;
                    //  System.out.println("Video List Response   "+outPut.toString());
                    if (outPut.has(ResponseParameters.Success)){
                        llLoading.setVisibility(View.GONE);
                        mainLayout.setVisibility(View.VISIBLE);
                        swipeRefreshLayout.setRefreshing(false);
                        if (videoDataList.size()!=0){
                            videoDataList.clear();
                        }
                        try{
                            if (outPut.getJSONArray("videos").length()>0){
                                for (int j=0; j<outPut.getJSONArray("videos").length(); j++){
                                    JSONObject videoObj = outPut.getJSONArray("videos").getJSONObject(j);
                                    VideoData videoData=new VideoData();
                                    System.out.println("Video Array Response    "+videoObj.getString(ResponseParameters.TITLE)+"=="+videoObj.getString(ResponseParameters.Approved));
                                    videoData.setUserFirstName(videoObj.getString(ResponseParameters.FirstName));
                                    videoData.setUserLastName(videoObj.getString(ResponseParameters.LastName));
                                    videoData.setVideoTitle(videoObj.getString(ResponseParameters.TITLE));
                                    videoData.setVideoDescription(videoObj.getString(ResponseParameters.DESCRIPTION));
                                    videoData.setVideoName(videoObj.getString(ResponseParameters.MEDIA));
                                    videoData.setVideoType(videoObj.getString(ResponseParameters.MEDIA_TYPE));
                                    videoData.setUserAvatar(videoObj.getString(ResponseParameters.Avatar));
                                    videoData.setVideoUploadedDate(videoObj.getString(ResponseParameters.CREATED_AT));
                                    videoData.setVideoEndDate(videoObj.getString(ResponseParameters.END_DATE));
                                    videoData.setVideoId(videoObj.getString(ResponseParameters.Id));
                                    videoData.setUserId(videoObj.getString(ResponseParameters.UserId));
                                    videoData.setVideoLikeCount(videoObj.getString(ResponseParameters.LIKE_COUNT));
                                    videoData.setVideoStatus(videoObj.getString(ResponseParameters.STATUS));
                                    videoData.setVideoApproved(videoObj.getString(ResponseParameters.Approved));
                                    videoData.setVideoPublish(videoObj.getString(ResponseParameters.Publish));
                                    videoData.setVideoThumbnail(videoObj.getString(ResponseParameters.THUMBNAIL));
                                    videoDataList.add(videoData);
                                }

                            }
                            if(outPut.getJSONArray("archived_videos").length()>0){
                                for (int j=0; j<outPut.getJSONArray("archived_videos").length(); j++){
                                    JSONObject videoObj = outPut.getJSONArray("archived_videos").getJSONObject(j);
                                    VideoData videoData=new VideoData();
                                    System.out.println("Video Archived Response    "+videoObj.getString(ResponseParameters.TITLE));
                                    videoData.setUserFirstName(videoObj.getString(ResponseParameters.FirstName));
                                    videoData.setUserLastName(videoObj.getString(ResponseParameters.LastName));
                                    videoData.setVideoTitle(videoObj.getString(ResponseParameters.TITLE));
                                    videoData.setVideoDescription(videoObj.getString(ResponseParameters.DESCRIPTION));
                                    videoData.setVideoName(videoObj.getString(ResponseParameters.MEDIA));
                                    videoData.setVideoType(videoObj.getString(ResponseParameters.MEDIA_TYPE));
                                    videoData.setUserAvatar(videoObj.getString(ResponseParameters.Avatar));
                                    videoData.setVideoUploadedDate(videoObj.getString(ResponseParameters.CREATED_AT));
                                    videoData.setVideoEndDate(videoObj.getString(ResponseParameters.END_DATE));
                                    videoData.setVideoId(videoObj.getString(ResponseParameters.Id));
                                    videoData.setUserId(videoObj.getString(ResponseParameters.UserId));
                                    videoData.setVideoLikeCount(videoObj.getString(ResponseParameters.LIKE_COUNT));
                                    videoData.setVideoStatus(videoObj.getString(ResponseParameters.STATUS));
                                    videoData.setVideoApproved(videoObj.getString(ResponseParameters.Approved));
                                    videoData.setVideoPublish(videoObj.getString(ResponseParameters.Publish));
                                    videoData.setVideoThumbnail(videoObj.getString(ResponseParameters.THUMBNAIL));
                                    videoDataList.add(videoData);
                                }
                            }
                            if(outPut.getJSONArray("videos").length()==0 && outPut.getJSONArray("archived_videos").length()==0){
                                llLoading.setVisibility(View.GONE);
                                llDataNotFound.setVisibility(View.VISIBLE);
                                String s=outPut.getString("message");
                                tv_video_notfound.setText(s);
                            }
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                        userVideoListAdapter.notifyDataSetChanged();

//                    linearLayoutManager = new LinearLayoutManager(getActivity());
//                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//                    rv_video_list.setLayoutManager(linearLayoutManager);
//                    userVideoListAdapter = new UserVideoListAdapter(getActivity(), videoDataList);
//                    userVideoListAdapter.notifyDataSetChanged();
//                    rv_video_list.setAdapter(userVideoListAdapter);
                        rv_video_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                                super.onScrollStateChanged(recyclerView, newState);

                            }

                            @Override
                            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                if (loading) {
                                    visibleItemCount = linearLayoutManager.getChildCount();
                                    totalItemCount = linearLayoutManager.getItemCount();
                                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                        loading = false;
                                        pagination = pagination + 10;
                                        pagination_loader.setVisibility(View.VISIBLE);
                                        fetchData(URL+"/"+pagination);
                                    }
                                }
                            }
                        });
                    }else{
                        llLoading.setVisibility(View.GONE);
                        llDataNotFound.setVisibility(View.VISIBLE);
                        tv_video_notfound.setText(getResources().getString(R.string.video_server_error_text));
                        System.out.println("Video List Response Error  "+outPut.toString());
                    }
                }


            }
        });
    }

    private void fetchData(final String URL){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                LoadMoreData(URL);
            }
        },500);

    }





    private void LoadMoreData(String URL){
        loading=true;
        new AQuery(getActivity()).ajax(URL,JSONObject.class,new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject outPut, AjaxStatus status) {
                super.callback(url, outPut, status);
                try{
                    pagination_loader.setVisibility(View.GONE);
                    for (int j=0; j<outPut.getJSONArray("videos").length(); j++){
                        JSONObject videoObj = outPut.getJSONArray("videos").getJSONObject(j);
                        VideoData videoData=new VideoData();
                        System.out.println("Video Array Response    "+videoObj.getString(ResponseParameters.TITLE));
                        videoData.setUserFirstName(videoObj.getString(ResponseParameters.FirstName));
                        videoData.setUserLastName(videoObj.getString(ResponseParameters.LastName));
                        videoData.setVideoTitle(videoObj.getString(ResponseParameters.TITLE));
                        videoData.setVideoDescription(videoObj.getString(ResponseParameters.DESCRIPTION));
                        videoData.setVideoName(videoObj.getString(ResponseParameters.MEDIA));
                        videoData.setVideoType(videoObj.getString(ResponseParameters.MEDIA_TYPE));
                        videoData.setUserAvatar(videoObj.getString(ResponseParameters.Avatar));
                        videoData.setVideoUploadedDate(videoObj.getString(ResponseParameters.CREATED_AT));
                        videoData.setVideoEndDate(videoObj.getString(ResponseParameters.END_DATE));
                        videoData.setVideoId(videoObj.getString(ResponseParameters.Id));
                        videoData.setUserId(videoObj.getString(ResponseParameters.UserId));
                        videoData.setVideoLikeCount(videoObj.getString(ResponseParameters.LIKE_COUNT));
                        videoData.setVideoStatus(videoObj.getString(ResponseParameters.STATUS));
                        videoData.setVideoApproved(videoObj.getString(ResponseParameters.Approved));
                        videoData.setVideoPublish(videoObj.getString(ResponseParameters.Publish));
                        videoData.setVideoThumbnail(videoObj.getString(ResponseParameters.THUMBNAIL));
                        videoDataList.add(videoData);
                    }
                    userVideoListAdapter.notifyDataSetChanged();
                }catch (Exception ex){
                    ex.getMessage();
                    ex.printStackTrace();
                }
            }
        });
    }


    private void onRefresh(final String URL, final String input){
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                videoDataList.clear();
                pagination=0;
                userVideoListAdapter.notifyDataSetChanged();
                rv_video_list.refreshDrawableState();
                rv_video_list.invalidate();
                getResponse();
            }
        });
    }

    private void findViews(){
        mToolbar=view.findViewById(R.id.toolbar_actionbar);
        mToolbar.setVisibility(View.GONE);
        header=view.findViewById(R.id.header);
        header.setVisibility(View.GONE);
        rv_video_list=view.findViewById(R.id.rv_video_list);
        swipeRefreshLayout=view.findViewById(R.id.swipelayout);
        mainLayout=view.findViewById(R.id.mainLayout);
        llLoading=view.findViewById(R.id.llLoading);
        llDataNotFound=view.findViewById(R.id.llDataNotFound);
        tv_video_notfound=view.findViewById(R.id.tv_video_notfound);
        pagination_loader=view.findViewById(R.id.pagination_loader);
    }

    @Override
    public void onStop() {
        if(videoDataList.size()>0)
        userVideoListAdapter.pauseAllVideo();
        super.onStop();
    }

    @Override
    public void StopAllVideos() {
        getActivity().unregisterReceiver(receiver);
        if(videoDataList.size()>0)
            userVideoListAdapter.stopVideo();
    }

    @Override
    public void PauseAllVideos() {
        if(videoDataList.size()>0)
            userVideoListAdapter.pauseAllVideo();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
