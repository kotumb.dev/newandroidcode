package com.codeholic.kotumb.app.Firebase;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.preference.PreferenceManager;
import androidx.core.app.NotificationCompat;
import android.text.Html;
import android.util.Log;

import com.codeholic.kotumb.app.Activity.ChatActivity;
import com.codeholic.kotumb.app.Activity.GroupPostActivity;
import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Activity.LoginActivity;
import com.codeholic.kotumb.app.Activity.MessageActivity;
import com.codeholic.kotumb.app.Activity.OtherUserProfileActivity;
import com.codeholic.kotumb.app.Activity.PostDetailsActivity;
import com.codeholic.kotumb.app.Activity.ReportedCommentActivity;
import com.codeholic.kotumb.app.Activity.ReportedPostActivity;
import com.codeholic.kotumb.app.Activity.ReportedUserActivity;
import com.codeholic.kotumb.app.Activity.ShowVideos;
import com.codeholic.kotumb.app.Activity.VideoDetailsActivity;
import com.codeholic.kotumb.app.BroadcastReciever.NotifyReminder;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.GroupMembers;
import com.codeholic.kotumb.app.Model.Note;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.Model.PostComments;
import com.codeholic.kotumb.app.Model.PostLikes;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.Model.VideoData;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.EmojiMapUtil;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;


/**
 * Created by DELL on 4/12/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    public static final int ID_BIG_NOTIFICATION = 234;
    public static int ID_SMALL_NOTIFICATION = 235;
    private Context mCtx;
    private boolean shown;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("MESSAGE","Message received : " + remoteMessage.toString());
        try {
            mCtx = MyFirebaseMessagingService.this;
            String userId = SharedPreferencesMethod.getUserId(getApplicationContext());
//            if (userId == null || userId.isEmpty()) {
//                return;
//            }
            if (remoteMessage.getData().size() > 0) {
                Log.e(TAG, "Message data payload: " + remoteMessage.getData().get("data"));
                JSONObject data = new JSONObject(remoteMessage.getData().get("data"));
                JSONObject notificationData = data.getJSONObject("notification");
                Log.e(TAG, "Message data payload 2: " + notificationData);
                String activityTypeString = notificationData.getString(ResponseParameters.ACTIVITY_TYPE);
                sendNotificationBroadcast(activityTypeString);
                System.out.println("Notify Type  "+notificationData);

                if (activityTypeString.equalsIgnoreCase("debit_reward_points")){

                }

                if (activityTypeString.equalsIgnoreCase("videoapproved")){
                    User user = new User();
                    VideoData videoData=new VideoData();
                    String message=notificationData.getString("message");
                    videoData.setVideoName(notificationData.getJSONObject("videoInfo").getString("media"));
                    videoData.setVideoTitle(notificationData.getJSONObject("videoInfo").getString("title"));
                    videoData.setVideoUploadedDate(notificationData.getJSONObject("videoInfo").getString("createdAt"));
                    videoData.setVideoDescription(notificationData.getJSONObject("videoInfo").getString("description"));
                    user.setUserId(notificationData.getJSONObject("videoInfo").getString("userId"));

                    Intent intent = new Intent(mCtx, VideoDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setAction(Long.toString(System.currentTimeMillis()));
                    intent.putExtra("video_id",notificationData.getJSONObject("videoInfo").getString("id"));
                    intent.putExtra("video_title",notificationData.getJSONObject("videoInfo").getString("title"));
                    intent.putExtra("video_desp",notificationData.getJSONObject("videoInfo").getString("description"));
                    intent.putExtra("video_uri",notificationData.getJSONObject("videoInfo").getString("media"));
                    intent.putExtra("video_avatar",notificationData.getJSONObject("videoInfo").getString("thumbnail"));
                    intent.putExtra("isPublished",notificationData.getJSONObject("videoInfo").getString("isPublished"));
                    intent.putExtra("video_user","");
                    intent.putExtra("fullname",notificationData.getJSONObject("video_userInfo").getString("firstName")+" "+notificationData.getJSONObject("video_userInfo").getString("lastName"));
                    intent.putExtra("avatar",notificationData.getJSONObject("video_userInfo").getString("avatar"));
                    intent.putExtra("activitytype",activityTypeString);
                    intent.putExtra("video_date_time",notificationData.getJSONObject("videoInfo").getString("createdAt"));
                    showSmallNotification("" +SharedPreferencesMethod.getUserInfo(getApplicationContext()).getFirstName()+ " " + SharedPreferencesMethod.getUserInfo(this).getLastName(), message, intent, user, new Random().nextInt(1000000));
                    //showSmallNotification("" +SharedPreferencesMethod.getUserInfo(this).getFirstName()+ " " + SharedPreferencesMethod.getUserInfo(this).getLastName(), "" + SharedPreferencesMethod.getUserInfo(this).getFirstName() + " " +SharedPreferencesMethod.getUserInfo(this).getLastName() + " " + getResources().getString(R.string.video_approved_text), intent, user, new Random().nextInt(1000000));
                }
                if (activityTypeString.equalsIgnoreCase("newvideonotification")){
                    User user = new User();
                    VideoData videoData=new VideoData();
                    String message=notificationData.getString("message");
                    Intent intent = new Intent(mCtx, ShowVideos.class);
                    intent.putExtra("tab","1");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setAction(Long.toString(System.currentTimeMillis()));
                    intent.putExtra("activitytype",activityTypeString);
                    showSmallNotification(message , intent, user, new Random().nextInt(1000000));
                }
                if (activityTypeString.equalsIgnoreCase("publishedvideonotification")){
                    User user = new User();
                    VideoData videoData=new VideoData();
                    String message=notificationData.getString("message");
                    videoData.setVideoName(notificationData.getJSONObject("videoInfo").getString("media"));
                    videoData.setVideoTitle(notificationData.getJSONObject("videoInfo").getString("title"));
                    videoData.setVideoUploadedDate(notificationData.getJSONObject("videoInfo").getString("createdAt"));
                    videoData.setVideoDescription(notificationData.getJSONObject("videoInfo").getString("description"));
                    user.setUserId(notificationData.getJSONObject("videoInfo").getString("userId"));
                    Intent intent = new Intent(mCtx, VideoDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setAction(Long.toString(System.currentTimeMillis()));
                    intent.putExtra("video_id",notificationData.getJSONObject("videoInfo").getString("id"));
                    intent.putExtra("video_title",notificationData.getJSONObject("videoInfo").getString("title"));
                    intent.putExtra("video_desp",notificationData.getJSONObject("videoInfo").getString("description"));
                    intent.putExtra("video_uri",notificationData.getJSONObject("videoInfo").getString("media"));
                    intent.putExtra("video_avatar",notificationData.getJSONObject("videoInfo").getString("thumbnail"));
                    intent.putExtra("video_user","");
                    intent.putExtra("like_count",notificationData.getString("like_count"));
                    intent.putExtra("like_status",notificationData.getString("like_status"));
                    intent.putExtra("fullname",notificationData.getJSONObject("video_userInfo").getString("firstName")+" "+notificationData.getJSONObject("video_userInfo").getString("lastName"));
                    intent.putExtra("avatar",notificationData.getJSONObject("video_userInfo").getString("avatar"));
                    intent.putExtra("activitytype",activityTypeString);
                    intent.putExtra("video_date_time",notificationData.getJSONObject("videoInfo").getString("createdAt"));
                    showSmallNotification("" +SharedPreferencesMethod.getUserInfo(this).getFirstName()+ " " + SharedPreferencesMethod.getUserInfo(this).getLastName(), message, intent, user, new Random().nextInt(1000000));
                    //showSmallNotification("" +SharedPreferencesMethod.getUserInfo(this).getFirstName()+ " " + SharedPreferencesMethod.getUserInfo(this).getLastName(), "" + SharedPreferencesMethod.getUserInfo(this).getFirstName() + " " +SharedPreferencesMethod.getUserInfo(this).getLastName() + " " + getResources().getString(R.string.video_approved_text), intent, user, new Random().nextInt(1000000));
                }
                if (activityTypeString.equalsIgnoreCase("groupvideoshare")){
                    User user = new User();
                    VideoData videoData=new VideoData();
                    String group_message=notificationData.getString("message");
                    user.setFirstName(notificationData.getJSONObject("userInfo").getString(ResponseParameters.FirstName));
                    user.setMiddleName(notificationData.getJSONObject("userInfo").getString(ResponseParameters.MiddleName));
                    user.setLastName(notificationData.getJSONObject("userInfo").getString(ResponseParameters.LastName));
                    user.setAvatar(notificationData.getJSONObject("userInfo").getString(ResponseParameters.Avatar));
                    user.setUserId(notificationData.getJSONObject("userInfo").getString(ResponseParameters.UserId));
                    user.setProfileHeadline(notificationData.getJSONObject("userInfo").getString(ResponseParameters.PROFILE_HEADLINE));
                    user.setProfileSummary(notificationData.getJSONObject("userInfo").getString(ResponseParameters.PROFILE_SUMMERY));
                    user.setCity(notificationData.getJSONObject("userInfo").getString(ResponseParameters.City));
                    user.setState(notificationData.getJSONObject("userInfo").getString(ResponseParameters.State));
                    videoData.setVideoName(notificationData.getJSONObject("videoInfo").getString("media"));
                    videoData.setVideoTitle(notificationData.getJSONObject("videoInfo").getString("title"));
                    videoData.setVideoUploadedDate(notificationData.getJSONObject("videoInfo").getString("createdAt"));
                    videoData.setVideoDescription(notificationData.getJSONObject("videoInfo").getString("description"));
                    Intent intent = new Intent(mCtx, VideoDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("USRE_GRP",user);
                    intent.putExtra("VIDEO_DATA",videoData);
                    intent.setAction(Long.toString(System.currentTimeMillis()));
                    showSmallNotification("" + user.getFirstName() + " " + user.getLastName(), "" + user.getFirstName() + " " + user.getLastName() + " " + group_message, intent, user, new Random().nextInt(1000000));
                }
                if(activityTypeString.equalsIgnoreCase("videorejected")){
                    System.out.println("Notify Type inside rejected");
                    User user=new User();
                    user.setAvatar(notificationData.getJSONObject("video_userInfo").getString("avatar"));
                    String message=notificationData.getString("message");
                    Intent intent=new Intent(mCtx,HomeScreenActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    showSmallNotification(notificationData.getJSONObject("video_userInfo").getString(ResponseParameters.FirstName)+" "+notificationData.getJSONObject("video_userInfo").getString(ResponseParameters.LastName),message,intent,user,new Random().nextInt(1000000));
                }
                 if (activityTypeString.equalsIgnoreCase("videolikes")){
                    User user = new User();
                    VideoData videoData=new VideoData();
                    System.out.println("Notify Type inside videolikes="+notificationData.getJSONObject("videos").getString("id")+"=="+notificationData.getJSONObject("videos").getString("description"));
                     String message=notificationData.getString("message");
                     user.setFirstName(notificationData.getJSONObject("user").getString("name"));
                        user.setAvatar(notificationData.getJSONObject("user").getString(ResponseParameters.Avatar));
                        user.setUserId(notificationData.getJSONObject("user").getString(ResponseParameters.UserId));
                        user.setCity(notificationData.getJSONObject("user").getString(ResponseParameters.City));
                        user.setState(notificationData.getJSONObject("user").getString(ResponseParameters.State));
                        videoData.setVideoTitle(notificationData.getJSONObject("videos").getString(ResponseParameters.TITLE));
                        videoData.setVideoTitle(notificationData.getJSONObject("videos").getString(ResponseParameters.UserId));
                        videoData.setVideoName(notificationData.getJSONObject("videos").getString(ResponseParameters.MEDIA));
                        videoData.setVideoId(notificationData.getJSONObject("videos").getString(ResponseParameters.Id));
                        videoData.setVideoId(notificationData.getJSONObject("video_user").getString(ResponseParameters.Name));
                       // videoData.setVideoUploadedDate(notificationData.getJSONObject("videos").getString(ResponseParameters.Date));
                        Intent intent = new Intent(mCtx, HomeScreenActivity.class);
                        intent.putExtra("user",user);
                     intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                     intent.putExtra("video_id",notificationData.getJSONObject("videos").getString("id"));
                     intent.putExtra("video_title",notificationData.getJSONObject("videos").getString("title"));
                     intent.putExtra("video_desp",notificationData.getJSONObject("videos").getString("description"));
                     intent.putExtra("video_uri",notificationData.getJSONObject("videos").getString("media"));
                     intent.putExtra("video_avatar",notificationData.getJSONObject("videos").getString("thumbnail"));
                     intent.putExtra("video_user","");
                     System.out.println("Notify Type inside videolikes="+notificationData.getJSONObject("videos").getString("id")+"=="+intent.getStringExtra("video_desp"));
                     intent.putExtra("like_status",notificationData.getString("like_status"));
                     intent.putExtra("like_count",notificationData.getString("like_count"));
                     intent.putExtra("fullname",notificationData.getJSONObject("video_user").getString("name"));
                     intent.putExtra("avatar",notificationData.getJSONObject("video_user").getString("avatar"));
                     intent.putExtra("activitytype",activityTypeString);
                     intent.putExtra("video_id",notificationData.getJSONObject("videos").getString("id"));
                     intent.putExtra("video_date_time",notificationData.getJSONObject("videos").getString("createdAt"));
                        showSmallNotification("" + user.getFirstName() + " " + user.getLastName(), message, intent, user, new Random().nextInt(1000000));
                }
                if (activityTypeString.equalsIgnoreCase(ResponseParameters.REQUESTED) ||
                        activityTypeString.equalsIgnoreCase(ResponseParameters.REQUESTACCEPTED) ||
                        activityTypeString.equalsIgnoreCase(ResponseParameters.REQUESTDELETED) ||
                        activityTypeString.equalsIgnoreCase(ResponseParameters.ASKED_RECOM)) {
                    User user = new User();
                    ID_SMALL_NOTIFICATION = Integer.parseInt(notificationData.getString(ResponseParameters.UserId));
                    user.setFirstName(notificationData.getString(ResponseParameters.FirstName));  user.setMiddleName(notificationData.getString(ResponseParameters.MiddleName));
                    user.setLastName(notificationData.getString(ResponseParameters.LastName));
                    user.setAvatar(notificationData.getString(ResponseParameters.Avatar));
                    user.setUserId(notificationData.getString(ResponseParameters.UserId));
                    user.setProfileHeadline(notificationData.getString(ResponseParameters.PROFILE_HEADLINE));
                    user.setProfileSummary(notificationData.getString(ResponseParameters.PROFILE_SUMMERY));
                    user.setCity(notificationData.getString(ResponseParameters.City));
                    user.setState(notificationData.getString(ResponseParameters.State));
                    user.setUserInfo(notificationData.toString());
                    Intent intent = new Intent(mCtx, OtherUserProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("USER", user);


                    if (activityTypeString.equalsIgnoreCase(ResponseParameters.ASKED_RECOM)) {
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setAction(Long.toString(System.currentTimeMillis()));
                        showSmallNotification("" + user.getFirstName() + " " + user.getLastName(), "" + user.getFirstName() + " " + user.getLastName() + " " + getResources().getString(R.string.notification_ask_recommendation), intent, user, new Random().nextInt(1000000));
                    } else {
                        Intent broadCast = new Intent();
                        broadCast.setAction(ResponseParameters.UPDATE_USER);
                        broadCast.putExtra("USER", user);
                        sendBroadcast(broadCast);
                        if (activityTypeString.equalsIgnoreCase(ResponseParameters.REQUESTED)) {
                            Intent requestReceived = new Intent();
                            requestReceived.setAction(ResponseParameters.REQUEST_RECEIVED);
                            requestReceived.putExtra("USER", user);
                            sendBroadcast(requestReceived);
                            showSmallNotification("" + user.getFirstName() + " " + user.getLastName(), "" + user.getFirstName() + " " + user.getLastName() + " " + getResources().getString(R.string.notification_sent_request), intent, user, new Random().nextInt(1000000));
                        }
                        if (activityTypeString.equalsIgnoreCase(ResponseParameters.REQUESTACCEPTED)) {
                            Intent requestAccepted = new Intent();
                            requestAccepted.setAction(ResponseParameters.REQUEST_ACCEPTED);
                            requestAccepted.putExtra("USER", user);
                            sendBroadcast(requestAccepted);
                            showSmallNotification("" + user.getFirstName() + " " + user.getLastName(), "" + user.getFirstName() + " " + user.getLastName() + " " + getResources().getString(R.string.notification_accept_request), intent, user, new Random().nextInt(1000000));
                        }
                    }

                }
                else if (activityTypeString.equalsIgnoreCase(ResponseParameters.NOTE_UPDATED)
                        || activityTypeString.equalsIgnoreCase(ResponseParameters.NOTE_CREATED)
                        || activityTypeString.equalsIgnoreCase(ResponseParameters.NOTE_DELETED)) {
                    Note note = new Note();
                    note.setNoteID(notificationData.getString(ResponseParameters.Id));
                    note.setNoteTitle(notificationData.getString(ResponseParameters.TITLE));
                    note.setNoteDes(notificationData.getString(ResponseParameters.DESCRIPTION));
                    note.setNoteDate(notificationData.getString(ResponseParameters.Date));
                    note.setNoteTime(notificationData.getString(ResponseParameters.TIME));
                    Calendar cal = Calendar.getInstance();
                    String date = note.getNoteDate() + " " + note.getNoteTime();
                    SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    cal.setTime(_24HourSDF.parse(date));
                    Intent intent = new Intent(getBaseContext(), NotifyReminder.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    intent.setAction(note.getNoteID());
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    ObjectOutputStream out = null;
                    try {
                        out = new ObjectOutputStream(bos);
                        out.writeObject(note);
                        out.flush();
                        byte[] noteData = bos.toByteArray();
                        intent.putExtra("NOTE", noteData);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            bos.close();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }


                    PendingIntent pendingIntent =
                            PendingIntent.getBroadcast(getBaseContext(),
                                    Integer.parseInt(note.getNoteID()), intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarmManager =
                            (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    if (activityTypeString.equalsIgnoreCase(ResponseParameters.NOTE_UPDATED) ||
                            activityTypeString.equalsIgnoreCase(ResponseParameters.NOTE_DELETED))
                        alarmManager.cancel(pendingIntent);

                    if (activityTypeString.equalsIgnoreCase(ResponseParameters.NOTE_UPDATED) ||
                            activityTypeString.equalsIgnoreCase(ResponseParameters.NOTE_CREATED)) {
                        JSONArray NOTELIST = new JSONArray();
                        if (!SharedPreferencesMethod.getHasString(mCtx, ResponseParameters.NOTELIST).isEmpty()) {
                            NOTELIST = new JSONArray(SharedPreferencesMethod.getHasString(mCtx, ResponseParameters.NOTELIST));
                        }
                        //adding on note list
                        JSONObject noteObject = new JSONObject();
                        noteObject.put(ResponseParameters.Id, note.getNoteID());
                        NOTELIST.put(noteObject);
                        SharedPreferencesMethod.setString(mCtx, ResponseParameters.NOTELIST, NOTELIST.toString());
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                            alarmManager.set(AlarmManager.RTC_WAKEUP,
                                    cal.getTimeInMillis(), pendingIntent);

                        } else {
                            alarmManager.setExact(AlarmManager.RTC_WAKEUP,
                                    cal.getTimeInMillis(), pendingIntent);
                        }
                    }
                }
                else if (activityTypeString.equalsIgnoreCase(ResponseParameters.MESSAGE_RECEIVED)
                        || activityTypeString.equalsIgnoreCase(ResponseParameters.MESSAGE_SENT)) {
                    User user = new User();
                    user.setFirstName(notificationData.getString(ResponseParameters.FirstName));
                    if (notificationData.has(ResponseParameters.MiddleName)) {
                        user.setMiddleName(notificationData.getString(ResponseParameters.MiddleName));
                    }
                    user.setLastName(notificationData.getString(ResponseParameters.LastName));
                    user.setAvatar(notificationData.getString(ResponseParameters.Avatar));
                    user.setUserId(notificationData.getString(ResponseParameters.UserId));
                    user.setAadhaarInfo(notificationData.getString(ResponseParameters.AADHARINFO));

                    if (Constants.openChatUser.getUserId().equalsIgnoreCase(user.getUserId())) {
                        notificationData.put(ResponseParameters.UNREAD_MESSAGE_COUNT, 0);
                    }

                    user.setUserInfo(notificationData.toString());

                    Intent broadCast = new Intent();
                    broadCast.setAction(ResponseParameters.UPDATE_CHAT);
                    broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    if (activityTypeString.equalsIgnoreCase(ResponseParameters.MESSAGE_SENT)) {
                        broadCast.putExtra(ResponseParameters.MESSAGE_SENT, ""); // for indicating that message send broadcast
                    }
                    broadCast.putExtra("USER", user);
                    sendBroadcast(broadCast);
                    if (activityTypeString.equalsIgnoreCase(ResponseParameters.MESSAGE_RECEIVED)) {
                        if (!Constants.openChatUser.getUserId().equalsIgnoreCase(user.getUserId())) {
                            Intent intent = new Intent(mCtx, ChatActivity.class);
                            intent.putExtra("USER", user);
                            intent.putExtra(ResponseParameters.UPDATE_CHAT, "");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            JSONObject messageObject = notificationData.getJSONObject(ResponseParameters.LAST_MSG_INFO);
                            System.out.println("FCM Chat Response  "+messageObject.toString());
                            if (SharedPreferencesMethod.getHasString(mCtx, ResponseParameters.NOTIFICATIONS).isEmpty()) {
                                JSONArray NOTIFICATION = new JSONArray();
                                NOTIFICATION.put(notificationData);
                                SharedPreferencesMethod.setString(mCtx, ResponseParameters.NOTIFICATIONS, NOTIFICATION.toString());
                                String reply = EmojiMapUtil.replaceCheatSheetEmojis("" + messageObject.getString("reply"));
                                System.out.println("Message Chat   "+reply);
                                showSmallNotification("" + user.getFirstName() + " " + user.getLastName(), "" + reply, intent, user, ID_SMALL_NOTIFICATION);
                            } else {
                                JSONArray NOTIFICATION = new JSONArray(SharedPreferencesMethod.getHasString(mCtx, ResponseParameters.NOTIFICATIONS));
                                NOTIFICATION.put(notificationData);
                                intent.setAction(Long.toString(System.currentTimeMillis()));
                                SharedPreferencesMethod.setString(mCtx, ResponseParameters.NOTIFICATIONS, NOTIFICATION.toString());
                                String reply = EmojiMapUtil.replaceCheatSheetEmojis("" + messageObject.getString("reply"));
                                System.out.println("Message Chat One  "+reply);
                                showSmallNotificationWithInbox("" + user.getFirstName() + " " + user.getLastName(), "" + reply, intent, user, NOTIFICATION);
                            }
                        }
                    }
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.MESSAGE_READ)) {
                    Intent broadCast = new Intent();
                    broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    broadCast.setAction(ResponseParameters.UPDATE_CHAT);
                    broadCast.putExtra(ResponseParameters.MESSAGE_READ, true);
                    sendBroadcast(broadCast);
                }
                else if (activityTypeString.equalsIgnoreCase(ResponseParameters.SUPPORT_CHAT)) {
                    Intent notifyReminder = new Intent(mCtx, NotifyReminder.class);
                    notifyReminder.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    notifyReminder.setAction(NotifyReminder.CONTACT_US);
                    notifyReminder.putExtra(ResponseParameters.DESCRIPTION, notificationData.getString(ResponseParameters.DESCRIPTION));
                    //showSmallNotificationSupport(getResources().getString(R.string.app_name), getResources().getString(R.string.new_message_kotumb_team), notifyReminder);
                }
                else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_CREATED)
                        || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_UPDATED)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Group group = gson.fromJson(notificationData.toString(), Group.class); //parses the JSON string into an Cloth object
                    Intent broadCast = new Intent();
                    broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.GROUP, group);
                    //sendBroadcast(broadCast);
                    /*LocalBroadcastManager.getInstance(mCtx).*/
                    sendBroadcast(broadCast);
                }
                else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_INVITED) ||
                        activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REQUEST)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Type listType = new TypeToken<List<GroupMembers>>() {
                    }.getType();
                    Group group = gson.fromJson(notificationData.getJSONObject(ResponseParameters.GROUP_INFO).toString(), Group.class); //parses the JSON string into an Cloth object
                    List<GroupMembers> invited_members = gson.fromJson(notificationData.getJSONArray(ResponseParameters.GROUP_MEMBERS).toString(), listType);
                    Intent broadCast = new Intent();
                    broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.GROUP_MEMBERS, (Serializable) invited_members);
                    broadCast.putExtra(ResponseParameters.GROUP, group);
                    sendBroadcast(broadCast);
                }
                else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_DELETE)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Group group = gson.fromJson(notificationData.getJSONObject(ResponseParameters.GROUP_INFO).toString(), Group.class); //parses the JSON string into an Cloth object
                    Intent broadCast = new Intent();
                    broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.STATUS, notificationData.getString(ResponseParameters.STATUS));
                    broadCast.putExtra(ResponseParameters.GROUP, group);
                    broadCast.putExtra(ResponseParameters.UserId, notificationData.getString(ResponseParameters.UserId));
                    sendBroadcast(broadCast);
                }
                else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_ROLE_UPDATE)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Type listType = new TypeToken<List<GroupMembers>>() {
                    }.getType();
                    Group group = gson.fromJson(notificationData.getJSONObject(ResponseParameters.GROUP_INFO).toString(), Group.class); //parses the JSON string into an Cloth object
                    Intent broadCast = new Intent();
                    broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    List<GroupMembers> group_members = gson.fromJson(notificationData.getJSONArray(ResponseParameters.GROUP_MEMBERS).toString(), listType);
                    broadCast.setAction(notificationData.getString(ResponseParameters.ACTIVITY_TYPE).trim());
//                    broadCast.setAction(notificationData.getString(ResponseParameters.MEDIA_TYPE).trim());
//                    broadCast.setAction(notificationData.getString(ResponseParameters.MEDIA).trim());
                    broadCast.putExtra(ResponseParameters.GROUP, group);
                    broadCast.putExtra(ResponseParameters.GROUP_MEMBERS, (Serializable) group_members);
                    sendBroadcast(broadCast);
                    String title = group.groupName;
                    String msg;
                    if (group_members.get(0).role.equalsIgnoreCase("0")) {
                        String s = getString(R.string.tile_noti_added_as_admin);
                        String group1 = getString(R.string.tile_noti_group);
                        msg = s + " " + group.groupName + " " + group1;
                    } else {
                        String s = getString(R.string.tile_noti_removed_as_admin);
                        String group1 = getString(R.string.tile_noti_group);
                        msg = s + " " + group.groupName + " " + group1;
                    }
                    Intent intent = new Intent(getApplicationContext(), GroupPostActivity.class);
                    //showSmallNotification(title, msg, intent, "", ID_SMALL_NOTIFICATION);
                }
                else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_ACCEPT)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Type listType = new TypeToken<List<GroupMembers>>() {
                    }.getType();
                    Group group = gson.fromJson(notificationData.getJSONObject(ResponseParameters.GROUP_INFO).toString(), Group.class); //parses the JSON string into an Cloth object
                    Intent broadCast = new Intent();
                    List<GroupMembers> group_members = gson.fromJson(notificationData.getJSONArray(ResponseParameters.GROUP_MEMBERS).toString(), listType);
                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.GROUP, group);
                    broadCast.putExtra(ResponseParameters.GROUP_MEMBERS, (Serializable) group_members);
                    broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    sendBroadcast(broadCast);
                }
                else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_BLOCK)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Type listType = new TypeToken<List<GroupMembers>>() {
                    }.getType();
                    Group group = gson.fromJson(notificationData.getJSONObject(ResponseParameters.GROUP_INFO).toString(), Group.class); //parses the JSON string into an Cloth object
                    Intent broadCast = new Intent();
                    List<GroupMembers> group_members = gson.fromJson(notificationData.getJSONArray(ResponseParameters.GROUP_MEMBERS).toString(), listType);
                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.GROUP, group);
                    broadCast.putExtra(ResponseParameters.GROUP_MEMBERS, (Serializable) group_members);
                    sendBroadcast(broadCast);
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_UNBLOCK)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Type listType = new TypeToken<List<GroupMembers>>() {
                    }.getType();
                    Group group = gson.fromJson(notificationData.getJSONObject(ResponseParameters.GROUP_INFO).toString(), Group.class); //parses the JSON string into an Cloth object
                    Intent broadCast = new Intent();
                    List<GroupMembers> group_members = gson.fromJson(notificationData.getJSONArray(ResponseParameters.GROUP_MEMBERS).toString(), listType);
                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.GROUP, group);
                    broadCast.putExtra(ResponseParameters.GROUP_MEMBERS, (Serializable) group_members);
                    sendBroadcast(broadCast);
                }
                else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_DELETED)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Group group = gson.fromJson(notificationData.toString(), Group.class); //parses the JSON string into an Cloth object
                    Intent broadCast = new Intent();
                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.GROUP, group);
                    sendBroadcast(broadCast);
                    Intent intent = new Intent(getApplicationContext(), HomeScreenActivity.class);
                    intent.putExtra(ResponseParameters.OPEN_FRAGMENT, ResponseParameters.NOTIFICATIONS);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(ResponseParameters.OPEN_GROUP, ResponseParameters.NOTIFICATIONS);
//                    showSmallNotification(title, msg, intent, "", Integer.parseInt(group.groupId));
//                    if (notificationData.getBoolean("byadmin")) {
                    String myUserId = SharedPreferencesMethod.getUserId(getApplicationContext());
                    if (!myUserId.equalsIgnoreCase(userId)) {
                        showSmallNotification(group.groupName, getString(R.string.tile_noti_group_deleted), intent, "", Integer.parseInt(group.groupId));
                    }
//                    }
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_EDIT_POST)
                        || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_DELETE_POST)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Post post = gson.fromJson(notificationData.toString(), Post.class); //parses the JSON string into an Cloth object
                    Intent broadCast = new Intent();
                    broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.POST, post);
                    sendBroadcast(broadCast);
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_LIKE)
                        || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_UNLIKE)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Post post = gson.fromJson(notificationData.getJSONObject(ResponseParameters.POST).toString(), Post.class); //parses the JSON string into an Cloth object
                    PostLikes likeInfo = gson.fromJson(notificationData.getJSONObject(ResponseParameters.LIKEINFO).toString(), PostLikes.class); //parses the JSON string into an Cloth object
                    System.out.println("Like Info    "+likeInfo.liked);
                    Intent broadCast = new Intent();
                    broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.POST, post);
                    broadCast.putExtra(ResponseParameters.LIKEINFO, likeInfo);
                    sendBroadcast(broadCast);

                    if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_LIKE)
                            && !likeInfo.userId.equalsIgnoreCase(userId)
                            && post.userId.equalsIgnoreCase(userId)) {
                        Group group = gson.fromJson(notificationData.getJSONObject(ResponseParameters.GROUP_INFO).toString(), Group.class); //parses the JSON string into an Cloth object
                        Intent intent = new Intent(mCtx, PostDetailsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setAction(Long.toString(System.currentTimeMillis()));
                        intent.putExtra(ResponseParameters.POST, post);
                        intent.putExtra("back", true);
                        intent.putExtra(ResponseParameters.GROUP, group);
                        intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, notificationData.getString(ResponseParameters.GROUP_IMG_BASE_URL));
                        showSmallNotification("" + likeInfo.firstName + " " + likeInfo.lastName, "" + likeInfo.firstName + " " + likeInfo.lastName + " " + getResources().getString(R.string.notification_liked_your_post), intent, API.imageUrl + likeInfo.avatar, new Random().nextInt(1000000));
                    }

                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_COMMENT_CREATED)
                        || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_COMMENT_DELETED)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    PostComments postComments = gson.fromJson(notificationData.toString(), PostComments.class); //parses the JSON string into an Cloth object
                    System.out.println("FCM Reply Id"+postComments.reply_id);
                    if (postComments.reply_id.equalsIgnoreCase("0")){
                        Post post = new Post();
                        if (notificationData.has(ResponseParameters.POST)) {
                            post = gson.fromJson(notificationData.getJSONObject(ResponseParameters.POST).toString(), Post.class); //parses the JSON string into an Cloth object
                        }
                        Intent broadCast = new Intent();
                        broadCast.setAction(activityTypeString.trim());
                        broadCast.putExtra(ResponseParameters.GROUP_POSTS_COMMENTS, postComments);
                        broadCast.putExtra(ResponseParameters.POST, post);
                        broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        sendBroadcast(broadCast);
                    }else{
                       // Toast.makeText(mCtx, "Reply Added", Toast.LENGTH_SHORT).show();
                    }
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_COMMENT)
                        || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_SHARE)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    PostComments postComments = gson.fromJson(notificationData.toString(), PostComments.class); //parses the JSON string into an Cloth object
                    System.out.println("FCM Reply Id2"+postComments.reply_id);
//                    if (postComments.reply_id.equalsIgnoreCase("0")){
                        Post post = new Post();
                        if (notificationData.has(ResponseParameters.POST)) {
                            post = gson.fromJson(notificationData.getJSONObject(ResponseParameters.POST).toString(), Post.class); //parses the JSON string into an Cloth object
                        }
                        if (!postComments.userId.equalsIgnoreCase(userId)) {
                            Group group = gson.fromJson(notificationData.getJSONObject(ResponseParameters.GROUP_INFO).toString(), Group.class); //parses the JSON string into an Cloth object
                            Intent intent = new Intent(mCtx, PostDetailsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.setAction(Long.toString(System.currentTimeMillis()));
                            intent.putExtra(ResponseParameters.POST, post);
                            intent.putExtra(ResponseParameters.GROUP, group);
                            intent.putExtra("back", true);
                            if (notificationData.has(ResponseParameters.GROUP_IMG_BASE_URL)) {
                                intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, notificationData.getString(ResponseParameters.GROUP_IMG_BASE_URL));
                            }
                            String message = "";
                            if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_COMMENT)) {
                                if (postComments.reply_id.equalsIgnoreCase("0")){
                                    message = getResources().getString(R.string.notification_commented_your_post);
                                    intent.putExtra(ResponseParameters.OPEN_COMMENT_VIEW, true);
                                }else{
                                    message = getResources().getString(R.string.notification_reply_your_post);
                                    intent.putExtra(ResponseParameters.OPEN_COMMENT_VIEW, true);
                                }
                            } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_SHARE)) {
                          /*  if(post.sharedPostUserId==){

                            }*/
                                String s = post.firstName + " " + post.middleName + " " + post.lastName;
                                s = s.replaceAll("  "," ");
                                message = s + " " + getResources().getString(R.string.notification_shared_post_msg);
                                intent.putExtra("back", true);
                                showSmallNotification("" + group.groupName,
                                        "" + postComments.firstName + " " + postComments.lastName + " " + message, intent,
                                        API.imageUrl + post.avatar,
                                        new Random().nextInt(1000000));
                                shown = true;
                            }
                            if (!shown) {
                                int id_small_notification = 0;
                                try {
                                    id_small_notification = Integer.parseInt(post.post_id) + Integer.parseInt(group.groupId) + Integer.parseInt(postComments.userId);
                                } catch (NumberFormatException e) {
                                    id_small_notification = new Random().nextInt(10000);
                                    e.printStackTrace();
                                }
                                showSmallNotification("" + postComments.firstName + " " + postComments.lastName, "" + postComments.firstName + " " + postComments.lastName + " " + message, intent, API.imageUrl + postComments.avatar, id_small_notification);
                            }
                        }
//                    }else{
//                        Toast.makeText(mCtx, "Reply Added2", Toast.LENGTH_SHORT).show();
//                    }

                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_INVITATION)
                        || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REQUESTED)
                        || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REQUEST_ACCEPTED)) {
                    com.codeholic.kotumb.app.Model.Notification notification = new com.codeholic.kotumb.app.Model.Notification();
                    User user = new User();
                    user.setFirstName(notificationData.getString(ResponseParameters.FirstName)); user.setMiddleName(notificationData.getString(ResponseParameters.MiddleName));
                    user.setLastName(notificationData.getString(ResponseParameters.LastName));
                    user.setAvatar(notificationData.getString(ResponseParameters.Avatar));
                    user.setUserId(notificationData.getString(ResponseParameters.UserId));
                    user.setProfileHeadline(notificationData.getString(ResponseParameters.PROFILE_HEADLINE));
                    user.setProfileSummary(notificationData.getString(ResponseParameters.PROFILE_SUMMERY));
                    //user.setAadhaarInfo(notificationData.getString(ResponseParameters.AADHARINFO));
                    user.setCity(notificationData.getString(ResponseParameters.City));
                    user.setState(notificationData.getString(ResponseParameters.State));
                    user.setUserInfo(notificationData.toString());
                    notification.setUser(user);
                    notification.setNotificationTime(notificationData.getString(ResponseParameters.ACTIVITY_TIME));
//
                    notification.setNotificationId(notificationData.getString(ResponseParameters.Id));
                    notification.setNotificationType(activityTypeString);
                    notification.setNotificationDes(notificationData.getString(ResponseParameters.DESCRIPTION));
                    notification.setNotificationTime(notificationData.getString(ResponseParameters.MEDIA));
                    notification.setNotificationTime(notificationData.getString(ResponseParameters.MEDIA_TYPE));
                   // Toast.makeText(mCtx,notificationData.getString(ResponseParameters.MEDIA) , Toast.LENGTH_SHORT).show();
                    System.out.println("Media Video  "+notificationData.getString(ResponseParameters.MEDIA));
                    Intent broadCast = new Intent();
                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.NOTIFICATIONS, notification);
                    sendBroadcast(broadCast);
                    Intent intent = new Intent(mCtx, HomeScreenActivity.class);
                    intent.putExtra(ResponseParameters.OPEN_FRAGMENT, ResponseParameters.NOTIFICATIONS);
                    intent.putExtra("USER", user);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setAction(Long.toString(System.currentTimeMillis()));

                    String[] splitArray = notification.getNotificationDes().trim().split("\\|");

                    String groupName = "";
                    if (splitArray.length >= 2) {
                        groupName = splitArray[1].trim();
                    }


                    if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_INVITATION))
                        showSmallNotification("" + user.getFirstName() + " " + user.getLastName(), "" + user.getFirstName() + " " + user.getLastName() + " " + getResources().getString(R.string.notification_ask_join_group) + " " + groupName, intent, user, new Random().nextInt(1000000));
                    else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REQUESTED))
                        showSmallNotification("" + user.getFirstName() + " " + user.getLastName(), "" + user.getFirstName() + " " + user.getLastName() + " " + getResources().getString(R.string.notification_request_join_group) + " " + groupName, intent, user, new Random().nextInt(1000000));
                    else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REQUEST_ACCEPTED))
                        showSmallNotification("" + user.getFirstName() + " " + user.getLastName(), "" + user.getFirstName() + " " + user.getLastName() + " " + getResources().getString(R.string.notification_request_accepted_join_group) + " " + groupName, intent, user, new Random().nextInt(1000000));
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.LOGOUT)) {
                    SharedPreferencesMethod.clear(this);
                    if (Utility.isAppRunning(this)) {
                        Intent intent = new Intent(this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent); // open loginactivity
                        Log.e("TAG", "true");
                    }
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_DELETED)) {
                    String notificationId = notificationData.getString(ResponseParameters.Id);
                    Intent broadCast = new Intent();
                    broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.Id, notificationId);
                    sendBroadcast(broadCast);
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REPORTED_POST)
                        || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REPORTED_USER)
                        || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REPORTED_COMMENTS)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Group group = gson.fromJson(notificationData.getJSONObject(ResponseParameters.GROUP_INFO).toString(), Group.class); //parses the JSON string into an Cloth object
                    Intent intent = new Intent();
                    String message = "";
                    if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REPORTED_POST)) {
                        intent = new Intent(this, ReportedPostActivity.class);
                        message = getResources().getString(R.string.notification_new_reported_post);
                    } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REPORTED_USER)) {
                        intent = new Intent(this, ReportedUserActivity.class);
                        message = getResources().getString(R.string.notification_new_reported_user);
                    } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REPORTED_COMMENTS)) {
                        intent = new Intent(this, ReportedCommentActivity.class);
                        message = getResources().getString(R.string.notification_new_reported_comment);
                    }
                    intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, notificationData.getString(ResponseParameters.GROUP_IMG_BASE_URL));
                    intent.putExtra(ResponseParameters.GROUP, group);
                    intent.putExtra(ResponseParameters.TYPE, 3);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    showSmallNotification("" + group.groupName, message, intent, notificationData.getString(ResponseParameters.GROUP_IMG_BASE_URL) + group.logo, new Random().nextInt(1000000));
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_ROLE_UPDATE_NEW)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Type listType = new TypeToken<List<GroupMembers>>() {
                    }.getType();
                    Group group = gson.fromJson(notificationData.getJSONObject(ResponseParameters.GROUP_INFO).toString(), Group.class); //parses the JSON string into an Cloth object
                    Intent broadCast = new Intent();
                    List<GroupMembers> group_members = gson.fromJson(notificationData.getJSONArray(ResponseParameters.GROUP_MEMBERS).toString(), listType);
                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.GROUP, group);
                    broadCast.putExtra(ResponseParameters.GROUP_MEMBERS, (Serializable) group_members);
                    broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    sendBroadcast(broadCast);
                    String title = group.groupName;
                    String groupString = " " + getResources().getString(R.string.group);
                    String msg;
                    if (group_members.get(0).role.equalsIgnoreCase("0")) {
                        String message = getResources().getString(R.string.you_are_added_as_admin) + " ";
                        msg = message + group.groupName + groupString;
                    } else {
                        String message = getResources().getString(R.string.you_are_removed_as_admin) + " ";
                        msg = message + group.groupName + groupString;
                    }
                    Intent intent = new Intent(getApplicationContext(), HomeScreenActivity.class);
                    intent.putExtra(ResponseParameters.OPEN_FRAGMENT, ResponseParameters.NOTIFICATIONS);
                    intent.putExtra(ResponseParameters.OPEN_GROUP, ResponseParameters.NOTIFICATIONS);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    showSmallNotification(title, msg, intent, "", Integer.parseInt(group.groupId));
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_DELETED_ALL)) {
                    String categoryId = notificationData.getString("categoryId");
                    Intent broadCast = new Intent();
                    broadCast.setAction(activityTypeString.trim());
                    broadCast.putExtra(ResponseParameters.CATEGORY, categoryId);
                    broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    sendBroadcast(broadCast);
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_ADD) ||
                        activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_SHARED_POST)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    Post post = new Post();
                    if (notificationData.has(ResponseParameters.POST)) {
                        post = gson.fromJson(notificationData.getJSONObject(ResponseParameters.POST).toString(), Post.class); //parses the JSON string into an Cloth object
                    }
                    Group group = gson.fromJson(notificationData.getJSONObject(ResponseParameters.GROUP_INFO).toString(), Group.class); //parses the JSON string into an Cloth object
                    Intent intent = new Intent(mCtx, PostDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setAction(Long.toString(System.currentTimeMillis()));
                    intent.putExtra(ResponseParameters.POST, post);
                    intent.putExtra(ResponseParameters.GROUP, group);
                    intent.putExtra("back", true);
                    intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, notificationData.getString(ResponseParameters.GROUP_IMG_BASE_URL));
                    String message;
                    if (activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_ADD)) {
                        String s = getString(R.string.tile_noti_posted_in);
                        String group1 = getString(R.string.tile_noti_group);
                        message = " " + s + " " + group.groupName + " " + group1 + ".";
                    } else {
                        String group1 = getString(R.string.tile_noti_group);
                        String s = getString(R.string.tile_noti_shared_post);
                        message = " " + s + " " + group.groupName + " " + group1 + ".";
                    }
                    if (!post.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(mCtx))) {
                        Intent broadCast = new Intent();
                        broadCast.putExtra("groupid", group.groupId);
                        broadCast.setAction(activityTypeString.trim());
                        broadCast.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        sendBroadcast(broadCast);
                        String title = "" + post.firstName + " " + post.middleName + " " + post.lastName;
                        title = title.replaceAll("  "," ");
                        String s = "" + post.firstName + " " + post.middleName + " " + post.lastName + " ";
                        s = s.replaceAll("  "," ");
                        showSmallNotification(title, s + message, intent, API.imageUrl + post.avatar, new Random().nextInt(1000000));
                    }
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.BROADCAST)) {
                    Intent intent = new Intent(mCtx, HomeScreenActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    String title = "New Message from Kotumb";
                    String message = notificationData.getString("description");
                    String image = "";
                    int nextInt = new Random().nextInt(1000);
                    //  title = getFirstNStrings(message, 10);
                    message = "Tap to open";
                    showSmallNotification(title, message, intent, image, nextInt);
                    SharedPreferences settinge = getSharedPreferences("YOUR_PREF_NAME", MODE_PRIVATE);
                    int snowDensity = settinge.getInt("SNOW_DENSITY", 0);
                    if (snowDensity==11){
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putBoolean(SharedPreferencesMethod.POP_UP, true);
                        editor.commit();
                    }else{
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putBoolean(SharedPreferencesMethod.POP_UP, false);
                        editor.commit();
                    }
                } else if (activityTypeString.equalsIgnoreCase(ResponseParameters.PUBLIC_GROUP)) {
                    Gson gson = new Gson(); // creates a Gson instance
                    JSONObject groupInfo = notificationData.getJSONObject("groupInfo");
                    Group group = gson.fromJson(groupInfo.toString(), Group.class); //parses the JSON string into an group object

                    Intent intent = new Intent(mCtx, HomeScreenActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    intent.putExtra(ResponseParameters.OPEN_FRAGMENT, ResponseParameters.NOTIFICATIONS);
                    intent.putExtra(ResponseParameters.OPEN_GROUP, ResponseParameters.NOTIFICATIONS);
                    String title = getString(R.string.new_public_group_added_msg);
                    //  String message = notificationData.getString("description");
                    String image = "";
                    int nextInt = new Random().nextInt(1000);
                    //  title = getFirstNStrings(message, 10);
                    String message = group.groupName;
                    showSmallNotification(title, message, intent, image, nextInt);
                }
            }
        } catch (Exception e) {
            //status 1=pending,2=approved,3=blocked, 4 = invited
            e.printStackTrace();
        }
    }

    public static String getFirstNStrings(String str, int n) {
        try {
            String firstStrs = "";
            String[] sArr = str.split(" ");
            for (int i = 0; i < n; i++) {
                firstStrs += sArr[i] + " ";
            }
            return firstStrs.trim();
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }



    public void ProfileBoolean(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(SharedPreferencesMethod.POP_UP, true);
        editor.commit();
    }

    private void sendNotificationBroadcast(String activityTypeString) {
        String user = SharedPreferencesMethod.getUserId(getApplicationContext());
        try {
            if (activityTypeString.equalsIgnoreCase(ResponseParameters.PROFILEVIEWED)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_ADD)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_SHARED)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_SHARE)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_COMMENT)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_INVITED)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.SUPPORT_CHAT)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.MAKE_GROUP_ADMIN)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_INVITATION)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.MAKE_GROUP_MEMBER)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.REQUEST_ACCEPTED)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REQUEST_ACCEPTED)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_POST_LIKE)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.REQUESTACCEPTED)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.REQUESTED)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.ASKED_RECOM)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_REQUESTED)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_DELETED_ALL)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.GROUP_DELETE_ADMIN)
                    || activityTypeString.equalsIgnoreCase(ResponseParameters.NEW_VIDEO_NOTIFICATION)
                    ||activityTypeString.equalsIgnoreCase(ResponseParameters.VIDEO_APPROVE)
                    ) {
                Intent broadCast2 = new Intent();
                broadCast2.setAction(ResponseParameters.NOTIFICATIONS);
                broadCast2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);


                // Toast.makeText(mCtx, "BROAd", Toast.LENGTH_SHORT).show();
                //  sendBroadcast(broadCast2);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //the method will show a small notification for message
    //parameters are title for message title, message for message text and an intent and user object that will open
    //when you will tap on the notification
    public void showSmallNotificationWithInbox(String title, String message, Intent intent, User user, JSONArray NOTIFICATION) {
        try {

            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
            inboxStyle.setBigContentTitle(title);

            NotificationCompat.InboxStyle inboxStyleMulti = new NotificationCompat.InboxStyle();
            inboxStyleMulti.setBigContentTitle(getResources().getString(R.string.app_name));
            boolean multi = false;

            String userID = NOTIFICATION.getJSONObject(0).getString(ResponseParameters.UserId);
            for (int count = 0; count < NOTIFICATION.length(); count++) {
                JSONObject messageObject = NOTIFICATION.getJSONObject(count).getJSONObject(ResponseParameters.LAST_MSG_INFO);
                System.out.println("Notification Last Msg   "+messageObject.toString());
                String reply = EmojiMapUtil.replaceCheatSheetEmojis("" + messageObject.getString("reply"));
                if (NOTIFICATION.length() > 5) {
                    if (count >= NOTIFICATION.length() - 5) {
                        inboxStyle.addLine("" + reply);
                        inboxStyleMulti.addLine(NOTIFICATION.getJSONObject(count).getString(ResponseParameters.FirstName) + " : " + reply);
                    }
                } else {
                    inboxStyle.addLine("" + reply);
                    inboxStyleMulti.addLine(NOTIFICATION.getJSONObject(count).getString(ResponseParameters.FirstName) + " : " + reply);
                }
                if (NOTIFICATION.getJSONObject(count).getString(ResponseParameters.UserId).equalsIgnoreCase(userID) && !multi) {
                    multi = false;
                    userID = NOTIFICATION.getJSONObject(count).getString(ResponseParameters.UserId);
                } else {
                    multi = true;
                }
            }


            NotificationCompat.InboxStyle inbox;
            Bitmap bitmap;
            if (multi) {
                bitmap = BitmapFactory.decodeResource(mCtx.getResources(), R.mipmap.ic_launcher);
                inbox = inboxStyleMulti;
                title = getResources().getString(R.string.app_name);
                message = NOTIFICATION.length() + " " + getResources().getString(R.string.app_message_from_text) + " " + getResources().getString(R.string.app_your_connections_text);
                inbox.setSummaryText(NOTIFICATION.length() + " " + getResources().getString(R.string.app_message_from_text) + " " + getResources().getString(R.string.app_your_connections_text));
                intent = new Intent(mCtx, MessageActivity.class);
                intent.putExtra("USER", user);
                intent.putExtra(ResponseParameters.UPDATE_CHAT, "");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setAction(Long.toString(System.currentTimeMillis()));
            } else {
                bitmap = getBitmapFromURL(API.imageUrl + user.getAvatar());
                inbox = inboxStyle;
                message = NOTIFICATION.length() + " " + getResources().getString(R.string.app_message_from_text) + " " + NOTIFICATION.getJSONObject(0).getString(ResponseParameters.FirstName);
                inbox.setSummaryText(NOTIFICATION.length() + " " + getResources().getString(R.string.app_message_from_text) + " " + NOTIFICATION.getJSONObject(0).getString(ResponseParameters.FirstName));
            }


            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            mCtx,
                            ID_SMALL_NOTIFICATION,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx);
            String channelId = "channel-01";
            String channelName = "Channel Name";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.createNotificationChannel(mChannel);
                mBuilder.setChannelId(channelId);
            }
            Notification notification;
            notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title).setWhen(0)
                    .setAutoCancel(true)
                    .setContentIntent(resultPendingIntent)
                    .setStyle(inbox).setColor(getResources().getColor(R.color.colorPrimary))
                    .setContentTitle(title).setColor(getResources().getColor(R.color.colorPrimary))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(bitmap)
                    .setContentText(message)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setVibrate(new long[0])
                    .build();

            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            NotificationManager notificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(ID_SMALL_NOTIFICATION, notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showSmallNotificationSupport(String title, String message, Intent intent) {
        PendingIntent resultPendingIntent =
                PendingIntent.getBroadcast(
                        mCtx,
                        ID_SMALL_NOTIFICATION,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx);
        Notification notification;
        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message).setBigContentTitle(title))
                .setContentTitle(title).setColor(getResources().getColor(R.color.colorPrimary))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mCtx.getResources(), R.mipmap.ic_launcher))
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long[0])
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ID_SMALL_NOTIFICATION, notification);
    }


    //the method will show a small notification
    //parameters are title for message title, message for message text and an intent and user object that will open
    //when you will tap on the notification
    public void showSmallNotification(String title, String message, Intent intent, User user, int ID_SMALL_NOTIFICATION) {
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mCtx,
                        ID_SMALL_NOTIFICATION,
                        intent,
                        PendingIntent.FLAG_ONE_SHOT
                );


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx);
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
            mBuilder.setChannelId(channelId);
        }
        Notification notification;
        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message).setBigContentTitle(title))
                .setContentTitle(title).setColor(getResources().getColor(R.color.colorPrimary))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(getBitmapFromURL(API.imageUrl + user.getAvatar()))
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long[0])
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ID_SMALL_NOTIFICATION, notification);
    }

    public void showSmallNotification(String title, Intent intent, User user, int ID_SMALL_NOTIFICATION) {
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mCtx,
                        ID_SMALL_NOTIFICATION,
                        intent,
                        PendingIntent.FLAG_ONE_SHOT
                );


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx);
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
            mBuilder.setChannelId(channelId);
        }
        Notification notification;
        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(title).setColor(getResources().getColor(R.color.colorPrimary))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(getBitmapFromURL(API.imageUrl + user.getAvatar()))
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long[0])
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ID_SMALL_NOTIFICATION, notification);
    }

    public void showSmallNotification(String title, String message, Intent intent, String image, int ID_SMALL_NOTIFICATION) {
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mCtx,
                        ID_SMALL_NOTIFICATION,
                        intent,
                        PendingIntent.FLAG_ONE_SHOT
                );

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx);
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
            mBuilder.setChannelId(channelId);
        }else{
            System.out.println("Not ORio");
        }
        Notification notification;
        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message).setBigContentTitle(title))
                .setContentTitle(title).setColor(getResources().getColor(R.color.colorPrimary))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(getBitmapFromURL(image))
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long[0])
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ID_SMALL_NOTIFICATION, notification);

    }

    //the method will show a big notification with an image
    //parameters are title for message title, message for message text, url of the big image and an intent that will open
    //when you will tap on the notification
    public void showBigNotification(String title, String message, Intent intent, User user) {
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mCtx,
                        ID_BIG_NOTIFICATION,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        //bigPictureStyle.bigPicture(getBitmapFromURL(url));
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx);
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
            mBuilder.setChannelId(channelId);
        }
        Notification notification;
        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(title)
                .setStyle(bigPictureStyle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(getBitmapFromURL(API.imageUrl + user.getAvatar()))
                .setContentText(message)
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ID_BIG_NOTIFICATION, notification);
    }


    //The method will return Bitmap from an image URL
    private Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
