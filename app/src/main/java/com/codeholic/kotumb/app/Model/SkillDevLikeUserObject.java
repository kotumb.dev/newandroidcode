package com.codeholic.kotumb.app.Model;

public class SkillDevLikeUserObject {


    private String firstName;
    private String lastName;
    private String avatar;
    private String userLikedBy;

    public SkillDevLikeUserObject() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserLikedBy() {
        return userLikedBy;
    }

    public void setUserLikedBy(String userLikedBy) {
        this.userLikedBy = userLikedBy;
    }
}
