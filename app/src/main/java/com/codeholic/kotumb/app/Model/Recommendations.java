package com.codeholic.kotumb.app.Model;

/**
 * Created by DELL on 1/15/2018.
 */

public class Recommendations {

    User user = new User();
    String description = "";
    String status = "";
    String recomId = "";

    public String getRecomId() {
        return recomId;
    }

    public void setRecomId(String recomId) {
        this.recomId = recomId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
