package com.codeholic.kotumb.app.Interface;

import org.json.JSONObject;

public interface OnResponseListener {
    void onGotResponse(JSONObject response);
}