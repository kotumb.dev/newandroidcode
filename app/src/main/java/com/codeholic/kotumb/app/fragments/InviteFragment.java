package com.codeholic.kotumb.app.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.codeholic.kotumb.app.Activity.OtherUserProfileActivity;
import com.codeholic.kotumb.app.Utility.Utils;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Activity.ConnectionsActivity;
import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Activity.InviteSearchActivity;
import com.codeholic.kotumb.app.Adapter.RequestsRecyclerViewAdapter;
import com.codeholic.kotumb.app.Adapter.UserRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.shahroz.svlibrary.interfaces.onSearchListener;
import com.shahroz.svlibrary.interfaces.onSimpleSearchActionsListener;
import com.shahroz.svlibrary.widgets.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static com.codeholic.kotumb.app.Activity.HomeScreenActivity.createDynamicLink;


public class InviteFragment extends BaseFragment implements onSimpleSearchActionsListener, onSearchListener, View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.llYouMayKnow)
    LinearLayout llYouMayKnow;

    private String queryText = "";
    private boolean mSearchViewAdded = false;
    private boolean searchActive = false;
    private MaterialSearchView mSearchView;
    private WindowManager mWindowManager;
    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private static SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    @BindView(R.id.container)
    public ViewPager mViewPager;

    public static TabLayout tabLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.invites_tab, container, false);
        ButterKnife.bind(this, view);
        tabLayout = view.findViewById(R.id.tabs);
        ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.header_main_menu_option3));
        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());


        mViewPager.setOffscreenPageLimit(5);
        Log.e("onResume", "InviteFragment");
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(0);
        tabLayout.setupWithViewPager(mViewPager);
        TabLayout.Tab tab = tabLayout.getTabAt(1);
        tab.setCustomView(mSectionsPagerAdapter.getTabView(getResources().getString(R.string.invitations_received_text), ""));


        tab = tabLayout.getTabAt(2);
        tab.setCustomView(mSectionsPagerAdapter.getTabView(getResources().getString(R.string.invitations_sent_text), ""));

        tab = tabLayout.getTabAt(3);
        tab.setCustomView(mSectionsPagerAdapter.getTabView(getResources().getString(R.string.share_btn_text), ""));

        tab = tabLayout.getTabAt(0);
        tab.setCustomView(mSectionsPagerAdapter.getTabView(getResources().getString(R.string.suggestions_btn_text), ""));

        llYouMayKnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ConnectionsActivity.class);
                intent.putExtra("SUGGESTION", true);
                getActivity().startActivity(intent);
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                System.out.println("Search Item  "+mViewPager.getTransitionName());
            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        setHasOptionsMenu(true);
        initToolbar();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
//        SharedPreferencesMethod.setInt(getContext(),"tabIndex",tabLayout.getSelectedTabPosition());
        mViewPager.setCurrentItem(SharedPreferencesMethod.getInt(getContext(), "tabIndex"));
    }


    private void initToolbar() {

//        header.setText(getResources().getString(R.string.header_message_text));

        mWindowManager = (WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE);
        mSearchView = new MaterialSearchView(getContext());
        mSearchView.setOnSearchListener(this);
        mSearchView.setSearchResultsListener(this);
        mSearchView.setHintText(getResources().getString(R.string.invitations_search));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
//        tvNouser.setText(getResources().getString(R.string.no_message_found));

        if (mToolbar != null) {
            // Delay adding SearchView until Toolbar has finished loading
            mToolbar.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!mSearchViewAdded && mWindowManager != null) {
//                            mWindowManager.addView(mSearchView,
//                                    MaterialSearchView.getSearchViewLayoutParams(getActivity()));
//                            mSearchViewAdded = true;
//                            System.out.println("Search Data "+mSearchViewAdded);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, final MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
            Intent intent=new Intent(getContext(),InviteSearchActivity.class);
            if (mViewPager.getCurrentItem()==0){
                intent.putExtra("page","Suggetions Search");
                startActivity(intent);
            }else if(mViewPager.getCurrentItem()==1){
                intent.putExtra("page","Recieved Search");
                startActivity(intent);
            }else if(mViewPager.getCurrentItem()==2){
                intent.putExtra("page","Sent Search");
                startActivity(intent);
                }
                return true;
            }
        });
        if (searchActive)
            mSearchView.display();
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onClick(View v) {

    }

    private void setView(String qureyText){
        API.sendRequestToServerGET_FRAGMENT(getActivity(), this, API.SUGGESTION_USERS + "0" +"/"+qureyText+ "/?userid=" + SharedPreferencesMethod.getUserId(getActivity()), API.SUGGESTION_USERS); // service call for user list
        System.out.println("Search User   "+API.SUGGESTION_USERS+ "0" +"/"+qureyText+ "/?userid=" + SharedPreferencesMethod.getUserId(getActivity())+" "+API.SUGGESTION_USERS);
    }

    @Override
    public void onSearch(String query) {
        setView(query);
    }

    @Override
    public void searchViewOpened() {

    }

    @Override
    public void searchViewClosed() {

    }

    @Override
    public void onCancelSearch() {
        Log.e("onCancelSearch", "onCancelSearch");
        searchActive = false;
        mSearchView.hide();
    }

    @Override
    public void onItemClicked(String item) {

    }

    @Override
    public void onScroll() {

    }

    @Override
    public void error(String localizedMessage) {

    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private LinearLayoutManager linearLayoutManager;
        RecyclerView rvUserList;
        LinearLayout llLoading;
        List<User> users = new ArrayList<>();
        RequestsRecyclerViewAdapter friendListRecyclerViewAdapter;
        UserRecyclerViewAdapter userRecyclerViewAdapter;
        int pagination = 0;
        LinearLayout llNotFound;
        private static final String ARG_SECTION_NUMBER = "section_number";
        TextView textView;
        Context _context;
        Activity activity;
        private BroadcastReceiver receiver;
        private BroadcastReceiver requestReceiver;
        private View rootView;
        private User user;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            user = new User();
        }

        @Override
        public void onResume() {
            super.onResume();
            Bundle args = getArguments();
            int index = args.getInt(ARG_SECTION_NUMBER, 0);
            //setView(rootView, index);
        }



        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
            Bundle args = getArguments();
            Log.e("args", "" + args.getInt(ARG_SECTION_NUMBER, 0));
            int index = args.getInt(ARG_SECTION_NUMBER, 0);
            if (index == 2) {
                rootView = inflater.inflate(R.layout.fragment_share, container, false);
                TextView line_branch=rootView.findViewById(R.id.line_branch);
                CardView cvShare = rootView.findViewById(R.id.cvShare);
                cvShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        HomeScreenActivity.share(activity);
                       createDynamicLink(activity,SharedPreferencesMethod.getUserId(activity),Utils.referralCode);
                    }
                });
            } else {
                rootView = inflater.inflate(R.layout.fragment_invites, container, false);
                rvUserList = rootView.findViewById(R.id.rvUserList);
                llLoading = rootView.findViewById(R.id.llLoading);
                llNotFound = rootView.findViewById(R.id.llNotFound);
                textView = rootView.findViewById(R.id.tv);

                if (index == 0) {
                    textView.setText(getResources().getString(R.string.invitations_received_not_found));
                } else {
                    textView.setText(getResources().getString(R.string.invitations_sent_not_found));
                }
                if (index != 3) {
                    setBroadCast(index);
                    setReceivedBroadCast(index);
                }
                setView(rootView, index,"");
            }
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            this.activity = activity;

        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            _context = context;
        }

        @Override
        public void onDestroyView() {
            if (receiver != null) {
                _context.unregisterReceiver(receiver);
                receiver = null;
            }
            if (requestReceiver != null) {
                _context.unregisterReceiver(requestReceiver);
                requestReceiver = null;
            }
            super.onDestroyView();
        }

        public void setView(View rootView, int index,String qureyText) {
            try {
                if (!Utility.isConnectingToInternet(getActivity())) {
                    final Snackbar snackbar = Snackbar.make(rootView, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                    llLoading.setVisibility(GONE);
                    llNotFound.setVisibility(View.VISIBLE);
                    textView.setText(getResources().getString(R.string.app_no_internet_error));
                    return;
                }

                pagination = 0;

                if (index == 1) {
                    API.sendRequestToServerGET_FRAGMENT(getActivity(), this, API.INVITATIONS_SENT + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + pagination, API.INVITATIONS_SENT);
                    System.out.println("Invitation Sent  "+API.INVITATIONS_SENT + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + pagination+" "+API.INVITATIONS_SENT);
                }else if (index == 0) {
                    API.sendRequestToServerGET_FRAGMENT(getActivity(), this, API.INVITATIONS_RECEIVED + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + pagination, API.INVITATIONS_RECEIVED);
                    System.out.println("Invitation Recived  "+API.INVITATIONS_RECEIVED + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset="+ pagination+" "+ API.INVITATIONS_RECEIVED);
                }else if (index == 3) {
                    API.sendRequestToServerGET_FRAGMENT(getActivity(), this, API.SUGGESTION_USERS + "0" +"/"+qureyText+ "/?userid=" + SharedPreferencesMethod.getUserId(getActivity()), API.SUGGESTION_USERS); // service call for user list
                    System.out.println("Invitation Suggestion User  "+API.SUGGESTION_USERS + "0" + "/?userid=" + SharedPreferencesMethod.getUserId(getActivity())+" "+API.SUGGESTION_USERS);
                }
                } catch (Exception e) {
                e.printStackTrace();
            }
        }


        //broadcast for update user list and user disconnection
        private void setBroadCast(final int index) {
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    //do something based on the intent's action
                    Log.e("test", "test");
                    User updatedUser = intent.getParcelableExtra("USER");
                    for (int i = 0; i < users.size(); i++) {
                        try {
                            User user = users.get(i);
                            if (updatedUser.getUserId().equals(user.getUserId())) {
                                try {
                                    if (friendListRecyclerViewAdapter != null) {
                                        friendListRecyclerViewAdapter.notifyItemRemoved(i);
                                    }
                                    users.remove(i);
                                    updateCount(index);
                                    if (users.size() <= 0) {
                                        showNoUser();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            };

            IntentFilter filter = new IntentFilter();
            filter.addAction(ResponseParameters.UPDATE_USER);
            _context.registerReceiver(receiver, filter);
        }


        //broadcast for request received and request send (for change on list)
        private void setReceivedBroadCast(final int index) {
            requestReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    //do something based on the intent's action

                    if ((index == 0 && intent.getAction().equalsIgnoreCase(ResponseParameters.REQUEST_RECEIVED))
                            || (index == 1 && intent.getAction().equalsIgnoreCase(ResponseParameters.REQUEST_SEND))) {
                        User updatedUser = intent.getParcelableExtra("USER");
                        if (users.size() > 0) {
                            users.add(0, updatedUser);
                            friendListRecyclerViewAdapter.notifyItemInserted(0);
                        } else {
                            llNotFound.setVisibility(View.GONE);
                            users.add(0, updatedUser);
                            linearLayoutManager = new LinearLayoutManager(getActivity());
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rvUserList.setLayoutManager(linearLayoutManager);
                            friendListRecyclerViewAdapter = new RequestsRecyclerViewAdapter(rvUserList, getActivity(), users, PlaceholderFragment.this);
                            friendListRecyclerViewAdapter.setTab(index);
                            rvUserList.setAdapter(friendListRecyclerViewAdapter);

                            friendListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                @Override
                                public void onLoadMore() {
                                    if (users.size() >= 6) {
                                        Log.e("inside", "inside");
                                        rvUserList.post(new Runnable() {
                                            public void run() {
                                                users.add(null);
                                                friendListRecyclerViewAdapter.notifyItemInserted(users.size() - 1);
                                            }
                                        });
                                        System.out.println("Invitation Received 2   "+API.INVITATIONS_RECEIVED + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + (pagination + 6)+" "+API.INVITATIONS_RECEIVED_UPDATE);
                                        API.sendRequestToServerGET_FRAGMENT(getActivity(), PlaceholderFragment.this, API.INVITATIONS_RECEIVED + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + (pagination + 6), API.INVITATIONS_RECEIVED_UPDATE);
                                    }
                                }
                            });
                        }
                        addCount(index, users.size());
                    }
                }
            };

            IntentFilter filter = new IntentFilter();
            filter.addAction(ResponseParameters.REQUEST_RECEIVED);
            filter.addAction(ResponseParameters.REQUEST_SEND);
            _context.registerReceiver(requestReceiver, filter);
        }

        public void getResponse(JSONObject outPut, final int i) {
            // updateCount(0); updateCount(1);
            llLoading.setVisibility(GONE);
            try {
                if (i == 0 || i == 1 || i == 6) {
                    if (i == 6)
                        Log.e("output", outPut.toString());
                    System.out.println("Invitation Response  "+outPut.toString());
                    if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                        if (outPut.has(ResponseParameters.Errors)) {
                            llNotFound.setVisibility(View.VISIBLE);
                        } else if (outPut.has(ResponseParameters.Success)) {
                            if (outPut.has(ResponseParameters.INVITATIONS) || outPut.has(ResponseParameters.RESULTS)) {
                                llNotFound.setVisibility(View.GONE);

                                JSONArray userList = new JSONArray();
                                if (outPut.has(ResponseParameters.INVITATIONS)) {
                                    userList = outPut.getJSONArray(ResponseParameters.INVITATIONS);
                                }
                                if (outPut.has(ResponseParameters.RESULTS)) {
                                    userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                                }

                                if (userList.length() <= 0) {
                                    llNotFound.setVisibility(View.VISIBLE);
                                }

                                users = new ArrayList<>();
                                for (int j = 0; j < userList.length(); j++) {
                                    JSONObject searchResults = userList.getJSONObject(j);
                                    User user = new User();
                                    user.setFirstName(searchResults.getString(ResponseParameters.FirstName));
                                    user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                                    user.setLastName(searchResults.getString(ResponseParameters.LastName));
                                    user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                                    user.setUserId(searchResults.getString(ResponseParameters.UserId));
                                    user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                                    user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                                    user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                                    user.setCity(searchResults.getString(ResponseParameters.City));
                                    user.setState(searchResults.getString(ResponseParameters.State));
                                    user.setUserInfo(searchResults.toString());
                                    user.setIsDeleted(searchResults.getString(ResponseParameters.IS_DELETED));
                                    user.setIsBlocked(searchResults.getString(ResponseParameters.IS_BLOCKED));
                                    users.add(user);
                                }


                                if (i == 6)
                                    Log.e("output", "size " + users.size());
                                linearLayoutManager = new LinearLayoutManager(getActivity());
                                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                                rvUserList.setLayoutManager(linearLayoutManager);
                                if (i == 6) {
                                    userRecyclerViewAdapter = new UserRecyclerViewAdapter(rvUserList, getActivity(), users);

                                    rvUserList.setAdapter(userRecyclerViewAdapter);

                                    userRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                        @Override
                                        public void onLoadMore() {
                                            if (users.size() >= 6) {
                                                Log.e("inside", "inside");
                                                rvUserList.post(new Runnable() {
                                                    public void run() {
                                                        users.add(null);
                                                        userRecyclerViewAdapter.notifyItemInserted(users.size() - 1);
                                                    }
                                                });
                                                System.out.println("Invtation Suggetion User   "+API.SUGGESTION_USERS + (pagination + 6) + "/?userid=" + SharedPreferencesMethod.getUserId(getActivity())+"  "+API.SUGGESTION_USERS_UPDATE);
                                                API.sendRequestToServerGET_FRAGMENT(getActivity(), PlaceholderFragment.this, API.SUGGESTION_USERS + (pagination + 6) + "/?userid=" + SharedPreferencesMethod.getUserId(getActivity()), API.SUGGESTION_USERS_UPDATE); // service call for user list
                                            }
                                        }
                                    });

                                } else {
                                    friendListRecyclerViewAdapter = new RequestsRecyclerViewAdapter(rvUserList, getActivity(), users, this);
                                    friendListRecyclerViewAdapter.setTab(i);
                                    rvUserList.setAdapter(friendListRecyclerViewAdapter);

                                    friendListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                        @Override
                                        public void onLoadMore() {
                                            if (users.size() >= 6) {
                                                Log.e("inside", "inside");
                                                rvUserList.post(new Runnable() {
                                                    public void run() {
                                                        users.add(null);
                                                        friendListRecyclerViewAdapter.notifyItemInserted(users.size() - 1);
                                                    }
                                                });

                                                if (i == 1) {
                                                    System.out.println("Invitations Sent 2  "+API.INVITATIONS_SENT + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + (pagination + 6)+" "+API.INVITATIONS_SENT_UPDATE);
                                                    API.sendRequestToServerGET_FRAGMENT(getActivity(), PlaceholderFragment.this, API.INVITATIONS_SENT + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + (pagination + 6), API.INVITATIONS_SENT_UPDATE);
                                                }else if (i == 0) {
                                                    System.out.println("Invitations Received 2  "+API.INVITATIONS_RECEIVED + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + (pagination + 6)+" "+API.INVITATIONS_RECEIVED_UPDATE);
                                                    API.sendRequestToServerGET_FRAGMENT(getActivity(), PlaceholderFragment.this, API.INVITATIONS_RECEIVED + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + (pagination + 6), API.INVITATIONS_RECEIVED_UPDATE);
                                                }else if (i == 6) {
                                                     System.out.println("Suggestion User 2  "+API.SUGGESTION_USERS + (pagination + 6) + "/?userid=" + SharedPreferencesMethod.getUserId(getActivity())+" "+API.SUGGESTION_USERS_UPDATE);
                                                    API.sendRequestToServerGET_FRAGMENT(getActivity(), PlaceholderFragment.this, API.SUGGESTION_USERS + (pagination + 6) + "/?userid=" + SharedPreferencesMethod.getUserId(getActivity()), API.SUGGESTION_USERS_UPDATE); // service call for user list
                                                }
                                                }
                                        }
                                    });
                                }


                                if (outPut.has(ResponseParameters.TOTAL_ROWS)) {
                                    if (Integer.parseInt(outPut.getString(ResponseParameters.TOTAL_ROWS)) > 0) {
                                        if (i == 0) {
                                            TabLayout.Tab tab = InviteFragment.tabLayout.getTabAt(1);
                                            tab.setCustomView(null);
                                            tab.setCustomView(mSectionsPagerAdapter.getTabView(getResources().getString(R.string.invitations_received_text), "" + Integer.parseInt(outPut.getString(ResponseParameters.TOTAL_ROWS))));
                                        } else if (i == 1) {
                                            TabLayout.Tab tab = InviteFragment.tabLayout.getTabAt(2);
                                            tab.setCustomView(null);
                                            tab.setCustomView(mSectionsPagerAdapter.getTabView(getResources().getString(R.string.invitations_sent_text), "" + Integer.parseInt(outPut.getString(ResponseParameters.TOTAL_ROWS))));
                                        }
                                    }
                                }else {
                                    if (i == 0) {
                                        TabLayout.Tab tab = InviteFragment.tabLayout.getTabAt(1);
                                        tab.setCustomView(null);
                                        tab.setCustomView(mSectionsPagerAdapter.getTabView(getResources().getString(R.string.invitations_received_text), "" + Integer.parseInt(outPut.getString(ResponseParameters.TOTAL_ROWS))));
                                    } else if (i == 1) {
                                        TabLayout.Tab tab = InviteFragment.tabLayout.getTabAt(2);
                                        tab.setCustomView(null);
                                        tab.setCustomView(mSectionsPagerAdapter.getTabView(getResources().getString(R.string.invitations_sent_text), "" + Integer.parseInt(outPut.getString(ResponseParameters.TOTAL_ROWS))));
                                    }
                                }
                            }
                        } else {
                            llNotFound.setVisibility(View.VISIBLE);
                        }
                    } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                }
                if (i == 2 || i == 3 || i == 7) {
                    users.remove(users.size() - 1);
                    if (i == 7) {
                        userRecyclerViewAdapter.notifyItemRemoved(users.size());
                    } else {
                        friendListRecyclerViewAdapter.notifyItemRemoved(users.size());
                    }
                    if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                        if (outPut.has(ResponseParameters.Success)) {
                            if (outPut.has(ResponseParameters.INVITATIONS) || outPut.has(ResponseParameters.RESULTS)) {

                                JSONArray userList = new JSONArray();
                                if (outPut.has(ResponseParameters.INVITATIONS)) {
                                    userList = outPut.getJSONArray(ResponseParameters.INVITATIONS);
                                }
                                if (outPut.has(ResponseParameters.RESULTS)) {
                                    userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                                }

                                llNotFound.setVisibility(View.GONE);
                                for (int j = 0; j < userList.length(); j++) {
                                    JSONObject searchResults = userList.getJSONObject(j);
                                    User user = new User();
                                    user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                                    user.setLastName(searchResults.getString(ResponseParameters.LastName));
                                    user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                                    user.setUserId(searchResults.getString(ResponseParameters.UserId));
                                    user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                                    user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                                    user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                                    user.setCity(searchResults.getString(ResponseParameters.City));
                                    user.setState(searchResults.getString(ResponseParameters.State));
                                    user.setUserInfo(searchResults.toString());
                                    user.setIsDeleted(searchResults.getString(ResponseParameters.IS_DELETED));
                                    user.setIsBlocked(searchResults.getString(ResponseParameters.IS_BLOCKED));
                                    users.add(user);
                                }
                                pagination = pagination + 6;
                                if (i == 7) {
                                    userRecyclerViewAdapter.notifyDataSetChanged();
                                    userRecyclerViewAdapter.setLoaded();
                                } else {
                                    friendListRecyclerViewAdapter.notifyDataSetChanged();
                                    friendListRecyclerViewAdapter.setLoaded();
                                }
                            }
                        }
                    }
                }

                if (i == 4 || i == 5) {
                    if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                        if (outPut.has(ResponseParameters.Success)) {
                            if (outPut.has(ResponseParameters.INVITATIONS)) {
                                llNotFound.setVisibility(View.GONE);
                                for (int j = 0; j < outPut.getJSONArray(ResponseParameters.INVITATIONS).length(); j++) {
                                    JSONObject searchResults = outPut.getJSONArray(ResponseParameters.INVITATIONS).getJSONObject(j);
                                    User user = new User();
                                    boolean check = true;
                                    for (int k = 0; k < users.size(); k++) {
                                        User checkUser = users.get(k);
                                        if (checkUser.getUserId().equalsIgnoreCase(searchResults.getString(ResponseParameters.UserId))) {
                                            check = false;
                                            break;
                                        }
                                    }
                                    if (check) {
                                        user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                                        user.setLastName(searchResults.getString(ResponseParameters.LastName));
                                        user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                                        user.setUserId(searchResults.getString(ResponseParameters.UserId));
                                        user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                                        user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                                        user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                                        user.setCity(searchResults.getString(ResponseParameters.City));
                                        user.setState(searchResults.getString(ResponseParameters.State));
                                        user.setUserInfo(searchResults.toString());
                                        user.setIsDeleted(searchResults.getString(ResponseParameters.IS_DELETED));
                                        user.setIsBlocked(searchResults.getString(ResponseParameters.IS_BLOCKED));
                                        users.add(user);
                                    }
                                }
                                //pagination = pagination + 6;
                                friendListRecyclerViewAdapter.notifyDataSetChanged();
                                //groupListRecyclerViewAdapter.setLoaded();
                            }
                        }
                    }
                }


            } catch (Exception e) {
                llNotFound.setVisibility(View.VISIBLE);
                e.printStackTrace();
            }
        }

        public void showNoUser() {
            llNotFound.setVisibility(View.VISIBLE);
        }

        public void updateCount(int tabNumber) {
            String count;
            int index = tabNumber;
            if (tabNumber == 0) {
                index = 1;
            } else if (tabNumber == 1) {
                index = 2;
            }
            TabLayout.Tab tab = InviteFragment.tabLayout.getTabAt(index);
            TextView textView = tab.getCustomView().findViewById(R.id.tvCount);
            count = textView.getText().toString();

            if (count.trim().isEmpty()) {
                textView.setVisibility(GONE);
            } else {
                textView.setText("" + (Integer.parseInt(count) - 1));
                if ((Integer.parseInt(count) - 1) > 0)
                    textView.setVisibility(View.VISIBLE);
                else
                    textView.setVisibility(View.GONE);
            }

            if (tabNumber == 1) {
                System.out.println("Invitations Sent 3   "+API.INVITATIONS_SENT + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + (pagination)+" "+API.INVITATIONS_SENT_UPDATE_);
                API.sendRequestToServerGET_FRAGMENT(getActivity(), this, API.INVITATIONS_SENT + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + (pagination), API.INVITATIONS_SENT_UPDATE_);
            } else if (tabNumber == 0) {
                System.out.println("Invitations Sent+Received 3   "+API.INVITATIONS_SENT + API.INVITATIONS_RECEIVED + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + (pagination)+" "+API.INVITATIONS_RECEIVED_UPDATE_);
                API.sendRequestToServerGET_FRAGMENT(getActivity(), this, API.INVITATIONS_RECEIVED + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + (pagination), API.INVITATIONS_RECEIVED_UPDATE_);
            }
            //tab.setCustomView(mSectionsPagerAdapter.getTabView(getResources().getString(R.string.sent), ""+count));
        }

        public void addCount(int tabNumber, int count) {
            int index = tabNumber;
            if (tabNumber == 0) {
                index = 1;
            } else if (tabNumber == 1) {
                index = 2;
            }
            TabLayout.Tab tab = InviteFragment.tabLayout.getTabAt(index);
            tab.setCustomView(null);
            if (tabNumber == 0)
                tab.setCustomView(mSectionsPagerAdapter.getTabView(getResources().getString(R.string.invitations_received_text), "" + count));
            if (tabNumber == 1)
                tab.setCustomView(mSectionsPagerAdapter.getTabView(getResources().getString(R.string.invitations_sent_text), "" + count));
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a FavLinkFragment (defined as a static inner class below).
            int sectionNumber = position;
            switch (position) {
                case 0:
                    sectionNumber = 3;
                    break;
                case 1:
                    sectionNumber = 0;
                    break;
                case 2:
                    sectionNumber = 1;
                    break;
                case 3:
                    sectionNumber = 2;
                    break;
            }
            return PlaceholderFragment.newInstance(sectionNumber);
        }

        public View getTabView(String title, String count) {
            Toolbar mToolbar;
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View v = LayoutInflater.from(getActivity()).inflate(R.layout.invite_tab, null);
            mToolbar=v.findViewById(R.id.toolbar);
            TextView tv = v.findViewById(R.id.tv);
            ImageView ivImage = v.findViewById(R.id.ivImage);
            tv.setText(title);
            if (title.trim().equalsIgnoreCase(getResources().getString(R.string.share_btn_text))) {
                ivImage.setImageResource(R.drawable.ic_share_black_24dp);
            }
            TextView tvCount = v.findViewById(R.id.tvCount);
            if (count.length() > 2) {
                count = "99+";
            }
            tvCount.setText(count);
            if (!count.isEmpty()) {
                tvCount.setVisibility(View.VISIBLE);
            }

            return v;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.invitations_received_text);
                case 1:
                    return getResources().getString(R.string.invitations_sent_text);
                case 2:
                    return getResources().getString(R.string.invitations_received_text);
                case 3:
                    return getResources().getString(R.string.invitations_sent_text);
            }
            return null;
        }
    }
}
