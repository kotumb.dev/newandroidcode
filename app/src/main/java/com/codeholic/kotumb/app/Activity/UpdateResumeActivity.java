package com.codeholic.kotumb.app.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utils;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import butterknife.BindView;
import butterknife.ButterKnife;
public class UpdateResumeActivity extends AppCompatActivity {
    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.rsFirstName)
    EditText resumefirstName;

    @BindView(R.id.rsMiddleName)
    EditText resumeMiddleName;

    @BindView(R.id.rsLastName)
    EditText resumeLastName;

    @BindView(R.id.rsEmail)
    EditText resumeEmail;


    @BindView(R.id.rsState)
    EditText resumeState;


    @BindView(R.id.rsProfileSummery)
    EditText resumeProfileSummery;

    @BindView(R.id.rsAddress)
    EditText resumeAddress;

    @BindView(R.id.rsPincode)
    EditText resumePinCode;

    @BindView(R.id.rsCity)
    EditText resumeCity;

    @BindView(R.id.rsFirstNameLayout)
    TextInputLayout rsFirstNameLayout;


    @BindView(R.id.rsMIddleNameLayout)
    TextInputLayout rsMIddleNameLayout;


    @BindView(R.id.rsLastNameLayout)
    TextInputLayout rsLastNameLayout;


    @BindView(R.id.rsPincodeLayout)
    TextInputLayout rsPincodeLayout;


    @BindView(R.id.rsStateLayout)
    TextInputLayout rsSateLayout;



    @BindView(R.id.rsCityLayout)
    TextInputLayout rsCityLayout;


    @BindView(R.id.rsProfileSummaryLayout)
    TextInputLayout rsProfileSummaryLayout;



    @BindView(R.id.rsEmailLayout)
    TextInputLayout rsEmailLayout;

    @BindView(R.id.tvResumeUpdate)
    TextView updateResume;
    String resumeId;
    String ProfileURL;
    String country;
    String intrest,profileHeadline;
    String mobile_number;
    boolean pin=true;
    ArrayList<String>years=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_resume);
        ButterKnife.bind(this);
        initToolbar();
        setView();
        getResumeDetails();
        updateResumeDetails();

    }
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        //setting screen title
        header.setText(getResources().getString(R.string.resume_template_edit_resume_btn_txt));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                API.sendRequestToServerGET(UpdateResumeActivity.this, API.GET_RESUME_INFO + SharedPreferencesMethod.getUserId(UpdateResumeActivity.this),API.GET_RESUME_INFO);
                onBackPressed();
            }
        });
    }



    public void updateResumeDetails(){
            updateResume.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{
                          Validation();
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            });
    }












    public void getResumeDetails(){
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait_text));
        progressDialog.show();
        User user = SharedPreferencesMethod.getUserInfo(this);
        final String userId=user.getUserId();
        final String url = API.GET_RESUME_INFO+userId;
        JsonObjectRequest  getRequest = new JsonObjectRequest(Request.Method.GET, url, (String) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.toString().equalsIgnoreCase("")){
//                            Toast.makeText(UpdateResumeActivity.this, "No Data Of Resume", Toast.LENGTH_SHORT).show();
                        }else{
                            progressDialog.dismiss();
                            System.out.println("Get Respoonse   "+response);
                            try{
                                JSONObject result=new JSONObject(response.get("userInfo").toString());
                                resumefirstName.setText(result.getString("firstName"));
                                resumeMiddleName.setText(result.getString("middleName"));
                                resumeLastName.setText(result.getString("lastName"));
                                resumeEmail.setText(result.getString("email"));
                                resumeAddress.setText(result.getString("address"));
                                resumeCity.setText(result.getString("city"));
                                resumeState.setText(result.getString("state"));
                                resumePinCode.setText(result.getString("zipcode"));
                                resumeProfileSummery.setText(result.getString("profileSummary"));
                                resumePinCode.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                        getZipCode(resumePinCode.getText().toString());
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
//                                        getZipCode(resumePinCode.getText().toString());
                                    }
                                });
                                intrest=(result.getString("interests"));
                                profileHeadline=(result.getString("profileHeadline"));
                                resumeId=result.getString("resumeId");
                                country=result.getString("country");
                                ProfileURL=result.getString("profileUrl");
                                System.out.println("Profile URL   "+ProfileURL);
                                mobile_number=result.getString("mobileNumber");
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );
        Volley.newRequestQueue(this).add(getRequest);
    }




    public void insertResumeData(){
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait_text));
        progressDialog.show();
        final User user = SharedPreferencesMethod.getUserInfo(this);
        final String user_id=user.getUserId();
        String url;
        url = API.UPDATE_RESUME;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pin = false;
                        try {
                            if (ProfileURL.equalsIgnoreCase("")){
//                                Toast.makeText(UpdateResumeActivity.this, "Profile URL is Null", Toast.LENGTH_SHORT).show();
                            }else{
                                System.out.println("Main Response  "+response);
                                progressDialog.dismiss();
                                final Snackbar snackbar = Snackbar.make(updateResume,ResponseParameters.Success, Toast.LENGTH_SHORT);
                                snackbar.show();
                                Utils.showPopup(UpdateResumeActivity.this, getString(R.string.success_text));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String,String> input = new HashMap<>();
                input.put(RequestParameters.USERID, "" + user_id);
                input.put(RequestParameters.RESUMEID,""+resumeId);
                input.put(RequestParameters.AVATAR,""+user.getAvatar());
                input.put("resumeId",""+resumeId);
                input.put("zipcode",""+resumePinCode.getText().toString());
                input.put(RequestParameters.PROFILE_URL, ProfileURL);
                input.put(RequestParameters.COUNTRY, "" + country);
                input.put(RequestParameters.FIRSTNAME, "" + resumefirstName.getText().toString().trim());
                input.put(RequestParameters.MIDDLENAME, "" + resumeMiddleName.getText().toString().trim());
                input.put(RequestParameters.LASTNAME, "" + resumeLastName.getText().toString().trim());
                input.put(RequestParameters.EMAIL, "" + resumeEmail.getText().toString().trim());
                input.put(RequestParameters.MOBILE_NUMBER,"" +mobile_number);
                input.put(RequestParameters.ADDRESS, "" + resumeAddress.getText().toString().trim());
                input.put(RequestParameters.CITY, "" + resumeCity.getText().toString().trim());
                input.put("headline", "" + profileHeadline);
                input.put("summary", "" + resumeProfileSummery.getText().toString().trim());
                input.put("interest", "" + intrest);
                input.put(RequestParameters.STATE, "" + resumeState.getText().toString().trim());
                System.out.println("Get Input   "+input);
                return input;
            }
        };
        Volley.newRequestQueue(this).add(postRequest);
    }


    private void setView(){
        resumefirstName=findViewById(R.id.rsFirstName);
        resumeMiddleName=findViewById(R.id.rsMiddleName);
        resumeLastName=findViewById(R.id.rsLastName);
        resumeEmail=findViewById(R.id.rsEmail);
        resumeState=findViewById(R.id.rsState);
        resumeCity=findViewById(R.id.rsCity);
        resumeAddress=findViewById(R.id.rsAddress);
        resumePinCode=findViewById(R.id.rsPincode);
        resumeProfileSummery=findViewById(R.id.rsProfileSummery);
        updateResume=findViewById(R.id.tvResumeUpdate);
    }



    public void Validation(){
        disableError();
        boolean fNameMatcher = Pattern.matches("[a-zA-Z0-9 ]+", resumefirstName.getText().toString().trim());
        boolean lNameMatcher = Pattern.matches("[a-zA-Z0-9 ]+", resumeLastName.getText().toString().trim());
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String email = resumeEmail.getText().toString().trim();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(resumeLastName.getWindowToken(), 0);
        if (resumefirstName.getText().toString().trim().isEmpty()){
            enableError(rsFirstNameLayout, getResources().getString(R.string.firstname_empty_input));
        }else if(resumeLastName.getText().toString().trim().isEmpty()) {
            enableError(rsFirstNameLayout, getResources().getString(R.string.lastname_empty_input));
        }else if (!fNameMatcher){
            enableError(rsFirstNameLayout, getResources().getString(R.string.name_incorrect_input_numeric));
        }else if (!lNameMatcher){
            enableError(rsLastNameLayout, getResources().getString(R.string.name_incorrect_input_numeric));
        }else if(!email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")||email.equalsIgnoreCase("")){
            enableError(rsEmailLayout, getResources().getString(R.string.email_incorrect_input_format));
        }else if(resumePinCode.getText().toString().trim().length()<6){
            enableError(rsPincodeLayout, getResources().getString(R.string.zipcode_incorrect_input_maxlength));
        }else if(resumePinCode.getText().toString().equalsIgnoreCase("")){
            enableError(rsPincodeLayout, getResources().getString(R.string.zipcode_empty_input));
        }else if(resumePinCode.getText().toString().trim().length()!=6){
            enableError(rsPincodeLayout, getResources().getString(R.string.zipcode_incorrect_input_maxlength));
        }
        else{
            getZipCode(resumePinCode.getText().toString());
            insertResumeData();
        }
    }


    private void enableError(TextInputLayout inputLayout, String error) {
        try {
            inputLayout.setErrorEnabled(true);
            inputLayout.setError(error);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void setEvent(){

    }



    public void getZipCode(final String zipCode){
        final String url = API.ZIPCODE + zipCode;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Get Response   " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = new JSONArray(jsonObject.getString("zipcodes"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject results = jsonArray.getJSONObject(i);
                                final String city = results.getString("city");
                                final String state = results.getString("state");
                                final String zipcode = results.getString("zipcode");
                                System.out.println("City "+city);
                                if (zipCode.trim().length() < 6) {
                                    enableError(rsPincodeLayout, getResources().getString(R.string.zipcode_incorrect_input_maxlength));
                                }else{
                                    rsPincodeLayout.setErrorEnabled(false);
                                }
                                if (zipcode.equalsIgnoreCase(resumePinCode.getText().toString())){
                                    System.out.println("City    "+ city);
                                    resumeCity.setText(city);
                                    resumeState.setText(state);
                                    rsPincodeLayout.setErrorEnabled(false);
                                    updateResume.setEnabled(true);
                                }else{
                                        enableError(rsPincodeLayout, getResources().getString(R.string.zipcode_incorrect_input));
                                        updateResume.setEnabled(false);
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        Volley.newRequestQueue(this).add(stringRequest);
    }




    void disableError() {
        try {
            rsFirstNameLayout.setErrorEnabled(false);
            rsMIddleNameLayout.setErrorEnabled(false);
            rsLastNameLayout.setErrorEnabled(false);
            rsCityLayout.setErrorEnabled(false);
            rsSateLayout.setErrorEnabled(false);
            rsPincodeLayout.setErrorEnabled(false);
            rsEmailLayout.setErrorEnabled(false);
            rsProfileSummaryLayout.setErrorEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
