package com.codeholic.kotumb.app.Activity;

import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Adapter.MyListAdapter;
import com.codeholic.kotumb.app.Interface.OnGetViewListener;
import com.codeholic.kotumb.app.Model.RewardObject;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.TimeAgo;
import com.codeholic.kotumb.app.Utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class RewardHistoryActivty extends AppCompatActivity {
    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.back)
    LinearLayout back;

    @BindView(R.id.listView)
    ListView listView;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.progress_circular)
    ProgressBar llLoading;

    @BindView(R.id.tv)
    TextView textView;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.ivUser)
    View ivUser;

    @BindView(R.id.swipelayout)
    SwipeRefreshLayout swipelayout;


    private int pagination = 0;
    private boolean flag_loading;
    private View footerView;
    private ArrayList<RewardObject> objects = new ArrayList<>();
    private MyListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward_history_activty);
        ButterKnife.bind(this);
        initToolbar();
        setupListView();
        fetchHistory();
        swipelayout.setEnabled(false);
    }

    public void fetchHistory() {
        try {
            if (!Utility.isConnectingToInternet(RewardHistoryActivty.this)) {
                final Snackbar snackbar = Snackbar.make(llNotFound, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(View.VISIBLE);
                textView.setText(getResources().getString(R.string.app_no_internet_error));
                return;
            }

            pagination = 0;

            llLoading.setVisibility(VISIBLE);
            llNotFound.setVisibility(View.GONE);
//            cvContainer.setVisibility(View.GONE);

            objects.clear();
            String userId = SharedPreferencesMethod.getUserId(RewardHistoryActivty.this);
            String url = API.REWARD_HISTORY + userId + "/" + pagination;
            API.sendRequestToServerGET(RewardHistoryActivty.this, url, API.REWARD_HISTORY);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupListView() {
        listView = findViewById(R.id.listView);
        adapter = getAdapter();
        listView.setAdapter(adapter);

        setOnLoadMoreListener(listView, new OnGetViewListener() {
            @Override
            public View onGetView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater inflater = getLayoutInflater();
                footerView = inflater.inflate(R.layout.item_loading, viewGroup, false);
                listView.addFooterView(footerView);
                pagination = pagination + 8;
                String userId = SharedPreferencesMethod.getUserId(RewardHistoryActivty.this);
                String url = API.REWARD_HISTORY + userId + "/" + pagination;
                API.sendRequestToServerGET(RewardHistoryActivty.this, url, API.REWARD_HISTORY + 1);

                return null;
            }
        });

    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        String title = "Rewards History";
        getSupportActionBar().setTitle(title);
        header.setText(title);
        ivUser.setVisibility(GONE);
        //setting screen title

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void setOnLoadMoreListener(final ListView listView, final OnGetViewListener listener) {
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {


            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if (!flag_loading) {
                        flag_loading = true;
                        listener.onGetView(0, null, null);
                    }
                }

                int topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();
                //swipelayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });
    }

    @NonNull
    private MyListAdapter getAdapter() {
        return new MyListAdapter(objects, new OnGetViewListener() {
            @Override
            public View onGetView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.reward_history_item, viewGroup, false);            //old
                TextView text = v.findViewById(R.id.text);
                TextView date = v.findViewById(R.id.date);

                final RewardObject rewardObject = objects.get(i);

                if (rewardObject.gettType().equalsIgnoreCase("credit")) {
                    String s = getString(R.string.was_added_text_reward_history);
                    text.setText(rewardObject.getPoints() + " " + rewardObject.getTitle()+ " " + s);

                } else {
                    String s = getString(R.string.was_deducted_text_reward_history);
                    text.setText(rewardObject.getPoints() + " " + rewardObject.getTitle() + " " + s);
                }

                String createdAt = rewardObject.getCreatedAt();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                try {
                    Date parse = dateFormat.parse(createdAt);
                    date.setText(new TimeAgo().locale(RewardHistoryActivty.this).getTimeAgo(parse));
                } catch (ParseException e) {
                    e.printStackTrace();
                    date.setText(createdAt);
                }
                return v;
            }
        });
    }

    public void getResponse(JSONObject outPut, int i) {
        if (i == 0) {
            llLoading.setVisibility(GONE);
            Log.e("output", outPut.toString());
            Log.e("xx", "getResponse: "+outPut);
            if (outPut.has("success")) {
                try {
                    JSONArray outPutJSONArray = outPut.getJSONArray("reward_points");
                    objects.addAll(Utility.getRewardObjects(outPutJSONArray));
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                    showErrorUI(getString(R.string.empty_reward_history_text));
                }
            } else {
                showErrorUI(getString(R.string.empty_reward_history_text));
            }
        } else if (i == 1) {
            flag_loading = false;
            listView.removeFooterView(footerView);
            Log.e("output", outPut.toString());
            Log.e("xx", "getResponse1: "+outPut);
            if (outPut.has("success")) {
                try {
                    JSONArray outPutJSONArray = outPut.getJSONArray("reward_points");
                    ArrayList<RewardObject> rewardObjects = Utility.getRewardObjects(outPutJSONArray);
                    this.objects.addAll(rewardObjects);
                    adapter.notifyDataSetChanged();
                    if (rewardObjects.size() == 0) {
                        flag_loading = true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showErrorUI(getString(R.string.empty_reward_history_text));
                }
            } else {
                flag_loading = true;
                // showErrorUI(getString(R.string.something_wrong));
            }
        }
    }

    private void showErrorUI(String errorText) {
        llNotFound.setVisibility(VISIBLE);
        textView.setText(errorText);
    }
}
