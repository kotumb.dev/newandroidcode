package com.codeholic.kotumb.app.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Adapter.CommentListRecyclerViewAdapter;
import com.codeholic.kotumb.app.Adapter.PostListRecyclerViewAdapter;
import com.codeholic.kotumb.app.Animation.BounceInterpolator;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.GroupMembers;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.Model.PostComments;
import com.codeholic.kotumb.app.Model.PostLikes;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;

import org.jitsi.meet.sdk.JitsiMeet;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class GroupPostActivity extends AppCompatActivity implements View.OnClickListener {

    public static Post postToAppend;
    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.new_notifications)
    TextView new_notifications;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.ivUser)
    ImageView ivUser;

    @BindView(R.id.ivUserImage)
    ImageView ivUserImage;

    @BindView(R.id.back)
    LinearLayout back;

    @BindView(R.id.llProfile)
    LinearLayout llProfile;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.container)
    CoordinatorLayout container;

    @BindView(R.id.rvPostList)
    RecyclerView rvPostList;

    @BindView(R.id.rvCommentList)
    RecyclerView rvCommentList;

    @BindView(R.id.llLikePost)
    LinearLayout llLikePost;

    @BindView(R.id.ivLike)
    ImageView ivLike;

    @BindView(R.id.tvLike)
    TextView tvLike;

    @BindView(R.id.etComment)
    EditText etComment;

    @BindView(R.id.ivAddComment)
    ImageButton ivAddComment;

    @BindView(R.id.cvPost)
    CardView cvPost;


    private SlideUp slideUp;
    List<PostComments> group_post_comments = new ArrayList<>();
    LinearLayoutManager layoutManager;
    Post post = new Post();

    @BindView(R.id.dim)
    View dim;

    @BindView(R.id.content_slide_up_view)
    View sliderView;

    @BindView(R.id.swipelayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.llCommentsLoading)
    LinearLayout llCommentsLoading;

    Context context;
    public Group group;
    MenuItem leftGroup, deleteGroup, reportedPost, reportedUsers, reportedComments, addMember;
    AlertDialog confirmAlert;
    AlertDialog finishAlert;
    private User user;
    private BroadcastReceiver receiver;

    List<GroupMembers> members = new ArrayList<>();
    List<GroupMembers> member_requested = new ArrayList<>();
    List<GroupMembers> member_invited = new ArrayList<>();
    List<GroupMembers> member_blocked = new ArrayList<>();
    List<Post> posts = new ArrayList<>();
    public String USER_IMAGE_BASE_URL = "";
    int pagination = 0;
    int commentsPagination = 0;
    private LinearLayoutManager linearLayoutManager = null;
    PostListRecyclerViewAdapter postListRecyclerViewAdapter;
    CommentListRecyclerViewAdapter commentListRecyclerViewAdapter;
    boolean isLeft = false;

    com.codeholic.kotumb.app.View.ProgressBar progressBar = null;
    int type = 0;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_post);
        ButterKnife.bind(this);
        init();
        initToolbar();
        getGroupInfo();
        setClickListener();
        setupCommentView();
        setBroadCast();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            Post post = (Post) data.getExtras().getSerializable("post");
            if (postToAppend != null) {
                posts.add(0, postToAppend);
            }
            postListRecyclerViewAdapter.notifyDataSetChanged();
            rvPostList.smoothScrollToPosition(0);
        }
    }

    private void setMenuItem() {

        Log.e("TAG", "role " + group.role);
        if (type == 0) {
            if (group.role.equalsIgnoreCase("0")) {
                addMember.setVisible(true);
                deleteGroup.setVisible(true);
                reportedPost.setVisible(true);
                reportedUsers.setVisible(true);
                reportedComments.setVisible(true);
                if (group.admins_count > 1) {
                    leftGroup.setVisible(true);
                } else {
                    leftGroup.setVisible(false);
                }
            } else {
                addMember.setVisible(false);
//                reportedPost.setVisible(true);
//                reportedUsers.setVisible(true);
//                reportedComments.setVisible(true);
                leftGroup.setVisible(true);
            }
        }
    }


    private void getGroupInfo() {
        try {
            if (!Utility.isConnectingToInternet(context)) { // check net connection
                final Snackbar snackbar = Snackbar.make(llProfile, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                swipeRefreshLayout.setRefreshing(false);
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(VISIBLE);
                return;
            }
            /*progressBar = new com.codeholic.kotumb.app.View.ProgressBar(this);
            progressBar.show(getResources().getString(R.string.app_please_wait_text));*/
            API.sendRequestToServerGET(context, API.GROUP_INFO + "/" + group.groupId + "/" + SharedPreferencesMethod.getUserId(context), API.GROUP_INFO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setClickListener() {
        llProfile.setOnClickListener(this);
        cvPost.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_group, menu);
        leftGroup = menu.findItem(R.id.action_group_left);
        deleteGroup = menu.findItem(R.id.action_group_delete);
        reportedPost = menu.findItem(R.id.action_reported_post);
        reportedComments = menu.findItem(R.id.action_reported_comment);
        reportedUsers = menu.findItem(R.id.action_reported_user);
        addMember = menu.findItem(R.id.action_add_member);
        setMenuItem();//set up menu items visisble
        this.menu = menu;

        String report = getIntent().getStringExtra("report");
        user = getIntent().getParcelableExtra("USER");
        if (report != null) {
            switch (report) {
                case ResponseParameters.GROUP_USER_REPORT_NOTIFICATION:
                    onOptionsItemSelected(menu.findItem(R.id.action_reported_user));
                    break;
                case ResponseParameters.GROUP_POST_REPORT_NOTIFICATION:
                    onOptionsItemSelected(menu.findItem(R.id.action_reported_post));
                    break;
                case ResponseParameters.GROUP_POST_COMMENT_REPORT_NOTIFICATION:
                    onOptionsItemSelected(menu.findItem(R.id.action_reported_comment));
                    break;
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;
        //noinspection SimplifiableIfStatement

        switch (id) {
            case R.id.action_group_info:
                llProfile.performClick();
                return true;

            case R.id.action_group_left:
                String leftGroupURL = API.GROUP_DELETE_MEMBER + group.groupId + "/" + SharedPreferencesMethod.getUserId(context);
                showPopUp(getResources().getString(R.string.group_left_message), leftGroupURL, "FINISH");
                return true;

            case R.id.action_group_delete:
                String deleteGroupURL = API.DELETE_GROUP + group.groupId + "/" + SharedPreferencesMethod.getUserId(context);
                showPopUp(getResources().getString(R.string.group_delete_message), deleteGroupURL, API.DELETE_GROUP);
                return true;
            case R.id.action_reported_post:
                intent = new Intent(this, ReportedPostActivity.class);
                intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL));
                intent.putExtra(ResponseParameters.GROUP, group);
                intent.putExtra(ResponseParameters.TYPE, 3);
                intent.putExtra("USER",user);
                Toast.makeText(context, user.getFirstName(), Toast.LENGTH_SHORT).show();
                startActivity(intent);
                break;
            case R.id.action_reported_user:
                intent = new Intent(this, ReportedUserActivity.class);
                intent.putExtra(ResponseParameters.GROUP, group);
                intent.putExtra("USER",user);
                startActivity(intent);
                break;
            case R.id.action_reported_comment:
                intent = new Intent(this, ReportedCommentActivity.class);
                intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL));
                intent.putExtra(ResponseParameters.GROUP, group);
                intent.putExtra("USER",user);
                startActivity(intent);
                break;
            case R.id.action_add_member:
               /* intent = new Intent(this, ReportedCommentActivity.class);
                intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL));
                intent.putExtra(ResponseParameters.GROUP, group);
                startActivity(intent);*/

                intent = new Intent(this, GroupInviteActivity.class);
                intent.putExtra(ResponseParameters.GROUP, group);
                startActivity(intent);
                break;

        }


        return super.onOptionsItemSelected(item);
    }


    private void setUpRecyclerView() {

    }

    private void init() {
        context = this;
        group = (Group) getIntent().getSerializableExtra(ResponseParameters.GROUP);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getAllPost();
            }
        });
        if (getIntent().hasExtra(ResponseParameters.TYPE)) {
            if (getIntent().getIntExtra(ResponseParameters.TYPE, 0) == 1) { // from public group tab
                cvPost.setVisibility(GONE);
                type = 1;
            } else {
                cvPost.setVisibility(VISIBLE);
            }
        }


    }

    private void getAllPost() {
        try {
            if (!Utility.isConnectingToInternet(context)) { // check net connection
                swipeRefreshLayout.setRefreshing(false);
                final Snackbar snackbar = Snackbar.make(llProfile, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }

            if (posts.size() > 0) {
                getGroupInfo();
                //API.sendRequestToServerGET(context, API.GROUP_LATEST_POST + "/" + group.groupId + "/" + SharedPreferencesMethod.getUserId(context) + "/" + posts.get(0).post_id, API.GROUP_LATEST_POST);
            } else {
                llLoading.setVisibility(VISIBLE);
                llNotFound.setVisibility(GONE);
                getGroupInfo();
                //swipeRefreshLayout.setRefreshing(false);
            }
            /*progressBar = new com.codeholic.kotumb.app.View.ProgressBar(this);
            progressBar.show(getResources().getString(R.string.app_please_wait_text));*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            setMenuItem();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //clear notification of message

        if (posts.size() > 0) {
            llNotFound.setVisibility(GONE);
        }
    }

    // initToolbar method initializing view
    private void initToolbar() {
        User user = SharedPreferencesMethod.getUserInfo(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("");
        //setting screen title
        setImageText();
        String url = API.imageUrl + user.getAvatar();
        new AQuery(this).id(ivUserImage).image(url, true, true, 300, R.drawable.ic_user);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    void setImageText() {
        header.setText(group.groupName.trim());
        String url = getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL) + group.logo;
        new AQuery(this).id(ivUser).image(url, true, true, 300, R.drawable.ic_user);
    }


    //getResponse for getting list of urlImages
    public void getResponse(JSONObject outPut, int i) {
        Log.e("response", "" + outPut);
        try {

            if (progressBar != null) {
                progressBar.dismiss(); // dismissing progressbar
            }
            if (i == 0) { // for getting urlImages message
                llLoading.setVisibility(GONE);
                swipeRefreshLayout.setRefreshing(false);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        if (outPut.has(ResponseParameters.GROUP_INFO)) {
                            Gson gson = new Gson(); // creates a Gson instance
                            group = gson.fromJson(outPut.getString(ResponseParameters.GROUP_INFO), Group.class);
                        }
                        if (outPut.has(ResponseParameters.GROUP_MEMBERS)) {
                            Gson gson = new Gson(); // creates a Gson instance
                            Type listType = new TypeToken<List<GroupMembers>>() {
                            }.getType();

                            Log.e("members list", "" + outPut.getJSONArray(ResponseParameters.GROUP_MEMBERS).length());
                            members = gson.fromJson(outPut.getString(ResponseParameters.GROUP_MEMBERS), listType);
                        }

                        if (outPut.has(ResponseParameters.GROUP_MEMBERS_REQUESTED)) {
                            Gson gson = new Gson(); // creates a Gson instance
                            Type listType = new TypeToken<List<GroupMembers>>() {
                            }.getType();

                            member_requested = gson.fromJson(outPut.getString(ResponseParameters.GROUP_MEMBERS_REQUESTED), listType);
                        }

                        if (outPut.has(ResponseParameters.GROUP_MEMBERS_BLOCKED)) {
                            Gson gson = new Gson(); // creates a Gson instance
                            Type listType = new TypeToken<List<GroupMembers>>() {
                            }.getType();

                            member_blocked = gson.fromJson(outPut.getString(ResponseParameters.GROUP_MEMBERS_BLOCKED), listType);
                        }

                        if (outPut.has(ResponseParameters.GROUP_MEMBERS_INVITED)) {
                            Gson gson = new Gson(); // creates a Gson instance
                            Type listType = new TypeToken<List<GroupMembers>>() {
                            }.getType();

                            member_invited = gson.fromJson(outPut.getString(ResponseParameters.GROUP_MEMBERS_INVITED), listType);
                        }

                        if (outPut.has(ResponseParameters.IMAGE_BASE_URL)) {
                            USER_IMAGE_BASE_URL = outPut.getString(ResponseParameters.IMAGE_BASE_URL);
                        }

                        if (outPut.has(ResponseParameters.GROUP_POSTS)) {
                            Gson gson = new Gson(); // creates a Gson instance
                            Type listType = new TypeToken<List<Post>>() {
                            }.getType();
                            posts = gson.fromJson(outPut.getString(ResponseParameters.GROUP_POSTS), listType);

                           /* for(int j = 0; j <outPut.getJSONArray(ResponseParameters.GROUP_POSTS).length(); j++){
                                Log.e("TAG", ""+outPut.getJSONArray(ResponseParameters.GROUP_POSTS).getJSONObject(j).toString());
                            }*/





                            linearLayoutManager = new LinearLayoutManager(this);
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rvPostList.setLayoutManager(linearLayoutManager);
                            postListRecyclerViewAdapter = new PostListRecyclerViewAdapter(rvPostList, this, posts, group.groupId);
                            postListRecyclerViewAdapter.setUserImageBaseUrl(USER_IMAGE_BASE_URL);
                            postListRecyclerViewAdapter.setType(type);
                            postListRecyclerViewAdapter.role = group.role;
                            postListRecyclerViewAdapter.setGroupImageBaseUrl(getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL));
                            //rvPostList.setHasFixedSize(true);

                            setUpJitSiMeet();

                            rvPostList.setAdapter(postListRecyclerViewAdapter);

                            postListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                @Override
                                public void onLoadMore() {
                                    Log.e("load more", "load moe");
                                    if (posts.size() >= 5) {
                                        rvPostList.post(new Runnable() {
                                            public void run() {
                                                posts.add(null);
                                                postListRecyclerViewAdapter.notifyItemInserted(posts.size() - 1);
                                            }
                                        });
                                        API.sendRequestToServerGET(GroupPostActivity.this, API.GROUP_POSTS + group.groupId + "/" + (pagination + 5) + "/" + SharedPreferencesMethod.getUserId(GroupPostActivity.this), API.GROUP_POSTS);// service call for getting messages
                                    }
                                }
                            });


                            if (posts.size() <= 0) {
                                llNotFound.setVisibility(View.VISIBLE);
                            }
                        } else {
                            llNotFound.setVisibility(View.VISIBLE);
                        }

                    } else {
                        //no userfound
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    llNotFound.setVisibility(View.VISIBLE);
                }

                boolean openGroupInfo = getIntent().getBooleanExtra(ResponseParameters.GROUP_INFO, false);
                if (openGroupInfo && posts.size() == 0) {
                    openGroupInfo();
                }

                if (group.isMember.equalsIgnoreCase("1")) {
                    cvPost.setVisibility(VISIBLE);
                    type = 0;
                    setMenuItem();
                    postListRecyclerViewAdapter.setType(type);
                }

            } else if (i == 1) { // for send message
                Log.e("output", "" + outPut);
                posts.remove(posts.size() - 1); // removing of loading item
                postListRecyclerViewAdapter.notifyItemRemoved(posts.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        if (outPut.has(ResponseParameters.GROUP_POSTS)) {
                            Gson gson = new Gson(); // creates a Gson instance
                            Type listType = new TypeToken<List<Post>>() {
                            }.getType();
                            List<Post> updatedPosts = gson.fromJson(outPut.getString(ResponseParameters.GROUP_POSTS), listType);
                            posts.addAll(updatedPosts);

                            if (updatedPosts.size() > 0) {
                                pagination = pagination + 5;
                                //updating recycler view
                                postListRecyclerViewAdapter.notifyDataSetChanged();
                                postListRecyclerViewAdapter.setLoaded();
                            }

                           /* for(int j = 0; j <outPut.getJSONArray(ResponseParameters.GROUP_POSTS).length(); j++){
                                Log.e("TAG", ""+outPut.getJSONArray(ResponseParameters.GROUP_POSTS).getJSONObject(j).toString());
                            }*/


                        }
                    }
                }
            } else if (i == 2 || i == 5) { // for send message
                Log.e("output", "" + outPut);
                System.out.println("Comment Gets  "+outPut.toString());
                llCommentsLoading.setVisibility(GONE);
                if (i == 5) {
                    group_post_comments.remove(group_post_comments.size() - 1); // removing of loading item
                    commentListRecyclerViewAdapter.notifyItemRemoved(group_post_comments.size());
                }
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                    } else if (outPut.has(ResponseParameters.GROUP_POSTS_COMMENTS)) {
                        Gson gson = new Gson(); // creates a Gson instance
                        Type listType = new TypeToken<List<PostComments>>() {
                        }.getType();
                        List<PostComments> updatedComments = gson.fromJson(outPut.getString(ResponseParameters.GROUP_POSTS_COMMENTS), listType);
                        group_post_comments.addAll(updatedComments);
                        if (updatedComments.size() > 0) {
                            if (i == 5) // for updating
                                commentsPagination = commentsPagination + 10;
                            //updating recycler view
                            if (commentListRecyclerViewAdapter != null) {
                                commentListRecyclerViewAdapter.notifyItemRangeInserted(group_post_comments.size() - updatedComments.size(), updatedComments.size());
                                commentListRecyclerViewAdapter.notifyItemRangeChanged(0, group_post_comments.size());
                            }
                            //commentListRecyclerViewAdapter.notifyDataSetChanged();
                            commentListRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            } else if (i == 3) { // for send message
                Log.e("output", "" + outPut);
                swipeRefreshLayout.setRefreshing(false);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                    }
                    if (outPut.has(ResponseParameters.IMAGE_BASE_URL)) {
                        USER_IMAGE_BASE_URL = outPut.getString(ResponseParameters.IMAGE_BASE_URL);
                    }
                    if (outPut.has(ResponseParameters.GROUP_POSTS)) {
                        Gson gson = new Gson(); // creates a Gson instance
                        Type listType = new TypeToken<List<Post>>() {
                        }.getType();
                        List<Post> updatedPosts = gson.fromJson(outPut.getString(ResponseParameters.GROUP_POSTS), listType);
                        posts.addAll(0, updatedPosts);

                        if (linearLayoutManager == null) {
                            linearLayoutManager = new LinearLayoutManager(this);
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rvPostList.setLayoutManager(linearLayoutManager);
                            postListRecyclerViewAdapter = new PostListRecyclerViewAdapter(rvPostList, this, posts, group.groupId);
                            postListRecyclerViewAdapter.setUserImageBaseUrl(USER_IMAGE_BASE_URL);
                            postListRecyclerViewAdapter.setType(type);
                            postListRecyclerViewAdapter.role = group.role;
                            postListRecyclerViewAdapter.setGroupImageBaseUrl(getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL));
                            rvPostList.setHasFixedSize(true);
                            rvPostList.setAdapter(postListRecyclerViewAdapter);

                            postListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                @Override
                                public void onLoadMore() {
                                    Log.e("load more", "load moe");
                                    if (posts.size() >= 5) {
                                        rvPostList.post(new Runnable() {
                                            public void run() {
                                                posts.add(null);
                                                postListRecyclerViewAdapter.notifyItemInserted(posts.size() - 1);
                                            }
                                        });
                                        API.sendRequestToServerGET(GroupPostActivity.this, API.GROUP_POSTS + group.groupId + "/" + (pagination + 5) + "/" + SharedPreferencesMethod.getUserId(GroupPostActivity.this), API.GROUP_POSTS);// service call for getting messages
                                    }
                                }
                            });
                        } else {
                            if (updatedPosts.size() > 0) {
                                postListRecyclerViewAdapter.notifyItemRangeInserted(0, updatedPosts.size());
                                postListRecyclerViewAdapter.notifyItemRangeChanged(0, posts.size());
                                //postListRecyclerViewAdapter.notifyDataSetChanged();
                                rvPostList.scrollToPosition(0);
                            }
                        }
                    }
                }
                if (posts.size() <= 0) {
                    llNotFound.setVisibility(View.VISIBLE);
                } else {
                    llNotFound.setVisibility(View.GONE);
                }
            } else if (i == 4) {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
//                        Toast.makeText(this, getResources().getString(R.string.group_left_error_msg), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(GroupPostActivity.this, getResources().getString(R.string.group_left_error_msg));
                    } else if (outPut.has(ResponseParameters.Success)) {
                        Utils.showPopup(GroupPostActivity.this, getString(R.string.group_deleted_success_msg), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        });
                    }
                }
            }

        } catch (Exception e) {
            //mo user found
            e.printStackTrace();
        }
    }

    private void setUpJitSiMeet() {

        // Initialize default options for Jitsi Meet conferences.
        URL serverURL;
        try {
            serverURL = new URL("https://meet.jit.si");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("Invalid server URL!");
        }
        JitsiMeetConferenceOptions defaultOptions
                = new JitsiMeetConferenceOptions.Builder()
                .setServerURL(serverURL)
                .setWelcomePageEnabled(false)
                .build();
        JitsiMeet.setDefaultConferenceOptions(defaultOptions);
    }


    //broadcast for update user list and user disconnection
    private void setBroadCast() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //do something based on the intent's action
                String type = intent.getAction().trim();
                if (type.equalsIgnoreCase(ResponseParameters.GROUP_UPDATED)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.logo = updatedGroup.logo;
                        group.groupName = updatedGroup.groupName;
                        group.groupSummary = updatedGroup.groupSummary;
                        group.category = updatedGroup.category;
                        group.category_name = updatedGroup.category_name;
                        group.privacy = updatedGroup.privacy;
                        group.members_count = updatedGroup.members_count;
                        group.admins_count = updatedGroup.admins_count;
                        setImageText();
                    }
                } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_INVITED)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        List<GroupMembers> invited_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                        member_invited.addAll(invited_members);
                    }
                } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_REQUEST)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        List<GroupMembers> requested_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                        member_requested.addAll(requested_members);
                    }
                } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_DELETE)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        String status = intent.getStringExtra(ResponseParameters.STATUS);
                        String userId = intent.getStringExtra(ResponseParameters.UserId);

                        if (userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(GroupPostActivity.this))) {
                            if (!isLeft)
                                showPopUp(getResources().getString(R.string.removed_from_group_message));
                            return;
                        }
                        List<GroupMembers> selectedList = new ArrayList<>();
                        //1=pending,2=approved,3=blocked, 4 = invited
                        if (status.trim().equalsIgnoreCase("2")) {
                            selectedList = members;
                        } else if (status.trim().equalsIgnoreCase("3")) {
                            selectedList = member_blocked;
                        } else if (status.trim().equalsIgnoreCase("4")) {
                            selectedList = member_invited;
                        } else if (status.trim().equalsIgnoreCase("1")) {
                            selectedList = member_requested;
                        }
                        for (int i = 0; i < selectedList.size(); i++) {
                            if (selectedList.get(i).userId.trim().equalsIgnoreCase(userId.trim())) {
                                selectedList.remove(i);
                                break;
                            }
                        }
                    }
                } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_ROLE_UPDATE)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        List<GroupMembers> group_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                        for (int i = 0; i < members.size(); i++) {
                            if (group_members.get(0).userId.trim().equalsIgnoreCase(members.get(i).userId.trim())) {
                                if (group_members.get(0).userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(GroupPostActivity.this).trim())) {
                                    group.role = group_members.get(0).role;
                                }
                                members.remove(i);
                                members.add(i, group_members.get(0));
                                break;
                            }
                        }
                    }
                } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_ACCEPT)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        List<GroupMembers> group_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                        boolean removed = false;
                        for (int i = 0; i < member_requested.size(); i++) {
                            if (group_members.get(0).userId.trim().equalsIgnoreCase(member_requested.get(i).userId.trim())) {
                                member_requested.remove(i);
                                removed = true;
                                break;
                            }
                        }
                        if (!removed)
                            for (int i = 0; i < member_blocked.size(); i++) {
                                if (group_members.get(0).userId.trim().equalsIgnoreCase(member_blocked.get(i).userId.trim())) {
                                    member_blocked.remove(i);
                                    removed = true;
                                    break;
                                }
                            }
                        if (!removed)
                            for (int i = 0; i < member_invited.size(); i++) {
                                if (group_members.get(0).userId.trim().equalsIgnoreCase(member_invited.get(i).userId.trim())) {
                                    member_invited.remove(i);
                                    removed = true;
                                    break;
                                }
                            }
                        members.addAll(group_members);
                    }
                } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_BLOCK)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        List<GroupMembers> group_members = (ArrayList<GroupMembers>) intent.getSerializableExtra(ResponseParameters.GROUP_MEMBERS);
                        for (int i = 0; i < members.size(); i++) {
                            if (group_members.get(0).userId.trim().equalsIgnoreCase(members.get(i).userId.trim())) {
                                members.remove(i);
                                break;
                            }
                        }
                        member_blocked.addAll(group_members);

                        if (group_members.get(0).userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(GroupPostActivity.this))) {
                            showPopUp(getResources().getString(R.string.blocked_from_group_message));
                            return;
                        }

                    }
                } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_DELETED)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group.admins_count = updatedGroup.admins_count;
                        group.members_count = updatedGroup.members_count;
                        if (!isLeft)
                            showPopUp(getResources().getString(R.string.deleted_group_message));
                        return;

                    }
                } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_EDIT_POST)) {
                    Post post = (Post) intent.getSerializableExtra(ResponseParameters.POST);
                    for (int i = 0; i < posts.size(); i++) {
                        if (posts.get(i).post_id.trim().equalsIgnoreCase(post.post_id.trim())) {
                            posts.remove(i);
                            posts.add(i, post);
                            if (postListRecyclerViewAdapter != null)
                                postListRecyclerViewAdapter.notifyItemChanged(i);
                            break;
                        }
                    }
                } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_DELETE_POST)) {
                    Post post = (Post) intent.getSerializableExtra(ResponseParameters.POST);
                    for (int i = 0; i < posts.size(); i++) {
                        if (posts.get(i).post_id.trim().equalsIgnoreCase(post.post_id.trim())) {
                            posts.remove(i);
                            if (postListRecyclerViewAdapter != null) {
                                postListRecyclerViewAdapter.notifyItemRemoved(i);
                                postListRecyclerViewAdapter.notifyItemRangeChanged(0, posts.size());
                            }

                            if (slideUp.isVisible() && post.post_id.trim().equalsIgnoreCase(GroupPostActivity.this.post.post_id.trim())) {
                                slideUp.hide();
                            }

                            break;
                        }
                    }
                    if (posts.size() == 0) {
                        llNotFound.setVisibility(VISIBLE);
                    }
                } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_POST_LIKE)
                        || type.equalsIgnoreCase(ResponseParameters.GROUP_POST_UNLIKE)) {
                    Post post = (Post) intent.getSerializableExtra(ResponseParameters.POST);
                    PostLikes postLikes = (PostLikes) intent.getSerializableExtra(ResponseParameters.LIKEINFO);
                    //  if (!postLikes.userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(GroupPostActivity.this))) {
                    for (int i = 0; i < posts.size(); i++) {
                        if (posts.get(i).post_id.trim().equalsIgnoreCase(post.post_id.trim())) {
                            posts.remove(i);
                            posts.add(i, post);
                            if (postListRecyclerViewAdapter != null)
                                postListRecyclerViewAdapter.notifyItemChanged(i);
                            break;
                        }
                    }

                    if (GroupPostActivity.this.post.post_id.trim().equalsIgnoreCase(post.post_id.trim())) {
                        if (slideUp.isVisible()) {
                            if (post.total_likes == 0) {
                                tvLike.setText(getResources().getString(R.string.be_the_first_to_like_this_post_msg));
                            } else if (post.total_likes == 1) {
                                tvLike.setText(post.total_likes + " " + getResources().getString(R.string.like_btn_text));
                            } else {
                                tvLike.setText(post.total_likes + " " + getResources().getString(R.string.likes_btn_text));
                            }
                            if (post.self_like == 1) {
                                ivLike.setImageResource(R.drawable.ic_thumb_up_liked);
                            } else {
                                ivLike.setImageResource(R.drawable.ic_thumb_up);
                            }
                        }
                    }
                    //  }

                } /*else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_COMMENT_CREATED)) {
                    PostComments postComments = (PostComments) intent.getSerializableExtra(ResponseParameters.GROUP_POSTS_COMMENTS);
                    Post post = (Post) intent.getSerializableExtra(ResponseParameters.POST);
                    for (int i = 0; i < posts.size(); i++) {
                        if (postComments.post_id.trim().equalsIgnoreCase(posts.get(i).post_id.trim())) {
                            posts.remove(i);
                            posts.add(i, post);
                            if (postListRecyclerViewAdapter != null)
                                postListRecyclerViewAdapter.notifyItemChanged(i);
                            break;
                        }
                    }

                    if (GroupPostActivity.this.post.post_id.trim().equalsIgnoreCase(postComments.post_id.trim())) {
                        if (slideUp.isVisible()) {
                            group_post_comments.add(0, postComments);
                            if (commentListRecyclerViewAdapter != null) {
                                commentListRecyclerViewAdapter.notifyItemRangeInserted(0, 1);
                                commentListRecyclerViewAdapter.notifyItemRangeChanged(0, group_post_comments.size());
                            }
                        }
                    }
                } */ else if (type.equalsIgnoreCase(ResponseParameters.GROUP_COMMENT_CREATED) ||
                        type.equalsIgnoreCase(ResponseParameters.GROUP_COMMENT_DELETED)) {
                    PostComments postComments = (PostComments) intent.getSerializableExtra(ResponseParameters.GROUP_POSTS_COMMENTS);
                    Post post = (Post) intent.getSerializableExtra(ResponseParameters.POST);
                    for (int i = 0; i < posts.size(); i++) {
                        if (postComments.post_id.trim().equalsIgnoreCase(posts.get(i).post_id.trim())) {
                            posts.remove(i);
                            posts.add(i, post);
                            if (postListRecyclerViewAdapter != null)
                                postListRecyclerViewAdapter.notifyItemChanged(i);
                            break;
                        }
                    }

                    if (GroupPostActivity.this.post.post_id.trim().equalsIgnoreCase(postComments.post_id.trim())) {
                        if (slideUp.isVisible()) {
                            if (type.equalsIgnoreCase(ResponseParameters.GROUP_COMMENT_CREATED)) {
                                group_post_comments.add(0, postComments);
                                if (commentListRecyclerViewAdapter != null) {
                                    commentListRecyclerViewAdapter.notifyItemRangeInserted(0, 1);
                                    commentListRecyclerViewAdapter.notifyItemRangeChanged(0, group_post_comments.size());
                                }
                            } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_COMMENT_DELETED)) {
                                for (int i = 0; i < group_post_comments.size(); i++) {
                                    if (group_post_comments.get(i).comment_id.trim().equalsIgnoreCase(postComments.comment_id.trim())) {
                                        group_post_comments.remove(i);
                                        if (commentListRecyclerViewAdapter != null) {
                                            commentListRecyclerViewAdapter.notifyItemRemoved(i);
                                            commentListRecyclerViewAdapter.notifyItemRangeChanged(0, group_post_comments.size());
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_SHARED_POST) ||
                        type.equalsIgnoreCase(ResponseParameters.GROUP_POST_ADD)) {
                    if (intent.getStringExtra("groupid").equalsIgnoreCase(group.groupId)) {
                        new_notifications.setVisibility(View.VISIBLE);
                        new_notifications.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                rvPostList.smoothScrollToPosition(0);
                                pagination = 0;
//                                swipelayout.setRefreshing(false);
                                getAllPost();
                                Toast.makeText(GroupPostActivity.this, "Get Post", Toast.LENGTH_SHORT).show();
                                new_notifications.setVisibility(GONE);
                            }
                        });
                    }
                } else if (type.equalsIgnoreCase(ResponseParameters.GROUP_DELETED_ALL)) {
                    if (group.category.equalsIgnoreCase(intent.getStringExtra(ResponseParameters.CATEGORY))) {
                        showPopUp(getResources().getString(R.string.deleted_group_message));
                        return;
                    }
                }
                Log.e("test", "test");
                setMenuItem();//set up menu items visisble
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.GROUP_UPDATED);
        filter.addAction(ResponseParameters.GROUP_INVITED);
        filter.addAction(ResponseParameters.GROUP_MEMBER_DELETE);
        filter.addAction(ResponseParameters.GROUP_MEMBER_ACCEPT);
        filter.addAction(ResponseParameters.GROUP_MEMBER_ROLE_UPDATE);
        filter.addAction(ResponseParameters.GROUP_MEMBER_BLOCK);
        filter.addAction(ResponseParameters.GROUP_DELETED);
        filter.addAction(ResponseParameters.GROUP_EDIT_POST);
        filter.addAction(ResponseParameters.GROUP_DELETE_POST);
        filter.addAction(ResponseParameters.GROUP_POST_LIKE);
        filter.addAction(ResponseParameters.GROUP_POST_UNLIKE);
        filter.addAction(ResponseParameters.GROUP_COMMENT_CREATED);
        filter.addAction(ResponseParameters.GROUP_COMMENT_DELETED);
        filter.addAction(ResponseParameters.GROUP_REQUEST);
        filter.addAction(ResponseParameters.GROUP_DELETED_ALL);
        filter.addAction(ResponseParameters.GROUP_POST_ADD);
        filter.addAction(ResponseParameters.GROUP_SHARED_POST);
        registerReceiver(receiver, filter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.llProfile:
                if (USER_IMAGE_BASE_URL.trim().isEmpty())
                    return;

                openGroupInfo();


                /*llLayout.setVisibility(View.VISIBLE);
                final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);

                // Use bounce interpolator with amplitude 0.2 and frequency 20
                BounceInterpolator interpolator = new BounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);



                cvComment.startAnimation(myAnim);*/

                break;
            case R.id.cvPost:
                Intent createPostIntent = new Intent(this, CreatePostActivity.class);
                createPostIntent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL));
                createPostIntent.putExtra(ResponseParameters.GROUP, group);
                startActivityForResult(createPostIntent, 1);
                break;

        }
    }

    private void openGroupInfo() {
        Intent intent = new Intent(this, GroupInfoActivity.class);
        //intent.putExtra("USER", user);
        Bundle bundle = new Bundle();
        Log.e("members list", "" + members.size());
        ArrayList<GroupMembers> emptyMembers = new ArrayList<>();
        GroupInfoActivity.members = members;
        GroupInfoActivity.member_invited = member_invited;
        GroupInfoActivity.member_requested = member_requested;
        GroupInfoActivity.member_blocked = member_blocked;
        bundle.putSerializable(ResponseParameters.GROUP_MEMBERS, (Serializable) emptyMembers);
        bundle.putSerializable(ResponseParameters.GROUP_MEMBERS_INVITED, (Serializable) emptyMembers);
        bundle.putSerializable(ResponseParameters.GROUP_MEMBERS_BLOCKED, (Serializable) emptyMembers);
        bundle.putSerializable(ResponseParameters.GROUP_MEMBERS_REQUESTED, (Serializable) emptyMembers);
        bundle.putSerializable(ResponseParameters.GROUP, group);
        bundle.putInt(ResponseParameters.TYPE, getIntent().getIntExtra(ResponseParameters.TYPE, 0));
        bundle.putString(ResponseParameters.GROUP_IMG_BASE_URL, getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL));
        bundle.putString(ResponseParameters.IMAGE_BASE_URL, USER_IMAGE_BASE_URL);
        intent.putExtra(ResponseParameters.BUNDLE, bundle);
        startActivity(intent);
    }

    public void setupCommentView() {
        slideUp = new SlideUpBuilder(sliderView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                        dim.setAlpha(1 - (percent / 100));
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        if (visibility == GONE) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                Window window = getWindow();
                                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
                            }
                        } else if (visibility == VISIBLE) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                Window window = getWindow();
                                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                window.setStatusBarColor(getResources().getColor(R.color.dimBg));
                            }
                        }
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                //.withInterpolator(new BounceInterpolator(0.3, 35))
                .withStartState(SlideUp.State.HIDDEN)
                .withTouchableAreaDp(1000)
                .withAutoSlideDuration(500)
                .build();
    }

    public void showPopUp(String title, final String url, final String apiUrl) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);
        final TextView tvCancel = dialogView.findViewById(R.id.tvCancel);


        tvTitle.setText(title);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utility.isConnectingToInternet(context)) { // check net connection
//                    Toast.makeText(context, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(GroupPostActivity.this, getResources().getString(R.string.app_no_internet_error));
                    confirmAlert.dismiss();
                    return;
                }
                progressBar = new com.codeholic.kotumb.app.View.ProgressBar(context);
                progressBar.show(getResources().getString(R.string.app_please_wait_text));
                isLeft = true;
                API.sendRequestToServerGET(context, url, apiUrl);

                confirmAlert.dismiss();
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                confirmAlert.dismiss();
            }
        });

        confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();

    }


    public void showPopUp(String title) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);
        final TextView tvCancel = dialogView.findViewById(R.id.tvCancel);


        tvTitle.setText(title);

        llYes.setVisibility(GONE);


        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAlert.dismiss();
                finish();
            }
        });

        finishAlert = dialogBuilder.create();
        finishAlert.setCancelable(false);
        finishAlert.show();

    }

    public void showCommentView(final Post post, final int position) {
        this.post = post;
        slideUp.show();

        commentsPagination = 0;
        /*this.group_post_comments = new ArrayList<>(post.group_post_comments);*/
        llCommentsLoading.setVisibility(VISIBLE);
        this.group_post_comments = new ArrayList<>();

        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        rvCommentList.setLayoutManager(layoutManager);
        commentListRecyclerViewAdapter = new CommentListRecyclerViewAdapter(rvCommentList, this, this.group_post_comments);
        commentListRecyclerViewAdapter.setUserImageBaseUrl(USER_IMAGE_BASE_URL);
        commentListRecyclerViewAdapter.setGroupImageBaseUrl(getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL));
        rvCommentList.setHasFixedSize(true);
        rvCommentList.setAdapter(commentListRecyclerViewAdapter);
        rvCommentList.smoothScrollToPosition(0);
        API.sendRequestToServerGET(GroupPostActivity.this, API.GROUP_POST_COMMENTS + posts.get(position).post_id + "/" + (commentsPagination) + "/" + SharedPreferencesMethod.getUserId(GroupPostActivity.this), API.GROUP_POST_COMMENTS);// service call for getting messages


        if (post.self_like == 1) {
            ivLike.setImageResource(R.drawable.ic_thumb_up_liked);
        } else {
            ivLike.setImageResource(R.drawable.ic_thumb_up);
        }


        if (post.total_likes == 0) {
            tvLike.setText(getResources().getString(R.string.be_the_first_to_like_this_post_msg));
        } else if (post.total_likes == 1) {
            tvLike.setText(post.total_likes + " " + getResources().getString(R.string.like_btn_text));
        } else {
            tvLike.setText(post.total_likes + " " + getResources().getString(R.string.likes_btn_text));
        }


        tvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (posts.get(position).total_likes > 0) {
                    Intent PostLikesIntent = new Intent(context, PostLikesActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ResponseParameters.POST, posts.get(position));
                    bundle.putString(ResponseParameters.IMAGE_BASE_URL, USER_IMAGE_BASE_URL);
                    PostLikesIntent.putExtra(ResponseParameters.BUNDLE, bundle);
                    startActivity(PostLikesIntent);
                }
            }
        });

        ivAddComment.setEnabled(false);

        etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().trim().isEmpty()) {
                    ivAddComment.setEnabled(true);
                } else {
                    ivAddComment.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ivAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etComment.getText().toString().trim().isEmpty()) {
                    if (!Utility.isConnectingToInternet(context)) { // check net connection
//                        Toast.makeText(context, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(GroupPostActivity.this, getResources().getString(R.string.app_no_internet_error));
                        return;
                    }

                    /*PostComments postComments = new PostComments();
                    postComments.comment = etComment.getText().toString().trim();
                    postComments.firstName = SharedPreferencesMethod.getString(context, ResponseParameters.FirstName);
                    postComments.lastName = SharedPreferencesMethod.getString(context, ResponseParameters.LastName);
                    postComments.avatar = SharedPreferencesMethod.getString(context, ResponseParameters.Avatar);
                    posts.get(position).group_post_comments.add(postComments);
                    postListRecyclerViewAdapter.notifyItemChanged(position);
                    GroupPostActivity.this.group_post_comments.add(0, postComments);
                    commentListRecyclerViewAdapter.notifyItemInserted(0);*/
                    HashMap<String, Object> input = new HashMap();
                    input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(context));
                    input.put(RequestParameters.GROUPID, "" + group.groupId);
                    input.put(RequestParameters.POSTID, "" + posts.get(position).post_id);
                    input.put(RequestParameters.COMMENT, "" + Utils.encode(etComment.getText().toString().trim()));
                    Log.e("input", "" + input);
                    System.out.println("Input Comments "+input);
                    API.sendRequestToServerPOST_PARAM(context, API.GROUP_ADD_COMMENT, input);
                    Utils.logEventGroupCommented(GroupPostActivity.this, group.groupId);
                    etComment.setText("");
                }
            }
        });

        llLikePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (posts.get(position).self_like == 0) {
                    if (!Utility.isConnectingToInternet(context)) { // check net connection
//                        Toast.makeText(context, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(GroupPostActivity.this, getResources().getString(R.string.app_no_internet_error));
                        return;
                    }


                    ivLike.setImageResource(R.drawable.ic_thumb_up_liked);

                    final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);

                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    BounceInterpolator interpolator = new BounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);
                    ivLike.startAnimation(myAnim);
                    posts.get(position).self_like = 1;
                    posts.get(position).total_likes = posts.get(position).total_likes + posts.get(position).self_like;
                    if (posts.get(position).total_likes == 0) {
                        tvLike.setText(getResources().getString(R.string.be_the_first_to_like_this_post_msg));
                    } else if (posts.get(position).total_likes == 1) {
                        tvLike.setText(posts.get(position).total_likes + " " + getResources().getString(R.string.like_btn_text));
                    } else {
                        tvLike.setText(posts.get(position).total_likes + " " + getResources().getString(R.string.likes_btn_text));
                    }


                    String url = API.GROUP_LIKE_POST + post.post_id + "/" + post.group_id + "/" + SharedPreferencesMethod.getUserId(context);
                    API.sendRequestToServerGET(context, url, API.GROUP_LIKE_POST);
                    Utils.logEventGroupLike(GroupPostActivity.this, group.groupId);
                } else {
                    if (!Utility.isConnectingToInternet(context)) { // check net connection
//                        Toast.makeText(context, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(GroupPostActivity.this, getResources().getString(R.string.app_no_internet_error));
                        return;
                    }


                    ivLike.setImageResource(R.drawable.ic_thumb_up);

                    final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);

                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    BounceInterpolator interpolator = new BounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);
                    ivLike.startAnimation(myAnim);
                    myAnim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    posts.get(position).self_like = 0;
                    posts.get(position).total_likes = posts.get(position).total_likes - 1;
                    if (posts.get(position).total_likes == 0) {
                        tvLike.setText(getResources().getString(R.string.be_the_first_to_like_this_post_msg));
                    } else if (posts.get(position).total_likes == 1) {
                        tvLike.setText(posts.get(position).total_likes + " " + getResources().getString(R.string.like_btn_text));
                    } else {
                        tvLike.setText(posts.get(position).total_likes + " " + getResources().getString(R.string.likes_btn_text));
                    }

                    String url = API.GROUP_UNLIKE_POST + post.post_id + "/" + post.group_id + "/" + SharedPreferencesMethod.getUserId(context);
                    API.sendRequestToServerGET(context, url, API.GROUP_UNLIKE_POST);
                }
                postListRecyclerViewAdapter.notifyItemChanged(position);
            }
        });


        commentListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("load more", "load moe");
                if (GroupPostActivity.this.group_post_comments.size() >= 10) {
                    rvCommentList.post(new Runnable() {
                        public void run() {
                            GroupPostActivity.this.group_post_comments.add(null);
                            Log.e("load more", "load more " + GroupPostActivity.this.group_post_comments.size());
                            commentListRecyclerViewAdapter.notifyItemInserted(GroupPostActivity.this.group_post_comments.size() - 1);
                        }
                    });

                    Log.e("URL", "" + API.GROUP_POST_COMMENTS + posts.get(position).post_id + "/" + (commentsPagination + 10) + "/" + SharedPreferencesMethod.getUserId(GroupPostActivity.this));
                    API.sendRequestToServerGET(GroupPostActivity.this, API.GROUP_POST_COMMENTS + posts.get(position).post_id + "/" + (commentsPagination + 10) + "/" + SharedPreferencesMethod.getUserId(GroupPostActivity.this), API.GROUP_POST_COMMENTS_UPDATE);// service call for getting messages
                }
            }
        });
    }

    @Override
    protected void onDestroy() {

        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (slideUp.isVisible()) {
            slideUp.hide();
            post = new Post();
            //slideUp.hideImmediately();
        } else {
            super.onBackPressed();
        }
    }
}
