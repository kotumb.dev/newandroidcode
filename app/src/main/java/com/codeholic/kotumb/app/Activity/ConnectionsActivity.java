package com.codeholic.kotumb.app.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Adapter.UserRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ConnectionsActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.tvNouser)
    TextView tvNouser;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.adView)
    ImageView adView;
    @BindView(R.id.rlAdView)
    RelativeLayout rlAdView;

    List<User> users;
    UserRecyclerViewAdapter userRecyclerViewAdapter;
    int pagination = 0;
    private LinearLayoutManager linearLayoutManager;
    User user;

    // onCreate method initializing view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connections);
        ButterKnife.bind(this);
        init();
        initToolbar();
        setView();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.UPDATE_USER);
        filter.addAction(ResponseParameters.REQUEST_SEND);
        registerReceiver(receiver, filter);
        IntentFilter filterAdd = new IntentFilter();
        filterAdd.addAction(ResponseParameters.ADD_RECEIVED);
        registerReceiver(getAd, filterAdd);
    }

    private BroadcastReceiver getAd = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            try {
//                HomeScreenActivity.setUpAdImage(intent.getStringExtra(ResponseParameters.ADD_RECEIVED), rlAdView, adView, ConnectionsActivity.this, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void setUpAdImage(String adResponse) {
        hideAdView();
        try {
            if (!adResponse.isEmpty()) {
                final JSONObject ad = new JSONObject(adResponse);
                if (ad.has(ResponseParameters.AD)) {
                    if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.IMAGE)) {
                        if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE).isEmpty()) {
                            rlAdView.setVisibility(VISIBLE);
                            new AQuery(this).id(adView).image(API.imageAdUrl + ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE), true, true, 300, R.drawable.default_ad);
                            adView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.URL)) {
                                            if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL).isEmpty()) {
                                                Utility.prepareCustomTab(ConnectionsActivity.this, ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL));
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideAdView() {
        rlAdView.setVisibility(GONE);
    }

    //init data of user
    private void init() {
        if (getIntent().hasExtra("USER")) {
            user = getIntent().getParcelableExtra("USER");
        }
    }

    //init toolbar function
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        if (getIntent().hasExtra("SUGGESTION")) {
            header.setText(getResources().getString(R.string.people_you_may_know_text));
        } else {
            header.setText(getResources().getString(R.string.connections_title));
        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //setview for getting user connection
    private void setView() {
        try {
            if (!Utility.isConnectingToInternet(this)) { // checking net connection
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(View.VISIBLE);
                tvNouser.setText(getResources().getString(R.string.app_no_internet_error));
                final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            if (getIntent().hasExtra("SUGGESTION")) {
                tvNouser.setText(getResources().getString(R.string.no_suggestions_found_error));
                API.sendRequestToServerGET(this, API.SUGGESTION_USERS + "0" + "/?userid=" + SharedPreferencesMethod.getUserId(this), API.SUGGESTION_USERS); // service call for user list
            } else {
                API.sendRequestToServerGET(this, API.CONNECTECTIONS + user.getUserId() + "/" + SharedPreferencesMethod.getUserId(this) + "/?offset=" + (pagination), API.CONNECTECTIONS);// service call for getting connection
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //getResponse for getting list of urlImages
    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        Log.e("response", "" + outPut);
        try {
            if (i == 0) { // for getting urlImages
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            userList = outPut.getJSONArray(ResponseParameters.CONNECTIONS);
                        }
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                        }
                        users = new ArrayList<>();
                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));    user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                            user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                            user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                            user.setCity(searchResults.getString(ResponseParameters.City));
                            user.setState(searchResults.getString(ResponseParameters.State));
                            user.setUserInfo(searchResults.toString());
                            users.add(user);
                        }
                        //setting options for recycler adapter
                        linearLayoutManager = new LinearLayoutManager(this);
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserList.setLayoutManager(linearLayoutManager);
                        userRecyclerViewAdapter = new UserRecyclerViewAdapter(rvUserList, this, users);
                        rvUserList.setAdapter(userRecyclerViewAdapter);
                        //load more setup for recycler adapter
                        userRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                if (users.size() >= 6) {
                                    rvUserList.post(new Runnable() {
                                        public void run() {
                                            users.add(null);
                                            userRecyclerViewAdapter.notifyItemInserted(users.size() - 1);
                                        }
                                    });
                                    if (!getIntent().hasExtra("SUGGESTION")) {
                                        API.sendRequestToServerGET(ConnectionsActivity.this, API.CONNECTECTIONS + user.getUserId() + "/" + SharedPreferencesMethod.getUserId(ConnectionsActivity.this) + "/?offset=" + (pagination + 6), API.CONNECTECTIONS_UPDATE);
                                    } else {
                                        API.sendRequestToServerGET(ConnectionsActivity.this, API.SUGGESTION_USERS + (pagination + 6) + "/?userid=" + SharedPreferencesMethod.getUserId(ConnectionsActivity.this), API.SUGGESTION_USERS_UPDATE); // service call for user list
                                    }
                                }
                            }
                        });
                    } else {
                        //no userfound
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } else if (i == 1) {// for updating list
                users.remove(users.size() - 1); // removing of loading item
                userRecyclerViewAdapter.notifyItemRemoved(users.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response

                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            userList = outPut.getJSONArray(ResponseParameters.CONNECTIONS);
                        }
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                        }

                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                            user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                            user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                            user.setCity(searchResults.getString(ResponseParameters.City));
                            user.setState(searchResults.getString(ResponseParameters.State));
                            user.setUserInfo(searchResults.toString());
                            users.add(user);
                        }
                        if (userList.length() > 0) {
                            pagination = pagination + 6;
                            //updating recycler view
                            userRecyclerViewAdapter.notifyDataSetChanged();
                            userRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            }

        } catch (Exception e) {
            //mo user found
            llNotFound.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            if (!intent.hasExtra("UPDATE_USER")) { // for not updating from its own adapter
                User updatedUser = intent.getParcelableExtra("USER");
                Log.e("test", "test " + updatedUser.getUserInfo());
                for (int i = 0; i < users.size(); i++) {
                    User user = users.get(i);
                    if (updatedUser.getUserId().equals(user.getUserId())) {
                        try {
                            users.remove(i);
                            users.add(i, updatedUser);
                            userRecyclerViewAdapter.notifyItemChanged(i);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        if (getAd != null) {
            unregisterReceiver(getAd);
            getAd = null;
        }
        super.onDestroy();
    }
}
