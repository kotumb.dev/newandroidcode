package com.codeholic.kotumb.app.Activity;

import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.codeholic.kotumb.app.Adapter.VideoLikeListAdapter;
import com.codeholic.kotumb.app.Model.VideoLikes;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.View.DividerItemDecoration;
import com.codeholic.kotumb.app.View.ProgressBar;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoLikesListActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.tvNouser)
    TextView tvNouser;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.fabAddConnections)
    FloatingActionButton fabAddConnections;

    @BindView(R.id.adView)
    ImageView adView;
    @BindView(R.id.rlAdView)
    RelativeLayout rlAdView;
    int pagination = 0;
    private LinearLayoutManager linearLayoutManager;
    VideoLikeListAdapter videoLikeListAdapter;
    List<VideoLikes> videoLikesList=new ArrayList<VideoLikes>();
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connections_group_invite);
        ButterKnife.bind(this);
        initToolbar();
        Intent intent = getIntent();
        String video_id=intent.getStringExtra("videoId");
        String url=API.VIDEO_LIKES_LIST+video_id;
        System.out.println("Video Like List Url  "+url);
        API.sendRequestToServerGET(VideoLikesListActivity.this, url, API.VIDEO_LIKES_LIST);
    }



    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.likes_btn_text));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }





    public void getResponse(JSONObject outPut, int i) {
        if (i==1){
            llLoading.setVisibility(View.GONE);
            System.out.println("Like List Response   "+outPut.toString());
                try{
                    for (int j=0; j<outPut.getJSONArray("likes").length(); j++){
                        JSONObject likeObj = outPut.getJSONArray("likes").getJSONObject(j);
                        VideoLikes videoLikes=new VideoLikes();
                        videoLikes.setLike_date(likeObj.getString(ResponseParameters.LIKED_AT));
                        videoLikes.setLike_id(likeObj.getString(ResponseParameters.LIKE_ID));
                        videoLikes.setVideo_avatar(likeObj.getString(ResponseParameters.Avatar));
                        videoLikes.setVideo_first_name(likeObj.getString(ResponseParameters.FirstName));
                        videoLikes.setVideo_last_name(likeObj.getString(ResponseParameters.LastName));
                        videoLikesList.add(videoLikes);
                    }
                    linearLayoutManager = new LinearLayoutManager(this);
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    rvUserList.setLayoutManager(linearLayoutManager);
                    videoLikeListAdapter = new VideoLikeListAdapter(rvUserList, this, videoLikesList);
//                    videoLikeListAdapter.setImageBaseUrl(bundle.getString(ResponseParameters.IMAGE_BASE_URL));
                    rvUserList.addItemDecoration(
                            new DividerItemDecoration(this, R.drawable.divider));
                    rvUserList.setAdapter(videoLikeListAdapter);
                }catch (Exception ex){
                    ex.printStackTrace();
                }



        }
    }
}
