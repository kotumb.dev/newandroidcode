package com.codeholic.kotumb.app.Utility;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.browser.customtabs.CustomTabsClient;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.browser.customtabs.CustomTabsServiceConnection;
import androidx.browser.customtabs.CustomTabsSession;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioButton;
import android.widget.TextView;

import com.codeholic.kotumb.app.Activity.OtherUserProfileActivity;
import com.codeholic.kotumb.app.Activity.SplashActivity;
import com.codeholic.kotumb.app.Animation.BounceInterpolator;
import com.codeholic.kotumb.app.Model.ConnectionObject;
import com.codeholic.kotumb.app.Model.DisplayUserObject;
import com.codeholic.kotumb.app.Model.GroupMembers;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.Model.PostComments;
import com.codeholic.kotumb.app.Model.RewardObject;
import com.codeholic.kotumb.app.Model.SkillDevLikeUserObject;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static android.content.Context.CONNECTIVITY_SERVICE;
import static android.content.Context.INPUT_METHOD_SERVICE;

public class Utility {
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", 2);

    private static CustomTabsServiceConnection mCustomTabsServiceConnection;
    private static CustomTabsClient mClient;
    private static CustomTabsSession mCustomTabsSession;
    private static CustomTabsIntent.Builder builder;
    private static boolean warmedUp = false;
    private static ProgressDialog progressDialog;


    public static boolean isEmailIdValid(String emailStr) {
        return VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr).find();
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (NetworkInfo state : info) {
                    if (state.getState() == State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void hideKeyboardOnStatrtActivity(Context context) {
        ((Activity) context).getWindow().setSoftInputMode(3);
    }

    public static void hideKeyboard(Context context) {
        View view = ((Activity) context).getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void hideStatusBar(Context context) {
        ((Activity) context).requestWindowFeature(1);
        ((Activity) context).getWindow().setFlags(1024, 1024);
    }

    public static boolean appInstalledOrNot(Context context, String uri) {
        try {
            context.getPackageManager().getPackageInfo(uri, 1);
            return true;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void prepareCustomTab(Context activity, final String url) {

        builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        // builder.setSecondaryToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
        builder.setShowTitle(true);
        builder.addDefaultShareMenuItem();

        //Bitmap icon = getBitmapFromDrawable(activity, R.drawable.ic_arrow_back);
        //PendingIntent pendingIntent = createPendingShareIntent();

        final Bitmap backButton = getBitmapFromDrawable(activity, R.drawable.ic_arrow_back);
        builder.setCloseButtonIcon(backButton);

        builder.setStartAnimations(activity, R.anim.slide_in_right, R.anim.slide_out_left);
        builder.setExitAnimations(activity, R.anim.slide_in_left, R.anim.slide_out_right);
        //builder.setActionButton(icon, activity.getString(R.string.app_name), pendingIntent, true);
        // builder.addMenuItem("Share this page", createPendingShareIntent());
        mCustomTabsServiceConnection = new CustomTabsServiceConnection() {
            @Override
            public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
                //Pre-warming
                mClient = customTabsClient;
                mClient.warmup(0L);
                mCustomTabsSession = mClient.newSession(null);
                mCustomTabsSession.mayLaunchUrl(Uri.parse(url), null, null);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mClient = null;
            }
        };
        warmedUp = CustomTabsClient.bindCustomTabsService(activity, "com.android.chrome", mCustomTabsServiceConnection);
        if (url.isEmpty())
            return;


        CustomTabsIntent intentCustomTabs = builder.build();
        intentCustomTabs.intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        String packageName = CustomTabsHelper.getPackageNameToUse(activity);
        if (packageName != null) {
            intentCustomTabs.intent.setPackage(packageName);
        }

        intentCustomTabs.launchUrl(activity, Uri.parse(url));
    }

    public static Animation showAnimtion(Context context) {
        final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        BounceInterpolator interpolator = new BounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);
        return myAnim;
    }

    public static void ChangeLanguagePopup(final Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(context.getResources().getString(R.string.select_language));
        View dialogView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.change_langauge_popup, null);
        dialogBuilder.setView(dialogView);

        final RadioButton rbEnglish = dialogView.findViewById(R.id.rbEnglish);

        final RadioButton rbHindi = dialogView.findViewById(R.id.rbHindi);
        final RadioButton rbKannada = dialogView.findViewById(R.id.rbKannada);

        if (SharedPreferencesMethod.getDefaultLanguage(context).equalsIgnoreCase("kn")) {
            rbHindi.setChecked(true);
            rbEnglish.setChecked(false);
        } else {
            rbHindi.setChecked(false);
            rbEnglish.setChecked(true);
        }

        if (SharedPreferencesMethod.getDefaultLanguage(context).equalsIgnoreCase("kn")) {
            rbKannada.setChecked(true);
            rbEnglish.setChecked(false);
            rbHindi.setChecked(false);
        } else {
            rbKannada.setChecked(false);
            rbEnglish.setChecked(true);
            rbHindi.setChecked(true);
        }

        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(context.getResources().getString(R.string.update_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String oldLAng = SharedPreferencesMethod.getDefaultLanguage(context);

                if (rbEnglish.isChecked()) {
                    if (!oldLAng.trim().equalsIgnoreCase("en")) {
                        SharedPreferencesMethod.clearSerViceCall(context);
                        SharedPreferencesMethod.setUpdateData(context);
                    }
                    SharedPreferencesMethod.setDefaultLanguage(context, "en");
//                    SharedPreferencesMethod.setString(context, SharedPreferencesMethod.DEFAULT_LANGUAGE, "en");
                } else if (rbHindi.isChecked()) {
                    if (!oldLAng.trim().equalsIgnoreCase("hi")) {
                        SharedPreferencesMethod.clearSerViceCall(context);
                        SharedPreferencesMethod.setUpdateData(context);
                    }
                    SharedPreferencesMethod.setDefaultLanguage(context, "hi");
//                    SharedPreferencesMethod.setString(context, SharedPreferencesMethod.DEFAULT_LANGUAGE, "hi");
                }
                context.startActivity(new Intent(context, SplashActivity.class));
                ((Activity) context).finish();
                ((Activity) context).overridePendingTransition(0, 0);
            }
        });

        dialogBuilder.setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog changeLanguageAlertDialog = dialogBuilder.create();


        //changeLanguageAlertDialog.setCancelable(false);
        changeLanguageAlertDialog.show();

    }

   /* public static List<byte[]> convertURIToByteArray(ArrayList<Uri> uriList, Context context) {
        List<byte[]> imagesEncodedList = new ArrayList<>();
        try {
            for (int i = 0; i < uriList.size(); i++) {
                Uri uri = uriList.get(i);
                Bitmap bitmap1 = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                bitmap1 = ConvertBitmap.Mytransform(bitmap1);

                File f1 = new File(context.getExternalCacheDir() + "/temp_one.jpg");
                if (f1.exists())
                    f1.delete();
                f1.createNewFile();

                bitmap1 = RotateImageCode.rotateImage(bitmap1, f1);
                ByteArrayOutputStream datasecond = new ByteArrayOutputStream();
                bitmap1.compress(Bitmap.CompressFormat.PNG, 100, datasecond);

                byte[] bitmapdata = datasecond.toByteArray();
                Log.e("Log", String.valueOf(bitmapdata));
                File f = new File(context.getExternalCacheDir() + File.separator + "temp_two.png");
                if (f.exists())
                    f.delete();
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bitmapdata);
                fo.close();
                byte[] picByteArray = bitmapdata;
                imagesEncodedList.add(picByteArray);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imagesEncodedList;
    }*/

    public static List<byte[]> convertURIToByteArray(ArrayList<Uri> uriList, Context context) {
        List<byte[]> imagesEncodedList = new ArrayList<>();
        try {
            for (int i = 0; i < uriList.size(); i++) {
                Uri uri = uriList.get(i);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                bitmap = ConvertBitmap.Mytransform(bitmap);

                File f1 = new File(context.getExternalCacheDir() + "/temp_one.png");
                if (f1.exists())
                    f1.delete();
                f1.createNewFile();

                bitmap = RotateImageCode.rotateImage(bitmap, f1);
                ByteArrayOutputStream datasecond = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, datasecond);

                byte[] bitmapdata = datasecond.toByteArray();
                Log.e("Log", String.valueOf(bitmapdata));
                File f = new File(context.getExternalCacheDir() + File.separator + "temp_two.png");
                if (f.exists())
                    f.delete();
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bitmapdata);
                fo.close();
                byte[] picByteArray = bitmapdata;
                imagesEncodedList.add(picByteArray);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imagesEncodedList;
    }

    public static void ComingSoonPopup(final Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(context.getResources().getString(R.string.app_alert_text));

        //dialogBuilder.setMessage("Coming Soon...");

        View dialogView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.coming, null);
        dialogBuilder.setView(dialogView);

        dialogBuilder.setCancelable(true);
       /* dialogBuilder.setPositiveButton(context.getResources().getString(R.string.update_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });*/

        dialogBuilder.setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog changeLanguageAlertDialog = dialogBuilder.create();


        //changeLanguageAlertDialog.setCancelable(false);
        changeLanguageAlertDialog.show();

    }


    public static Bitmap getBitmapFromDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof VectorDrawable) {
            return getBitmapFromVectorDrawable((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("Unable to convert to bitmap");
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static Bitmap getBitmapFromVectorDrawable(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    public static boolean isAppRunning(final Context context) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null) {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(context.getPackageName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isFirstTime(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        String first = preferences.getString("first", null);
        if (first == null) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("first", "yes").commit();
            return true;
        }
        return false;
    }

    public static ArrayList<DisplayUserObject> getDisplayUserObjects(JSONArray outPutJSONArray) {
        ArrayList<DisplayUserObject> arrayList = new ArrayList<>();
        for (int i = 0; i < outPutJSONArray.length(); i++) {
            try {
                DisplayUserObject displayUserObject =
                        new Gson().fromJson(outPutJSONArray.getJSONObject(i) + "",
                                DisplayUserObject.class);
                arrayList.add(displayUserObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }

    public static void showLoading(Activity activity, String msg) {
        progressDialog = new ProgressDialog(activity);
        if (msg == null) {
            progressDialog.setMessage("Loading...");
        } else {
            progressDialog.setMessage(msg);
        }
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public static void hideLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public static ArrayList<PostComments> getPostCommentsObjects(JSONArray array) {
        ArrayList<PostComments> arrayList = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                PostComments postComments =
                        new Gson().fromJson(array.getJSONObject(i) + "", PostComments.class);
                arrayList.add(postComments);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }

    public static ArrayList<SkillDevLikeUserObject> getLikedUserObjects(JSONArray array) {
        ArrayList<SkillDevLikeUserObject> arrayList = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                SkillDevLikeUserObject userObject =
                        new Gson().fromJson(array.getJSONObject(i) + "", SkillDevLikeUserObject.class);
                arrayList.add(userObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }

    public static ArrayList<ConnectionObject> getConnectionsObjects(JSONArray array) {
        ArrayList<ConnectionObject> arrayList = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                ConnectionObject userObject =
                        new Gson().fromJson(array.getJSONObject(i) + "", ConnectionObject.class);
                arrayList.add(userObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }

    public static ArrayList<RewardObject> getRewardObjects(JSONArray array) {
        ArrayList<RewardObject> arrayList = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                RewardObject userObject =
                        new Gson().fromJson(array.getJSONObject(i) + "", RewardObject.class);
                arrayList.add(userObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }

    public static ArrayList<GroupMembers> getGroupMemberObjects(JSONArray array) {
        ArrayList<GroupMembers> arrayList = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                GroupMembers userObject = new Gson().fromJson(array.getJSONObject(i) + "", GroupMembers.class);
                arrayList.add(userObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }

    @NonNull
    public static View.OnClickListener getListenerUserClick(final Activity activity, final String userId) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUserProfile(activity, userId);
            }
        };
    }

    public static void openUserProfile(Activity activity, String userId) {
        Intent intent = new Intent(activity, OtherUserProfileActivity.class);
        User user = new User();
        user.setUserId(userId);
        intent.putExtra("USER", user);
        //OtherUserProfileActivity.setHolder(holder);
        activity.startActivity(intent);
    }

    @NonNull
    public static View.OnTouchListener getTouchListenerForUser(final Post post, final SpannableString spannableName, final Activity activity, final TextView tvUserName) {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    int offsetForPosition = tvUserName.getOffsetForPosition(event.getX(), event.getY());
                    if (offsetForPosition < spannableName.length()) {
                        Utility.openUserProfile(activity, post.userId);
                    } else {
                        Utility.openUserProfile(activity, post.sharedPostUserId);
                    }
                }
                return true;
            }
        };
    }

    public static String findWordForRightHanded(String str, int offset) { // when you touch ' ', this method returns left word.
        if (str.length() == offset) {
            offset--; // without this code, you will get exception when touching end of the text
        }
        try {
            if (str.charAt(offset) == ' ') {
                offset--;
            }
        } catch (StringIndexOutOfBoundsException e) {
            offset = str.length();
        }
        int startIndex = offset;
        int endIndex = offset;

        try {
            while (str.charAt(startIndex) != ' ' && str.charAt(startIndex) != '\n') {
                startIndex--;
            }
        } catch (StringIndexOutOfBoundsException e) {
            startIndex = 0;
        }

        try {
            while (str.charAt(endIndex) != ' ' && str.charAt(endIndex) != '\n') {
                endIndex++;
            }
        } catch (StringIndexOutOfBoundsException e) {
            endIndex = str.length();
        }

        // without this code, you will get 'here!' instead of 'here'
        // if you use only english, just check whether this is alphabet,
       // but 'I' use korean, so i use below algorithm to get clean word.
        try {
            char last = str.charAt(endIndex - 1);
            if (last == ',' || last == '.' ||
                    last == '!' || last == '?' ||
                    last == ':' || last == ';') {
                endIndex--;
            }
        } catch (StringIndexOutOfBoundsException e) {
            endIndex = str.length();
        }

        return str.substring(startIndex, endIndex);
    }
}
