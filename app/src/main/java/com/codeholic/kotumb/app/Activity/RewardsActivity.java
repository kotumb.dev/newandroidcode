package com.codeholic.kotumb.app.Activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.fragments.CurrentRewardFragment;
import com.codeholic.kotumb.app.fragments.ExpiredRewardFragment;
import com.codeholic.kotumb.app.fragments.RedeemRewardFragment;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RewardsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.toolbar)
    Toolbar rewardToolbar;
    @BindView(R.id.tabs)
    TabLayout rewardTabLayout;
    @BindView(R.id.viewpager)
    ViewPager rewardViewpager;
    @BindView(R.id.collapse_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    public static int currentPosition = 0;
    ViewPagerAdapter adapter;

    @BindView(R.id.tv_expire_point)
    TextView tvExpirePoints;
    @BindView(R.id.tv_current_point)
    TextView tvCurrentPoint;
    @BindView(R.id.points_layout)
    LinearLayout points_layout;

    private String mCurrentPoint;
    private String mExpire_in_this_month;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards);
        ButterKnife.bind(this);


        mCurrentPoint = getIntent().getStringExtra("CURRENT_POINT");
        mExpire_in_this_month = getIntent().getStringExtra("EXPIRE_CURRENT_MONTH");


        setUptoolbar();
        setViewPager();
        setUpDrawer();
    }

    private void setUptoolbar() {
        collapsingToolbarLayout.setTitleEnabled(false);
        rewardToolbar.setTitle("Rewards");
        setSupportActionBar(rewardToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Rewards");
        navigationView.setNavigationItemSelectedListener(this);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setViewPager() {
        addFragment(rewardViewpager);
        rewardViewpager.setOffscreenPageLimit(1);
        rewardViewpager.setCurrentItem(0);
        rewardTabLayout.setupWithViewPager(rewardViewpager);
        rewardViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position == 0){
                    points_layout.setVisibility(View.VISIBLE);

                    tvCurrentPoint.setText(mCurrentPoint);
                    tvExpirePoints.setText(mExpire_in_this_month);

                }else if (position == 1){

//                    tvRedeemCount.setText("Redeem points till date = "+ CurrentRewardFragment.mTotalRedeemCount);

                    points_layout.setVisibility(View.GONE);



                } else  {
                    points_layout.setVisibility(View.GONE);
                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void addFragment(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new CurrentRewardFragment(), getResources().getString(R.string.current_reward_text));
        adapter.addFrag(new RedeemRewardFragment(), getResources().getString(R.string.redeem_reward_text));
        adapter.addFrag(new ExpiredRewardFragment(), getResources().getString(R.string.expired_reward_text));
        viewPager.setAdapter(adapter);
    }

    private void setUpDrawer(){

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, rewardToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        drawerLayout.closeDrawer(GravityCompat.START);
        switch (menuItem.getItemId()){
            case R.id.navigation_home:
                finish();
                break;
            case R.id.navigation_current:
                currentPosition = 0;
                rewardViewpager.setCurrentItem(0);
                rewardViewpager.setOffscreenPageLimit(1);
                adapter.notifyDataSetChanged();
                break;
            case R.id.navigation_redeem:
                currentPosition = 1;
                /*rewardViewpager.setCurrentItem(1);
                rewardViewpager.setOffscreenPageLimit(1);
                adapter.notifyDataSetChanged();*///recently i commented this block
                break;
            case R.id.navigation_expired:
                currentPosition = 2;
                rewardViewpager.setCurrentItem(2);
                rewardViewpager.setOffscreenPageLimit(1);
                adapter.notifyDataSetChanged();
                break;
        }
        return false;
    }


    public static class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }


}
