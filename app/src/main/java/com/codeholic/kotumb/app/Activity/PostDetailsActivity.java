package com.codeholic.kotumb.app.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Adapter.CommentListRecyclerViewAdapter;
import com.codeholic.kotumb.app.Animation.BounceInterpolator;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.Model.PostComments;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;

import org.jitsi.meet.sdk.JitsiMeet;
import org.jitsi.meet.sdk.JitsiMeetActivity;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.codeholic.kotumb.app.Utility.Utils.showPopup;

public class PostDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.tvUserName)
    TextView tvUserName;

    @BindView(R.id.tvTime)
    TextView tvTime;

    @BindView(R.id.tvPostContent)
    TextView tvPostContent;

    @BindView(R.id.tvLikePost)
    TextView tvLikePost;

    @BindView(R.id.tvLike)
    TextView tvLike;

    @BindView(R.id.ivAddComment)
    ImageButton ivAddComment;

    @BindView(R.id.etComment)
    EditText etComment;

    @BindView(R.id.llLikePost)
    LinearLayout llLikePost;

    @BindView(R.id.ivLike)
    ImageView ivLike;

    @BindView(R.id.tvComments)
    TextView tvComments;

    @BindView(R.id.tvPhotoNumber)
    TextView tvPhotoNumber;

    @BindView(R.id.ivUserImage)
    ImageView ivUserImage;


    @BindView(R.id.pdfImage)
    ImageView pdfImage;


    @BindView(R.id.ivImageOne)
    ImageView ivImageOne;

    @BindView(R.id.ivImageTwo)
    ImageView ivImageTwo;

    @BindView(R.id.ivImageThree)
    ImageView ivImageThree;

    @BindView(R.id.ivImageFour)
    ImageView ivImageFour;

    @BindView(R.id.ivImageFive)
    ImageView ivImageFive;

    @BindView(R.id.ivImageSingle)
    ImageView ivImageSingle;

    @BindView(R.id.llEditPost)
    LinearLayout llEditPost;

    @BindView(R.id.llLike)
    LinearLayout llLike;

    @BindView(R.id.llComment)
    LinearLayout llComment;

    @BindView(R.id.llShare)
    LinearLayout llShare;

    @BindView(R.id.llSquareContainer)
    LinearLayout llSquareContainer;

    @BindView(R.id.llImageContainer)
    LinearLayout llImageContainer;

    @BindView(R.id.dim)
    View dim;

    @BindView(R.id.count_text_like)
    TextView count_text_like;

    @BindView(R.id.count_text_comment)
    TextView count_text_comment;

    @BindView(R.id.backtogroup)
    TextView backtogroup;

    @BindView(R.id.backtogrouplayout)
    LinearLayout backtogrouplayout;

    @BindView(R.id.content)
    LinearLayout content;

    @BindView(R.id.content_slide_up_view)
    View sliderView;

    @BindView(R.id.llCommentsLoading)
    LinearLayout llCommentsLoading;

    @BindView(R.id.rvCommentList)
    RecyclerView rvCommentList;

    @BindView(R.id.llBottomControl)
    LinearLayout llBottomControl;



    List<PostComments> group_post_comments = new ArrayList<>();
    LinearLayoutManager layoutManager;
    CommentListRecyclerViewAdapter commentListRecyclerViewAdapter;
    ArrayList<String> imageNameList = new ArrayList<>();
    int type = 0;
    public Group group;
    private BroadcastReceiver receiver;
    String groupImageBaseUrl = "";
    private SlideUp slideUp;
    Activity context;
    Post post;
    int commentsPagination = 0;
    AlertDialog alert;
    AlertDialog confirmAlert;
    private String url;
    private User user;
    //on create method for initializing layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);
        ButterKnife.bind(this);
        content.setVisibility(GONE);

        Log.e("postdetailsactivity", "onCreate: "+ "we have entered" );

        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();
        System.out.println("URI DATA   "+data);
        if (data != null) {
            try {
                int lastIndexOf = data.toString().lastIndexOf("/");
                String post_id = data.toString().substring(lastIndexOf + 1, data.toString().length());
                int indexOf = data.toString().indexOf("group/");
                int indexOfPost = data.toString().indexOf("/singlePost");
                String groupId = data.toString().substring(indexOfPost +12, lastIndexOf);
                System.out.println("Index Of Post    "+post_id);
                fetchPostById(post_id, groupId);
                initToolbar();
                setupCommentView();
                slideUp.hideImmediately();
                setOnClickListener();
                return ;
            } catch (Exception e) {
                e.printStackTrace();
                initToolbar();
                setupCommentView();
                slideUp.hideImmediately();
                Utils.showPopup(this, getString(R.string.something_wrong), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            }
        }

        init();
        initToolbar();
        setupCommentView();
        showView();
        setOnClickListener();
        setBroadCast();
        setUpJitSiMeet();

        boolean openComment = getIntent().getBooleanExtra(ResponseParameters.OPEN_COMMENT_VIEW, false);

        if (openComment) {
            showCommentView();
        }

//        fetchPostObject();

        if (post!=null) {
            fetchPostById(post.post_id, group.groupId);
        }
    }

    private void fetchPostById(String post_id, String groupId) {
        String userId = SharedPreferencesMethod.getUserId(PostDetailsActivity.this);
        String url = API.GROUP_INFO + groupId + "/" + userId + "?post_id=" + post_id;
        System.out.println("Post URL  "+url);
        PostDetailsActivity context = PostDetailsActivity.this;
        API.sendRequestToServerGET(context, url, API.GROUP_POSTS + 2);// service call for getting post
    }

    private void fetchPostObject() {
        String url = API.base_api + "services/post/" + post.post_id;
        AQuery aq = new AQuery(this);
        aq.ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                try {
                    //Log.e(context.getClass().getName(), json.toString());
                    if (json != null) {
                        //Toast.makeText(PostDetailsActivity.this, json.toString(), Toast.LENGTH_SHORT).show();
                        // callApiCallback(context, input, output);
                        if (json.has("errors")) {
                            showPopup(PostDetailsActivity.this, "Post Deleted", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                }
                            });
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //callApiCallback(context, input, null);
                }
            }
        }.method(AQuery.METHOD_GET).header("Auth-Key", "KOTUMB__AuTh_keY"));
    }

    public void setupCommentView() {
        slideUp = new SlideUpBuilder(sliderView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                        dim.setAlpha(1 - (percent / 100));
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        if (visibility == GONE) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                Window window = getWindow();
                                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
                            }
                        } else if (visibility == VISIBLE) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                Window window = getWindow();
                                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                window.setStatusBarColor(getResources().getColor(R.color.dimBg));
                            }
                        }
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                //.withInterpolator(new BounceInterpolator(0.3, 35))
                .withStartState(SlideUp.State.HIDDEN)
                .withTouchableAreaDp(1000)
                .withAutoSlideDuration(500)
                .build();
    }

    //initilizing toolbar and setting title
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.share_post_btn_text));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //initializing for variables0
    private void init() {
        context = this;
        if (getIntent().hasExtra(ResponseParameters.TYPE)) {
            type = getIntent().getIntExtra(ResponseParameters.TYPE, 1);
        }
        post = (Post) getIntent().getSerializableExtra(ResponseParameters.POST);
        group = (Group) getIntent().getSerializableExtra(ResponseParameters.GROUP);
        groupImageBaseUrl = getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL);
        user = getIntent().getParcelableExtra("USER");

        boolean back = getIntent().getBooleanExtra("back", false);

            if (back) {
                if (group!=null) {
                    backtogroup.setText("Go to " + group.groupName);
                    backtogrouplayout.setVisibility(VISIBLE);
                }else {
                    startActivity(new Intent(PostDetailsActivity.this,HomeScreenActivity.class));

                }

            } else {
                backtogrouplayout.setVisibility(GONE);

            }

    }

    //initializing for variables
    private void initForDeepLink() {
        context = this;
        if (getIntent().hasExtra(ResponseParameters.TYPE)) {
            type = getIntent().getIntExtra(ResponseParameters.TYPE, 1);
            System.out.println("Links Type   "+type);
        }
//        post = (Post) getIntent().getSerializableExtra(ResponseParameters.POST);
//        group = (Group) getIntent().getSerializableExtra(ResponseParameters.GROUP);
//        groupImageBaseUrl = getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL);

        boolean back = getIntent().getBooleanExtra("back", true);
        System.out.println("Back Links  "+type);
        if (back) {
            backtogroup.setText("Go to " + group.groupName);
            backtogrouplayout.setVisibility(VISIBLE);
        } else {
            backtogrouplayout.setVisibility(GONE);
        }
    }
    private void showFullPDF(Post post){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + API.group_media+post.image), "text/html");
        startActivity(intent);
    }


    @SuppressLint("ClickableViewAccessibility")
    private void showView() {

        if (post!=null) {
            String source = "" + post.firstName.trim() + " " + post.middleName + " " + post.lastName.trim() + " ";
            SpannableString spannableName = new SpannableString(source.replaceAll("  ", " "));
            spannableName.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, spannableName.length(), SPAN_EXCLUSIVE_EXCLUSIVE);

            if (post.sharedPostUserName.trim().isEmpty()) {
                tvUserName.setText(TextUtils.concat(spannableName));
            } else {
                SpannableString shared = new SpannableString(getResources().getString(R.string.shared_btn_text));
                shared.setSpan(new RelativeSizeSpan(.7f),
                        0, shared.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                SpannableString sharedUserName = new SpannableString(" " + post.sharedPostUserName.trim());
                sharedUserName.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, sharedUserName.length(), SPAN_EXCLUSIVE_EXCLUSIVE);

                SpannableString postText = new SpannableString(" " + getResources().getString(R.string.post_btn_text));
                postText.setSpan(new RelativeSizeSpan(.7f),
                        0, postText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvUserName.setText(TextUtils.concat(spannableName, shared, sharedUserName, postText));
            }


            tvUserName.setTextColor(getResources().getColor(android.R.color.secondary_text_light));


            if (!post.content.trim().isEmpty()) {
                tvPostContent.setVisibility(View.VISIBLE);
                tvPostContent.setText(post.content);


                tvPostContent.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                            int mOffset = tvPostContent.getOffsetForPosition(motionEvent.getX(), motionEvent.getY());
                            //  mTxtOffset.setText("" + mOffset);

                            String tappedwordwithspaceandnewline = Utility.findWordForRightHanded(tvPostContent.getText().toString(), mOffset);
                            String tappedword = tappedwordwithspaceandnewline
                                    .replaceAll(System.getProperty("line.separator"), "")
                                    .replaceAll(" ","");


                            //Toast.makeText(context, tappedword , Toast.LENGTH_SHORT).show();

                            if (tappedword.contains("meet.jit.si/")) {

                                tvPostContent.setLinksClickable(false);

                                String[] splitUrl = post.content.split("meet.jit.si/");

                                JitsiMeetConferenceOptions options
                                        = new JitsiMeetConferenceOptions.Builder()
                                        .setRoom(splitUrl[1])
                                        .build();
                                // Launch the new activity with the given options. The launch() method takes care
                                // of creating the required Intent and passing the options.
                                JitsiMeetActivity.launch(PostDetailsActivity.this, options);

                            } else if(tappedword.toLowerCase().contains("https")||tappedword.toLowerCase().contains("http")){

                                tvPostContent.setLinksClickable(true);

                                if(tappedword.toLowerCase().contains("https")) {

                                    navigateToActivity( tappedword );

                                }else if(tappedword.toLowerCase().contains("http")){

                                    navigateToActivity( tappedword);

                                }else {
                                    Log.e("postdetailsacxxx", "onTouch3: some url"+tappedword );
                                    Intent intent = new Intent(context, PostDetailsActivity.class);
                                    System.out.println("Send Data    " + post + "    " + (PostDetailsActivity.this).group + "    " + groupImageBaseUrl + "   " + "  " + type);
                                    intent.putExtra(ResponseParameters.POST, post);
                                    intent.putExtra(ResponseParameters.GROUP, (PostDetailsActivity.this).group);
                                    intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, groupImageBaseUrl);
                                    intent.putExtra(ResponseParameters.TYPE, type);
                                    startActivity(intent);
                                }
                            }
                        }

                        tvPostContent.setLinksClickable(false);
                        return false;
                    }
                });


            } else {
                tvPostContent.setVisibility(View.GONE);
            }
            String countText = "";
            if (post.total_likes == 0) {
                tvLikePost.setText(getResources().getString(R.string.like_btn_text));
            } else if (post.total_likes == 1) {
                countText = "1 Like";
                //tvLikePost.setText(post.total_likes + " " + getResources().getString(R.string.like_btn_text));
            } else {
                countText = post.total_likes + " Likes";
                // tvLikePost.setText(post.total_likes + " " + getResources().getString(R.string.likes_btn_text));
            }

            if (post.total_comments == 0) {
                tvComments.setText(getResources().getString(R.string.comment_btn_text));
            } else if (post.total_comments == 0) {
                //tvComments.setText(post.total_comments + getResources().getString(R.string.comment_btn_text));
                if (!countText.isEmpty()) {
                    countText = countText + "     ";
                }
                countText = countText + "1 Comment";
            } else {
                // tvComments.setText(post.total_comments + getResources().getString(R.string.comments_btn_text));
                if (!countText.isEmpty()) {
                    countText = countText + "     ";
                }
                countText = countText + post.total_comments + " Comments";
            }

            if (post.total_likes > 0) {
                String like = post.total_likes > 1 ? " Likes" : " Like";
                count_text_like.setText(post.total_likes + like);
                count_text_like.setVisibility(View.VISIBLE);
                //countText.put(post.post_id, countText);
                count_text_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent PostLikesIntent = new Intent(context, PostLikesActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(ResponseParameters.POST, post);
                        bundle.putString(ResponseParameters.IMAGE_BASE_URL, API.imageUrl);
                        PostLikesIntent.putExtra(ResponseParameters.BUNDLE, bundle);
                        startActivity(PostLikesIntent);
                    }
                });
            } else {
                count_text_like.setVisibility(View.GONE);
            }

            if (post.total_comments > 0) {
                String like = post.total_likes > 1 ? " Comments" : " Comment";
                count_text_comment.setText(post.total_comments + like);
                count_text_comment.setVisibility(View.VISIBLE);
                // countTexts.put(post.post_id, countText);
                count_text_comment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showCommentView();
                    }
                });
            } else {
                count_text_comment.setVisibility(View.GONE);
            }

            tvTime.setText(post.duration.trim());
            String url = API.imageUrl + post.avatar.trim();
       /* ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", url);
        clipboard.setPrimaryClip(clip);*/
            new AQuery(this).id(ivUserImage).image(url, true, true, 300, R.drawable.default_profile);
            Log.e("TAG", "image " + post.image);
            if (!post.image.trim().isEmpty()) {
                /* slidesPager.setVisibility(View.VISIBLE);
                 pageIndicatorView.setVisibility(View.VISIBLE);
                 llPointer.setVisibility(View.VISIBLE);
                 pageIndicatorView.setViewPager( slidesPager);*/

                String[] splitArray = post.image.split("\\|");

                imageNameList = new ArrayList<String>(Arrays.asList(splitArray));

                //configurePagerHolder(holder, position);
            }
                /* llPointer.setVisibility(View.GONE);
                 slidesPager.setVisibility(View.GONE);
                 pageIndicatorView.setVisibility(View.GONE);*/

            if (post.self_like == 1) {
                Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up_liked);
                tvLikePost.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            } else {
                Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up);
                tvLikePost.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            }


            ivImageOne.setVisibility(View.GONE);
            ivImageTwo.setVisibility(View.GONE);
            ivImageThree.setVisibility(View.GONE);
            ivImageFour.setVisibility(View.GONE);
            ivImageFive.setVisibility(View.GONE);
            llSquareContainer.setVisibility(View.GONE);
            ivImageSingle.setVisibility(View.GONE);
            if (Utils.getExtension(post.image).equalsIgnoreCase("pdf")) {
                pdfImage.setVisibility(VISIBLE);
                pdfImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showFullPDF(post);
                    }
                });
            } else {
                pdfImage.setVisibility(GONE);
                if (imageNameList.size() == 0) {
                    llImageContainer.setVisibility(View.GONE);
                } else {
                    llImageContainer.setVisibility(View.VISIBLE);
                    if (imageNameList.size() == 1) {
                        new AQuery(this).id(ivImageSingle).image(groupImageBaseUrl + imageNameList.get(0), true, true, 300, R.drawable.no_image);
                        ivImageSingle.setVisibility(View.VISIBLE);
                    } else if (imageNameList.size() == 3) {
                        new AQuery(this).id(ivImageThree).image(groupImageBaseUrl + imageNameList.get(0), true, true, 300, R.drawable.no_image);
                        ivImageThree.setVisibility(View.VISIBLE);
                        new AQuery(this).id(ivImageFour).image(groupImageBaseUrl + imageNameList.get(1), true, true, 300, R.drawable.no_image);
                        ivImageFour.setVisibility(View.VISIBLE);
                        new AQuery(this).id(ivImageFive).image(groupImageBaseUrl + imageNameList.get(2), true, true, 300, R.drawable.no_image);
                        ivImageFive.setVisibility(View.VISIBLE);
                    } else {
                        for (int i = 0; i < imageNameList.size(); i++) {
                            switch (i) {
                                case 0:
                                    new AQuery(this).id(ivImageOne).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.no_image);
                                    ivImageOne.setVisibility(View.VISIBLE);
                                    break;
                                case 1:
                                    new AQuery(this).id(ivImageTwo).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.no_image);
                                    ivImageTwo.setVisibility(View.VISIBLE);
                                    break;
                                case 2:
                                    new AQuery(this).id(ivImageThree).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.no_image);
                                    ivImageThree.setVisibility(View.VISIBLE);
                                    break;
                                case 3:
                                    new AQuery(this).id(ivImageFour).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.no_image);
                                    ivImageFour.setVisibility(View.VISIBLE);
                                    break;
                                case 4:
                                    new AQuery(this).id(ivImageFive).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.no_image);
                                    ivImageFive.setVisibility(View.VISIBLE);
                                    break;
                            }
                        }
                    }
                    if (imageNameList.size() > 5) {
                        llSquareContainer.setVisibility(View.VISIBLE);
                        tvPhotoNumber.setText((imageNameList.size() - 5) + "+");
                    }
                }

            }

            if (type == 1) {
                llBottomControl.setVisibility(View.GONE);
                llEditPost.setVisibility(View.GONE);
            } else {
                llBottomControl.setVisibility(View.VISIBLE);
                if (post.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(this))
                        || group.role.equalsIgnoreCase("0")
                        || post.role.trim().equalsIgnoreCase("1")) {
                    llEditPost.setVisibility(View.VISIBLE);
                } else {
                    llEditPost.setVisibility(View.GONE);
                }
            }


            tvUserName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(PostDetailsActivity.this, OtherUserProfileActivity.class);
                    intent.putExtra("USER", user);
                    startActivity(intent);

//                if (user.getFirstName().equalsIgnoreCase("Super")&&user.getLastName().equalsIgnoreCase("User")) {
//                }else{
//                    Intent intent = new Intent(PostDetailsActivity.this, OtherUserProfileActivity.class);
//                    intent.putExtra("USER", user);
//                    startActivity(intent);
//                }
                }
            });
        }
    }

    private void navigateToActivity(String url){
        Log.e("hover", "navigateToActivityx: requested url: "+url );
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }catch (ActivityNotFoundException e){
            Log.e("hover", "navigateToActivityx: please check the link.. its invalid.. there shouldnt be any space or newline or capital letter in https etc " + url );
            Toast.makeText(context,"Invalid link", Toast.LENGTH_SHORT).show();
        }
    }

    public void setOnClickListener() {
        llImageContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ImagesShowActivity.class);
                intent.putExtra(ResponseParameters.IMAGE, imageNameList);
                intent.putExtra(ResponseParameters.IMAGE_URI_LIST, new ArrayList<>());
                intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, groupImageBaseUrl);
                intent.putExtra(ResponseParameters.SHOW_CLEAR, false);
                startActivity(intent);
            }
        });

        llEditPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                llEditPost.startAnimation(Utility.showAnimtion(context));
                showPopUp();

            }
        });



        llLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (post.self_like == 0) {
                    if (!Utility.isConnectingToInternet(context)) { // check net connection
//                        Toast.makeText(context, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                        showPopup(PostDetailsActivity.this, getResources().getString(R.string.app_no_internet_error));
                        return;
                    }

                    String url = API.GROUP_LIKE_POST + post.post_id + "/" + post.group_id + "/" + SharedPreferencesMethod.getUserId(context);
                    API.sendRequestToServerGET(context, url, API.GROUP_LIKE_POST);
                    Utils.logEventGroupLike(PostDetailsActivity.this, group.groupId);

                    Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up_liked);
                    tvLikePost.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);


                    tvLikePost.startAnimation(Utility.showAnimtion(context));
                    post.self_like = 1;
                    post.total_likes = post.total_likes + post.self_like;
                } else {
                    if (!Utility.isConnectingToInternet(context)) { // check net connection
//                        Toast.makeText(context, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                        showPopup(PostDetailsActivity.this, getResources().getString(R.string.app_no_internet_error));
                        return;
                    }

                    String url = API.GROUP_UNLIKE_POST + post.post_id + "/" + post.group_id + "/" + SharedPreferencesMethod.getUserId(context);
                    API.sendRequestToServerGET(context, url, API.GROUP_UNLIKE_POST);

                    Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up);
                    tvLikePost.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);


                    tvLikePost.startAnimation(Utility.showAnimtion(context));
                    post.self_like = 0;
                    post.total_likes = post.total_likes - 1;
                }
                if (post.total_likes == 0) {
                    tvLikePost.setText(getResources().getString(R.string.like_btn_text));
                } else if (post.total_likes == 1) {
                    //tvLikePost.setText(post.total_likes + " " + getResources().getString(R.string.like_btn_text));
                } else {
                    //tvLikePost.setText(post.total_likes + " " + getResources().getString(R.string.likes_btn_text));
                }
            }
        });

        llComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llComment.startAnimation(Utility.showAnimtion(context));
                showCommentView();
            }
        });

        llShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llShare.startAnimation(Utility.showAnimtion(context));
                Utils.showSharePopUpMenu(llShare, post, group.groupId, PostDetailsActivity.this);
//                Intent intent = new Intent(context, JoinedGroupListActivity.class);
//                intent.putExtra(ResponseParameters.POST, post);
//                startActivity(intent);
            }
        });

        backtogrouplayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PostDetailsActivity.this, GroupPostActivity.class);
                intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, groupImageBaseUrl);
                intent.putExtra(ResponseParameters.GROUP, group);
                intent.putExtra(ResponseParameters.TYPE, 0);
                PostDetailsActivity.this.startActivity(intent);
                finish();
            }
        });
    }


    private void showPopUp() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_group_post, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llRemoveUser = dialogView.findViewById(R.id.llRemoveUser);
        final LinearLayout llBlockUser = dialogView.findViewById(R.id.llBlockUser);
        final LinearLayout llDeletePost = dialogView.findViewById(R.id.llDeletePost);
        final LinearLayout llEditPost = dialogView.findViewById(R.id.llEditPost);
        final LinearLayout llReportPost = dialogView.findViewById(R.id.llReportPost);
        final LinearLayout llReportUser = dialogView.findViewById(R.id.llReportUser);
        final LinearLayout llIgnore = dialogView.findViewById(R.id.llIgnore);

        if (post.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(context))) {
            llEditPost.setVisibility(View.VISIBLE);
            llDeletePost.setVisibility(View.VISIBLE);
        }

        if (group.role.equalsIgnoreCase("0")) {
            llRemoveUser.setVisibility(View.VISIBLE);
            llBlockUser.setVisibility(View.VISIBLE);
            llDeletePost.setVisibility(View.VISIBLE);
            if (post.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(context))) {
                llRemoveUser.setVisibility(View.GONE);
                llBlockUser.setVisibility(View.GONE);
            }
        }

        if (post.role.trim().equalsIgnoreCase("1") // simple member post
                && !post.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(context))) {
            if (group.role.equalsIgnoreCase("0")) {
                llReportPost.setVisibility(View.GONE);
                llReportUser.setVisibility(View.GONE);
            } else {
                llReportPost.setVisibility(View.VISIBLE);
                llReportUser.setVisibility(View.VISIBLE);
            }
        }

        llRemoveUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = API.GROUP_DELETE_MEMBER + post.group_id + "/" + post.userId;
                Log.e("TAG", "" + url);

                showPopUp(getResources().getString(R.string.delete_user_dialog_msg), url, "", null);
                /*notifyItemRemoved(position);
                posts.remove(position);
                notifyItemRangeChanged(0, posts.size());*/
                alert.dismiss();
                //hideLayout();
            }
        });
        llBlockUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = API.GROUP_BLOCK_MEMBER + post.group_id + "/" + post.userId;


                /*notifyItemRemoved(position);
                posts.remove(position);
                notifyItemRangeChanged(0, posts.size());*/

                //hideLayout();
                showPopUp(getResources().getString(R.string.block_user_dialog_msg), url, API.GROUP_BLOCK_MEMBER, null);
                alert.dismiss();
            }
        });
        llDeletePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "";
                url = API.GROUP_POST_DELETE + post.post_id + "/" + SharedPreferencesMethod.getUserId(context);
                showPopUp(getResources().getString(R.string.delete_post_dialog_msg), url, API.GROUP_POST_DELETE, null);
                Log.e("TAG", "" + url);
                //API.sendRequestToServerGET(activity, url, API.GROUP_POST_DELETE);


                alert.dismiss();
                //hideLayout();
            }
        });

        llIgnore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "";
                url = API.REPORTED_GROUP_POST_ACTION + post.post_id + "/" + SharedPreferencesMethod.getUserId(context);
                API.sendRequestToServerGET(context, url, API.REPORTED_GROUP_POST_ACTION);
            }
        });

        llEditPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createPostIntent = new Intent(context, CreatePostActivity.class);
                createPostIntent.putExtra(ResponseParameters.POST,post);
                createPostIntent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL));
                createPostIntent.putExtra(ResponseParameters.GROUP, group);
                startActivity(createPostIntent);
                alert.dismiss();
            }
        });

        llReportPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = API.GROUP_POST_REPORT;
                Log.e("TAG", "" + url);

                HashMap<String, Object> input = new HashMap<>();
                input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(context));
                input.put(RequestParameters.GROUPID, "" + group.groupId);
                input.put(RequestParameters.POSTID, "" + post.post_id);


                Log.e("INPUT", "" + input);
                //API.sendRequestToServerGET(activity, url, API.GROUP_POST_DELETE);
                reportPopup(getResources().getString(R.string.post_report_btn_text), url, API.GROUP_POST_REPORT, input);
                alert.dismiss();
            }
        });


        llReportUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> input = new HashMap<>();
                input.put(RequestParameters.FROMUSERID, "" + SharedPreferencesMethod.getUserId(context));
                input.put(RequestParameters.GROUPID, "" + group.groupId);
                input.put(RequestParameters.TOUSERID, "" + post.userId);
                //input.put(RequestParameters.REASON, "--");
                Log.e("input", "" + input.toString());
                reportPopup(getResources().getString(R.string.user_report_btn_text), API.GROUP_USER_REPORT, API.GROUP_USER_REPORT, input);
                alert.dismiss();
            }
        });


        alert = dialogBuilder.create();
        alert.setCancelable(true);
        alert.show();

    }


    private void reportPopup(String title, final String url, final String apiUrl, final HashMap<String, Object> input) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.report_popup, null);
        dialogBuilder.setView(dialogView);
        final EditText etTitle = dialogView.findViewById(R.id.etTitle);
        final TextView Pop_title = dialogView.findViewById(R.id.Pop_title);
        Pop_title.setText(title);
        final TextInputLayout etTitleLayout = dialogView.findViewById(R.id.etTitleLayout);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etTitleLayout.setErrorEnabled(false);
                if (etTitle.getText().toString().trim().isEmpty()) {
                    etTitleLayout.setErrorEnabled(true);
                    etTitleLayout.setError(getResources().getString(R.string.reason_empty_input));
                } else if (Utility.isConnectingToInternet(context)) {
                    if (apiUrl.equalsIgnoreCase(API.GROUP_USER_REPORT)) {
                        showPopup(PostDetailsActivity.this, getResources().getString(R.string.user_has_been_reported_msg));
//                        Toast.makeText(context, getResources().getString(R.string.user_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                        input.put(RequestParameters.REASON, "" + etTitle.getText().toString().trim());
                    } else if (apiUrl.equalsIgnoreCase(API.GROUP_POST_REPORT)) {
                        showPopup(PostDetailsActivity.this, getResources().getString(R.string.app_no_internet_error));
//                        Toast.makeText(context, getResources().getString(R.string.post_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                        input.put(RequestParameters.DESCRIPTION, "" + etTitle.getText().toString().trim());
                    }
                    Log.e("input", "" + input);
                    API.sendRequestToServerPOST_PARAM(context, url, input);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                    confirmAlert.dismiss();
                } else {
//                    Toast.makeText(context, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    showPopup(PostDetailsActivity.this, getResources().getString(R.string.app_no_internet_error));
                }
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                confirmAlert.dismiss();
            }
        });


        this.confirmAlert = dialogBuilder.create();
        this.confirmAlert.setCancelable(true);
        this.confirmAlert.show();
    }


    private void showPopUp(String title, final String url, final String apiUrl, final HashMap<String, Object> input) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);
        final TextView tvCancel = dialogView.findViewById(R.id.tvCancel);


        tvTitle.setText(title);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Utility.isConnectingToInternet(context)) { // check net connection
//                    Toast.makeText(context, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    showPopup(PostDetailsActivity.this, getResources().getString(R.string.app_no_internet_error));
                    confirmAlert.dismiss();
                    return;
                }
                if (input == null)
                    API.sendRequestToServerGET(context, url, apiUrl);
                else
                    API.sendRequestToServerPOST_PARAM(context, url, input);
                if (apiUrl.equalsIgnoreCase(API.GROUP_USER_REPORT)) {
//                    Toast.makeText(context, getResources().getString(R.string.user_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                    showPopup(PostDetailsActivity.this, getResources().getString(R.string.user_has_been_reported_msg));
                } else if (apiUrl.equalsIgnoreCase(API.GROUP_POST_REPORT)) {
//                    Toast.makeText(context, getResources().getString(R.string.post_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                    showPopup(PostDetailsActivity.this, getResources().getString(R.string.user_has_been_reported_msg));
                }
                confirmAlert.dismiss();
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                confirmAlert.dismiss();
            }
        });

        confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();

    }


    public void showCommentView() {
        slideUp.show();

        commentsPagination = 0;
        /*this.group_post_comments = new ArrayList<>(post.group_post_comments);*/
        llCommentsLoading.setVisibility(VISIBLE);
        group_post_comments = new ArrayList<>();

        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        rvCommentList.setLayoutManager(layoutManager);
        commentListRecyclerViewAdapter = new CommentListRecyclerViewAdapter(rvCommentList, this, this.group_post_comments);
        commentListRecyclerViewAdapter.setUserImageBaseUrl(API.imageUrl);
        commentListRecyclerViewAdapter.setGroupImageBaseUrl(getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL));
        rvCommentList.setHasFixedSize(true);
        rvCommentList.setAdapter(commentListRecyclerViewAdapter);
        rvCommentList.smoothScrollToPosition(0);

        if (url == null) {
            url = API.GROUP_POST_COMMENTS + post.post_id + "/" + (commentsPagination) + "/" + SharedPreferencesMethod.getUserId(this);
        }
//        Toast.makeText(context, url, Toast.LENGTH_SHORT).show();
        API.sendRequestToServerGET(this, url, API.GROUP_POST_COMMENTS);// service call for getting messages


        if (post.self_like == 1) {
            ivLike.setImageResource(R.drawable.ic_thumb_up_liked);
        } else {
            ivLike.setImageResource(R.drawable.ic_thumb_up);
        }


        if (post.total_likes == 0) {
            tvLike.setText(getResources().getString(R.string.be_the_first_to_like_this_post_msg));
        } else if (post.total_likes == 1) {
            tvLike.setText(post.total_likes + " " + getResources().getString(R.string.like_btn_text));
        } else {
            tvLike.setText(post.total_likes + " " + getResources().getString(R.string.likes_btn_text));
        }


        tvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (post.total_likes > 0) {
                    Intent PostLikesIntent = new Intent(context, PostLikesActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(ResponseParameters.POST, post);
                    bundle.putString(ResponseParameters.IMAGE_BASE_URL, API.imageUrl);
                    PostLikesIntent.putExtra(ResponseParameters.BUNDLE, bundle);
                    startActivity(PostLikesIntent);
                }
            }
        });

        ivAddComment.setEnabled(false);

        etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().trim().isEmpty()) {
                    ivAddComment.setEnabled(true);
                } else {
                    ivAddComment.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ivAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etComment.getText().toString().trim().isEmpty()) {
                    if (!Utility.isConnectingToInternet(context)) { // check net connection
//                        Toast.makeText(context, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                        showPopup(PostDetailsActivity.this, getResources().getString(R.string.app_no_internet_error));
                        return;
                    }

                    /*PostComments postComments = new PostComments();
                    postComments.comment = etComment.getText().toString().trim();
                    postComments.firstName = SharedPreferencesMethod.getString(context, ResponseParameters.FirstName);
                    postComments.lastName = SharedPreferencesMethod.getString(context, ResponseParameters.LastName);
                    postComments.avatar = SharedPreferencesMethod.getString(context, ResponseParameters.Avatar);
                    posts.get(position).group_post_comments.add(postComments);
                    postListRecyclerViewAdapter.notifyItemChanged(position);
                    GroupPostActivity.this.group_post_comments.add(0, postComments);
                    commentListRecyclerViewAdapter.notifyItemInserted(0);*/
                    HashMap<String, Object> input = new HashMap();
                    input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(context));
                    input.put(RequestParameters.GROUPID, "" + group.groupId);
                    input.put(RequestParameters.POSTID, "" + post.post_id);
                    input.put(RequestParameters.COMMENT, "" + Utils.encode(etComment.getText().toString().trim()));

                    Log.e("input", "" + input);

                    API.sendRequestToServerPOST_PARAM(context, API.GROUP_ADD_COMMENT, input);
                    Utils.logEventGroupCommented(PostDetailsActivity.this, group.groupId);
                    etComment.setText("");
                }
            }
        });

        llLikePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (post.self_like == 0) {
                    if (!Utility.isConnectingToInternet(context)) { // check net connection
//                        Toast.makeText(context, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                        showPopup(PostDetailsActivity.this, getResources().getString(R.string.app_no_internet_error));
                        return;
                    }


                    ivLike.setImageResource(R.drawable.ic_thumb_up_liked);

                    final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);

                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    BounceInterpolator interpolator = new BounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);
                    ivLike.startAnimation(myAnim);
                    post.self_like = 1;
                    post.total_likes = post.total_likes + post.self_like;
                    if (post.total_likes == 0) {
                        tvLikePost.setText(getResources().getString(R.string.be_the_first_to_like_this_post_msg));
                    } else if (post.total_likes == 1) {
                        tvLikePost.setText(post.total_likes + " " + getResources().getString(R.string.like_btn_text));
                    } else {
                        tvLikePost.setText(post.total_likes + " " + getResources().getString(R.string.likes_btn_text));
                    }


                    String url = API.GROUP_LIKE_POST + post.post_id + "/" + post.group_id + "/" + SharedPreferencesMethod.getUserId(context);
                    API.sendRequestToServerGET(context, url, API.GROUP_LIKE_POST);
                    Utils.logEventGroupLike(PostDetailsActivity.this, group.groupId);
                } else {
                    if (!Utility.isConnectingToInternet(context)) { // check net connection
//                        Toast.makeText(context, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                        showPopup(PostDetailsActivity.this, getResources().getString(R.string.app_no_internet_error));
                        return;
                    }


                    ivLike.setImageResource(R.drawable.ic_thumb_up);

                    final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);

                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    BounceInterpolator interpolator = new BounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);
                    ivLike.startAnimation(myAnim);
                    myAnim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    post.self_like = 0;
                    post.total_likes = post.total_likes - 1;
                    if (post.total_likes == 0) {
                        tvLikePost.setText(getResources().getString(R.string.be_the_first_to_like_this_post_msg));
                    } else if (post.total_likes == 1) {
                        tvLikePost.setText(post.total_likes + " " + getResources().getString(R.string.like_btn_text));
                    } else {
                        tvLikePost.setText(post.total_likes + " " + getResources().getString(R.string.likes_btn_text));
                    }

                    String url = API.GROUP_UNLIKE_POST + post.post_id + "/" + post.group_id + "/" + SharedPreferencesMethod.getUserId(context);
                    API.sendRequestToServerGET(context, url, API.GROUP_UNLIKE_POST);
                }
            }
        });


        commentListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("load more", "load moe");
                if (group_post_comments.size() >= 10) {
                    rvCommentList.post(new Runnable() {
                        public void run() {
                            group_post_comments.add(null);
                            Log.e("load more", "load more " + group_post_comments.size());
                            commentListRecyclerViewAdapter.notifyItemInserted(group_post_comments.size() - 1);
                        }
                    });

                    Log.e("URL", "" + API.GROUP_POST_COMMENTS + post.post_id + "/" + (commentsPagination + 10) + "/" + SharedPreferencesMethod.getUserId(context));
                    API.sendRequestToServerGET(context, API.GROUP_POST_COMMENTS + post.post_id + "/" + (commentsPagination + 10) + "/" + SharedPreferencesMethod.getUserId(context), API.GROUP_POST_COMMENTS_UPDATE);// service call for getting messages
                }
            }
        });
    }

    private void setBroadCast() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //do something based on the intent's action
                if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_DELETE)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        String userId = intent.getStringExtra(ResponseParameters.UserId);

                        if (userId.trim().equalsIgnoreCase(SharedPreferencesMethod.getUserId(PostDetailsActivity.this))) {
                            finish();
                            return;
                        }
                    }
                } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_ROLE_UPDATE)) {
                    Group updatedGroup = (Group) intent.getSerializableExtra(ResponseParameters.GROUP);
                    if (group.groupId.trim().equalsIgnoreCase(updatedGroup.groupId.trim())) {
                        group = updatedGroup;
                        showView();
                    }
                } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_EDIT_POST)) {
                    Post post = (Post) intent.getSerializableExtra(ResponseParameters.POST);
                    if (PostDetailsActivity.this.post.post_id.trim().equalsIgnoreCase(post.post_id.trim())) {
                        PostDetailsActivity.this.post = post;
                        showView();
                    }
                } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_DELETE_POST)
                        || intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_DELETED)
                        || intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_BLOCK)) {
                    Post post = (Post) intent.getSerializableExtra(ResponseParameters.POST);
                    if (PostDetailsActivity.this.post.post_id.trim().equalsIgnoreCase(post.post_id.trim())) {
                        finish();
                    }
                } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_POST_LIKE)
                        || intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_POST_UNLIKE)) {
                    Post post = (Post) intent.getSerializableExtra(ResponseParameters.POST);
                    if (PostDetailsActivity.this.post.post_id.trim().equalsIgnoreCase(post.post_id.trim())) {
                        PostDetailsActivity.this.post = post;
                        showView();
                        if (slideUp.isVisible()) {
                            if (post.total_likes == 0) {
                                tvLike.setText(getResources().getString(R.string.be_the_first_to_like_this_post_msg));
                            } else if (post.total_likes == 1) {
                                tvLike.setText(post.total_likes + " " + getResources().getString(R.string.like_btn_text));
                            } else {
                                tvLike.setText(post.total_likes + " " + getResources().getString(R.string.likes_btn_text));
                            }
                            if (post.self_like == 1) {
                                ivLike.setImageResource(R.drawable.ic_thumb_up_liked);
                            } else {
                                ivLike.setImageResource(R.drawable.ic_thumb_up);
                            }
                        }
                    }
                    //   }

                } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_COMMENT_CREATED) ||
                        intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_COMMENT_DELETED)) {
                    PostComments postComments = (PostComments) intent.getSerializableExtra(ResponseParameters.GROUP_POSTS_COMMENTS);
                    Post post = (Post) intent.getSerializableExtra(ResponseParameters.POST);


                    if (PostDetailsActivity.this.post.post_id !=null){
                    if (PostDetailsActivity.this.post.post_id.trim().equalsIgnoreCase(post.post_id.trim())) {
                        PostDetailsActivity.this.post = post;
                        showView();
                        if (slideUp.isVisible()) {
                            if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_COMMENT_CREATED)) {
                                group_post_comments.add(0, postComments);
                                if (commentListRecyclerViewAdapter != null) {
                                    commentListRecyclerViewAdapter.notifyItemRangeInserted(0, 1);
                                    commentListRecyclerViewAdapter.notifyItemRangeChanged(0, group_post_comments.size());
                                }
                            } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_COMMENT_DELETED)) {
                                for (int i = 0; i < group_post_comments.size(); i++) {
                                    if (group_post_comments.get(i).comment_id.trim().equalsIgnoreCase(postComments.comment_id.trim())) {
                                        group_post_comments.remove(i);
                                        if (commentListRecyclerViewAdapter != null) {
                                            commentListRecyclerViewAdapter.notifyItemRemoved(i);
                                            commentListRecyclerViewAdapter.notifyItemRangeChanged(0, group_post_comments.size());
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    }
                }
                Log.e("test", "test");
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.GROUP_MEMBER_DELETE);
        filter.addAction(ResponseParameters.GROUP_MEMBER_ROLE_UPDATE);
        filter.addAction(ResponseParameters.GROUP_MEMBER_BLOCK);
        filter.addAction(ResponseParameters.GROUP_DELETED);
        filter.addAction(ResponseParameters.GROUP_EDIT_POST);
        filter.addAction(ResponseParameters.GROUP_DELETE_POST);
        filter.addAction(ResponseParameters.GROUP_POST_LIKE);
        filter.addAction(ResponseParameters.GROUP_POST_UNLIKE);
        filter.addAction(ResponseParameters.GROUP_COMMENT_CREATED);
        filter.addAction(ResponseParameters.GROUP_COMMENT_DELETED);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {

        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroy();
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.cvEduSave: // save button click
                try {
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.etYearOfPassing: // for selecting passing year

                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (slideUp.isVisible()) {
            slideUp.hide();
            //post = new Post();
            //slideUp.hideImmediately();
        } else {
            if (!isTaskRoot()) {
                super.onBackPressed();
            } else {
                startActivity(new Intent(this, SplashActivity.class));
                finish();
            }
        }
    }

    public void getResponse(JSONObject outPut, int i) {
        content.setVisibility(VISIBLE);
//        Toast.makeText(context, outPut+"", Toast.LENGTH_SHORT).show();
        Log.e("response", "" + outPut);
        System.out.println("Post Detail Response  "+outPut.toString());
        try {
            if (i == 2 || i == 5) { // for send message
                Log.e("output", "" + outPut);
                llCommentsLoading.setVisibility(GONE);
                if (i == 5) {
                    group_post_comments.remove(group_post_comments.size() - 1); // removing of loading item
                    commentListRecyclerViewAdapter.notifyItemRemoved(group_post_comments.size());
                }
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                    } else if (outPut.has(ResponseParameters.GROUP_POSTS_COMMENTS)) {
                        Gson gson = new Gson(); // creates a Gson instance
                        Type listType = new TypeToken<List<PostComments>>() {
                        }.getType();
                        List<PostComments> updatedComments = gson.fromJson(outPut.getString(ResponseParameters.GROUP_POSTS_COMMENTS), listType);
                        group_post_comments.addAll(updatedComments);
                        if (updatedComments.size() > 0) {
                            if (i == 5) // for updating
                                commentsPagination = commentsPagination + 10;
                            //updating recycler view
                            if (commentListRecyclerViewAdapter != null) {
                                commentListRecyclerViewAdapter.notifyItemRangeInserted(group_post_comments.size() - updatedComments.size(), updatedComments.size());
                                commentListRecyclerViewAdapter.notifyItemRangeChanged(0, group_post_comments.size());
                            }
                            //commentListRecyclerViewAdapter.notifyDataSetChanged();
                            commentListRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            } else if (i == 10) {
                Log.e("output", "" + outPut);
                if (outPut.toString().contains("success")) {
                    if (outPut.has("group_posts")) {
                        JSONArray group_posts = outPut.getJSONArray("group_posts");
                        JSONObject jsonObject = group_posts.getJSONObject(0);
                        post = new Gson().fromJson(jsonObject + "", Post.class);
                        post.self_like = jsonObject.getInt("self_like");
                        groupImageBaseUrl = outPut.getString("group_img_base_url");
                        if (outPut.has("groupInfo")) {
                            JSONObject groupInfo = outPut.getJSONObject("groupInfo");
                            group = new Gson().fromJson(groupInfo + "", Group.class);
                        }
                        showView();
                        initForDeepLink();
                    }
                } else {
                    content.setVisibility(VISIBLE);
                    Utils.showPopup(context, "Post Deleted", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                }
            }

        } catch (Exception e) {
            //mo user found
            e.printStackTrace();
            showPopup(PostDetailsActivity.this, e.getMessage());
        }
    }

    private void setUpJitSiMeet() {

        // Initialize default options for Jitsi Meet conferences.
        URL serverURL;
        try {
            serverURL = new URL("https://meet.jit.si");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("Invalid server URL!");
        }
        JitsiMeetConferenceOptions defaultOptions
                = new JitsiMeetConferenceOptions.Builder()
                .setServerURL(serverURL)
                .setWelcomePageEnabled(false)
                .build();
        JitsiMeet.setDefaultConferenceOptions(defaultOptions);
    }

}
