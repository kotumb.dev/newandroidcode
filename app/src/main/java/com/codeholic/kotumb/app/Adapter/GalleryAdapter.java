package com.codeholic.kotumb.app.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import com.codeholic.kotumb.app.Model.GalleryData;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Utils;
import java.util.List;


public class GalleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<GalleryData> galleryData;
    Context activity;
    ProgressDialog progressDialog;

    public GalleryAdapter(Context activity, List<GalleryData> galleryData) {
        this.galleryData = galleryData;
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(activity).inflate(R.layout.gallery_item,parent,false);
        GalleryViewHolder holder=new GalleryViewHolder(view);
        return holder;
    }



    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final GalleryData data=galleryData.get(position);
        try{
            if (viewHolder instanceof  GalleryViewHolder){
                GalleryViewHolder holder=(GalleryViewHolder)viewHolder;
                progressDialog = new ProgressDialog(activity);
                progressDialog.setMessage(activity.getResources().getString(R.string.loading_data_text));
                progressDialog.setCancelable(false);
                if (Utils.getExtension(data.getGalleryMedia()).equalsIgnoreCase("pdf")){
                    holder.webView.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.pdf_notification
                    ));
                }else{
                    holder.webView.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video_notification));
                }
                System.out.println("Gallery URL    "+API.LOAD_GALLERY+data.getGalleryMedia());

                holder.viewMedia.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Utils.getExtension(data.getGalleryMedia()).equalsIgnoreCase("pdf")){
//                            String pdfURL = "http://docs.google.com/gview?embedded=true&url=" + API.gallary_media+data.getGalleryMedia();
//                            videoLoad(pdfURL,data);
                            showFullPDF(data);
                        }else{
                            String videoURL = "<html><head><title>Sample</title></head><body><video controls autoplay width=100% height=100% ><source src='"+API.gallary_media+data.getGalleryMedia()+"'></video></body></html>";
                            videoLoad(videoURL,data);
                        }
                    }
                });
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }


    public void showFullPDF(GalleryData galleryData){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + API.gallary_media+galleryData.getGalleryMedia()), "text/html");
        activity.startActivity(intent);
    }




    private void videoLoad(String url,GalleryData galleryData){
        androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(activity);
        final View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.gallery_video_popup, null);
        dialogBuilder.setView(dialogView);
        final WebView webView=dialogView.findViewById(R.id.video_load);
        final TextView close = dialogView.findViewById(R.id.video_close_btn);
        System.out.println("Video URL  "+API.gallary_media+galleryData.getGalleryMedia());
        getData(galleryData,webView,url);
        final AlertDialog confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });
    }

    private void getData(final GalleryData data,WebView webview,String url){
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setLoadsImagesAutomatically(true);
        webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        webview.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.requestFocus();
        if (Utils.getExtension(data.getGalleryMedia()).equalsIgnoreCase("pdf")){
            webview.loadUrl(url);
        }else{
            webview.loadData(url, "text/html", "UTF-8");
        }
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (Utils.getExtension(data.getGalleryMedia()).equalsIgnoreCase("pdf")){
                    view.loadUrl(url);
                }else{
                    view.loadData(url, "text/html", "UTF-8");
                }
                return true;
            }
        });
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100) {
                    progressDialog.show();
                }
                if (progress==100) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        int arr=0;
        try{
            if (galleryData.size()==0){
                arr=0;
            }else {
                arr=galleryData.size();
            }
        }catch (Exception e){}
        return arr;
    }




    public static class GalleryViewHolder extends RecyclerView.ViewHolder {
        ImageView webView;
        TextView viewMedia;
        GalleryViewHolder(View view) {
            super(view);
            webView = view.findViewById(R.id.gallery_item_load);
            viewMedia=view.findViewById(R.id.view_media);
        }


    }
}
