package com.codeholic.kotumb.app.Activity;

import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.codeholic.kotumb.app.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;

import org.json.JSONObject;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.SharingHelper;
import io.branch.referral.util.CommerceEvent;
import io.branch.referral.util.CurrencyType;
import io.branch.referral.util.LinkProperties;
import io.branch.referral.util.Product;
import io.branch.referral.util.ProductCategory;
import io.branch.referral.util.ShareSheetStyle;
import io.branch.referral.validators.IntegrationValidator;

public class BranchLink extends AppCompatActivity {

    TextView appLink,shareLink;
    final String appPackageName ="com.codeholic.kotumb.app";
    String message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_link);
        message = "https://play.google.com/store/apps/details?id=" + appPackageName;
        appLink=findViewById(R.id.app_link);
        appLink.setText(message);
        shareLink=findViewById(R.id.share_link);
        Branch.enableDebugMode();
        Branch.getAutoInstance(this);
        try {
            ProviderInstaller.installIfNeeded(BranchLink.this);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

        shareBranch();
    }
    @Override
    protected void onStart() {
        super.onStart();
        IntegrationValidator.validate(this);
        // Branch init
        Branch branch = Branch.getInstance(getApplicationContext());
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    System.out.println("BRANCH SDK1   "+referringParams.toString());
//                    Toast.makeText(BranchLink.this, referringParams.toString(), Toast.LENGTH_SHORT).show();
                } else {
                    System.out.println("BRANCH SDK2   "+error.toString());
//                    Toast.makeText(BranchLink.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }, this.getIntent().getData(), this);
//



//        JSONObject sessionParams = Branch.getInstance().getLatestReferringParams();
//
//// first
//        JSONObject installParams = Branch.getInstance().getFirstReferringParams();

//        System.out.println("Branch JSON Result   "+sessionParams.toString()+ "    "+installParams.toString());
//        new BranchEvent(BRANCH_STANDARD_EVENT.VIEW_ITEM).addContentItems(buo).logEvent(this);
//        new BranchEvent(BRANCH_STANDARD_EVENT.PURCHASE)
//                .addCustomDataProperty("MonsterName","Arpit")
//                .setCurrency(CurrencyType.USD)
//                .setDescription("Arpit")
//                .addContentItems(buo)
//                .logEvent(BranchLink.this);
//
//        Branch.getInstance().loadRewards(new Branch.BranchReferralStateChangedListener() {
//            @Override
//            public void onStateChanged(boolean changed, BranchError error) {
//                System.out.println("State Change    "+error.toString());
//                Toast.makeText(BranchLink.this, "Branch State Change   "+String.valueOf(error), Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        Branch.getInstance().getCreditHistory(new Branch.BranchListResponseListener() {
//            @Override
//            public void onReceivingResponse(JSONArray list, BranchError error) {
//                System.out.println("Branch Response   "+error.toString()+"     "+list.toString());
//            }
//        });




//        Intent resultIntent = new Intent(this, TestTargetClass.class);
//        resultIntent.putExtra("branch","http://xxxx.app.link/testlink");
//        resultIntent.putExtra("branch_force_new_session",true);
//        PendingIntent resultPendingIntent =  PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        try {
//            resultPendingIntent.send();
////            startActivity(resultIntent);
//            Toast.makeText(this, "Pending", Toast.LENGTH_SHORT).show();
//        } catch (PendingIntent.CanceledException e) {
//            e.printStackTrace();
//            System.out.println("Pending Response    "+e.toString());
//            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
//        }
    }
    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }


    public void shareBranch(){
        shareLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BranchUniversalObject buo = new BranchUniversalObject()
                        .setCanonicalIdentifier("content/12345")
                        .setTitle("My Content Title")
                        .setContentDescription("My Content Description")
                        .setContentImageUrl("https://lorempixel.com/400/400")
                        .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                        .setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC);
//                        .setContentMetadata(new ContentMetadata().addCustomMetadata("key1", "value1"));
                LinkProperties lp = new LinkProperties()
                        .addTag("myMonsterTag1")
                        //.setAlias("myCustomMonsterLink") // In case you need to white label your link
                        .setFeature("myMonsterSharefeature1")
                        .setStage("1")
                        .addControlParameter("$android_deeplink_path", "monster/view/");
//                LinkProperties lp = new LinkProperties()
//                        .setChannel("facebook")
//                        .setFeature("sharing")
//                        .setCampaign("content 123 launch")
//                        .setStage("new user")
//                        .addControlParameter("$desktop_url", "http://example.com/home")
//                        .addControlParameter("custom", "data")
//                        .addControlParameter("custom_random", Long.toString(Calendar.getInstance().getTimeInMillis()));
                buo.generateShortUrl(BranchLink.this, lp, new Branch.BranchLinkCreateListener() {
                    @Override
                    public void onLinkCreate(String url, BranchError error) {
                        if (error == null) {
                            System.out.println("BRANCH SDK3  "+"  got my Branch link to share" + url);
                            Toast.makeText(BranchLink.this,url, Toast.LENGTH_SHORT).show();
                        }else{
                            System.out.println("BRANCH SDK3   "+ error.toString());
                            Toast.makeText(BranchLink.this, error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                ShareSheetStyle ss = new ShareSheetStyle(BranchLink.this, "Check this out!", "This stuff is awesome: ")
                        .setCopyUrlStyle(ContextCompat.getDrawable(BranchLink.this, android.R.drawable.ic_menu_send), "Copy To Clipboard", message)
                        .setMoreOptionStyle(ContextCompat.getDrawable(BranchLink.this, android.R.drawable.ic_menu_search), "Show more")
                        .addPreferredSharingOption(SharingHelper.SHARE_WITH.FACEBOOK)
                        .addPreferredSharingOption(SharingHelper.SHARE_WITH.EMAIL)
                        .addPreferredSharingOption(SharingHelper.SHARE_WITH.MESSAGE)
                        .addPreferredSharingOption(SharingHelper.SHARE_WITH.HANGOUT)
                        .setAsFullWidthStyle(true)
                        .setSharingTitle("Share With");

                buo.showShareSheet(BranchLink.this, lp,  ss,  new Branch.BranchLinkShareListener() {
                    @Override
                    public void onShareLinkDialogLaunched() {
                    }
                    @Override
                    public void onShareLinkDialogDismissed() {

                    }
                    @Override
                    public void onLinkShareResponse(String sharedLink, String sharedChannel, BranchError error) {

                    }
                    @Override
                    public void onChannelSelected(String channelName) {
                        Product branchster = new Product();
                        branchster.setSku("Kotumb");
                        branchster.setPrice((double) 100);
                        branchster.setQuantity(1);
                        branchster.setVariant("X-Tra Hairy");
                        branchster.setBrand("Branch");
                        branchster.setCategory(ProductCategory.ANIMALS_AND_PET_SUPPLIES);
                        branchster.setName("Kotumb");
                        CommerceEvent commerceEvent = new CommerceEvent();
                        commerceEvent.setRevenue((double) 100);
                        commerceEvent.setCurrencyType(CurrencyType.USD);
                        commerceEvent.addProduct(branchster);
                        Branch.getInstance().sendCommerceEvent(commerceEvent, null, null);
                    }
                }, new Branch.IChannelProperties() {
                    @Override
                    public String getSharingTitleForChannel(String channel) {
                        return channel.contains("Messaging") ? "title for SMS" :
                                channel.contains("Slack") ? "title for slack" :
                                        channel.contains("Gmail") ? "title for gmail" : null;
                    }

                    @Override
                    public String getSharingMessageForChannel(String channel) {
                        return channel.contains("Messaging") ? "message for SMS" :
                                channel.contains("Slack") ? "message for slack" :
                                        channel.contains("Gmail") ? "message for gmail" : null;
                    }
                });
            }
        });
    }




}





