package com.codeholic.kotumb.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codeholic.kotumb.app.Model.RewardObject;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.DateUtils;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

import static com.codeholic.kotumb.app.Utility.ResponseParameters.CURRENT_REWARD;
import static com.codeholic.kotumb.app.Utility.ResponseParameters.REDEEM_REWARD;

public class CurrentRewardFragment extends Fragment {

    RecyclerView rewardRCView;
    private RewardListAdapter rewardListAdapter;
    Activity activity;
    ArrayList<RewardObject> currentRewardList = new ArrayList<>();
    RelativeLayout loadingView;
    TextView nodataText;
    int points = 0;



    public static int mTotalRedeemCount;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reward, container, false);
        ButterKnife.bind(activity);
        rewardRCView = view.findViewById(R.id.fragment_list_rv);
        loadingView = view.findViewById(R.id.loadingLayout);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        rewardRCView.setLayoutManager(linearLayoutManager);
        rewardRCView.setHasFixedSize(true);
        rewardListAdapter = new RewardListAdapter(new ArrayList<>(),"Current");
        rewardRCView.setAdapter(rewardListAdapter);




        getCurrentReward();
        return view;
    }

    private void getCurrentReward(){
        loadingView.setVisibility(View.VISIBLE);
        API.sendRequestToServerGET_FRAGMENT(activity,this,API.REDEEEM_POINTS + "/"+ SharedPreferencesMethod.getUserId(activity)+"/current",API.REDEEEM_POINTS);
    }

    public void getResponse(JSONObject outPut){
        loadingView.setVisibility(View.GONE);
        try {
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Errors)) {
//                    final Snackbar snackbar = Snackbar.make(getView(), Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
//                    snackbar.show();
                    nodataText.setVisibility(View.VISIBLE);
                } else {
                    parseResponse(outPut);
                }
            } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                nodataText.setVisibility(View.VISIBLE);
//                final Snackbar snackbar = Snackbar.make(getView(), getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
//                snackbar.show();
            } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                final Snackbar snackbar = Snackbar.make(getView(), getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
            }

        } catch (Exception e) {
            Log.e("REWARD","Exception is : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void parseResponse(JSONObject jsonObject){
        try {
            if (jsonObject.has(CURRENT_REWARD)){
                if (currentRewardList.size()!=0){
                    currentRewardList.clear();
                }

                JSONArray parent = jsonObject.getJSONArray(CURRENT_REWARD);
                for (int i=0;i<parent.length();i++){
                    JSONObject child = parent.getJSONObject(i);
                    RewardObject rewardObject = new RewardObject(child.getString(ResponseParameters.POINTS),child.getString(ResponseParameters.REWARDTYPE),child.getString(ResponseParameters.CREATED_AT));
                    currentRewardList.add(rewardObject);
                    rewardListAdapter.addItem(currentRewardList);
                    rewardListAdapter.notifyDataSetChanged();
                }

                //get redeem Data
                API.sendRequestToServerGET_FRAGMENT(activity,this,API.REDEEEM_POINTS + "/"+ SharedPreferencesMethod.getUserId(activity)+"/redeem",API.REDEEEM_POINTS);


               // int points = Integer.parseInt(jsonObject.getString(ResponseParameters.POINTS));

            } else if (jsonObject.has(REDEEM_REWARD)){


                JSONArray parent = jsonObject.getJSONArray(REDEEM_REWARD);
                for (int i=0;i<parent.length();i++){
                    JSONObject child = parent.getJSONObject(i);
                    RewardObject rewardObject = new RewardObject(child.getString(ResponseParameters.POINTS),child.getString(ResponseParameters.REWARDTYPE),child.getString(ResponseParameters.CREATED_AT));

                    points = points + Integer.parseInt(rewardObject.getPoints());

                }

                SharedPreferencesMethod.setInt(activity,"REDEEM_TOTAL",points);




            }else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public static class RewardListAdapter extends RecyclerView.Adapter<RewardListAdapter.MyViewHolder> {
        List<String> mListData;
        public List<RewardObject> rewardObjectList;
        private String listCategry;

        public RewardListAdapter(List<RewardObject> rewardObjectList, String current) {
            this.rewardObjectList = rewardObjectList;
            this.listCategry = current;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.reward_list_item,
                    viewGroup, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyViewHolder myViewHolder, int i) {

            String mDateTime = null;
            try {
                //2020-03-26 15:55:50
                mDateTime = DateUtils.formatDateFromDateString(DateUtils.DATE_FORMAT_8, DateUtils.DATE_FORMAT_5+" "+"hh:mm a", rewardObjectList.get(i).getCreatedAt());
                Log.e("REWARD","Time & date is : " + mDateTime);
                myViewHolder.createdText.setText("Created At : "+mDateTime);
                myViewHolder.updateText.setText("Updated At : "+mDateTime);
            } catch (ParseException e) {
                Log.e("REWARD","Time & date exception is : " + e.getMessage());
                e.printStackTrace();
            }


            if (!rewardObjectList.get(i).getRewardTypeText().isEmpty()){
                String rewardType =  rewardObjectList.get(i).getRewardTypeText().replaceAll("_"," ");
                rewardType = rewardType.substring(0, 1).toUpperCase() + rewardType.substring(1).toLowerCase();
                myViewHolder.typeText.setText(rewardType);
            }

            if (listCategry.equals("Redeem")) {
                mTotalRedeemCount = mTotalRedeemCount + Integer.valueOf(rewardObjectList.get(i).getPoints());
            }


           myViewHolder.pointsText.setText(rewardObjectList.get(i).getPoints() + "\n" + "Points");
//           myViewHolder.createdText.setText("Created At : "+rewardObjectList.get(i).getCreatedAt());
//           myViewHolder.updateText.setText("Updated At : "+rewardObjectList.get(i).getCreatedAt());

        }

        @Override
        public int getItemCount() {
            return rewardObjectList == null ? 0 : rewardObjectList.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView typeText,pointsText,updateText,createdText;
            public MyViewHolder(View itemView) {
                super(itemView);
                typeText = itemView.findViewById(R.id.rewrdTypeText);
                pointsText = itemView.findViewById(R.id.pointsText);
                updateText = itemView.findViewById(R.id.updatedText);
                createdText = itemView.findViewById(R.id.createdText);
            }
        }

        public void addItem(ArrayList<RewardObject> rewardObjects){
            this.rewardObjectList = rewardObjects;
        }
    }


}
