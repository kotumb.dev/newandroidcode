package com.codeholic.kotumb.app.fragments;

import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Adapter.LinksRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Link;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;

public class LinksFragment extends BaseFragment {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.tvLinks)
    TextView tvLinks;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.adView)
    ImageView adView;
    @BindView(R.id.rlAdView)
    RelativeLayout rlAdView;

    User user;
    int pagination = 0;
    List<Link> links;
    private LinearLayoutManager linearLayoutManager;
    LinksRecyclerViewAdapter linksRecyclerViewAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.activity_links, container, false);
        ButterKnife.bind(this, view);
        ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.gov_link_title));
        init();
        setView();
        return view;
    }


    private void init() {
        user = SharedPreferencesMethod.getUserInfo(getActivity());
    }


    //setview for getting user connection
    private void setView() {
        try {
            if (!Utility.isConnectingToInternet(getActivity())) { // checking net connection
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(View.VISIBLE);
                tvLinks.setText(getResources().getString(R.string.app_no_internet_error));
                final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            tvLinks.setText(getResources().getString(R.string.app_no_link_found));
            API.sendRequestToServerGET_FRAGMENT(getActivity(), this, API.GET_RSS_LIST + (pagination), API.GET_RSS_LIST);// service call for getting GOV links
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //getResponse for getting list of links
    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        Log.e("response", "" + outPut);
        try {
            if (i == 0) { // for getting links
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response

                        if (outPut.has(ResponseParameters.RSSFEEDS)) {
                            JSONObject rssFeeds = outPut.getJSONObject(ResponseParameters.RSSFEEDS);
                            JSONArray userList = rssFeeds.getJSONArray(ResponseParameters.ITEMS);
                            links = new ArrayList<>();
                            for (int j = 0; j < userList.length(); j++) {
                                JSONObject searchResults = userList.getJSONObject(j);
                                Link link = new Link();
                                //link.setLinkId(searchResults.getString(ResponseParameters.Id));
                                link.setLink(searchResults.getString(ResponseParameters.LINK));
                                link.setLinkName(searchResults.getString(ResponseParameters.TITLE));
                                links.add(link);
                            }
                            //setting options for recycler adapter
                            linearLayoutManager = new LinearLayoutManager(getActivity());
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rvUserList.setLayoutManager(linearLayoutManager);
                            linksRecyclerViewAdapter = new LinksRecyclerViewAdapter(rvUserList, getActivity(), links);
                            rvUserList.setAdapter(linksRecyclerViewAdapter);
                            //load more setup for recycler adapter
                            linksRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                @Override
                                public void onLoadMore() {
                                    //if (links.size() >= 6) {
                                    rvUserList.post(new Runnable() {
                                        public void run() {
                                            links.add(null);
                                            linksRecyclerViewAdapter.notifyItemInserted(links.size() - 1);
                                        }
                                    });
                                    String url = API.GET_RSS_LIST + (pagination + 1) + "/?userid=" + user.getUserId();
                                    API.sendRequestToServerGET_FRAGMENT(getActivity(), LinksFragment.this, url, API.GET_RSS_LIST_UPDATE);// service call for getting GOV links
                                    // }
                                }
                            });
                        } else {
                            //no userfound
                            llNotFound.setVisibility(View.VISIBLE);
                        }
                    } else {
                        //no userfound
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } else if (i == 1) {// for updating list
                links.remove(links.size() - 1); // removing of loading item
                linksRecyclerViewAdapter.notifyItemRemoved(links.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response

                        if (outPut.has(ResponseParameters.RSSFEEDS)) {
                            JSONObject rssFeeds = outPut.getJSONObject(ResponseParameters.RSSFEEDS);
                            JSONArray userList = rssFeeds.getJSONArray(ResponseParameters.ITEMS);
                            for (int j = 0; j < userList.length(); j++) {
                                JSONObject searchResults = userList.getJSONObject(j);
                                Link link = new Link();
                                //link.setLinkId(searchResults.getString(ResponseParameters.Id));
                                link.setLink(searchResults.getString(ResponseParameters.LINK));
                                link.setLinkName(searchResults.getString(ResponseParameters.TITLE));
                                links.add(link);
                            }
                            if (userList.length() > 0) {
                                pagination = pagination + 1;
                                //updating recycler view
                                linksRecyclerViewAdapter.notifyDataSetChanged();
                                linksRecyclerViewAdapter.setLoaded();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            //mo user found
            llNotFound.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }

}
