package com.codeholic.kotumb.app.Utility;

public class EventNames {

    /**
     * Home page search
     * user profile view
     * User send message
     * Send connect req
     * Accept connect req
     * App Invite people
     * Create Group
     * Group invite send
     * Group invite accept
     * Join request
     * Group post
     * Group post like
     * Group post comment
     * Skill dev search
     * Skill Dev profile view
     * Skill comment
     * Skill like
     * give Recommendation
     * Share resume
     * Survey given
     * Ad click
     */

    public static final String HOME_PAGE_SEARCH = "Home_page_search";
    public static final String USER_PROFILE_VIEW = "User_profile_view";
    public static final String USER_SEND_MESSAGE = "User_send_message";
    public static final String SEND_CONNECT_REQ = "Send_connect_req";
    public static final String ACCEPT_CONNECT_REQ = "Accept_connect_req";
    public static final String APP_INVITE_PEOPLE = "App_Invite_people";
    public static final String CREATE_GROUP = "Create_Group";
    public static final String GROUP_INVITE_SEND = "Group_invite_send";
    public static final String GROUP_INVITE_ACCEPT = "Group_invite_accept";
    public static final String GROUP_JOIN_REQUEST = "Join_request";
    public static final String GROUP_POST = "Group_post";
    public static final String GROUP_POST_LIKE = "Group_post_like";
    public static final String GROUP_POST_COMMENT = "Group_post_comment";
    public static final String SKILL_DEV_SEARCH = "Skill_dev_search";
    public static final String SKILL_DEV_PROFILE_VIEW = "Skill_Dev_profile_view";
    public static final String SKILL_COMMENT = "Skill_comment";
    public static final String SKILL_LIKE = "Skill_like";
    public static final String GIVE_RECOMMENDATION = "Give_Recommendation";
    public static final String SHARE_RESUME = "Share_resume";
    public static final String SURVEY_GIVEN = "Survey_given";
    public static final String AD_CLICK = "Ad_click";


}
