package com.codeholic.kotumb.app.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DELL on 11/1/2017.
 */

public class ReportedUser implements Serializable {

    @SerializedName("id")
    public String id = "";

    @SerializedName("group_id")
    public String group_id = "";

    @SerializedName("fromUserId")
    public String fromUserId = "";

    @SerializedName("toUserId")
    public String toUserId = "";

    @SerializedName("reason")
    public String reason = "";

    @SerializedName("createdAt")
    public String createdAt = "";

    @SerializedName("updatedAt")
    public String updatedAt = "";

    @SerializedName("fromName")
    public String fromName = "";

    @SerializedName("fromAvatar")
    public String fromAvatar = "";

    @SerializedName("toName")
    public String toName = "";

    @SerializedName("toAvatar")
    public String toAvatar = "";

}
