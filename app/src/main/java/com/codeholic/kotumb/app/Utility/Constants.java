package com.codeholic.kotumb.app.Utility;

import com.codeholic.kotumb.app.Model.Education;
import com.codeholic.kotumb.app.Model.Note;
import com.codeholic.kotumb.app.Model.Profession;
import com.codeholic.kotumb.app.Model.User;

/**
 * Created by DELL on 11/22/2017.
 */

public class Constants {
    public static boolean fromProfile = false;
    public static boolean completeProfileActivity = false;
    public static Education education = null;
    public static Profession profession = null;
    public static int education_position = -1;
    public static int profession_position = -1;
    public static Boolean showPopUpOnHome = true;
    public static Boolean showPopUpOnHomeProfile = true;
    public static Boolean showPopUpVerCheck = true;
    public static int REMINDER = -1;
    public static Note note = null;
    public static User openChatUser = new User();
}
