package com.codeholic.kotumb.app.fragments;

import android.animation.Animator;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;

import com.codeholic.kotumb.app.Activity.InvitationDetails;
import com.codeholic.kotumb.app.Activity.RewardsActivity;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.CompleteProfileActivity;
import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Activity.ResumeTemplateActivity;
import com.codeholic.kotumb.app.Activity.ShareResumeActivity;
import com.codeholic.kotumb.app.Activity.WhoViewActivity;
import com.codeholic.kotumb.app.Adapter.MyListAdapter;
import com.codeholic.kotumb.app.Interface.OnGetViewListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.codeholic.kotumb.app.Utility.ResponseParameters.SUMMATION_OF_POINT_EXPIRE;


public class MeFragment extends BaseFragment implements View.OnClickListener {

    public static final int DELAY_MILLIS = 500;
    @BindView(R.id.ll_personal_details)
    LinearLayout llPersonalDetails;

    @BindView(R.id.container)
    LinearLayout container;

    @BindView(R.id.ll_organisation)
    LinearLayout llOrganisation;

    @BindView(R.id.ll_skill)
    LinearLayout llSkill;

    @BindView(R.id.ll_work)
    LinearLayout llWork;

    @BindView(R.id.ll_education)
    LinearLayout llEducation;

    @BindView(R.id.ll_login)
    LinearLayout llLogin;

    @BindView(R.id.ll_aadhar)
    LinearLayout llAadhar;

    @BindView(R.id.ll_recom)
    LinearLayout llRecomm;

    @BindView(R.id.tvUserName)
    TextView tvUserName;

    @BindView(R.id.tvCity)
    TextView tvCity;

    @BindView(R.id.info)
    ImageView info;

    @BindView(R.id.tvRewardPoints)
    TextView tvRewardPoints;

    @BindView(R.id.loadingLayout)
    RelativeLayout loadingLayout;

    @BindView(R.id.tvAdharVerified)
    TextView tvAdharVerified;

    @BindView(R.id.tvConnectionCount)
    TextView tvConnectionCount;

    @BindView(R.id.tvProfileComplete)
    TextView tvProfileComplete;

    @BindView(R.id.link_text)
    TextView link_text;

    @BindView(R.id.llProfileLink)
    RelativeLayout llProfileLink;

    @BindView(R.id.copy)
    ImageView copy;

    @BindView(R.id.viewOne)
    View viewOne;

    @BindView(R.id.viewTwo)
    View viewTwo;

    @BindView(R.id.viewThree)
    View viewThree;

    @BindView(R.id.llAwards_points)
    View llAwards_points;

    @BindView(R.id.viewFour)
    View viewFour;

    @BindView(R.id.ivUser)
    ImageView ivUser;

    @BindView(R.id.rbPersonalDetails)
    RadioButton rbPersonalDetails;

    @BindView(R.id.rbOrganisationDetails)
    RadioButton rbOrganisationDetails;

    @BindView(R.id.rbSkillDetails)
    RadioButton rbSkillDetails;

    @BindView(R.id.rbEducationDetail)
    RadioButton rbEducationDetail;

    @BindView(R.id.rbAadharDetail)
    RadioButton rbAadharDetail;

    @BindView(R.id.rbRecom)
    RadioButton rbRecom;

    @BindView(R.id.llViewResume)
    LinearLayout llViewResume;

    @BindView(R.id.llShareResume)
    LinearLayout llShareResume;

    @BindView(R.id.llWSearched)
    LinearLayout llWSearched;

    @BindView(R.id.llWViewed)
    LinearLayout llWViewed;

    @BindView(R.id.tvConnections)
    TextView tvConnections;

    @BindView(R.id.av_loader)
    LottieAnimationView lottieAnimationView;

    @BindView(R.id.inviteLayout)
    CardView inviteLayout;

    //redeem view
    @BindView(R.id.cv_redeem)
    CardView redeemView;

    @BindView(R.id.tv_point_expire)
    TextView tv_point_expire;

    @BindView(R.id.expire_layout)
    LinearLayout expire_layout;

    @BindView(R.id.tv_userType)
    TextView tv_userType;

    TextInputLayout redeemLayout;

    JSONObject profile = new JSONObject();
    JSONArray recommedations = new JSONArray();
    boolean flag;
    User user;
    String minRedeem, expDate, paisePoint;
    BaseFragment fragment;
    Dialog dialog;
    Locale locale;
    Configuration config = new Configuration();
    PercentageListener percentageListener;
    private Handler handler;
    private Integer profilePercentage = 0;
    private Activity activity;
    private String languageToLoad = "";
    private int mSuperStarPoint, mRockStarPoint, mPoweStarpoint;

    private Boolean is75ProfileComplete;

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        fragment = this;
        dialog = new Dialog(activity);

        try {
            percentageListener = (PercentageListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }


//        FacebookSdk.setApplicationId(getResources().getString(R.string.facebook_app_id));
//        FacebookSdk.sdkInitialize(activity.getApplication());
//        AppEventsLogger.activateApp(activity.getApplication());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // FacebookSdk.sdkInitialize(activity);

        languageToLoad = SharedPreferencesMethod.getDefaultLanguage(activity);
        changeLocale(languageToLoad);

        is75ProfileComplete = SharedPreferencesMethod.getBoolean(activity, "Profile_75");

        user = SharedPreferencesMethod.getUserInfo(getActivity());
        flag = true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.me_tab, container, false);
        ButterKnife.bind(this, view);

        ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.app_my_profile_text));


        setView();
        setClickListener();

        if (is75ProfileComplete) {

            ApiCallForReward();

        }

        ApiGetExpiredSummation();


//        enableInstantUpdate();

        return view;
    }

    private void enableInstantUpdate() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvRewardPoints.setText(Utils.rewardPoints + "");
                    }
                });
                handler.postDelayed(this, DELAY_MILLIS);
            }
        }, DELAY_MILLIS);
    }


    private void setView() {

        tvUserName.setText(user.getFirstName() + " " + user.getLastName());
        tvCity.setText(user.getCity() + " " + user.getState());
        link_text.setText("");
        llProfileLink.setVisibility(View.VISIBLE);
        System.out.println("Image Path  " + user.getAvatar());
        //Code Write By Arpit Kanda
        String userId = SharedPreferencesMethod.getUserId(getActivity());
        if(userId.equalsIgnoreCase("868")){
            Log.e("xyz", "setView: visible "+userId );
            redeemView.setVisibility(VISIBLE);
        }else{
            Log.e("xyz", "setView: not visible "+userId );
            redeemView.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        new AQuery(getActivity()).id(ivUser).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.default_profile);

//        String name=API.imageUrl+user.getAvatar();
//
//        if (name.equalsIgnoreCase(API.imageUrl+"default_profile.png")){
//              System.out.println("Images  "+name);
//        }else{
//            new AQuery(getActivity()).id(ivUser).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.ic_user);
//            System.out.println("Images User  "+user.getAvatar());
//        }

        ApiCall();


    }


    private void ApiCall() {
        try {
            if (!Utility.isConnectingToInternet(getActivity())) {
                final Snackbar snackbar = Snackbar.make(llAadhar, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            /*snackbar.setAction(getResources().getString(R.string.cancel), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    snackbar.dismiss();
                                }
                            }).show();*/
                snackbar.show();
                return;
            }
            HashMap<String, Object> input = new HashMap<>();
            input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(getActivity()));
            input.put(RequestParameters.VIEWERID, "" + SharedPreferencesMethod.getUserId(getActivity()));
            Log.e("input", "" + input);
//            String values= SharedPreferencesMethod.getString(getContext(),RequestParameters.AVATAR);
//            String value_profile= SharedPreferencesMethod.getString(getContext(),ResponseParameters.PROFILE);
//            Toast.makeText(getContext(), values, Toast.LENGTH_SHORT).show();
            API.sendRequestToServerPOST_PARAM_FRAGMENT(getActivity(), this, API.PROFILE, input);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ApiCallForReward() {
        try {
            if (!Utility.isConnectingToInternet(getActivity())) {
                final Snackbar snackbar = Snackbar.make(llAadhar, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            /*snackbar.setAction(getResources().getString(R.string.cancel), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    snackbar.dismiss();
                                }
                            }).show();*/
                snackbar.show();
                return;
            }
            HashMap<String, Object> input = new HashMap<>();
            input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(getActivity()));
            Log.e("input", "" + input);
//            String values= SharedPreferencesMethod.getString(getContext(),RequestParameters.AVATAR);
//            String value_profile= SharedPreferencesMethod.getString(getContext(),ResponseParameters.PROFILE);
//            Toast.makeText(getContext(), values, Toast.LENGTH_SHORT).show();
            API.sendRequestToServerPOST_PARAM_FRAGMENT(getActivity(), this, API.USER_REFER_REWARD, input);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //This Method is Created By Arpit Kanda

    // For Getting Birthday Bash on Birthday

    public void birthdayCeleb() {
        final User user = SharedPreferencesMethod.getUserInfo(getContext());
        final String DOB = user.getDOB().substring(5);
        final String userId = user.getUserId();
        final String url = API.NOTIFICATION + userId;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //System.out.println("Get Response   " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = new JSONArray(jsonObject.getString("notifications"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject results = jsonArray.getJSONObject(i);
                                System.out.println("Main Response  " + results.getString("activityType"));
                                if (results.getString("activityType").equalsIgnoreCase("birthday") && DOB.equalsIgnoreCase(getDateAndTime())) {
                                    lottieAnimationView.setVisibility(View.VISIBLE);
                                    lottieAnimationView.setAnimation("balloons.json");
                                    lottieAnimationView.playAnimation();
                                    lottieAnimationView.addAnimatorListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart(Animator animation) {

                                        }

                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            lottieAnimationView.cancelAnimation();
                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animation) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(Animator animation) {
                                            lottieAnimationView.cancelAnimation();

                                        }
                                    });
                                } else {

                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        Volley.newRequestQueue(getContext()).add(stringRequest);
    }

    public String getDateAndTime() {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = format.format(today);
        String DOB = dateToStr.substring(5);
        System.out.println("Current Date   " + DOB);
//        Toast.makeText(getContext(), DOB, Toast.LENGTH_SHORT).show();
        return DOB;
    }


    private void setClickListener() {
        llPersonalDetails.setOnClickListener(this);
        llOrganisation.setOnClickListener(this);
        llSkill.setOnClickListener(this);
        //llWork.setOnClickListener(this);
        llEducation.setOnClickListener(this);
        //llLogin.setOnClickListener(this);
        //llAadhar.setOnClickListener(this);
        llRecomm.setOnClickListener(this);
        llViewResume.setOnClickListener(this);
        llShareResume.setOnClickListener(this);
        llWSearched.setOnClickListener(this);
        llWViewed.setOnClickListener(this);
        tvConnections.setOnClickListener(this);
        ivUser.setOnClickListener(this);
        copy.setOnClickListener(this);
        info.setOnClickListener(this);
        llAwards_points.setOnClickListener(this);
        redeemView.setOnClickListener(this);

        inviteLayout.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Constants.fromProfile) {
            loadingLayout.setVisibility(View.VISIBLE);
            setView();
            if (!is75ProfileComplete) {

                ApiCallForReward();

            }
            Constants.fromProfile = false;
        }
        Log.e("resume", "resume");
        tvRewardPoints.setText(Utils.rewardPoints + "");


    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llAwards_points:

                Intent redeem_Intent = new Intent(getActivity(), RewardsActivity.class);
                redeem_Intent.putExtra("CURRENT_POINT",tvRewardPoints.getText().toString());
                redeem_Intent.putExtra("EXPIRE_CURRENT_MONTH",tv_point_expire.getText().toString());
                startActivity(redeem_Intent);

                //startActivity(new Intent(getActivity(), RewardHistoryActivty.class));
                break;
            case R.id.info:
                showPopupRewards();
                break;
            case R.id.llViewResume:
                getActivity().startActivity(new Intent(getActivity(), ResumeTemplateActivity.class));
                break;
            case R.id.llShareResume:
                if (!SharedPreferencesMethod.getString(getActivity(), "" + ResponseParameters.resumeid).isEmpty()
                        && !SharedPreferencesMethod.getString(getActivity(), "" + ResponseParameters.resumeid).equalsIgnoreCase("0")) {
                    Intent ResumeTemplate = new Intent(getActivity(), ShareResumeActivity.class);
                    getActivity().startActivity(ResumeTemplate);
                } else {
                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                    dialogBuilder.setTitle(getResources().getString(R.string.app_alert_text));
                    dialogBuilder.setMessage(getResources().getString(R.string.resume_choose_format));
                    dialogBuilder.setCancelable(true);
                    dialogBuilder.setPositiveButton(getResources().getString(R.string.app_select_text), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            getActivity().startActivity(new Intent(getActivity(), ResumeTemplateActivity.class));
//                            Toast.makeText(getActivity(), "ResumeAcitivity", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    });
                    dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            //pass
                            dialog.dismiss();
                        }
                    });
                    dialogBuilder.create().show();
                }
                break;
            case R.id.llWSearched:
                //getActivity().startActivity(new Intent(getActivity(), ResumeTemplateActivity.class));
                break;
            case R.id.llWViewed:
                getActivity().startActivity(new Intent(getActivity(), WhoViewActivity.class));
                break;
            case R.id.tvConnections:
                ((HomeScreenActivity) getActivity()).openConnectionsFragment();
                break;
            case R.id.ivUser: {
                Intent intent = new Intent(getActivity(), CompleteProfileActivity.class);
                intent.putExtra(ResponseParameters.Id, v.getId());
                intent.putExtra("DATA", profile.toString());
                intent.putExtra("RECOMMENDATIONS", recommedations.toString());
                getActivity().startActivity(intent);
            }
            break;
            case R.id.copy:
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                User user = SharedPreferencesMethod.getUserInfo(getActivity());
                ClipData clip = ClipData.newPlainText("label", link_text.getText().toString());
                clipboard.setPrimaryClip(clip);
                Utils.showPopup(getActivity(), "Profile Link Copied");
                break;

            case R.id.cv_redeem:
                showPopUpRedeem();
                break;

            case R.id.inviteLayout:
                activity.startActivity(new Intent(activity, InvitationDetails.class));
                break;
            default:
                Intent intent = new Intent(getActivity(), CompleteProfileActivity.class);
                intent.putExtra(ResponseParameters.Id, v.getId());
                intent.putExtra("DATA", profile.toString());
                intent.putExtra("RECOMMENDATIONS", recommedations.toString());
                getActivity().startActivity(intent);
                break;
        }
    }


    private void showPopupRewards() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(R.layout.reward_layout);
        //  builder.setPositiveButton(getString(R.string.ok), null);
        final AlertDialog dialog = builder.create();
        dialog.show();
        View decorView = dialog.getWindow().getDecorView();
        ListView listView = decorView.findViewById(R.id.listView);
        final ArrayList<JSONObject> data = new ArrayList<>();

        for (int i = 0; i < Utils.rewards.length(); i++) {
            System.out.println("Reward    " + Utils.rewards.length());
            try {
                JSONObject jsonObject = Utils.rewards.getJSONObject(i);
//                if (!jsonObject.toString().contains("Survey")) {
                data.add(jsonObject);
//                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        listView.setAdapter(new MyListAdapter(data, new OnGetViewListener() {
            @Override
            public View onGetView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.reward_item, viewGroup, false);
                TextView name = v.findViewById(R.id.name);
                TextView value = v.findViewById(R.id.value);
                try {
                    name.setText(data.get(i).getString("title"));
                    value.setText(data.get(i).getString("value") + " points");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return v;
            }
        }));
        View ok = decorView.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void showPopUpRedeem() {

        dialog.setContentView(R.layout.redeem_layout);
        TextInputEditText redeemPoints = dialog.findViewById(R.id.redeempointText);
        redeemLayout = dialog.findViewById(R.id.etRedeemTextLayout);
        Button okRedeemBtn = dialog.findViewById(R.id.okRedeemBtn);
        CardView infoLayout = dialog.findViewById(R.id.infoLayout);
        TextView minRedeemText = dialog.findViewById(R.id.minRedeemText);
        TextView slabText = dialog.findViewById(R.id.slabText);
        TextView expireText = dialog.findViewById(R.id.expirtationText);
        setTextWatcher(okRedeemBtn, redeemPoints, redeemLayout);
        dialog.findViewById(R.id.okRedeemBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, Object> input = new HashMap<>();
                input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(getActivity()));
                input.put(RequestParameters.REDEEMPOINTS, "" + redeemPoints.getText().toString());
                Log.e("input", "" + SharedPreferencesMethod.getUserId(activity));
                API.sendRequestToServerPOST_PARAM_FRAGMENT(getActivity(), fragment, API.REDEEEM_POINTS, input);
            }
        });
        dialog.findViewById(R.id.cancelRedeemBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                minRedeemText.setText("-  Minimum points to redeem : " + minRedeem);
                expireText.setText("-  Expiration of Rewards is " + expDate + " Days");
                slabText.setText("-  1 Point = " + paisePoint + " paise");
                infoLayout.setVisibility(VISIBLE);
            }
        });
        dialog.findViewById(R.id.closeBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoLayout.setVisibility(GONE);
            }
        });
        dialog.show();

    }


    private void setTextWatcher(final Button okRedeemBtn, final EditText editText, final TextInputLayout textInputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    textInputLayout.setErrorEnabled(false);
                    okRedeemBtn.setEnabled(false);
                }

                if (!s.toString().isEmpty()) {
                    String value = s.toString();
                    Log.e("PROFILE", "Value is :" + value);

                    if (value.length() > 0 && !minRedeem.isEmpty()) {
                        if (Integer.parseInt(value) < Integer.parseInt(minRedeem)) {
                            okRedeemBtn.setEnabled(false);
                            enableError(textInputLayout, "Less than " + minRedeem + " Points cannot be redeemed");
                        } else {
                            okRedeemBtn.setEnabled(true);
                        }
                    } else {
                        textInputLayout.setErrorEnabled(false);
                        okRedeemBtn.setEnabled(false);
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s) {
//                if (Integer.parseInt(String.valueOf(s)) > 500){
//                    enableError(textInputLayout,getResources().getString(R.string.redeem_error_text));
//                }
            }
        });
    }


    void enableError(TextInputLayout inputLayout, String error) {
        if (inputLayout != null || !error.isEmpty()) {
            inputLayout.setErrorEnabled(true);
            inputLayout.setError(error);
        }

    }


    public void getResponse(JSONObject outPut, int i) {
        loadingLayout.setVisibility(View.GONE);
        if (i == 0) {
            Log.e("profile", "" + outPut);
            try {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        final Snackbar snackbar = Snackbar.make(llAadhar, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {

                        parseResponse(outPut, i);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    final Snackbar snackbar = Snackbar.make(llAadhar, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    final Snackbar snackbar = Snackbar.make(llAadhar, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            birthdayCeleb();
        } else if (i == 1) {
            if (outPut.has(ResponseParameters.Success)) {
                parseResponse(outPut, i);
            } else {
                if (outPut.has("errors")) {
                    try {
                        enableError(redeemLayout, outPut.getString("errors"));
                        Utils.showPopup(activity, getString(R.string.better_luck_next_time), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                dialog.findViewById(R.id.closeBtn).performClick();
                            }
                        });
                        //  Toast.makeText(activity,outPut.getString("errors"),Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    enableError(redeemLayout, "Oops! Something went wrong");
                }

            }
        } else if (i == 2) {
            if (outPut.has(ResponseParameters.Success)) {
                parseResponse(outPut, i);

                /*Log.e("mefragment", "getResponse: profilePercentage "+ profilePercentage );
                if (profilePercentage >= 75) {
                    SharedPreferencesMethod.setBoolean(activity, "Profile_75", true);
                }*/

            } else {
                if (outPut.has("errors")) {

                    try {
                        enableError(redeemLayout, outPut.getString("errors"));
                        //  Toast.makeText(activity,outPut.getString("errors"),Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    enableError(redeemLayout, "Oops! Something went wrong");
                }

            }
        } else if (i == 3) {
            if (outPut.has(ResponseParameters.Success)) {

                if (outPut.has(SUMMATION_OF_POINT_EXPIRE)) {


                    JSONArray parent = null;
                    try {
                        parent = outPut.getJSONArray(SUMMATION_OF_POINT_EXPIRE);
                        for (int j = 0; j < parent.length(); j++) {
                            JSONObject child = parent.getJSONObject(j);
                            tv_point_expire.setText(" = "+child.getString("points"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    // int points = Integer.parseInt(jsonObject.getString(ResponseParameters.POINTS));

                }
            } else {
                if (outPut.has("errors")) {
                    try {
//                        enableError(redeemLayout, outPut.getString("errors"));
                        if (outPut.getString("errors").contains("No"))
                        tv_point_expire.setText(" = 0");
                        //  Toast.makeText(activity,outPut.getString("errors"),Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    enableError(redeemLayout, "Oops! Something went wrong");
                }

            }
        }

    }

    private void parseResponse(JSONObject outPut, int val) {
        if (val == 1) {
            if (dialog != null) {
                dialog.dismiss();
                ApiCall();
                Toast.makeText(activity, "Your points redeemed successfully", Toast.LENGTH_SHORT).show();
                tvRewardPoints.setText(Utils.rewardPoints + "");

            }
            hideKeyboard(activity);
            Log.e("PROFILE", "parse response called" + outPut.toString());

        } else if (val == 0) {
            try {

                if (outPut.has(ResponseParameters.MIN_REDEEM_POINTS)) {
                    minRedeem = outPut.getString(ResponseParameters.MIN_REDEEM_POINTS);
                }


                if (outPut.has(ResponseParameters.EXPIREDATE)) {
                    expDate = outPut.getString(ResponseParameters.EXPIREDATE);
                }


                if (outPut.has(ResponseParameters.PAISEPERPOINT)) {
                    paisePoint = outPut.getString(ResponseParameters.PAISEPERPOINT);
                }

                if (outPut.has(ResponseParameters.PROFILE)) {
                    profile = outPut.getJSONObject(ResponseParameters.PROFILE);
                    Log.e("PROFILE", "Profile data is : " + profile.toString());

                    String profileURL1 = "profileURL";
                    System.out.println();
                    if (outPut.has(profileURL1)) {
                        String profileURL = outPut.getString(profileURL1);
                        link_text.setText(profileURL);
                        System.out.println("Output Object   " + outPut.toString());
                    }

                    if (profile.has(ResponseParameters.CONNECTIONS_COUNT)) {
                        tvConnectionCount.setText(profile.getString(ResponseParameters.CONNECTIONS_COUNT));
                    }
                    if (profile.has(ResponseParameters.Userdata)) {

                        tvProfileComplete.setText(getResources().getString(R.string.home_profile_complete_text) + " " + profile.getJSONObject(ResponseParameters.Userdata).getString(ResponseParameters.PROFILE_STRENGTH) + "%");
                        profilePercentage = Integer.parseInt(profile.getJSONObject(ResponseParameters.Userdata).getString(ResponseParameters.PROFILE_STRENGTH));
                        percentageListener.profileValue(profilePercentage);
                        int profileComplete = Integer.parseInt(profile.getJSONObject(ResponseParameters.Userdata).getString(ResponseParameters.PROFILE_STRENGTH)) / 25;
                        viewFour.setBackgroundColor(getResources().getColor(R.color.black_overlay));
                        viewThree.setBackgroundColor(getResources().getColor(R.color.black_overlay));
                        viewTwo.setBackgroundColor(getResources().getColor(R.color.black_overlay));
                        viewOne.setBackgroundColor(getResources().getColor(R.color.black_overlay));

                        Log.e("mefragment", "parseResponse: profilePercentage "+ profilePercentage );
                        if (profilePercentage >= 75) {
                            SharedPreferencesMethod.setBoolean(activity, "Profile_75", true);
                        }

                        switch (profileComplete) {
                            case 4:
                                viewFour.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            case 3:
                                viewThree.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            case 2:
                                viewTwo.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            case 1:
                                viewOne.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                break;
                        }

                    }

//                    if (profilePercentage >= 75){
//
////                    FacebookSdk.sdkInitialize(activity);
////                   // AppEventsLogger.activateApp(activity.getApplication());
//                        FacebookSdk.setIsDebugEnabled(true);
//                        FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
//                        logSendProfileEvent(outPut.toString());
//                    }

                    if (profile.has(ResponseParameters.CHECK_PERSONAL_INFO)) {
                        if (profile.getBoolean(ResponseParameters.CHECK_PERSONAL_INFO)) {
                            rbPersonalDetails.setChecked(true);
                        }
                    }

                    if (profile.has(ResponseParameters.PersonalInfo)) {
                        JSONObject PERSONALINFO = profile.getJSONObject(ResponseParameters.PersonalInfo);
                        User user = new User();
                        if (PERSONALINFO.has(ResponseParameters.resumeId)) {
                            SharedPreferencesMethod.setString(getActivity(), ResponseParameters.resumeid, PERSONALINFO.getString(ResponseParameters.resumeId));
                        }
                        JSONObject jsonObject = profile.getJSONObject(ResponseParameters.Userdata);
                        user.setUserName(jsonObject.getString("userName"));
                        user.setFirstName(PERSONALINFO.getString(ResponseParameters.FirstName));
                        user.setMiddleName(PERSONALINFO.getString(ResponseParameters.MiddleName));
                        user.setLastName(PERSONALINFO.getString(ResponseParameters.LastName));
                        user.setDOB(PERSONALINFO.getString(ResponseParameters.DateOfBirth));
                        user.setGender(PERSONALINFO.getString(ResponseParameters.Gender));
                        user.setEmail(PERSONALINFO.getString(ResponseParameters.Email));
                        user.setAvatar(PERSONALINFO.getString(ResponseParameters.Avatar));
                        user.setAddress(PERSONALINFO.getString(ResponseParameters.Address));
                        user.setMiddleName(PERSONALINFO.getString(ResponseParameters.MiddleName));
                        user.setCity(PERSONALINFO.getString(ResponseParameters.City));
                        user.setState(PERSONALINFO.getString(ResponseParameters.State));
                        user.setZipCode(PERSONALINFO.getString(ResponseParameters.ZIP));
                        user.setEmailVisibility(PERSONALINFO.getString(ResponseParameters.EMAIL_VISIBILITY));
                        user.setMobileVisibility(PERSONALINFO.getString(ResponseParameters.MOBILE_VISIBILITY));
                        user.setAddressVisibility(PERSONALINFO.getString(ResponseParameters.ADDRESS_VISIBILITY));
                        user.setDobVisibility(PERSONALINFO.getString(ResponseParameters.DOB_VISIBILITY));
                        user.setGenderVisibility(PERSONALINFO.getString(ResponseParameters.GENDER_VISIBILITY));
                        if (PERSONALINFO.has(ResponseParameters.IS_CHALLENGED)) {
                            user.setIsChallenged(PERSONALINFO.getString(ResponseParameters.IS_CHALLENGED));
                        }
                        if (PERSONALINFO.has(ResponseParameters.AlternateMobile))
                            if (!PERSONALINFO.getString(ResponseParameters.AlternateMobile).equalsIgnoreCase("null") && PERSONALINFO.getString(ResponseParameters.AlternateMobile) != null)
                                user.setAlternateMobile(PERSONALINFO.getString(ResponseParameters.AlternateMobile));
                        if (PERSONALINFO.has(ResponseParameters.PROFILE_SUMMERY))
                            if (!PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY).equalsIgnoreCase("null") && PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY) != null)
                                user.setProfileSummary(PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY));

                        if (PERSONALINFO.has(ResponseParameters.PROFILE_HEADLINE))
                            if (!PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE).equalsIgnoreCase("null") && PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE) != null)
                                user.setProfileHeadline(PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE));

                        SharedPreferencesMethod.setUserInfo(getActivity(), user);
                    }
                    if (profile.has(ResponseParameters.EDUCATION_DETAIL_CHECK)) {
                        if (profile.getBoolean(ResponseParameters.EDUCATION_DETAIL_CHECK)) {
                            rbEducationDetail.setChecked(true);
                        } else {
                            rbEducationDetail.setChecked(false);
                        }
                    } else {
                        rbEducationDetail.setChecked(false);
                    }
                    if (profile.has(ResponseParameters.WORK_EXPERIANCE_CHECK)) {
                        if (profile.getBoolean(ResponseParameters.WORK_EXPERIANCE_CHECK)) {
                            rbOrganisationDetails.setChecked(true);
                        } else {
                            rbOrganisationDetails.setChecked(false);
                        }
                    } else {
                        rbOrganisationDetails.setChecked(false);
                    }
                    if (profile.has(ResponseParameters.USER_SKILLS_CHECK) || profile.has(ResponseParameters.USER_DKILLS_CHECK)) {
                        if (profile.getBoolean(ResponseParameters.USER_SKILLS_CHECK) || profile.getBoolean(ResponseParameters.USER_DKILLS_CHECK)) {
                            rbSkillDetails.setChecked(true);
                        } else {
                            rbSkillDetails.setChecked(false);
                        }
                    } else {
                        rbSkillDetails.setChecked(false);
                    }
                    if (profile.has(ResponseParameters.CHECK_ADHAR_INFO)) {
                        if (profile.getBoolean(ResponseParameters.CHECK_ADHAR_INFO)) {
                            rbAadharDetail.setChecked(true);
                        } else {
                            rbAadharDetail.setChecked(false);
                        }
                    } else {
                        rbAadharDetail.setChecked(false);
                    }
                    if (profile.has(ResponseParameters.AADHAR_INFO)) {
                        JSONObject adharInfo = profile.getJSONObject(ResponseParameters.AADHAR_INFO);
                        if (adharInfo.has(ResponseParameters.VERIFICATION_STATUS)) {
                            if (Integer.parseInt(adharInfo.getString(ResponseParameters.VERIFICATION_STATUS)) == 1)
                                tvAdharVerified.setVisibility(View.VISIBLE);
                            else
                                tvAdharVerified.setVisibility(View.GONE);
                        } else
                            tvAdharVerified.setVisibility(View.GONE);
                    }
                }
                if (outPut.has(ResponseParameters.recommendations)) {
                    recommedations = outPut.getJSONArray(ResponseParameters.recommendations);
                    if (recommedations.length() > 0) {
                        rbRecom.setChecked(true);
                    } else {
                        rbRecom.setChecked(false);
                    }
                }

                if (outPut.has(ResponseParameters.LANGUAGES)) {
                    String langauge = "";
                    for (int i = 0; i < outPut.getJSONArray(ResponseParameters.LANGUAGES).length(); i++) {
                        langauge = langauge + outPut.getJSONArray(ResponseParameters.LANGUAGES).getJSONObject(i).getString(ResponseParameters.LANGUAGE).trim() + ", ";
                    }
                    SharedPreferencesMethod.setString(getActivity(), SharedPreferencesMethod.LANGUAGES, langauge.trim());
                }

                if (outPut.has(ResponseParameters.MIN_POINT_ROCKSTAR)) {
                    mRockStarPoint = Integer.parseInt(outPut.getString(ResponseParameters.MIN_POINT_ROCKSTAR));
                }
                if (outPut.has(ResponseParameters.MIN_POINT_SUPERSTAR)) {
                    mSuperStarPoint = Integer.parseInt(outPut.getString(ResponseParameters.MIN_POINT_SUPERSTAR));
                }
                if (outPut.has(ResponseParameters.MIN_POINT_POWERSTAR)) {
                    mPoweStarpoint = Integer.parseInt(outPut.getString(ResponseParameters.MIN_POINT_POWERSTAR));
                }

                int reward_points = Integer.parseInt(tvRewardPoints.getText().toString());

                if (reward_points < mRockStarPoint) {

                    tv_userType.setText("");


                }

                if (reward_points > mRockStarPoint && reward_points < mSuperStarPoint) {

                    if (outPut.has(ResponseParameters.ROCKSTAR)) {
                        tv_userType.setText(outPut.getString(ResponseParameters.ROCKSTAR));
                    }

                }

                if (reward_points > mSuperStarPoint && reward_points < mPoweStarpoint) {

                    if (outPut.has(ResponseParameters.SUPERSTAR)) {
                        tv_userType.setText(outPut.getString(ResponseParameters.SUPERSTAR));
                    }

                }

                if (reward_points > mPoweStarpoint) {

                    if (outPut.has(ResponseParameters.POWERSTAR)) {
                        tv_userType.setText(outPut.getString(ResponseParameters.POWERSTAR));
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    public void logSendProfileEvent(String profiledata) {
        AppEventsLogger logger = AppEventsLogger.newLogger(activity);
        Bundle params = new Bundle();
        params.putString("profiledata", profiledata);
        logger.logEvent("sendProfile", params);
    }

    public void logSendProfileDataEvent(String username, String userlink, int profilepercentage, String useraddress, String profilepiclink, double valToSum) {
        AppEventsLogger logger = AppEventsLogger.newLogger(activity);
        Bundle params = new Bundle();
        params.putString("username", username);
        params.putString("userlink", userlink);
        params.putInt("profilepercentage", profilepercentage);
        params.putString("useraddress", useraddress);
        params.putString("profilepiclink", profilepiclink);
        logger.logEvent("sendProfileData", valToSum, params);
    }

    public void ApiGetExpiredSummation() {
        String userId = SharedPreferencesMethod.getUserId(getActivity());
        API.sendRequestToServerGET_FRAGMENT(getActivity(), this, API.USER_EXPIRED_SUMMATION + "/" + userId, API.USER_EXPIRED_SUMMATION);
    }

    //change locale
    private void changeLocale(String languageToLoad) {
        locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        config = new Configuration();
        config.locale = locale;
        getContext().getResources().updateConfiguration(config,
                getContext().getResources().getDisplayMetrics());

    }

    public interface PercentageListener {
        public void profileValue(int s);
    }


}