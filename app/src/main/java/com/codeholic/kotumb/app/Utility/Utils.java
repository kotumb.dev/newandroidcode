package com.codeholic.kotumb.app.Utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.fragment.app.FragmentActivity;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Activity.JoinedGroupListActivity;
import com.codeholic.kotumb.app.Activity.MessageActivity;
import com.codeholic.kotumb.app.Activity.ShowVideoFromLink;
import com.codeholic.kotumb.app.Activity.SplashActivity;
import com.codeholic.kotumb.app.Adapter.LanguageAdapter;
import com.codeholic.kotumb.app.Interface.OnDoneListener;
import com.codeholic.kotumb.app.Interface.OnGetLanguageListener;
import com.codeholic.kotumb.app.Interface.OnGetViewListener;
import com.codeholic.kotumb.app.Interface.OnResponseListener;
import com.codeholic.kotumb.app.Interface.OnStringResponseListener;
import com.codeholic.kotumb.app.Model.AppLanguage;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.Model.VideoData;
import com.codeholic.kotumb.app.R;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import saschpe.android.customtabs.CustomTabsHelper;
import saschpe.android.customtabs.WebViewFallback;

/**
 * Created by f22labs on 07/03/17.
 */

public class Utils {


    public static String shareText = "";
    public static String rewardPoints = "";
    public static int surveyTimeInSeconds = 10;
    public static Calendar appStartTime;
    public static boolean tookSurvey;
    public static JSONArray rewards;
    public static String referralCode = "";
    public static ArrayList<JSONObject> globalAds = new ArrayList<JSONObject>();
    public static String base_url_group = "";
    public static String canTakeSurvey = "0";
    private static ArrayList<AppLanguage> languages;
    public static boolean isFirstLogin = false;

    public static final void showToast(Context context, String message) {

        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public static final String getDeviceID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }


    public static final String getVersionName(Context context) {

        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo.versionName;

    }


    public static void setButtonBackgroundColor(Context context, Button button, int color) {

        if (Build.VERSION.SDK_INT >= 23) {
            button.setBackgroundColor(context.getResources().getColor(color, null));
        } else {
            button.setBackgroundColor(context.getResources().getColor(color));
        }
    }


    public static void setButtonBackgroundColor(Context context, TextView textView, int color) {

        if (Build.VERSION.SDK_INT >= 23) {
            textView.setBackgroundColor(context.getResources().getColor(color, null));
        } else {
            textView.setBackgroundColor(context.getResources().getColor(color));
        }
    }


    public static Drawable setDrawableSelector(Context context, int normal, int selected) {


        Drawable state_normal = ContextCompat.getDrawable(context, normal);

        Drawable state_pressed = ContextCompat.getDrawable(context, selected);

        Bitmap state_normal_bitmap = ((BitmapDrawable) state_normal).getBitmap();

        // Setting alpha directly just didn't work, so we draw a new bitmap!
        Bitmap disabledBitmap = Bitmap.createBitmap(
                state_normal.getIntrinsicWidth(),
                state_normal.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(disabledBitmap);

        Paint paint = new Paint();
        paint.setAlpha(126);
        canvas.drawBitmap(state_normal_bitmap, 0, 0, paint);

        BitmapDrawable state_normal_drawable = new BitmapDrawable(context.getResources(), disabledBitmap);

        StateListDrawable drawable = new StateListDrawable();

        drawable.addState(new int[]{android.R.attr.state_selected},
                state_pressed);
        drawable.addState(new int[]{android.R.attr.state_enabled},
                state_normal_drawable);

        return drawable;
    }


    public static StateListDrawable selectorRadioImage(Context context, Drawable normal, Drawable pressed) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked}, pressed);
        states.addState(new int[]{}, normal);
        //                imageView.setImageDrawable(states);
        return states;
    }

    public static StateListDrawable selectorRadioButton(Context context, int normal, int pressed) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked}, new ColorDrawable(pressed));
        states.addState(new int[]{}, new ColorDrawable(normal));
        return states;
    }

    public static ColorStateList selectorRadioText(Context context, int normal, int pressed) {
        ColorStateList colorStates = new ColorStateList(new int[][]{new int[]{android.R.attr.state_checked}, new int[]{}}, new int[]{pressed, normal});
        return colorStates;
    }


    public static StateListDrawable selectorRadioDrawable(Drawable normal, Drawable pressed) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked}, pressed);
        states.addState(new int[]{}, normal);
        return states;
    }

    public static StateListDrawable selectorBackgroundColor(Context context, int normal, int pressed) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_pressed}, new ColorDrawable(pressed));
        states.addState(new int[]{}, new ColorDrawable(normal));
        return states;
    }

    public static StateListDrawable selectorBackgroundDrawable(Drawable normal, Drawable pressed) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_pressed}, pressed);
        states.addState(new int[]{}, normal);
        return states;
    }

    public static ColorStateList selectorText(Context context, int normal, int pressed) {
        ColorStateList colorStates = new ColorStateList(new int[][]{new int[]{android.R.attr.state_pressed}, new int[]{}}, new int[]{pressed, normal});
        return colorStates;
    }


    //Utility method to show pop up with plain simple message and a OK button to dismiss.
    public static void showPopup(final Activity activity, String string) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_normal, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);
        tvTitle.setText(string);
        //tvYes.setText(activity.getResources().getString(R.string.request_accept_btn_text));

        final AlertDialog confirmAlert = dialogBuilder.create();
        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(activity instanceof ShowVideoFromLink){
                    Intent intent=new Intent(activity, HomeScreenActivity.class);
                    activity.startActivity(intent);
                    activity.finish();
                }
                confirmAlert.dismiss();
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();

    }

    public static void showPopup(Activity activity, String string, final View.OnClickListener onClickListener) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_normal, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);

        tvTitle.setText(string);
        //tvYes.setText(activity.getResources().getString(R.string.request_accept_btn_text));

        final AlertDialog confirmAlert = dialogBuilder.create();
        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
                if (onClickListener != null) {
                    onClickListener.onClick(null);
                }
            }
        });
        confirmAlert.setCancelable(false);
        confirmAlert.show();

    }

    public static void showPopup(Activity activity, String string, final View.OnClickListener onClickListener, String text) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_normal, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);

        tvTitle.setText(string);
        tvYes.setText(text);
        //tvYes.setText(activity.getResources().getString(R.string.request_accept_btn_text));

        final AlertDialog confirmAlert = dialogBuilder.create();
        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
                if (onClickListener != null) {
                    onClickListener.onClick(null);
                }
            }
        });
        confirmAlert.setCancelable(false);
        confirmAlert.show();

    }

    public static String encode(String toServer) {
        return StringEscapeUtils.escapeJava(toServer);
    }

    public static String decode(String serverResponse) {
        try {
            return StringEscapeUtils.unescapeJava(serverResponse);
        } catch (Exception e) {
            e.printStackTrace();
            return serverResponse;
        }
    }

    public static void openCustomTab(String uriString, Activity activity) {
        CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                .addDefaultShareMenuItem()
                .setToolbarColor(activity.getResources().getColor(R.color.colorPrimary))
                .setShowTitle(true)
                .build();

        // This is optional but recommended
        saschpe.android.customtabs.CustomTabsHelper.addKeepAliveExtra(activity, customTabsIntent.intent);

        // This is where the magic happens...
        CustomTabsHelper.openCustomTab(activity, customTabsIntent,
                Uri.parse(uriString),
                new WebViewFallback());
    }

    public static void showOthersPopup(Activity activity, final AutoCompleteTextView autoCompleteTextView, final OnStringResponseListener listener) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_other, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final EditText other = dialogView.findViewById(R.id.other);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);

        //tvTitle.setText(string);
        //tvYes.setText(activity.getResources().getString(R.string.request_accept_btn_text));

        final AlertDialog confirmAlert = dialogBuilder.create();
        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String response = other.getText().toString().trim();
                if (!response.isEmpty()) {
                    confirmAlert.dismiss();
                    if (listener != null) {
                        listener.onGotResponse(response);
                    }
                } else {
                    other.setError("Cannot be empty");
                }
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();

        confirmAlert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (autoCompleteTextView.getText().toString().equalsIgnoreCase("other")) {
                    autoCompleteTextView.setText("");
                }
            }
        });

    }

    public static void addOtherOptionHandler(final AutoCompleteTextView autoCompleteTextView, final List<String> items, final Activity activity) {
        addOtherOptionHandler(autoCompleteTextView, items, activity, false);
    }

    public static void addOtherOptionHandler(final AutoCompleteTextView autoCompleteTextView, final List<String> items, final Activity activity, final boolean add) {
        Utils.addCustomAdapter(autoCompleteTextView, items, activity);
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                autoCompleteTextView.setTag(true);
                // Toast.makeText(activity, autoCompleteTextView.getTag()+"", Toast.LENGTH_SHORT).show();
                String s = autoCompleteTextView.getText().toString();
                //boolean other = items.get(i).equalsIgnoreCase("other");
                boolean other2 = s.contains("Other");
                if (other2) {
                    String text = autoCompleteTextView.getText().toString();
                    text = text.replace("Other,", "");
                    autoCompleteTextView.setText(text);
                    Utils.showOthersPopup(activity, autoCompleteTextView, new OnStringResponseListener() {
                        @Override
                        public void onGotResponse(String response) {
                            if (add) {
                                String text = autoCompleteTextView.getText().toString().trim();
                                String space = text.isEmpty() ? "" : " ";
                                autoCompleteTextView.setText(text + space + response + ", ");
                                autoCompleteTextView.setTag(true);
//                                autoCompleteTextView.setText(response);
//                                autoCompleteTextView.setTag(true);
                            } else {
                                autoCompleteTextView.setText(response);
                                autoCompleteTextView.setTag(true);
                                //  Toast.makeText(activity, autoCompleteTextView.getTag()+"", Toast.LENGTH_SHORT).show();
                            }
                            autoCompleteTextView.dismissDropDown();
                            autoCompleteTextView.setSelection(autoCompleteTextView.getText().toString().length());
                        }
                    });
                }
            }
        });
    }

    public static void getLanguages(final Activity activity, final OnGetLanguageListener listener) {
        System.out.println("Inside function===");
            final ProgressDialog dialog = new ProgressDialog(activity);
            dialog.setMessage("Loading");
            dialog.show();
            languages = new ArrayList<>();
            AQuery aQuery = new AQuery(activity);
            aQuery.ajax(API.GET_APP_LANGUAGE, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject object, AjaxStatus status) {
                    dialog.dismiss();
                    System.out.println("langResponse=="+object);
                    try {
                        if (object != null) {
                            if (object.has(ResponseParameters.LANGUAGES)) {
                                JSONArray array = object.getJSONArray(ResponseParameters.LANGUAGES);
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);
                                    System.out.println("language==="+jsonObject);
                                    try {
                                        languages.add(new AppLanguage(jsonObject));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                if (listener != null) {
                                    listener.onGotLanguage(languages);
                                }
                            }
                        } else {
                            Utils.showPopup(activity, object + "");
                        }
                    } catch (JSONException e) {
                        showPopup(activity, e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
    }

    public static void showChangeLanguagePopup(ArrayList<AppLanguage> languages, final Activity activity) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        dialogBuilder.setTitle(activity.getResources().getString(R.string.select_language));
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.change_langauge_popup2, null);
        dialogBuilder.setView(dialogView);

        String oldLAng = SharedPreferencesMethod.getDefaultLanguage(activity);
        final ListView listView = dialogView.findViewById(R.id.listView);
        final LanguageAdapter adapter = new LanguageAdapter(languages, activity);
        adapter.setOldLang(oldLAng);
        listView.setAdapter(adapter);

        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(activity.getResources().getString(R.string.update_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Utility.isConnectingToInternet(activity)) {
                    final AppLanguage language = adapter.getSelectedLang();
                   // Toast.makeText(activity,language.getLanguage()+"=="+language.getLang_code(),Toast.LENGTH_LONG).show();
                    if (language != null) {
                        String userId = SharedPreferencesMethod.getUserId(activity);
                        if (userId != null && !userId.isEmpty()) {
                            callServ(language, activity, getChangeLanguageResponseListener(language, activity));
                        } else {
                            performChangeLangRitual(activity, language);
                        }
                    } else {
                        dialog.dismiss();
                    }
                } else {
                    showPopup(activity, activity.getString(R.string.app_no_internet_error));
                }
            }
        });

        dialogBuilder.setNegativeButton(activity.getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog changeLanguageAlertDialog = dialogBuilder.create();
        //changeLanguageAlertDialog.setCancelable(false);
        changeLanguageAlertDialog.show();
    }

    @NonNull
    private static OnResponseListener getChangeLanguageResponseListener(final AppLanguage language, final Activity activity) {
        return new OnResponseListener() {
            @Override
            public void onGotResponse(JSONObject response) {
                if (response != null) {
                    performChangeLangRitual(activity, language);
                } else {
                    showPopup(activity, activity.getString(R.string.something_wrong));
                }
            }
        };
    }

    private static void performChangeLangRitual(Activity activity, AppLanguage language) {
        SharedPreferencesMethod.clearSerViceCall(activity);
        SharedPreferencesMethod.setUpdateData(activity);
        SharedPreferencesMethod.setDefaultLanguage(activity, language.getLang_code());
        SharedPreferencesMethod.setDefaultLanguageName(activity, language.getLanguage());
        activity.startActivity(new Intent(activity, SplashActivity.class));
        activity.finish();
        activity.overridePendingTransition(0, 0);
    }

    private static void callServ(AppLanguage language, final Activity activity, final OnResponseListener responseListener) {
        final ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setMessage("Loading");
        dialog.show();
        String lang_code = language.getLang_code();
        String userId = SharedPreferencesMethod.getUserId(activity);
        String url = API.CHANGE_LANG + userId + "/" + lang_code;
        System.out.println("Change Lang API  "+url);
        new AQuery(activity).ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject jsonObject, AjaxStatus status) {
                dialog.dismiss();
                if (jsonObject != null) {
                    if (jsonObject.has(ResponseParameters.Success)) {
                        responseListener.onGotResponse(jsonObject);
                    } else {
                        showPopup(activity, jsonObject + "");
                    }
                } else {
                    responseListener.onGotResponse(null);
                }
            }
        });
    }

    public static void getLang(final Activity activity) {
        Utils.getLanguages(activity, new OnGetLanguageListener() {
            @Override
            public void onGotLanguage(ArrayList<AppLanguage> language) {
                System.out.println("lang=="+language);
                Utils.showChangeLanguagePopup(language, activity);
            }
        });
    }

    public static void showPopup(Activity activity, String s, String yes, String no, final View.OnClickListener onClickYesListener, final View.OnClickListener onClickNoListener) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_yes_no, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);
        final LinearLayout llNo = dialogView.findViewById(R.id.llCancel);
        final TextView tvNo = dialogView.findViewById(R.id.tvCancel);

        tvTitle.setText(s);
        tvYes.setText(yes);
        tvNo.setText(no);

        final AlertDialog confirmAlert = dialogBuilder.create();
        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
                if (onClickYesListener != null) {
                    onClickYesListener.onClick(v);
                }
            }
        });

        llNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
                if (onClickNoListener != null) {
                    onClickNoListener.onClick(v);
                }
            }
        });
        confirmAlert.setCancelable(false);
        confirmAlert.show();
    }

    private static void addCustomAdapter(AutoCompleteTextView etNameOrg, List<String> searchArrayList, Context context) {
        AutoCompleteAdapter adapter2 = new AutoCompleteAdapter(context, R.layout.autocomplete_item, android.R.id.text1, searchArrayList);
        etNameOrg.setAdapter(adapter2);
    }

    public static String getErrorMessage(JSONObject outPut) throws JSONException {
        try {
            String key = outPut.getString(ResponseParameters.ErrorKey);
            ErrorMessages errorMessages = ErrorMessages.getInstance();
            String errorMsg = errorMessages.getMessageFor(key);
            if (errorMsg != null) {
                return errorMsg + "";
            } else {
                return outPut.getString(ResponseParameters.Errors);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return outPut.getString(ResponseParameters.Errors);
        }
    }

    private static void showShareExternalOptions(Post post, String group_id, Activity activity) {
        User userInfo = SharedPreferencesMethod.getUserInfo(activity);
        String s = userInfo.getFirstName() + " " + userInfo.getLastName() + " Shared a post via Kotumb App: \n\n";
//        String shareBody = s + post.content;
        String host = API.share_post_link;
        String pathPrefix = "group/singlePost/" + group_id + "/";
        String shareBody = s + host + pathPrefix + post.post_id;
        shareBody = shareBody + "\n\n" + Utils.shareText;
        System.out.println("Share Post Link    "+shareBody);
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        if (post.image.length() > 0) {
//            sharingIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
//            sharingIntent.setType("image/jpeg");
        }
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Kotumb Share");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        activity.startActivity(Intent.createChooser(sharingIntent, activity.getResources().getString(R.string.share_using)));
    }

    public static void showSharePopUpMenu(LinearLayout llShare, final Post post, final String group_id, final Activity activity) {
        PopupMenu menu = new PopupMenu(activity, llShare);
        menu.getMenu().add(1, 0, 1, activity.getString(R.string.share_in_app_text));
        menu.getMenu().add(1, 1, 1, activity.getString(R.string.share_external_text));
        menu.show();
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case 0:
                        Intent intent = new Intent(activity, JoinedGroupListActivity.class);
                        intent.putExtra(ResponseParameters.POST, post);
                        activity.startActivity(intent);
                        break;
                    case 1:
                        Utils.showShareExternalOptions(post, group_id, activity);
                        break;
                }
                return false;
            }
        });
    }



    public static void showVideoSharePopUpMenu(LinearLayout llShare, final String videoDetails, final VideoData videoData, final String video_id, final Activity activity) {
        PopupMenu menu = new PopupMenu(activity, llShare);
        menu.getMenu().add(1, 0, 1, activity.getString(R.string.share_in_app_text));
        menu.getMenu().add(1, 1, 1, activity.getString(R.string.share_external_text));
        menu.show();
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case 0:
                        Intent intent = new Intent(activity, MessageActivity.class);
                        intent.putExtra(ResponseParameters.VIDEOS, videoData);
                        intent.putExtra("videoid",videoDetails);
                        intent.putExtra("videoName",videoData.getUserFirstName()+" "+videoData.getUserLastName());
                        intent.putExtra("videoLike",videoData.getVideoLikeCount());
                        intent.putExtra("videoDate",videoData.getVideoUploadedDate());
                        intent.putExtra("videoUserId",videoData.getUserId());
                        intent.putExtra("videoUserAvatar",videoData.getUserAvatar());
                        intent.putExtra("videoTitle",videoData.getVideoTitle());
                        intent.putExtra("videoFormate",videoData.getVideoName());
                        intent.putExtra("videoDesp",videoData.getVideoDescription());
//                        intent.putExtra("videoid",videoData.getVideoId());
                        activity.startActivity(intent);
                        break;
                    case 1:
                        Utils.showVideoShareExternalOptions(videoDetails,videoData, video_id, activity);
                        break;
                }
                return false;
            }
        });
    }





    private static void showVideoShareExternalOptions(String videoDetails,final VideoData videoData, String video_id,final  Activity activity) {
        User userInfo = SharedPreferencesMethod.getUserInfo(activity);

        final String[] message = {""};
        String url=API.base_api+"services/upload_video_limits/"+userInfo.getUserId();
        new AQuery(activity).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                System.out.println("Response== "+object.toString());
                try {
                    message[0] =object.getString("outside_message_on_video_approved_and_published");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String shareBody = message[0] +"\n"+API.share_post_link+"KotumbVideo/video/"+videoData.getVideoId();
                System.out.println("Share Post Link    "+Html.fromHtml(shareBody));
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
//        if (post.image.length() > 0) {
////            sharingIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
////            sharingIntent.setType("image/jpeg");
//        }
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Kotumb Share");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                activity.startActivity(Intent.createChooser(sharingIntent, activity.getResources().getString(R.string.share_using)));
            }
        });



//        String shareBody = s + post.content;
       // String host = API.share_post_link;
       // String pathPrefix = videoData.getUserId() + "/"+API.user_videos+videoData.getVideoName();

    }


    public static void showDeletePopUpMenu(View llShare, final Activity activity, PopupMenu.OnMenuItemClickListener itemClickListener) {
        PopupMenu menu = new PopupMenu(activity, llShare);
        menu.getMenu().add(1, 0, 1, activity.getString(R.string.delete_btn_text));
        //  menu.getMenu().add(1, 1, 1, activity.getString(R.string.share_external_text));
        menu.show();
        menu.setOnMenuItemClickListener(itemClickListener);
    }

    public static void showPopupInTextView(FragmentActivity activity, String timeString, OnGetViewListener onGetViewListener, DialogInterface.OnDismissListener onDismissListener) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_normal, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);
        dialogBuilder.setOnDismissListener(onDismissListener);
        tvTitle.setText(timeString);
        if (onGetViewListener != null) {
            onGetViewListener.onGetView(0, tvTitle, null);
        }
        //tvYes.setText(activity.getResources().getString(R.string.request_accept_btn_text));

        final AlertDialog confirmAlert = dialogBuilder.create();
        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();
    }

   /* public static JSONObject getAUniqueAd() {
        for (JSONObject object: globalAds) {
            try {
                if (!usedAdIds.contains(object.getString("id"))) {
                    usedAdIds.add(object.getString("id"));
                    return object;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        usedAdIds.clear();
        try {
            usedAdIds.add(globalAds.get(0).getString("id"));
            return globalAds.get(0);
        } catch (JSONException e) {
            e.printStackTrace();
            return globalAds.get(0);
        }
    }*/

   /* public static JSONObject getAUniqueAd() {
        if(index>=globalAds.size()){
            index=0;
        }
        JSONObject jsonObject = globalAds.get(index);
        index++;
        return jsonObject;
    }*/

    public static void logEvent(String eventName, Bundle bundle, Context context) {
        try {
            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
            mFirebaseAnalytics.logEvent(eventName, bundle);
            Log.e("c", eventName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void logEventGroupLike(Activity activity, String groupId) {
        Bundle bundle = new Bundle();
        bundle.putString("UserId", SharedPreferencesMethod.getUserId(activity));
        bundle.putString("GroupId", groupId);
        Utils.logEvent(EventNames.GROUP_POST_LIKE, bundle, activity);
    }

    public static void logEventGroupCommented(Activity activity, String groupId) {
        Bundle bundle = new Bundle();
        bundle.putString("UserId", SharedPreferencesMethod.getUserId(activity));
        bundle.putString("GroupId", groupId);
        Utils.logEvent(EventNames.GROUP_POST_COMMENT, bundle, activity);
    }

    public static void logEventUserConnectRequest(Activity activity, String sentTo) {
        Bundle bundle = new Bundle();
        bundle.putString("SentFrom", SharedPreferencesMethod.getUserId(activity));
        bundle.putString("SentTo", sentTo);
        Utils.logEvent(EventNames.SEND_CONNECT_REQ, bundle, activity);
    }

    public static void logEventUserConnectAccept(Activity activity, String sentTo) {
        Bundle bundle = new Bundle();
        bundle.putString("SentTo", SharedPreferencesMethod.getUserId(activity));
        bundle.putString("SentFrom", sentTo);
        Utils.logEvent(EventNames.ACCEPT_CONNECT_REQ, bundle, activity);
    }

    public static void logEventGroupInviteSend(Activity activity, String groupId) {
        Bundle bundle = new Bundle();
        bundle.putString("userId", SharedPreferencesMethod.getUserId(activity));
        bundle.putString("GroupId", groupId);
        Utils.logEvent(EventNames.GROUP_INVITE_SEND, bundle, activity);
    }

    public static void logEventGroupInviteAccept(Activity activity, String groupId) {
        Bundle bundle = new Bundle();
        bundle.putString("userId", SharedPreferencesMethod.getUserId(activity));
        bundle.putString("GroupId", groupId);
        Utils.logEvent(EventNames.GROUP_INVITE_ACCEPT, bundle, activity);
    }

    public static void logEventGroupJoinRequest(Activity activity, String groupId) {
        Bundle bundle = new Bundle();
        bundle.putString("userId", SharedPreferencesMethod.getUserId(activity));
        bundle.putString("GroupId", groupId);
        Utils.logEvent(EventNames.GROUP_JOIN_REQUEST, bundle, activity);
    }

    public static void copyToClipboard(String text, Activity activity) {
        ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("", text);
        clipboard.setPrimaryClip(clip);
    }

    public static void showPopupWithImage(String string, String imgURL, Activity activity) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_normal_image, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);

        tvTitle.setText(string);
        //tvYes.setText(activity.getResources().getString(R.string.request_accept_btn_text));

        final AlertDialog confirmAlert = dialogBuilder.create();
        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();
    }

    public static String getExtension(String fileName) {
        String encoded;
        try { encoded = URLEncoder.encode(fileName, "UTF-8").replace("+", "%20"); }
        catch(UnsupportedEncodingException e) { encoded = fileName; }
        return MimeTypeMap.getFileExtensionFromUrl(encoded).toLowerCase();
    }


    public static void showVideoPopup(String string, Activity activity, final OnDoneListener listener) {
        try {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
            builder.setMessage(string);
            builder.setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //listener.onDone();
                }
            });
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    listener.onDone();
                }
            });
            builder.create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static void buildMediaSource(Uri mUri, SimpleExoPlayer player,Activity activity) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(activity,
                Util.getUserAgent(activity, activity.getResources().getString(R.string.app_name)), bandwidthMeter);
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);
        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        player.addListener((Player.EventListener) activity);
    }


    public static void initializePlayer(PlayerView videoFullScreenPlayer,SimpleExoPlayer player,Activity activity) {
        if (player == null) {
            // 1. Create a default TrackSelector
            LoadControl loadControl = new DefaultLoadControl(
                    new DefaultAllocator(true, 20),
                    3000,
                    5000,
                    1500,
                    5000, -1, true);

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);
            // 2. Create the player
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(activity), trackSelector, loadControl);
            videoFullScreenPlayer.setPlayer(player);
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN |View.SYSTEM_UI_FLAG_IMMERSIVE);
        }
    }



    public static void playVideo(Uri videoUri,PlayerView videoFullScreenPlayer,SimpleExoPlayer player,Activity activity) {
        initializePlayer(videoFullScreenPlayer,player,activity);
        if (videoUri == null) {
            return;
        }
        buildMediaSource(videoUri,player,activity);
    }






}