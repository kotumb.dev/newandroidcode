package com.codeholic.kotumb.app.Activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.codeholic.kotumb.app.R;

public class ImageViewActivity extends AppCompatActivity {

    private static Bitmap image;
    private static String link;

    public static void setImage(Bitmap image) {
        ImageViewActivity.image = image;
    }

    public static void setLink(String link) {
        ImageViewActivity.link = link;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);

        ImageView imageView = findViewById(R.id.image);
        if (image != null) {
            imageView.setImageBitmap(image);
        } else if (link != null) {
            new AQuery(this).id(imageView).image(link);
        } else {
            finish();
        }

        imageView.setOnTouchListener(new ImageMatrixTouchHandler(imageView.getContext()));
    }

    @Override
    protected void onStop() {
        super.onStop();
        image = null;
        link = null;
    }
}
