package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;

import com.codeholic.kotumb.app.Adapter.EducationListAdapter;
import com.codeholic.kotumb.app.Adapter.ProfessionListAdapter;
import com.codeholic.kotumb.app.Model.Education;
import com.codeholic.kotumb.app.Utility.Constants;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.google.firebase.iid.FirebaseInstanceId;
import com.shagi.materialdatepicker.date.DatePickerFragmentDialog;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.github.florent37.viewtooltip.ViewTooltip.ALIGN.CENTER;
import static com.github.florent37.viewtooltip.ViewTooltip.Position.TOP;

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener
{

    private CardView cvSave;
    private Activity context;
    private boolean educationFlag = false;
    private String languageToLoad = "";
    private RadioGroup rgGenderGroup;
    private RadioButton rbMale, rbFemale, rbOther;
    private AutoCompleteTextView etPincode;
    private EditText etFirstName, etLastName, etDOB, etPhone, etCity, etState, etEmail, etAnswerOne, etAnswerTwo, etQuestionOne, etQuestionTwo, etGender, etCAddress;
    private TextInputLayout etFirstNameLayout, etLastNameLayout, etDOBLayout, etGenderLayout, etPhoneLayout, etPincodeLayout, etCityLayout, etStateLayout, etEmailLayout, etAnswerOneLayout, etAnswerTwoLayout, etCAddressLayout;
    private TextView tvSave;
    private ImageView ivHelp;
    private CheckBox cbTerms, cbEmail, cbAddress, cbMobile;
    private Configuration config = new Configuration();
    private Locale locale;
    private User user = new User();
    private android.widget.ProgressBar pbLoading, pbPinCode;
    private Spinner spQuestionOne, spQuestionTwo;
    private int questionId1 = 0, questionId2 = 0;
    private String gender = "";
    private JSONArray questionSetOne = new JSONArray();
    private JSONArray questionSetTwo = new JSONArray();
    private Calendar calendar;
    private int year, month, day;
    ViewTooltip v;
    NestedScrollView nvcontainer;
    ImageView ivMobile, ivAddress, ivEmail, ivQuestionOne, ivQuestionTwo;
    @BindView(R.id.maLanguageLayout)
    TextInputLayout maLanguageLayout;

    @BindView(R.id.maLanguage)
    MultiAutoCompleteTextView maLanguage;
    @BindView(R.id.ivLanguage)
    ImageView ivLanguage;

    @BindView(R.id.cbDOB)
    CheckBox cbDOB;

    @BindView(R.id.cbChallenged)
    CheckBox cbChallenged;

    @BindView(R.id.ivDOB)
    ImageView ivDOB;

    @BindView(R.id.ivChallenged)
    ImageView ivChallenged;

    @BindView(R.id.cbGender)
    CheckBox cbGender;

    @BindView(R.id.etMiddleName)
    EditText etMiddleName;

    @BindView(R.id.ivGender)
    ImageView ivGender;

    @BindView(R.id.cvAddEducation)
    CardView cvAddEducation;

    @BindView(R.id.rvEducationList)
    RecyclerView rvEducationList;

    JSONObject profile = new JSONObject();
    private LinearLayoutManager linearLayoutManager;
    List<String> languages = new ArrayList<>();
    List<Education> educations = new ArrayList<>();
    private EducationListAdapter educationListAdapter;
    private TextInputLayout errorLayout;

    //oncreate method for setting content
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        languageToLoad = SharedPreferencesMethod.getDefaultLanguage(this);
        changeLocale(languageToLoad);
        setEducationRecyclerView();
    }

    //on resume method
    @Override
    protected void onResume()
    {
        setEducationRecyclerView();
        super.onResume();
        if (Constants.completeProfileActivity)
        { // for updating eduction recycler view
            educationFlag = true;
            if (Constants.education_position != -1 && Constants.education != null)
            {
                educations.remove(Constants.education_position);
                educations.add(Constants.education_position, Constants.education);
                educationListAdapter.notifyDataSetChanged();
            }
            else if (Constants.education != null)
            {
                if (educations.size() > 0)
                {
                    educations.add(Constants.education);
                    educationFlag = true;
                    educationListAdapter.notifyDataSetChanged();
                } else
                {
                    educations.add(Constants.education);
                    educationListAdapter = new EducationListAdapter(this, educations);
                    rvEducationList.setAdapter(educationListAdapter);
                    rvEducationList.setVisibility(VISIBLE);

                }
            }
        }
        Constants.education = null;
        Constants.education_position = -1;
        Constants.completeProfileActivity = false;
    }


    //changeLocale method for changing locale
    private void changeLocale(String languageToLoad)
    {
        locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setView();
    }

    //setView method for setting layout
    private void setView()
    {
        setContentView(R.layout.activity_my_profile);
        ButterKnife.bind(this);
        setToolbar();
        initView();
        fillValues();
        setClickListner();
        populateSpinner();
        setCheckedChangeListener();
        setEditTextWatcher();


        etPincode.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {

                if (hasFocus)
                {
                    if (!etPincodeLayout.isErrorEnabled())
                        etPincode.performClick();
                }
            }
        });

        etPincode.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!Utility.isConnectingToInternet(context))
                {
                    return;
                }
                Log.e("TAG", "CLICKED " + etPincode.isPopupShowing());
                if (etPincode.getText().toString().trim().length() < 6)
                {
//                    pbPinCode.setVisibility(VISIBLE);
//                    API.sendRequestToServerGET(context, API.ZIPCODE + etPincode.getText().toString(), API.ZIPCODE_CLICKED);
                }
            }
        });


        maLanguage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.e("onclick", "onclick");
                try
                {
                    if (!SharedPreferencesMethod.getString(MyProfileActivity.this, SharedPreferencesMethod.ALL_LANGUAGES).toString().isEmpty())
                    {
                        setUpLanguageAdapter(new JSONObject(SharedPreferencesMethod.getString(MyProfileActivity.this, SharedPreferencesMethod.ALL_LANGUAGES)), maLanguage);
                        maLanguage.showDropDown();
                    } else
                    {
                        if (!Utility.isConnectingToInternet(context))
                        {
                            return;
                        }
                        API.sendRequestToServerGET(context, API.GET_LANGUAGE, API.GET_LANGUAGE);
                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        ivLanguage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!maLanguage.getText().toString().trim().isEmpty())
                {
                    clearAutoCompleteEditText(maLanguage);
                }
            }
        });

    }

    private void clearAutoCompleteEditText(AutoCompleteTextView multiAutoCompleteTextView)
    {
        try
        {
            String tempText = multiAutoCompleteTextView.getText().toString().trim().replace(", ", ",");
            if (tempText.endsWith(","))
            {
                tempText = tempText.trim().substring(0, tempText.length() - 1);
            }
            if (!tempText.contains(","))
            {
                multiAutoCompleteTextView.setText("");
            } else
            {
                String[] temp = tempText.trim().split(",");
                List<String> list = new LinkedList<String>(Arrays.asList(temp));
                list.remove(list.size() - 1);
                StringBuilder sb = new StringBuilder();
                for (String str : list.toArray(new String[list.size()]))
                    sb.append(str).append(", ");
                multiAutoCompleteTextView.setText(sb.substring(0, sb.length() - 1));
                multiAutoCompleteTextView.setSelection(multiAutoCompleteTextView.getText().length());
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    void setUpLanguageAdapter(JSONObject outPut, final MultiAutoCompleteTextView multiAutoCompleteTextView)
    {
        try
        {
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY"))
            {
                if (outPut.has(ResponseParameters.Success))
                {
                    if (outPut.has(ResponseParameters.ALL_LANGUAGES))
                    {
                        languages = new ArrayList<>();
                        List<String> selectedList = new ArrayList<>();
                        String tempText = multiAutoCompleteTextView.getText().toString().trim().replace(", ", ",");
                        if (tempText.endsWith(","))
                        {
                            tempText = tempText.trim().substring(0, tempText.length() - 1);
                        }
                        if (!tempText.trim().isEmpty())
                            if (!tempText.contains(","))
                            {
                                selectedList.add(tempText);
                            } else
                            {
                                String[] temp = tempText.trim().split(",");
                                selectedList = Arrays.asList(temp);
                            }
                        for (int j = 0; j < outPut.getJSONArray(ResponseParameters.ALL_LANGUAGES).length(); j++)
                        {
                            if (!languages.contains(outPut.getJSONArray(ResponseParameters.ALL_LANGUAGES).getJSONObject(j).getString(ResponseParameters.LANGUAGE).toString().trim())
                                    && !selectedList.contains(outPut.getJSONArray(ResponseParameters.ALL_LANGUAGES).getJSONObject(j).getString(ResponseParameters.LANGUAGE).toString().trim()))
                                languages.add(outPut.getJSONArray(ResponseParameters.ALL_LANGUAGES).getJSONObject(j).getString(ResponseParameters.LANGUAGE).toString().trim());
                        }
                        String[] data = languages.toArray(new String[languages.size()]);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.autocomplete_item, data);
                        multiAutoCompleteTextView.setAdapter(adapter);
                        multiAutoCompleteTextView.setThreshold(1);
                        multiAutoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
                    }
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    //initView method for initializing view
    private void initView()
    {
        context = this;
        user = getIntent().getParcelableExtra(ResponseParameters.Userdata);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        tvSave = findViewById(R.id.tvSave);
        cbTerms = findViewById(R.id.cbTerms);
        cbEmail = findViewById(R.id.cbEmail);
        cbAddress = findViewById(R.id.cbAddress);
        cbMobile = findViewById(R.id.cbMobile);
        cvSave = findViewById(R.id.cv_save);
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etDOB = findViewById(R.id.etDOB);
        etPhone = findViewById(R.id.etPhone);
        etPincode = findViewById(R.id.etPincode);
        etCity = findViewById(R.id.etCity);
        etState = findViewById(R.id.etState);
        etEmail = findViewById(R.id.etEmail);
        etAnswerOne = findViewById(R.id.etAnswerOne);
        etAnswerTwo = findViewById(R.id.etAnswerTwo);
        etGender = findViewById(R.id.etGender);
        etCAddress = findViewById(R.id.etCAddress);
        etQuestionOne = findViewById(R.id.etQuestionOne);
        etQuestionTwo = findViewById(R.id.etQuestionTwo);
        pbLoading = findViewById(R.id.pbLoading);
        pbPinCode = findViewById(R.id.pbPinCode);

        etFirstNameLayout = findViewById(R.id.etFirstNameLayout);
        etLastNameLayout = findViewById(R.id.etLastNameLayout);
        etDOBLayout = findViewById(R.id.etDOBLayout);
        etGenderLayout = findViewById(R.id.etGenderLayout);
        etPhoneLayout = findViewById(R.id.etPhoneLayout);
        etPincodeLayout = findViewById(R.id.etPincodeLayout);
        etCityLayout = findViewById(R.id.etCityLayout);
        etStateLayout = findViewById(R.id.etStateLayout);
        etEmailLayout = findViewById(R.id.etEmailLayout);
        etAnswerOneLayout = findViewById(R.id.etAnswerOneLayout);
        etAnswerTwoLayout = findViewById(R.id.etAnswerTwoLayout);
        etCAddressLayout = findViewById(R.id.etCAddressLayout);

        spQuestionOne = findViewById(R.id.spQuestionOne);
        spQuestionTwo = findViewById(R.id.spQuestionTwo);

        rgGenderGroup = findViewById(R.id.rgGenderGroup);
        rbMale = findViewById(R.id.rbMale);
        rbFemale = findViewById(R.id.rbFemale);
        rbOther = findViewById(R.id.rbOther);

        ivHelp = findViewById(R.id.ivHelp);
        ivMobile = findViewById(R.id.ivMobile);
        ivAddress = findViewById(R.id.ivAddress);
        ivEmail = findViewById(R.id.ivEmail);
        ivQuestionOne = findViewById(R.id.ivQuestionOne);
        ivQuestionTwo = findViewById(R.id.ivQuestionTwo);
        nvcontainer = findViewById(R.id.nvcontainer);
       /* focusView = findViewById(id);
        focusView.getParent().requestChildFocus(focusView, focusView); // for scrolling to specific view*/

        Constants.education = null;
        Constants.education_position = -1;
        Constants.completeProfileActivity = false;

    }

    private void fillValues()
    {
        System.out.println("User Mobile Number    " + user.getMobileNumber() + "===" + user.getDOB());
        etPhone.setText(user.getMobileNumber());
        etFirstName.setText(user.getOnlyFirstName());
        etLastName.setText(user.getLastName());
        etMiddleName.setText(user.getMiddleName());
        try
        {
            if (!user.getDOB().trim().equalsIgnoreCase("0000-00-00"))
            {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");
                etDOB.setText(df_.format(df.parse(user.getDOB())));
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        etPincode.setText(user.getZipCode());
        etCity.setText(user.getCity());
        etState.setText(user.getState());
        etEmail.setText(user.getEmail());
        etCAddress.setText(user.getAddress());
        if (user.getGender().equalsIgnoreCase("M"))
        {
            gender = "M";
            rbMale.setChecked(true);
        } else if (user.getGender().equalsIgnoreCase("F"))
        {
            gender = "F";
            rbFemale.setChecked(true);
        } else if (user.getGender().equalsIgnoreCase("O"))
        {
            gender = "O";
            rbOther.setChecked(true);
        }
        if (user.getEmailVisibility().equalsIgnoreCase("0"))
        {
            cbEmail.setChecked(false);
        } else
        {
            cbEmail.setChecked(false);
        }

        if (user.getMobileVisibility().equalsIgnoreCase("0"))
        {
            cbMobile.setChecked(false);
        } else
        {
            cbMobile.setChecked(false);
        }
        if (user.getAddressVisibility().equalsIgnoreCase("0"))
        {
            cbAddress.setChecked(false);
        } else
        {
            cbAddress.setChecked(false);
        }
        if (user.getGenderVisibility().equalsIgnoreCase("0"))
        {
            cbGender.setChecked(true);
        } else
        {
            cbGender.setChecked(false);
        }

        if (user.getDobVisibility().equalsIgnoreCase("0"))
        {
            cbDOB.setChecked(false);
        } else
        {
            cbDOB.setChecked(false);
        }


        maLanguage.setText(SharedPreferencesMethod.getString(this, SharedPreferencesMethod.LANGUAGES));
        maLanguage.setSelection(maLanguage.getText().length());
    }

    //populateSpinner of questions set
    private void populateSpinner()
    {
        try
        {
            spQuestionOne.setOnItemSelectedListener(this);
            List<String> questionsetOne = new ArrayList<String>();
            questionSetOne = new JSONArray(user.getSet1Questions());
            questionSetTwo = new JSONArray(user.getSet2Questions());
            for (int i = 0; i < questionSetOne.length(); i++)
            {
                questionsetOne.add(questionSetOne.getJSONObject(i).getString(ResponseParameters.Question));
            }
            ArrayAdapter<String> questionOneAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_dropdown_item, questionsetOne);
            spQuestionOne.setAdapter(questionOneAdapter);
            spQuestionOne.setSelection(0);

            spQuestionTwo.setOnItemSelectedListener(this);
            questionsetOne = new ArrayList<String>();
            for (int i = 0; i < questionSetTwo.length(); i++)
            {
                questionsetOne.add(questionSetTwo.getJSONObject(i).getString(ResponseParameters.Question));
            }
            ArrayAdapter<String> questionTwoAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_dropdown_item, questionsetOne);
            spQuestionTwo.setAdapter(questionTwoAdapter);
            spQuestionTwo.setSelection(0);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    //check change listener for gender
    private void setCheckedChangeListener()
    {
        rgGenderGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                etGenderLayout.setErrorEnabled(false);
                switch (checkedId)
                {
                    case R.id.rbMale:
                        gender = "M";
                        break;
                    case R.id.rbFemale:
                        gender = "F";
                        break;
                    case R.id.rbOther:
                        gender = "O";
                        break;
                }
            }
        });
    }

    //setting edittext watcher
    private void setEditTextWatcher()
    {
        setTextWatcher(etFirstName, etFirstNameLayout);
        setTextWatcher(etLastName, etLastNameLayout);
        setTextWatcher(etDOB, etDOBLayout);
        setTextWatcher(etGender, etGenderLayout);
        setTextWatcher(etPhone, etPhoneLayout);
        setTextWatcher(etPincode, etPincodeLayout);
        setTextWatcher(etCAddress, etCAddressLayout);
        setTextWatcher(etCity, etCityLayout);
        setTextWatcher(etState, etStateLayout);
        setTextWatcher(etEmail, etEmailLayout);
        setTextWatcher(etAnswerOne, etAnswerOneLayout);
        setTextWatcher(etAnswerTwo, etAnswerTwoLayout);
    }

    //textwatcher method for edittext
    private void setTextWatcher(final EditText editText, final TextInputLayout textInputLayout)
    {
        editText.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

                try
                {
                    if (s.length() > 0)
                    {
                        textInputLayout.setErrorEnabled(false);
                    }
                    if (editText == etPincode)
                    { // for pincode
                        if (s.length() >= 6)
                        {
                            if (!Utility.isConnectingToInternet(context))
                            {
                                return;
                            }
                            pbPinCode.setVisibility(VISIBLE);
                            API.sendRequestToServerGET(context, API.ZIPCODES + "/" + s.toString(), API.ZIPCODES); // getting zipode details
                        } else
                        {
                            if (!Utility.isConnectingToInternet(context))
                            { // checking net connection
                                return;
                            }
                            String trim = s.toString().trim();
                            if (!trim.isEmpty() && trim.length() >= 6)
                            {
                                pbPinCode.setVisibility(VISIBLE);
                                API.sendRequestToServerGET(context, API.ZIPCODE + trim, API.ZIPCODE); //service call for pincode list
                            }
                        }
                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }

    //setting click listener
    private void setClickListner()
    {
        cvAddEducation.setOnClickListener(this);
        cvSave.setOnClickListener(this);
        etDOB.setOnClickListener(this);
        ivHelp.setOnClickListener(this);
        ivMobile.setOnClickListener(this);
        ivDOB.setOnClickListener(this);
        ivChallenged.setOnClickListener(this);
        ivGender.setOnClickListener(this);
        ivAddress.setOnClickListener(this);
        ivEmail.setOnClickListener(this);
        ivQuestionTwo.setOnClickListener(this);
        ivQuestionOne.setOnClickListener(this);
        nvcontainer.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent event)
            {
                if (v != null)
                {
                    v.close();
                }
                return false;
            }
        });
    }

    //disabling error onf inputlayout
    void disableError()
    {
        etFirstNameLayout.setErrorEnabled(false);
        etLastNameLayout.setErrorEnabled(false);
        etDOBLayout.setErrorEnabled(false);
        etGenderLayout.setErrorEnabled(false);
        etPhoneLayout.setErrorEnabled(false);
        etPincodeLayout.setErrorEnabled(false);
        etCityLayout.setErrorEnabled(false);
        etStateLayout.setErrorEnabled(false);
        etEmailLayout.setErrorEnabled(false);
        etAnswerOneLayout.setErrorEnabled(false);
        etAnswerTwoLayout.setErrorEnabled(false);
    }

    void clearFocus()
    {
        etFirstName.clearFocus();
        etLastName.clearFocus();
        etDOB.clearFocus();
        etPhone.clearFocus();
        etCity.clearFocus();
        etState.clearFocus();
        etPincode.clearFocus();
        etEmail.clearFocus();
        etAnswerOne.clearFocus();
        etAnswerTwo.clearFocus();
        etQuestionOne.clearFocus();
        etQuestionTwo.clearFocus();
        etGender.clearFocus();
        etCAddress.clearFocus();
        etMiddleName.clearFocus();
        maLanguage.clearFocus();
    }

    //enabling error for input layout
    void enableError(TextInputLayout inputLayout, String error)
    {
        inputLayout.setErrorEnabled(true);
        inputLayout.setError(error);
//        inputLayout.requestFocus();
    }

    //setting toolbar and header title
    private void setToolbar()
    {
        Toolbar mToolbar = findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        TextView header = findViewById(R.id.header);
        header.setText(getResources().getString(R.string.app_my_profile_text));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v)
    {

        switch (v.getId())
        {
            case R.id.cv_save: // save click
                if (validation())
                {
                    try
                    {
                        if (!Utility.isConnectingToInternet(context))
                        { // check net connection
                            final Snackbar snackbar = Snackbar.make(v, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        HashMap<String, Object> input = new HashMap<>();

                        input.put(RequestParameters.USERID, "" + user.getUserId());
                        input.put(RequestParameters.FIRSTNAME, "" + etFirstName.getText().toString().trim());
                        input.put(RequestParameters.MIDDLENAME, "" + etMiddleName.getText().toString().trim());

                        input.put(RequestParameters.LASTNAME, "" + etLastName.getText().toString().trim());

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");
                        Date date = df_.parse(etDOB.getText().toString().trim());

                        input.put(RequestParameters.DATEOFBIRTH, "" + df.format(date));
                        input.put(RequestParameters.GENDER, "" + gender);
                        input.put(RequestParameters.EMAIL, "" + etEmail.getText().toString().trim());
                        input.put(RequestParameters.CITY, "" + etCity.getText().toString().trim());
                        input.put(RequestParameters.STATE, "" + etState.getText().toString().trim());
                        input.put(RequestParameters.ZIP, "" + etPincode.getText().toString().trim());
                        input.put(RequestParameters.ADDRESS, "" + etCAddress.getText().toString().trim());
                        input.put(RequestParameters.QUESTION_ID1, "" + questionId1);
                        input.put(RequestParameters.ANSWER1, "" + etAnswerOne.getText().toString().trim());
                        input.put(RequestParameters.QUESTION_ID2, "" + questionId2);
                        input.put(RequestParameters.ANSWER2, "" + etAnswerTwo.getText().toString().trim());
                        input.put(RequestParameters.TOKEN, "" + FirebaseInstanceId.getInstance().getToken());

                        if (maLanguage.getText().toString().trim().length() > 0)
                        {
                            String lang = maLanguage.getText().toString().trim().substring(0, maLanguage.getText().toString().trim().length() - 1);
                            input.put(RequestParameters.LANGUAGE, "" + lang.trim());
                        } else
                        {
                            input.put(RequestParameters.LANGUAGE, "");
                        }


                        if (cbEmail.isChecked())
                        {
                            input.put(RequestParameters.SHOW_EMAIL, "0");
                        } else
                        {
                            input.put(RequestParameters.SHOW_EMAIL, "1");
                        }
                        if (cbMobile.isChecked())
                        {
                            input.put(RequestParameters.SHOW_MOBILE, "0");
                        } else
                        {
                            input.put(RequestParameters.SHOW_MOBILE, "1");
                        }
                        if (cbAddress.isChecked())
                        {
                            input.put(RequestParameters.SHOW_ADDRESS, "0");
                        } else
                        {
                            input.put(RequestParameters.SHOW_ADDRESS, "1");
                        }
                        if (cbDOB.isChecked())
                        {
                            input.put(RequestParameters.SHOW_DOB, "0");
                        } else
                        {
                            input.put(RequestParameters.SHOW_DOB, "1");
                        }
                        if (cbGender.isChecked())
                        {
                            input.put(RequestParameters.SHOW_GENDER, "0");
                        } else
                        {
                            input.put(RequestParameters.SHOW_GENDER, "1");
                        }
                        if (!cbChallenged.isChecked())
                        {
                            input.put(RequestParameters.IS_CHALLENGED, "0");
                        } else
                        {
                            input.put(RequestParameters.IS_CHALLENGED, "1");
                        }
                        setDisable();

                        Log.e("request", "" + input);
                        System.out.println("Personal Info Post   " + input);
                        API.sendRequestToServerPOST_PARAM(context, API.PERSONAL_INFO, input); // saving personal info
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.etDOB: // click on dob
                showCal2(etDOB);
                break;
            case R.id.ivMobile:
                showToolTip(v, getResources().getString(R.string.mobile_visibility_helptext));
                break;
            case R.id.ivGender:
                showToolTip(v, getResources().getString(R.string.gender_visibility_helptext));
                break;
            case R.id.ivDOB:
                showToolTip(v, getResources().getString(R.string.dob_visibility_helptext));
                break;
            case R.id.ivAddress:
                showToolTip(v, getResources().getString(R.string.address_visibility_helptext));
                break;
            case R.id.ivEmail:
                showToolTip(v, getResources().getString(R.string.Email_visibility_helptext));
                break;
            case R.id.ivQuestionOne:
            case R.id.ivQuestionTwo:
                showToolTip(v, getResources().getString(R.string.forget_pwd_squestion_helptext));
                break;
            case R.id.ivChallenged:
                showToolTip(v, getString(R.string.help_tip_text_challenged));
                break;
            case R.id.cvAddEducation:
                Intent intent = new Intent(context, EducationActivity.class);
                Log.d("SILFRA", "USERID MyProfileActivity" + user.getUserId());
                intent.putExtra("USERID", user.getUserId());
                startActivity(intent);
                break;
        }
    }

    private void showCal2(EditText v)
    {
        Calendar instance = Calendar.getInstance();
        String[] split = {};
        String setDate = v.getText().toString();

        int year = instance.get(Calendar.YEAR);
        int monthIndexedFromZero = instance.get(Calendar.MONTH);
        int day = instance.get(Calendar.DAY_OF_MONTH);

        if (!setDate.isEmpty())
        {
            split = setDate.split("-");
            year = Integer.parseInt(split[2]);
            monthIndexedFromZero = Integer.parseInt(split[1]) - 1;
            day = Integer.parseInt(split[0]);
        }

        new SpinnerDatePickerDialogBuilder()
                .context(MyProfileActivity.this)
                .callback(new DatePickerDialog.OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        MyProfileActivity.this.year = year;
                        MyProfileActivity.this.month = monthOfYear;
                        MyProfileActivity.this.day = dayOfMonth;
                        monthOfYear += 1;
                        String month = "" + monthOfYear;
                        String date = "" + dayOfMonth;
                        if (date.length() == 1)
                        {
                            date = "0" + date;
                        }
                        if (month.length() == 1)
                        {
                            month = "0" + month;
                        }
                        //etDOB.setText(year + "-" + month + "-" + date);
                        MyProfileActivity.this.etDOB.setText(date + "-" + month + "-" + year);
                    }
                })
                .spinnerTheme(R.style.NumberPickerStyle)
                .showTitle(true)
                .showDaySpinner(true)
                .defaultDate(year, monthIndexedFromZero, day)
                .maxDate(instance.get(Calendar.YEAR) - 16, instance.get(Calendar.MONTH), instance.get(Calendar.DAY_OF_MONTH))
                .minDate(instance.get(Calendar.YEAR) - 100, instance.get(Calendar.MONTH), instance.get(Calendar.DAY_OF_MONTH))
                .build()
                .show();
    }


    private void showCal()
    {
        DatePickerFragmentDialog datePickerDialog = DatePickerFragmentDialog.newInstance(new DatePickerFragmentDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePickerFragmentDialog view, int year, int monthOfYear, int dayOfMonth)
            {
                MyProfileActivity.this.year = year;
                MyProfileActivity.this.month = monthOfYear;
                MyProfileActivity.this.day = dayOfMonth;
                monthOfYear += 1;
                String month = "" + monthOfYear;
                String date = "" + dayOfMonth;
                if (date.length() == 1)
                {
                    date = "0" + date;
                }
                if (month.length() == 1)
                {
                    month = "0" + month;
                }
                //etDOB.setText(year + "-" + month + "-" + date);
                etDOB.setText(date + "-" + month + "-" + year);
            }
        }, year, month, day);


        try
        {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date date = df.parse("1947-01-01");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            datePickerDialog.setMinDate(calendar.getTimeInMillis());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, -18);
            datePickerDialog.setMaxDate(cal.getTimeInMillis());
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        datePickerDialog.setTitle("");
        datePickerDialog.show(getSupportFragmentManager(), "");
    }

    void showToolTip(View view, String toolTipTExt)
    {
        if (v != null)
        {
            v.close();
        }
        v = ViewTooltip
                .on(view)
                .autoHide(true, 20000)
                .clickToHide(true)
                .align(CENTER)
                .position(TOP)
                .text(toolTipTExt)
                .textColor(Color.WHITE)
                .color(getResources().getColor(R.color.colorPrimary))
                .corner(10)
                .onHide(new ViewTooltip.ListenerHide()
                {
                    @Override
                    public void onHide(View view)
                    {
                        v = null;
                    }
                });
        v.show();
    }

    //validation method
    private boolean validation()
    {
        disableError();
        clearFocus();
        boolean result = true;
        boolean fNameMatcher = Pattern.matches("[a-zA-Z0-9 ]+", etFirstName.getText().toString().trim());
        boolean lNameMatcher = Pattern.matches("[a-zA-Z0-9 ]+", etLastName.getText().toString().trim());
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etFirstName.getWindowToken(), 0);


        if (etFirstName.getText().toString().trim().isEmpty())
        {
            if (result)
            {
                etFirstName.requestFocus();
                imm.showSoftInput(etFirstName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etFirstNameLayout, getResources().getString(R.string.firstname_empty_input));
            if (errorLayout == null)
            {
                errorLayout = etFirstNameLayout;
            }
            result = false;
        } else if (!fNameMatcher)
        {
            if (result)
            {
                etFirstName.requestFocus();
                imm.showSoftInput(etFirstName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etFirstNameLayout, getResources().getString(R.string.name_incorrect_input_numeric));
            if (errorLayout == null)
            {
                errorLayout = etFirstNameLayout;
            }
            result = false;
        }
        if (etLastName.getText().toString().trim().isEmpty())
        {
            if (result)
            {
                etLastName.requestFocus();
                imm.showSoftInput(etFirstName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etLastNameLayout, getResources().getString(R.string.lastname_empty_input));
            if (errorLayout == null)
            {
                errorLayout = etLastNameLayout;
            }
            result = false;
        } else if (!lNameMatcher)
        {
            if (result)
            {
                etFirstName.requestFocus();
                imm.showSoftInput(etFirstName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etLastNameLayout, getResources().getString(R.string.name_incorrect_input_numeric));
            if (errorLayout == null)
            {
                errorLayout = etLastNameLayout;
            }
            result = false;
        }
        if (etDOB.getText().toString().trim().isEmpty())
        {
            enableError(etDOBLayout, getResources().getString(R.string.dob_empty_input));
            result = false;
        }
        if (gender.trim().isEmpty())
        {
            enableError(etGenderLayout, getResources().getString(R.string.gender_choose_option));
            result = false;
        }
        if (etPhone.getText().toString().trim().isEmpty())
        {
            if (result)
            {
                etPhone.requestFocus();
                imm.showSoftInput(etPhone, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etPhoneLayout, getResources().getString(R.string.mobile_empty_input));
            result = false;
        }
       /* if (!etEmail.getText().toString().trim().isEmpty() && !Utility.isEmailIdValid(etEmail.getText().toString().trim())) {
            if (result) {
              //  etEmail.requestFocus();
                imm.showSoftInput(etEmail, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etEmailLayout, getResources().getString(R.string.email_incorrect_input_format));
            result = false;
        }*/
       /* if (etCAddress.getText().toString().trim().isEmpty()) {
            if (result) {
                etCAddress.requestFocus();
                imm.showSoftInput(etCAddress, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etCAddressLayout, getResources().getString(R.string.address_empty_input));
            result = false;
        }*/
        if (etPincode.getText().toString().trim().isEmpty())
        {
            enableError(etPincodeLayout, getResources().getString(R.string.zipcode_empty_input));
            if (result)
            {
                etPincode.requestFocus();
                imm.showSoftInput(etPincode, InputMethodManager.SHOW_IMPLICIT);
            }
            if (errorLayout == null)
            {
                errorLayout = etPincodeLayout;
            }
            result = false;
        } else if (etPincode.getText().toString().trim().length() < 6)
        {
            enableError(etPincodeLayout, getResources().getString(R.string.zipcode_incorrect_input));
            if (result)
            {
                etPincode.requestFocus();
                imm.showSoftInput(etPincode, InputMethodManager.SHOW_IMPLICIT);
            }
            if (errorLayout == null)
            {
                errorLayout = etPincodeLayout;
            }
            result = false;
        }
        if (etCity.getText().toString().trim().isEmpty())
        {
            /*etCity.requestFocus();
            imm.showSoftInput(etCity, InputMethodManager.SHOW_IMPLICIT);*/
            enableError(etCityLayout, getResources().getString(R.string.city_empty_input));
            result = false;
        }
        if (etState.getText().toString().trim().isEmpty())
        {
            /*etState.requestFocus();
            imm.showSoftInput(etState, InputMethodManager.SHOW_IMPLICIT);*/
            enableError(etStateLayout, getResources().getString(R.string.state_empty_input));
            result = false;
        }
        if(!educationFlag)
        {
            result = false;
            Toast.makeText(context, "Please Enter Educational Details", Toast.LENGTH_LONG).show();
        }

        /*if (etAnswerOne.getText().toString().trim().isEmpty()) {
            if (result) {
                etAnswerOne.requestFocus();
                imm.showSoftInput(etAnswerOne, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etAnswerOneLayout, getResources().getString(R.string.security_answer_empty_input));
            result = false;
        }
        if (etAnswerTwo.getText().toString().trim().isEmpty()) {
            if (result) {
                etAnswerTwo.requestFocus();
                imm.showSoftInput(etAnswerTwo, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etAnswerTwoLayout, getResources().getString(R.string.security_answer_empty_input));
            result = false;
        }*/ /*else if (!cbTerms.isChecked()) {
            final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.accept_terms_error), Toast.LENGTH_SHORT);
            snackbar.show();
            return false;
        }*/

        if (!result && errorLayout != null)
        {
            errorLayout.requestFocus();
        } else if (!result)
        {
            nvcontainer.smoothScrollTo(0, 0);
        }

        errorLayout = null;
        return result;
    }

    //enable views
    void setEnable()
    {
        tvSave.setVisibility(VISIBLE);
        pbLoading.setVisibility(GONE);
        cvSave.setClickable(true);
        rgGenderGroup.setEnabled(true);
        rbFemale.setEnabled(true);
        rbMale.setEnabled(true);
        rbOther.setEnabled(true);
        etFirstName.setEnabled(true);
        etLastName.setEnabled(true);
        etDOB.setEnabled(true);
        etPhone.setEnabled(true);
        etPincode.setEnabled(true);
        etCity.setEnabled(true);
        etState.setEnabled(true);
        etEmail.setEnabled(true);
        etAnswerOne.setEnabled(true);
        etAnswerTwo.setEnabled(true);
        etGender.setEnabled(true);
        spQuestionOne.setEnabled(true);
        spQuestionTwo.setEnabled(true);
        cbTerms.setClickable(true);
        etQuestionOne.setEnabled(true);
        etQuestionTwo.setEnabled(true);
        cbAddress.setClickable(true);
        cbAddress.setEnabled(true);
        cbEmail.setClickable(true);
        cbEmail.setEnabled(true);
        cbMobile.setClickable(true);
        cbMobile.setEnabled(true);
        cbDOB.setClickable(true);
        cbDOB.setEnabled(true);
        cbGender.setClickable(true);
        cbGender.setEnabled(true);
        maLanguage.setEnabled(true);
    }

    //disable views
    private void setDisable()
    {
        tvSave.setVisibility(GONE);
        pbLoading.setVisibility(VISIBLE);
        cvSave.setClickable(false);
        rgGenderGroup.setEnabled(false);
        rbFemale.setEnabled(false);
        rbMale.setEnabled(false);
        rbOther.setEnabled(false);
        etFirstName.setEnabled(false);
        etLastName.setEnabled(false);
        etDOB.setEnabled(false);
        etPhone.setEnabled(false);
        etPincode.setEnabled(false);
        etCity.setEnabled(false);
        etState.setEnabled(false);
        etEmail.setEnabled(false);
        etAnswerOne.setEnabled(false);
        etAnswerTwo.setEnabled(false);
        etGender.setEnabled(false);
        etQuestionOne.setEnabled(false);
        etQuestionTwo.setEnabled(false);
        spQuestionOne.setEnabled(false);
        spQuestionTwo.setEnabled(false);
        cbTerms.setClickable(false);
        cbAddress.setClickable(false);
        cbAddress.setEnabled(false);
        cbEmail.setClickable(false);
        cbEmail.setEnabled(false);
        cbMobile.setClickable(false);
        cbMobile.setEnabled(false);
        cbDOB.setClickable(false);
        cbDOB.setEnabled(false);
        cbGender.setClickable(false);
        cbGender.setEnabled(false);
        maLanguage.setEnabled(false);
    }

    //getresponse from service
    public void getResponse(JSONObject outPut, int type)
    {
        try
        {
            System.out.println("My Profile Response" + outPut.toString());
            setEnable();
            if (type == 0)
            { // saving personal info
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY"))
                {
                    if (outPut.has(ResponseParameters.Errors))
                    { // error
                        final Snackbar snackbar = Snackbar.make(cvSave, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else
                    {
                        if (outPut.has(ResponseParameters.Success))
                        { // success
                            if (outPut.getString(ResponseParameters.Success).equalsIgnoreCase("success"))
                            {
                                if (outPut.has(ResponseParameters.PersonalInformation))
                                {
                                    JSONObject PERSONALINFO = outPut.getJSONObject(ResponseParameters.PersonalInformation);

                                    Log.e("MYPROFILEACTIVITY", "Profile info is : " + PERSONALINFO);
                                    user.setFirstName(PERSONALINFO.getString(ResponseParameters.FirstName));
                                    user.setMiddleName(PERSONALINFO.getString(ResponseParameters.MiddleName));
                                    user.setLastName(PERSONALINFO.getString(ResponseParameters.LastName));
                                    user.setDOB(PERSONALINFO.getString(ResponseParameters.DateOfBirth));
                                    user.setGender(PERSONALINFO.getString(ResponseParameters.Gender));
                                    user.setEmail(PERSONALINFO.getString(ResponseParameters.Email));
                                    user.setAvatar(PERSONALINFO.getString(ResponseParameters.Avatar));
                                    user.setAddress(PERSONALINFO.getString(ResponseParameters.Address));
                                    user.setCity(PERSONALINFO.getString(ResponseParameters.City));
                                    user.setState(PERSONALINFO.getString(ResponseParameters.State));
                                    user.setZipCode(PERSONALINFO.getString(ResponseParameters.ZIP));
                                    user.setEmailVisibility(PERSONALINFO.getString(ResponseParameters.EMAIL_VISIBILITY));
                                    user.setMobileVisibility(PERSONALINFO.getString(ResponseParameters.MOBILE_VISIBILITY));
                                    user.setAddressVisibility(PERSONALINFO.getString(ResponseParameters.ADDRESS_VISIBILITY));
                                    if (PERSONALINFO.has(ResponseParameters.AlternateMobile))
                                        if (!PERSONALINFO.getString(ResponseParameters.AlternateMobile).equalsIgnoreCase("null") && PERSONALINFO.getString(ResponseParameters.AlternateMobile) != null)
                                            user.setAlternateMobile(PERSONALINFO.getString(ResponseParameters.AlternateMobile));
                                    if (PERSONALINFO.has(ResponseParameters.PROFILE_SUMMERY))
                                        if (!PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY).equalsIgnoreCase("null") && PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY) != null)
                                            user.setProfileSummary(PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY));

                                    if (PERSONALINFO.has(ResponseParameters.PROFILE_HEADLINE))
                                        if (!PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE).equalsIgnoreCase("null") && PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE) != null)
                                            user.setProfileHeadline(PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE));
                                }
                                SharedPreferencesMethod.setUserDetails(this, user);

                                Intent intent = new Intent(context, HomeScreenActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                this.finish();
                            } else
                            {
                                final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                                snackbar.show();
                            }
                        } else
                        {
                            final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                            snackbar.show();
                        }
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR"))
                { // error
                    final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK"))
                { //error
                    final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            }
            if (type == 1)
            { //getting zipcode details for setting city and state
                pbPinCode.setVisibility(GONE);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY"))
                {
                    if (outPut.has(ResponseParameters.Success))
                    {
                        if (outPut.has(ResponseParameters.ZIPCODES))
                        {
                            if (outPut.getJSONObject(ResponseParameters.ZIPCODES).has(ResponseParameters.State))
                            {
                                etState.setText(outPut.getJSONObject(ResponseParameters.ZIPCODES).getString(ResponseParameters.State));
                            }
                            if (outPut.getJSONObject(ResponseParameters.ZIPCODES).has(ResponseParameters.DISTRICT))
                            {
                                etCity.setText(outPut.getJSONObject(ResponseParameters.ZIPCODES).getString(ResponseParameters.DISTRICT));
                            }
                        }
                    } else
                    {
                        etPincode.setText("");
                        enableError(etPincodeLayout, getResources().getString(R.string.zipcode_incorrect_input));
                    }
                } else
                {
                    etPincode.setText("");
                    enableError(etPincodeLayout, getResources().getString(R.string.zipcode_incorrect_input));
                }
            }
            if (type == 2 || type == 4)
            { // getting zipcode list
                pbPinCode.setVisibility(GONE);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY"))
                {
                    if (outPut.has(ResponseParameters.Success))
                    { //check output have success
                        if (outPut.has(ResponseParameters.ZIPCODES))
                        { //check output have zipcode
                            List<String> roles = new ArrayList<>();
                            for (int j = 0; j < outPut.getJSONArray(ResponseParameters.ZIPCODES).length(); j++)
                            {
                                roles.add(outPut.getJSONArray(ResponseParameters.ZIPCODES).getJSONObject(j).getString(ResponseParameters.ZIP));
                            }
                            String[] data = roles.toArray(new String[roles.size()]);
                            ArrayAdapter<?> adapter = new ArrayAdapter<Object>(this, R.layout.autocomplete_item, data);
                            etPincode.setAdapter(adapter); // setting adapter
                            etPincode.setThreshold(1);
                            if (!etPincode.getText().toString().trim().isEmpty() || type == 4)
                                etPincode.showDropDown();
                        }
                    }
                }
            }
            if (type == 3)
            {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY"))
                {
                    if (outPut.has(ResponseParameters.Errors))
                    { // error

                    } else if (outPut.has(ResponseParameters.Success))
                    {
                        if (outPut.has(ResponseParameters.ALL_LANGUAGES))
                        { //check output have language
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.ALL_LANGUAGES, outPut.toString());
                            setUpLanguageAdapter(outPut, maLanguage);
                        }
                    }
                }
            }
        } catch (Exception e)
        {
            final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
            snackbar.show();
            e.printStackTrace();
        }
    }

    /**
     * <p>Callback method to be invoked when an item in this view has been
     * selected. This callback is invoked only when the newly selected
     * position is different from the previously selected position or if
     * there was no selected item.</p>
     * <p>
     * Impelmenters can call getItemAtPosition(position) if they need to access the
     * data associated with the selected item.
     *
     * @param parent   The AdapterView where the selection happened
     * @param view     The view within the AdapterView that was clicked
     * @param position The position of the view in the adapter
     * @param id       The row id of the item that is selected
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        try
        {
            switch (parent.getId())
            {
                case R.id.spQuestionOne:
                    questionId1 = questionSetOne.getJSONObject(position).getInt(ResponseParameters.Id);// setting question id one
                    break;
                case R.id.spQuestionTwo:
                    questionId2 = questionSetTwo.getJSONObject(position).getInt(ResponseParameters.Id);// setting question id two
                    break;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Callback method to be invoked when the selection disappears from this
     * view. The selection can disappear for instance when touch is activated
     * or when the adapter becomes empty.
     *
     * @param parent The AdapterView that now contains no selected item.
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    // setEducationRecyclerView creating list for Education details
    private void setEducationRecyclerView()
    {
        try
        {
            // setting options for education recycler view
            linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvEducationList.setLayoutManager(linearLayoutManager);
            rvEducationList.setNestedScrollingEnabled(false);
            // rvEducationList.setHasFixedSize(true);
            if (profile.has(ResponseParameters.EDUCATION_DETAIL))
            { // checking user have education details
                educations = new ArrayList<>();
                for (int i = 0; i < profile.getJSONArray(ResponseParameters.EDUCATION_DETAIL).length(); i++)
                {
                    JSONObject jsonObject = profile.getJSONArray(ResponseParameters.EDUCATION_DETAIL).getJSONObject(i);
                    Education education = new Education();
                    education.setId(jsonObject.getString(ResponseParameters.Id));
                    education.setCourse(jsonObject.getString(RequestParameters.COURSE));
                    education.setSchool(jsonObject.getString(RequestParameters.SCHOOL));
                    education.setYearOfPassing(jsonObject.getString(RequestParameters.YEAR));
                    education.setDetails(jsonObject.getString(RequestParameters.DESCRIPTION));
                    educations.add(education);
                }
                educationListAdapter = new EducationListAdapter(this, educations);
                Log.e("profile", "" + educations.size());
                rvEducationList.setAdapter(educationListAdapter);

                if (profile.getJSONArray(ResponseParameters.EDUCATION_DETAIL).length() <= 0)
                { // checking if user have don't have education data
                    rvEducationList.setVisibility(GONE);
                    cvAddEducation.setVisibility(VISIBLE);
                } else
                {
                    rvEducationList.setVisibility(VISIBLE);
                    cvAddEducation.setVisibility(VISIBLE);
                }
            } else
            {
                rvEducationList.setVisibility(VISIBLE);
                cvAddEducation.setVisibility(VISIBLE);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
