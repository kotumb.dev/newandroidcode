package com.codeholic.kotumb.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by DELL on 1/8/2018.
 */

public class Note implements Parcelable, Serializable{
    private String noteID = "";
    private String noteTitle = "";
    private String noteDes = "";
    private String noteDate = "";
    private String noteTime = "";

    protected Note(Parcel in) {
        noteID = in.readString();
        noteTitle = in.readString();
        noteDes = in.readString();
        noteDate = in.readString();
        noteTime = in.readString();
    }

    public Note(){

    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    public String getNoteID() {
        return noteID;
    }

    public void setNoteID(String noteID) {
        this.noteID = noteID;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public String getNoteDes() {
        return noteDes;
    }

    public void setNoteDes(String noteDes) {
        this.noteDes = noteDes;
    }

    public String getNoteDate() {
        return noteDate;
    }

    public void setNoteDate(String noteDate) {
        this.noteDate = noteDate;
    }

    public String getNoteTime() {
        return noteTime;
    }

    public void setNoteTime(String noteTime) {
        this.noteTime = noteTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(noteID);
        dest.writeString(noteTitle);
        dest.writeString(noteDes);
        dest.writeString(noteDate);
        dest.writeString(noteTime);
    }
}
