package com.codeholic.kotumb.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class VideoData implements Parcelable {


    String videoType="";
    String videoName="";
    String videoStatus="";
    String videoUrl="";
    String like="";
    String videoCount="";
    String videoTitle="";
    String videoApproved="";
    String videoDescription="";
    String userFirstName="";
    String userLastName="";
    String videoUploadedDate="";
    String videoEndDate="";
    String userAvatar="";
    String videoId="";
    String userId="";
    String videoLikeCount="";
    String shareMessage="";
    String videoThumbnail="";
    String videoPublish="";


    public VideoData(){

    }



    protected VideoData(Parcel in) {
        videoType = in.readString();
        videoName = in.readString();
        videoStatus = in.readString();
        videoApproved=in.readString();
        like=in.readString();
        videoUrl=in.readString();
        videoTitle = in.readString();
        videoPublish=in.readString();
        videoDescription = in.readString();
        videoCount=in.readString();
        userFirstName = in.readString();
        userLastName = in.readString();
        videoUploadedDate = in.readString();
        videoEndDate = in.readString();
        userAvatar = in.readString();
        videoId = in.readString();
        userId=in.readString();
        videoLikeCount=in.readString();
        shareMessage=in.readString();
        videoThumbnail=in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(videoType);
        dest.writeString(videoName);
        dest.writeString(videoStatus);
        dest.writeString(videoTitle);
        dest.writeString(like);
        dest.writeString(videoDescription);
        dest.writeString(videoId);
        dest.writeString(videoUrl);
        dest.writeString(videoApproved);
        dest.writeString(videoPublish);
        dest.writeString(userFirstName);
        dest.writeString(userLastName);
        dest.writeString(videoCount);
        dest.writeString(videoUploadedDate);
        dest.writeString(videoEndDate);
        dest.writeString(userAvatar);
        dest.writeString(userId);
        dest.writeString(userId);
        dest.writeString(shareMessage);
        dest.writeString(videoThumbnail);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VideoData> CREATOR = new Creator<VideoData>() {
        @Override
        public VideoData createFromParcel(Parcel in) {
            return new VideoData(in);
        }

        @Override
        public VideoData[] newArray(int size) {
            return new VideoData[size];
        }
    };

    public String getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(String videoCount) {
        this.videoCount = videoCount;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getLike() {
        return like;
    }

    public String getVideoApproved() {
        return videoApproved;
    }

    public void setVideoApproved(String videoApproved) {
        this.videoApproved = videoApproved;
    }

    public String getVideoPublish() {
        return videoPublish;
    }

    public void setVideoPublish(String videoPublish) {
        this.videoPublish = videoPublish;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }


    public void setVideoStatus(String videoStatus) {
        this.videoStatus = videoStatus;
    }


    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }


    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    public String getVideoDescription() {
        return videoDescription;
    }


    public String getVideoId() {
        return videoId;
    }


    public String getVideoName() {
        return videoName;
    }


    public String getVideoStatus() {
        return videoStatus;
    }

    public String getVideoTitle() {
        return videoTitle;
    }


    public String getVideoType() {
        return videoType;
    }


    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public void setVideoUploadedDate(String videoUploadedDate) {
        this.videoUploadedDate = videoUploadedDate;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public String getVideoUploadedDate() {
        return videoUploadedDate;
    }

    public void setVideoEndDate(String videoEndDate) {
        this.videoEndDate = videoEndDate;
    }

    public String getUserId() {
        return userId;
    }


    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVideoEndDate() {
        return videoEndDate;
    }

    public void setVideoLikeCount(String videoLikeCount) {
        this.videoLikeCount = videoLikeCount;
    }

    public String getVideoLikeCount() {
        return videoLikeCount;
    }


    public String getShareMessage() {
        return shareMessage;
    }

    public void setShareMessage(String shareMessage) {
        this.shareMessage = shareMessage;
    }


    public String getVideoThumbnail() {
        return videoThumbnail;
    }


    public void setVideoThumbnail(String videoThumbnail) {
        this.videoThumbnail = videoThumbnail;
    }
}
