package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Adapter.VideoLikeListAdapter;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.Model.VideoData;
import com.codeholic.kotumb.app.Model.VideoLikes;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.View.DividerItemDecoration;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

/*This VideoDetailsActivity Made By Arpit Kanda*/

public class ShowVideoActivity extends AppCompatActivity  implements Player.EventListener{
    private TextView tvUserName,tvTime,tvVideoDescription,tvLike,header,tv_video_title,ls_video_status,tv_likes_count,dialog_termsContionsHeading,dialog_termsConditionText,publishVideo;
    private ImageView ivUserAvatar,ivDefaulticon,ivThumbnail,ivCross,dismiss_dialog,videoStatus_logo,play_icon,publishLogo;
    private RelativeLayout rl_videoContainer,dialog_headerLayout;
    private VideoView videoview;
    private WebView dialog_web_view;
    private SimpleExoPlayer player;
    private PlayerView playerView;
    private ProgressBar spinnerVideoDetails;
    private LinearLayout video_like,dialog_termsContionsLayout,dialog_llLoading,video_share,llEditPost;
    private RecyclerView rv_like_list;
    private FrameLayout videoThumbnailFrame;
    private  Toolbar mToolbar;
    boolean clicked=true;
    String activityType="";
    private User user;
    LinearLayoutManager linearLayoutManager;
    VideoLikeListAdapter videoLikeListAdapter;
    List<VideoLikes> videoLikesList=new ArrayList<VideoLikes>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_details);
        findViews();
        initToolbar();
        setVisibilty();
        initStartup();
    }

    private void dialogfindViews(View dialogView){
        dialog_web_view = dialogView.findViewById(R.id.web_view);
        dialog_termsContionsLayout = dialogView.findViewById(R.id.condition_message_layout);
        dialog_termsContionsHeading = dialogView.findViewById(R.id.condtion_heading);
        dialog_termsConditionText=dialogView.findViewById(R.id.condtion_text);
        dialog_headerLayout=dialogView.findViewById(R.id.header_layout);
        dismiss_dialog=dialogView.findViewById(R.id.dismiss_dialog);
        rv_like_list=dialogView.findViewById(R.id.rv_user_like_list);
        dialog_llLoading=dialogView.findViewById(R.id.llLoading);
    }



    private void findViews(){
        videoview=findViewById(R.id.videoview);
        ivThumbnail=findViewById(R.id.video_thumbnail);
        ivDefaulticon=findViewById(R.id.default_icon);
        rl_videoContainer=findViewById(R.id.play_video_container);
        playerView=findViewById(R.id.play_video);
        publishVideo=findViewById(R.id.ls_video_publish_status);
        publishLogo=findViewById(R.id.publishVideoStatus_logo);
        spinnerVideoDetails=findViewById(R.id.video_loader);
        tvVideoDescription=findViewById(R.id.ls_video_description);
        tvUserName=findViewById(R.id.tvUserName);
        ivUserAvatar=findViewById(R.id.ivUserImage);
        ivCross=findViewById(R.id.remove_video);
        tvLike=findViewById(R.id.tvLike);
        video_like=findViewById(R.id.video_like);
        mToolbar=findViewById(R.id.toolbar_actionbar);
        videoStatus_logo=findViewById(R.id.videoStatus_logo);
        header=findViewById(R.id.header);
        ls_video_status=findViewById(R.id.ls_video_status);
        tv_likes_count=findViewById(R.id.tv_likes_count);
        tvTime=findViewById(R.id.tvTime);
        video_share=findViewById(R.id.video_share);
        llEditPost=findViewById(R.id.llEditPost);
        videoThumbnailFrame=findViewById(R.id.videoThumbnailFrame);
        play_icon=findViewById(R.id.play_icon);
    }


    private void setVisibilty(){
        ivDefaulticon.setVisibility(View.GONE);
        ivCross.setVisibility(View.GONE);
        rl_videoContainer.setVisibility(View.VISIBLE);
        spinnerVideoDetails.setVisibility(View.VISIBLE);
        publishLogo.setVisibility(View.GONE);
        publishVideo.setVisibility(View.GONE);
        ls_video_status.setVisibility(View.GONE);
        videoStatus_logo.setVisibility(View.GONE);
        llEditPost.setVisibility(View.GONE);
        //  tv_likes_count.setVisibility(View.GONE);
    }



    private void initStartup(){
        //tv_video_title.setVisibility(View.VISIBLE);
        Intent intent = getIntent();
        if (intent.hasExtra("video_id")){
            VideoData videoData=new VideoData();
            String video_id=intent.getStringExtra("video_id");
            String video_title=intent.getStringExtra("video_title");
            String video_desp=intent.getStringExtra("video_desp");
            String video_like_count=intent.getStringExtra("video_like_count");
            String video_date_time=intent.getStringExtra("video_date_time");
            String video_user=intent.getStringExtra("video_user");
            String user_avatar=intent.getStringExtra("video_avatar");
            String video_uri=intent.getStringExtra("video_uri");
            videoData.setUserFirstName(video_user);
            videoData.setVideoUploadedDate(video_date_time);
            videoData.setVideoTitle(video_title);
            videoData.setVideoLikeCount(video_like_count);
            videoData.setVideoId(video_id);
            videoData.setVideoDescription(video_desp);
            videoData.setVideoName(video_uri);
            videoData.setUserAvatar(user_avatar);

            if(video_desp.length()>0) {
                tvVideoDescription.setText(getResources().getString(R.string.video_description_text_1) + " " + video_desp);
            }
            else tvVideoDescription.setVisibility(View.GONE);

            tvUserName.setText(SharedPreferencesMethod.getUserInfo(this).getFirstName()+" "+SharedPreferencesMethod.getUserInfo(this).getLastName());

            if(intent.hasExtra("activitytype")){
                tvUserName.setText(intent.getStringExtra("fullname"));
                new AQuery(this).id(ivUserAvatar).image(API.imageUrl +intent.getStringExtra("avatar"), true, true, 300, R.drawable.default_profile);
                if(intent.getStringExtra("activitytype").equals("videoapproved")){
                    activityType="videoapproved";
                    video_like.setVisibility(View.GONE);
                    new AQuery(this).id(ivUserAvatar).image(API.imageUrl + SharedPreferencesMethod.getUserInfo(this).getAvatar(), true, true, 300, R.drawable.default_profile);
                    publishLogo.setVisibility(View.VISIBLE);
                    publishVideo.setVisibility(View.VISIBLE);
                    ls_video_status.setVisibility(View.VISIBLE);
                    videoStatus_logo.setVisibility(View.VISIBLE);
                    ls_video_status.setText(getResources().getString(R.string.video_status_text)+" "+getResources().getString(R.string.video_approved_text));
                    publishVideo.setText("Publish Status: "+getResources().getString(R.string.pending_text));
                    videoStatus_logo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check_mark));
                    publishLogo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.pending));
                }
                else if(intent.getStringExtra("activitytype").equals("publishedvideonotification")){
                    activityType="publishedvideonotification";
                    tv_likes_count.setVisibility(View.VISIBLE);
                    video_like.setVisibility(View.VISIBLE);
                    new AQuery(this).id(ivUserAvatar).image(API.imageUrl + SharedPreferencesMethod.getUserInfo(this).getAvatar(), true, true, 300, R.drawable.default_profile);
                    String url=API.VIDEO_LIKES+video_id+"/"+ SharedPreferencesMethod.getUserId(ShowVideoActivity.this)+"/"+0;
                    new AQuery(this).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
                        @Override
                        public void callback(String url, JSONObject object, AjaxStatus status) {
                            super.callback(url, object, status);
                            System.out.println("Video Like Response  "+object.toString());
                            try{
                                if (object.has(ResponseParameters.Success)){
                                    object.getString("like_count");
                                    int totalLikes=Integer.parseInt(object.getString("like_count"));
                                    tv_likes_count.setText("Likes: "+String.valueOf(totalLikes));
                                }
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }

                        }
                    });
                }
            }
            else{
                new AQuery(this).id(ivUserAvatar).image(API.imageUrl +user_avatar, true, true, 300, R.drawable.default_profile);
            }
            // tv_video_title.setText(video_title);
            tvTime.setText(video_date_time);
            tv_likes_count.setText(video_like_count);
            playVideo(Uri.parse(API.user_videos+video_uri));
            String url=API.VIDEO_LIKES+video_id+"/"+ SharedPreferencesMethod.getUserId(ShowVideoActivity.this)+"/"+1;
            System.out.println("Video Like URL   "+url);
            videoLikes(video_id);
            setOnClick(video_id,videoData);
        }else if (intent.hasExtra("video_Uri")){
            VideoData videoData=new VideoData();
            user = intent.getParcelableExtra("user");
            String uri=intent.getStringExtra("videoUri");
            String description=intent.getStringExtra("videoDescription");
            String video_id=intent.getStringExtra("videoId");
            String userName=intent.getStringExtra("video_userName");
            videoData.setUserFirstName(SharedPreferencesMethod.getUserInfo(this).getFirstName());
            videoData.setUserLastName(SharedPreferencesMethod.getUserInfo(this).getLastName());
            videoData.setVideoId(video_id);
            videoData.setVideoDescription(getResources().getString(R.string.video_description_text_1)+" "+description);
            videoData.setVideoName(uri);
            tvUserName.setText(SharedPreferencesMethod.getUserInfo(this).getFirstName()+" "+SharedPreferencesMethod.getUserInfo(this).getLastName());
            tvVideoDescription.setText(description);
            new AQuery(this).id(ivUserAvatar).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.default_profile);
            playVideo(Uri.parse(API.user_videos+uri));
            String url=API.VIDEO_LIKES+video_id+"/"+ SharedPreferencesMethod.getUserId(ShowVideoActivity.this)+"/"+1;
            System.out.println("Video Like URL   "+url);
            videoLikes(video_id);
            setOnClick(video_id,videoData);
        }else if (intent.hasExtra("videoUri")){
            videoGetData(intent);
        }
    }




    private void videoGetData(Intent intent){
        VideoData videoData=new VideoData();
        user = intent.getParcelableExtra("user");
        String uri=intent.getStringExtra("videoUri");
        String description=intent.getStringExtra("videoDescription");
        String video_id=intent.getStringExtra("videoId");
        String userName=intent.getStringExtra("video_userName");
        String fname=intent.getStringExtra("fname");
        String lname=intent.getStringExtra("laname");
        videoData.setUserFirstName(user.getFirstName());
        videoData.setUserLastName(user.getLastName());
        videoData.setVideoId(video_id);
        videoData.setVideoDescription(getResources().getString(R.string.video_description_text_1)+" "+description);
        videoData.setVideoName(uri);
        System.out.println("check==="+user.getFirstName()+" "+user.getLastName()+"=="+user.getAvatar());
        tvUserName.setText(user.getFirstName()+" "+user.getLastName());
        tvVideoDescription.setText(description);
        new AQuery(this).id(ivUserAvatar).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.default_profile);
        playVideo(Uri.parse(API.user_videos+uri));
        String url=API.VIDEO_LIKES+video_id+"/"+ SharedPreferencesMethod.getUserId(ShowVideoActivity.this)+"/"+0;
        new AQuery(this).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                System.out.println("Video Like Response  "+object.toString());
                try{
                    if (object.has(ResponseParameters.Success)){
                        object.getString("like_count");
                        int totalLikes=Integer.parseInt(object.getString("like_count"));
                        tv_likes_count.setText("Likes: "+String.valueOf(totalLikes));
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }

            }
        });
        System.out.println("Video Like URL   "+url);
        videoLikes(video_id);
        setOnClick(video_id,videoData);
    }





    private void LikePopup(final Activity activity,String videoId){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.video_terms_condition_layout, null);
        dialogBuilder.setView(dialogView);
        dialogfindViews(dialogView);
        FrameLayout frameLayout=dialogView.findViewById(R.id.frameLayout);
        frameLayout.setVisibility(View.GONE);
        dialog_web_view.setVisibility(View.GONE);
        dialog_headerLayout.setVisibility(View.VISIBLE);
        dialog_termsConditionText.setVisibility(View.GONE);
        dialog_termsContionsLayout.setVisibility(View.VISIBLE);
        dialog_termsContionsHeading.setText("User Like List");
        rv_like_list.setVisibility(View.VISIBLE);
        userLikesListServiceCall(videoId,activity);
        final AlertDialog confirmAlert = dialogBuilder.create();
        dismiss_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });
        confirmAlert.setCancelable(true);
        confirmAlert.show();
    }



    private void userLikesListServiceCall(final String videoId, final Activity activity){
        dialog_llLoading.setVisibility(View.VISIBLE);
        videoLikesList.clear();
        String url=API.VIDEO_LIKES_LIST+videoId;
        System.out.println("Video Delete URL   "+url);
        new AQuery(activity).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
                    @Override
                    public void callback(String url, JSONObject outPut, AjaxStatus status) {
                        super.callback(url, outPut, status);
                        dialog_llLoading.setVisibility(View.GONE);
                        System.out.println("Video Like List Response   "+outPut.toString());
                        try{
                            for (int j=0; j<outPut.getJSONArray("likes").length(); j++){
                                JSONObject likeObj = outPut.getJSONArray("likes").getJSONObject(j);
                                VideoLikes videoLikes=new VideoLikes();
                                videoLikes.setLike_date(likeObj.getString(ResponseParameters.LIKED_AT));
                                videoLikes.setLike_id(likeObj.getString(ResponseParameters.LIKE_ID));
                                videoLikes.setVideo_avatar(likeObj.getString(ResponseParameters.Avatar));
                                videoLikes.setVideo_first_name(likeObj.getString(ResponseParameters.FirstName));
                                videoLikes.setVideo_last_name(likeObj.getString(ResponseParameters.LastName));
                                videoLikesList.add(videoLikes);
                            }
                            linearLayoutManager = new LinearLayoutManager(activity);
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rv_like_list.setLayoutManager(linearLayoutManager);
                            videoLikeListAdapter = new VideoLikeListAdapter(rv_like_list, activity, videoLikesList);
                            rv_like_list.addItemDecoration(
                                    new DividerItemDecoration(activity, R.drawable.divider));
                            rv_like_list.setAdapter(videoLikeListAdapter);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }
                }
        );
    }





    private void setOnClick(final String videoId, final VideoData videoData){
        tv_likes_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LikePopup(ShowVideoActivity.this,videoId);
            }
        });



        video_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                video_share.startAnimation(Utility.showAnimtion(ShowVideoActivity.this));
                Utils.showVideoSharePopUpMenu(video_share,videoId, videoData, videoId, ShowVideoActivity.this);
            }
        });
    }




    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText("Video Details");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!SharedPreferencesMethod.getBoolean(ShowVideoActivity.this,"HomeScreenActivity")){
                    finish();
                }
                else{
                    Intent intent=new Intent(ShowVideoActivity.this,HomeScreenActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        if(!SharedPreferencesMethod.getBoolean(ShowVideoActivity.this,"HomeScreenActivity")){
            finish();
        }
        else{
            Intent intent=new Intent(ShowVideoActivity.this,HomeScreenActivity.class);
            startActivity(intent);
            finish();
        }
        super.onBackPressed();  // optional depending on your needs
    }


    private void videoLikes(final String video_id){
        video_like.setClickable(true);
        video_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvLike.getText().toString().equalsIgnoreCase("Like")){
                    String url=API.VIDEO_LIKES+video_id+"/"+ SharedPreferencesMethod.getUserId(ShowVideoActivity.this)+"/"+1;
                    System.out.println("Video Like URL   "+url);
                    Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up_liked);
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                    tvLike.startAnimation(Utility.showAnimtion(ShowVideoActivity.this));
                    API.sendRequestToServerGET(ShowVideoActivity.this, url, API.VIDEO_LIKES);
                    tvLike.setText("Unlike");
                }else{
                    String url=API.VIDEO_LIKES+video_id+"/"+ SharedPreferencesMethod.getUserId(ShowVideoActivity.this)+"/"+0;
                    System.out.println("Video Like URL   "+url);
                    Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up);
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                    tvLike.startAnimation(Utility.showAnimtion(ShowVideoActivity.this));
                    API.sendRequestToServerGET(ShowVideoActivity.this, url, API.VIDEO_LIKES);
                    tvLike.setText("Like");
                }

            }
        });
    }



    public void getResponse(JSONObject outPut, int i) {
        if (i==0){
            System.out.println("Video Likes Response  "+outPut.toString());
            try{
                if (outPut.has(ResponseParameters.Success)){
                    outPut.getString("like_count");
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

    }




    private void initializePlayer() {
        if (player == null) {
            // 1. Create a default TrackSelector
            LoadControl loadControl = new DefaultLoadControl(
                    new DefaultAllocator(true, 20),
                    3000,
                    5000,
                    1500,
                    5000, -1, true);

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);
            // 2. Create the player
            player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this), trackSelector, loadControl);
            playerView.setPlayer(player);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN |View.SYSTEM_UI_FLAG_IMMERSIVE);
        }
    }



    private void playVideo(Uri videoUri) {
        initializePlayer();
        if (videoUri == null) {
            return;
        }
        buildMediaSource(videoUri);

    }


    private void buildMediaSource(Uri mUri) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getResources().getString(R.string.app_name)), bandwidthMeter);
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);
        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        player.addListener(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (player != null){
            player.stop();
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (player != null){
            player.stop();
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {

            case Player.STATE_BUFFERING:
                spinnerVideoDetails.setVisibility(View.VISIBLE);
                break;
            case Player.STATE_ENDED:
                break;
            case Player.STATE_IDLE:

                break;
            case Player.STATE_READY:
                spinnerVideoDetails.setVisibility(View.GONE);

                break;
            default:
                break;
        }
    }





    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }



}
