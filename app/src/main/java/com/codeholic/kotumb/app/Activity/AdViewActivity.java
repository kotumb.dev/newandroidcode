package com.codeholic.kotumb.app.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.EventNames;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import org.json.JSONObject;

import static com.androidquery.util.AQUtility.getContext;


public class AdViewActivity extends AppCompatActivity {

    private String url="";
    ProgressDialog progressDialog;
    WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_view);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.loading_data_text));
        progressDialog.setCancelable(false);
        try {
            JSONObject ad = new JSONObject(getIntent().getStringExtra("ad"));
            ImageView img = findViewById(R.id.img);
            webView=findViewById(R.id.ad_video);
            img.setOnTouchListener(new ImageMatrixTouchHandler(img.getContext()));
            JSONObject adJSONObject = ad.getJSONObject(ResponseParameters.AD);
            String url = ad.getString("ad_img_url").trim() + adJSONObject.getString(ResponseParameters.IMAGE).trim();
            url.trim();
            System.out.println("ad Url  "+getLastThree(url) +"===="+url);
            if (getLastThree(url).equalsIgnoreCase("jpg")||getLastThree(url).equalsIgnoreCase("jpeg")||getLastThree(url).equalsIgnoreCase("png")){
               webView.setVisibility(View.GONE);
               img.setVisibility(View.VISIBLE);
                new AQuery(this).id(img).image(url, true, true, getResources().getDisplayMetrics().widthPixels, R.drawable.ads_1);
            }else if (getLastThree(url).equalsIgnoreCase("mp4")||getLastThree(url).equalsIgnoreCase("mkv")||getLastThree(url).equalsIgnoreCase("3gp")||getLastThree(url).equalsIgnoreCase("flv")){
                webView.setVisibility(View.VISIBLE);
                img.setVisibility(View.GONE);
                String videoURL = "<html><head><title>Sample</title></head><body><video controls autoplay width=100% height=100% ><source src='"+url+"'></video></body></html>";
                loadAdvertisment(videoURL,webView);
            }



            FloatingActionButton fab = findViewById(R.id.fab);
            if(adJSONObject.getString("url").trim().isEmpty()){
                fab.hide();
            }else{
                fab.show();
                this.url = adJSONObject.getString("url");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClickFab(View view) {
        try {
            Utility.prepareCustomTab(this, url);
            Bundle bundle = new Bundle();
            bundle.putString("UserId", SharedPreferencesMethod.getUserId(this));
            bundle.putString("type", "Actual Ad");
            Utils.logEvent(EventNames.AD_CLICK, bundle, getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String getLastThree(String myString) {
        int a=myString.lastIndexOf('.');
        if(a > -1)
            return myString.substring(a+1);
        else
            return myString;
    }



    public void loadAdvertisment(final String videoURL,final WebView webview){
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setLoadsImagesAutomatically(true);
        webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        webview.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.requestFocus();
        webview.loadData(videoURL, "text/html", "UTF-8");
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadData(videoURL, "text/html", "UTF-8");
                return true;
            }
        });
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100) {
                    progressDialog.show();
                }
                if (progress==100) {
                    progressDialog.dismiss();
                }
            }
        });
    }

}








