package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.CompleteProfileActivity;
import com.codeholic.kotumb.app.Activity.ConnectionsActivity;
import com.codeholic.kotumb.app.Activity.OtherUserProfileActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class UserRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<User> users;
    Activity activity;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;

    public void setSuggestion(boolean suggestion) {
        this.suggestion = suggestion;
    }

    private boolean suggestion = false;

    public UserRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<User> users) {
        this.users = users;
        this.activity = activity;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (linearLayoutManager!=null) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.
                                    onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return users.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }



    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.user_list_item, parent, false);
            UserViewHolder UserViewHolder = new UserViewHolder(view);
            return UserViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            if(suggestion){
                View view = LayoutInflater.from(activity).inflate(R.layout.item_view_all, parent, false);
                return new ViewAllViewHolder(view);
            } else {
                View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
                return new LoadingViewHolder(view);
            }

        }

        return null;
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    private class ViewAllViewHolder extends RecyclerView.ViewHolder {
        public CardView cvViewAll;

        public ViewAllViewHolder(View view) {
            super(view);
            cvViewAll = view.findViewById(R.id.cvViewAll);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {
        User user = users.get(position);

        if (viewholder instanceof UserViewHolder) {
            UserViewHolder holder = (UserViewHolder) viewholder;
            holder.tvUserName.setText("" + user.getFirstName() + " " + user.getLastName());
            System.out.println("The Name  "+ user.getFirstName() + " " + user.getLastName());
            if (user.getProfileHeadline() != null && !user.getProfileHeadline().trim().equalsIgnoreCase("null") && !user.getProfileHeadline().trim().isEmpty()) {
                holder.tvProfession.setText("" + user.getProfileHeadline().trim());
                holder.tvProfession.setVisibility(View.VISIBLE);
            } else {
                holder.tvProfession.setVisibility(View.GONE);
            }

            if (user.getProfileSummary() != null && !user.getProfileSummary().trim().equalsIgnoreCase("null") && !user.getProfileSummary().trim().isEmpty()) {
                holder.tvAbout.setText("" + user.getProfileSummary().trim());
                holder.tvAbout.setVisibility(View.VISIBLE);
            } else {
                holder.tvAbout.setText("");
            }

            if (user.getAadhaarInfo().equalsIgnoreCase("true")) {
                holder.tvAadharVerified.setVisibility(View.VISIBLE);
            } else {
                holder.tvAadharVerified.setVisibility(View.GONE);
            }
            holder.tvLocation.setText(user.getCity().trim() + ", " + user.getState().trim());
            new AQuery(activity).id(holder.ivUserImage).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.ic_user);
            setOnClickListener(holder, user, position);

            try {
                JSONObject outPut = new JSONObject(user.getUserInfo());
                if (outPut.has(ResponseParameters.CONNECTION_FLAG) && !SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(user.getUserId().trim())) {
                    holder.btnContainer.setVisibility(VISIBLE);
                    holder.llAcceptConnect.setVisibility(VISIBLE);
                    if (outPut.getBoolean(ResponseParameters.CONNECTION_FLAG)) {
                        if (outPut.has(ResponseParameters.CONNECTION_INFO)) {
                            JSONObject CONNECTION_INFO = outPut.getJSONObject(ResponseParameters.CONNECTION_INFO);
                            String status = CONNECTION_INFO.getString(ResponseParameters.STATUS);
                            String ACTION_USER_ID = CONNECTION_INFO.getString(ResponseParameters.ACTIONUSERID);
                            if (status.trim().equalsIgnoreCase("0")) {
                                if (SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(ACTION_USER_ID.trim())) {
//                                    holder.cvContainer.setVisibility(GONE);//Code By Me
                                    holder.tvConnectAccept.setText(activity.getResources().getString(R.string.request_cancel_btn_text).trim());
                                    holder.tvConnectAccept.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_decline_request, 0, 0 ,0);
//                                    holder.llDecline.setVisibility(View.GONE);
                                } else {
                                    holder.tvConnectAccept.setText(activity.getResources().getString(R.string.request_accept_btn_text).trim());
                                    holder.tvConnectAccept.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_box, 0, 0 ,0);
                                    holder.llDecline.setVisibility(VISIBLE);
                                    if(user.getIsBlocked().equalsIgnoreCase("0")){
                                        holder.tvConnectAccept.setVisibility(GONE);
//                                        holder.llDecline.setVisibility(GONE);//By Me
                                    }
                                }
                            } else if (status.trim().equalsIgnoreCase("1")) {
                                holder.tvConnectAccept.setText(activity.getResources().getString(R.string.unfriend_btn_text).trim());
                                holder.tvConnectAccept.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_decline_request, 0, 0 ,0);
                                holder.llDecline.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        holder.llDecline.setVisibility(View.GONE);
                        holder.tvConnectAccept.setText(activity.getResources().getString(R.string.connect_btn_text).trim());
                        holder.tvConnectAccept.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_box, 0, 0 ,0);
                    }
                } else {
                    holder.btnContainer.setVisibility(GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        } else if (viewholder instanceof ViewAllViewHolder) {
            ViewAllViewHolder viewAllViewHolder = (ViewAllViewHolder) viewholder;
            viewAllViewHolder.cvViewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, ConnectionsActivity.class);
                    intent.putExtra("SUGGESTION", true);
                    activity.startActivity(intent);
                }
            });
        }
    }

    public void setOnClickListener(final UserViewHolder holder, final User user, final int position) {


        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(user.getUserId().trim())) {
                    Intent intent = new Intent(activity, OtherUserProfileActivity.class);
                    intent.putExtra("USER", user);
                    OtherUserProfileActivity.setHolder(holder);
                    activity.startActivity(intent);
                } else {
                    Intent intent = new Intent(activity, CompleteProfileActivity.class);
                    intent.putExtra(ResponseParameters.Id, R.id.ll_personal_details);
                    //intent.putExtra("DATA", new JSONObject().toString());
                    activity.startActivity(intent);
                }

            }
        });
        holder.tvDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(user.getUserId().trim())) {
                    Intent intent = new Intent(activity, OtherUserProfileActivity.class);
                    intent.putExtra("USER", user);
                    OtherUserProfileActivity.setHolder(holder);
                    activity.startActivity(intent);
                }
            }
        });

        holder.llAcceptConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!Utility.isConnectingToInternet(activity)) {
                        final Snackbar snackbar = Snackbar.make(holder.llAcceptConnect, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }
                    final HashMap<String, Object> input = new HashMap<>();
                    input.put(RequestParameters.USERID1, "" + SharedPreferencesMethod.getUserId(activity));
                    input.put(RequestParameters.USERID2, "" + user.getUserId());
                    input.put(RequestParameters.ACTIONUSERID, "" + SharedPreferencesMethod.getUserId(activity));
                    final JSONObject jsonObject = new JSONObject(users.get(position).getUserInfo());
                    if (jsonObject.getBoolean(ResponseParameters.CONNECTION_FLAG)) {
                        if (jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).getString(ResponseParameters.STATUS).trim().equalsIgnoreCase("0")) {
                            if (SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).getString(ResponseParameters.ACTIONUSERID).trim())) {
                                // canceling request

                                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                                dialogBuilder.setTitle(activity.getResources().getString(R.string.app_alert_text));
                                dialogBuilder.setMessage(activity.getResources().getString(R.string.confirm_cancel_request_message));
                                dialogBuilder.setCancelable(true);
                                dialogBuilder.setPositiveButton(activity.getResources().getString(R.string.done_btn_text), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        try{
                                            if (!Utility.isConnectingToInternet(activity)) {
                                                final Snackbar snackbar = Snackbar.make(holder.llDecline, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                                                snackbar.show();
                                                return;
                                            }
                                            input.put(RequestParameters.STATUS, "2");
                                            jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "2");
                                            jsonObject.put(ResponseParameters.CONNECTION_FLAG, false);
                                            holder.tvConnectAccept.setText(activity.getResources().getString(R.string.connect_btn_text));
                                            holder.tvConnectAccept.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_box, 0, 0 ,0);
                                            holder.llDecline.setVisibility(GONE);
                                            users.get(position).setUserInfo(jsonObject.toString());

                                            Log.e("request", "" + input);

                                            Intent broadCast = new Intent();
                                            broadCast.setAction(ResponseParameters.UPDATE_USER);
                                            broadCast.putExtra("UPDATE_USER", "");// for not updating connection activity item
                                            broadCast.putExtra("USER", user);
                                            activity.sendBroadcast(broadCast);

                                            API.sendRequestToServerPOST_PARAM(activity, API.CONNECT, input);
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        dialog.dismiss();
                                    }
                                });

                                dialogBuilder.setNegativeButton(activity.getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                    }
                                });

                                dialogBuilder.create().show();


                            } else {
                                // accepting request
                                jsonObject.put(ResponseParameters.CONNECTION_FLAG, false);
                                jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "1");
                                input.put(RequestParameters.STATUS, "1");
                                holder.llDecline.setVisibility(GONE);
                                holder.tvConnectAccept.setText(activity.getResources().getString(R.string.unfriend_btn_text));
                                holder.tvConnectAccept.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_decline_request, 0, 0 ,0);
                                users.get(position).setUserInfo(jsonObject.toString());

                                Log.e("request", "" + input);

                                Intent requestAccepted = new Intent();
                                requestAccepted.setAction(ResponseParameters.UPDATE_USER);
                                requestAccepted.putExtra("UPDATE_USER", "");// for not updating connection activity item
                                requestAccepted.putExtra("USER", user);
                                activity.sendBroadcast(requestAccepted);

                                API.sendRequestToServerPOST_PARAM(activity, API.CONNECT, input);
                            }
                        } else if (jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).getString(ResponseParameters.STATUS).trim().equalsIgnoreCase("1")) {
                            // un friend request
                            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                            dialogBuilder.setTitle(activity.getResources().getString(R.string.app_alert_text));
                            dialogBuilder.setMessage(activity.getResources().getString(R.string.confirm_unfriend_message));
                            dialogBuilder.setCancelable(true);
                            dialogBuilder.setPositiveButton(activity.getResources().getString(R.string.done_btn_text), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    try{
                                        if (!Utility.isConnectingToInternet(activity)) {
                                            final Snackbar snackbar = Snackbar.make(holder.llDecline, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                                            snackbar.show();
                                            return;
                                        }
                                        input.put(RequestParameters.STATUS, "4");
                                        jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "4");
                                        jsonObject.put(ResponseParameters.CONNECTION_FLAG, false);
                                        holder.tvConnectAccept.setText(activity.getResources().getString(R.string.connect_btn_text));
                                        holder.tvConnectAccept.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_box, 0, 0 ,0);
                                        holder.llDecline.setVisibility(GONE);
                                        users.get(position).setUserInfo(jsonObject.toString());

                                        Log.e("request", "" + input);

                                        Intent broadCast = new Intent();
                                        broadCast.setAction(ResponseParameters.UPDATE_USER);
                                        broadCast.putExtra("UPDATE_USER", "");// for not updating connection activity item
                                        broadCast.putExtra("USER", user);
                                        activity.sendBroadcast(broadCast);


                                        API.sendRequestToServerPOST_PARAM(activity, API.CONNECT, input);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    dialog.dismiss();
                                }
                            });

                            dialogBuilder.setNegativeButton(activity.getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.dismiss();
                                }
                            });
                            dialogBuilder.create().show();
                        }
                    } else {
                        input.put(RequestParameters.STATUS, "0");
                        if (!jsonObject.has(ResponseParameters.CONNECTION_INFO)) {
                            jsonObject.put(ResponseParameters.CONNECTION_INFO, new JSONObject());
                        }
                        jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "0");
                        jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.ACTIONUSERID, "" + SharedPreferencesMethod.getUserId(activity));
                        jsonObject.put(ResponseParameters.CONNECTION_FLAG, true);
                        holder.llDecline.setVisibility(GONE);
                        holder.tvConnectAccept.setText(activity.getResources().getString(R.string.request_cancel_btn_text));
                        holder.tvConnectAccept.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_decline_request, 0, 0 ,0);
                        users.get(position).setUserInfo(jsonObject.toString());
                        Log.e("request", "" + input);

                        //broad cast for updating request send
                        Intent broadCast = new Intent();
                        broadCast.setAction(ResponseParameters.REQUEST_SEND);
                        broadCast.putExtra("UPDATE_USER", ""); // for not updating connection activity item
                        broadCast.putExtra("USER", user);
                        activity.sendBroadcast(broadCast);
                        API.sendRequestToServerPOST_PARAM(activity, API.CONNECT, input);
                        users.remove(user);//code by me
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        holder.llDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //decline request
                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                    dialogBuilder.setTitle(activity.getResources().getString(R.string.app_alert_text));
                    dialogBuilder.setMessage(activity.getResources().getString(R.string.confirm_decline_request_message));
                    dialogBuilder.setCancelable(true);
                    dialogBuilder.setPositiveButton(activity.getResources().getString(R.string.primary_number_yes_btn_text), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                           try{
                               if (!Utility.isConnectingToInternet(activity)) {
                                   final Snackbar snackbar = Snackbar.make(holder.llDecline, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                                   snackbar.show();
                                   return;
                               }
                               HashMap<String, Object> input = new HashMap<>();
                               input.put(RequestParameters.USERID1, "" + SharedPreferencesMethod.getUserId(activity));
                               input.put(RequestParameters.USERID2, "" + user.getUserId());
                               input.put(RequestParameters.ACTIONUSERID, "" + SharedPreferencesMethod.getUserId(activity));
                               input.put(RequestParameters.STATUS, "2");
                               Log.e("input", "" + input);
                               JSONObject jsonObject = new JSONObject(users.get(position).getUserInfo());
                               jsonObject.put(ResponseParameters.CONNECTION_FLAG, false);
                               users.get(position).setUserInfo(jsonObject.toString());
                               holder.tvConnectAccept.setText(activity.getResources().getString(R.string.connect_btn_text));
                               holder.tvConnectAccept.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_box, 0, 0 ,0);
                               holder.llDecline.setVisibility(GONE);

                               Intent broadCast = new Intent();
                               broadCast.setAction(ResponseParameters.UPDATE_USER);
                               broadCast.putExtra("UPDATE_USER", "");// for not updating connection activity item
                               broadCast.putExtra("USER", user);
                               activity.sendBroadcast(broadCast);

                               API.sendRequestToServerPOST_PARAM(activity, API.CONNECT, input);
                           }catch (Exception e){
                               e.printStackTrace();
                           }
                            dialog.dismiss();
                        }
                    });

                    dialogBuilder.setNegativeButton(activity.getResources().getString(R.string.primary_number_no_btn_text), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                        }
                    });

                    dialogBuilder.create().show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        public TextView tvUserName, tvConnectAccept, tvProfession, tvAadharVerified, tvLocation, tvAbout, tvDetails;
        public ImageView ivUserImage;
        public CardView cvContainer;
        public LinearLayout llAcceptConnect, llDecline, btnContainer;


        UserViewHolder(View view) {
            super(view);
            tvUserName = view.findViewById(R.id.tvUserName);
            tvConnectAccept = view.findViewById(R.id.tvConnectAccept);
            ivUserImage = view.findViewById(R.id.ivUserImage);
            tvProfession = view.findViewById(R.id.tvProfession);
            tvAadharVerified = view.findViewById(R.id.tvAadharVerified);
            tvLocation = view.findViewById(R.id.tvLocation);
            cvContainer = view.findViewById(R.id.cvContainer);
            llAcceptConnect = view.findViewById(R.id.llAcceptConnect);
            llDecline = view.findViewById(R.id.llDecline);
            btnContainer= view.findViewById(R.id.btnContainer);
            tvAbout= view.findViewById(R.id.tvAbout);
            tvDetails = view.findViewById(R.id.tvDetails);
        }
    }
}