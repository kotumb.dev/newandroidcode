package com.codeholic.kotumb.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DELL on 1/25/2018.
 */

public class Message implements Parcelable {
    String id = "";
    String reply = "";
    String refrenceId="";
    String refrenceMessage="";
    String messageId="";
    String userId = "";
    String messageAT = "";
    String readStatus = "";
    String messageMedia="";
    String messageMediaType="";
    String pdfPath="";
    private boolean sent;

    public String getMsg_identifer() {
        return msg_identifer;
    }

    public void setMsg_identifer(String msg_identifer) {
        this.msg_identifer = msg_identifer;
    }

    String msg_identifer = "";

    protected Message(Parcel in) {
        id = in.readString();
        reply = in.readString();
        userId = in.readString();
        messageAT = in.readString();
        readStatus = in.readString();
        messageMedia = in.readString();
        messageMediaType = in.readString();
        pdfPath=in.readString();
        refrenceId=in.readString();
        refrenceMessage=in.readString();
        messageId=in.readString();
    }

    public Message() {

    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessageAT() {
        return messageAT;
    }

    public void setMessageAT(String messageAT) {
        this.messageAT = messageAT;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }


    public String getMessageMedia() {
        return messageMedia;
    }

    public void setMessageMedia(String messageMedia) {
        this.messageMedia = messageMedia;
    }


    public String getMessageMediaType() {
        return messageMediaType;
    }

    public void setMessageMediaType(String messageMediaType) {
        this.messageMediaType = messageMediaType;
    }
    public String getRefrenceMessage() {
        return refrenceMessage;
    }

    public void setRefrenceMessage(String refrenceMessage) {
        this.refrenceMessage = refrenceMessage;
    }



    public String getRefrenceId() {
        return refrenceId;
    }

    public void setRefrenceId(String refrenceId) {
        this.refrenceId = refrenceId;
    }



    public String getMessageId() {
        return messageId;
    }

    public void settMessageId(String messageId) {
        this.messageId = messageId;
    }



    public String getPdfPath() {
        return pdfPath;
    }

    public void setPdfPath(String pdfPath) {
        this.pdfPath = pdfPath;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(reply);
        dest.writeString(userId);
        dest.writeString(messageAT);
        dest.writeString(readStatus);
        dest.writeString(messageMedia);
        dest.writeString(messageMediaType);
        dest.writeString(pdfPath);
        dest.writeString(refrenceId);
        dest.writeString(refrenceMessage);
        dest.writeString(messageId);
    }

    public void setSent(boolean b) {
        this.sent = b;
    }

    public boolean isSent() {
        return sent;
    }



}
