package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Model.Profession;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.AutoCompleteAdapter;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.shagi.materialdatepicker.date.DatePickerFragmentDialog;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ProfessionActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.etNameOrgLayout)
    TextInputLayout etNameOrgLayout;

    @BindView(R.id.etNameOrg)
    AutoCompleteTextView etNameOrg;

    @BindView(R.id.etRoleOrgLayout)
    TextInputLayout etRoleOrgLayout;

    @BindView(R.id.etRoleOrg)
    AutoCompleteTextView etRoleOrg;

    @BindView(R.id.etIndustriesLayout)
    TextInputLayout etIndustriesLayout;

    @BindView(R.id.etIndustries)
    AutoCompleteTextView etIndustries;

    @BindView(R.id.etDateFromOrgLayout)
    TextInputLayout etDateFromOrgLayout;

    @BindView(R.id.etDateFromOrg)
    EditText etDateFromOrg;

    @BindView(R.id.etDateToOrgLayout)
    TextInputLayout etDateToOrgLayout;

    @BindView(R.id.etDateToOrg)
    EditText etDateToOrg;

    @BindView(R.id.etWorkDetailsOrgLayout)
    TextInputLayout etWorkDetailsOrgLayout;

    @BindView(R.id.etWorkDetailsOrg)
    EditText etWorkDetailsOrg;

    @BindView(R.id.cbCurrentOrg)
    CheckBox cbCurrentOrg;

    @BindView(R.id.cvOrgSave)
    CardView cvOrgSave;

    @BindView(R.id.tvOrgSave)
    TextView tvOrgSave;

    @BindView(R.id.pbOrgLoading)
    ProgressBar pbOrgLoading;

    @BindView(R.id.pbRoleOrg)
    ProgressBar pbRoleOrg;

    @BindView(R.id.pbOrg)
    ProgressBar pbOrg;

    @BindView(R.id.spIndustry)
    Spinner spIndustry;

    Activity context;
    Profession profession = new Profession();

    private Calendar calendar;
    private int year, month, day;
    private String toDate = "";
    List<String> roles = new ArrayList<>();
    List<String> sector = new ArrayList<>();
    List<String> names = new ArrayList<>();
    boolean role = true, org = true;
    boolean opened = false;
    boolean clearFocus = true;

    //oncreate method for setting content view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profession);
        ButterKnife.bind(this);
        setOncheckChangeListener();
        init();
        initToolbar();
        setClickListener();
        setEditTextWatcher();
        setUpAdpter();
    }

    private void setUpAdpter() {
        try {
            if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.JOBROLES).isEmpty()) {
                if (!Utility.isConnectingToInternet(context)) {
                    return;
                }
                if (role) {
                    pbRoleOrg.setVisibility(VISIBLE);
                    API.sendRequestToServerGET(context, API.JOBROLES, API.JOBROLES);
                }
                role = true;
            } else {
//                setUpSpinnerAdapter(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.JOBROLES)));
                setUpSpinnerAdapterBySkills(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.INDUSTRIES)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    //setting toolbar and header title
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.profile_professional_title));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //init values and show user details
    private void init() {
        context = this;
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        if (getIntent().hasExtra(RequestParameters.ORGANISATION)) {
            profession = getIntent().getParcelableExtra(RequestParameters.ORGANISATION);
            fillView();
        } else {
            cbCurrentOrg.setEnabled(false);
        }

        etRoleOrg.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    etRoleOrg.performClick();
            }
        });

        etRoleOrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS).isEmpty()) {
                        if (!Utility.isConnectingToInternet(context)) {
                            return;
                        }
                        API.sendRequestToServerGET(context, API.JOBROLES, API.JOBROLES);
                    } else {
                        getResponse(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.JOBROLES)), 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                etRoleOrg.showDropDown();
            }
        });

        etRoleOrg.setOnDismissListener(new AutoCompleteTextView.OnDismissListener() {
            @Override
            public void onDismiss() {
                //maDSkills.clearFocus();
                /*InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etRoleOrg.getWindowToken(), 0);*/
            }
        });


        etNameOrg.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && opened && (clearFocus || etNameOrg.getText().toString().trim().isEmpty()))
                    etNameOrg.performClick();
                opened = true;
            }
        });

        etNameOrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS).isEmpty()) {
                        if (!Utility.isConnectingToInternet(context)) {
                            return;
                        }
                        API.sendRequestToServerGET(context, API.ORGANIZATION, API.ORGANIZATION);
                    } else {
                        getResponse(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.ORGANISATIONS)), 2);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                etNameOrg.showDropDown();
            }
        });

        etNameOrg.setOnDismissListener(new AutoCompleteTextView.OnDismissListener() {
            @Override
            public void onDismiss() {
                //maDSkills.clearFocus();
                /*InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etNameOrg.getWindowToken(), 0);*/
            }
        });
    }

    private void setOncheckChangeListener() {
        cbCurrentOrg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    maskToDate();
                } else {
                    unmaskToDate(true, 1, Color.WHITE);
                }
            }
        });
    }

    private void unmaskToDate(boolean b, float i, int white) {
        etDateToOrgLayout.setEnabled(b);
        etDateToOrgLayout.setAlpha(i);
        etDateToOrgLayout.setBackgroundColor(white);
        etDateToOrg.setText("");
    }

    private void maskToDate() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date toDay = Calendar.getInstance().getTime();
        String toDayS = df.format(toDay);
        //etDateToOrg.setText(toDayS);
        unmaskToDate(false, 0.5f,Color.GRAY);
    }

    //fill values from data
    private void fillView() {
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");
            role = false;
            org = false;
            etNameOrg.setText(profession.getCompanyName());
            etRoleOrg.setText(profession.getProfessionName());
            if (!profession.getYearFrom().trim().equalsIgnoreCase("0000-00-00")) {
                etDateFromOrg.setText(df_.format(df.parse(profession.getYearFrom())));
            }
            etWorkDetailsOrg.setText(profession.getDescription());
            if (Integer.parseInt(profession.getIsCurrent()) == 1) {
                cbCurrentOrg.setChecked(true);
                etDateToOrg.setText(df_.format(Calendar.getInstance().getTime()));
            } else {
                if (!profession.getYearTo().trim().equalsIgnoreCase("0000-00-00"))
                    etDateToOrg.setText(df_.format(df.parse(profession.getYearTo())));
            }
            tvOrgSave.setText(getResources().getString(R.string.update_btn_text));
            Log.e("sector ", "" + profession.getSector());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //set click listener on views
    private void setClickListener() {
        cvOrgSave.setOnClickListener(this);
        etDateFromOrg.setOnClickListener(this);
        etDateToOrg.setOnClickListener(this);
    }

    //validation for organisation fields
    private boolean orgValidation() {
        disableOrgError();
        boolean result = true;
        clearFocus = false;
        clearFocus();
        clearFocus = true;
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etNameOrg.getWindowToken(), 0);
        if (etNameOrg.getText().toString().trim().isEmpty()) {
            if (result) {
                etNameOrg.requestFocus();
                imm.showSoftInput(etNameOrg, InputMethodManager.SHOW_IMPLICIT);
            }

            enableError(etNameOrgLayout, getResources().getString(R.string.org_name_empty_input));
            result = false;
        }
        if (etRoleOrg.getText().toString().trim().isEmpty()) {
            if (result) {
                etRoleOrg.requestFocus();
                imm.showSoftInput(etRoleOrg, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etRoleOrgLayout, getResources().getString(R.string.org_role_empty_input));
            result = false;
        }
        if (etDateFromOrg.getText().toString().trim().isEmpty()) {
            if (result) {
                etDateFromOrgLayout.getParent().requestChildFocus(etDateFromOrgLayout, etDateFromOrgLayout);
            }
            enableError(etDateFromOrgLayout, getResources().getString(R.string.from_year_empty_input));
            result = false;
        }
        if (!cbCurrentOrg.isChecked()) {
            if (etDateToOrg.getText().toString().trim().isEmpty()) {
                if (result) {
                    etDateToOrg.getParent().requestChildFocus(etDateToOrg, etDateToOrg);
                }
                enableError(etDateToOrgLayout, getResources().getString(R.string.to_year_empty_input));
                result = false;
            }
        }

        Object tag = etNameOrg.getTag();
        if (tag != null) {
            if (!(boolean) tag) {
                enableError(etNameOrgLayout, getString(R.string.select_from_dropdown_text));
                result = false;
            }
        }
        Object tag2 = etRoleOrg.getTag();
        if (tag2 != null) {
            if (!(boolean) tag2) {
                enableError(etRoleOrgLayout, getString(R.string.select_from_dropdown_text));
                result = false;
            }
        }
        /*if (etWorkDetailsOrg.getText().toString().trim().isEmpty()) {
            enableError(etWorkDetailsOrgLayout, getResources().getString(R.string.work_detail_error));
            return false;
        }*/
        return result;
    }

    private void clearFocus() {
        etNameOrg.clearFocus();
        etRoleOrg.clearFocus();
        etIndustries.clearFocus();
        etDateFromOrg.clearFocus();
        etDateToOrg.clearFocus();
        etWorkDetailsOrg.clearFocus();
    }

    //enable error of TextInputLayout
    private void enableError(TextInputLayout inputLayout, String error) {
        inputLayout.setErrorEnabled(true);
        inputLayout.setError(error);
    }

    //disable error of TextInputLayout
    private void disableOrgError() {
        etNameOrgLayout.setErrorEnabled(false);
        etRoleOrgLayout.setErrorEnabled(false);
        etIndustriesLayout.setErrorEnabled(false);
        etDateFromOrgLayout.setErrorEnabled(false);
        etDateToOrgLayout.setErrorEnabled(false);
        etWorkDetailsOrgLayout.setErrorEnabled(false);
    }

    //setting textwatcher
    private void setEditTextWatcher() {
        setTextWatcher(etNameOrg, etNameOrgLayout);
        setTextWatcher(etRoleOrg, etRoleOrgLayout);
        setTextWatcher(etDateFromOrg, etDateFromOrgLayout);
        setTextWatcher(etDateToOrg, etDateToOrgLayout);
        setTextWatcher(etWorkDetailsOrg, etWorkDetailsOrgLayout);
    }

    //setting textwatchr to edittext
    private void setTextWatcher(final EditText editText, final TextInputLayout textInputLayout) {


        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    textInputLayout.setErrorEnabled(false);
                }
                if (s.length() >= 1) {
                    try {
                        if (editText == etRoleOrg) {
                            if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.JOBROLES).isEmpty()) {
                                if (!Utility.isConnectingToInternet(context)) {
                                    return;
                                }
                                if (role) {
                                    pbRoleOrg.setVisibility(VISIBLE);
                                    API.sendRequestToServerGET(context, API.JOBROLES, API.JOBROLES);
                                }
                                role = true;
                            } else {
                                if (role) {
                                    getResponse(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.JOBROLES)), 1);
                                }
                                role = true;
                            }
                        }
                        if (editText == etNameOrg) {
                            if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.ORGANISATIONS).isEmpty()) {
                                if (!Utility.isConnectingToInternet(context)) {
                                    return;
                                }
                                if (org) {
                                    pbOrg.setVisibility(VISIBLE);
                                    API.sendRequestToServerGET(context, API.ORGANIZATION, API.ORGANIZATION);
                                }
                                org = true;
                            } else {
                                if (org) {
                                    getResponse(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.ORGANISATIONS)), 2);
                                }
                                org = true;
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editText == etNameOrg) {
                    Log.e("TAG", "maSkills");
                    if (etNameOrg.getText().toString().trim().isEmpty()) {
                        Log.e("TAG", "if");
                        etNameOrg.performClick();
                    }
                    etNameOrg.setTag(false);
                }

                if (editText == etRoleOrg) {
                    if (etRoleOrg.getText().toString().trim().isEmpty()) {
                        etRoleOrg.performClick();
                    }
                    etRoleOrg.setTag(false);
                }
            }
        });
    }

    //getting response from server
    public void getResponse(JSONObject output, int i) {
        try {
            if (i == 0) { // saving experience
                if (output.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (output.has(ResponseParameters.Errors)) { //error
                        final Snackbar snackbar = Snackbar.make(etNameOrg, Utils.getErrorMessage(output), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else if (output.has(ResponseParameters.Success)) {
                        final Snackbar snackbar = Snackbar.make(etNameOrg, output.getString(ResponseParameters.Success), Toast.LENGTH_SHORT);
                        snackbar.show();

                        JSONObject jsonObject = output.getJSONObject(ResponseParameters.EXPERIANCE);
                        Constants.profession = new Profession();
                        Constants.profession.setId(jsonObject.getString(ResponseParameters.Id));
                        Constants.profession.setCompanyName(jsonObject.getString(ResponseParameters.ORGANIZATION_NAME));
                        Constants.profession.setProfessionName(jsonObject.getString(ResponseParameters.ORGANIZATION_ROLE));
                        Constants.profession.setYearFrom(jsonObject.getString(ResponseParameters.YEARFROM));
                        Constants.profession.setYearTo(jsonObject.getString(ResponseParameters.YEARTO));
                        Constants.profession.setDescription(jsonObject.getString(ResponseParameters.DESCRIPTION));
                        Constants.profession.setIsCurrent(jsonObject.getString(ResponseParameters.ISCURRENT));
                        if (jsonObject.has(ResponseParameters.ORGANIZATION_INDUSTRY))
                            Constants.profession.setSector(jsonObject.getString(ResponseParameters.ORGANIZATION_INDUSTRY));
                        Constants.completeProfileActivity = true;
                        Utils.showPopup(ProfessionActivity.this, getString(R.string.success_text), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        });
                        System.out.println("Descriptions  "+jsonObject.getString(ResponseParameters.DESCRIPTION));
                    } else {
//                        Toast.makeText(context, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(ProfessionActivity.this, getResources().getString(R.string.error_text));
                    }
                    tvOrgSave.setVisibility(VISIBLE);
                    pbOrgLoading.setVisibility(GONE);
                    cvOrgSave.setClickable(true);
                    etNameOrg.setEnabled(true);
                    etRoleOrg.setEnabled(true);
                    spIndustry.setEnabled(true);
                    etDateFromOrg.setEnabled(true);
                    etDateToOrg.setEnabled(true);
                    etWorkDetailsOrg.setEnabled(true);
                    cbCurrentOrg.setEnabled(true);
                }
            }
            if (i == 1) { // getting roles
                Log.e("output", "" + output);
                pbRoleOrg.setVisibility(GONE);
                if (output.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (output.has(ResponseParameters.Success)) {
                        if (output.has(ResponseParameters.JOBROLES)) {
                            roles = new ArrayList<>();
                            if (sector.size() == 0) {
                                setUpSpinnerAdapter(output);
                            }
                            for (int j = 0; j < output.getJSONArray(ResponseParameters.JOBROLES).length(); j++) {
//                                if (spIndustry.getSelectedItem().toString().equalsIgnoreCase(output.getJSONArray(ResponseParameters.JOBROLES).getJSONObject(j).getString(ResponseParameters.SECTOR)))
                                    roles.add(output.getJSONArray(ResponseParameters.JOBROLES).getJSONObject(j).getString(ResponseParameters.ROLENAME));
                            }
                            roles.add("Other");
                            String[] data = roles.toArray(new String[roles.size()]);
                            ArrayAdapter<?> adapter = new ArrayAdapter<>(this, R.layout.autocomplete_item, data);
                            List<String> searchArrayList = new ArrayList<>();
                            Collections.addAll(searchArrayList, data);
                            AutoCompleteAdapter adapter2 = new AutoCompleteAdapter(this, R.layout.autocomplete_item, android.R.id.text1, searchArrayList);
                            etRoleOrg.setAdapter(adapter2);
                            etRoleOrg.setThreshold(1);

                            Utils.addOtherOptionHandler(etRoleOrg, roles, ProfessionActivity.this);

                            if (!etRoleOrg.getText().toString().trim().isEmpty() && !roles.contains(etRoleOrg.getText().toString().trim()))
                                etRoleOrg.showDropDown();

                            if (!etIndustries.getText().toString().trim().isEmpty() && !sector.contains(etIndustries.getText().toString().trim()))
                                etIndustries.showDropDown();
                        }
                    }
                }
            }
            if (i == 2) { // getting organizations
                pbOrg.setVisibility(GONE);
                if (output.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (output.has(ResponseParameters.Success)) {
                        if (output.has(ResponseParameters.ORGANISATIONS)) {
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.ORGANISATIONS, output.toString());
                            names = new ArrayList<>();
                            for (int j = 0; j < output.getJSONArray(ResponseParameters.ORGANISATIONS).length(); j++) {
                                names.add(output.getJSONArray(ResponseParameters.ORGANISATIONS).getJSONObject(j).getString(ResponseParameters.ORGANISATION_NAME));
                            }
                            names.add("Other");
                            String[] data = names.toArray(new String[names.size()]);
                            ArrayAdapter<?> adapter = new ArrayAdapter<Object>(this, R.layout.autocomplete_item, data);
                            etNameOrg.setAdapter(adapter);
                            etNameOrg.setThreshold(1);
                            Utils.addOtherOptionHandler(etNameOrg, names, ProfessionActivity.this);

                            if (!etNameOrg.getText().toString().trim().isEmpty() && !names.contains(etNameOrg.getText().toString().trim()))
                                etNameOrg.showDropDown();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    void setUpSpinnerAdapter(JSONObject output) {
        try {
            if (output.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (output.has(ResponseParameters.Success)) {
                    if (output.has(ResponseParameters.JOBROLES)) {
                        System.out.println("Get Response From "+ output.has(ResponseParameters.INDUSTRIES));
                        sector = new ArrayList<>();
                        int selectedPosition = -1;
                        for (int j = 0; j < output.getJSONArray(ResponseParameters.JOBROLES).length(); j++) {
                            if (!sector.contains(output.getJSONArray(ResponseParameters.JOBROLES).getJSONObject(j).getString(ResponseParameters.SECTOR))
                                    && !output.getJSONArray(ResponseParameters.JOBROLES).getJSONObject(j).getString(ResponseParameters.SECTOR).trim().isEmpty()) {
                                sector.add(output.getJSONArray(ResponseParameters.JOBROLES).getJSONObject(j).getString(ResponseParameters.SECTOR).trim());
                                if (output.getJSONArray(ResponseParameters.JOBROLES).getJSONObject(j).getString(ResponseParameters.SECTOR).equals(profession.getSector())) {
                                    selectedPosition = sector.size() - 1;
                                }
                            }
                        }

                        String[] dataSector = sector.toArray(new String[sector.size()]);
                        ArrayAdapter<?> adapterSector = new ArrayAdapter<Object>(this, R.layout.selected_item, dataSector);
                        adapterSector.setDropDownViewResource(R.layout.autocomplete_item);
                        spIndustry.setAdapter(adapterSector);
                        if (selectedPosition != -1) {
                            spIndustry.setSelection(selectedPosition);
                        }
                        Log.e("TAG", "" + spIndustry.getSelectedItem().toString());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setUpSpinnerAdapterBySkills(JSONObject output) {
        try {
            if (output.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (output.has(ResponseParameters.Success)) {
                    if (output.has(ResponseParameters.INDUSTRIES)) {
                        sector = new ArrayList<>();
                        int selectedPosition = -1;
                        for (int j = 0; j < output.getJSONArray(ResponseParameters.INDUSTRIES).length(); j++) {
                            if (!sector.contains(output.getJSONArray(ResponseParameters.INDUSTRIES).getJSONObject(j).getString(ResponseParameters.SECTOR))
                                    && !output.getJSONArray(ResponseParameters.INDUSTRIES).getJSONObject(j).getString(ResponseParameters.SECTOR).trim().isEmpty()) {
                                sector.add(output.getJSONArray(ResponseParameters.INDUSTRIES).getJSONObject(j).getString(ResponseParameters.SECTOR).trim());
                                if (output.getJSONArray(ResponseParameters.INDUSTRIES).getJSONObject(j).getString(ResponseParameters.SECTOR).equals(profession.getSector())) {
                                    selectedPosition = sector.size() - 1;
                                }
                            }
                        }

                        String[] dataSector = sector.toArray(new String[sector.size()]);
                        ArrayAdapter<?> adapterSector = new ArrayAdapter<Object>(this, R.layout.selected_item, dataSector);
                        adapterSector.setDropDownViewResource(R.layout.autocomplete_item);
                        spIndustry.setAdapter(adapterSector);
                        if (selectedPosition != -1) {
                            spIndustry.setSelection(selectedPosition);
                        }
                        Log.e("TAG", "" + spIndustry.getSelectedItem().toString());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.cvOrgSave: // saving organization values
                if (orgValidation()) { // checking validation
                    try {
                        if (!Utility.isConnectingToInternet(context)) { // checking net connection
                            final Snackbar snackbar = Snackbar.make(v, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        HashMap<String, Object> input = new HashMap<>();
                        if (getIntent().hasExtra(RequestParameters.ORGANISATION)) {
                            input.put(RequestParameters.EXPID, profession.getId());
                        }
                        input.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(this));
                        input.put(RequestParameters.ORGANISATION, "" + etNameOrg.getText().toString().trim());
                        input.put(RequestParameters.ROLE, "" + etRoleOrg.getText().toString().trim());

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");

                        input.put(RequestParameters.FROMYEAR, "" + df.format(df_.parse(etDateFromOrg.getText().toString().trim())));
                        if (etDateToOrg.getText().toString().trim().isEmpty())
                            input.put(RequestParameters.TOYEAR, "");
                        else
                            input.put(RequestParameters.TOYEAR, "" + df.format(df_.parse(etDateToOrg.getText().toString().trim())));
                        input.put(RequestParameters.DESCRIPTION, "" + etWorkDetailsOrg.getText().toString().trim());
                        input.put(RequestParameters.SECTOR, "" + spIndustry.getSelectedItem().toString());


                        if (cbCurrentOrg.isChecked())
                            input.put(RequestParameters.CURRENTORG, "1");
                        else
                            input.put(RequestParameters.CURRENTORG, "0");
                        tvOrgSave.setVisibility(GONE);
                        pbOrgLoading.setVisibility(VISIBLE);
                        cvOrgSave.setClickable(false);
                        etNameOrg.setEnabled(false);
                        etRoleOrg.setEnabled(false);
                        spIndustry.setEnabled(false);
                        etDateFromOrg.setEnabled(false);
                        etDateToOrg.setEnabled(false);
                        etWorkDetailsOrg.setEnabled(false);
                        cbCurrentOrg.setEnabled(false);

                        if (getIntent().hasExtra(RequestParameters.ORGANISATION)) {
                            API.sendRequestToServerPOST_PARAM(context, API.UPDATE_WORK_EXPERIANCE, input); // service call for update education details
                        } else {
                            API.sendRequestToServerPOST_PARAM(context, API.WORK_EXPERIANCE, input); // service call for save education details
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.etDateFromOrg: // get date from
                //showCal((EditText) v);
                showCal2((EditText) v);
                break;
            case R.id.etDateToOrg:// get date to
                if (!etDateFromOrg.getText().toString().trim().isEmpty()) {
//                    showCal3((EditText) v);
                    showCal4((EditText) v);
                } else {
//                    Toast.makeText(context, getResources().getString(R.string.from_year_empty_input), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(ProfessionActivity.this, getResources().getString(R.string.from_year_empty_input));
                }
        }
    }

    private void showCal4(final EditText v) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = df.parse(etDateFromOrg.getText().toString().trim());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        final Calendar calendarmin = Calendar.getInstance();
        if (etDateFromOrg.getText().toString().trim().equalsIgnoreCase(df.format(calendar.getTimeInMillis()))) {
            calendarmin.setTime(date);
        } else {
            calendarmin.setTime(date);
            calendarmin.add(Calendar.DATE, 1);
        }

        Calendar calendar = Calendar.getInstance();
        new SpinnerDatePickerDialogBuilder()
                .context(ProfessionActivity.this)
                .callback(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        monthOfYear += 1;
                        String month = "" + monthOfYear;
                        String date = "" + dayOfMonth;
                        if (date.length() == 1) {
                            date = "0" + date;
                        }
                        if (month.length() == 1) {
                            month = "0" + month;
                        }
                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        String selectedDateString = year + "-" + month + "-" + date;
                        try {
                            Date selectedDate = df.parse(selectedDateString);

                            if (selectedDate.getTime() > df.parse(df.format(Calendar.getInstance().getTime())).getTime()) {
//                                Toast.makeText(context, getResources().getString(R.string.to_year_not_more_then_today), Toast.LENGTH_SHORT).show();
                                Utils.showPopup(ProfessionActivity.this, getResources().getString(R.string.to_year_not_more_then_today));
                                return;
                            }

                            toDate = year + "-" + month + "-" + date;


                            Date toDay = Calendar.getInstance().getTime();
                            String toDayS = df.format(toDay);
                            if (toDate.toString().trim().equalsIgnoreCase(toDayS.trim())) {
                                cbCurrentOrg.setChecked(true);
                            } else {
                                cbCurrentOrg.setChecked(false);
                            }
                        } catch (Exception e) {

                        }
                        //setting date to 'to' field
                        v.setText(date + "-" + month + "-" + year);
                    }
                })
                .spinnerTheme(R.style.NumberPickerStyle)
                .showTitle(true)
                .showDaySpinner(true)
                .defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
//                .maxDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .minDate(calendarmin.get(Calendar.YEAR), calendarmin.get(Calendar.MONTH), calendarmin.get(Calendar.DAY_OF_MONTH))
                .build()
                .show();
    }

    private void showCal3(final EditText v) {
        final DatePickerFragmentDialog datePickerDialog = DatePickerFragmentDialog.newInstance(new DatePickerFragmentDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerFragmentDialog view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear += 1;
                String month = "" + monthOfYear;
                String date = "" + dayOfMonth;
                if (date.length() == 1) {
                    date = "0" + date;
                }
                if (month.length() == 1) {
                    month = "0" + month;
                }
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String selectedDateString = year + "-" + month + "-" + date;
                try {
                    Date selectedDate = df.parse(selectedDateString);

                    if (selectedDate.getTime() > df.parse(df.format(Calendar.getInstance().getTime())).getTime()) {
//                        Toast.makeText(context, getResources().getString(R.string.to_year_not_more_then_today), Toast.LENGTH_SHORT).show();
                        Utils.showPopup(ProfessionActivity.this, getResources().getString(R.string.to_year_not_more_then_today));
                        return;
                    }

                    toDate = year + "-" + month + "-" + date;


                    Date toDay = Calendar.getInstance().getTime();
                    String toDayS = df.format(toDay);
                    if (toDate.toString().trim().equalsIgnoreCase(toDayS.trim())) {
                        cbCurrentOrg.setChecked(true);
                    } else {
                        cbCurrentOrg.setChecked(false);
                    }
                } catch (Exception e) {

                }
                //setting date to 'to' field
                v.setText(date + "-" + month + "-" + year);
            }
        }, year, month, day);

        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            Date date = df.parse(etDateFromOrg.getText().toString().trim());
            final Calendar calendar = Calendar.getInstance();
            if (etDateFromOrg.getText().toString().trim().equalsIgnoreCase(df.format(calendar.getTimeInMillis()))) {
                calendar.setTime(date);
            } else {
                calendar.setTime(date);
                calendar.add(Calendar.DATE, 1);
            }

            //calendar.add(Calendar.DATE, 1);
            //datePickerDialog.setMaxDate(Calendar.getInstance());
            datePickerDialog.setMinDate(calendar);
            datePickerDialog.setTitle("");
            datePickerDialog.show(getSupportFragmentManager(), "");
        } catch (Exception ignored) {

        }
    }

    private void showCal(final EditText v) {
        DatePickerFragmentDialog datePickerDialog1 = DatePickerFragmentDialog.newInstance(new DatePickerFragmentDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerFragmentDialog view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear += 1;
                String month = "" + monthOfYear;
                String date = "" + dayOfMonth;
                if (date.length() == 1) {
                    date = "0" + date;
                }
                if (month.length() == 1) {
                    month = "0" + month;
                }
                //setting date to from field
                //((EditText) v).setText(year + "-" + month + "-" + date);
                v.setText(date + "-" + month + "-" + year);
                etDateToOrg.setText("");
                cbCurrentOrg.setEnabled(true);
                cbCurrentOrg.setChecked(false);
            }
        }, year, month, day);

        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date date = df.parse("1947-01-01");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            datePickerDialog1.setMinDate(calendar.getTimeInMillis());
            //Calendar calendar = Calendar.getInstance();
            datePickerDialog1.setMaxDate(Calendar.getInstance().getTimeInMillis());
        } catch (Exception e) {

        }
        datePickerDialog1.setTitle("");
        datePickerDialog1.show(getSupportFragmentManager(), "");
    }

    private void showCal2(final EditText v) {
        Calendar calendar = Calendar.getInstance();
        new SpinnerDatePickerDialogBuilder()
                .context(ProfessionActivity.this)
                .callback(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        monthOfYear += 1;
                        String month = "" + monthOfYear;
                        String date = "" + dayOfMonth;
                        if (date.length() == 1) {
                            date = "0" + date;
                        }
                        if (month.length() == 1) {
                            month = "0" + month;
                        }
                        //setting date to from field
                        //((EditText) v).setText(year + "-" + month + "-" + date);
                        v.setText(date + "-" + month + "-" + year);
                        etDateToOrg.setText("");
                        cbCurrentOrg.setEnabled(true);
                        cbCurrentOrg.setChecked(false);
                    }
                })
                .spinnerTheme(R.style.NumberPickerStyle)
                .showTitle(true)
                .showDaySpinner(true)
                .defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .maxDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .minDate(calendar.get(Calendar.YEAR) - 100, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                .build()
                .show();
    }
}
