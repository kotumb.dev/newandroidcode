package com.codeholic.kotumb.app.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReportPost implements Serializable {

    @SerializedName("id")
    public String id = "";

    @SerializedName("post_id")
    public String post_id = "";

    @SerializedName("comment_id")
    public String comment_id = "";

    @SerializedName("group_id")
    public String group_id = "";

    @SerializedName("userId")
    public String userId = "";

    @SerializedName("description")
    public String description = "";

    @SerializedName("createdAt")
    public String createdAt = "";

    @SerializedName("updatedAt")
    public String updatedAt = "";

    @SerializedName("firstName")
    public String firstName = "";

    @SerializedName("lastName")
    public String lastName = "";

    @SerializedName("avatar")
    public String avatar = "";

    @SerializedName("post_info")
    public Post post_info ;

    @SerializedName("group_comment_info")
    public PostComments group_comment_info ;

}
