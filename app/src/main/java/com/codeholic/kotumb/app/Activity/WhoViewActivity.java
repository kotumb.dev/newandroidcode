package com.codeholic.kotumb.app.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Adapter.UserRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;

public class WhoViewActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.tvNouser)
    TextView tvNouser;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    List<User> users;
    UserRecyclerViewAdapter userRecyclerViewAdapter;
    int pagination = 0;
    private LinearLayoutManager linearLayoutManager;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_who_view);
        ButterKnife.bind(this);
        initToolbar();
        setView();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.UPDATE_USER);
        registerReceiver(receiver, filter);
    }

    private void setView() {
        if (!Utility.isConnectingToInternet(this)) {
            final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
            snackbar.show();
            return;
        }
        tvNouser.setText(getResources().getString(R.string.profile_view_no_view));
        API.sendRequestToServerGET(this, API.PROFILE_VIEWS  + SharedPreferencesMethod.getUserId(this)+"/0", API.PROFILE_VIEWS); // service call for user list
        try {
            /*getResponse(new JSONObject("{\n" +
                    "  \"success\": \"success\",\n" +
                    "  \"RP_MESSAGE\": \"ALL_OKAY\",\n" +
                    "  \"viewers\": [\n" +
                    "    {\n" +
                    "      \"viewerId\": \"12\",\n" +
                    "      \"viewedStartTime\": \"\",\n" +
                    "      \"viewedEndTime\": null,\n" +
                    "      \"firstName\": \"maruti\",\n" +
                    "      \"lastName\": \"raj\",\n" +
                    "      \"avatar\": \"default_profile.png\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"viewerId\": \"19\",\n" +
                    "      \"viewedStartTime\": \"2017-11-25 14:44:30\",\n" +
                    "      \"viewedEndTime\": \"2017-11-25 15:02:20\",\n" +
                    "      \"firstName\": \"Brajesh\",\n" +
                    "      \"lastName\": \"Sharma\",\n" +
                    "      \"avatar\": \"1511693349_01f1eb754fc311375ba3ebc4dd8801a2.png\"\n" +
                    "    }\n" +
                    "  ]\n" +
                    "}"), 1);*/
        } catch (Exception e) {

        }
    }

    //init toolbar function
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        header.setText(getResources().getString(R.string.profile_view_title));

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //getResponse for getting list of urlImages
    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        Log.e("response", "" + outPut);
        try {
            if (i == 0) { // for getting urlImages
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                        }
                        users = new ArrayList<>();
                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                            user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                            user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                            user.setCity(searchResults.getString(ResponseParameters.City));
                            user.setState(searchResults.getString(ResponseParameters.State));
                            user.setUserInfo(searchResults.toString());
                            users.add(user);
                        }
                        //setting options for recycler adapter
                        linearLayoutManager = new LinearLayoutManager(this);
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserList.setLayoutManager(linearLayoutManager);
                        userRecyclerViewAdapter = new UserRecyclerViewAdapter(rvUserList, this, users);
                        rvUserList.setAdapter(userRecyclerViewAdapter);
                        //load more setup for recycler adapter
                        userRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                if (users.size() >= 6) {
                                    rvUserList.post(new Runnable() {
                                        public void run() {
                                            users.add(null);
                                            userRecyclerViewAdapter.notifyItemInserted(users.size() - 1);
                                        }
                                    });
                                    API.sendRequestToServerGET(WhoViewActivity.this, API.PROFILE_VIEWS  + SharedPreferencesMethod.getUserId(WhoViewActivity.this) + "/" + (pagination + 6), API.PROFILE_VIEWS_UPDATE);
                                }
                            }
                        });

                    } else {
                        //no userfound
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } else if (i == 1) {// for updating list
                users.remove(users.size() - 1); // removing of loading item
                userRecyclerViewAdapter.notifyItemRemoved(users.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response

                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                            for (int j = 0; j < userList.length(); j++) {
                                JSONObject searchResults = userList.getJSONObject(j);
                                User user = new User();
                                user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                                user.setLastName(searchResults.getString(ResponseParameters.LastName));
                                user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                                user.setUserId(searchResults.getString(ResponseParameters.UserId));
                                user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                                user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                                user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                                user.setCity(searchResults.getString(ResponseParameters.City));
                                user.setState(searchResults.getString(ResponseParameters.State));
                                user.setUserInfo(searchResults.toString());
                                users.add(user);
                            }
                            pagination = pagination + 6;
                            //updating recycler view
                            userRecyclerViewAdapter.notifyDataSetChanged();
                            userRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            }

        } catch (Exception e) {
            //mo user found
            llNotFound.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            Log.e("test", "test");
            User updatedUser = intent.getParcelableExtra("USER");
            for (int i = 0; i < users.size(); i++) {
                User user = users.get(i);
                if (updatedUser.getUserId().equals(user.getUserId())) {
                    try {
                        users.remove(i);
                        users.add(i, updatedUser);
                        userRecyclerViewAdapter.notifyItemChanged(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroy();
    }
}
