package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Activity.CompleteProfileActivity;
import com.codeholic.kotumb.app.Activity.OtherUserProfileActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.fragments.ConnectionFragment;
import com.codeholic.kotumb.app.fragments.InviteFragment;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class RequestsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<User> users;
    Activity activity;
    Fragment connectionFragment;


    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;


    public void setTab(int tab) {
        this.tab = tab;
    }

    int tab = 0;

    public RequestsRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<User> users, Fragment connectionFragment) {
        this.users = users;
        this.activity = activity;
        this.connectionFragment = connectionFragment;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }


    @Override
    public int getItemViewType(int position) {
        return users.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.requests_list_item, parent, false);
            UserViewHolder UserViewHolder = new UserViewHolder(view);
            return UserViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {
        User user = users.get(position);
        if (viewholder instanceof UserViewHolder) {
            UserViewHolder holder = (UserViewHolder) viewholder;
            holder.tvUserName.setText("" + user.getFirstName().trim() + " " + user.getLastName().trim());
            System.out.println("User Name     "+user.getFirstName().trim() + " " + user.getLastName().trim());
            if (user.getProfileSummary() != null && !user.getProfileSummary().trim().equalsIgnoreCase("null") && !user.getProfileSummary().trim().isEmpty()) {
                holder.tvProfession.setText("" + user.getProfileSummary().trim());
                holder.tvProfession.setVisibility(View.VISIBLE);
            } else {
                holder.tvProfession.setText("--");
            }
            if (user.getAadhaarInfo().equalsIgnoreCase("true")) {
                holder.tvAadharVerified.setVisibility(View.VISIBLE);
            } else {
                holder.tvAadharVerified.setVisibility(View.GONE);
            }
            new AQuery(activity).id(holder.ivUserImage).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.ic_user);
            setOnClickListener(holder, user, position);
            if (user.getIsBlocked().equalsIgnoreCase("1")
                    || user.getIsDeleted().equalsIgnoreCase("1")) {
                if (holder.llAcceptConnect!=null) {
                    holder.llAcceptConnect.setVisibility(GONE);
                }
            }

            try {
                JSONObject outPut = new JSONObject(user.getUserInfo());
                if (outPut.has(ResponseParameters.CONNECTION_FLAG) && !SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(user.getUserId().trim())) {
                    holder.llAcceptConnect.setVisibility(VISIBLE);
                    if (outPut.getBoolean(ResponseParameters.CONNECTION_FLAG)) {
                        if (outPut.has(ResponseParameters.CONNECTION_INFO)) {
                            JSONObject CONNECTION_INFO = outPut.getJSONObject(ResponseParameters.CONNECTION_INFO);
                            String status = CONNECTION_INFO.getString(ResponseParameters.STATUS);
                            String ACTION_USER_ID = CONNECTION_INFO.getString(ResponseParameters.ACTIONUSERID);
                            if (status.trim().equalsIgnoreCase("0")) {
                                if (SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(ACTION_USER_ID.trim())) {
                                    holder.tvConnectAccept.setText(activity.getResources().getString(R.string.request_cancel_btn_text).trim());
                                    holder.tvConnectAccept.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_decline_request, 0, 0 ,0);
                                    holder.llDecline.setVisibility(View.GONE);
                                } else {
                                    holder.tvConnectAccept.setText(activity.getResources().getString(R.string.request_accept_btn_text).trim());
                                    holder.llDecline.setVisibility(VISIBLE);
                                }
                            } else if (status.trim().equalsIgnoreCase("1")) {
                                holder.tvConnectAccept.setText(activity.getResources().getString(R.string.unfriend_btn_text).trim());
                                holder.llDecline.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        holder.llDecline.setVisibility(View.GONE);
                        holder.tvConnectAccept.setText(activity.getResources().getString(R.string.connect_btn_text).trim());
                    }
                }

                if(outPut.has(ResponseParameters.CONNECTION_INFO)){
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    JSONObject jsonObject = outPut.getJSONObject(ResponseParameters.CONNECTION_INFO);
                    String date;
                    if (jsonObject.has(ResponseParameters.CONNECTIONS_DATE)) {
                        date = jsonObject.getString(ResponseParameters.CONNECTIONS_DATE);
                    } else {
                        DateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date time = Calendar.getInstance().getTime();
                        date = serverFormat.format(time);
                    }
                    DateFormat formatText = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
                    SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
                    SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
                    holder.tvTime.setText(""+formatText.format(format.parse(date.split(" ")[0])) + " "+_12HourSDF.format(_24HourSDF.parse(date.split(" ")[1])));
                    holder.tvTime.setVisibility(VISIBLE);
                } else {
                    holder.tvTime.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
                holder.tvTime.setText("Just Now");
                holder.tvTime.setVisibility(View.VISIBLE);
            }
        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    public void setOnClickListener(final UserViewHolder holder, final User user, final int position) {
        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(user.getUserId().trim())) {
                    Intent intent = new Intent(activity, OtherUserProfileActivity.class);
                    intent.putExtra("USER", user);
                    activity.startActivity(intent);
                } else {
                    Intent intent = new Intent(activity, CompleteProfileActivity.class);
                    intent.putExtra(ResponseParameters.Id, R.id.ll_personal_details);
                    //intent.putExtra("DATA", new JSONObject().toString());
                    activity.startActivity(intent);
                }

                /*Intent intent = new Intent(activity, OtherUserProfileActivity.class);
                intent.putExtra("USER", user);
                activity.startActivity(intent);*/
            }
        });

        holder.tvDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(user.getUserId().trim())) {
                    Intent intent = new Intent(activity, OtherUserProfileActivity.class);
                    intent.putExtra("USER", user);
                    activity.startActivity(intent);
                }
            }
        });

        holder.llAcceptConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!Utility.isConnectingToInternet(activity)) {
                        final Snackbar snackbar = Snackbar.make(holder.llAcceptConnect, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }
                    final HashMap<String, Object> input = new HashMap<>();
                    input.put(RequestParameters.USERID1, "" + SharedPreferencesMethod.getUserId(activity));
                    input.put(RequestParameters.USERID2, "" + user.getUserId());
                    input.put(RequestParameters.ACTIONUSERID, "" + SharedPreferencesMethod.getUserId(activity));
                    final JSONObject jsonObject = new JSONObject(users.get(position).getUserInfo());
                    if (jsonObject.getBoolean(ResponseParameters.CONNECTION_FLAG)) {
                        if (jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).getString(ResponseParameters.STATUS).trim().equalsIgnoreCase("0")) {
                            if (SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).getString(ResponseParameters.ACTIONUSERID).trim())) {
                                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                                dialogBuilder.setTitle(activity.getResources().getString(R.string.app_alert_text));
                                dialogBuilder.setMessage(activity.getResources().getString(R.string.confirm_cancel_request_message));
                                dialogBuilder.setCancelable(true);
                                dialogBuilder.setPositiveButton(activity.getResources().getString(R.string.primary_number_yes_btn_text), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        try {
                                            if (!Utility.isConnectingToInternet(activity)) {
                                                final Snackbar snackbar = Snackbar.make(holder.llDecline, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                                                snackbar.show();
                                                return;
                                            }


                                            input.put(RequestParameters.STATUS, "2");
                                            Log.e("request", "" + input);
                                            requestServiceCall(holder, input, jsonObject, position);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        dialog.dismiss();
                                    }
                                });

                                dialogBuilder.setNegativeButton(activity.getResources().getString(R.string.primary_number_no_btn_text), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                    }
                                });

                                dialogBuilder.create().show();


                                //  jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "2");
                                // jsonObject.put(ResponseParameters.CONNECTION_FLAG, false);
                                //   holder.tvConnectAccept.setText(activity.getResources().getString(R.string.connect).trim());
                                //   holder.llDecline.setVisibility(GONE);
                            } else {
                                //jsonObject.put(ResponseParameters.CONNECTION_FLAG, false);
                                // jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "1");

                                input.put(RequestParameters.STATUS, "1");
                                Log.e("request", "" + input);
                                requestServiceCall(holder, input, jsonObject, position);
                                // holder.llDecline.setVisibility(GONE);
                                //  holder.tvConnectAccept.setText(activity.getResources().getString(R.string.disconnect).trim());
                            }
                        } else if (jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).getString(ResponseParameters.STATUS).trim().equalsIgnoreCase("1")) {


                            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                            dialogBuilder.setTitle(activity.getResources().getString(R.string.app_alert_text));
                            dialogBuilder.setMessage(activity.getResources().getString(R.string.confirm_unfriend_message));
                            dialogBuilder.setCancelable(true);
                            dialogBuilder.setPositiveButton(activity.getResources().getString(R.string.done_btn_text), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    try {
                                        if (!Utility.isConnectingToInternet(activity)) {
                                            final Snackbar snackbar = Snackbar.make(holder.llDecline, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                                            snackbar.show();
                                            return;
                                        }

                                        input.put(RequestParameters.STATUS, "4");
                                        Log.e("request", "" + input);
                                        requestServiceCall(holder, input, jsonObject, position);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    dialog.dismiss();
                                }
                            });

                            dialogBuilder.setNegativeButton(activity.getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.dismiss();
                                }
                            });
                            dialogBuilder.create().show();

                            // jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "4");
                            //jsonObject.put(ResponseParameters.CONNECTION_FLAG, false);
                            // holder.tvConnectAccept.setText(activity.getResources().getString(R.string.connect).trim());
                            // holder.llDecline.setVisibility(GONE);

                        }
                    } else {
                        input.put(RequestParameters.STATUS, "0");
                        Log.e("request", "" + input);
                        if (!jsonObject.has(ResponseParameters.CONNECTION_INFO)) {
                            //  jsonObject.put(ResponseParameters.CONNECTION_INFO, new JSONObject());
                        }
                        requestServiceCall(holder, input, jsonObject, position);
                        // jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "0");
                        // jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.ACTIONUSERID, "" + SharedPreferencesMethod.getLinkName(activity));
                        //  jsonObject.put(ResponseParameters.CONNECTION_FLAG, true);
                        // holder.llDecline.setVisibility(GONE);
                        // holder.tvConnectAccept.setText(activity.getResources().getString(R.string.cancel_req).trim());
                    }
                    //recommendationsList.get(position).setUserInfo(jsonObject.toString());

                    //Log.e("request", "" + input);

                    // API.sendRequestToServerPOST_PARAM(activity, API.CONNECT, input);

//////
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        holder.llDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                    dialogBuilder.setTitle(activity.getResources().getString(R.string.app_alert_text));
                    dialogBuilder.setMessage(activity.getResources().getString(R.string.confirm_decline_request_message));
                    dialogBuilder.setCancelable(true);
                    dialogBuilder.setPositiveButton(activity.getResources().getString(R.string.primary_number_yes_btn_text), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (!Utility.isConnectingToInternet(activity)) {
                                final Snackbar snackbar = Snackbar.make(holder.llDecline, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                                snackbar.show();
                                return;
                            }
                            HashMap<String, Object> input = new HashMap<>();
                            input.put(RequestParameters.USERID1, "" + SharedPreferencesMethod.getUserId(activity));
                            input.put(RequestParameters.USERID2, "" + user.getUserId());
                            input.put(RequestParameters.ACTIONUSERID, "" + SharedPreferencesMethod.getUserId(activity));
                            input.put(RequestParameters.STATUS, "2");
                            Log.e("input", "" + input);

                            try {
                                final com.codeholic.kotumb.app.View.ProgressBar progressBar = new com.codeholic.kotumb.app.View.ProgressBar(activity);
                                progressBar.show(activity.getResources().getString(R.string.app_please_wait_text));
                                AQuery aq = new AQuery(activity);
                                //Log.d("Url ", url);
                                aq.ajax(API.CONNECT, input, JSONObject.class, new AjaxCallback<JSONObject>() {
                                    @Override
                                    public void callback(String url, JSONObject json, AjaxStatus status) {
                                        progressBar.dismiss();
                                        try {
                                            Log.d("API_OUTPUT", "" + json);
                                            if (json != null) {

                                                json.put("RP_MESSAGE", "ALL_OKAY");
                                                JSONObject jsonObject = new JSONObject(users.get(position).getUserInfo());
                                                jsonObject.put(ResponseParameters.CONNECTION_FLAG, false);
                                                users.get(position).setUserInfo(jsonObject.toString());
                                                // holder.tvConnectAccept.setText(activity.getResources().getString(R.string.connect).trim());
                                                // holder.llDecline.setVisibility(GONE);
                                                users.remove(position);
                                                notifyItemRemoved(position);
                                                notifyItemRangeChanged(position, users.size());

                                                if (users.size() <= 0) {
                                                    if (connectionFragment instanceof ConnectionFragment)
                                                        ((ConnectionFragment) connectionFragment).showNoUser();
                                                    if (connectionFragment instanceof InviteFragment.PlaceholderFragment)
                                                        ((InviteFragment.PlaceholderFragment) connectionFragment).showNoUser();
                                                }

                                                if (connectionFragment instanceof InviteFragment.PlaceholderFragment) {
                                                    if (users.size() <= 0) {
                                                        ((InviteFragment.PlaceholderFragment) connectionFragment).updateCount(tab);
                                                    } else {
                                                        ((InviteFragment.PlaceholderFragment) connectionFragment).updateCount(tab);
                                                    }
                                                }

                                            } else {
                                                JSONObject output = new JSONObject();
                                                if (status.getCode() == AjaxStatus.NETWORK_ERROR) {
                                                    output.put("RP_MESSAGE", "NO NETWORK");
                                                } else {
                                                    output.put("RP_MESSAGE", "ERROR");
                                                }

                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }.header("Auth-Key","KOTUMB__AuTh_keY"));

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                            dialog.dismiss();
                        }
                    });

                    dialogBuilder.setNegativeButton(activity.getResources().getString(R.string.primary_number_no_btn_text), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                        }
                    });

                    dialogBuilder.create().show();

                    //API.sendRequestToServerPOST_PARAM(activity, API.CONNECT, input);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void requestServiceCall(final UserViewHolder holder, HashMap<String, Object> input, final JSONObject jsonObject, final int position) {
        if (!Utility.isConnectingToInternet(activity)) {
            final Snackbar snackbar = Snackbar.make(holder.tvConnectAccept, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
            snackbar.show();
            return;
        }


        try {
            final com.codeholic.kotumb.app.View.ProgressBar progressBar = new com.codeholic.kotumb.app.View.ProgressBar(activity);
            progressBar.show(activity.getResources().getString(R.string.app_please_wait_text));
            AQuery aq = new AQuery(activity);
            //Log.d("Url ", url);
            aq.ajax(API.CONNECT, input, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    progressBar.dismiss();
                    try {
                        Log.d("API_OUTPUT", "" + json);
                        if (json != null) {
                            if (jsonObject.getBoolean(ResponseParameters.CONNECTION_FLAG)) {
                                if (jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).getString(ResponseParameters.STATUS).trim().equalsIgnoreCase("0")) {
                                    if (SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).getString(ResponseParameters.ACTIONUSERID).trim())) {
                                        jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "2");
                                        jsonObject.put(ResponseParameters.CONNECTION_FLAG, false);
                                        // holder.tvConnectAccept.setText(activity.getResources().getString(R.string.connect).trim());
                                        //holder.llDecline.setVisibility(GONE);
                                    } else {
                                        jsonObject.put(ResponseParameters.CONNECTION_FLAG, false);
                                        jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "1");
                                        //holder.llDecline.setVisibility(GONE);
                                        // holder.tvConnectAccept.setText(activity.getResources().getString(R.string.disconnect).trim());
                                    }
                                } else if (jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).getString(ResponseParameters.STATUS).trim().equalsIgnoreCase("1")) {
                                    jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "4");
                                    jsonObject.put(ResponseParameters.CONNECTION_FLAG, false);
                                    //holder.tvConnectAccept.setText(activity.getResources().getString(R.string.connect).trim());
                                    //holder.llDecline.setVisibility(GONE);
                                }
                            } else {
                                if (!jsonObject.has(ResponseParameters.CONNECTION_INFO)) {
                                    jsonObject.put(ResponseParameters.CONNECTION_INFO, new JSONObject());
                                }
                                jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.STATUS, "0");
                                jsonObject.getJSONObject(ResponseParameters.CONNECTION_INFO).put(ResponseParameters.ACTIONUSERID, "" + SharedPreferencesMethod.getUserId(activity));
                                jsonObject.put(ResponseParameters.CONNECTION_FLAG, true);
                                // holder.llDecline.setVisibility(GONE);
                                //holder.tvConnectAccept.setText(activity.getResources().getString(R.string.cancel_req).trim());
                            }
                            users.get(position).setUserInfo(jsonObject.toString());
                            users.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, users.size());
                            if (users.size() <= 0) {
                                if (connectionFragment instanceof ConnectionFragment)
                                    ((ConnectionFragment) connectionFragment).showNoUser();
                                if (connectionFragment instanceof InviteFragment.PlaceholderFragment)
                                    ((InviteFragment.PlaceholderFragment) connectionFragment).showNoUser();
                            }

                            if (connectionFragment instanceof InviteFragment.PlaceholderFragment) {
                                if (users.size() <= 0) {
                                    ((InviteFragment.PlaceholderFragment) connectionFragment).updateCount(tab);
                                } else {
                                    ((InviteFragment.PlaceholderFragment) connectionFragment).updateCount(tab);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.header("Auth-Key","KOTUMB__AuTh_keY"));

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvConnectAccept, tvProfession, tvAadharVerified, tvTime,tvDetails;
        ImageView ivUserImage;
        CardView cvContainer;
        LinearLayout llAcceptConnect, llDecline;


        UserViewHolder(View view) {
            super(view);
            tvUserName = view.findViewById(R.id.tvUserName);
            tvConnectAccept = view.findViewById(R.id.tvConnectAccept);
            ivUserImage = view.findViewById(R.id.ivUserImage);
            tvProfession = view.findViewById(R.id.tvProfession);
            tvTime = view.findViewById(R.id.tvTime);
            tvAadharVerified = view.findViewById(R.id.tvAadharVerified);
            cvContainer = view.findViewById(R.id.cvContainer);
            llAcceptConnect = view.findViewById(R.id.llAcceptConnect);
            llDecline = view.findViewById(R.id.llDecline);
            tvDetails = view.findViewById(R.id.tvDetails);
        }
    }


}