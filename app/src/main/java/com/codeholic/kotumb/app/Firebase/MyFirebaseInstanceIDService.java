package com.codeholic.kotumb.app.Firebase;

/**
 * Created by DELL on 4/12/2017.
 */

import android.content.Context;
import android.util.Log;

import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.google.firebase.messaging.FirebaseMessagingService;


import java.util.HashMap;


public class MyFirebaseInstanceIDService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseIIDService";

    private Context context;


//    @Override
//    public void onTokenRefresh() {
//        super.onTokenRefresh();
//
//        Log.d(TAG, "Refreshed token: ");
//        FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                        if (!task.isSuccessful()) {
//                            Log.w(TAG, "getInstanceId failed", task.getException());
//                            return;
//                        }
//
//                        // Get new Instance ID token
//                        String token = task.getResult().getToken();
//                        Log.e("My Token",token);
//                        sendRegistrationToServer(token);
//                    }
//                });
//    }


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        Log.e("My Token",s);
        sendRegistrationToServer(s);
    }

    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
        try {
            HashMap<String, Object> input = new HashMap<>();
            input.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(getApplicationContext()));
            input.put(RequestParameters.TOKEN, token);
            API.sendRequestToServerPOST_PARAM(getApplicationContext(), API.ADD_TOKEN, input);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
