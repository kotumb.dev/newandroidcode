package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.ImageViewActivity;
import com.codeholic.kotumb.app.Activity.ImagesShowActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.R;

import java.util.ArrayList;
import java.util.List;


public class ImagesRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<String> imagesName;
    List<byte[]> imagesByteAraay;
    ArrayList<Uri> uriList = new ArrayList<>();
    Activity activity;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;
    private boolean isURI = false;


    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;
    private boolean showCancel = true;

    public void setShowCancel(boolean showCancel) {
        this.showCancel = showCancel;
    }

    public void setGroupImageBaseUrl(String base_url) {
        this.groupImageBaseUrl = base_url;
    }

    String groupImageBaseUrl = "";

    public ImagesRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<String> imagesName) {
        this.imagesName = imagesName;
        this.activity = activity;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
        isURI = false;
    }

    public ImagesRecyclerViewAdapter(Activity activity, List<byte[]> imagesByteAraay, ArrayList<Uri> uriList) {
        this.imagesByteAraay = imagesByteAraay;
        this.activity = activity;
        this.uriList = uriList;
        isURI = true;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        if (isURI)
            return imagesByteAraay.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        return imagesName.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        if (isURI)
            return imagesByteAraay.size();
        return imagesName.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.image_list_item, parent, false);
            ImageViewHolder ImageViewHolder = new ImageViewHolder(view);
            return ImageViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);

        }

        return null;
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {


        if (viewholder instanceof ImageViewHolder) {
            ImageViewHolder holder = (ImageViewHolder) viewholder;
            if (isURI) {
                final Bitmap bitmap = BitmapFactory.decodeByteArray(imagesByteAraay.get(position), 0, imagesByteAraay.get(position).length);
                holder.ivSelectedImage.setImageBitmap(bitmap);
                holder.ivSelectedImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ImageViewActivity.setImage(bitmap);
                        activity.startActivity(new Intent(activity, ImageViewActivity.class));
                    }
                });
            } else {
                final String images = imagesName.get(position);
                new AQuery(activity).id(holder.ivSelectedImage).image(groupImageBaseUrl + images, true, true, 300, R.drawable.ic_user);
                holder.ivSelectedImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ImageViewActivity.setLink(groupImageBaseUrl + images);
                        activity.startActivity(new Intent(activity, ImageViewActivity.class));
                    }
                });
            }
            if (showCancel) {
                holder.ivClear.setVisibility(View.VISIBLE);
            } else {
                holder.ivClear.setVisibility(View.GONE);
            }
            setOnClickListener(holder, position);


        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    public void setOnClickListener(final ImageViewHolder holder, final int position) {

        holder.ivClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isURI) {
                    notifyItemRemoved(position);
                    imagesByteAraay.remove(position);
                    uriList.remove(position);
                    notifyItemRangeChanged(0, imagesByteAraay.size());
                } else {
                    notifyItemRemoved(position);
                    imagesName.remove(position);
                    notifyItemRangeChanged(0, imagesName.size());
                }
                ((ImagesShowActivity) activity).hideView();
            }
        });


    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        ImageView ivSelectedImage, ivClear;


        ImageViewHolder(View view) {
            super(view);
            ivSelectedImage = (ImageView) view.findViewById(R.id.ivSelectedImage);
            ivClear = (ImageView) view.findViewById(R.id.ivClear);
        }
    }
}