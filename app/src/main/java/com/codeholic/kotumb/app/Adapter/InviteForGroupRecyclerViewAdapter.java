package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.ConnectionsActivity;
import com.codeholic.kotumb.app.Activity.GroupInviteActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class InviteForGroupRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<User> users;
    Activity activity;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;

    public void setSuggestion(boolean suggestion) {
        this.suggestion = suggestion;
    }

    private boolean suggestion = false;

    public InviteForGroupRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<User> users) {
        this.users = users;
        this.activity = activity;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return users.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.user_list_invite_group, parent, false);
            UserViewHolder userViewHolder = new UserViewHolder(view);
            userViewHolder.setIsRecyclable(false);
            return userViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            if (suggestion) {
                View view = LayoutInflater.from(activity).inflate(R.layout.item_view_all, parent, false);
                return new ViewAllViewHolder(view);
            } else {
                View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
                return new LoadingViewHolder(view);
            }

        }

        return null;
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    private class ViewAllViewHolder extends RecyclerView.ViewHolder {
        public CardView cvViewAll;

        public ViewAllViewHolder(View view) {
            super(view);
            cvViewAll = view.findViewById(R.id.cvViewAll);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {
        User user = users.get(position);

        if (viewholder instanceof UserViewHolder) {
            UserViewHolder holder = (UserViewHolder) viewholder;
            holder.tvUserName.setText("" + user.getFirstName() + " " + user.getLastName());

            if (user.getProfileSummary() != null && !user.getProfileSummary().trim().equalsIgnoreCase("null") && !user.getProfileSummary().trim().isEmpty()) {
                holder.tvAbout.setText("" + user.getProfileSummary().trim());
                holder.tvAbout.setVisibility(View.VISIBLE);
            } else {
                holder.tvAbout.setText("");
                holder.tvAbout.setVisibility(View.GONE);
            }

            if(GroupInviteActivity.members.contains(user.getUserId())){
                holder.rlSelectUser.setVisibility(VISIBLE);
                holder.tvInviteMember.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.tvInviteMember.setBackground(activity.getResources().getDrawable(R.drawable.text_primary_color));
            }

            if(GroupInviteActivity.admins.contains(user.getUserId())){
                holder.rlSelectUser.setVisibility(VISIBLE);
                holder.tvInviteMember.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.tvInviteMember.setBackground(activity.getResources().getDrawable(R.drawable.text_primary_color));
                holder.tvSetAsAdmin.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                holder.tvSetAsAdmin.setBackground(activity.getResources().getDrawable(R.drawable.text_primary_color));
            }


            holder.tvLocation.setText(user.getCity().trim() + ", " + user.getState().trim());
            new AQuery(activity).id(holder.ivUserImage).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.ic_user);
            setOnClickListener(holder, user, position);

        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        } else if (viewholder instanceof ViewAllViewHolder) {
            ViewAllViewHolder viewAllViewHolder = (ViewAllViewHolder) viewholder;
            viewAllViewHolder.cvViewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, ConnectionsActivity.class);
                    intent.putExtra("SUGGESTION", true);
                    activity.startActivity(intent);
                }
            });
        }
    }

    public void setOnClickListener(final UserViewHolder holder, final User user, final int position) {

        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.rlSelectUser.getVisibility() == GONE) {
                    updateSelectedUserList(user);
                    ((GroupInviteActivity) activity).members.add(user.getUserId());
                    holder.rlSelectUser.setVisibility(VISIBLE);
                    holder.tvInviteMember.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                    holder.tvInviteMember.setBackground(activity.getResources().getDrawable(R.drawable.text_primary_color));
                } else {
                    updateSelectedUserList(user);
                    holder.rlSelectUser.setVisibility(GONE);
                    holder.tvInviteMember.setTextColor(activity.getResources().getColor(R.color.black));
                    holder.tvInviteMember.setBackground(activity.getResources().getDrawable(R.drawable.text_black_color));
                }

                holder.tvSetAsAdmin.setTextColor(activity.getResources().getColor(R.color.black));
                holder.tvSetAsAdmin.setBackground(activity.getResources().getDrawable(R.drawable.text_black_color));
                ((GroupInviteActivity) activity).changeView();
            }
        });

        holder.tvSetAsAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.rlSelectUser.getVisibility() == GONE) {
                    updateSelectedUserList(user);
                    ((GroupInviteActivity) activity).admins.add(user.getUserId());
                    holder.rlSelectUser.setVisibility(VISIBLE);
                    holder.tvSetAsAdmin.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                    holder.tvSetAsAdmin.setBackground(activity.getResources().getDrawable(R.drawable.text_primary_color));
                } else {
                    updateSelectedUserList(user);
                    holder.rlSelectUser.setVisibility(GONE);
                    holder.tvSetAsAdmin.setTextColor(activity.getResources().getColor(R.color.black));
                    holder.tvSetAsAdmin.setBackground(activity.getResources().getDrawable(R.drawable.text_black_color));
                }
                ((GroupInviteActivity) activity).changeView();
            }
        });
    }

    private void updateSelectedUserList(User user) {
        if (((GroupInviteActivity) activity).admins.contains(user.getUserId())) {
            ((GroupInviteActivity) activity).admins.remove(user.getUserId());
        }
        if (((GroupInviteActivity) activity).members.contains(user.getUserId())) {
            ((GroupInviteActivity) activity).members.remove(user.getUserId());
        }
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvLocation, tvAbout, tvSetAsAdmin, tvInviteMember;
        ImageView ivUserImage;
        CardView cvContainer;
        RelativeLayout rlSelectUser;
        LinearLayout llAdminTextContainer;


        UserViewHolder(View view) {
            super(view);
            tvUserName = view.findViewById(R.id.tvUserName);
            ivUserImage = view.findViewById(R.id.ivUserImage);
            tvLocation = view.findViewById(R.id.tvLocation);
            cvContainer = view.findViewById(R.id.cvContainer);
            tvAbout = view.findViewById(R.id.tvAbout);
            tvSetAsAdmin = view.findViewById(R.id.tvSetAsAdmin);
            rlSelectUser = view.findViewById(R.id.rlSelectUser);
            llAdminTextContainer = view.findViewById(R.id.llAdminTextContainer);
            tvInviteMember = view.findViewById(R.id.inviteAsMember);
        }
    }
}