package com.codeholic.kotumb.app.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Adapter.LinksRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.Link;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class LinksActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.tvLinks)
    TextView tvLinks;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.adView)
    ImageView adView;
    @BindView(R.id.rlAdView)
    RelativeLayout rlAdView;

    User user;
    int pagination = 0;
    List<Link> links;
    private LinearLayoutManager linearLayoutManager;
    LinksRecyclerViewAdapter linksRecyclerViewAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_links);
        ButterKnife.bind(this);
        init();
        initToolbar();
        setView();
        IntentFilter filterAdd = new IntentFilter();
        filterAdd.addAction(ResponseParameters.ADD_RECEIVED);
        registerReceiver(getAd, filterAdd);
    }

    private BroadcastReceiver getAd = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            try{
//                HomeScreenActivity.setUpAdImage(intent.getStringExtra(ResponseParameters.ADD_RECEIVED), rlAdView, adView, LinksActivity.this, true);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    private void setUpAdImage(String adResponse) {
        hideAdView();
        try {
            if (!adResponse.isEmpty()) {
                final JSONObject ad = new JSONObject(adResponse);
                if (ad.has(ResponseParameters.AD)) {
                    if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.IMAGE)) {
                        if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE).isEmpty()) {
                            rlAdView.setVisibility(VISIBLE);
                            new AQuery(this).id(adView).image(API.imageAdUrl + ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE), true, true, 300, R.drawable.ic_user);
                            adView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.URL)) {
                                            if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL).isEmpty()) {
                                                Utility.prepareCustomTab(LinksActivity.this, ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL));
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    private void hideAdView() {
        rlAdView.setVisibility(GONE);
    }

    private void init() {
        user = SharedPreferencesMethod.getUserInfo(this);
    }

    //init toolbar function
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        if (getIntent().hasExtra("FAVOURITES")) {
            header.setText(getResources().getString(R.string.home_favourite_title));
        } else {
            header.setText(getResources().getString(R.string.gov_link_title));
        }

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (getAd != null) {
            unregisterReceiver(getAd);
            getAd = null;
        }
        super.onDestroy();
    }

    //setview for getting user connection
    private void setView() {
        try {
            if (!Utility.isConnectingToInternet(this)) { // checking net connection
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(View.VISIBLE);
                tvLinks.setText(getResources().getString(R.string.app_no_internet_error));
                final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            tvLinks.setText(getResources().getString(R.string.app_no_link_found));
            if (getIntent().hasExtra("FAVOURITES")) {
                API.sendRequestToServerGET(LinksActivity.this, API.FAVOURITES_LINKS +  (pagination)+"//?userid="+user.getUserId(), API.FAVOURITES_LINKS);// service call for getting fav links
            } else {
                API.sendRequestToServerGET(LinksActivity.this, API.GOV_LINKS  +  (pagination)+"/?userid="+user.getUserId(), API.GOV_LINKS);// service call for getting GOV links
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //getResponse for getting list of links
    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        Log.e("response", "" + outPut);
        try {
            if (i == 0) { // for getting links
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.LINKS)) {
                            userList = outPut.getJSONArray(ResponseParameters.LINKS);
                        }
                        links = new ArrayList<>();
                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            Link link = new Link();
                            link.setLinkId(searchResults.getString(ResponseParameters.Id));
                            link.setLink(searchResults.getString(ResponseParameters.URL));
                            link.setLinkName(searchResults.getString(ResponseParameters.TITLE));
                            links.add(link);
                        }
                        //setting options for recycler adapter
                        linearLayoutManager = new LinearLayoutManager(this);
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserList.setLayoutManager(linearLayoutManager);
                        linksRecyclerViewAdapter = new LinksRecyclerViewAdapter(rvUserList, this, links);
                        rvUserList.setAdapter(linksRecyclerViewAdapter);
                        //load more setup for recycler adapter
                        linksRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                if (links.size() >= 6) {
                                    rvUserList.post(new Runnable() {
                                        public void run() {
                                            links.add(null);
                                            linksRecyclerViewAdapter.notifyItemInserted(links.size() - 1);
                                        }
                                    });
                                    if (getIntent().hasExtra("FAVOURITES")) {
                                        API.sendRequestToServerGET(LinksActivity.this, API.FAVOURITES_LINKS  + (pagination+6)+"//?userid="+user.getUserId(), API.FAVOURITES_LINKS_UPDATE);// service call for getting fav links
                                    } else {
                                        API.sendRequestToServerGET(LinksActivity.this, API.GOV_LINKS   + (pagination+6)+"/?userid="+user.getUserId(), API.GOV_LINKS_UPDATE);// service call for getting GOV links
                                    }
                                }
                            }
                        });
                    } else {
                        //no userfound
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } else if (i == 1) {// for updating list
                links.remove(links.size() - 1); // removing of loading item
                linksRecyclerViewAdapter.notifyItemRemoved(links.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response

                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.LINKS)) {
                            userList = outPut.getJSONArray(ResponseParameters.LINKS);
                        }

                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            Link link = new Link();
                            link.setLinkId(searchResults.getString(ResponseParameters.Id));
                            link.setLink(searchResults.getString(ResponseParameters.URL));
                            link.setLinkName(searchResults.getString(ResponseParameters.TITLE));
                            links.add(link);
                        }
                        if (userList.length() > 0) {
                            pagination = pagination + 6;
                            //updating recycler view
                            linksRecyclerViewAdapter.notifyDataSetChanged();
                            linksRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            }

        } catch (Exception e) {
            //mo user found
            llNotFound.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }

}
