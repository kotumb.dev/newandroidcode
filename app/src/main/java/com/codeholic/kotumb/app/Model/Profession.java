package com.codeholic.kotumb.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DELL on 11/17/2017.
 */

public class Profession implements Parcelable {
    public String id = "";
    public String professionName = "";
    public String companyName = "";
    public String yearFrom = "";
    public String yearTo = "";
    public String description = "";
    public String isCurrent = "0";
    public String sector = "";

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getIsCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(String isCurrent) {
        this.isCurrent = isCurrent;
    }

    public static Creator<Profession> getCREATOR() {
        return CREATOR;
    }

    public String getYearTo() {
        return yearTo;
    }

    public void setYearTo(String yearTo) {
        this.yearTo = yearTo;
    }

    protected Profession(Parcel in) {
        id = in.readString();
        professionName = in.readString();
        companyName = in.readString();
        yearFrom = in.readString();
        yearTo = in.readString();
        description = in.readString();
        isCurrent = in.readString();
        sector = in.readString();
    }

    public Profession(){

    }

    public static final Creator<Profession> CREATOR = new Creator<Profession>() {
        @Override
        public Profession createFromParcel(Parcel in) {
            return new Profession(in);
        }

        @Override
        public Profession[] newArray(int size) {
            return new Profession[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfessionName() {
        return professionName;
    }

    public void setProfessionName(String professionName) {
        this.professionName = professionName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(String yearFrom) {
        this.yearFrom = yearFrom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable
     * instance's marshaled representation. For example, if the object will
     * include a file descriptor in the output of {@link #writeToParcel(Parcel, int)},
     * the return value of this method must include the
     * {@link #CONTENTS_FILE_DESCRIPTOR} bit.
     *
     * @return a bitmask indicating the set of special object types marshaled
     * by this Parcelable object instance.
     * @see #CONTENTS_FILE_DESCRIPTOR
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(professionName);
        dest.writeString(companyName);
        dest.writeString(yearFrom);
        dest.writeString(yearTo);
        dest.writeString(description);
        dest.writeString(isCurrent);
        dest.writeString(sector);
    }
}
