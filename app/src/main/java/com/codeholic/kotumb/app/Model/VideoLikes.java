package com.codeholic.kotumb.app.Model;


import android.os.Parcel;
import android.os.Parcelable;

public class VideoLikes implements Parcelable {


    String like_id="";
    String video_id="";
    String video_avatar="";
    String video_first_name="";
    String video_last_name="";
    String like_date="";
    String user_id="";


    public VideoLikes(){

    }

    public VideoLikes(Parcel in) {
        like_id = in.readString();
        video_id = in.readString();
        user_id=in.readString();
        video_avatar = in.readString();
        video_first_name = in.readString();
        video_last_name = in.readString();
        like_date = in.readString();
    }

    public static final Creator<VideoLikes> CREATOR = new Creator<VideoLikes>() {
        @Override
        public VideoLikes createFromParcel(Parcel in) {
            return new VideoLikes(in);
        }

        @Override
        public VideoLikes[] newArray(int size) {
            return new VideoLikes[size];
        }
    };

    public void setLike_date(String like_date) {
        this.like_date = like_date;
    }

    public void setLike_id(String like_id) {
        this.like_id = like_id;
    }

    public void setVideo_avatar(String video_avatar) {
        this.video_avatar = video_avatar;
    }

    public void setVideo_first_name(String video_first_name) {
        this.video_first_name = video_first_name;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public void setVideo_last_name(String video_last_name) {
        this.video_last_name = video_last_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLike_date() {
        return like_date;
    }

    public String getLike_id() {
        return like_id;
    }

    public String getVideo_avatar() {
        return video_avatar;
    }

    public String getVideo_first_name() {
        return video_first_name;
    }

    public String getVideo_id() {
        return video_id;
    }

    public String getVideo_last_name() {
        return video_last_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(like_id);
        dest.writeString(video_id);
        dest.writeString(user_id);
        dest.writeString(video_avatar);
        dest.writeString(video_first_name);
        dest.writeString(video_last_name);
        dest.writeString(like_date);
    }
}
