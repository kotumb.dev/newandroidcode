package com.codeholic.kotumb.app.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.codeholic.kotumb.app.Model.Invitations;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;

import androidx.fragment.app.Fragment;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Adapter.NotificationRecyclerViewAdapter;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Services.GetAddService;
import com.codeholic.kotumb.app.Services.GetPreLoadDataService;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.EventNames;
import com.codeholic.kotumb.app.Utility.FragmentHistory;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.View.BadgeDrawable;
import com.codeholic.kotumb.app.View.FragNavController;
import com.codeholic.kotumb.app.View.OtpEditText;
import com.codeholic.kotumb.app.fragments.BaseFragment;
import com.codeholic.kotumb.app.fragments.ConnectionFragment;
import com.codeholic.kotumb.app.fragments.HomeFragment;
import com.codeholic.kotumb.app.fragments.InviteFragment;
import com.codeholic.kotumb.app.fragments.MeFragment;
import com.codeholic.kotumb.app.fragments.NotificationFragment;
import com.codeholic.kotumb.app.vidphon;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.firebase.iid.FirebaseInstanceId;
import com.unomer.sdk.Unomer;
import com.unomer.sdk.UnomerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.os.FileObserver.DELETE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.androidquery.util.AQUtility.getContext;
import static com.crashlytics.android.Crashlytics.log;

public class HomeScreenActivity extends AppCompatActivity implements BaseFragment.FragmentNavigation, FragNavController.TransactionListener, FragNavController.RootFragmentListener, NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener,MeFragment.PercentageListener {

    private Boolean is75ProfileComplete;

    public static final String PUB_ID = "9OyNihZH3F2xauIia9gaYw==";
    public static final String APP_ID = "8ZqrxBtg04OlMKmLT73HWbukXYEpPn";
    private static final int MY_REQUEST_CODE = 12345;
    public static String TAG = "Home";
    public boolean openPublicGroup;
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.adView)
    ImageView adView;
    @BindView(R.id.rlAdView)
    RelativeLayout rlAdView;
    String deleteReason;
    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;
    @BindView(R.id.header)
    TextView header;
    LayerDrawable messageCount;
    AlertDialog alertDialog;
    com.codeholic.kotumb.app.View.ProgressBar progressBar;
    String[] TABS = new String[5];
    boolean SHOW_POPUP_PRIMARY_NUMBER = false;
    boolean SHOW_POPUP_PROFESSION = false;
    boolean PROFILE_SUMMERY = true;
    boolean PROFILE_SUMMERY2 = true;
    boolean SURVEY = true;
    Unomer survey;
    UnomerListener uListener;
    @BindView(R.id.bottom_tab_layout)
    TabLayout bottomTabLayout;
    AlertDialog mobileAlertDialog;
    AlertDialog otpAlertDialog;
    HashMap<String, Object> changeMobile;
    String otp = "";
    int num=0;

    private FragNavController mNavController;

    private FragmentHistory fragmentHistory;

    private String languageToLoad = "";
    Locale locale;
    Configuration config = new Configuration();

    HomeFragment homeFragment = null;
    Handler handler = new Handler();
    boolean isRunning = true;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {

        }
    };
    //    FirebaseDatabase firebaseDatabase;
//    DatabaseReference databaseReference,childReference;
//    FirebaseStorage storage;
//    StorageReference storageRef;
//    StorageReference invitationRef,pendingRef;
    Map<String, Invitations> invitationsMap = new HashMap<>();
    private ActionBarDrawerToggle toggle;
    //icons for bottom tabs
    private int[] mTabIconsSelected = {
            R.drawable.ic_footer_home,
            R.drawable.ic_footer_notification,
            R.drawable.ic_footer_me,
            R.drawable.ic_footer_invite,
            R.drawable.ic_footer_contact};

    private NotificationFragment notificationFragment;
    private long lastBubbleDisplayTime = 0;
    private int prevNotiCount = -1;
    private int prevActId = -1;
    private SimpleDateFormat serverDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private Date prevlastNotiTime = Calendar.getInstance().getTime();
    private int run = -1;
    private boolean start = true;
    private BroadcastReceiver getAd = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action

            try {
                String adResponse = intent.getStringExtra(ResponseParameters.ADD_RECEIVED);
                //setUpAdImage(adResponse, HomeScreenActivity.this.rlAdView, HomeScreenActivity.this.adView, HomeScreenActivity.this, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @SuppressLint("NewApi")
    public static void setUpAdImage(final String adResponse, NotificationRecyclerViewAdapter.AdsViewHolder holder, final Activity activity) {
        hideAdView(holder.rlAdView);
        try {
            if (!adResponse.isEmpty()) {
                final JSONObject ad = new JSONObject(adResponse);
                System.out.println("AD Response     " + ad.toString());
                if (ad.has(ResponseParameters.AD)) {
                    if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.IMAGE)) {
                        if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE).isEmpty()) {
                            holder.rlAdView.setVisibility(VISIBLE);
                            String adUrl = "";
                            if (ad.has(ResponseParameters.AD_IMG_URL)) {
                                adUrl = ad.getString(ResponseParameters.AD_IMG_URL);
                            }
                            String url = adUrl + ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE);
                            // new AQuery(activity).id(adView).image(url, true, true, 300, R.drawable.ads_1);
                            holder.adView.setImageResource(R.drawable.ads_1);

                            TextView header = holder.itemView.findViewById(R.id.header);
                            TextView desc = holder.itemView.findViewById(R.id.desc);

                            JSONObject adJSONObject = ad.getJSONObject("ad");
                            String title = adJSONObject.getString("title");
                            String description = adJSONObject.getString("description");
                            header.setText(title);
                            desc.setText(description);

                            holder.itemView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.URL)) {
                                            if (true/*!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL).isEmpty()*/) {
                                                Utility.prepareCustomTab(activity, ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL));
                                                Bundle bundle = new Bundle();
                                                bundle.putString("UserId", SharedPreferencesMethod.getUserId(getContext()));
                                                bundle.putString("type", "Actual Ad");
                                                Utils.logEvent(EventNames.AD_CLICK, bundle, getContext());
                                                Intent intent = new Intent(activity, AdViewActivity.class);
                                                intent.putExtra("ad", adResponse);
                                                activity.startActivity(intent);
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void hideAdView(RelativeLayout rlAdView) {
        rlAdView.setVisibility(GONE);
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {

        try {

            BadgeDrawable badge;

            // Reuse drawable if possible
            Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
            if (reuse != null && reuse instanceof BadgeDrawable) {
                badge = (BadgeDrawable) reuse;
            } else {
                badge = new BadgeDrawable(context);
            }

        /*if(prevCount.isEmpty()){
            badge.setVisible(false, true);
        } else {
            badge.setVisible(true, true);
        }*/
            badge.setCount(count);
            icon.mutate();
            icon.setDrawableByLayerId(R.id.ic_badge, badge);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    //checking for number is valid or not
    public static Boolean isValidLong(String value) {
        try {
            if (value.length() <= 0)
                return false;
            if (value.startsWith("0"))
                return false;
            Long val = Long.valueOf(value);
            return val != null;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static void createDynamicLink(Activity activity, String userId, String referralCode) {
//        https://kotumb.page.link/
        String myLink = "https://kotumb.page.link/?link=https://www.kotumb.in/?userID="+SharedPreferencesMethod.getUserId(activity)+"-"+Utils.referralCode +"&apn="+activity.getPackageName()+"&st="+activity.getString(R.string.check_out)+
                "&sd="+activity.getString(R.string.check_out_text2)+"\n"+ "\nMy Referral Code: " + Utils.referralCode+
                "&si="+"https://s3.ap-south-1.amazonaws.com/kotumb-store-1/assets/images/logo_300.png";

        Bundle bundle = new Bundle();
        Utils.logEvent(EventNames.APP_INVITE_PEOPLE, bundle, activity);

//        hey check out KOTUMB professional networking app .....  Sign up using my referral code we can chat with professional there and earn referral points

        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(myLink))
                .buildShortDynamicLink()
                .addOnCompleteListener(activity, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {

                            Uri shortLink = Objects.requireNonNull(task.getResult()).getShortLink();

                            Log.e("main ", "short link "+ shortLink.toString());

                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra(Intent.EXTRA_TEXT, shortLink.toString());
                            sendIntent.setType("text/plain");
                            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Install kotumb app using my refer link, you can get exciting rewards");
                            activity.startActivity(sendIntent);

                            Bundle bundle = new Bundle();
                            Utils.logEvent(EventNames.APP_INVITE_PEOPLE, bundle, activity);

                        } else {
                            // Error
                            Toast.makeText(activity,"Exception : " + task.getException().getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }
                });


        /*DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(myLink))
                .setDomainUriPrefix("https://kotumbapp.page.link/mVYD")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder(activity.getPackageName()).build())
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.example.ios").build())
                .buildDynamicLink();

        Uri dynamicLinkUri1 = dynamicLink.getUri();
        Log.e("INVITE","Uri is : " + dynamicLinkUri1.toString());
        String message = activity.getResources().getString(R.string.check_out);
        message = message  + dynamicLinkUri1.toString() + " " + "UID = " + SharedPreferencesMethod.getUserId(activity);
        message = message + "\n\nSign up using my referral code and we both earn \"Kotumb Reward Points\"" + "\n\nMy Referral Code: " + Utils.referralCode;

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.setType("text/plain");
        activity.startActivity(sendIntent);

        Bundle bundle = new Bundle();
        Utils.logEvent(EventNames.APP_INVITE_PEOPLE, bundle, activity);*/


        //test dynamic url


//        String permLink = "https://www.kotumb.in/" + "?route=profile&referralcode=" + Utils.referralCode
//                + "&userid=" + SharedPreferencesMethod.getUserId(activity)
//                + "&picture=" + "https://s3.ap-south-1.amazonaws.com/kotumb-store-1/assets/images/logo_300.png";
//
//        FirebaseDynamicLinks.getInstance().createDynamicLink()
//                .setLink(Uri.parse(permLink))
//                .setDomainUriPrefix("kotumbapp.page.link")
//                .setAndroidParameters(new
//                        DynamicLink.AndroidParameters.Builder().build())
//                .setSocialMetaTagParameters(
//                        new DynamicLink.SocialMetaTagParameters.Builder()
//                                .setTitle("My Refer link")
//                                .setDescription("Kotumb is the professional app to use this you can connect with knowledgeable peoples")
//                                .setImageUrl(Uri.parse("https://s3.ap-south-1.amazonaws.com/kotumb-store-1/assets/images/logo_300.png"))
//                                .build())
//                .buildShortDynamicLink()
//                .addOnCompleteListener(activity, task -> {
//                    if (task.isSuccessful()) {
//                        Intent intent = new Intent();
//                        intent.setAction(Intent.ACTION_SEND);
//                        intent.putExtra(Intent.EXTRA_TEXT,task.getResult().getShortLink());
//                        intent.setType("text/plain");
//                        activity.startActivity(intent);
//                    } else {
//                        Toast.makeText(activity,"Failed to Generate Profile Link, Try Again " + task.getException().getMessage(),Toast.LENGTH_LONG).show();
//                    }
//                });




//        String permLink = "https://www.kotumb.in/" + "?route=profile&userId=" + SharedPreferencesMethod.getUserId(activity)
//                + "&referralcode=" + Utils.referralCode
//                + "&picture=" + "https://s3.ap-south-1.amazonaws.com/kotumb-store-1/assets/images/logo_300.png";

//        FirebaseDynamicLinks.getInstance().createDynamicLink()
//                .setLink(Uri.parse(permLink))
//                .setDomainUriPrefix("https://kotumbapp.page.link/")
//                .setAndroidParameters(new
//                        DynamicLink.AndroidParameters.Builder().build())
//                .setSocialMetaTagParameters(
//                        new DynamicLink.SocialMetaTagParameters.Builder()
//                                .setTitle("Referral link")
//                                .setDescription("Reward points 4")
//                                .build())
//                .buildShortDynamicLink()
//                .addOnCompleteListener(activity, task -> {
//                    if (task.isSuccessful()) {
//                        String message = activity.getString(R.string.check_out);
//                        message = message + task.getResult().getShortLink().toString();
//                        message = message + "\n\nSign up using my referral code and we both earn \"Kotumb Reward Points\"" + "\n\nMy Referral Code: " + Utils.referralCode;
//
//                        Log.e("HOME", "Url is : " + task.getResult().getShortLink().toString());
//                        Intent intent = new Intent();
//                        intent.setAction(Intent.ACTION_SEND);
//                        intent.putExtra(Intent.EXTRA_TEXT, message);
//                        intent.setType("text/plain");
//                        activity.startActivity(intent);
//                    } else {
//                        Log.e("HOME", "Fail to create a dynamic link" + task.getException().getMessage());
//                    }
//                });


    }

    public static void share(Activity activity) {
        final String appPackageName = activity.getPackageName();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String message = activity.getString(R.string.check_out);
        message = message + " https://play.google.com/store/apps/details?id=" + appPackageName;
        message = message + activity.getString(R.string.check_out_text2) + "\n\nMy Referral Code: " + Utils.referralCode;
        Log.e("HOME", "REFCODE iss : " + message);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.setType("text/plain");
        activity.startActivity(sendIntent);

        Bundle bundle = new Bundle();
        Utils.logEvent(EventNames.APP_INVITE_PEOPLE, bundle, activity);
    }

    void updateToken() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("userid", SharedPreferencesMethod.getUserId(this));
        hashMap.put("token", FirebaseInstanceId.getInstance().getToken());
        System.out.println("paramsInput==" + hashMap);
        String url = API.base_api + "services/update_remove_device_token/";
        AQuery aq = new AQuery(this);
        //Log.d("Url ", url);
        aq.ajax(url, hashMap, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                System.out.println("Response==" + json);
            }
        });
    }

//    public static void setUpAdImage(String adResponse, RelativeLayout rlAdView, ImageView adView, final Activity activity, boolean disable) {
//        hideAdView(rlAdView);
//        if (disable) { // to disable ad
//            return;
//        }
//        try {
//            if (!adResponse.isEmpty()) {
//                final JSONObject ad = new JSONObject(adResponse);
//                if (ad.has(ResponseParameters.AD)) {
//                    if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.IMAGE)) {
//                        if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE).isEmpty()) {
//                            rlAdView.setVisibility(VISIBLE);
//                            String adUrl = "";
//                            if (ad.has(ResponseParameters.AD_IMG_URL)) {
//                                adUrl = ad.getString(ResponseParameters.AD_IMG_URL);
//                            }
//                            String url = adUrl + ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE);
//                            new AQuery(activity).id(adView).image(url, true, true, 300, R.drawable.default_ad);
//                            adView.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    try {
//                                        if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.URL)) {
//                                            if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL).isEmpty()) {
//                                                Utility.prepareCustomTab(activity, ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL));
//                                                Bundle bundle = new Bundle();
//                                                bundle.putString("UserId", SharedPreferencesMethod.getUserId(getContext()));
//                                                bundle.putString("type", "Actual Ad");
//                                                Utils.logEvent(EventNames.AD_CLICK, bundle, getContext());
//                                            }
//                                        }
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            });
//                        }
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
//        }
//    }

    //on create method for setting layout
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        languageToLoad = SharedPreferencesMethod.getDefaultLanguage(this);
        changeLocale(languageToLoad);

        registerTimeStamp();

        initToolbar();
        initTab();
        updateToken();
        fragmentHistory = new FragmentHistory();
        mNavController = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.content_frame)
                .transactionListener(this)
                .rootFragmentListener(this, TABS.length)
                .build();
        switchTab(0); // for setting on first tab

        bottomTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() { // tab selector listener
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.e("position", "" + tab.getPosition());
                fragmentHistory.push(tab.getPosition());
                switchTab(tab.getPosition()); // switching tab to selected position
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.e("position re", "" + tab.getPosition());

                mNavController.clearStack();
                switchTab(tab.getPosition());
            }
        });
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (isRunning) {
                    if (!SharedPreferencesMethod.getUserId(HomeScreenActivity.this).trim().isEmpty()){
                        serViceCall();
                    }
                }
            }
        };

        serViceCall();
        try {
            GetPreLoadDataService.startActionGetNameOrg(this);
            GetPreLoadDataService.startActionGetSchools(this);
            GetPreLoadDataService.startActionGetCourses(this);
            GetPreLoadDataService.startActionGetRoles(this);
            GetPreLoadDataService.startActionGetSkills(this);
            GetPreLoadDataService.startActionGetIndustries(this);
            GetPreLoadDataService.startActionGetLanguage(this);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Crash Report Home   " + ex.toString());
        }


        startService(new Intent(this, GetAddService.class));
        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.ADD_RECEIVED);
        registerReceiver(getAd, filter);


        Intent intent = getIntent();
        if (intent.hasExtra(ResponseParameters.OPEN_FRAGMENT)) {
            if (intent.getStringExtra(ResponseParameters.OPEN_FRAGMENT).equalsIgnoreCase(ResponseParameters.NOTIFICATIONS)) {
                openNotificationFragment();
                return;
            }
        }

        if (intent.hasExtra(ResponseParameters.OPEN_GROUP)) {
            if (intent.getStringExtra(ResponseParameters.OPEN_GROUP).equalsIgnoreCase(ResponseParameters.NOTIFICATIONS)) {
                openHomeFragment();
                return;
            }
        }
        String userid = SharedPreferencesMethod.getUserId(this);
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("KTOKEN", token + "");

        run = SharedPreferencesMethod.getInt(this, "run");
        SharedPreferencesMethod.setInt(this, "run", run + 1);

        uListener = getuListener();
        survey = new Unomer(getApplicationContext(), PUB_ID, APP_ID, uListener);
        fetchSurvey();


//        firebaseDatabase = FirebaseDatabase.getInstance();
//        databaseReference = FirebaseDatabase.getInstance().getReference();
//        childReference = databaseReference.child("invite");
//
//        homeFragment = new HomeFragment();
//        registerTimeStamp();
//
//        initToolbar();
//        initTab();
//        updateToken();
//
//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( HomeScreenActivity.this,  new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                String newToken = instanceIdResult.getToken();
//                if (!newToken.isEmpty()){
//                    sendRegistrationToServer(newToken);
//                }
//
//            }
//        });
//
//
//        fragmentHistory = new FragmentHistory();
//        mNavController = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.content_frame)
//                .transactionListener(this)
//                .rootFragmentListener(this, TABS.length)
//                .build();
//        switchTab(0); // for setting on first tab
//        bottomTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() { // tab selector listener
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                Log.e("HOME", "position is" + tab.getPosition());
//                fragmentHistory.push(tab.getPosition());
//                switchTab(tab.getPosition()); // switching tab to selected position
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//                Log.e("position re", "" + tab.getPosition());
//
//                mNavController.clearStack();
//                switchTab(tab.getPosition());
//            }
//        });
//        handler = new Handler();
//        runnable = new Runnable() {
//            @Override
//            public void run() {
//                if (isRunning) {
//                    if (!SharedPreferencesMethod.getUserId(HomeScreenActivity.this).trim().isEmpty())
//                        serViceCall();
//                }
//            }
//        };
//
//        serViceCall();
//
//        //Calling every time app opens,
//        // as android pie and other versions of android
//        // were facing issues for updating values
//        try{
//            GetPreLoadDataService.startActionGetNameOrg(this);
//            GetPreLoadDataService.startActionGetSchools(this);
//            GetPreLoadDataService.startActionGetCourses(this);
//            GetPreLoadDataService.startActionGetRoles(this);
//            GetPreLoadDataService.startActionGetSkills(this);
//            GetPreLoadDataService.startActionGetIndustries(this);
//            GetPreLoadDataService.startActionGetLanguage(this);
//        }catch (Exception ex){
//            ex.printStackTrace();
//            System.out.println("Crash Report Home   "+ex.toString());
//        }
//
//
//        startService(new Intent(this, GetAddService.class));
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(ResponseParameters.ADD_RECEIVED);
//        registerReceiver(getAd, filter);
//
//
//        Intent intent = getIntent();
//        if (intent.hasExtra(ResponseParameters.OPEN_FRAGMENT)) {
//            if (intent.getStringExtra(ResponseParameters.OPEN_FRAGMENT).equalsIgnoreCase(ResponseParameters.NOTIFICATIONS)) {
//                openNotificationFragment();
//                return;
//            }
//        }
//
//        if (intent.hasExtra(ResponseParameters.OPEN_GROUP)) {
//            if (intent.getStringExtra(ResponseParameters.OPEN_GROUP).equalsIgnoreCase(ResponseParameters.NOTIFICATIONS)) {
//                openHomeFragment();
//                return;
//            }
//        }
////        openNotificationFragment();
//
//      /*  try {
//            String adResponse = "{\"success\":\"success\",\"ad\":{\"id\":\"37\",\"title\":\"Buy 1 Get 1 Free!\",\"image\":\"resized_advertise3.jpg\",\"zipcode\":\"482002\",\"url\":\"\",\"type\":\"1\",\"createdAT\":\"2018-02-05 15:44:41\",\"fromDateTime\":\"2018-02-05 01:00:00\",\"toDateTime\":\"2018-02-12 01:00:00\",\"status\":\"1\"}}";
//            setUpAdImage(adResponse, rlAdView, adView, this);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }*/
//
////         startActivity(new Intent(this, MyProfileActivity.class));
//        String userid = SharedPreferencesMethod.getUserId(this);
////        Toast.makeText(this, userid + "", Toast.LENGTH_SHORT).show();
//        String token = FirebaseInstanceId.getInstance().getToken();
//        Log.d("KTOKEN", token + "");
//
////        startActivity(new Intent(this,OrganizationActivity.class));
//
//        run = SharedPreferencesMethod.getInt(this, "run");
//        SharedPreferencesMethod.setInt(this, "run", run + 1);
//
////        showUnomerDialog();
//
//        //for testing purpose, remove if not needed
//       /* intent = new Intent(this, MyProfileActivity.class);
//        intent.putExtra(ResponseParameters.Userdata,  SharedPreferencesMethod.getUserInfo(this));
//        startActivity(intent);*/
//
//      /* new Handler().postDelayed(new Runnable() {
//           @Override
//           public void run() {
//               showUnomerDialog();
//           }
//       }, 10000);*/
////      checkSummryValidation();
//        uListener = getuListener();
//        survey = new Unomer(getApplicationContext(), PUB_ID, APP_ID, uListener);
//        fetchSurvey();

    }

    public void fetchSurvey() {
        String eParam = "";
        System.out.println("Fetch Survry  " + "  survey get  ");
        survey.fetchSurvey(getApplicationContext(), uListener, eParam);
        survey.showSurvey(this, uListener);
    }

    private void registerTimeStamp() {
        Utils.appStartTime = Calendar.getInstance();
    }

    //service call for home response
    private void serViceCall() {
        if (!Utility.isConnectingToInternet(this)) { //net connection check
            return;
        }
        API.sendRequestToServerGET(this, API.HOME + SharedPreferencesMethod.getUserId(this), API.HOME);// for getting prevCount

    }

    //initializing toolbar and setting header
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = Objects.requireNonNull(getSupportActionBar(), "Support actionbar cannot be null");
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        mToolbar.setNavigationIcon(R.drawable.ic_dehaze_black);
        toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    }

    //initializing bottom tabs
    private void initTab() {
        TABS[0] = getResources().getString(R.string.header_main_menu_option1);
        TABS[1] = getResources().getString(R.string.home_notification_title);
        TABS[2] = getResources().getString(R.string.header_setting_menu_text);
        TABS[3] = getResources().getString(R.string.header_main_menu_option3);
        TABS[4] = getResources().getString(R.string.connections_title);
        if (bottomTabLayout != null) {
            for (int i = 0; i < TABS.length; i++) {
                bottomTabLayout.addTab(bottomTabLayout.newTab());
                TabLayout.Tab tab = bottomTabLayout.getTabAt(i);
                if (tab != null) {
                    tab.setCustomView(getTabView(i));
                    bottomTabLayout.getTabAt(0).select();
                }
            }
        }
    }

    //get view for tab
    private View getTabView(int position) {
        View view = LayoutInflater.from(this).inflate(R.layout.tab_item_bottom, null);
        ImageView icon = view.findViewById(R.id.tab_icon);
        TextView textView;
        textView = view.findViewById(R.id.text);
        if (textView != null) {
            textView.setText(TABS[position]);
        } else {
            System.out.println("Null textView ==========");
            textView = view.findViewById(R.id.text);
            textView.setText(TABS[position]);
        }
        icon.setImageDrawable(Utils.setDrawableSelector(this, mTabIconsSelected[position], mTabIconsSelected[position]));
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        //  dynamicLinkCall();

    }

    @Override
    public void onStop() {
        super.onStop();
        SharedPreferencesMethod.setBoolean(this, "HomeScreenActivity", false);
        SharedPreferences settings = getSharedPreferences("YOUR_PREF_NAME", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("SNOW_DENSITY", 11);
        editor.commit();
    }

    //switch tab
    private void switchTab(int position) {
        changeLocale(languageToLoad);
        mNavController.switchTab(position);
//        updateToolbarTitle(position);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!SharedPreferencesMethod.getBoolean(this, "HomeScreenActivity")) {
            SharedPreferencesMethod.setBoolean(this, "HomeScreenActivity", true);
        }
        if (start) {
            start = false;
            if (run >= 0 && run < 2) {
                openInviteFragment();
            } else if (run < 4) {
                openPublicGroup = true;
                if (homeFragment != null)
                    homeFragment.openGroup();
            }
        }


       /* appUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(
                        appUpdateInfo -> {
                            if (appUpdateInfo.updateAvailability()
                                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                                // If an in-app update is already running, resume the update.
                                try {
                                    appUpdateManager.startUpdateFlowForResult(
                                            appUpdateInfo,
                                            IMMEDIATE,
                                            this,
                                            MY_REQUEST_CODE);
                                } catch (IntentSender.SendIntentException e) {
                                    e.printStackTrace();
                                }
                            }
                        });*/

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);

        MenuItem itemCart = menu.findItem(R.id.message);
        messageCount = (LayerDrawable) itemCart.getIcon();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.message:
                startActivity(new Intent(this, MessageActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //on back press event
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) { // check if drawer is open
            drawer.closeDrawer(GravityCompat.START); // close drawer
        } else { // go back to previous fragment
            if (!mNavController.isRootFragment()) {
                mNavController.popFragment();
            } else {
                if (fragmentHistory.isEmpty()) {
                    handler.removeCallbacks(runnable);
                    isRunning = false;
                    handler = null;
                    runnable = null;
                    Constants.showPopUpOnHome = true;
                    Constants.showPopUpOnHomeProfile = true;
                    Constants.showPopUpVerCheck = true;
                    Utils.tookSurvey = false;
                    super.onBackPressed();
                } else {
                    if (fragmentHistory.getStackSize() > 1) {
                        int position = fragmentHistory.popPrevious();
                        switchTab(position);
                        updateTabSelection(position);
                        fragmentHistory.pop();
                    } else {
                        switchTab(0);
                        updateTabSelection(0);
                        fragmentHistory.emptyStack();
                    }
                }

            }
        }
    }

    //updating tab selection
    private void updateTabSelection(int currentTab) {
        for (int i = 0; i < TABS.length; i++) {
            TabLayout.Tab selectedTab = bottomTabLayout.getTabAt(i);
            if (currentTab != i) {
                selectedTab.getCustomView().setSelected(false);
            } else {
                selectedTab.getCustomView().setSelected(true);
                selectedTab.select();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mNavController != null) {
            mNavController.onSaveInstanceState(outState);
        }
    }

    @Override
    public void pushFragment(Fragment fragment) {
        if (mNavController != null) {
            mNavController.pushFragment(fragment);
        }
    }

    @Override
    public void onTabTransaction(Fragment fragment, int index) {
        // If we have a backstack, displayBubble the back button
        if (getSupportActionBar() != null && mNavController != null) {
            updateToolbar();
        }
    }

    private void updateToolbar() {
        /*getSupportActionBar().setDisplayHomeAsUpEnabled(!mNavController.isRootFragment());
        getSupportActionBar().setDisplayShowHomeEnabled(!mNavController.isRootFragment());
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);*/
    }

    @Override
    public void onFragmentTransaction(Fragment fragment, FragNavController.TransactionType transactionType) {
        //do fragmentty stuff. Maybe change title, I'm not going to tell you how to live your life
        // If we have a backstack, displayBubble the back button
        if (getSupportActionBar() != null && mNavController != null) {
            updateToolbar();
        }
    }

    //getting fragment
    @Override
    public Fragment getRootFragment(int index) {
        switch (index) {

            case FragNavController.TAB1:
                homeFragment = new HomeFragment();
                return homeFragment;
            case FragNavController.TAB2:
                notificationFragment = new NotificationFragment();
                return notificationFragment;
            case FragNavController.TAB3:
                return new MeFragment();
            case FragNavController.TAB4:
                return new InviteFragment();
            case FragNavController.TAB5:
                return new ConnectionFragment();


        }
        throw new IllegalStateException("Need to send an index that we know");
    }

    //open me fragment directly
    public void openMeFragment() {
        /*fragmentHistory.push(2);
        switchTab(2);*/
        updateTabSelection(2);
        TabLayout.Tab tab = bottomTabLayout.getTabAt(2);
        tab.select();

        //updateTabSelection(2);
    }

//    private void updateToolbarTitle(int position){
//
//
//        getSupportActionBar().setTitle(TABS[position]);
//
//    }

    //open connections fragment directly
    public void openConnectionsFragment() {
        /*fragmentHistory.push(4);
        switchTab(4);*/
        updateTabSelection(4);
        TabLayout.Tab tab = bottomTabLayout.getTabAt(4);
        tab.select();
    }

    //open Invite fragment directly
    public void openInviteFragment() {

        updateTabSelection(3);
        TabLayout.Tab tab = bottomTabLayout.getTabAt(3);
        tab.select();

    }

    //open Notification fragment directly
    public void openNotificationFragment() {

       /* fragmentHistory.push(1);
        switchTab(1);*/
        updateTabSelection(1);
        TabLayout.Tab tab = bottomTabLayout.getTabAt(1);
        tab.select();
    }

    //open Home fragment directly
    public void openHomeFragment() {
        updateTabSelection(0);
        TabLayout.Tab tab = bottomTabLayout.getTabAt(0);
        tab.select();
        if (homeFragment != null)
            homeFragment.performGroupClick();
    }

    //update toolbar title
    public void updateToolbarTitle(String title) {
        header.setText(title);
    }

    /**
     * Called when an item in the navigation menu is selected.
     *
     * @param item The selected item
     * @return true to display the item as the selected item
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home: // home click of navigation menu
                /*switchTab(0);*/
                updateTabSelection(0);
                TabLayout.Tab tab = bottomTabLayout.getTabAt(0);
                tab.select();
                fragmentHistory.emptyStack();
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.navigation_logout: // logout
                AlertDialog.Builder errorDialog = new AlertDialog.Builder(this) // dialog for confirming logout
                        .setTitle(getResources().getString(R.string.app_alert_text))
                        .setMessage(getResources().getString(R.string.logout_message))
                        .setPositiveButton(getResources().getString(R.string.cancel_btn_text), null)
                        .setNeutralButton(getResources().getString(R.string.header_setting_logout_text), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                onClickLogout();
                            }
                        });

                alertDialog = errorDialog.show();
                break;
            case R.id.navigation_share:
                // shareApp();
//                  share(this);
                createDynamicLink(HomeScreenActivity.this, SharedPreferencesMethod.getUserId(this), Utils.referralCode);
                break;
            case R.id.referral_code:
                copyReferalCode();
                break;
            case R.id.navigation_change_pass:
                startActivity(new Intent(this, UpdatePasswordActivity.class));
                break;
            case R.id.navigation_delete:
                Utils.showPopup(this, getString(R.string.delete_acc_msg_dialog), getString(R.string.primary_number_yes_btn_text), getString(R.string.primary_number_no_btn_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDeleteAccountReasonPopup();
                        User user = SharedPreferencesMethod.getUserInfo(HomeScreenActivity.this);
//                        Toast.makeText(HomeScreenActivity.this, user.getUserId(), Toast.LENGTH_SHORT).show();
                    }
                }, null);
                break;
            case R.id.navigation_change_lang:
//                Utility.ChangeLanguagePopup(this);
                Utils.getLang(HomeScreenActivity.this);

                break;

            case R.id.navigation_contact:
                //startActivity(new Intent(this, ContactFeedbackActivity.class));
                String userId = SharedPreferencesMethod.getUserId(this);
                Log.e("TAG", "" + API.contactUsLink + userId);
                //startActivity(new Intent(this, ContactActivity.class));
                //Utility.prepareCustomTab(this, API.contactUsLink + SharedPreferencesMethod.getUserId(this));


                try {
                    String url = API.contactUsLink + Integer.parseInt(userId);
                    Utils.openCustomTab(url, this);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                break;

        /*    case R.id.invite_user:
                Intent intent12=new Intent(HomeScreenActivity.this,InviteUser.class);
                startActivity(intent12);
                break;*/

            case R.id.vidphon_live: {
                startActivity(new Intent(HomeScreenActivity.this, vidphon.class));
                break;
            }
        }
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if (resultCode != RESULT_OK) {
            return;
        }
        drawer.closeDrawers();
//        if (requestCode == DELETE) {
////            showDeleteAccountPopup();
//        }

        if (resultCode == RESULT_OK) {
            if (requestCode == DELETE && data != null) {
                String deleteReason = data.getStringExtra("reason");
                showDeleteAccountPopup(deleteReason);
            }
        }
    }

    private void showDeleteAccountPopup(final String reason) {
        String s = getResources().getString(R.string.delete_account_text);
//        String s = "Are you sure";
        String delete_account = getResources().getString(R.string.delete_btn);
        String cancel = getResources().getString(R.string.cancel_btn_text);
        Utils.showPopup(this, s, delete_account, cancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("DELETE", "YES");

                onClickDeleteAccount(reason);
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @NonNull
    private UnomerListener getuListener() {
        return new UnomerListener() {
            @Override
            public void unomerSurveyFetchStarted() {
            }

            @Override
            public void unomerSurveyFetchSuccess(boolean b, int i, String s, int i1, String s1) {
                showSurveyPopup(this);

            }

            @Override
            public void unomerSurveyFetchFailed(String eMsg, String s1) {
                survey.destroy();
            }

            @Override
            public void unomerSurveyDisplayed() {

            }

            @Override
            public void unomerSurveyClosed() {

            }

            @Override
            public void unomerUploadComplete(int i, String s, String s1, boolean isRewarded, String s2) {
                String url = API.COMPLETE_SURVEY + SharedPreferencesMethod.getUserId(HomeScreenActivity.this);
                if (isRewarded) {
                    API.sendRequestToServerGET(HomeScreenActivity.this, url, API.COMPLETE_SURVEY);
                } else {
                    Utils.showPopup(HomeScreenActivity.this, "No reward points earned", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                }
                survey.destroy();
                Utils.tookSurvey = true;

                Bundle bundle = new Bundle();
                bundle.putString("UserId", SharedPreferencesMethod.getUserId(HomeScreenActivity.this));
                Utils.logEvent(EventNames.SURVEY_GIVEN, bundle, HomeScreenActivity.this);
            }

            @Override
            public void unomerSurveySubmittedForUpload(int i, String s, String s1, boolean b) {

            }

            @Override
            public void unomerSurveyConfirmationDeclined() {

            }

            @Override
            public void unomerUploadFailed() {

            }

            @Override
            public void unomerUserDisqualified(int i, String s, String s1, boolean b) {

            }
        };
    }

    private void showSurveyPopup(final UnomerListener uListener) {
        try {
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle(getResources().getString(R.string.app_alert_text));
            dialogBuilder.setMessage(getResources().getString(R.string.survey_available_text));
            dialogBuilder.setCancelable(true);
            dialogBuilder.setPositiveButton(getResources().getString(R.string.primary_number_yes_btn_text), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    survey.showSurvey(HomeScreenActivity.this, uListener);
                    dialog.dismiss();
                }
            });

            dialogBuilder.setNegativeButton(getResources().getString(R.string.primary_number_no_btn_text), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                }
            });

            dialogBuilder.create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //This Method
    public void showDeleteAccountReasonPopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.delete_account_reason_popup, null);
        dialogBuilder.setView(dialogView);
        final RadioGroup radioGroup = dialogView.findViewById(R.id.rgDeleteAccounteGroup);
        final RadioButton radioButton1 = dialogView.findViewById(R.id.radio1);
        final RadioButton radioButton2 = dialogView.findViewById(R.id.radio2);
        final RadioButton radioButton3 = dialogView.findViewById(R.id.radio3);
        final RadioButton radioButton4 = dialogView.findViewById(R.id.radio4);
        final RadioButton radioButton5 = dialogView.findViewById(R.id.radio5);
        final RadioButton radioButton6 = dialogView.findViewById(R.id.radio6);
        final EditText enterReason = dialogView.findViewById(R.id.enterReason);
        final TextView fillReason = dialogView.findViewById(R.id.submmitDeleteAccountReason);
        if (radioButton1.isChecked()) {
            submmitDeleteInformation(fillReason, radioButton1, enterReason);
        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radio6) {
                    enterReason.setVisibility(VISIBLE);
                    submmitDeleteInformation(fillReason, radioButton6, enterReason);
                }
                if (checkedId == R.id.radio1) {
                    enterReason.setVisibility(GONE);
                    submmitDeleteInformation(fillReason, radioButton1, enterReason);
                }
                if (checkedId == R.id.radio2) {
                    enterReason.setVisibility(GONE);
                    submmitDeleteInformation(fillReason, radioButton2, enterReason);
                }
                if (checkedId == R.id.radio3) {
                    enterReason.setVisibility(GONE);
                    submmitDeleteInformation(fillReason, radioButton3, enterReason);
                }
                if (checkedId == R.id.radio4) {
                    enterReason.setVisibility(GONE);
                    submmitDeleteInformation(fillReason, radioButton4, enterReason);
                }
                if (checkedId == R.id.radio5) {
                    enterReason.setVisibility(GONE);
                    submmitDeleteInformation(fillReason, radioButton5, enterReason);
                }
            }
        });
        final AlertDialog confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();
    }

    public void submmitDeleteInformation(TextView textView, final RadioButton radioButton, final EditText editText) {
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioButton.isChecked()) {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    if (radioButton.getText().toString().equalsIgnoreCase("Other")) {
                        deleteReason = editText.getText().toString();
                        intent.putExtra("delete", true);
                        intent.putExtra("reason", editText.getText().toString());
                        startActivityForResult(intent, DELETE);
                    } else {
                        intent.putExtra("delete", true);
                        intent.putExtra("reason", radioButton.getText());
                        startActivityForResult(intent, DELETE);
                    }
                }
            }
        });
    }

    private void copyReferalCode() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", Utils.referralCode);
        clipboard.setPrimaryClip(clip);
        Utils.showPopup(this, "Referral Code Copied");
    }

    private void onClickLogout() {
        if (!Utility.isConnectingToInternet(HomeScreenActivity.this)) { // check net connection
            final Snackbar snackbar = Snackbar.make(header, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        progressBar = new com.codeholic.kotumb.app.View.ProgressBar(HomeScreenActivity.this);
        progressBar.show(getResources().getString(R.string.app_please_wait_text));
//        alertDialog.dismiss();
        HashMap<String, Object> input = new HashMap<>();
        input.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(HomeScreenActivity.this));
        input.put(RequestParameters.TOKEN, "" + FirebaseInstanceId.getInstance().getToken());

        Log.e("request", "" + input);

        API.sendRequestToServerPOST_PARAM(HomeScreenActivity.this, API.LOGOUT, input);//service call for logout
    }

    private void onClickDeleteAccount(String deleteReason) {

        Log.e("DELETE", "onClickDeleteAccount");

        if (!Utility.isConnectingToInternet(HomeScreenActivity.this)) { // check net connection
            final Snackbar snackbar = Snackbar.make(header, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        progressBar = new com.codeholic.kotumb.app.View.ProgressBar(HomeScreenActivity.this);
        progressBar.show(getResources().getString(R.string.app_please_wait_text));
//        alertDialog.dismiss();
        HomeScreenActivity context = HomeScreenActivity.this;
        String url = API.DELETE_ACC + SharedPreferencesMethod.getUserId(context);
        HashMap<String, Object> input = new HashMap<>();
        input.put(RequestParameters.REASON, deleteReason);
        Log.e("DELETE", "" + url);

        API.sendRequestToServerPOST_PARAM(context, url, input);// service call for otp
    }

    public void shareApp() {
       /* final String appPackageName = getPackageName();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out KOTUMB app at: https://play.google.com/store/apps/details?id=" + appPackageName);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);*/
        //share(this);
    }

    //getting response from service
    public void getResponse(JSONObject outPut, int i) {
        if (progressBar != null && i != 1) {
            progressBar.dismiss(); // dismissing progressbar
        }
        try {
            if (i == 0) { //logout response
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) {
                        SharedPreferencesMethod.clear(this);
                        //   SharedPreferencesMethod.setInt(HomeScreenActivity.this, "run", run);
                        Intent intent = new Intent(this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent); // open loginactivity
                    }
                } else {

                }
            } else if (i == 1) { // getting home service ressponse
                handler.postDelayed(runnable, 1500);
                //Log.e("home response", outPut.toString());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) {

                        if (outPut.has(ResponseParameters.REWARD_POINTS)) {
                            Utils.rewardPoints = outPut.getString(ResponseParameters.REWARD_POINTS);
                        }

                        if (outPut.has("canTakeSurvey")) {
                            Utils.canTakeSurvey = outPut.getString("canTakeSurvey");
                        }

                        if (outPut.has("group_img_base_url")) {
                            Utils.base_url_group = outPut.getString("group_img_base_url");
                        }

                        if (outPut.has(ResponseParameters.SURVEY_TIME)) {
                            try {
                                Utils.surveyTimeInSeconds = Integer.parseInt(outPut.getString(ResponseParameters.SURVEY_TIME));
//                                Utils.surveyTimeInSeconds = 15;
                            } catch (NumberFormatException | JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        if (outPut.has(ResponseParameters.IMAGE_BASE_URL)) {
                            API.imageUrl = outPut.getString(ResponseParameters.IMAGE_BASE_URL);
                        }


                        if (outPut.has(ResponseParameters.Userdata)) { // check is response have user data
                            if (homeFragment != null) {
                                JSONObject userObject = outPut.getJSONObject(ResponseParameters.Userdata);
                                if (!userObject.getString(ResponseParameters.PROFILE_STRENGTH).isEmpty() &&
                                        !userObject.getString(ResponseParameters.PROFILE_STRENGTH).equalsIgnoreCase("0")) {
                                    homeFragment.setProfileText("" + userObject.getString(ResponseParameters.PROFILE_STRENGTH) + "%");
                                    SharedPreferencesMethod.setString(this, ResponseParameters.PROFILE_STRENGTH, userObject.getString(ResponseParameters.PROFILE_STRENGTH) + "%");// setting profile strength
                                } else {
                                    SharedPreferencesMethod.setString(this, ResponseParameters.PROFILE_STRENGTH, "");
                                    homeFragment.setProfileText("");
                                }

                                if (userObject.has("referralCode")) {
                                    Utils.referralCode = userObject.getString("referralCode");
                                    navigationView.getMenu().getItem(7).setTitle("Referral Code: " + Utils.referralCode);
                                }

                            }
                        }

                        if (outPut.has(ResponseParameters.REWARDS)) {
                            Utils.rewards = outPut.getJSONArray(ResponseParameters.REWARDS);

                        }


                        if (outPut.has(ResponseParameters.MESSAGE_COUNT)) {
                            setBadgeCount(this, messageCount, "" + outPut.getInt(ResponseParameters.MESSAGE_COUNT));
                        }

                        if (outPut.has(ResponseParameters.CONNECTIONS_COUNT)) { // checking if response connection prevCount
                            if (bottomTabLayout != null) {
                                TabLayout.Tab tab = bottomTabLayout.getTabAt(4);
                                if (tab != null) {
                                    TextView textView = tab.getCustomView().findViewById(R.id.tvCount);
                                    if (!outPut.getString(ResponseParameters.CONNECTIONS_COUNT).trim().isEmpty() &&
                                            !outPut.getString(ResponseParameters.CONNECTIONS_COUNT).trim().equalsIgnoreCase("0")) {
                                        textView.setVisibility(View.VISIBLE);
                                        boolean b = outPut.getString(ResponseParameters.CONNECTIONS_COUNT).trim().length() > 2;
                                        b = false;
                                        if (b) {
                                            textView.setText("99");
                                        } else {
                                            textView.setText(outPut.getString(ResponseParameters.CONNECTIONS_COUNT).trim()); // setting connection badge
                                        }
                                    } else {
                                        textView.setVisibility(View.GONE);
                                    }
                                }
                            }
                        }

                        if (outPut.has(ResponseParameters.SHARE_TEXT)) {
                            JSONObject jsonObject = outPut.getJSONObject(ResponseParameters.SHARE_TEXT);
                            if (jsonObject.has("value")) {
                                String value = jsonObject.getString("value");
                                Utils.shareText = value + "";
                            }
                        }

                        if (outPut.has(ResponseParameters.NOTIFICATION_COUNT)) { // checking if response connection prevCount
                            if (bottomTabLayout != null) {
                                TabLayout.Tab tab = bottomTabLayout.getTabAt(1);
                                if (tab != null) {
                                    TextView textView = tab.getCustomView().findViewById(R.id.tvCount);
                                    if (!outPut.getString(ResponseParameters.NOTIFICATION_COUNT).trim().isEmpty() &&
                                            !outPut.getString(ResponseParameters.NOTIFICATION_COUNT).trim().equalsIgnoreCase("0")) {
                                        textView.setVisibility(View.VISIBLE);
                                        int actId = 0;
                                        String lastActTime = "";
                                        Date lastNotiTime;
                                        if (outPut.has("act_id")) {
                                            actId = Integer.parseInt(outPut.getString("act_id"));
                                        }
                                        if (outPut.has("lastActTime")) {
                                            lastActTime = outPut.getString("lastActTime");
                                        }
                                        int count = Integer.parseInt(outPut.getString(ResponseParameters.NOTIFICATION_COUNT).trim());
//                                        System.out.println("Notification Count  " + count);
                                        lastNotiTime = serverDate.parse(lastActTime);
                                        boolean condition1 = prevActId != -1 && actId > prevActId;
                                        boolean condition2 = lastNotiTime.after(prevlastNotiTime);
                                        if (condition1 || condition2) {
                                            showNewNotificationsBubble(true, count);
                                        }
                                        prevActId = actId;
                                        prevNotiCount = count;
                                        prevlastNotiTime = lastNotiTime;
                                        if (outPut.getString(ResponseParameters.NOTIFICATION_COUNT).trim().length() > 2) {
                                            textView.setText("99");
                                        } else {
                                            textView.setText(outPut.getString(ResponseParameters.NOTIFICATION_COUNT).trim()); // setting connection badge
                                        }
                                    } else {
                                        textView.setVisibility(View.GONE);
                                        showNewNotificationsBubble(false, 0);
                                        prevNotiCount = 0;
                                    }
                                }
                            }
                        }

                        if (outPut.has(ResponseParameters.INVITATIONS_COUNT)) { // checking invitation prevCount
                            if (bottomTabLayout != null) {
                                TabLayout.Tab tab = bottomTabLayout.getTabAt(3);
                                if (tab != null) {
                                    TextView textView = tab.getCustomView().findViewById(R.id.tvCount);
                                    if (!outPut.getString(ResponseParameters.INVITATIONS_COUNT).trim().isEmpty() &&
                                            !outPut.getString(ResponseParameters.INVITATIONS_COUNT).trim().equalsIgnoreCase("0")) {
                                        textView.setVisibility(View.VISIBLE);
                                        if (outPut.getString(ResponseParameters.INVITATIONS_COUNT).trim().length() > 2) {
                                            SharedPreferencesMethod.setString(this, ResponseParameters.INVITATIONS_COUNT, "99"); //setting invitation prevCount
                                            textView.setText("99");
                                            homeFragment.setInviteText("99");
                                        } else {
                                            SharedPreferencesMethod.setString(this, ResponseParameters.INVITATIONS_COUNT, outPut.getString(ResponseParameters.INVITATIONS_COUNT).trim());//setting invitation prevCount
                                            textView.setText(outPut.getString(ResponseParameters.INVITATIONS_COUNT).trim());
                                            homeFragment.setInviteText(outPut.getString(ResponseParameters.INVITATIONS_COUNT).trim());
                                        }
                                    } else {
                                        SharedPreferencesMethod.setString(this, ResponseParameters.INVITATIONS_COUNT, "");
                                        textView.setVisibility(View.GONE);
                                        homeFragment.setInviteText("");
                                    }
                                }
                            }
                        }
                        if (outPut.has(ResponseParameters.SHOW_POPUP_PROFESSION) && Constants.showPopUpOnHome) {
                            Constants.showPopUpOnHome = false;
                            //User user = SharedPreferencesMethod.getUserInfo(this);
                            //for changing number
                            //SHOW_POPUP_PRIMARY_NUMBER = outPut.getBoolean(ResponseParameters.SHOW_POPUP_PRIMARY_NUMBER);
//                            PROFILE_POPUP = outPut.getBoolean(ResponseParameters.PROFILE_SUMMERY);
                            SHOW_POPUP_PROFESSION = outPut.getBoolean(ResponseParameters.SHOW_POPUP_PROFESSION);
                            System.out.println("Boolean Details 1  " + SHOW_POPUP_PROFESSION);
                            showPopUp();
                        }


                        if (Constants.showPopUpVerCheck) {
//                            Constants.showPopUpVerCheck = false;
                            //for changing number
                            //SHOW_POPUP_PRIMARY_NUMBER = outPut.getBoolean(ResponseParameters.SHOW_POPUP_PRIMARY_NUMBER);
                            if (outPut.has(ResponseParameters.UPDATE)) {
                                PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
                                int verCode = pInfo.versionCode;
                                if (verCode < outPut.getJSONObject(ResponseParameters.UPDATE).getInt(ResponseParameters.VERSION)) {
                                    showUpdatePopUP(outPut.getJSONObject(ResponseParameters.UPDATE).getBoolean(ResponseParameters.REQUIRED));
                                }
                            }
                        }
                    }
                }
            } else if (i == 2) {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(bottomTabLayout, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        otp = outPut.getString(ResponseParameters.Otp).trim();// updating otp
                        OtpPopup();
                        /*final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.verify_otp), Toast.LENGTH_SHORT);
                        snackbar.displayBubble();*/
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { // error
                    final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { // no net connection
                    final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            } else if (i == 3) {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(bottomTabLayout, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else if (outPut.has(ResponseParameters.Success)) {
                        SharedPreferencesMethod.setString(HomeScreenActivity.this, ResponseParameters.MobileNumber, "" + changeMobile.get(RequestParameters.MOBILE));
                        final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.number_changed_success), Toast.LENGTH_SHORT);
                        snackbar.show();
                        if (SHOW_POPUP_PROFESSION) {
                            showAddProfessionPopUp();
                        }

                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { // error
                    final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { // no net connection
                    final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            }
            /*if (i == 20) {//logout
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) {
                        // performLogout();
                        HomeScreenActivity context = HomeScreenActivity.this;
                        String url = API.DELETE_ACC + SharedPreferencesMethod.getUserId(context);
                        API.sendRequestToServerPOST_PARAM(context, url, new HashMap<String, Object>());// service call for otp
                    }
                } else {

                }
            }*/
            if (i == 30) {//delete response

                Log.e("DELETE", "" + outPut);

                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) {
                        performLogout();
                    }
                }
            }
            if (i == 12) {
                System.out.println("Survey Response   " + outPut.toString());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) {

                    } else if (outPut.has(ResponseParameters.Errors)) {
//                    Snackbar.make(lladdMembers, Utils.getErrorMessage(outPut), 0).show();
                        Utils.showPopup(HomeScreenActivity.this, Utils.getErrorMessage(outPut), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        });
                    } else {
//                    Snackbar.make(lladdMembers, UnomerActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                        Utils.showPopup(HomeScreenActivity.this, getResources().getString(R.string.app_no_internet_error), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        });
                    }
                } else {
//                Snackbar.make(lladdMembers, UnomerActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                    Utils.showPopup(HomeScreenActivity.this, getResources().getString(R.string.app_no_internet_error), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (PROFILE_SUMMERY) {
            setValueShare();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
//        PROFILE_SUMMERY=false;
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
//        PROFILE_SUMMERY=false;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private void performLogout() {
        SharedPreferencesMethod.clear(this);
        //   SharedPreferencesMethod.setInt(HomeScreenActivity.this, "run", run);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("popup", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent); // open loginactivity
    }

    private void showNewNotificationsBubble(boolean b, int notiCount) {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        long diff = currentTime - lastBubbleDisplayTime;
        long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(diff);
        //don't displayBubble bubble if less than one second
        if (b) {
            if (diffInSeconds > 2 && notiCount >= prevNotiCount) {
                lastBubbleDisplayTime = Calendar.getInstance().getTimeInMillis();
                displayBubble(true);
            }
        } else {
            displayBubble(false);
        }
    }

    private void displayBubble(boolean b2) {
        if (notificationFragment != null) {
            notificationFragment.showNewNotificationsBubble(b2);
        }
        if (homeFragment != null && homeFragment.fragment != null) {
            homeFragment.fragment.showNewNotificationsBubble(b2);
        }
    }

    private void showPopUp() {
        if (SHOW_POPUP_PRIMARY_NUMBER) {
            showMobileNumberChangePopUp();
        } else if (SHOW_POPUP_PROFESSION) {
            showAddProfessionPopUp();
        }
    }

    private void showUpdatePopUP(boolean force_update) {
        try {
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle(getResources().getString(R.string.app_alert_text));
            dialogBuilder.setMessage(getResources().getString(R.string.app_update_popup_msg));
            dialogBuilder.setCancelable(!force_update);
            dialogBuilder.setPositiveButton(getResources().getString(R.string.primary_number_yes_btn_text), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                    dialog.dismiss();
                    finish();
                }
            });

            if (!force_update)
                dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });


            final AlertDialog dialog = dialogBuilder.create();

            /*dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface arg0) {
                    dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorSecondary));
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.textColor));
                }
            });*/

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showMobileNumberChangePopUp() {
        try {
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle(getResources().getString(R.string.app_alert_text));
            dialogBuilder.setMessage(getResources().getString(R.string.primary_number_question_text));
            dialogBuilder.setCancelable(true);
            dialogBuilder.setPositiveButton(getResources().getString(R.string.primary_number_yes_btn_text), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    MobilePopup();
                    dialog.dismiss();
                }
            });

            dialogBuilder.setNegativeButton(getResources().getString(R.string.primary_number_no_btn_text), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                    if (SHOW_POPUP_PROFESSION) {
                        showAddProfessionPopUp();
                    }
                }
            });

            dialogBuilder.create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void MobilePopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.phone_popup, null);
        dialogBuilder.setView(dialogView);
        final EditText etUserPhone = dialogView.findViewById(R.id.etPhone);
        final TextInputLayout etPhoneLayout = dialogView.findViewById(R.id.etPhoneLayout);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.change_login_info_change_btn), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        this.mobileAlertDialog = dialogBuilder.create();
        this.mobileAlertDialog.setCancelable(false);
        this.mobileAlertDialog.show();
        this.mobileAlertDialog.getButton(-1).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Boolean.valueOf(false).booleanValue()) {
                    HomeScreenActivity.this.mobileAlertDialog.dismiss();
                }
                etPhoneLayout.setErrorEnabled(false);
                if (etUserPhone.getText().toString().trim().isEmpty()) {
                    etPhoneLayout.setErrorEnabled(true);
                    etPhoneLayout.setError(HomeScreenActivity.this.getResources().getString(R.string.mobile_empty_input));
                } else if (isValidLong(etUserPhone.getText().toString())) {
                    if (etUserPhone.getText().toString().length() < 10) {
                        etPhoneLayout.setError(HomeScreenActivity.this.getResources().getString(R.string.mobile_incorrect_input_minlength));
                    } else if (Utility.isConnectingToInternet(HomeScreenActivity.this)) {
                        changeMobile = new HashMap<>();
                        changeMobile.put(RequestParameters.LANG, "" + SharedPreferencesMethod.getDefaultLanguageName(HomeScreenActivity.this));
                        changeMobile.put(RequestParameters.MOBILE, "" + etUserPhone.getText().toString().trim());
                        changeMobile.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(HomeScreenActivity.this));
                        HomeScreenActivity.this.progressBar = new com.codeholic.kotumb.app.View.ProgressBar(HomeScreenActivity.this);
                        HomeScreenActivity.this.progressBar.show(HomeScreenActivity.this.getResources().getString(R.string.app_please_wait_text));
                        Log.e("input", changeMobile.toString());
                        API.sendRequestToServerPOST_PARAM(HomeScreenActivity.this, API.SEND_OTP, changeMobile);// service call for otp
                        HomeScreenActivity.this.mobileAlertDialog.dismiss();
                    } else {
                        Snackbar.make(etPhoneLayout, HomeScreenActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                    }
                } else {
                    etPhoneLayout.setError(HomeScreenActivity.this.getResources().getString(R.string.mobile_incorrect_input_number));
                }
            }
        });
    }

    private void OtpPopup() {


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.otp_popup, null);
        dialogBuilder.setView(dialogView);
        final OtpEditText etOtp = dialogView.findViewById(R.id.etOtp);
        final TextView tvOtpHint = dialogView.findViewById(R.id.tvOtpHint);
        final TextView tvResend = dialogView.findViewById(R.id.tv_resend);
        final TextView tv_timer = dialogView.findViewById(R.id.tv_otp_timer);
        tv_timer.setVisibility(VISIBLE);
        tvResend.setVisibility(GONE);
        final CountDownTimer countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tv_timer.setText(String.valueOf(getResources().getString(R.string.otp_timer_text_1) + " " + (int) millisUntilFinished / 1000) + " sec. " + getResources().getString(R.string.otp_timer_text_2));
            }

            @Override
            public void onFinish() {
                tv_timer.setVisibility(View.GONE);
                tvResend.setVisibility(View.VISIBLE);
            }
        };
        //timer for otp resend start
        countDownTimer.start();
        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    if (s.length() >= 6) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(etOtp.getWindowToken(), 0);
                        if (s.toString().equalsIgnoreCase(otp)) { // checking otp
                            if (!Utility.isConnectingToInternet(HomeScreenActivity.this)) { // check net connection
                                /*final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.no_net), Toast.LENGTH_SHORT);
                                snackbar.displayBubble();*/
//                                Toast.makeText(HomeScreenActivity.this, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).displayBubble();
                                Utils.showPopup(HomeScreenActivity.this, getResources().getString(R.string.app_no_internet_error));
                                return;
                            }
                            progressBar = new com.codeholic.kotumb.app.View.ProgressBar(HomeScreenActivity.this);
                            progressBar.show(getResources().getString(R.string.app_please_wait_text));
                            verifyOtp();
                            otpAlertDialog.dismiss();
                        } else {
                            Utils.showPopup(HomeScreenActivity.this, getResources().getString(R.string.otp_incorrect_input));
//                            Toast.makeText(HomeScreenActivity.this, getResources().getString(R.string.otp_incorrect_input), Toast.LENGTH_SHORT).displayBubble();
                            /*final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.check_otp), Toast.LENGTH_SHORT);
                            snackbar.displayBubble();*/
                        }
                    }
                    tvOtpHint.setVisibility(GONE);
                } else {
                    tvOtpHint.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!Utility.isConnectingToInternet(HomeScreenActivity.this)) { //check net connection
                        return;
                    }
                    HomeScreenActivity.this.progressBar = new com.codeholic.kotumb.app.View.ProgressBar(HomeScreenActivity.this);
                    HomeScreenActivity.this.progressBar.show(HomeScreenActivity.this.getResources().getString(R.string.app_please_wait_text));
                    otpAlertDialog.dismiss();
                    API.sendRequestToServerPOST_PARAM(HomeScreenActivity.this, API.SEND_OTP, changeMobile); // resend web service call

                } catch (Exception e) {
                    final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                    e.printStackTrace();
                }
                tvResend.setVisibility(View.GONE);
                tv_timer.setVisibility(View.VISIBLE);
                countDownTimer.start();
            }
        });
        otpAlertDialog = dialogBuilder.create();
        otpAlertDialog.setCancelable(true);
        otpAlertDialog.show();
    }

    //verify otp method
    private void verifyOtp() {
        try {
            if (!Utility.isConnectingToInternet(HomeScreenActivity.this)) { // check net connection
                if (progressBar != null) {
                    progressBar.dismiss(); // dismissing progressbar
                }
                final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }

            API.sendRequestToServerPOST_PARAM(HomeScreenActivity.this, API.CHANGE_MOBILE, changeMobile);// service call for account verification

        } catch (Exception e) {
            final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.otp_incorrect_input), Toast.LENGTH_SHORT);
            snackbar.show();
            e.printStackTrace();
        }
    }

    private void showAddProfessionPopUp() {
        try {
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle(getResources().getString(R.string.app_alert_text));
            dialogBuilder.setMessage(getResources().getString(R.string.confirm_profession_add_message));
            dialogBuilder.setCancelable(true);
            dialogBuilder.setPositiveButton(getResources().getString(R.string.primary_number_yes_btn_text), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //startActivity(new Intent(HomeScreenActivity.this, ProfessionActivity.class));
                    Intent intent = new Intent(HomeScreenActivity.this, CompleteProfileActivity.class);
                    intent.putExtra(ResponseParameters.Id, R.id.ll_organisation);
                    startActivity(intent);
                    dialog.dismiss();
                }
            });

            dialogBuilder.setNegativeButton(getResources().getString(R.string.primary_number_no_btn_text), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    switchTab(0);
                    dialog.dismiss();
                }
            });

            dialogBuilder.create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setValueShare() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (!prefs.getBoolean(SharedPreferencesMethod.POP_UP, false)) {
            User user = SharedPreferencesMethod.getUserInfo(this);

            String profileSummury = user.getProfileSummary();
            if (profileSummury.equalsIgnoreCase("") || profileSummury.isEmpty()) {
                showAddSummryPopUp();
            }
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(SharedPreferencesMethod.POP_UP, true);
            editor.commit();
            PROFILE_SUMMERY = false;
        }
    }

    public void setValueForSummery() {
        SharedPreferences settings = getSharedPreferences("PROFILE_SUMMARY", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("PROFILE", 111);
        editor.commit();
    }

    private void showAddSummryPopUp() {
        try {
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle(getResources().getString(R.string.app_alert_text));
            dialogBuilder.setMessage(getResources().getString(R.string.confirm_summary_add_message));
            dialogBuilder.setCancelable(true);
            dialogBuilder.setPositiveButton(getResources().getString(R.string.primary_number_yes_btn_text), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //startActivity(new Intent(HomeScreenActivity.this, ProfessionActivity.class));
                    Intent intent = new Intent(HomeScreenActivity.this, CompleteProfileActivity.class);
                    intent.putExtra(ResponseParameters.Id, R.id.etProfileHeadLine);
                    startActivity(intent);
                    dialog.dismiss();
                }
            });


            dialogBuilder.setNegativeButton(getResources().getString(R.string.primary_number_no_btn_text), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    JSONObject outPut = new JSONObject();
                    if (outPut.has(ResponseParameters.SHOW_POPUP_PROFESSION) && Constants.showPopUpOnHome) {
                        Constants.showPopUpOnHome = false;
                        try {
                            SHOW_POPUP_PROFESSION = outPut.getBoolean(ResponseParameters.SHOW_POPUP_PROFESSION);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        showPopUp();
                    }
                }
            });

            dialogBuilder.create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        if (getAd != null) {
            unregisterReceiver(getAd);
            getAd = null;
        } else {
        }

        AppEventsLogger.deactivateApp(getApplication());

        Constants.showPopUpOnHome = true;
        Constants.showPopUpOnHomeProfile = true;
        Constants.showPopUpVerCheck = true;
        stopService(new Intent(this, GetAddService.class));
        super.onDestroy();
    }

    public void share(View view) {
//         share(this);
        createDynamicLink(this, SharedPreferencesMethod.getUserId(this), Utils.referralCode);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.w(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services Error: "
                        + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }


    @Override
    public void profileValue(int s) {
        if (s > 75){
            Log.e("HOME","Profile percentage is : " + (SharedPreferencesMethod.getInt(this,"PROFILEPERCENTAGE")));

            if ((SharedPreferencesMethod.getInt(this,"PROFILEPERCENTAGE")) > 75){
                FacebookSdk.setApplicationId(getResources().getString(R.string.facebook_app_id));
                FacebookSdk.sdkInitialize(getApplication());
                AppEventsLogger.activateApp(getApplication());
            }
        }
    }

    //change locale
    private void changeLocale(String languageToLoad) {
        locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

    }
}