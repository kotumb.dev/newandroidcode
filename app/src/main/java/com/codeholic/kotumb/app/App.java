package com.codeholic.kotumb.app;

import android.app.Application;
import android.content.Context;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;

/**
 * Created by DELL on 1/9/2018.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

//        FacebookSdk.setApplicationId(getResources().getString(R.string.facebook_app_id));
//        FacebookSdk.sdkInitialize(this);
//        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }

    public static App instance = null;

    /* Protection at runtime */
    public App() {
        if (instance == null) {
            instance = this;
        } else {
            throw new RuntimeException("Cannot create more than one FacebookEventsApp");
        }
    }
}
