package com.codeholic.kotumb.app.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DELL on 1/15/2018.
 */

public class Notification implements Parcelable {

    User user = new User();
    Group group = new Group();
    String notificationId = "";
    String notificationType = "";
    String notificationTime = "";
    String notificationDes = "";
    private String id = "";
    String isPublished="";
    String fullname="";
    private String status="1";
    String media="";
    String likeCount="";
    String title="";
    String avatar="";
    String thumbnail="";
    String mediaType="";
    String notifyUserFirstName="";
    String notifyUserLasrName="";
    String video_date_time="";
    String message="";
    String like="";

//    String UserFrom="";
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public static Creator<Notification> getCREATOR() {
        return CREATOR;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    String comment = "";
    private String groupImageURL;
    private int count;
    private int fieldId;
    private Post post;
    private VideoData videoData;

    protected Notification(Parcel in) {
        user = in.readParcelable(User.class.getClassLoader());
        try {
            group = in.readParcelable(Group.class.getClassLoader());
        } catch (Exception e) {
            e.printStackTrace();
        }
        notificationId = in.readString();
        isPublished=in.readString();
        notificationType = in.readString();
        notificationTime = in.readString();
        notificationDes = in.readString();
        message=in.readString();
        likeCount=in.readString();
        title=in.readString();
        mediaType = in.readString();
        like=in.readString();
        media = in.readString();
        thumbnail=in.readString();
        avatar=in.readString();
        fullname=in.readString();
        video_date_time=in.readString();
        notifyUserFirstName=in.readString();
        notifyUserLasrName=in.readString();

//        UserFrom=in.readString();
    }

    public Notification() {

    }

    public Group getGroup() {
        return group;
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public String getNotificationDes() {
        return notificationDes;
    }

    public void setNotificationDes(String notificationDes) {
        this.notificationDes = notificationDes;
    }

    public String getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(String notificationTime) {
        this.notificationTime = notificationTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }


    public String getNotifyUserFirstName() {
        return notifyUserFirstName;
    }

    public String getNotifyUserLasrName() {
        return notifyUserLasrName;
    }

    public void setNotifyUserFirstName(String notifyUserFirstName) {
        this.notifyUserFirstName = notifyUserFirstName;
    }

    public String getVideo_date_time() {
        return video_date_time;
    }

    public void setVideo_date_time(String video_date_time) {
        this.video_date_time = video_date_time;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setNotifyUserLasrName(String notifyUserLasrName) {
        this.notifyUserLasrName = notifyUserLasrName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(user, flags);
        dest.writeString(mediaType);
        dest.writeString(media);
        dest.writeString(notificationId);
        dest.writeString(notificationType);
        dest.writeString(title);
        dest.writeString(isPublished);
        dest.writeString(notificationTime);
        dest.writeString(notificationDes);
        dest.writeString(message);
        dest.writeString(likeCount);
        dest.writeString(avatar);
        dest.writeString(like);
        dest.writeString(fullname);
        dest.writeString(thumbnail);
        dest.writeString(video_date_time);
//        dest.writeString(UserFrom);

    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getIsPublished() {
        return isPublished;
    }

    public void setIsPublished(String isPublished) {
        this.isPublished = isPublished;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public void setGroupImageURL(String groupImageURL) {
        this.groupImageURL = groupImageURL;
    }

    public String getGroupImageURL() {
        return groupImageURL;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public int getCount() {
        return count;
    }

    public void setFieldId(int fieldId) {
        this.fieldId = fieldId;
    }

    public int getFieldId() {
        return fieldId;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public void setVideoData(VideoData videoData) {
        this.videoData = videoData;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public VideoData getVideoData() {
        return videoData;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }
    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }
//
//    public String getUserFrom() {
//        return UserFrom;
//    }
//
//    public void setUserFrom(String UserFrom) {
//        this.UserFrom = UserFrom;
//    }
}
