package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.PostLikes;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;

import java.util.List;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class LikeListRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<PostLikes> postLikes;
    Activity activity;
    AlertDialog alert;


    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;


    public void setImageBaseUrl(String base_url) {
        this.base_url = base_url;
    }

    String base_url = "";

    public LikeListRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<PostLikes> postLikes) {
        this.postLikes = postLikes;
        this.activity = activity;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }


    @Override
    public int getItemViewType(int position) {
        return postLikes.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return postLikes.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.member_list_item, parent, false);
            GroupViewHolder GroupViewHolder = new GroupViewHolder(view);
            return GroupViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, final int position) {
        PostLikes postLikes = this.postLikes.get(position);
        if (viewholder instanceof GroupViewHolder) {
            GroupViewHolder holder = (GroupViewHolder) viewholder;
            String text = "" + postLikes.firstName.trim() + " " + postLikes.middleName + " " + postLikes.lastName.trim();
            holder.tvUserName.setText(text.replaceAll("  "," "));

                holder.llAdminTextContainer.setVisibility(View.GONE);
            new AQuery(activity).id(holder.ivGroupImage).image(base_url + postLikes.avatar.trim(), true, true, 300, R.drawable.ic_user);


                setOnClickListener(holder, postLikes, position);

        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    public void setOnClickListener(final GroupViewHolder holder, final PostLikes group, final int position) {
        holder.rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                API.openOtherUserProfile(group.userId,activity);
            }
        });
    }




    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvCount;
        ImageView ivGroupImage;
        RelativeLayout rlContainer;
        LinearLayout llAdminTextContainer;


        GroupViewHolder(View view) {
            super(view);
            tvUserName = (TextView) view.findViewById(R.id.tvUserName);
            ivGroupImage = (ImageView) view.findViewById(R.id.ivGroupImage);
            rlContainer = (RelativeLayout) view.findViewById(R.id.rlContainer);
            tvCount = (TextView) view.findViewById(R.id.tvCount);
            llAdminTextContainer = (LinearLayout) view.findViewById(R.id.llAdminTextContainer);
        }
    }
}