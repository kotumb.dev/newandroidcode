package com.codeholic.kotumb.app.Services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;

import org.json.JSONObject;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class GetPreLoadDataService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_GET_ORG = "com.codeholic.kotumb.app.Services.action.GET_ORG";
    private static final String ACTION_GET_SCHOOLS = "com.codeholic.kotumb.app.Services.action.GET_SCHOOLS";
    private static final String ACTION_GET_COURSES = "com.codeholic.kotumb.app.Services.action.GET_COURSES";
    private static final String ACTION_GET_ROLES = "com.codeholic.kotumb.app.Services.action.GET_ROLES";
    private static final String ACTION_GET_SKILLS = "com.codeholic.kotumb.app.Services.action.GET_SKILLS";
    private static final String ACTION_GET_INDUSTRIES = "com.codeholic.kotumb.app.Services.action.GET_INDUSTRIES";
    private static final String ACTION_GET_LANGUAGE = "com.codeholic.kotumb.app.Services.action.GET_LANGUAGE";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "com.codeholic.kotumb.app.Services.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "com.codeholic.kotumb.app.Services.extra.PARAM2";

    public GetPreLoadDataService() {
        super("GetPreLoadDataService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionGetNameOrg(Context context) {

        Log.e("TAG", "TAG");

        Intent intent = new Intent(context, GetPreLoadDataService.class);
        intent.setAction(ACTION_GET_ORG);

        try {
            context.startService(intent);
        }
        catch (Exception IllegalStateException) {
           // intent.putExtra(NEED_FOREGROUND_KEY, true);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            }
            else {
                context.startService(intent);
            }
        }


       // context.startService(intent);
    }

    public static void startActionGetSchools(Context context) {
        Intent intent = new Intent(context, GetPreLoadDataService.class);
        intent.setAction(ACTION_GET_SCHOOLS);
      //  context.startService(intent);
        try {
            context.startService(intent);
        }
        catch (Exception IllegalStateException) {
            // intent.putExtra(NEED_FOREGROUND_KEY, true);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            }
            else {
                context.startService(intent);
            }
        }
    }

    public static void startActionGetCourses(Context context) {
        Intent intent = new Intent(context, GetPreLoadDataService.class);
        intent.setAction(ACTION_GET_COURSES);
       // context.startService(intent);

        try {
            context.startService(intent);
        }
        catch (Exception IllegalStateException) {
            // intent.putExtra(NEED_FOREGROUND_KEY, true);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            }
            else {
                context.startService(intent);
            }
        }
    }

    public static void startActionGetRoles(Context context) {
        Intent intent = new Intent(context, GetPreLoadDataService.class);
        intent.setAction(ACTION_GET_ROLES);
       // context.startService(intent);
        try {
            context.startService(intent);
        }
        catch (Exception IllegalStateException) {
            // intent.putExtra(NEED_FOREGROUND_KEY, true);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            }
            else {
                context.startService(intent);
            }
        }
    }

    public static void startActionGetLanguage(Context context) {
        Intent intent = new Intent(context, GetPreLoadDataService.class);
        intent.setAction(ACTION_GET_LANGUAGE);
       // context.startService(intent);
        try {
            context.startService(intent);
        }
        catch (Exception IllegalStateException) {
            // intent.putExtra(NEED_FOREGROUND_KEY, true);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            }
            else {
                context.startService(intent);
            }
        }
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionGetSkills(Context context) {
        Intent intent = new Intent(context, GetPreLoadDataService.class);
        intent.setAction(ACTION_GET_SKILLS);
       // context.startService(intent);

        try {
            context.startService(intent);
        }
        catch (Exception IllegalStateException) {
            // intent.putExtra(NEED_FOREGROUND_KEY, true);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            }
            else {
                context.startService(intent);
            }
        }
    }

    public static void startActionGetIndustries(Context context) {
        Intent intent = new Intent(context, GetPreLoadDataService.class);
        intent.setAction(ACTION_GET_INDUSTRIES);
       // context.startService(intent);

        try {
            context.startService(intent);
        }
        catch (Exception IllegalStateException) {
            // intent.putExtra(NEED_FOREGROUND_KEY, true);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            }
            else {
                context.startService(intent);
            }
        }
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_ORG.equals(action)) {
                handleActionGetNameOrg();
            } else if (ACTION_GET_SCHOOLS.equals(action)) {
                handleActionGetSchools();
            } else if (ACTION_GET_COURSES.equals(action)) {
                handleActionGetCourses();
            } else if (ACTION_GET_ROLES.equals(action)) {
                handleActionGetRoles();
            } else if (ACTION_GET_SKILLS.equals(action)) {
                handleActionGetSkills();
            } else if (ACTION_GET_INDUSTRIES.equals(action)) {
                handleActionGetIndustries();
            } else if (ACTION_GET_LANGUAGE.equals(action)) {
                handleActionGetLanguage();
            }
        }
    }

    /**
     * Handle action get name org in the provided background thread
     */
    private void handleActionGetNameOrg() {
        // TODO: Handle action
        Log.e("TAG", "" + API.ORGANIZATION + SharedPreferencesMethod.getDefaultLanguage(this));
        API.sendRequestToServerGET(this, API.ORGANIZATION, API.ORGANIZATION);
    }

    /**
     * Handle action get schools in the provided background thread
     */
    private void handleActionGetSchools() {
        // TODO: Handle action
        API.sendRequestToServerGET(this, API.SCHOOLS , API.SCHOOLS);
    }

    /**
     * Handle action get courses in the provided background thread
     */
    private void handleActionGetCourses() {
        // TODO: Handle action
        API.sendRequestToServerGET(this, API.COURSES , API.COURSES);
    }

    /**
     * Handle action get roles in the provided background thread
     */
    private void handleActionGetRoles() {
        // TODO: Handle action
        API.sendRequestToServerGET(this, API.JOBROLES, API.JOBROLES);
    }


    /**
     * Handle action get skills in the provided background thread
     */
    private void handleActionGetSkills() {
        // TODO: Handle action
        API.sendRequestToServerGET(this, API.GET_SKILLS, API.GET_SKILLS);
    }

    private void handleActionGetIndustries() {
        // TODO: Handle action
        API.sendRequestToServerGET(this, API.GET_INDUSTRIES, API.GET_INDUSTRIES);
    }

    /**
     * Handle action get language in the provided background thread
     */
    private void handleActionGetLanguage() {
        // TODO: Handle action
        API.sendRequestToServerGET(this, API.GET_LANGUAGE , API.GET_LANGUAGE);
    }

    public void getResponse(JSONObject outPut, int type) {
        try {
            Log.e("service response", "" + outPut);
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Success)) { //check output have success
                    if (type == 0) {
                        if (outPut.has(ResponseParameters.ORGANISATIONS)) { //check output have org
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.ORGANISATIONS, outPut.toString());
                        }
                    }
                    if (type == 1) {
                        if (outPut.has(ResponseParameters.SCHOOLS)) { //check output have org
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.SCHOOLS, outPut.toString());
                        }
                    }
                    if (type == 2) {
                        if (outPut.has(ResponseParameters.COURSES)) { //check output have org
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.COURSES, outPut.toString());
                        }
                    }
                    if (type == 3) {
                        if (outPut.has(ResponseParameters.JOBROLES)) { //check output have job roles
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.JOBROLES, outPut.toString());
                        }
                    }
                    if (type == 4) {
                        if (outPut.has(ResponseParameters.SKILLS)) { //check output have skills
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.SKILLS, outPut.toString());
                        }
                    }
                    if (type == 5) {
                        if (outPut.has(ResponseParameters.ALL_LANGUAGES)) { //check output have language
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.ALL_LANGUAGES, outPut.toString());
                        }
                    }
                    if (type == 40) {
                        if (outPut.has(ResponseParameters.INDUSTRIES)) { //check output have org
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.INDUSTRIES, outPut.toString());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
