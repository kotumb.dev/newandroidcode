package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Activity.ChatActivity;
import com.codeholic.kotumb.app.Activity.CompleteProfileActivity;
import com.codeholic.kotumb.app.Activity.MessageActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.EmojiMapUtil;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class MessageRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<User> users;
    Activity activity;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;
    private boolean fromShareResume = false;
    private boolean videoShare=false;

    public void setConnections(boolean connections) {
        this.connections = connections;
    }

    private boolean connections = false;

    public MessageRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<User> users) {
        this.users = users;
        this.activity = activity;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return users.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_list_item, parent, false);
            UserViewHolder UserViewHolder = new UserViewHolder(view);
            return UserViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {

            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);

        }

        return null;
    }

    public void setFromShareResume(boolean fromShareResume) {
        this.fromShareResume = fromShareResume;
    }
    public void setVideoShare(boolean videoShare) {
        this.videoShare = videoShare;
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {
        User user = users.get(position);
        try {
            if (viewholder instanceof UserViewHolder) {
                UserViewHolder holder = (UserViewHolder) viewholder;
                String firstName = user.getFirstName();
                holder.tvUserName.setText("" + firstName + " " + user.getLastName());

                if (connections) {
                    if (user.getProfileSummary() != null && !user.getProfileSummary().trim().equalsIgnoreCase("null") && !user.getProfileSummary().trim().isEmpty()) {
                        holder.tvMessage.setText("" + user.getProfileSummary().trim());
                        holder.tvMessage.setTextSize(16);
                        holder.tvMessage.setMaxLines(3);
                    } else {
                        holder.tvMessage.setText("");
                    }
                    holder.tvTime.setVisibility(GONE);
                } else {
                    JSONObject jsonObject = new JSONObject(user.getUserInfo());
                    if (jsonObject.has(ResponseParameters.LAST_MSG_FLAG)) {
                        if (jsonObject.getBoolean(ResponseParameters.LAST_MSG_FLAG)) {
                            JSONObject msgInfo = jsonObject.getJSONObject(ResponseParameters.LAST_MSG_INFO);
                            holder.tvTime.setText(msgInfo.getString(ResponseParameters.MESSAGEAT));
                            String reply = EmojiMapUtil.replaceCheatSheetEmojis("" + msgInfo.getString(ResponseParameters.REPLY));
                            if (msgInfo.getString(ResponseParameters.User_Id).equalsIgnoreCase(SharedPreferencesMethod.getUserId(activity))) {
                                holder.tvMessage.setText(""+reply);
                            } else {
                                holder.tvMessage.setText(""+reply);
                            }
                        } else {
                            holder.tvTime.setText("");
                            holder.tvMessage.setText("");
                        }
                    }

                    if (jsonObject.has(ResponseParameters.UNREAD_MESSAGE_COUNT)) {
                        if (jsonObject.getInt(ResponseParameters.UNREAD_MESSAGE_COUNT) > 0) {
                            holder.tvCount.setVisibility(VISIBLE);
                            holder.tvCount.setText("" + jsonObject.getInt(ResponseParameters.UNREAD_MESSAGE_COUNT));
                            holder.tvMessage.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                            holder.tvMessage.setTypeface(holder.tvMessage.getTypeface(), Typeface.NORMAL);
                        } else {
                            holder.tvMessage.setTextColor(activity.getResources().getColor(R.color.textColor));
                            holder.tvMessage.setTypeface(holder.tvMessage.getTypeface(), Typeface.NORMAL);
                            holder.tvCount.setVisibility(GONE);
                        }
                    } else {
                        holder.tvMessage.setTypeface(holder.tvMessage.getTypeface(), Typeface.NORMAL);
                        holder.tvMessage.setTextColor(activity.getResources().getColor(R.color.textColor));
                        holder.tvCount.setVisibility(GONE);
                    }
                }
                new AQuery(activity).id(holder.ivUserImage).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.default_profile);
                userDisable(holder,position,activity);
                setOnClickListener(holder, user, position);
                JSONObject jsonObject = new JSONObject(users.get(position).getUserInfo());
                System.out.println("All Users  "+jsonObject.toString());
            } else if (viewholder instanceof LoadingViewHolder) {
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
                loadingViewHolder.progressBar.setIndeterminate(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void setFilter(List<User> users) {
        this.users = users;
        notifyDataSetChanged();
    }


    public  void NotePopup(final User user,String NoteTitle) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        final View dialogView = ((LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_resume_note, null);
        dialogBuilder.setView(dialogView);
        final EditText resume_note = dialogView.findViewById(R.id.resume_note);
        final TextView cancel = dialogView.findViewById(R.id.cancel);
        final TextView note = dialogView.findViewById(R.id.share_note);
        note.setText(NoteTitle);
        if (!note.getText().toString().equalsIgnoreCase("Send Via Email")){
            note.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent();
                    intent.putExtra(ResponseParameters.UserId,user.getUserId().trim());
                    intent.putExtra(ResponseParameters.NOTE,resume_note.getText().toString().trim());
                    activity.setResult(1,intent);
                    activity.finish();
                }
            });
        }
        final AlertDialog confirmAlert = dialogBuilder.create();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });
        confirmAlert.show();
    }




    public void setOnClickListener(final UserViewHolder holder, final User user, final int position) {
        holder.cvContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fromShareResume){
              NotePopup(user,activity.getResources().getString(R.string.home_share_resume_link));
                }else if(videoShare){
                    if (!SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(user.getUserId().trim())) {
                        try {
                            final Intent intent = new Intent(activity, ChatActivity.class);
                            JSONObject jsonObject = new JSONObject(users.get(position).getUserInfo());
                            jsonObject.put(ResponseParameters.UNREAD_MESSAGE_COUNT, 0);
                            users.get(position).setUserInfo(jsonObject.toString());

                            final String videoId=((MessageActivity)activity).videoId;
                            //Toast.makeText(activity,videoId,Toast.LENGTH_SHORT).show();
                            String shareMessage="Share Video";
                            final String[] message={""};
                            String url=API.base_api+"services/upload_video_limits/"+SharedPreferencesMethod.getUserInfo(activity).getUserId();
                            new AQuery(activity).ajax(url, JSONObject.class,new AjaxCallback<JSONObject>(){
                                @Override
                                public void callback(String url, JSONObject object, AjaxStatus status) {
                                    super.callback(url, object, status);
                                    System.out.println("Response== "+object.toString());
                                    try {
                                        message[0] =object.getString("inside_message_on_video_approved_and_published");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    String shareBody = message[0] + "\n\n"+API.share_post_link+"KotumbVideo/video/"+ videoId+"\n";
                                    intent.putExtra("USER", user);
                                    intent.putExtra("videoData",shareBody);
                                    System.out.println("Chat Profile  "+ user.getUserInfo());
                                    activity.startActivity(intent);
                                    notifyItemChanged(position);
                                }
                            });
                            //String videoData=videoName+"~"+shareMessage+"~"+videoTitle+"~"+videoDesp+"~"+videoId+"~"+videoUserId+"~"+videoLike+"~"+videoDate+"~"+API.user_videos+videoFormate+"~";

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Intent intent = new Intent(activity, CompleteProfileActivity.class);
                        intent.putExtra(ResponseParameters.Id, R.id.ll_personal_details);
                        //intent.putExtra("DATA", new JSONObject().toString());
                        activity.startActivity(intent);
                    }
                }else{
                    if (!SharedPreferencesMethod.getUserId(activity).trim().equalsIgnoreCase(user.getUserId().trim())) {
                        try {
                            Intent intent = new Intent(activity, ChatActivity.class);
                            JSONObject jsonObject = new JSONObject(users.get(position).getUserInfo());
                            jsonObject.put(ResponseParameters.UNREAD_MESSAGE_COUNT, 0);
                            users.get(position).setUserInfo(jsonObject.toString());
                            intent.putExtra("USER", user);
                            System.out.println("Chat Profile  "+ user.getUserInfo());
                            activity.startActivity(intent);
                            notifyItemChanged(position);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Intent intent = new Intent(activity, CompleteProfileActivity.class);
                        intent.putExtra(ResponseParameters.Id, R.id.ll_personal_details);
                        //intent.putExtra("DATA", new JSONObject().toString());
                        activity.startActivity(intent);
                    }
                }

            }

        });


    }







    // Method Created By -Arpit Kanda
    // user Delete Method
    public void userDisable(UserViewHolder holder,int position,Activity activity){
        try{
            JSONObject jsonObject = new JSONObject(users.get(position).getUserInfo());
            JSONObject json2 = jsonObject.getJSONObject("userInfo");
            String test = (String) json2.get("isDeleted");
            if (test.equalsIgnoreCase("1")){
                holder.tvUserName.setText("Kotumb User");
                new AQuery(activity).id(holder.ivUserImage).image(API.imageUrl + "user_removed.png", true, true, 300, R.drawable.default_profile);
            }else{

            }
        }catch(Exception ex){
            ex.printStackTrace();
            System.out.println("Error Name   "+ ex);
        }
    }
    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvTime, tvCount;
        EmojiconTextView tvMessage;
        ImageView ivUserImage;
        CardView cvContainer;

        UserViewHolder(View view) {
            super(view);
            tvUserName = view.findViewById(R.id.tvUserName);
            ivUserImage = view.findViewById(R.id.ivUserImage);
            cvContainer = view.findViewById(R.id.cvContainer);
            tvTime = view.findViewById(R.id.tvTime);
            tvMessage = view.findViewById(R.id.tvMessage);
            tvCount = view.findViewById(R.id.tvCount);
        }
    }
}