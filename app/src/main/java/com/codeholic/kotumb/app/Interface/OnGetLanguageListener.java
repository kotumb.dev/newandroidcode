package com.codeholic.kotumb.app.Interface;

import com.codeholic.kotumb.app.Model.AppLanguage;

import java.util.ArrayList;

public interface OnGetLanguageListener {
    void onGotLanguage(ArrayList<AppLanguage> language);
}