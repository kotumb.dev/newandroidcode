package com.codeholic.kotumb.app.Activity;

import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.codeholic.kotumb.app.Camera2VideoFragment;
import com.codeholic.kotumb.app.R;

public class CameraActivity extends AppCompatActivity {
    public interface StopOnCall{
        public void onCallStop();
    }


    public StopOnCall stopOnCall;
    TelephonyManager mgr;
    PhoneStateListener phoneStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        getFragmentManager().beginTransaction()
                .replace(R.id.container, Camera2VideoFragment.newInstance())
                .commit();


        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    //Incoming call: Pause music
                    if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M) {
                        System.out.println("onstop======11");
                        stopOnCall.onCallStop();
                    }
                } else if(state == TelephonyManager.CALL_STATE_IDLE) {
                    //Not in call: Play music
                } else if(state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    //A call is dialing, active or on hold
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        stopOnCall.onCallStop();
                        System.out.println("onstop======12");
                    }
                }
                super.onCallStateChanged(state, incomingNumber);
            }
        };
        mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if(mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }
    }

}
