package com.codeholic.kotumb.app.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.util.Pair;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Adapter.NotificationRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Interface.OnResponseListener;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.Notification;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.Model.VideoData;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static com.codeholic.kotumb.app.Activity.HomeScreenActivity.setUpAdImage;


public class NotificationFragment extends BaseFragment {


    public static ArrayList<String> notificationToRemove = new ArrayList<>();
    private int index;
    @BindView(R.id.new_notifications)
    TextView new_notifications;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.swipelayout)
    SwipeRefreshLayout swipelayout;

    @BindView(R.id.tvNoNotification)
    TextView tvNoNotification;

    List<Notification> notifications = new ArrayList<>();
    NotificationRecyclerViewAdapter notificationRecyclerViewAdapter;
    int pagination = 0;
    private LinearLayoutManager linearLayoutManager = null;
    User user;
    private BroadcastReceiver receiver;
    Context _context;
    public boolean firstTime = true;
    private int AD_OFFSET = 1;
    private ArrayList<NotificationRecyclerViewAdapter.AdsViewHolder> loadingViewHolders = new ArrayList<>();
    private ArrayList<String> ads = new ArrayList<>();
    private HashMap<Integer, Pair<NotificationRecyclerViewAdapter.AdsViewHolder, JSONObject>> map = new HashMap<>();
    private static String ad_img_url;
    private boolean enableDefaultAds;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.content_notification, container, false);

        ButterKnife.bind(this, view);

//        Collections.shuffle(Utils.globalAds);

        Bundle arguments = getArguments();
        if (arguments != null && arguments.getBoolean("hide")) {
            firstTime = true;
            ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.header_main_menu_option1));
        } else {
            firstTime = false;
            ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.home_notification_title));
        }
        pagination = 0;
        setView();
        setBroadCast();
        Log.d("check", "Notificationfragment opened");


        swipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llNotFound.setVisibility(View.GONE);
                pagination = 0;
                swipelayout.setRefreshing(false);
                notifications.clear();
                String userId = SharedPreferencesMethod.getUserId(getActivity());
                String url = API.NOTIFICATION + userId + "/0";
                API.sendRequestToServerGET_FRAGMENT(getActivity(), NotificationFragment.this, url, API.NOTIFICATION); // service call for user list
            }
        });

        return view;
    }

    //setview for getting user notification
    private void setView() {
        try {
            if (!Utility.isConnectingToInternet(getActivity())) { // checking net connection
                llNotFound.setVisibility(View.VISIBLE);
                rvUserList.setVisibility(GONE);
                llLoading.setVisibility(GONE);
                tvNoNotification.setText(getResources().getString(R.string.app_no_internet_error));
                final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            tvNoNotification.setText(getResources().getString(R.string.no_notification_found));
            String extraString = firstTime ? "?clear=false" : "";
            String userId = SharedPreferencesMethod.getUserId(getActivity());
            String url = API.NOTIFICATION + userId + "/0" + extraString;
            System.out.println("Video URL  "+ url);
            API.sendRequestToServerGET_FRAGMENT(getActivity(),
                    this, url, API.NOTIFICATION); // service call for user list
//            firstTime = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _context = context;
    }


    //getResponse for getting list of notification
    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        Log.e("response", "" + outPut);
        System.out.println("Notification Response    "+outPut);
        try {
            if (i == 0) { // for getting groups
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        JSONArray notificationList = new JSONArray();
                        if (outPut.has(ResponseParameters.NOTIFICATIONS)) {
                            notificationList = outPut.getJSONArray(ResponseParameters.NOTIFICATIONS);
                        }
                        if (outPut.has("noti_ad_offset")) {
                            AD_OFFSET = Integer.parseInt(outPut.getString("noti_ad_offset"));
                        }
                        notifications = new ArrayList<>();
                        for (int j = 0; j < notificationList.length(); j++) {

                            JSONObject result = notificationList.getJSONObject(j);
                            Notification notification = new Notification();
                            User user = new User();
                            if (result.has(ResponseParameters.GROUP_IMG_BASE_URL)) {
                                String url = result.getString(ResponseParameters.GROUP_IMG_BASE_URL);
                                notification.setGroupImageURL(url);
                            }
                            if (result.has(ResponseParameters.GROUP_INFO)) {
                                Group group = new Group();
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = result.getJSONObject(ResponseParameters.GROUP_INFO);
                                    group.groupName = jsonObject.getString(ResponseParameters.GROUP_NAME);
                                    group.groupId = jsonObject.getString(ResponseParameters.GROUP_ID);
                                    group.logo = jsonObject.getString(ResponseParameters.LOGO);
                                } catch (JSONException e) {
                                    try {
                                        group.groupName = result.getString(ResponseParameters.COMMENT);
                                    } catch (JSONException e1) {
                                        group.groupName = "";
                                        e1.printStackTrace();
                                    }
                                    e.printStackTrace();
                                }
                                notification.setGroup(group);
                            }

                            if (result.has(ResponseParameters.SHARE_COUNT)) {
                                notification.setCount(result.getInt(ResponseParameters.SHARE_COUNT));
                            }

                            if (result.has(ResponseParameters.LIKE_COUNT)) {
                                notification.setCount(result.getInt(ResponseParameters.LIKE_COUNT));
                            }

                            if (result.has(ResponseParameters.COMMENT_COUNT)) {
                                notification.setCount(result.getInt(ResponseParameters.COMMENT_COUNT));
                            }

                            if (result.has(ResponseParameters.FIELD_ID)) {
                                notification.setFieldId(result.getInt(ResponseParameters.FIELD_ID));
                            }

                            if (result.has(ResponseParameters.COMMENT)) {
                                notification.setComment(result.getString(ResponseParameters.COMMENT));
                            }
                       /*     if(result.has("approved_video")){
                                VideoData videoData=new VideoData();
                                JSONObject jsonObject = null;
                                try{
                                    jsonObject = result.getJSONObject("approved_video");
                                    System.out.println("Video Notify   "+jsonObject.toString());
                                    videoData.setVideoTitle(jsonObject.getString(ResponseParameters.TITLE));
                                    videoData.setVideoType(jsonObject.getString(ResponseParameters.TYPE));
                                    videoData.setVideoName(jsonObject.getString(ResponseParameters.MEDIA));
                                    videoData.setVideoDescription(jsonObject.getString(ResponseParameters.DESCRIPTION));
                                    videoData.setVideoId(jsonObject.getString(ResponseParameters.Id));
                                    videoData.setVideoType(jsonObject.getString(ResponseParameters.MEDIA_TYPE));
                                    notification.setVideoData(videoData);
                                }catch (Exception ex){
                                    ex.printStackTrace();
                                }

                            }*/
//                            if(result.has(ResponseParameters.VIDEO_APPROVE)){
//                                VideoData videoData=new VideoData();
//                                JSONObject jsonObject = null;
//                                try{
//                                    jsonObject = result.getJSONObject(ResponseParameters.VIDEO_APPROVE);
//                                    videoData.setVideoTitle(jsonObject.getString(ResponseParameters.TITLE));
//                                    videoData.setVideoType(jsonObject.getString(ResponseParameters.TYPE));
//                                    videoData.setVideoName(jsonObject.getString(ResponseParameters.MEDIA));
//                                    videoData.setVideoDescription(jsonObject.getString(ResponseParameters.DESCRIPTION));
//                                    videoData.setVideoId(jsonObject.getString(ResponseParameters.Id));
//                                }catch (Exception ex){
//                                    ex.printStackTrace();
//                                    videoData=null;
//                                }
//                                notification.setVideoData(videoData);
//
//                            }
                            if (result.has(ResponseParameters.POST_INFO)) {
                                Post post = new Post();
                                JSONObject jsonObject = null;
                                if (result.has(ResponseParameters.POST_INFO)){
                                    try {
                                        jsonObject = result.getJSONObject(ResponseParameters.POST_INFO);
                                        System.out.println("Video Post Response  "+jsonObject.toString());
                                        post.post_id = jsonObject.getString(ResponseParameters.POST_ID);
                                        post.firstName = jsonObject.getString(ResponseParameters.FirstName);
                                        post.middleName = jsonObject.getString(ResponseParameters.MiddleName);
                                        post.lastName = jsonObject.getString(ResponseParameters.LastName);
                                        post.userId = jsonObject.getString(ResponseParameters.UserId);
                                        post.title = jsonObject.getString(ResponseParameters.TITLE);
                                        post.content = jsonObject.getString("content");
                                        post.image = jsonObject.getString(ResponseParameters.IMAGE);
                                        post.postType = jsonObject.getString("postType");
                                        post.sharedPostId = jsonObject.getString("sharedPostId");
                                        post.sharedPostUserId = jsonObject.getString("sharedPostUserId");
                                        post.sharedPostGroupId = jsonObject.getString("sharedPostGroupId");
//                                    post.sharedPostUserName = jsonObject.getString(ResponseParameters.LastName);
                                        post.profileHeadline = jsonObject.getString(ResponseParameters.PROFILE_HEADLINE);
                                        post.createdAt = jsonObject.getString("createdAt");
                                        post.updatedAt = jsonObject.getString("updatedAt");
                                        post.group_id = jsonObject.getString(ResponseParameters.GROUPID);
                                        post.avatar = jsonObject.getString(ResponseParameters.Avatar);
                                        post.total_likes = jsonObject.getInt("total_likes");
                                        post.total_comments = jsonObject.getInt("total_comments");
                                        post.duration = jsonObject.getString("duration");
                                    } catch (JSONException e) {
                                        post = null;
                                        e.printStackTrace();
                                    }
                                    notification.setPost(post);
                                }

                            }

                            user.setFirstName(result.getString(ResponseParameters.FirstName));
                            if (result.has(ResponseParameters.MiddleName)) {
                                user.setMiddleName(result.getString(ResponseParameters.MiddleName));
                            }
                            user.setLastName(result.getString(ResponseParameters.LastName));
                            user.setAvatar(result.getString(ResponseParameters.Avatar));

                            notification.setAvatar(result.getString(ResponseParameters.Avatar));
                            System.out.println("name==="+result.getString(ResponseParameters.FirstName)+"=="+result.getString(ResponseParameters.Avatar));
                            notification.setFullname(result.getString(ResponseParameters.FirstName)+" "+result.getString(ResponseParameters.LastName));
                            user.setUserId(result.getString(ResponseParameters.UserId));
                            user.setProfileHeadline(result.getString(ResponseParameters.PROFILE_HEADLINE));
                            user.setProfileSummary(result.getString(ResponseParameters.PROFILE_SUMMERY));
                            //user.setAadhaarInfo(result.getString(ResponseParameters.AADHARINFO));
                            user.setCity(result.getString(ResponseParameters.City));
                            user.setState(result.getString(ResponseParameters.State));
                            notification.setStatus(result.getString(ResponseParameters.STATUS));
                            user.setUserInfo(result.toString());
                            notification.setUser(user);
                            notification.setId(result.getString(ResponseParameters.Id));
                            notification.setNotificationTime(result.getString(ResponseParameters.ACTIVITY_TIME));
                            notification.setNotificationId(result.getString(ResponseParameters.Id));
                            notification.setNotificationType(result.getString(ResponseParameters.ACTIVITY_TYPE));
                            if(result.getString(ResponseParameters.ACTIVITY_TYPE).equals(ResponseParameters.GROUP_POST_LIKE)){
                                System.out.println("message=="+result.getString("message"));
                                notification.setMessage(result.getString("message"));
                            }
                            if(result.getString(ResponseParameters.ACTIVITY_TYPE).equals("publishedvideonotification")){
                                if(result.has("like_status")) {
                                    System.out.println("found==");
                                    notification.setLike(result.getString("like_status"));
                                    notification.setLikeCount(result.getString("like_count"));
                                }
                                if(result.has("thumbnail"))
                                notification.setThumbnail(result.getJSONObject("publish_video").getString("thumbnail"));
                                notification.setTitle(result.getJSONObject("publish_video").getString("title"));
                                notification.setMessage(outPut.getString("owner_video_published_notification"));
                                user.setFirstName(result.getJSONObject("publish_video_userInfo").getString("firstName"));
                                user.setLastName(result.getJSONObject("publish_video_userInfo").getString("lastName"));
                                user.setAvatar(result.getJSONObject("publish_video_userInfo").getString("avatar"));
                            }
                           // System.out.println("check==1213=="+result.getString("like_status"));
                            if(result.getString(ResponseParameters.ACTIVITY_TYPE).equals("videoapproved")){

                                if(result.has("like_status")) {
                                    System.out.println("found===");
                                    notification.setLike(result.getString("like_status"));
                                }
                                notification.setIsPublished(result.getJSONObject("approved_video").getString("isPublished"));
                                notification.setMessage(outPut.getString("video_approved_message"));
                                notification.setTitle(result.getJSONObject("approved_video").getString("title"));
                                notification.setThumbnail(result.getJSONObject("approved_video").getString("thumbnail"));
                                user.setFirstName(result.getJSONObject("approved_video_userInfo").getString("firstName"));
                                user.setLastName(result.getJSONObject("approved_video_userInfo").getString("lastName"));
                                user.setAvatar(result.getJSONObject("approved_video_userInfo").getString("avatar"));
                            }

                            if(result.getString(ResponseParameters.ACTIVITY_TYPE).equals("videorejected")){
                                notification.setMessage(outPut.getString("video_reject_message"));
                            }
                            if((result.getString(ResponseParameters.ACTIVITY_TYPE).equals("newvideonotification"))){
                                notification.setMessage(outPut.getString("all_user_video_published_notification"));
                            }
                            if((result.getString(ResponseParameters.ACTIVITY_TYPE).equals(ResponseParameters.VIDEO_LIKES))){
                                if(result.has("like_status")) {
                                    System.out.println("found==");
                                    notification.setLike(result.getString("like_status"));
                                    notification.setLikeCount(result.getString("video_like_count"));
                                }
                                System.out.println("testing=="+result.getJSONObject("videolikes").getString("thumbnail"));
                                notification.setThumbnail(result.getJSONObject("videolikes").getString("thumbnail"));
                                notification.setTitle(result.getJSONObject("videolikes").getString("title"));
                                if(result.has("video_like_message")) {
                                    notification.setMessage(result.getString("video_like_message"));
                                }
                                user.setFirstName(result.getJSONObject("video_userInfo").getString("firstName"));
                                user.setLastName(result.getJSONObject("video_userInfo").getString("lastName"));
                                user.setAvatar(result.getJSONObject("video_userInfo").getString("avatar"));
                            }
                            notification.setFieldId(result.getInt("field_id"));
                            //notification.setLike(result.getString("like_status"));
                            notification.setVideo_date_time(result.getString("activityTime"));
                            notification.setNotificationDes(result.getString(ResponseParameters.DESCRIPTION));
                            notification.setMedia(result.getString(ResponseParameters.MEDIA));
                            notification.setMediaType(result.getString(ResponseParameters.MEDIA_TYPE));
                            notification.setNotifyUserFirstName(result.getString(ResponseParameters.FirstName));
                            notification.setNotifyUserLasrName(result.getString(ResponseParameters.LastName));
                            notifications.add(notification);
                            int lastAdIndex = getLastAdIndex();
                            if ((notifications.size()) - lastAdIndex > AD_OFFSET) {
                                notifications.add((new Notification())); //null values for ads
                            }
                        }

//                        List<Notification> listedNotifications = removeBlackListedNotifications(this.notifications);
//                        this.notifications.clear();
//                        this.notifications.addAll(listedNotifications);

                        //setting options for recycler adapter
                        linearLayoutManager = new LinearLayoutManager(getActivity());
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserList.setLayoutManager(linearLayoutManager);
                        llNotFound.setVisibility(GONE);
                        notificationRecyclerViewAdapter = new NotificationRecyclerViewAdapter(rvUserList, getActivity(), NotificationFragment.this, this.notifications);
                        rvUserList.setAdapter(notificationRecyclerViewAdapter);
                        rvUserList.setVisibility(View.VISIBLE);
                        //load more setup for recycler adapter
                        notificationRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                if (NotificationFragment.this.notifications.size() >= 8) {
                                    rvUserList.post(new Runnable() {
                                        public void run() {
                                            NotificationFragment.this.notifications.add(null);
                                            notificationRecyclerViewAdapter.notifyItemInserted(NotificationFragment.this.notifications.size() - 1);
                                        }
                                    });
                                    String extraString = firstTime ? "?clear=false" : "";
                                    String url = API.NOTIFICATION + SharedPreferencesMethod.getUserId(getActivity()) + "/" + (pagination + 8) + extraString;
                                    API.sendRequestToServerGET_FRAGMENT(getActivity(), NotificationFragment.this, url, API.NOTIFICATION_UPDATE); // service call for user list
                                }
                            }
                        });

                        notificationRecyclerViewAdapter.setRefreshListener(getRefreshListener());

                    } else {
                        //no userfound
                        if(notifications.size()==0) {
                            llNotFound.setVisibility(View.VISIBLE);
                            rvUserList.setVisibility(GONE);
                        }
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    llNotFound.setVisibility(View.VISIBLE);
                    rvUserList.setVisibility(GONE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    llNotFound.setVisibility(View.VISIBLE);
                    rvUserList.setVisibility(GONE);
                }
            } else if (i == 1) {// for updating list
                notifications.remove(notifications.size() - 1); // removing of loading item
                notificationRecyclerViewAdapter.notifyItemRemoved(notifications.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response

                        JSONArray notificationList = new JSONArray();
                        if (outPut.has(ResponseParameters.NOTIFICATIONS)) {
                            notificationList = outPut.getJSONArray(ResponseParameters.NOTIFICATIONS);
                            for (int j = 0; j < notificationList.length(); j++) {
                                JSONObject results = notificationList.getJSONObject(j);
                                Notification notification = getNotification(results,outPut);
                                notifications.add(notification);
                                int lastAdIndex = getLastAdIndex();
                                if ((notifications.size()) - lastAdIndex > AD_OFFSET) {
                                    notifications.add(new Notification()); //null values for ads
                                }
                            }
//                            List<Notification> listedNotifications = removeBlackListedNotifications(notifications);
//                            notifications.clear();
//                            notifications.addAll(listedNotifications);
                            pagination = pagination + 8;
                            //updating recycler view
                            notificationRecyclerViewAdapter.notifyDataSetChanged();
                            notificationRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            } else if (i >= 100) {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { //check output have success
                        if (outPut.has(ResponseParameters.AD_IMAGE_BASE_URL)) {
                            API.imageAdUrl = outPut.getString(ResponseParameters.AD_IMAGE_BASE_URL);
                            System.out.println("Get AD    "+outPut.toString());
                            ads.add(outPut.toString());
                            NotificationRecyclerViewAdapter.AdsViewHolder adsViewHolder = loadingViewHolders.get(0);
                            setUpAdImage(outPut.toString(), adsViewHolder, getActivity());
                        }
                    }
                }
            }
        } catch (Exception e) {
            //mo user found
            if(notifications.size()==0)
            llNotFound.setVisibility(View.VISIBLE);
            e.printStackTrace();
            Log.d("ERROR", outPut + "");
        }
    }

    private List<Notification> removeBlackListedNotifications(List<Notification> notifications) {
        ArrayList<Integer> pos = new ArrayList<>();
        int count = 0;
        for (Notification notification : notifications) {
            for (String s : notificationToRemove) {
                if (notification.getId().equalsIgnoreCase(s)) {
                    pos.add(count);
                }
            }
            count++;
        }

        for (Integer integer : pos) {
            notifications.remove((int) integer);
        }

        return notifications;
    }

    private int getLastAdIndex() {
        for (int i = notifications.size() - 1; i > 0; i--) {
            if (notifications.get(i) != null && notifications.get(i).getId().isEmpty()) {
                return i;
            }
        }
        return -1;
    }

    @NonNull
    private OnResponseListener getRefreshListener() {
        return new OnResponseListener() {
            @Override
            public void onGotResponse(JSONObject response) {
                setView();
            }
        };
    }

    @NonNull
    private Notification getNotification(JSONObject result , JSONObject outPut) throws JSONException {
        Notification notification = new Notification();
        User user = new User();

        if (result.has(ResponseParameters.GROUP_IMG_BASE_URL)) {
            String url = result.getString(ResponseParameters.GROUP_IMG_BASE_URL);
            notification.setGroupImageURL(url);
        }
        if (result.has(ResponseParameters.GROUP_INFO)) {
            Group group = new Group();
            JSONObject jsonObject = null;
            try {
                jsonObject = result.getJSONObject(ResponseParameters.GROUP_INFO);
                group.groupName = jsonObject.getString(ResponseParameters.GROUP_NAME);
                group.groupId = jsonObject.getString(ResponseParameters.GROUP_ID);
                group.logo = jsonObject.getString(ResponseParameters.LOGO);
            } catch (JSONException e) {
                try {
                    group.groupName = result.getString(ResponseParameters.COMMENT);
                } catch (JSONException e1) {
                    group.groupName = "";
                    e1.printStackTrace();
                }
                e.printStackTrace();
            }
            notification.setGroup(group);
        }

        if (result.has(ResponseParameters.SHARE_COUNT)) {
            notification.setCount(result.getInt(ResponseParameters.SHARE_COUNT));
        }

        if (result.has(ResponseParameters.LIKE_COUNT)) {
            notification.setCount(result.getInt(ResponseParameters.LIKE_COUNT));
        }

        if (result.has(ResponseParameters.COMMENT_COUNT)) {
            notification.setCount(result.getInt(ResponseParameters.COMMENT_COUNT));
        }

        if (result.has(ResponseParameters.FIELD_ID)) {
            notification.setFieldId(result.getInt(ResponseParameters.FIELD_ID));
        }

        if (result.has(ResponseParameters.COMMENT)) {
            notification.setComment(result.getString(ResponseParameters.COMMENT));
        }

        if (result.has(ResponseParameters.POST_INFO)) {
            Post post = new Post();
            JSONObject jsonObject = null;
            try {
                jsonObject = result.getJSONObject(ResponseParameters.POST_INFO);
                System.out.println("Video Post Response  "+jsonObject.toString());
                post.post_id = jsonObject.getString(ResponseParameters.POST_ID);
                post.firstName = jsonObject.getString(ResponseParameters.FirstName);
                post.middleName = jsonObject.getString(ResponseParameters.MiddleName);
                post.lastName = jsonObject.getString(ResponseParameters.LastName);
                post.userId = jsonObject.getString(ResponseParameters.UserId);
                post.title = jsonObject.getString(ResponseParameters.TITLE);
                post.content = jsonObject.getString("content");
                post.image = jsonObject.getString(ResponseParameters.IMAGE);
                post.postType = jsonObject.getString("postType");
                post.sharedPostId = jsonObject.getString("sharedPostId");
                post.sharedPostUserId = jsonObject.getString("sharedPostUserId");
                post.sharedPostGroupId = jsonObject.getString("sharedPostGroupId");
//                                    post.sharedPostUserName = jsonObject.getString(ResponseParameters.LastName);
                post.profileHeadline = jsonObject.getString(ResponseParameters.PROFILE_HEADLINE);
                post.createdAt = jsonObject.getString("createdAt");
                post.updatedAt = jsonObject.getString("updatedAt");
                post.group_id = jsonObject.getString(ResponseParameters.GROUPID);
                post.avatar = jsonObject.getString(ResponseParameters.Avatar);
                post.total_likes = jsonObject.getInt("total_likes");
                post.total_comments = jsonObject.getInt("total_comments");
                post.duration = jsonObject.getString("duration");
            } catch (JSONException e) {
                post = null;
                e.printStackTrace();
            }
            notification.setPost(post);
        }

        user.setFirstName(result.getString(ResponseParameters.FirstName));
        if (result.has(ResponseParameters.MiddleName)) {
            user.setMiddleName(result.getString(ResponseParameters.MiddleName));
        }
        user.setLastName(result.getString(ResponseParameters.LastName));
        user.setAvatar(result.getString(ResponseParameters.Avatar));
        notification.setAvatar(result.getString(ResponseParameters.Avatar));
        System.out.println("name==="+result.getString(ResponseParameters.FirstName)+"=="+result.getString(ResponseParameters.Avatar));
        notification.setFullname(result.getString(ResponseParameters.FirstName)+" "+result.getString(ResponseParameters.LastName));
        user.setUserId(result.getString(ResponseParameters.UserId));
        user.setProfileHeadline(result.getString(ResponseParameters.PROFILE_HEADLINE));
        user.setProfileSummary(result.getString(ResponseParameters.PROFILE_SUMMERY));
        //user.setAadhaarInfo(result.getString(ResponseParameters.AADHARINFO));
        user.setCity(result.getString(ResponseParameters.City));
        user.setState(result.getString(ResponseParameters.State));
        notification.setStatus(result.getString(ResponseParameters.STATUS));
        user.setUserInfo(result.toString());
        notification.setUser(user);
        notification.setId(result.getString(ResponseParameters.Id));
        notification.setNotificationTime(result.getString(ResponseParameters.ACTIVITY_TIME));
        notification.setNotificationId(result.getString(ResponseParameters.Id));
        notification.setNotificationType(result.getString(ResponseParameters.ACTIVITY_TYPE));
        if(result.getString(ResponseParameters.ACTIVITY_TYPE).equals(ResponseParameters.GROUP_POST_LIKE)){
            System.out.println("message=="+result.getString("message"));
            notification.setMessage(result.getString("message"));
        }
        if(result.getString(ResponseParameters.ACTIVITY_TYPE).equals("publishedvideonotification")){
            if(result.has("like_status")) {
                System.out.println("found==");
                notification.setLike(result.getString("like_status"));
                notification.setLikeCount(result.getString("like_count"));
            }
            notification.setThumbnail(result.getJSONObject("publish_video").getString("thumbnail"));
            notification.setTitle(result.getJSONObject("publish_video").getString("title"));
            notification.setMessage(outPut.getString("owner_video_published_notification"));
            user.setFirstName(result.getJSONObject("publish_video_userInfo").getString("firstName"));
            user.setLastName(result.getJSONObject("publish_video_userInfo").getString("lastName"));
            user.setAvatar(result.getJSONObject("publish_video_userInfo").getString("avatar"));
        }

        if(result.getString(ResponseParameters.ACTIVITY_TYPE).equals("videorejected")){
            notification.setMessage(outPut.getString("video_reject_message"));
        }
        // System.out.println("check==1213=="+type);
        if(result.getString(ResponseParameters.ACTIVITY_TYPE).equals("videoapproved")){

            if(result.has("like_status")) {
                System.out.println("found===");
                notification.setLike(result.getString("like_status"));
            }
            notification.setMessage(outPut.getString("video_approved_message"));
            if(result.has("thumbnail"))
                notification.setThumbnail(result.getJSONObject("approved_video").getString("thumbnail"));
            notification.setTitle(result.getJSONObject("approved_video").getString("title"));
            user.setFirstName(result.getJSONObject("approved_video_userInfo").getString("firstName"));
            user.setLastName(result.getJSONObject("approved_video_userInfo").getString("lastName"));
            user.setAvatar(result.getJSONObject("approved_video_userInfo").getString("avatar"));
        }
        if((result.getString(ResponseParameters.ACTIVITY_TYPE).equals("newvideonotification"))){
            notification.setMessage(outPut.getString("all_user_video_published_notification"));
        }
        if((result.getString(ResponseParameters.ACTIVITY_TYPE).equals(ResponseParameters.VIDEO_LIKES))){

            if(result.has("like_status")) {
                System.out.println("found==");
                notification.setLike(result.getString("like_status"));
                notification.setLikeCount(result.getString("video_like_count"));
            }
            notification.setThumbnail(result.getJSONObject("videolikes").getString("thumbnail"));
            notification.setTitle(result.getJSONObject("videolikes").getString("title"));
            if(result.has("video_like_message"))
                notification.setMessage(result.getString("video_like_message"));
            user.setFirstName(result.getJSONObject("video_userInfo").getString("firstName"));
            user.setLastName(result.getJSONObject("video_userInfo").getString("lastName"));
            user.setAvatar(result.getJSONObject("video_userInfo").getString("avatar"));
        }

        notification.setFieldId(result.getInt("field_id"));
        //notification.setLike(result.getString("like_status"));
        notification.setVideo_date_time(result.getString("activityTime"));
        notification.setNotificationDes(result.getString(ResponseParameters.DESCRIPTION));
        notification.setMedia(result.getString(ResponseParameters.MEDIA));
        notification.setMediaType(result.getString(ResponseParameters.MEDIA_TYPE));
        notification.setNotifyUserFirstName(result.getString(ResponseParameters.FirstName));
        notification.setNotifyUserLasrName(result.getString(ResponseParameters.LastName));
        //notifications.add(notification);
       // int lastAdIndex = getLastAdIndex();
       /* if (result.has("groupInfo")) {
            try {
                notification.setGroup(getGroupInfo(result.getJSONObject("groupInfo")));
                notification.setComment(notification.getGroup().groupName);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("ERROR", result + "");
            }
        }
        if (result.has("postInfo")) {
            try {
                notification.setPost(getPostInfo(result.getJSONObject("postInfo")));
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("ERROR", result + "");
            }
        }*/

      /*  if (results.has("approved_video")) {
            try {
                notification.setVideoData(getVideoInfo(results.getJSONObject("approved_video")));
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("ERROR", results + "");
            }
        }if (results.has("approved_video")) {
            try {
                notification.setVideoData(getVideoInfo(results.getJSONObject("approved_video")));
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("ERROR", results + "");
            }
        }*/
        return notification;
    }

//
    private VideoData getVideoInfo(JSONObject video) throws JSONException{
        VideoData videoData=new VideoData();
        videoData.setVideoTitle(video.getString(ResponseParameters.TITLE));
        videoData.setVideoType(video.getString(ResponseParameters.TYPE));
        videoData.setVideoName(video.getString(ResponseParameters.MEDIA));
        videoData.setVideoDescription(video.getString(ResponseParameters.DESCRIPTION));
        videoData.setVideoId(video.getString(ResponseParameters.Id));
        return videoData;
    }

    private Post getPostInfo(JSONObject postInfo) throws JSONException {
        Post post = new Post();
        post.post_id = postInfo.getString("post_id");
        post.group_id = postInfo.getString("group_id");
        post.userId = postInfo.getString("userId");
        post.title = postInfo.getString("title");
        post.content = postInfo.getString("content");
        post.image = postInfo.getString("image");
        post.postType = postInfo.getString("postType");
        post.sharedPostId = postInfo.getString("sharedPostId");
        post.sharedPostGroupId = postInfo.getString("sharedPostGroupId");
        post.sharedPostUserId = postInfo.getString("sharedPostUserId");
        post.createdAt = postInfo.getString("createdAt");
        post.updatedAt = postInfo.getString("updatedAt");
        post.firstName = postInfo.getString("firstName");
        if (postInfo.has("middleName")) {
            post.middleName = postInfo.getString("middleName");
        }
        post.lastName = postInfo.getString("lastName");
        post.avatar = postInfo.getString("avatar");
        post.profileHeadline = postInfo.getString("profileHeadline");
        post.duration = postInfo.getString("duration");
        post.total_likes = postInfo.getInt("total_likes");
        post.total_comments = postInfo.getInt("total_comments");
        return post;
    }

    private Group getGroupInfo(JSONObject jsonObject) throws JSONException {
        Group group = new Group();
        group.groupId = jsonObject.getString("groupId");
        group.groupName = jsonObject.getString("groupName");
        group.userId = jsonObject.getString("userId");
        group.groupSummary = jsonObject.getString("groupSummary");
        group.logo = jsonObject.getString("logo");
        group.category = jsonObject.getString("category");
        group.delete_status = jsonObject.getString("delete_status");
        group.owner_type = jsonObject.getString("owner_type");
        group.privacy = jsonObject.getString("privacy");
        group.createdAt = jsonObject.getString("createdAt");
        group.updatedAt = jsonObject.getString("updatedAt");
        group.category_name = jsonObject.getString("category_name");
        group.admins_count = jsonObject.getInt("admins_count");
        group.members_count = jsonObject.getInt("members_count");
        return group;
    }

    private void setBroadCast() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //do something based on the intent's action
                Log.e("BroadcastReceiver", "BroadcastReceiver");
               /* intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_INVITATION)
                        || intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_REQUESTED)
                        || intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_REQUEST_ACCEPTED)*/
               if(intent.hasExtra("refresh")){
                   llNotFound.setVisibility(View.GONE);
                   pagination = 0;
                   notifications.clear();
                   rvUserList.refreshDrawableState();
                   rvUserList.invalidate();
                   notificationRecyclerViewAdapter.notifyDataSetChanged();
                   String userId = SharedPreferencesMethod.getUserId(getActivity());
                   String url = API.NOTIFICATION + userId + "/0";
                   API.sendRequestToServerGET_FRAGMENT(getActivity(), NotificationFragment.this, url, API.NOTIFICATION);
               }
                else if (false) {
                    Notification notification = intent.getParcelableExtra(ResponseParameters.NOTIFICATIONS);
                    notifications.add(0, notification);
                    if (linearLayoutManager == null) {
                        linearLayoutManager = new LinearLayoutManager(getActivity());
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserList.setLayoutManager(linearLayoutManager);
                        notificationRecyclerViewAdapter = new NotificationRecyclerViewAdapter(rvUserList, getActivity(), NotificationFragment.this, notifications);
                        rvUserList.setAdapter(notificationRecyclerViewAdapter);
                        rvUserList.setVisibility(View.VISIBLE);
                        //load more setup for recycler adapter
                        notificationRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                                if (notifications.size() >= 8) {
                                    rvUserList.post(new Runnable() {
                                        public void run() {
                                            notifications.add(null);
                                            notificationRecyclerViewAdapter.notifyItemInserted(notifications.size() - 1);
                                        }
                                    });
                                    System.out.println("Notification API +Notification Update  "+API.NOTIFICATION + SharedPreferencesMethod.getUserId(getActivity()) + "/" + (pagination + 8)+API.NOTIFICATION_UPDATE);
                                    API.sendRequestToServerGET_FRAGMENT(getActivity(), NotificationFragment.this, API.NOTIFICATION + SharedPreferencesMethod.getUserId(getActivity()) + "/" + (pagination + 8), API.NOTIFICATION_UPDATE); // service call for user list
                                }
                            }
                        });
                        notificationRecyclerViewAdapter.setRefreshListener(getRefreshListener());
                    } else {
                        notificationRecyclerViewAdapter.notifyItemRangeInserted(0, 1);
                        notificationRecyclerViewAdapter.notifyItemRangeChanged(0, notifications.size());
                    }
                    llNotFound.setVisibility(GONE);
                } else if (intent.getAction().trim().equalsIgnoreCase(ResponseParameters.GROUP_MEMBER_DELETED)) {
                    String notificationId = intent.getStringExtra(ResponseParameters.Id);
                    for (int i = 0; i < notifications.size(); i++) {
                        if (notifications.get(i).getNotificationId().trim().equalsIgnoreCase(notificationId.trim())) {
                            boolean b = notifications.get(i).getNotificationType().equalsIgnoreCase(ResponseParameters.GROUP_INVITATION);
                            if (b) {
                                notifications.remove(i);
                            }
                            notificationRecyclerViewAdapter.notifyItemRemoved(i);
                            notificationRecyclerViewAdapter.notifyItemRangeChanged(0, notifications.size());
                            break;
                        }
                    }
                } else if (intent.getAction().equalsIgnoreCase(ResponseParameters.NOTIFICATIONS)) {
                    showNewNotificationsBubble(true);
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.GROUP_INVITATION);
        filter.addAction(ResponseParameters.NOTIFICATIONS);
        filter.addAction(ResponseParameters.GROUP_REQUESTED);
        filter.addAction(ResponseParameters.GROUP_REQUEST_ACCEPTED);
        filter.addAction(ResponseParameters.GROUP_MEMBER_DELETED); // for removing notification item
        _context.registerReceiver(receiver, filter);
    }

    public void showNewNotificationsBubble(boolean b) {
        if (b) {
            new_notifications.setVisibility(View.VISIBLE);
            new_notifications.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rvUserList.smoothScrollToPosition(0);
                    pagination = 0;
                    swipelayout.setRefreshing(false);
                    API.sendRequestToServerGET_FRAGMENT(getActivity(), NotificationFragment.this,
                            API.NOTIFICATION + SharedPreferencesMethod.getUserId(getActivity()) + "/0",
                            API.NOTIFICATION); // service call for user list
                    new_notifications.setVisibility(GONE);
                }
            });
        } else {
            new_notifications.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        Log.e("onDestroyView", "onDestroyView");
        if (receiver != null) {
            _context.unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroyView();
    }


    public void loadAdForThisView(final NotificationRecyclerViewAdapter.AdsViewHolder loadingViewHolder, final int position) {
        if (!map.containsKey(position)) {
            if (Utils.globalAds.size() > 0) {
                try {
                    setupNewAd(getAUniqueAd(), position, loadingViewHolder, ad_img_url);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (!enableDefaultAds) {
                    fetchNewAds(loadingViewHolder, position);
                } else {
                    //setupDefaultAd(loadingViewHolder);
                }
            }
        } else {
            try {
                final Pair<NotificationRecyclerViewAdapter.AdsViewHolder, JSONObject> pair = map.get(position);
                NotificationRecyclerViewAdapter.AdsViewHolder adsViewHolder = pair.first;
                TextView header = loadingViewHolder.itemView.findViewById(R.id.header);
                TextView desc = loadingViewHolder.itemView.findViewById(R.id.desc);
                JSONObject ad = pair.second.getJSONObject("ad");
                header.setText(ad.getString("title"));
                desc.setText(ad.getString("description"));
                loadingViewHolder.adView.setImageDrawable(adsViewHolder.adView.getDrawable());
                loadingViewHolder.rlAdView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println("click-----------------");
                        pair.first.itemView.performClick();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                loadingViewHolder.rlAdView.setVisibility(GONE);
            }
        }


    }

    private void fetchNewAds(final NotificationRecyclerViewAdapter.AdsViewHolder loadingViewHolder, final int position) {
        String url = API.GET_ADS + SharedPreferencesMethod.getUserId(getActivity()) + "?ad_ids=";//+ array;
        try {
            AQuery aq = new AQuery(getActivity());
            //Log.d("Url ", url);
            aq.ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {
                        if (json != null) {
                            Log.e(getActivity().getClass().getName(), json.toString());
                            json.put("RP_MESSAGE", "ALL_OKAY");

                            if (json.has("ads")){
                                JSONArray ads = json.getJSONArray("ads");
                                for (int i = 0; i < ads.length(); i++) {
                                    if (!contains(ads.getJSONObject(i), Utils.globalAds)) {
                                        Utils.globalAds.add(ads.getJSONObject(i));
                                    }
                                }
                                JSONObject uniqueAd = getAUniqueAd();
                                ad_img_url = json.getString("ad_img_url");
                                setupNewAd(uniqueAd, position, loadingViewHolder, ad_img_url);
                            }



                            //getResponse(json);
                        } else {
                            JSONObject output = new JSONObject();
                            if (status.getCode() == AjaxStatus.NETWORK_ERROR) {
                                output.put("RP_MESSAGE", "NO NETWORK");
                            } else {
                                output.put("RP_MESSAGE", "ERROR");
                            }
                            // getResponse(output);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        enableDefaultAds = false;
                        // getResponse(new JSONObject());
                        setupDefaultAd(loadingViewHolder);

                    }
                }
            }.method(AQuery.METHOD_GET).header("Auth-Key", "KOTUMB__AuTh_keY"));


        } catch (Exception e) {
            e.printStackTrace();
            enableDefaultAds = false;
            setupDefaultAd(loadingViewHolder);
            //getResponse(new JSONObject());
        }
    }

    private void setupDefaultAd(NotificationRecyclerViewAdapter.AdsViewHolder loadingViewHolder) {
//        loadingViewHolder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0,0));
        loadingViewHolder.adView.setImageResource(R.drawable.ads_1);
        View itemView = loadingViewHolder.itemView;
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String url = API.defaultAdUrl;
//                Utility.prepareCustomTab(getActivity(), url);
//                Bundle bundle = new Bundle();
//                bundle.putString("UserId", SharedPreferencesMethod.getUserId(getContext()));
//                bundle.putString("type", "Default");
//                Utils.logEvent(EventNames.AD_CLICK, bundle, getContext());
            }
        });

        TextView header = itemView.findViewById(R.id.header);
        TextView desc = itemView.findViewById(R.id.desc);
        header.setText("Advertise with Kotumb");
        desc.setText("Contact us for more information");

//        loadingViewHolder.itemView.getLayoutParams().height=0;
//        loadingViewHolder.itemView.requestLayout();
    }

    private void setupNewAd(JSONObject uniqueAd, int position, NotificationRecyclerViewAdapter.AdsViewHolder loadingViewHolder, String adUrl) throws JSONException {
        JSONObject adJSON = new JSONObject();
        adJSON.put("success", "success");
        adJSON.put("reset_ad", "false");
        adJSON.put("ad_img_url", adUrl);
        adJSON.put("ad", uniqueAd);

        map.put(position, new Pair<>(loadingViewHolder, adJSON));
        setUpAdImage(adJSON.toString(), loadingViewHolder, getActivity());
    }

    public JSONObject getAUniqueAd() {
        if (index >= Utils.globalAds.size()) {
            index = 0;
        }
        JSONObject jsonObject = Utils.globalAds.get(index);
        index++;
        return jsonObject;
    }

    private boolean contains(JSONObject jsonObject, ArrayList<JSONObject> globalAds) {

        for (JSONObject jsonObject1 : globalAds) {
            try {
                if (jsonObject.getString("id").equalsIgnoreCase(jsonObject1.getString("id"))) {
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
