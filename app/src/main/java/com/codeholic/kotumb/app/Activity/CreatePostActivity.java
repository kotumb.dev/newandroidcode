package com.codeholic.kotumb.app.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import okhttp3.RequestBody;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Model.Group;
import com.codeholic.kotumb.app.Model.Post;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.EventNames;
import com.codeholic.kotumb.app.Utility.PermissionsUtils;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.google.gson.Gson;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.BaseActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.NormalFile;
//import com.vincent.filepicker.Constant;
//import com.vincent.filepicker.activity.NormalFilePickActivity;
//import com.vincent.filepicker.filter.entity.NormalFile;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;

//import static com.vincent.filepicker.activity.BaseActivity.IS_NEED_FOLDER_LIST;

public class CreatePostActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int REQUEST_IMAGES = 6810;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.header)
    TextView header;
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    @BindView(R.id.tvGroupName)
    TextView tvGroupName;
    @BindView(R.id.llUploadPhoto)
    LinearLayout llUploadPhoto;
    @BindView(R.id.llUploadPDF)
    LinearLayout llUploadPDF;
    @BindView(R.id.llPhoto)
    LinearLayout llPhoto;
    @BindView(R.id.tvViewPhoto)
    TextView tvViewPhoto;
    @BindView(R.id.ivGroupImage)
    ImageView ivGroupImage;
    @BindView(R.id.etPostLayout)
    TextInputLayout etPostLayout;
    @BindView(R.id.etPost)
    EditText etPost;
    @BindView(R.id.ivImageOne)
    ImageView ivImageOne;
    @BindView(R.id.ivImageTwo)
    ImageView ivImageTwo;
    @BindView(R.id.ivImageThree)
    ImageView ivImageThree;
    @BindView(R.id.ivImageFour)
    ImageView ivImageFour;
    @BindView(R.id.ivImageFive)
    ImageView ivImageFive;
    @BindView(R.id.ivImageSingle)
    ImageView ivImageSingle;
    @BindView(R.id.llSquareContainer)
    LinearLayout llSquareContainer;
    @BindView(R.id.llImageContainer)
    LinearLayout llImageContainer;
    @BindView(R.id.pdf_image_container)
    LinearLayout pdf_image_container;
    @BindView(R.id.tvPhotoNumber)
    TextView tvPhotoNumber;
    @BindView(R.id.pdf_name)
    TextView pdf_name;
    @BindView(R.id.ivImagePDF)
    ImageView ivImagePDF;
    Context context;
    Group group;
    Post post = null;
    String updatePDFName;
    ArrayList<String> imagesURL = new ArrayList<>();
    String groupImageBaseUrl = "";
    MenuItem sharePost;
    String pdfBase64Data;
    AlertDialog titleAlert;
    AlertDialog alert;
    List<byte[]> imagesEncodedList = new ArrayList<>();
    ArrayList<Uri> uriList = new ArrayList<>();
    byte[] picByteArray = null;
    com.codeholic.kotumb.app.View.ProgressBar progressBar = null;
    private int maxCount = 5;
    private String key = "";
    private String post_earn_points = "";


    // oncreate method initializing view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        ButterKnife.bind(this);
        initToolbar();
        init();
        setClickListener();

        key = ResponseParameters.GROUP + group.groupId;
        if (!getIntent().hasExtra(ResponseParameters.POST)) {
            String retrievedMsg = SharedPreferencesMethod.getString(this, key);
            if (retrievedMsg != null && !retrievedMsg.isEmpty()) {
                showChangesNotSavedDialog(false);
            }
        }
        setTextWatcher(etPost, etPostLayout);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private void init() {
        context = this;
        group = (Group) getIntent().getSerializableExtra(ResponseParameters.GROUP);
        groupImageBaseUrl = getIntent().getStringExtra(ResponseParameters.GROUP_IMG_BASE_URL);
        new AQuery(this).id(ivGroupImage).image(groupImageBaseUrl + group.logo, true, true, 300, R.drawable.ic_user);
        tvUserName.setText(group.groupName);
        tvGroupName.setText(SharedPreferencesMethod.getString(this, ResponseParameters.FirstName) + " " + SharedPreferencesMethod.getString(this, ResponseParameters.LastName));
        if (getIntent().hasExtra(ResponseParameters.POST)) {
            post = (Post) getIntent().getSerializableExtra(ResponseParameters.POST);
            Log.e("POST ID", "" + post.post_id);
            if (Utils.getExtension(post.image).equalsIgnoreCase("pdf")) {
                pdf_image_container.setVisibility(View.VISIBLE);
                pdf_name.setText(post.image);
                ivImagePDF.setImageResource(R.drawable.pdf);
                etPost.setText(post.content.trim());
                etPost.setSelection(etPost.getText().toString().trim().length());
                updatePDFName = post.image;
            } else {
                pdf_image_container.setVisibility(View.GONE);
                initUpdatePost();
            }

        }


        etPost.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (keyboardShown(etPost.getRootView())) {
                    Log.d("keyboard", "keyboard UP");
                    etPost.setCursorVisible(true);
                } else {
                    Log.d("keyboard", "keyboard Down");
                    etPost.setCursorVisible(false);
                    etPost.clearFocus();
                }
            }
        });
    }

    private void initUpdatePost() {
        try {
            if (!post.image.trim().isEmpty()) {
                String[] splitArray = post.image.split("\\|");
                imagesURL = new ArrayList<String>(Arrays.asList(splitArray));
                Log.e("TAG", "" + imagesURL.size());
                showImageByURL(imagesURL);
            }
            etPost.setText(post.content.trim());
            etPost.setSelection(etPost.getText().toString().trim().length());
            maxCount = maxCount - imagesURL.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean keyboardShown(View rootView) {
        final int softKeyboardHeight = 100;
        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        DisplayMetrics dm = rootView.getResources().getDisplayMetrics();
        int heightDiff = rootView.getBottom() - r.bottom;
        return heightDiff > softKeyboardHeight * dm.density;
    }


    private void setClickListener() {
        llUploadPhoto.setOnClickListener(this);
        llUploadPDF.setOnClickListener(this);
        tvViewPhoto.setOnClickListener(this);
        llImageContainer.setOnClickListener(this);
        ivImageSingle.setOnClickListener(this);
    }

    // initToolbar method initializing Toolbar
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        // setting title
        header.setText(getResources().getString(R.string.crate_post_title));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setTextWatcher(final EditText editText, final TextInputLayout textInputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (pdf_image_container.getVisibility() == View.VISIBLE) {
                    sharePost.setEnabled(true);
                    String msg = s.toString().trim();
                    SharedPreferencesMethod.setString(CreatePostActivity.this, key, msg);
                } else {
                    enablePostBtn(s);
                    String msg = s.toString().trim();
                    SharedPreferencesMethod.setString(CreatePostActivity.this, key, msg);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void enablePostBtn(CharSequence s) {
        try {
            if (s.length() > 6) {
                sharePost.setEnabled(true);
            } else {

                if (!getIntent().hasExtra(ResponseParameters.POST)) {
                    if (imagesEncodedList.size() > 0) {
                        sharePost.setEnabled(true);
                    } else {
                        sharePost.setEnabled(false);
                    }
                } else {
                    if (imagesURL.size() > 0) {
                        sharePost.setEnabled(true);
                    } else {
                        sharePost.setEnabled(false);
                    }
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llUploadPhoto:
                if (pdf_image_container.getVisibility() == View.VISIBLE) {
                    Utils.showPopup(CreatePostActivity.this, getResources().getString(R.string.media_upload_text));
                } else {
                    checkPermissionForGallery();
                }


                break;
            case R.id.llImageContainer:


            case R.id.ivImageSingle:
                Intent intent = new Intent(this, ImagesShowActivity.class);
                intent.putExtra(ResponseParameters.IMAGE, imagesURL);
                intent.putExtra(ResponseParameters.IMAGE_URI_LIST, uriList);
                intent.putExtra(ResponseParameters.GROUP_IMG_BASE_URL, groupImageBaseUrl);
                startActivityForResult(intent, REQUEST_IMAGES);
                break;

            case R.id.llUploadPDF:
                if (ivImageOne.getVisibility() == View.VISIBLE || ivImageTwo.getVisibility() == View.VISIBLE ||
                        ivImageThree.getVisibility() == View.VISIBLE || ivImageFour.getVisibility() == View.VISIBLE ||
                        ivImageFive.getVisibility() == View.VISIBLE || ivImageSingle.getVisibility() == View.VISIBLE) {
                    Utils.showPopup(CreatePostActivity.this, getResources().getString(R.string.media_upload_text));
                } else {
                    sendPdfAsMessage();
                }
                break;
        }
    }

    public void sendPdfAsMessage() {
        Intent intent4 = new Intent(CreatePostActivity.this, NormalFilePickActivity.class);
        intent4.putExtra(Constant.MAX_NUMBER, 9);
        intent4.putExtra(BaseActivity.IS_NEED_FOLDER_LIST, true);
        intent4.putExtra(NormalFilePickActivity.SUFFIX, new String[]{"pdf"});
        startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);
    }

    void checkPermissionForGallery() {
        if (Build.VERSION.SDK_INT >= PermissionsUtils.SDK_INT_MARSHMALLOW) { // for permission
            if (PermissionsUtils.getInstance(context).requiredPermissionsGranted(context)) {
                gallery();
            }else{
                gallery();
            }
        } else {
            gallery();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_IMAGES) {
            imagesURL = data.getStringArrayListExtra(ResponseParameters.IMAGE);
            uriList = data.getParcelableArrayListExtra(ResponseParameters.IMAGE_URI_LIST);
            imagesEncodedList = Utility.convertURIToByteArray(uriList, context);
            if (imagesEncodedList.size() == 0) {
                showImageByURL(imagesURL);
                maxCount = maxCount - imagesURL.size();
            } else if (imagesURL.size() == 0) {
                showImage(imagesEncodedList);
                maxCount = maxCount - imagesEncodedList.size();
            } else {
                showImagesBoth();
                maxCount = maxCount - (imagesEncodedList.size() + imagesURL.size());
            }
            enablePostBtn(etPost.getText().toString());
            Log.e("DATA", "imagesURL " + imagesURL.size());
            Log.e("DATA", "imagesEncodedList " + imagesEncodedList.size());
        }

        if (requestCode == Constant.REQUEST_CODE_PICK_FILE) {
            ArrayList<NormalFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
            StringBuilder builder = new StringBuilder();
            for (NormalFile file : list) {
                final String path = file.getPath();
                long size = file.getSize();
                System.out.println("File Size   " + size);
                builder.append(path + "\n");
                final String pdfPath = builder.toString();
                System.out.println("Post PDF Name  " + new File(pdfPath).getName());
                File fileObj = new File(path);
                String fileSizeReadable = FileUtils.byteCountToDisplaySize(fileObj.length());
                File f = new File(path);
                long length = f.length();
                length = length / 1024;
                if (length < 10240) {
                    pdf_image_container.setVisibility(View.VISIBLE);
                    pdf_name.setText(new File(pdfPath).getName());
                    pdfBase64Data = path;
                    setPDFPath(this, path, new File(path).getName());
                    sharePost.setEnabled(true);
                } else {
                    Utils.showPopup(this, getResources().getString(R.string.pdf_size_error_text));
                }
            }
        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setPDFPath(Activity activity, String path, String name) {
        SharedPreferences settings = activity.getSharedPreferences("PDF_DATA", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("path", path);
        editor.putString("name", name);
        editor.commit();
    }


    private void getPDFPath() {
        SharedPreferences settings = context.getSharedPreferences("PDF_DATA", Context.MODE_PRIVATE);
        String path = settings.getString("path", "");
        String name = settings.getString("name", "");
        System.out.println("PDF Path  " + path);
        System.out.println("PDF Name  " + name);
    }


    public int countNumberEqual(ArrayList<String> itemList, String itemToCheck) {
        int count = 0;
        for (String i : itemList) {
            if (i.equals(itemToCheck)) {
                count++;
            }
        }
        return count;
    }


    // onRequestPermissionsResult for geeting permission from user
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermissionsUtils.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                if (PermissionsUtils.getInstance(context).areAllPermissionsGranted(grantResults)) {
                    gallery();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    void gallery() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(this)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        // here is selected uri list
                        CreatePostActivity.this.uriList = uriList;
                        Log.e("URI", "list size " + uriList.size());
                        if (uriList.size() > 0) {
                            //tvViewPhoto.setVisibility(View.VISIBLE);
                            sharePost.setEnabled(true);
                            imagesEncodedList = Utility.convertURIToByteArray(uriList, context);
                            Log.e("DATA", "imagesURL " + imagesURL.size());
                            Log.e("DATA", "imagesEncodedList " + imagesEncodedList.size());
                            if (imagesEncodedList.size() == 0) {
                                showImageByURL(imagesURL);
                            } else if (imagesURL.size() == 0) {
                                showImage(imagesEncodedList);
                            } else {
                                showImagesBoth();
                            }
                        }
                    }
                })
                .setSelectedUriList(uriList)
                .setSelectedForeground(R.drawable.selected_image_bg)
                .setPreviewMaxCount(500)
                .setPeekHeight(700)
                .showTitle(true)
                .setTitle(getResources().getString(R.string.select_image_title_text))
                .setCompleteButtonText(getResources().getString(R.string.done_btn_text))
                .setEmptySelectionText(getResources().getString(R.string.no_image_selected_msg_text))
                .setSelectMaxCount(maxCount)
                .setSelectMaxCountErrorText("You can select only 5 images")
                .create();

        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }

    private void showImagesBoth() {
        int length = imagesEncodedList.size() + imagesURL.size();
        Log.e("TAG", "Length " + imagesURL.size());
        ivImageOne.setVisibility(View.GONE);
        ivImageTwo.setVisibility(View.GONE);
        ivImageThree.setVisibility(View.GONE);
        ivImageFour.setVisibility(View.GONE);
        ivImageFive.setVisibility(View.GONE);
        llSquareContainer.setVisibility(View.GONE);
        ivImageSingle.setVisibility(View.GONE);

        ArrayList<String> images = new ArrayList<>(imagesURL);
        ArrayList<byte[]> imageBytes = new ArrayList<>(imagesEncodedList);

        byte[] imageByte = null;
        String image = "";

        if ((images.size() + imageBytes.size()) == 1) {
            if (images.size() == 1)
                new AQuery(this).id(ivImageSingle).image(groupImageBaseUrl + images.get(0), true, true, 300, R.drawable.ic_user);
            else
                ivImageSingle.setImageBitmap(BitmapFactory.decodeByteArray(imageBytes.get(0), 0, imageBytes.get(0).length));
            ivImageSingle.setVisibility(View.VISIBLE);
        } else if ((images.size() + imageBytes.size()) == 3) {

            for (int i = 0; i < 3; i++) {
                boolean isBitmap = false;
                if (images.size() > 0) {
                    isBitmap = false;
                    image = images.get(0);
                    images.remove(0);
                } else if (imageBytes.size() > 0) {
                    isBitmap = true;
                    imageByte = imageBytes.get(0);
                    imageBytes.remove(0);
                }

                switch (i) {
                    case 0:
                        if (isBitmap)
                            ivImageThree.setImageBitmap(BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length));
                        else
                            new AQuery(this).id(ivImageThree).image(groupImageBaseUrl + image, true, true, 300, R.drawable.ic_user);
                        ivImageThree.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        if (isBitmap)
                            ivImageFour.setImageBitmap(BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length));
                        else
                            new AQuery(this).id(ivImageTwo).image(groupImageBaseUrl + image, true, true, 300, R.drawable.ic_user);
                        ivImageFour.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        if (isBitmap)
                            ivImageFive.setImageBitmap(BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length));
                        else
                            new AQuery(this).id(ivImageThree).image(groupImageBaseUrl + image, true, true, 300, R.drawable.ic_user);
                        ivImageFive.setVisibility(View.VISIBLE);
                        break;
                }
            }
        } else {
            int totalLength = images.size() + imageBytes.size();
            if (totalLength > 5)
                totalLength = 5;
            for (int i = 0; i < totalLength; i++) {
                boolean isBitmap = false;
                if (images.size() > 0) {
                    isBitmap = false;
                    image = images.get(0);
                    images.remove(0);
                } else if (imageBytes.size() > 0) {
                    isBitmap = true;
                    imageByte = imageBytes.get(0);
                    imageBytes.remove(0);
                }

                switch (i) {
                    case 0:
                        if (isBitmap)
                            ivImageOne.setImageBitmap(BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length));
                        else
                            new AQuery(this).id(ivImageOne).image(groupImageBaseUrl + image, true, true, 300, R.drawable.ic_user);
                        ivImageOne.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        if (isBitmap)
                            ivImageTwo.setImageBitmap(BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length));
                        else
                            new AQuery(this).id(ivImageTwo).image(groupImageBaseUrl + image, true, true, 300, R.drawable.ic_user);
                        ivImageTwo.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        if (isBitmap)
                            ivImageThree.setImageBitmap(BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length));
                        else
                            new AQuery(this).id(ivImageThree).image(groupImageBaseUrl + image, true, true, 300, R.drawable.ic_user);
                        ivImageThree.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        if (isBitmap)
                            ivImageFour.setImageBitmap(BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length));
                        else
                            new AQuery(this).id(ivImageFour).image(groupImageBaseUrl + image, true, true, 300, R.drawable.ic_user);
                        ivImageFour.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        if (isBitmap)
                            ivImageFive.setImageBitmap(BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length));
                        else
                            new AQuery(this).id(ivImageFive).image(groupImageBaseUrl + image, true, true, 300, R.drawable.ic_user);
                        ivImageFive.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }

        if (length > 5) {
            llSquareContainer.setVisibility(View.VISIBLE);
            tvPhotoNumber.setText((length - 5) + "+");
        }

        Log.e("TAG", "Length " + imagesURL.size());
    }

    void showImage(List<byte[]> imagesEncodedList) {

        int length = imagesEncodedList.size();// + imagesURL.size();

        ivImageOne.setVisibility(View.GONE);
        ivImageTwo.setVisibility(View.GONE);
        ivImageThree.setVisibility(View.GONE);
        ivImageFour.setVisibility(View.GONE);
        ivImageFive.setVisibility(View.GONE);
        llSquareContainer.setVisibility(View.GONE);
        ivImageSingle.setVisibility(View.GONE);

        if (length == 1) {
            ivImageSingle.setImageBitmap(BitmapFactory.decodeByteArray(imagesEncodedList.get(0), 0, imagesEncodedList.get(0).length));
            ivImageSingle.setVisibility(View.VISIBLE);
        } else if (imagesEncodedList.size() == 3) {
            ivImageThree.setImageBitmap(BitmapFactory.decodeByteArray(imagesEncodedList.get(0), 0, imagesEncodedList.get(0).length));
            ivImageThree.setVisibility(View.VISIBLE);
            ivImageFour.setImageBitmap(BitmapFactory.decodeByteArray(imagesEncodedList.get(1), 0, imagesEncodedList.get(1).length));
            ivImageFour.setVisibility(View.VISIBLE);
            ivImageFive.setImageBitmap(BitmapFactory.decodeByteArray(imagesEncodedList.get(2), 0, imagesEncodedList.get(2).length));
            ivImageFive.setVisibility(View.VISIBLE);
        } else {
            for (int i = 0; i < length; i++) {
                if (imagesURL.size() < i + 1) {
                    switch (i) {
                        case 0:
                            ivImageOne.setImageBitmap(BitmapFactory.decodeByteArray(imagesEncodedList.get(i), 0, imagesEncodedList.get(i).length));
                            ivImageOne.setVisibility(View.VISIBLE);
                            break;
                        case 1:
                            ivImageTwo.setImageBitmap(BitmapFactory.decodeByteArray(imagesEncodedList.get(i), 0, imagesEncodedList.get(i).length));
                            ivImageTwo.setVisibility(View.VISIBLE);
                            break;
                        case 2:
                            ivImageThree.setImageBitmap(BitmapFactory.decodeByteArray(imagesEncodedList.get(i), 0, imagesEncodedList.get(i).length));
                            ivImageThree.setVisibility(View.VISIBLE);
                            break;
                        case 3:
                            ivImageFour.setImageBitmap(BitmapFactory.decodeByteArray(imagesEncodedList.get(i), 0, imagesEncodedList.get(i).length));
                            ivImageFour.setVisibility(View.VISIBLE);
                            break;
                        case 4:
                            ivImageFive.setImageBitmap(BitmapFactory.decodeByteArray(imagesEncodedList.get(i), 0, imagesEncodedList.get(i).length));
                            ivImageFive.setVisibility(View.VISIBLE);
                            break;
                    }
                }
            }
        }

        /*if(length == 1){
            ivImageSingle.setVisibility(View.VISIBLE);
        }

        if (length >= 2) {
            ivImageOne.setVisibility(View.VISIBLE);
            ivImageTwo.setVisibility(View.VISIBLE);
        }
        if (length == 3) {
            ivImageOne.setVisibility(View.GONE);
            ivImageTwo.setVisibility(View.GONE);
            ivImageThree.setVisibility(View.VISIBLE);
            ivImageFour.setVisibility(View.VISIBLE);
            ivImageFive.setVisibility(View.VISIBLE);
        }

        if (length >= 3) {
            ivImageThree.setVisibility(View.VISIBLE);
        }

        if (length >= 4) {
            ivImageFour.setVisibility(View.VISIBLE);
        }
        if (length >= 5) {
            ivImageFive.setVisibility(View.VISIBLE);
        }*/

        if (length > 5) {
            llSquareContainer.setVisibility(View.VISIBLE);
            tvPhotoNumber.setText((length - 5) + "+");
        }
    }

    void showImageByURL(List<String> imageNameList) {

        ivImageOne.setVisibility(View.GONE);
        ivImageTwo.setVisibility(View.GONE);
        ivImageThree.setVisibility(View.GONE);
        ivImageFour.setVisibility(View.GONE);
        ivImageFive.setVisibility(View.GONE);
        llSquareContainer.setVisibility(View.GONE);
        ivImageSingle.setVisibility(View.GONE);

        if (imageNameList.size() == 1) {
            new AQuery(this).id(ivImageSingle).image(groupImageBaseUrl + imageNameList.get(0), true, true, 300, R.drawable.ic_user);
            ivImageSingle.setVisibility(View.VISIBLE);
        } else if (imageNameList.size() == 3) {
            new AQuery(this).id(ivImageThree).image(groupImageBaseUrl + imageNameList.get(0), true, true, 300, R.drawable.ic_user);
            ivImageThree.setVisibility(View.VISIBLE);
            new AQuery(this).id(ivImageFour).image(groupImageBaseUrl + imageNameList.get(1), true, true, 300, R.drawable.ic_user);
            ivImageFour.setVisibility(View.VISIBLE);
            new AQuery(this).id(ivImageFive).image(groupImageBaseUrl + imageNameList.get(2), true, true, 300, R.drawable.ic_user);
            ivImageFive.setVisibility(View.VISIBLE);
        } else {
            for (int i = 0; i < imageNameList.size(); i++) {
                switch (i) {
                    case 0:
                        new AQuery(this).id(ivImageOne).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.ic_user);
                        ivImageOne.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        new AQuery(this).id(ivImageTwo).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.ic_user);
                        ivImageTwo.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        new AQuery(this).id(ivImageThree).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.ic_user);
                        ivImageThree.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        new AQuery(this).id(ivImageFour).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.ic_user);
                        ivImageFour.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        new AQuery(this).id(ivImageFive).image(groupImageBaseUrl + imageNameList.get(i), true, true, 300, R.drawable.ic_user);
                        ivImageFive.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }
        if (imageNameList.size() > 5) {
            llSquareContainer.setVisibility(View.VISIBLE);
            tvPhotoNumber.setText((imageNameList.size() - 5) + "+");
        }
    }

    /*private void TitlePopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.title_popup, null);
        dialogBuilder.setView(dialogView);
        final EditText etTitle = (EditText) dialogView.findViewById(R.id.etTitle);
        if (getIntent().hasExtra(RequestParameters.NOTE)) {
            etTitle.setText(note.getPostTitle());
        }
        final TextInputLayout etTitleLayout = (TextInputLayout) dialogView.findViewById(R.id.etTitleLayout);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.save_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                CreatePostActivity.this.titleAlert.dismiss();
            }
        });
        this.titleAlert = dialogBuilder.create();
        this.titleAlert.setCancelable(false);
        this.titleAlert.show();
        this.titleAlert.getButton(-1).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Boolean.valueOf(false).booleanValue()) {
                    CreatePostActivity.this.titleAlert.dismiss();
                }
                etTitleLayout.setErrorEnabled(false);
                if (etTitle.getText().toString().trim().isEmpty()) {
                    etTitleLayout.setErrorEnabled(true);
                    etTitleLayout.setError(CreatePostActivity.this.getResources().getString(R.string.note_title_empty_input));
                } else if (Utility.isConnectingToInternet(CreatePostActivity.this)) {
                    HashMap<String, Object> input = new HashMap();
                    input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(CreatePostActivity.this));
                    input.put(RequestParameters.TITLE, "" + etTitle.getText().toString().trim());
                    input.put(RequestParameters.DESCRIPTION, "" + etPost.getText().toString().trim());
                    input.put(RequestParameters.DATE, "" + toDate);
                    input.put(RequestParameters.TIME, "" + toTime);
                    Log.e("input", "" + input);
                    progressBar = new com.codeholic.kotumb.app.View.ProgressBar(CreatePostActivity.this);
                    progressBar.show(getResources().getString(R.string.app_please_wait_text));
                    if (getIntent().hasExtra(RequestParameters.NOTE)) {
                        input.put(RequestParameters.NOTEID, "" + note.getPostID());
                        API.sendRequestToServerPOST_PARAM(CreatePostActivity.this, API.UPDATE_NOTE, input);// service call for share resume
                    } else {
                        API.sendRequestToServerPOST_PARAM(CreatePostActivity.this, API.CREATE_NOTE, input);// service call for share resume
                    }

                    CreatePostActivity.this.titleAlert.dismiss();
                } else {
                    Snackbar.make(etTitleLayout, CreatePostActivity.this.getResources().getString(R.string.app_no_internet_error), 0).show();
                }
            }
        });
    }*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_post, menu);
        sharePost = menu.findItem(R.id.action_post);
        if (getIntent().hasExtra(ResponseParameters.POST)) {
            sharePost.setEnabled(true);
            sharePost.setTitle(getResources().getString(R.string.update_btn_text));
        }
        enablePostBtn(etPost.getText().toString().trim());
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_post) {
            if (validation()) {
                if (!Utility.isConnectingToInternet(context)) { // check net connection
                    Utils.showPopup(CreatePostActivity.this, getResources().getString(R.string.app_no_internet_error));
                } else {
                    if (pdf_image_container.getVisibility() == View.VISIBLE) {

                        final ProgressDialog progressDialog;
                        progressDialog = new ProgressDialog(this);
                        progressDialog.setMessage("PDF Posting");
                        progressDialog.show();
                        try {
                            post_pdf(new File(pdfBase64Data), progressDialog);
                        } catch (Exception ex) {
                            post_pdf(new File("1"), progressDialog);
                        }

                    } else {
                        String url = API.GROUP_ADD_POST;
                        HashMap<String, Object> input = new HashMap();
                        input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(CreatePostActivity.this));
                        input.put(RequestParameters.GROUPID, "" + group.groupId);
                        input.put(RequestParameters.CONTENT, "" + etPost.getText().toString().trim());
                        for (int i = 0; i < imagesEncodedList.size(); i++) {
                            Log.e("input", "" + imagesEncodedList.get(i));
                            input.put(RequestParameters.IMAGE + i, "data:image/png;base64," + Base64.encodeToString(imagesEncodedList.get(i), Base64.NO_WRAP));
                        }
                        if (getIntent().hasExtra(ResponseParameters.POST)) {
                            url = API.GROUP_UPDATE_POST;
                            String oldImages = "";
                            for (int i = 0; i < imagesURL.size(); i++) {
                                oldImages = oldImages + "|" + imagesURL.get(i).trim();
                            }
                            if (oldImages.length() > 0)
                                oldImages = oldImages.substring(1, oldImages.length());
                            System.out.println("Old Image Get   " + oldImages);
                            Log.e("TAG", "old images " + oldImages);
                            input.put(RequestParameters.OLD_IMAGES, oldImages);
                            input.put(RequestParameters.POSTID, post.post_id);

                        }
                        Set set = input.entrySet();
                        Iterator iterator = set.iterator();
                        while (iterator.hasNext()) {
                            Map.Entry mentry = (Map.Entry) iterator.next();
                            Log.e("input", "" + mentry.getKey());
                        }
                        progressBar = new com.codeholic.kotumb.app.View.ProgressBar(CreatePostActivity.this);
                        progressBar.show(getResources().getString(R.string.app_please_wait_text));
                        SharedPreferencesMethod.setString(CreatePostActivity.this, key, "");
                        System.out.println("Image Post  " + input.toString());
                        System.out.println("Post URL  " + url);
                        API.sendRequestToServerPOST_PARAM(CreatePostActivity.this, url, input);// service call for share resume
                    }

                }
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void post_pdf(File file, final ProgressDialog progressDialog) {
//        System.out.println("File Get  "+file);
//        String URL=API.GROUP_ADD_POST;
//        try {
//            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();
//            MultipartBody.Builder buildernew = new MultipartBody.Builder().setType(MultipartBody.FORM);
//            buildernew.addFormDataPart(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(CreatePostActivity.this));
//            buildernew.addFormDataPart(RequestParameters.GROUPID, "" + group.groupId);
//            buildernew.addFormDataPart(RequestParameters.CONTENT, "" + etPost.getText().toString().trim());
//            if (getIntent().hasExtra(ResponseParameters.POST)) {
//                URL=API.GROUP_UPDATE_POST;
//                if (file.toString().equalsIgnoreCase("1")){
//                    buildernew .addFormDataPart(RequestParameters.OLD_IMAGES,post.image);
//                    buildernew.addFormDataPart(RequestParameters.POSTID, "" + post.post_id);
//                }else{
//                    buildernew .addFormDataPart(RequestParameters.IMAGE,file.getName(),RequestBody.create(MediaType.parse("application/pdf"),file));
//                    buildernew.addFormDataPart(RequestParameters.POSTID, "" + post.post_id);
//                }
//            }else{
//                buildernew.addFormDataPart(RequestParameters.IMAGE,file.getName(),RequestBody.create(MediaType.parse("application/pdf"),file));
//            }
//            MultipartBody requestBody = buildernew.build();
//        okhttp3.Request request = new okhttp3.Request.Builder()
//                    .url(URL)
//                    .post(requestBody)
//                    .build();
//            System.out.println("Input Data"+buildernew.toString());
//            client.newCall(request).enqueue(new Callback() {
//                @Override
//                public void onFailure(final Call call, final IOException e) {
//                    System.out.println("PDF Faliure   "+e.toString());
//                }
//
//                @Override
//                public void onResponse(Call call, final okhttp3.Response response) throws IOException {
//                    if (!response.isSuccessful()) {
//                        System.out.println("PDF Response Error   "+response.toString());
//                    }else{
//                        System.out.println("PDF Response is   "+response.body().toString());
//                            progressDialog.dismiss();
//                        JSONObject responseObj = null;
//                        try {
//                            responseObj = new JSONObject(URLDecoder.decode(response.body().string(), "UTF-8" ));
//                            Log.e("CREATEPOST", "responseObj: " + responseObj);
//                        } catch (JSONException e) {
//                            Log.e("CREATEPOST", "Error is: " + e.toString());
//                            e.printStackTrace();
//                        }
//
//
//                            CreatePostActivity.this.runOnUiThread(new Runnable() {
//                                public void run() {
//                                    try{
//                                        String resStr = response.body().string();
//
//                                        JSONObject json = new JSONObject(resStr);
//                                        System.out.println("Posting Response  "+json.toString());
//                                        if (json.has(ResponseParameters.POST)) {
//                                                    Gson gson = new Gson(); // creates a Gson instance
//                                                    Post post = gson.fromJson(json.getString(ResponseParameters.POST), Post.class);
//                                                    GroupPostActivity.postToAppend = post;
//                                        }
//                                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                                        imm.hideSoftInputFromWindow(etPost.getWindowToken(), 0);
//                                        Intent data = new Intent();
//                                        Bundle bundle = new Bundle();
//                                        bundle.putSerializable("post", post);
//                                        data.putExtras(bundle);
//                                        setResult(RESULT_OK, data);
//                                        Bundle logBundle = new Bundle();
//                                        logBundle.putString("UserId", SharedPreferencesMethod.getUserId(CreatePostActivity.this));
//                                        logBundle.putString("GroupId", group.groupId);
//                                        Utils.logEvent(EventNames.GROUP_POST, logBundle, CreatePostActivity.this);
//                                        Utils.showPopup(CreatePostActivity.this, getString(R.string.success_text), new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View view) {
//                                                finish();
//                                            }
//                                        });
//                                    }catch (Exception ex){
//                                        System.out.println("Posting Error  "+ex.toString());
//                                    }
//                                }
//                            });
//                    }
//                }
//
//            });
//
//        } catch (Exception ex) {
//            System.out.println("Media Message Error  "+ex);
//        }

        System.out.println("File Get  " + file);
        String URL = API.GROUP_ADD_POST;
        try {
            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();
            MultipartBody.Builder buildernew = new MultipartBody.Builder().setType(MultipartBody.FORM);
            buildernew.addFormDataPart(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(CreatePostActivity.this));
            buildernew.addFormDataPart(RequestParameters.GROUPID, "" + group.groupId);
            buildernew.addFormDataPart(RequestParameters.CONTENT, "" + etPost.getText().toString().trim());
            if (getIntent().hasExtra(ResponseParameters.POST)) {
                URL = API.GROUP_UPDATE_POST;
                if (file.toString().equalsIgnoreCase("1")) {
                    buildernew.addFormDataPart(RequestParameters.OLD_IMAGES, post.image);
                    buildernew.addFormDataPart(RequestParameters.POSTID, "" + post.post_id);
                } else {
                    buildernew.addFormDataPart(RequestParameters.IMAGE, file.getName(), RequestBody.create(MediaType.parse("application/pdf"), file));
                    buildernew.addFormDataPart(RequestParameters.POSTID, "" + post.post_id);
                }
            } else {
                buildernew.addFormDataPart(RequestParameters.IMAGE, file.getName(), RequestBody.create(MediaType.parse("application/pdf"), file));
            }
            MultipartBody requestBody = buildernew.build();
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(URL)
                    .post(requestBody)
                    .build();
            System.out.println("Input Data" + request.toString());
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(final Call call, final IOException e) {
                    System.out.println("PDF Faliure   " + e.toString());
                }

                @Override
                public void onResponse(Call call, final okhttp3.Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        System.out.println("PDF Response Error   " + response.toString());
                    } else {
                        progressDialog.dismiss();
                        CreatePostActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                try {
                                    String resStr = response.body().string().toString();
                                    JSONObject json = new JSONObject(resStr);
                                    System.out.println("Posting Response  " + json.toString());
                                    if (json.has(ResponseParameters.POST)) {
                                        Gson gson = new Gson(); // creates a Gson instance
                                        Post post = gson.fromJson(json.getString(ResponseParameters.POST), Post.class);
                                        GroupPostActivity.postToAppend = post;
                                    }
                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(etPost.getWindowToken(), 0);
                                    Intent data = new Intent();
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("post", post);
                                    data.putExtras(bundle);
                                    setResult(RESULT_OK, data);
                                    Bundle logBundle = new Bundle();
                                    logBundle.putString("UserId", SharedPreferencesMethod.getUserId(CreatePostActivity.this));
                                    logBundle.putString("GroupId", group.groupId);
                                    Utils.logEvent(EventNames.GROUP_POST, logBundle, CreatePostActivity.this);
                                    Utils.showPopup(CreatePostActivity.this, getString(R.string.success_text), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finish();
                                        }
                                    });
                                } catch (Exception ex) {
                                    System.out.println("Posting Error  " + ex.toString());
                                }
                            }
                        });
                    }
                }

            });

        } catch (Exception ex) {
            System.out.println("Media Message Error  " + ex);
        }

    }


    private boolean validation() {
        boolean result = true;
        if (etPost.getText().toString().isEmpty() && imagesEncodedList.size() <= 0) {
            //showPopUp(getResources().getString(R.string.please_enter_post_description_msg), false, true);
            //result = false;
        }

        return result;
    }

    public void getResponse(JSONObject outPut, int i) {
        try {
            if (progressBar != null)
                progressBar.dismiss();
            Log.e("output", "" + outPut);
            System.out.println("Post Response   " + outPut.toString());

            if (outPut.has("group_point")){
                post_earn_points = outPut.getString("group_point");
            }

            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Success)) {
                    if (outPut.has(ResponseParameters.POST)) {
                        Gson gson = new Gson(); // creates a Gson instance
                        Post post = gson.fromJson(outPut.getString(ResponseParameters.POST), Post.class);
                        GroupPostActivity.postToAppend = post;
                    }
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etPost.getWindowToken(), 0);
                    Intent data = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("post", post);
                    data.putExtras(bundle);
                    setResult(RESULT_OK, data);

                    Bundle logBundle = new Bundle();
                    logBundle.putString("UserId", SharedPreferencesMethod.getUserId(this));
                    logBundle.putString("GroupId", group.groupId);
                    Utils.logEvent(EventNames.GROUP_POST, logBundle, this);

                    Utils.showPopup(CreatePostActivity.this, getString(R.string.success_text), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            //reward alert
                            if(post_earn_points.length()>0) {
                                if (!post_earn_points.equals("0")) {
                                    //showPointsEarnDialog(post_earn_points);
                                } else {
                                    finish();
                                }
                            }else{
                                finish();
                            }
                        }
                    });
                } else if (outPut.has(ResponseParameters.Errors)) {
                    System.out.println("Response Error  " + outPut.toString());
                    final Snackbar snackbar = Snackbar.make(etPost, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            } else {
                System.out.println("Response Error  " + outPut.toString());
                final Snackbar snackbar = Snackbar.make(etPost, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                snackbar.show();
            }
        } catch (Exception e) {

        }
    }


    private void showPopUp(String title, boolean showYes, boolean showCancel) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = (LinearLayout) dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = (LinearLayout) dialogView.findViewById(R.id.llCancel);

        final TextView tvTitle = (TextView) dialogView.findViewById(R.id.title);
        final View view = dialogView.findViewById(R.id.space);
        tvTitle.setText(title);
        if (!showYes) {
            view.setVisibility(View.GONE);
            llYes.setVisibility(View.GONE);
        }

        if (!showCancel) {
            view.setVisibility(View.GONE);
            llCancel.setVisibility(View.GONE);
        }

        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etPost.getWindowToken(), 0);
                finish();
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert = dialogBuilder.create();
        alert.setCancelable(false);


        alert.show();

    }

    @Override
    public void onBackPressed() {

        if (imagesEncodedList.size() > 0 || !etPost.getText().toString().trim().isEmpty() || !pdfBase64Data.isEmpty()) {
            //showPopUp(getResources().getString(R.string.create_post_back_message), true, true);
            showChangesNotSavedDialog(true);
        } else {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etPost.getWindowToken(), 0);
            finish();
        }
        //super.onBackPressed();
    }

    private void showChangesNotSavedDialog(final boolean onBackPressed) {
        String string;
        String yes;
        String no;
        if (onBackPressed) {
            string = getString(R.string.create_post_back_message);
            yes = getString(R.string.discard_btn_text);
            no = getString(R.string.save_btn_text);
        } else {
            string = getString(R.string.create_post_retrieve_message);
            yes = getString(R.string.discard_btn_text);
            no = getString(R.string.continue_btn_text);
        }
        Utils.showPopup(this, string, yes, no, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferencesMethod.setString(CreatePostActivity.this, key, "");
                if (onBackPressed) {
                    finish();
                }
                // fillValues(SharedPreferencesMethod.getUserInfo(CompleteProfileActivity.this));
                // SharedPreferencesMethod.setString(CompleteProfileActivity.this, ResponseParameters.PROFILE, "");
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onBackPressed) {
                    Utils.showPopup(CreatePostActivity.this, "Post Saved Succesfully", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    });
                } else {
                    String retrievedMsg = SharedPreferencesMethod.getString(CreatePostActivity.this, key);
                    etPost.setText(retrievedMsg);
                    etPost.setSelection(retrievedMsg.length());
                }
            }
        });
    }

    private void showPointsEarnDialog(String pointValue) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.alert_reward_points, viewGroup, false);
        TextView tv_points = dialogView.findViewById(R.id.tv_points);
        TextView tv_ok = dialogView.findViewById(R.id.tv_ok);
        tv_points.setText(getString(R.string.reward_str_pre)+" "+pointValue+" "+getString(R.string.reward_str_post));

        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
