package com.codeholic.kotumb.app.Services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.NonNull;
import android.util.Log;

import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;


public class GetAddService extends Service {
    public static final int DELAY_MILLIS = 45000;
    Handler handler = new Handler();
    boolean isDestroyed = false;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {

        }
    };
    private ArrayList<String> ids = new ArrayList<>();

    public GetAddService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String idString = SharedPreferencesMethod.getString(getApplicationContext(), "ads");
        if (idString != null && !idString.isEmpty()) {
            ArrayList<String> retrievedIds = new Gson().fromJson(idString, ArrayList.class);
            ids.addAll(retrievedIds);
        }

        runnable = new Runnable() {
            @Override
            public void run() {
                if (!isDestroyed) {
                    /*Log.e("service running", "service running");
                    Log.e("tag", ""+API.GET_ADD+ SharedPreferencesMethod.getUserId(GetAddService.this));*/
                    String array = getStringArrayOfIds();
                    String url = API.GET_ADD + SharedPreferencesMethod.getUserId(GetAddService.this) + "?ad_ids=" + array;
                    API.sendRequestToServerGET(GetAddService.this, url, API.GET_ADD);
                    handler.postDelayed(this, DELAY_MILLIS);
                }
            }
        };
        handler.post(runnable);
        return Service.START_NOT_STICKY;
    }

    @NonNull
    private String getStringArrayOfIds() {
        String array = "";
        for (int i = 0; i < ids.size(); i++) {
            array = array + ids.get(i);
            if (i != ids.size() - 1) {
                array = array + ",";
            }
        }
        return array;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.e("tag", "onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Log.e("tag", "ondestroy");
        isDestroyed = true;
        if (handler != null && runnable != null)
            handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    public void getResponse(JSONObject outPut, int type) {
        try {
            //Log.e("service response", "" + outPut);
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Success)) { //check output have success
                    if (type == 0) {

                        String resetad = outPut.getString("reset_ad");
                        boolean reset = Boolean.parseBoolean(resetad);
                        if (reset) {
                           // Toast.makeText(this, reset + ": reset", Toast.LENGTH_SHORT).show();
                            ids.clear();
                            String adId = outPut.getJSONObject("ad").getString("id");
                            ids.add(adId);
                            SharedPreferencesMethod.setString(getApplicationContext(), "ads", "");
                        } else {
                            //Toast.makeText(this, reset + ": reset", Toast.LENGTH_SHORT).show();
                            String adId = outPut.getJSONObject("ad").getString("id");
                            ids.add(adId);
                            SharedPreferencesMethod.setString(getApplicationContext(), "ads", new Gson().toJson(ids));
                        }

                        Intent adReceived = new Intent();

                        if (outPut.has(ResponseParameters.AD_IMAGE_BASE_URL)) {
                            API.imageAdUrl = outPut.getString(ResponseParameters.AD_IMAGE_BASE_URL);
                        }

                        adReceived.setAction(ResponseParameters.ADD_RECEIVED);
                        adReceived.putExtra(ResponseParameters.ADD_RECEIVED, outPut.toString());
                        sendBroadcast(adReceived);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
