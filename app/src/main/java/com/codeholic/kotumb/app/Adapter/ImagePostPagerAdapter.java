package com.codeholic.kotumb.app.Adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.View.CustomImageView;

import java.util.ArrayList;
import java.util.List;



/**
 * @author Harneev Sethi
 */

public class ImagePostPagerAdapter extends PagerAdapter {

    private LayoutInflater mInflater;
    private Context mContext;
    private String base_url = "";

    private List<String> mPagerList = new ArrayList<>();

    public ImagePostPagerAdapter(List<String> list, Context context, String base_url) {

        if (list != null && list.size() > 0)
            mPagerList.addAll(list);

        this.mContext = context;
        this.base_url = base_url;

        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View rootView = mInflater.inflate(R.layout.item_post_image_pager, container, false);

        ViewHolder holder = new ViewHolder(rootView);

        /*final PagerItem data = mPagerList.get(position);

        if (data != null) {

            holder.pagerText.setText(data.getItemText());
        }*/


        Log.e("TAG", ""+base_url + mPagerList.get(position));

        new AQuery(mContext).id(holder.pic).image(base_url + mPagerList.get(position), true, true, 300, R.drawable.ic_user);

        container.addView(rootView);

        return rootView;
    }

    @Override
    public int getCount() {

        return mPagerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Don't call the super
        // super.destroyItem(container, position, object);

        container.removeView((View) object);
    }

    /**
     * {@link RecyclerView.ViewHolder}
     */
    class ViewHolder {

        public CustomImageView pic;

        ViewHolder(View rootView) {
            pic = rootView.findViewById(R.id.pic);
        }
    }
}
