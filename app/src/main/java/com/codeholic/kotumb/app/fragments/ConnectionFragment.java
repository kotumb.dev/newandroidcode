package com.codeholic.kotumb.app.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Activity.SearchConnectionActivtiy;
import com.codeholic.kotumb.app.Adapter.FriendListRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;


public class ConnectionFragment extends BaseFragment {

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.tvNouser)
    TextView tvNouser;

    int pagination = 0;

    FriendListRecyclerViewAdapter friendListRecyclerViewAdapter;

    private LinearLayoutManager linearLayoutManager;
    List<User> users = new ArrayList<>();
    Context _context;
    private BroadcastReceiver receiver;
    private BroadcastReceiver requestAccepted;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("onCreate", "onCreate");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_connections, container, false);

        ButterKnife.bind(this, view);

        ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.connections_title));
        Log.e("onCreateView", "onCreateView");
        setView();
        setRequestAcceptedBroadCast();
        setBroadCast();

        setHasOptionsMenu(true);
        return view;
    }

    private void setBroadCast() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //do something based on the intent's action
                Log.e("BroadcastReceiver", "BroadcastReceiver");
                User updatedUser = intent.getParcelableExtra("USER");
                for (int i = 0; i < users.size(); i++) {
                    User user = users.get(i);
                    if (updatedUser.getUserId().equals(user.getUserId())) {
                        try {
                            JSONObject updatedJson = new JSONObject(updatedUser.getUserInfo());
                            JSONObject json = new JSONObject(user.getUserInfo());
                            if (updatedJson.has(ResponseParameters.CONNECTION_FLAG)) {
                                json.put(ResponseParameters.CONNECTION_FLAG, updatedJson.getBoolean(ResponseParameters.CONNECTION_FLAG));
                            }
                            if (updatedJson.has(ResponseParameters.CONNECTION_INFO)) {
                                json.put(ResponseParameters.CONNECTION_INFO, updatedJson.getJSONObject(ResponseParameters.CONNECTION_INFO));
                            }
                            user.setUserInfo(json.toString());

                            if (friendListRecyclerViewAdapter != null) {
                                friendListRecyclerViewAdapter.notifyItemRemoved(i);
                            }
                            users.remove(i);

                            if (users.size() <= 0) {
                                showNoUser();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.UPDATE_USER);
        _context.registerReceiver(receiver, filter);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(getActivity(), SearchConnectionActivtiy.class));
//                openKeyboard();
                return true;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);

    }


    private void setRequestAcceptedBroadCast() {
        requestAccepted = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //do something based on the intent's action
                Log.e("test", "test");
                User updatedUser = intent.getParcelableExtra("USER");
                if (users.size() > 0) {
                    users.add(0, updatedUser);
                    friendListRecyclerViewAdapter.notifyItemInserted(0);
                } else {
                    llNotFound.setVisibility(View.GONE);
                    users.add(0, updatedUser);
                    linearLayoutManager = new LinearLayoutManager(getActivity());
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    rvUserList.setLayoutManager(linearLayoutManager);
                    rvUserList.setNestedScrollingEnabled(false);
                    friendListRecyclerViewAdapter = new FriendListRecyclerViewAdapter(rvUserList, getActivity(), users, ConnectionFragment.this);
                    rvUserList.setAdapter(friendListRecyclerViewAdapter);

                    friendListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {
                            if (users.size() >= 6) {
                                Log.e("inside", "inside");
                                rvUserList.post(new Runnable() {
                                    public void run() {
                                        users.add(null);
                                        friendListRecyclerViewAdapter.notifyItemInserted(users.size() - 1);
                                    }
                                });
                                String url = API.CONNECTECTIONS + SharedPreferencesMethod.getUserId(getActivity()) + "/" + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + (pagination + 6);
                                API.sendRequestToServerGET_FRAGMENT(getActivity(), ConnectionFragment.this, url, API.CONNECTECTIONS_UPDATE);
                            }
                        }
                    });
                }

            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.REQUEST_ACCEPTED);
        _context.registerReceiver(requestAccepted, filter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Constants.fromProfile) {
            setView();
            Constants.fromProfile = false;
        }
        Log.e("resume", "resume");
    }


    @Override
    public void onDestroyView() {
        Log.e("onDestroyView", "onDestroyView");
        if (receiver != null) {
            _context.unregisterReceiver(receiver);
            receiver = null;
        }
        if (requestAccepted != null) {
            _context.unregisterReceiver(requestAccepted);
            requestAccepted = null;
        }
        super.onDestroyView();
    }

    private void setView() {
        try {
            if (!Utility.isConnectingToInternet(getActivity())) {
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(View.VISIBLE);
                tvNouser.setText(getResources().getString(R.string.app_no_internet_error));
                final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            pagination = 0;
            String url = API.CONNECTECTIONS + SharedPreferencesMethod.getUserId(getActivity()) + "/" + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + pagination;
            API.sendRequestToServerGET_FRAGMENT(getActivity(), this, url, API.CONNECTECTIONS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        try {
            if (i == 0) {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) {
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            users = new ArrayList<>();
                            for (int j = 0; j < outPut.getJSONArray(ResponseParameters.CONNECTIONS).length(); j++) {
                                JSONObject searchResults = outPut.getJSONArray(ResponseParameters.CONNECTIONS).getJSONObject(j);
                                User user = new User();
                                user.setFirstName(searchResults.getString(ResponseParameters.FirstName));
                                user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                                user.setLastName(searchResults.getString(ResponseParameters.LastName));
                                user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                                user.setUserId(searchResults.getString(ResponseParameters.UserId));
                                user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                                user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                                user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                                user.setCity(searchResults.getString(ResponseParameters.City));
                                user.setState(searchResults.getString(ResponseParameters.State));
                                user.setIsBlocked(searchResults.getString(ResponseParameters.IS_BLOCKED));
                                user.setIsDeleted(searchResults.getString(ResponseParameters.IS_DELETED));
                                user.setUserInfo(searchResults.toString());
                                users.add(user);
                            }

                            Log.e("DELETE", ""+users.size());

                            linearLayoutManager = new LinearLayoutManager(getActivity());
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rvUserList.setLayoutManager(linearLayoutManager);
                            rvUserList.setNestedScrollingEnabled(false);
                            friendListRecyclerViewAdapter = new FriendListRecyclerViewAdapter(rvUserList, getActivity(), users, this);
                            rvUserList.setAdapter(friendListRecyclerViewAdapter);

                            friendListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                @Override
                                public void onLoadMore() {
                                    if (users.size() >= 6) {
                                        Log.e("inside", "inside");
                                        rvUserList.post(new Runnable() {
                                            public void run() {
                                                users.add(null);
                                                friendListRecyclerViewAdapter.notifyItemInserted(users.size() - 1);
                                                friendListRecyclerViewAdapter.notifyDataSetChanged2();
                                            }
                                        });
                                        String url = API.CONNECTECTIONS + SharedPreferencesMethod.getUserId(getActivity()) + "/" + SharedPreferencesMethod.getUserId(getActivity()) + "/?offset=" + (pagination + 6);
                                        API.sendRequestToServerGET_FRAGMENT(getActivity(), ConnectionFragment.this, url, API.CONNECTECTIONS_UPDATE);
                                    }
                                }
                            });
                        }
                    } else {
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            } else if (i == 1) {
                users.remove(users.size() - 1);
                friendListRecyclerViewAdapter.notifyItemRemoved(users.size());
                friendListRecyclerViewAdapter.notifyDataSetChanged2();
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) {
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            for (int j = 0; j < outPut.getJSONArray(ResponseParameters.CONNECTIONS).length(); j++) {
                                JSONObject searchResults = outPut.getJSONArray(ResponseParameters.CONNECTIONS).getJSONObject(j);
                                User user = new User();
                                user.setFirstName(searchResults.getString(ResponseParameters.FirstName));
                                user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                                user.setLastName(searchResults.getString(ResponseParameters.LastName));
                                user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                                user.setUserId(searchResults.getString(ResponseParameters.UserId));
                                user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                                user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                                user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                                user.setCity(searchResults.getString(ResponseParameters.City));
                                user.setState(searchResults.getString(ResponseParameters.State));
                                user.setUserInfo(searchResults.toString());
                                user.setIsDeleted(searchResults.getString(ResponseParameters.IS_DELETED));
                                user.setIsBlocked(searchResults.getString(ResponseParameters.IS_BLOCKED));
                                users.add(user);
                            }
                            pagination = pagination + 6;
                            friendListRecyclerViewAdapter.notifyDataSetChanged2();
                            friendListRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            }

        } catch (Exception e) {
            llNotFound.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }

    public void showNoUser() {
        llNotFound.setVisibility(View.VISIBLE);
    }

}
