package com.codeholic.kotumb.app.Activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.codeholic.kotumb.app.Adapter.MessageRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.shahroz.svlibrary.interfaces.onSearchListener;
import com.shahroz.svlibrary.interfaces.onSimpleSearchActionsListener;
import com.shahroz.svlibrary.widgets.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;

public class MessageActivity extends AppCompatActivity implements onSimpleSearchActionsListener, onSearchListener, View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.tvNouser)
    TextView tvNouser;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    List<User> users = new ArrayList<>();
    List<User> searchedUsers = new ArrayList<>();
    MessageRecyclerViewAdapter messageRecyclerViewAdapter;
    int pagination = 0;
    int searchPagination = 0;
    private LinearLayoutManager linearLayoutManager;
    String userStatus="";
    private boolean mSearchViewAdded = false;
    private MaterialSearchView mSearchView;
    private WindowManager mWindowManager;
    private MenuItem searchItem;
    private boolean searchActive = false;
    private String queryText = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        ButterKnife.bind(this);
        initToolbar();
        setClickListener();
        setView("");
        IntentFilter filter = new IntentFilter();
        filter.addAction(ResponseParameters.UPDATE_CHAT);
      /*  Intent intent = getIntent();
        VideoData videoData=intent.getParcelableExtra(ResponseParameters.VIDEOS);
        Toast.makeText(this, videoData.getVideoId()+"  "+videoData.getVideoName()+"  "+videoData.getUserFirstName(), Toast.LENGTH_SHORT).show();
        */
        registerReceiver(receiver, filter);
    }

    // initToolbar method initializing view
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        SharedPreferencesMethod.setString(this, ResponseParameters.NOTIFICATIONS, "");// clearing notifications
        //setting screen title
        header.setText(getResources().getString(R.string.header_message_text));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        mSearchView = new MaterialSearchView(this);
        mSearchView.setOnSearchListener(this);
        mSearchView.setSearchResultsListener(this);
        mSearchView.setHintText(getResources().getString(R.string.invitations_search));

        tvNouser.setText(getResources().getString(R.string.no_message_found));

        if (mToolbar != null) {
            // Delay adding SearchView until Toolbar has finished loading
            mToolbar.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!mSearchViewAdded && mWindowManager != null) {
                            mWindowManager.addView(mSearchView,
                                    MaterialSearchView.getSearchViewLayoutParams(MessageActivity.this));
                            mSearchViewAdded = true;
                            System.out.println("Search Data "+mSearchViewAdded);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void setClickListener() {
        fab.setOnClickListener(this);
    }

    //setview for getting user connection
    private void setView(String query) {
        try {
            if (!Utility.isConnectingToInternet(this)) { // checking net connection
                @SuppressLint("WrongConstant") final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(View.VISIBLE);
                tvNouser.setText(getResources().getString(R.string.app_no_internet_error));
                return;
            }

            tvNouser.setText(getResources().getString(R.string.no_message_found));
            String url = API.MESSAGES_LIST + SharedPreferencesMethod.getUserId(this) + "/" + query+ "/?offset=0";
            System.out.println("Message URL     "+url);
            if (query.isEmpty()){
                API.sendRequestToServerGET(this, url, API.MESSAGES_LIST);// service call for getting messages
            }else
                API.sendRequestToServerGET(this, url, API.MESSAGES_LIST_SEARCH);// service call for getting messages
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            try {
                User user = intent.getParcelableExtra("USER");
                Log.e("TAG", "" + user.getUserInfo());
//                Toast.makeText(context, user.getState(), Toast.LENGTH_SHORT).show();
                if (users.size() > 0) {
                    for (int i = 0; i < users.size(); i++) {
                        if (users.get(i).getUserId().equalsIgnoreCase(user.getUserId())) {
                            users.remove(i);
                            if (queryText.isEmpty()) {
                                messageRecyclerViewAdapter.notifyItemRemoved(i);
                                messageRecyclerViewAdapter.notifyItemRangeChanged(0, users.size());
                            }
                            break;
                        }
                    }

                    JSONObject jsonObject = new JSONObject(user.getUserInfo());
                    if (jsonObject.has(ResponseParameters.LAST_MSG_FLAG)) {
                        if (jsonObject.getBoolean(ResponseParameters.LAST_MSG_FLAG)) {
                            users.add(0, user);
                            if (queryText.isEmpty()) {
                                messageRecyclerViewAdapter.notifyItemInserted(0);
                            }
                        }
                    }


                } else {
                    users.add(user);
                    if (queryText.isEmpty()) {
                        recyclerViewSetUp(users, pagination, queryText);
                    }
                }

                if (searchedUsers.size() > 0) {
                    for (int i = 0; i < searchedUsers.size(); i++) {
                        if (searchedUsers.get(i).getUserId().equalsIgnoreCase(user.getUserId())) {
                            searchedUsers.remove(i);
                            if (!queryText.isEmpty()) {
                                messageRecyclerViewAdapter.notifyItemRemoved(i);
                                messageRecyclerViewAdapter.notifyItemRangeChanged(0, users.size());
                            }
                            break;
                        }
                    }

                    JSONObject jsonObject = new JSONObject(user.getUserInfo());
                    if (jsonObject.has(ResponseParameters.LAST_MSG_FLAG)) {
                        if (jsonObject.getBoolean(ResponseParameters.LAST_MSG_FLAG)) {
                            JSONObject msgInfo = jsonObject.getJSONObject(ResponseParameters.LAST_MSG_INFO);
                            if (!msgInfo.getString(ResponseParameters.MESSAGEAT).trim().isEmpty()) {
                                searchedUsers.add(0, user);
                                if (!queryText.isEmpty()) {
                                    messageRecyclerViewAdapter.notifyItemInserted(0);
                                }
                            }
                        }
                    }

                } else {
                    searchedUsers.add(user);
                    if (!queryText.isEmpty()) {
                        recyclerViewSetUp(searchedUsers, pagination, queryText);
                    }
                }

            } catch (Exception e) {

            }
        }
    };




    //getResponse for getting list of urlImages
    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        llNotFound.setVisibility(View.GONE);
        Log.e("response", "" + outPut);
        System.out.println("Message Response "+ outPut.toString());
        try {
            if (i == 0) { // for getting urlImages message
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            userList = outPut.getJSONArray(ResponseParameters.CONNECTIONS);
                        }
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                        }
                        users = new ArrayList<>();
                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
                            System.out.println("Message Name    "+searchResults.getString(ResponseParameters.FirstName));
                            //  user.setRole(searchResults.getString(ResponseParameters.Role));
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));
                            user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                            user.setUserInfo(searchResults.toString());
                            if (searchResults.has("userInfo")) {
                                JSONObject userInfo = searchResults.getJSONObject("userInfo");
                                String isBlocked = userInfo.getString("isBlocked");
                                user.setIsBlocked(isBlocked);
                                user.setRole(userInfo.getString(ResponseParameters.Role));
                            }
//                            if (searchResults.has(ResponseParameters.LAST_MSG_FLAG)) {
//                                if (searchResults.getBoolean(ResponseParameters.LAST_MSG_FLAG)) {
                                    users.add(user);
//                                }
//                            }

                        }
                        if (queryText.isEmpty())
                            recyclerViewSetUp(users, pagination, queryText);
                    } else {
                        //no userfound
                        rvUserList.setVisibility(GONE);
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    rvUserList.setVisibility(GONE);
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    rvUserList.setVisibility(GONE);
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } else if (i == 1) {// for updating list
                users.remove(users.size() - 1); // removing of loading item
                messageRecyclerViewAdapter.notifyItemRemoved(users.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response

                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            userList = outPut.getJSONArray(ResponseParameters.CONNECTIONS);
                        }
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                        }

                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
//                            user.setRole(searchResults.getString(ResponseParameters.Role));
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                            user.setUserInfo(searchResults.toString());
                            String test = (String) searchResults.get("userId");
                            System.out.println("USER ID"+ test);
//                            if (searchResults.has(ResponseParameters.LAST_MSG_FLAG)) {
//                                if (searchResults.getBoolean(ResponseParameters.LAST_MSG_FLAG)) {
                                    users.add(user);
//                                }
//                            }
                            //userObjects.add(user);
                        }
                        if (userList.length() > 0) {
                            pagination = pagination + 6;
                            //updating recycler view
                            messageRecyclerViewAdapter.notifyDataSetChanged();
                            messageRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            }

            if (i == 2) { // for getting urlImages message
                searchedUsers = new ArrayList<>();
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        rvUserList.setVisibility(GONE);
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            userList = outPut.getJSONArray(ResponseParameters.CONNECTIONS);
                        }
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                        }
                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
//                            user.setRole(searchResults.getString(ResponseParameters.Role));
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));
                            user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                            user.setUserInfo(searchResults.toString());
                            if (searchResults.has(ResponseParameters.LAST_MSG_FLAG)) {
                                if (searchResults.getBoolean(ResponseParameters.LAST_MSG_FLAG)) {
                                    searchedUsers.add(user);
                                }
                            }
                            //searchedUsers.add(user);
                        }
                        if (!queryText.isEmpty())
                            recyclerViewSetUp(searchedUsers, searchPagination, queryText);
                    } else {
                        //no userfound
                        if (searchedUsers.size() == 0 && !queryText.isEmpty()) {
                            rvUserList.setVisibility(GONE);
                            llNotFound.setVisibility(View.VISIBLE);
                        }
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    if (searchedUsers.size() == 0 && !queryText.isEmpty()) {
                        rvUserList.setVisibility(GONE);
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    if (searchedUsers.size() == 0 && !queryText.isEmpty()) {
                        llNotFound.setVisibility(View.VISIBLE);
                        rvUserList.setVisibility(GONE);
                    }
                }
            } else if (i == 3) {// for updating list
                searchedUsers.remove(searchedUsers.size() - 1); // removing of loading item
                messageRecyclerViewAdapter.notifyItemRemoved(searchedUsers.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response

                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            userList = outPut.getJSONArray(ResponseParameters.CONNECTIONS);
                        }
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                        }

                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
//                            user.setRole(searchResults.getString(ResponseParameters.Role));
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
//                            Toast.makeText(this, user.getState(), Toast.LENGTH_SHORT).show();
                            user.setUserInfo(searchResults.toString());

                            if (searchResults.has(ResponseParameters.LAST_MSG_FLAG)) {
                                if (searchResults.getBoolean(ResponseParameters.LAST_MSG_FLAG)) {
                                    searchedUsers.add(user);
                                }
                            }

                            //searchedUsers.add(user);
                        }
                        if (userList.length() > 0 && !queryText.isEmpty()) {
                            searchPagination = searchPagination + 6;
                            //updating recycler view
                            messageRecyclerViewAdapter.notifyDataSetChanged();
                            messageRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            }

        } catch (Exception e) {
            //mo user found
            llNotFound.setVisibility(View.VISIBLE);
            rvUserList.setVisibility(GONE);
            e.printStackTrace();
        }
    }

    public String videoId="";
    //setting options for recycler adapter
    public void recyclerViewSetUp(final List<User> users, final int pagination_, final String searchQuery) {
        if (users.size() > 0) {
            llLoading.setVisibility(GONE);
            llNotFound.setVisibility(View.GONE);
            rvUserList.setVisibility(View.VISIBLE);
        }
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvUserList.setLayoutManager(linearLayoutManager);
        messageRecyclerViewAdapter = new MessageRecyclerViewAdapter(rvUserList, this, users);
        rvUserList.setAdapter(messageRecyclerViewAdapter);
        //load more setup for recycler adapter
        if (getIntent().hasExtra(ResponseParameters.VIDEOS)){
            messageRecyclerViewAdapter.setVideoShare(true);
            String s=getIntent().getStringExtra("videoid");
            //SharedPreferencesMethod.setString(this,"VIDEO_ID",s);
            videoId=s;
            //Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        }
        messageRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (users.size() >= 6) {

                    rvUserList.post(new Runnable() {
                        public void run() {
                            users.add(null);
                            messageRecyclerViewAdapter.notifyItemInserted(users.size() - 1);
                        }
                    });
                    if (searchQuery.isEmpty()) {
                        API.sendRequestToServerGET(MessageActivity.this, API.MESSAGES_LIST + SharedPreferencesMethod.getUserId(MessageActivity.this) + "/" + searchQuery + "/" + "/?offset=" + (pagination + 6), API.MESSAGES_LIST_UPDATE);
                    } else {
                        API.sendRequestToServerGET(MessageActivity.this, API.MESSAGES_LIST + SharedPreferencesMethod.getUserId(MessageActivity.this) + "/" + searchQuery + "/" + "/?offset=" + (searchPagination + 6), API.MESSAGES_LIST_SEARCH_UPDATE);
                    }
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        searchItem = menu.findItem(R.id.search);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                mSearchView.display();
                openKeyboard();
                return true;
            }
        });
        if (searchActive)
            mSearchView.display();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //setView(""); was causing problem, dont enable
    }

    private void openKeyboard() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mSearchView.getSearchView().dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                mSearchView.getSearchView().dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
            }
        }, 200);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSearch(String query) {
        Log.e("onSearch", "onSearch " + users.size());
        try {
            if (query.isEmpty()) {
                if (!queryText.isEmpty()) {
                    recyclerViewSetUp(users, pagination, query);
                }
            } else {
                recyclerViewSetUp(new ArrayList<User>(), 0, queryText);
                llLoading.setVisibility(View.VISIBLE);
                llNotFound.setVisibility(GONE);
                setView(query);
            }
            queryText = query;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void searchViewOpened() {
        Log.e("searchViewOpened", "searchViewOpened " + searchActive);
    }

    @Override
    public void searchViewClosed() {
        Log.e("searchViewClosed", "searchViewClosed");
        //Util.showSnackBarMessage(fab,"Search View Closed");
    }

    @Override
    public void onItemClicked(String item) {

    }

    @Override
    public void onScroll() {

    }

    @Override
    public void error(String localizedMessage) {

    }

    @Override
    public void onCancelSearch() {
        Log.e("onCancelSearch", "onCancelSearch");
        searchActive = false;
        mSearchView.hide();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(this, ConnectionsMessageActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroy();
    }
}
