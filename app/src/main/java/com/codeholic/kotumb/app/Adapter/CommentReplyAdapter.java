package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Model.ReplyComment;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import java.util.List;

public class CommentReplyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ReplyComment> replyComments;
    Activity activity;
//    private final int VIEW_TYPE_ITEM = 0;
//    private final int VIEW_TYPE_LOADING = 1;
//    private boolean isLoading;
//    private OnLoadMoreListener onLoadMoreListener;
//    private int visibleItemCount;
//    private int lastVisibleItem, totalItemCount;



    public CommentReplyAdapter(final Activity activity, List<ReplyComment> replyComments) {
        this.replyComments = replyComments;
        this.activity = activity;
//        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                visibleItemCount = linearLayoutManager.getChildCount();
//                totalItemCount = linearLayoutManager.getItemCount();
//                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
//                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
//                    if (onLoadMoreListener != null) {
//                        onLoadMoreListener.onLoadMore();
//                        Toast.makeText(activity, "Scroll List", Toast.LENGTH_SHORT).show();
//                    }
//                    isLoading = true;
//                }
//            }
//        });
    }


//    public void setLoaded() {
//        isLoading = false;
//    }
//
//    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
//        this.onLoadMoreListener = mOnLoadMoreListener;
//    }

//    @Override
//    public int getItemViewType(int position) {
//        return replyComments.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
//    }


    @Override
    public int getItemCount() {
        return replyComments.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
//        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.post_comment_item, parent, false);
            CommentReplyViewHolder viewHolder = new CommentReplyViewHolder(view);
            return viewHolder;
//        } else if (viewType == VIEW_TYPE_LOADING) {
//            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading_comments, parent, false);
//            return new LoadingViewHolder(view);
//        }
//        return null;
    }




//    private class LoadingViewHolder extends RecyclerView.ViewHolder {
//        public ProgressBar progressBar;
//        public LoadingViewHolder(View view) {
//            super(view);
//            progressBar = view.findViewById(R.id.progressBar1);
//        }
//    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final ReplyComment comment=replyComments.get(position);
        try{
            if (viewHolder instanceof  CommentReplyViewHolder) {
                CommentReplyViewHolder holder = (CommentReplyViewHolder) viewHolder;
                holder.tvUserName.setText(comment.getFirstName()+" "+comment.getLastName());
                holder.tvCommentContent.setText(comment.getReplyComment());
                System.out.println("Reply Data  "+comment.getFirstName()+" "+comment.getLastName()+ "  "+comment.getReplyComment());
                new AQuery(activity).id(holder.ivUserImage).image(API.imageUrl+comment.getReplyAvatar(), true, true, 300, R.drawable.default_profile);
                holder.comment_reply.setVisibility(View.GONE);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }


    public static class CommentReplyViewHolder extends RecyclerView.ViewHolder {
        View more;
        TextView tvUserName, tvTime, tvCommentContent,comment_reply;
        ImageView ivUserImage;
        LinearLayout cvContainer, llEditPost,comment_reply_container,comment_reply_list;
        RecyclerView comment_reply_recyclerview;
        ProgressBar comment_reply_loader;
        View viewLine;
        CommentReplyViewHolder(View view) {
            super(view);
            tvUserName = view.findViewById(R.id.tvUserName);
            ivUserImage = view.findViewById(R.id.ivUserImage);
            cvContainer = view.findViewById(R.id.cvContainer);
            tvTime = view.findViewById(R.id.tvTime);
            comment_reply = view.findViewById(R.id.comment_reply);
            tvCommentContent = view.findViewById(R.id.tvPostContent);
            viewLine = view.findViewById(R.id.viewLine);
            llEditPost = view.findViewById(R.id.llEditPost);
            more = view.findViewById(R.id.more);
            comment_reply_container=view.findViewById(R.id.comment_reply_container);
            comment_reply_list=view.findViewById(R.id.comment_reply_list);
            comment_reply_recyclerview=view.findViewById(R.id.comment_reply_recyclerview);
            comment_reply_loader=view.findViewById(R.id.comment_reply_loader);
        }

    }

}
