package com.codeholic.kotumb.app.Activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResumeTemplateActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    @BindView(R.id.container)
    ViewPager mViewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.edit_resume)
    TextView editResume;

    com.codeholic.kotumb.app.View.ProgressBar progressBar = null;

    int resumeid = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume_template);
        final User user = SharedPreferencesMethod.getUserInfo(this);
        ButterKnife.bind(this);
        initToolbar();
        setUpViewPager();

        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utility.isConnectingToInternet(ResumeTemplateActivity.this)) {
                    final Snackbar snackbar = Snackbar.make(view, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }
                progressBar = new com.codeholic.kotumb.app.View.ProgressBar(ResumeTemplateActivity.this);
                progressBar.show(getResources().getString(R.string.app_please_wait_text));
//                getResumeResponse();
                API.sendRequestToServerGET(ResumeTemplateActivity.this, API.SET_DEFAULT_RESUME + SharedPreferencesMethod.getUserId(ResumeTemplateActivity.this) + "/" + resumeid, API.SET_DEFAULT_RESUME);
           System.out.println("Default URL   "+API.SET_DEFAULT_RESUME + SharedPreferencesMethod.getUserId(ResumeTemplateActivity.this) + "/" + resumeid);
            }
        });
        editResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ResumeTemplateActivity.this, UpdateResumeActivity.class));
            }
        });
    }


    private void setUpViewPager() {
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(4);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                resumeid = tab.getPosition() + 1;
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        /*if (getIntent().hasExtra("SHOW"))
            header.setText(getResources().getString(R.string.share_resume));
        else*/
        header.setText(getResources().getString(R.string.home_view_resume_link));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_resume_template, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    public void getResponse(JSONObject outPut, int i) {
        try {
                progressBar.dismiss();
            Log.e("output", "" + outPut);
            System.out.println("Resume Output   "+outPut);
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Success)) {
                    final Snackbar snackbar = Snackbar.make(tabLayout, outPut.getString(ResponseParameters.Success), Toast.LENGTH_SHORT);
                    snackbar.show();
                    if (outPut.has(ResponseParameters.resumeid))
                        SharedPreferencesMethod.setString(this, ResponseParameters.resumeid, outPut.getString(ResponseParameters.resumeid));
                } else if (outPut.has(ResponseParameters.Errors)) {
                    final Snackbar snackbar = Snackbar.make(tabLayout, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            } else {
                final Snackbar snackbar = Snackbar.make(tabLayout, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                snackbar.show();
            }
        } catch (Exception e) {
          e.printStackTrace();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private ProgressBar pbLoading;
        private WebView wv;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_resume_template, container, false);
            wv = rootView.findViewById(R.id.wv);
            pbLoading = rootView.findViewById(R.id.pbLoading);

            DownloadWebPageTask task = new DownloadWebPageTask();
            task.execute(API.RESUME_TEMPLATE + "/" + getArguments().getInt(ARG_SECTION_NUMBER) + "/" + SharedPreferencesMethod.getUserId(getActivity()));

            return rootView;
        }

        private class DownloadWebPageTask extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... urls) {
                try {
                    if (!Utility.isConnectingToInternet(getActivity())) {
                        final Snackbar snackbar = Snackbar.make(wv, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                        snackbar.show();
                        return "";
                    }

                    String response = "";
                    for (String url : urls) {
                        DefaultHttpClient client = new DefaultHttpClient();
                        HttpGet httpGet = new HttpGet(url);
                        try {
                            HttpResponse execute = client.execute(httpGet);
                            InputStream content = execute.getEntity().getContent();

                            BufferedReader buffer = new BufferedReader(
                                    new InputStreamReader(content));
                            String s = "";
                            while ((s = buffer.readLine()) != null) {
                                response += s;
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return response;
                } catch (Exception e) {
                    e.printStackTrace();
                    return "";
                }
            }

            @Override
            protected void onPostExecute(String result) {
                if (!result.isEmpty()) {
                    pbLoading.setVisibility(View.GONE);
                    wv.getSettings().setUseWideViewPort(true);
                    wv.getSettings().setJavaScriptEnabled(true);
                    wv.setWebChromeClient(new WebChromeClient() {
                    });
                    wv.getSettings().setLoadWithOverviewMode(true);
                    wv.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            return true;
                        }
                    });
                    wv.setLongClickable(false);
                    Log.e("result", "" + result);
//                    wv.loadData(result, "text/html", "UTF-8");
                    wv.loadDataWithBaseURL(null,result, "text/html", "UTF-8", null);
                    System.out.println("The Result   "+    result);
                }
            }
        }


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a FavLinkFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            /*switch (position) {
                case 0:
                    return getResources().getString(R.string.template)+(position+1);
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
                case 3:
                    return "SECTION 4";
            }*/
            return getResources().getString(R.string.resume_templates_resume_text) + " " + (position + 1);
        }
    }
}
