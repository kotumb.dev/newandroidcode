package com.codeholic.kotumb.app.Model;

public class RewardObject {

    /**
     * rewardId": "332",
     * "userId": "706",
     * "points": "5.00",
     * "rewardType": "survey_reward",
     * "tType": "credit",
     * "createdAt": "2019-03-12 13:18:39",
     * "updatedAt": "2019-03-12 13:18:39"
     */

    private String rewardId;
    private String userId;
    private String points;
    private String rewardType;
    private String tType;
    private String createdAt;
    private String updatedAt;
    private String title;

    public RewardObject(String points, String rewardType, String createdAt) {
        this.points = points;
        this.rewardType = rewardType;
        this.createdAt = createdAt;
    }

    public RewardObject() {
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String  getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String gettType() {
        return tType;
    }

    public void settType(String tType) {
        this.tType = tType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getRewardTypeText() {
//        switch (rewardType){
//            case "survey_reward":
//                return " for taking survey";
//            case "message":
//                return "for messaging";
//                default:
//                    return "";
//        }
        return rewardType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
