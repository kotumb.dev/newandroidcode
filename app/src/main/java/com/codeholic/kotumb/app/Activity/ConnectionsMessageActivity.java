package com.codeholic.kotumb.app.Activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Adapter.MessageRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.shahroz.svlibrary.interfaces.onSearchListener;
import com.shahroz.svlibrary.interfaces.onSimpleSearchActionsListener;
import com.shahroz.svlibrary.widgets.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;

public class ConnectionsMessageActivity extends AppCompatActivity implements onSimpleSearchActionsListener, onSearchListener, View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.tvNouser)
    TextView tvNouser;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    List<User> users = new ArrayList<>();
    List<User> searchedUsers = new ArrayList<>();
    MessageRecyclerViewAdapter messageRecyclerViewAdapter;
    int pagination = 0;
    int searchPagination = 0;
    private LinearLayoutManager linearLayoutManager;

    private boolean mSearchViewAdded = false;
    private MaterialSearchView mSearchView;
    private WindowManager mWindowManager;
    private MenuItem searchItem;
    private boolean searchActive = false;
    private String queryText = "";
    String userStatus="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        ButterKnife.bind(this);
        initToolbar();
        setClickListener();
        setView("");
    }

    // initToolbar method initializing view
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        //setting screen title
        header.setText(getResources().getString(R.string.connections_title));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        mSearchView = new MaterialSearchView(this);
        mSearchView.setOnSearchListener(this);
        mSearchView.setSearchResultsListener(this);
        mSearchView.setHintText(getResources().getString(R.string.invitations_search));

        if (mToolbar != null) {
            // Delay adding SearchView until Toolbar has finished loading
            mToolbar.post(new Runnable() {
                @Override
                public void run() {
                    if (!mSearchViewAdded && mWindowManager != null) {
                        mWindowManager.addView(mSearchView,
                                MaterialSearchView.getSearchViewLayoutParams(ConnectionsMessageActivity.this));
                        mSearchViewAdded = true;
                    }
                }
            });
        }
        fab.hide();
    }

    private void setClickListener() {
        fab.setOnClickListener(this);
    }

    //setview for getting user connection
    private void setView(String query) {
        try {
            if (!Utility.isConnectingToInternet(this)) { // checking net connection
                final Snackbar snackbar = Snackbar.make(rvUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            if (query.isEmpty()){
                API.sendRequestToServerGET(this, API.CONNECTECTIONS + SharedPreferencesMethod.getUserId(this) + "/" + SharedPreferencesMethod.getUserId(this) + "/" + query + "/?offset=0", API.CONNECTECTIONS);
            }else{
                API.sendRequestToServerGET(this, API.CONNECTECTIONS + SharedPreferencesMethod.getUserId(this) + "/" + SharedPreferencesMethod.getUserId(this) + "/" + query + "/?offset=0", API.CONNECTECTIONS_SEARCH);
                System.out.println("Search URL 1     "+ API.CONNECTECTIONS + SharedPreferencesMethod.getUserId(this) + "/" + SharedPreferencesMethod.getUserId(this) + "/" + query + "/?offset=0");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    //getResponse for getting list of urlImages
    public void getResponse(JSONObject outPut, int i) {
        llLoading.setVisibility(GONE);
        llNotFound.setVisibility(View.GONE);
        Log.e("response", "" + outPut);
        try {
            if (i == 0) { // for getting urlImages message
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            userList = outPut.getJSONArray(ResponseParameters.CONNECTIONS);
                        }
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                        }
                        users = new ArrayList<>();
                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                            user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                            user.setUserInfo(searchResults.toString());
                            if (searchResults.has("userInfo")) {
                                JSONObject userInfo = searchResults.getJSONObject("userInfo");
                                String isBlocked = userInfo.getString("isBlocked");
                                user.setIsBlocked(isBlocked);
                                user.setRole(userInfo.getString(ResponseParameters.Role));
                            }
                            users.add(user);
                        }
                        if (queryText.isEmpty())
                            recyclerViewSetUp(users, pagination, queryText);
                    } else {
                        //no userfound
                        rvUserList.setVisibility(GONE);
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    rvUserList.setVisibility(GONE);
                    llNotFound.setVisibility(View.VISIBLE);
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    llNotFound.setVisibility(View.VISIBLE);
                }
            } else if (i == 1) {// for updating list


                users.remove(users.size() - 1); // removing of loading item
                messageRecyclerViewAdapter.notifyItemRemoved(users.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response

                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            userList = outPut.getJSONArray(ResponseParameters.CONNECTIONS);
                        }
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                        }

                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));
                            user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                            user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                            user.setUserInfo(searchResults.toString());
                            users.add(user);
                        }
                        if (userList.length() > 0) {

                            pagination = pagination + 6;

                            Log.e("TAG", "TEST"+pagination);
                            System.out.println("Paginationas   "+pagination);
                            //updating recycler view
                            messageRecyclerViewAdapter.notifyDataSetChanged();
                            messageRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            }

            if (i == 2) { // for getting urlImages message
                searchedUsers = new ArrayList<>();
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error response
                        rvUserList.setVisibility(GONE);
                        llNotFound.setVisibility(View.VISIBLE);
                    } else if (outPut.has(ResponseParameters.Success)) { // success response
                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            userList = outPut.getJSONArray(ResponseParameters.CONNECTIONS);
                        }
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                        }
                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                            user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                            user.setUserInfo(searchResults.toString());
                            searchedUsers.add(user);
                        }
                        if (!queryText.isEmpty())
                            recyclerViewSetUp(searchedUsers, searchPagination, queryText);
                    } else {
                        //no userfound
                        if (searchedUsers.size() == 0 && !queryText.isEmpty())
                            rvUserList.setVisibility(GONE);
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    //error response
                    if (searchedUsers.size() == 0 && !queryText.isEmpty()) {
                        rvUserList.setVisibility(GONE);
                        llNotFound.setVisibility(View.VISIBLE);
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    //no net connection
                    if (searchedUsers.size() == 0 && !queryText.isEmpty()) {
                        llNotFound.setVisibility(View.VISIBLE);
                        rvUserList.setVisibility(GONE);
                    }
                }
            } else if (i == 3) {// for updating list
                searchedUsers.remove(searchedUsers.size() - 1); // removing of loading item
                messageRecyclerViewAdapter.notifyItemRemoved(searchedUsers.size());
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) { // success response

                        JSONArray userList = new JSONArray();
                        if (outPut.has(ResponseParameters.CONNECTIONS)) {
                            userList = outPut.getJSONArray(ResponseParameters.CONNECTIONS);
                        }
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            userList = outPut.getJSONArray(ResponseParameters.RESULTS);
                        }

                        for (int j = 0; j < userList.length(); j++) {
                            JSONObject searchResults = userList.getJSONObject(j);
                            User user = new User();
                            user.setFirstName(searchResults.getString(ResponseParameters.FirstName));
                            user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                            user.setLastName(searchResults.getString(ResponseParameters.LastName));
                            user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                            user.setUserId(searchResults.getString(ResponseParameters.UserId));
                            user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                            user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                            user.setUserInfo(searchResults.toString());
                            searchedUsers.add(user);
                        }
                        if (userList.length() > 0 && !queryText.isEmpty()) {
                            searchPagination = searchPagination + 6;
                            //updating recycler view
                            messageRecyclerViewAdapter.notifyDataSetChanged();
                            messageRecyclerViewAdapter.setLoaded();
                        }
                    }
                }
            }

        } catch (Exception e) {
            //mo user found
            llNotFound.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }

    }





    //setting options for recycler adapter
    public void recyclerViewSetUp(final List<User> users, final int pagination_, final String searchQuery) {
        if (users.size() > 0) {
            llLoading.setVisibility(GONE);
            llNotFound.setVisibility(View.GONE);
            rvUserList.setVisibility(View.VISIBLE);
        }
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvUserList.setLayoutManager(linearLayoutManager);
        messageRecyclerViewAdapter = new MessageRecyclerViewAdapter(rvUserList, this, users);
        messageRecyclerViewAdapter.setConnections(true);
        if(getIntent().hasExtra(ResponseParameters.SHARERESUME)){
            messageRecyclerViewAdapter.setFromShareResume(true);
        }
//        if(getIntent().hasExtra(ResponseParameters.VIDEOS)){
//            messageRecyclerViewAdapter.setVideoShare(true);
//            VideoData videoData=getIntent().getParcelableExtra(ResponseParameters.VIDEOS);
//            SharedPreferencesMethod.setString(this,"VIDEO_FILE",videoData.getVideoName());
//            SharedPreferencesMethod.setString(this,"VIDEO_ID",videoData.getVideoId());
//            SharedPreferencesMethod.setString(this,"VIDEO_USER",videoData.getUserFirstName());
//        }
        rvUserList.setAdapter(messageRecyclerViewAdapter);
        //load more setup for recycler adapter
        messageRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (users.size() >= 6) {

                    rvUserList.post(new Runnable() {
                        public void run() {
                            users.add(null);
                            messageRecyclerViewAdapter.notifyItemInserted(users.size() - 1);
                        }
                    });
                    if (searchQuery.isEmpty()) {
                        String url =API.CONNECTECTIONS + SharedPreferencesMethod.getUserId(ConnectionsMessageActivity.this) + "/" + SharedPreferencesMethod.getUserId(ConnectionsMessageActivity.this) + "/" + searchQuery + "/" + "/?offset=" + (searchPagination + 6);

                     System.out.println("Search URL    "+url);

                        Log.e("inside if", "insode if "+(pagination+6));
                        API.sendRequestToServerGET(ConnectionsMessageActivity.this, API.CONNECTECTIONS + SharedPreferencesMethod.getUserId(ConnectionsMessageActivity.this) + "/" + SharedPreferencesMethod.getUserId(ConnectionsMessageActivity.this) + "/" + searchQuery + "/" + "/?offset=" + (pagination + 6), API.CONNECTECTIONS_UPDATE);
                    }
                    else {
                        Log.e("inside else", "insode else "+(searchPagination+6));
                        API.sendRequestToServerGET(ConnectionsMessageActivity.this, API.CONNECTECTIONS + SharedPreferencesMethod.getUserId(ConnectionsMessageActivity.this) + "/" + SharedPreferencesMethod.getUserId(ConnectionsMessageActivity.this) + "/" + searchQuery + "/" + "/?offset=" + (searchPagination + 6) , API.CONNECTECTIONS_SEARCH_UPDATE);
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        searchItem = menu.findItem(R.id.search);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                mSearchView.display();
                openKeyboard();
                return true;
            }
        });
        if (searchActive)
            mSearchView.display();
        return true;
    }

    private void openKeyboard() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mSearchView.getSearchView().dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                mSearchView.getSearchView().dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
            }
        }, 200);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSearch(String query) {
        Log.e("onSearch", "onSearch " + users.size());
        try {
            if (query.isEmpty()) {
                if (!queryText.isEmpty())
                    recyclerViewSetUp(users, pagination, query);
            } else {
                recyclerViewSetUp(new ArrayList<User>(), 0, queryText);
                llLoading.setVisibility(View.VISIBLE);
                llNotFound.setVisibility(GONE);
                setView(query);
            }
            queryText = query;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void searchViewOpened() {
        Log.e("searchViewOpened", "searchViewOpened " + searchActive);
    }

    @Override
    public void searchViewClosed() {
        Log.e("searchViewClosed", "searchViewClosed");
        //Util.showSnackBarMessage(fab,"Search View Closed");
    }

    @Override
    public void onItemClicked(String item) {

    }

    @Override
    public void onScroll() {

    }

    @Override
    public void error(String localizedMessage) {

    }

    @Override
    public void onCancelSearch() {
        Log.e("onCancelSearch", "onCancelSearch");
        searchActive = false;
        mSearchView.hide();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                break;
        }
    }
}
