package com.codeholic.kotumb.app.Activity;

import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codeholic.kotumb.app.Adapter.FriendListRecyclerViewAdapter;
import com.codeholic.kotumb.app.Adapter.UserRecyclerViewAdapter;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InviteSearchActivity extends AppCompatActivity {
    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.searchview)
    SearchView searchview;

    @BindView(R.id.rvUserList)
    RecyclerView rvUserList;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.no_data)
    TextView no_data;

    int pagination = 0;
    private String newText = "";
    ArrayList<User> userObjects = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    UserRecyclerViewAdapter userRecyclerViewAdapter;
    FriendListRecyclerViewAdapter friendListRecyclerViewAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_search);
        ButterKnife.bind(this);
        try{
            Intent intent = getIntent();
            String name = intent.getStringExtra("page");
                searchview.setQueryHint(name);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        initToolbar();
        searchViewSetUp();
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void searchViewSetUp() {
        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {


            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                InviteSearchActivity.this.newText = newText;
                userObjects.clear();
                pagination = 0;
                if(searchview.getQueryHint().toString().equalsIgnoreCase("Suggetions Search")){
                    if (userRecyclerViewAdapter != null) {
                        userRecyclerViewAdapter.notifyDataSetChanged();
                    }
                }else if(searchview.getQueryHint().toString().equalsIgnoreCase("Recieved Search")){
                    if (friendListRecyclerViewAdapter != null) {
                        friendListRecyclerViewAdapter.notifyDataSetChanged();
                    }
                }else if(searchview.getQueryHint().toString().equalsIgnoreCase("Sent Search")){
                    if (friendListRecyclerViewAdapter != null) {
                        friendListRecyclerViewAdapter.notifyDataSetChanged();
                    }
                }
                //rvUserList.setAdapter(getAdapter());

                if (!newText.isEmpty()) {
                    if (searchview.getQueryHint().toString().equalsIgnoreCase("Sent Search")){
                        String url=API.INVITATIONS_SENT + SharedPreferencesMethod.getUserId(InviteSearchActivity.this)+"/"+newText+"/?offset=" + pagination;
                        SearchData(url,"sent");
                        no_data.setVisibility(View.GONE);
                        userObjects.clear();
                    }else if(searchview.getQueryHint().toString().equalsIgnoreCase("Suggetions Search")){
                        String url =API.SUGGESTION_USERS+ "0" +"/"+newText+ "/?userid=" + SharedPreferencesMethod.getUserId(InviteSearchActivity.this);
                        SearchData(url,"sugg");
                        no_data.setVisibility(View.GONE);
                        userObjects.clear();
                    }else if(searchview.getQueryHint().toString().equalsIgnoreCase("Recieved Search")){
                        String url=API.INVITATIONS_RECEIVED + SharedPreferencesMethod.getUserId(InviteSearchActivity.this) +"/" +newText+"/?offset=" + pagination;
                        SearchData(url,"get");
                        no_data.setVisibility(View.GONE);
                        userObjects.clear();
                    }else{
                        llLoading.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                        userObjects.clear();
                    }
                }
                return false;
            }
        });

    }



    private void SearchData(final String url, final String status){
        System.out.println("Status Have   "+status);
        StringRequest postRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    System.out.println("Main Response  "+url);
                    JSONObject object=new JSONObject(response);
                    if (status.equalsIgnoreCase("sugg")){
                        System.out.println("Status   "+status);
                        onGetResponseSearch(object,"results","sugg");
                        onGetLoadMoreResponse(userRecyclerViewAdapter,object,"results","sugg");
                    }else if (status.equalsIgnoreCase("get")){
                        System.out.println("Status   "+status);
                        onGetResponseSearch(object,"invitations","get");
                        onGetLoadMoreResponse(friendListRecyclerViewAdapter,object,"invitations","get");
                    }else if (status.equalsIgnoreCase("sent")){
                        System.out.println("Status   "+status);
                        onGetResponseSearch(object,"invitations","sent");
                        onGetLoadMoreResponse(friendListRecyclerViewAdapter,object,"invitations","sent");
                    }

                }catch (Exception ex){
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Response  Error  "+error.toString());
            }
        });
        Volley.newRequestQueue(InviteSearchActivity.this).add(postRequest);
    }





    private void onGetLoadMoreResponse(RecyclerView.Adapter adapter,JSONObject outPut, String array,String status) throws JSONException {
        llLoading.setVisibility(View.GONE);
        userObjects.remove(userObjects.size() - 1); // remove loading item
        adapter.notifyItemRemoved(userObjects.size());
            if (outPut.has(ResponseParameters.Errors)) {
            }
                if (outPut.has(array)) {
                    JSONArray jsonArray = outPut.getJSONArray(array);
                    for (int j = 0; j < jsonArray.length(); j++) {
                        JSONObject searchResults = jsonArray.getJSONObject(j);
                        System.out.println("After Result  "+searchResults.toString());
                        User user = new User();
                        user.setFirstName(searchResults.getString(ResponseParameters.FirstName));
                        user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                        user.setLastName(searchResults.getString(ResponseParameters.LastName));
                        user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                        user.setUserId(searchResults.getString(ResponseParameters.UserId));
                        user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                        user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                        user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                        user.setCity(searchResults.getString(ResponseParameters.City));
                        user.setState(searchResults.getString(ResponseParameters.State));
                        user.setUserInfo(searchResults.toString());
                        userObjects.add(user);
                    }
                    pagination = pagination + 6;
                    //add next urlImages
                    if (status.equalsIgnoreCase("sugg")){
                        userRecyclerViewAdapter.notifyDataSetChanged();
                        userRecyclerViewAdapter.setLoaded();
                        llLoading.setVisibility(View.GONE);
                    }else if (status.equalsIgnoreCase("sent")){
                        friendListRecyclerViewAdapter.notifyDataSetChanged();
                        friendListRecyclerViewAdapter.setLoaded();
                        llLoading.setVisibility(View.GONE);
                    }else if (status.equalsIgnoreCase("get")){
                        friendListRecyclerViewAdapter.notifyDataSetChanged();
                        friendListRecyclerViewAdapter.setLoaded();
                        llLoading.setVisibility(View.GONE);
                    }
                }


    }








    private void onGetResponseSearch(JSONObject outPut, final String array, String status) throws JSONException {
        llLoading.setVisibility(View.GONE);
            if (outPut.has(ResponseParameters.Errors)) { // error
                final Snackbar snackbar = Snackbar.make(rvUserList, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                snackbar.show();
            }
                if (outPut.has(array)) {
                    JSONArray outPutJSONArray = outPut.getJSONArray(array);
                    userObjects.clear();

                    userObjects = new ArrayList<>();
                    for (int j = 0; j < outPutJSONArray.length(); j++) {
                        JSONObject searchResults = outPut.getJSONArray(array).getJSONObject(j);
                        System.out.println("After Result2  "+searchResults.toString());
                        User user = new User();
                        user.setFirstName(searchResults.getString(ResponseParameters.FirstName));   user.setMiddleName(searchResults.getString(ResponseParameters.MiddleName));
                        user.setLastName(searchResults.getString(ResponseParameters.LastName));
                        user.setAvatar(searchResults.getString(ResponseParameters.Avatar));
                        user.setUserId(searchResults.getString(ResponseParameters.UserId));
                        user.setProfileHeadline(searchResults.getString(ResponseParameters.PROFILE_HEADLINE));
                        user.setProfileSummary(searchResults.getString(ResponseParameters.PROFILE_SUMMERY));
                        user.setAadhaarInfo(searchResults.getString(ResponseParameters.AADHARINFO));
                        user.setCity(searchResults.getString(ResponseParameters.City));
                        user.setState(searchResults.getString(ResponseParameters.State));
                        user.setUserInfo(searchResults.toString());
                        userObjects.add(user);
                    }
                    if (status.equalsIgnoreCase("sent")){
                        linearLayoutManager = new LinearLayoutManager(InviteSearchActivity.this);
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserList.setLayoutManager(linearLayoutManager);
                        rvUserList.setNestedScrollingEnabled(false);
//                        userRecyclerViewAdapter = new UserRecyclerViewAdapter(rvUserList,InviteSearchActivity.this, userObjects);
                        friendListRecyclerViewAdapter = new FriendListRecyclerViewAdapter(rvUserList, this, userObjects, null);
                        rvUserList.setAdapter(friendListRecyclerViewAdapter);
                        System.out.println("Find User   "+status);
                        friendListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                            if (userObjects.size() >= 6) {
                                Log.e("inside", "inside");
                                rvUserList.post(new Runnable() {
                                    public void run() {
                                        userObjects.add(null);
                                        friendListRecyclerViewAdapter.notifyItemInserted(userObjects.size() - 1);
                                    }
                                });
                                String url = API.INVITATIONS_SENT + SharedPreferencesMethod.getUserId(InviteSearchActivity.this)+"/"+newText+"/?offset=" + pagination+6;
                                API.sendRequestToServerGET(InviteSearchActivity.this, url, API.INVITATIONS_SENT); // service call for user list


                            }
                            }
                        });
                        llLoading.setVisibility(View.GONE);
                    }else if (status.equalsIgnoreCase("sugg")){
                        linearLayoutManager = new LinearLayoutManager(InviteSearchActivity.this);
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserList.setLayoutManager(linearLayoutManager);
                        rvUserList.setNestedScrollingEnabled(false);
                        userRecyclerViewAdapter = new UserRecyclerViewAdapter(rvUserList,InviteSearchActivity.this, userObjects);
                        rvUserList.setAdapter(userRecyclerViewAdapter);
                        System.out.println("Find User   "+status);
                        userRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                            if (userObjects.size() >= 6) {
                                Log.e("inside", "inside");
                                rvUserList.post(new Runnable() {
                                    public void run() {
                                        userObjects.add(null);
                                        userRecyclerViewAdapter.notifyItemInserted(userObjects.size() - 1);
                                    }
                                });
                                    String url = API.SUGGESTION_USERS+(pagination+6)+"/"+newText+ "/?userid=" + SharedPreferencesMethod.getUserId(InviteSearchActivity.this);
                                    API.sendRequestToServerGET(InviteSearchActivity.this, url, API.SUGGESTION_USERS); // service call for user list


                            }
                            }
                        });
                        llLoading.setVisibility(View.GONE);
                    }else if (status.equalsIgnoreCase("get")){
                        linearLayoutManager = new LinearLayoutManager(InviteSearchActivity.this);
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rvUserList.setLayoutManager(linearLayoutManager);
                        rvUserList.setNestedScrollingEnabled(false);
//                        userRecyclerViewAdapter = new UserRecyclerViewAdapter(rvUserList,InviteSearchActivity.this, userObjects);
                        friendListRecyclerViewAdapter = new FriendListRecyclerViewAdapter(rvUserList, this, userObjects, null);
                        rvUserList.setAdapter(friendListRecyclerViewAdapter);
                        System.out.println("Find User   "+status);
                        friendListRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                            @Override
                            public void onLoadMore() {
                            if (userObjects.size() >= 6) {
                                Log.e("inside", "inside");
                                rvUserList.post(new Runnable() {
                                    public void run() {
                                        userObjects.add(null);
                                        friendListRecyclerViewAdapter.notifyItemInserted(userObjects.size() - 1);
                                    }
                                });
                                String url = API.INVITATIONS_RECEIVED + SharedPreferencesMethod.getUserId(InviteSearchActivity.this) +"/" +newText+"/?offset=" + (pagination+6);
                                API.sendRequestToServerGET(InviteSearchActivity.this, url, API.INVITATIONS_RECEIVED); // service call for user list


                            }
                            }
                        });
                        llLoading.setVisibility(View.GONE);
                    }
                }


    }

}
