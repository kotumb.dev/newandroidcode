package com.codeholic.kotumb.app.Adapter;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Activity.GroupInfoActivity;
import com.codeholic.kotumb.app.Interface.OnLoadMoreListener;
import com.codeholic.kotumb.app.Interface.OnResponseListener;
import com.codeholic.kotumb.app.Model.GroupMembers;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class MemberListRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String BLOCK_CODE = "1";
    List<GroupMembers> groupMembers;
    Activity activity;
    AlertDialog alert;
    AlertDialog confirmAlert;


    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    public boolean isLoading;

    private OnLoadMoreListener onLoadMoreListener;
    private int visibleItemCount;
    private int lastVisibleItem, totalItemCount;
    public boolean isMember = false;
    public boolean isRequest = false;
    public boolean isInvited = false;
    public boolean isBlocked = false;
    private int type = 0;
    public String myRole;
    private String newRoleToAssign = "0";


    public void setImageBaseUrl(String base_url) {
        this.base_url = base_url;
    }

    String base_url = "";

    public MemberListRecyclerViewAdapter(RecyclerView recyclerView, Activity activity, List<GroupMembers> groupMembers) {
        this.groupMembers = groupMembers;
        this.activity = activity;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleItemCount)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }


    @Override
    public int getItemViewType(int position) {
        return groupMembers.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return groupMembers.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_ITEM) {
            View view = null;
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.member_list_item, parent, false);
            GroupViewHolder GroupViewHolder = new GroupViewHolder(view);
            return GroupViewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
    }

    public void setType(int type) {
        this.type = type;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, final int position) {
        final GroupMembers groupMember = this.groupMembers.get(position);
        if (viewholder instanceof GroupViewHolder) {
            GroupViewHolder holder = (GroupViewHolder) viewholder;
            String text = "" + groupMember.firstName.trim() + " " + groupMember.middleName.trim() + " " + groupMember.lastName.trim();
            holder.tvUserName.setText(text.replaceAll("  ", " "));

            if (groupMember.role.trim().equalsIgnoreCase("1") || groupMember.role.trim().isEmpty()) {
                holder.tvAdminText.setText(activity.getResources().getString(R.string.group_info_member_btn_text));
                //holder.llAdminTextContainer.setVisibility(View.GONE);
            } else {
                holder.tvAdminText.setText(activity.getResources().getString(R.string.group_info_admin_btn_text));
                //holder.llAdminTextContainer.setVisibility(View.VISIBLE);
            }
            new AQuery(activity).id(holder.ivGroupImage).image(base_url + groupMember.avatar.trim(), true, true, 300, R.drawable.ic_user);

            if (type == 0) {
                setOnClickListener(holder, groupMember, position);
            }

            if (groupMember.isBlocked.equalsIgnoreCase(BLOCK_CODE)) {
                holder.tvdesc.setVisibility(View.VISIBLE);
            } else {
                holder.tvdesc.setVisibility(View.GONE);
            }

            if (!isMember) {
                holder.llAdminTextContainer.setVisibility(View.GONE);
            }

            holder.tvUserName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 API.openOtherUserProfile(groupMember.userId,activity);
                }
            });
        } else if (viewholder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) viewholder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }



    }





    public void setOnClickListener(final GroupViewHolder holder, final GroupMembers group, final int position) {
        holder.rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userId = SharedPreferencesMethod.getUserId(activity);
                if (!groupMembers.get(position).userId.equalsIgnoreCase(userId))
                    showPopUp(position);
            }
        });
    }

    private void showPopUp(String title) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_normal, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final TextView tvTitle = dialogView.findViewById(R.id.title);
        final TextView tvYes = dialogView.findViewById(R.id.tvYes);

        tvTitle.setText(title);
        //tvYes.setText(activity.getResources().getString(R.string.request_accept_btn_text));

        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmAlert.dismiss();
            }
        });

        confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();

    }


    private void showPopUp(final int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_group_members, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llMakeAdmin = dialogView.findViewById(R.id.llMakeAdmin);
        final LinearLayout llRemoveUser = dialogView.findViewById(R.id.llRemoveUser);
        final LinearLayout llBlockUser = dialogView.findViewById(R.id.llBlockUser);
        final LinearLayout llUnBlockUser = dialogView.findViewById(R.id.llUnBlockUser);
        final LinearLayout llAcceptRequest = dialogView.findViewById(R.id.llAcceptRequest);
        final LinearLayout llReportUser = dialogView.findViewById(R.id.llReportUser);
        final LinearLayout llRemoveAdmin = dialogView.findViewById(R.id.llRemoveAdmin);

        final GroupMembers groupMember = MemberListRecyclerViewAdapter.this.groupMembers.get(position);
        if (((GroupInfoActivity) activity).group.role.trim().equalsIgnoreCase("0")) {// if current user is not sdmin then open click options

            if (isMember) {
                llAcceptRequest.setVisibility(View.GONE);
                llUnBlockUser.setVisibility(View.GONE);
                if (groupMember.role.equalsIgnoreCase("0")) { // 0 for admin 1 for simple member
                    llMakeAdmin.setVisibility(View.GONE);
                    llRemoveAdmin.setVisibility(View.VISIBLE);
                } else {
                    llReportUser.setVisibility(View.VISIBLE); // member can be reported by admin
                    llMakeAdmin.setVisibility(View.VISIBLE);
                    llRemoveAdmin.setVisibility(View.GONE);
                }
            }

            if (isInvited) {
                llAcceptRequest.setVisibility(View.GONE);
                llUnBlockUser.setVisibility(View.GONE);
                llMakeAdmin.setVisibility(View.GONE);
                llBlockUser.setVisibility(View.GONE);
            }

            if (isRequest) {
                llUnBlockUser.setVisibility(View.GONE);
                llMakeAdmin.setVisibility(View.GONE);
                llBlockUser.setVisibility(View.GONE);
            }

            if (isBlocked) {
                llAcceptRequest.setVisibility(View.GONE);
                llMakeAdmin.setVisibility(View.GONE);
                llBlockUser.setVisibility(View.GONE);
            }


        } else {
            llMakeAdmin.setVisibility(View.GONE);
            llRemoveUser.setVisibility(View.GONE);
            llBlockUser.setVisibility(View.GONE);
            llUnBlockUser.setVisibility(View.GONE);
            llAcceptRequest.setVisibility(View.GONE);
            if (isMember) {
                if (!groupMember.role.equalsIgnoreCase("0")) { // 0 for admin 1 for simple member
                    llReportUser.setVisibility(View.VISIBLE); // member can be reported by member
                } else {
                    return;
                }
            } else {
                return;
            }
        }

        llMakeAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String adminId = SharedPreferencesMethod.getUserId(activity);
                String url = API.GROUP_CHANGE_MEMBER + groupMember.groupId + "/" + groupMember.userId + "/0" + "?adminid=" + adminId;
                //API.sendRequestToServerGET(activity, url, API.GROUP_CHANGE_MEMBER);
                newRoleToAssign = "0";
                showPopUp(activity.getResources().getString(R.string.make_admin_user_dialog_msg), url, API.GROUP_CHANGE_MEMBER, position, null);
                alert.dismiss();
            }
        });
        llRemoveUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = API.GROUP_DELETE_MEMBER + groupMember.groupId + "/" + groupMember.userId;
                //API.sendRequestToServerGET(activity, url, API.GROUP_DELETE_MEMBER);

                showPopUp(activity.getResources().getString(R.string.delete_user_dialog_msg), url, API.GROUP_DELETE_MEMBER, position, null);


                Log.e("TAG", "" + url);

                alert.dismiss();
            }
        });
        llBlockUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = API.GROUP_BLOCK_MEMBER + groupMember.groupId + "/" + groupMember.userId;
                //API.sendRequestToServerGET(activity, url, API.GROUP_BLOCK_MEMBER);

                showPopUp(activity.getResources().getString(R.string.block_user_dialog_msg), url, API.GROUP_BLOCK_MEMBER, position, null);
                alert.dismiss();

            }
        });
        llUnBlockUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    alert.dismiss();
                    return;
                }
                API.setResponseListener(new OnResponseListener() {
                    @Override
                    public void onGotResponse(JSONObject response) {
                        if (response.has("errors")) {
                            try {
                                showPopUp(response.getString("errors"));
                            } catch (JSONException e) {
                                showPopUp("Group user limit exceeded");
                                e.printStackTrace();
                            }
                        } else {
                            notifyItemRemoved(position);
                            MemberListRecyclerViewAdapter.this.groupMembers.remove(position);
                            notifyItemRangeChanged(0, MemberListRecyclerViewAdapter.this.groupMembers.size());
                        }
                    }
                });

                String url = API.GROUP_ACCEPT_MEMBER + groupMember.groupId + "/" + groupMember.userId + "/" + SharedPreferencesMethod.getUserId(activity);
                API.sendRequestToServerGET(activity, url, API.GROUP_ACCEPT_MEMBER);
                alert.dismiss();
                hideLayout();
            }
        });
        llAcceptRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    alert.dismiss();
                    return;
                }
                String url = API.GROUP_ACCEPT_MEMBER + groupMember.groupId + "/" + groupMember.userId + "/" + SharedPreferencesMethod.getUserId(activity);
                API.sendRequestToServerGET(activity, url, API.GROUP_ACCEPT_MEMBER);
                notifyItemRemoved(position);
                MemberListRecyclerViewAdapter.this.groupMembers.remove(position);
                notifyItemRangeChanged(0, MemberListRecyclerViewAdapter.this.groupMembers.size());
                alert.dismiss();
                hideLayout();
            }
        });

        llReportUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    alert.dismiss();
                    return;
                }

                HashMap<String, Object> input = new HashMap<>();
                input.put(RequestParameters.FROMUSERID, "" + SharedPreferencesMethod.getUserId(activity));
                input.put(RequestParameters.GROUPID, "" + ((GroupInfoActivity) activity).group.groupId);
                input.put(RequestParameters.TOUSERID, "" + groupMember.userId);
                //input.put(RequestParameters.REASON, "");


                String title = activity.getResources().getString(R.string.user_report_btn_text);
                if (myRole.equalsIgnoreCase("1")) {
                    reportUser(title, API.GROUP_USER_REPORT, input);
                } else {
                    title = activity.getResources().getString(R.string.report_user_title);
                    reportUser(title, API.USER_REPORT, input);
                }
                alert.dismiss();
            }
        });

        llRemoveAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String adminId = SharedPreferencesMethod.getUserId(activity);
                String url = API.GROUP_CHANGE_MEMBER + groupMember.groupId + "/" + groupMember.userId + "/1" + "?adminid=" + adminId;
                ;
                //API.sendRequestToServerGET(activity, url, API.GROUP_CHANGE_MEMBER);
                newRoleToAssign = "1";
                showPopUp(activity.getResources().getString(R.string.remove_admin_user_dialog_msg), url, API.GROUP_CHANGE_MEMBER, position, null);
                alert.dismiss();
            }
        });

        if (groupMember.isBlocked.equalsIgnoreCase(BLOCK_CODE)) {
            llAcceptRequest.setVisibility(View.GONE);
            llBlockUser.setVisibility(View.GONE);
            llMakeAdmin.setVisibility(View.GONE);
            llRemoveAdmin.setVisibility(View.GONE);
            llUnBlockUser.setVisibility(View.GONE);
            llReportUser.setVisibility(View.GONE);
            llRemoveUser.setVisibility(View.VISIBLE);
        }
        alert = dialogBuilder.create();
        alert.setCancelable(true);
        alert.show();

    }


    private void reportUser(String title, final String url, final HashMap<String, Object> input) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.report_popup, null);
        dialogBuilder.setView(dialogView);
        final EditText etTitle = dialogView.findViewById(R.id.etTitle);
        final TextView Pop_title = dialogView.findViewById(R.id.Pop_title);
        Pop_title.setText(title);
        final TextInputLayout etTitleLayout = dialogView.findViewById(R.id.etTitleLayout);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                etTitleLayout.setErrorEnabled(false);
                if (etTitle.getText().toString().trim().isEmpty()) {
                    etTitleLayout.setErrorEnabled(true);
                    etTitleLayout.setError(activity.getResources().getString(R.string.reason_empty_input));
                } else if (Utility.isConnectingToInternet(activity)) {
                    input.put(RequestParameters.REASON, "" + etTitle.getText().toString().trim());
                    Log.e("input", "" + input);
                    API.sendRequestToServerPOST_PARAM(activity, url, input);
//                    Toast.makeText(activity, activity.getResources().getString(R.string.user_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.user_has_been_reported_msg));
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                    confirmAlert.dismiss();
                } else {
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                }
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                confirmAlert.dismiss();
            }
        });


        this.confirmAlert = dialogBuilder.create();
        this.confirmAlert.setCancelable(true);
        this.confirmAlert.show();
    }


    private void showPopUp(String title, final String url, final String apiUrl, final int position, final HashMap<String, Object> input) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        View dialogView = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_view_for_create_group_screen, null);
        dialogBuilder.setView(dialogView);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.title);


        tvTitle.setText(title);


        llYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utility.isConnectingToInternet(activity)) { // check net connection
//                    Toast.makeText(activity, activity.getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.app_no_internet_error));
                    confirmAlert.dismiss();
                    return;
                }
                API.setResponseListener(new OnResponseListener() {
                    @Override
                    public void onGotResponse(JSONObject response) {
                        showPopUp(response + "");
                    }
                });
                if (input == null) {
                    API.sendRequestToServerGET(activity, url, apiUrl);
                    GroupInfoActivity.groupInfoActivity.blockedMembersFragment.refresh();
                } else {
                    API.sendRequestToServerPOST_PARAM(activity, url, input);
                    GroupInfoActivity.groupInfoActivity.blockedMembersFragment.refresh();
                }
                if (apiUrl.equalsIgnoreCase(API.GROUP_CHANGE_MEMBER)) {
                    groupMembers.get(position).role = newRoleToAssign;//"0";
                    notifyItemChanged(position);
                } else if (apiUrl.equalsIgnoreCase(API.GROUP_USER_REPORT)) {
//                    Toast.makeText(activity, activity.getResources().getString(R.string.user_has_been_reported_msg), Toast.LENGTH_SHORT).show();
                    Utils.showPopup(activity, activity.getResources().getString(R.string.user_has_been_reported_msg));
                } else {
                    notifyItemRemoved(position);
                    groupMembers.remove(position);
                    notifyItemRangeChanged(0, groupMembers.size());
                    hideLayout();
                }
                confirmAlert.dismiss();
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                confirmAlert.dismiss();
            }
        });

        confirmAlert = dialogBuilder.create();
        confirmAlert.setCancelable(true);
        confirmAlert.show();

    }

    void hideLayout() {
        if (isInvited) {
            if (groupMembers.size() <= 0)
                ((GroupInfoActivity) activity).hideInviteLayout();
        }

        if (isRequest) {
            if (groupMembers.size() <= 0)
                ((GroupInfoActivity) activity).hideRequestLayout();
        }

        if (isBlocked) {
            if (groupMembers.size() <= 0)
                ((GroupInfoActivity) activity).hideBlockLayout();
        }
    }


    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvCount, tvAdminText, tvdesc;
        ImageView ivGroupImage;
        RelativeLayout rlContainer;
        LinearLayout llAdminTextContainer;


        GroupViewHolder(View view) {
            super(view);
            tvUserName = view.findViewById(R.id.tvUserName);
            ivGroupImage = view.findViewById(R.id.ivGroupImage);
            rlContainer = view.findViewById(R.id.rlContainer);
            tvCount = view.findViewById(R.id.tvCount);
            llAdminTextContainer = view.findViewById(R.id.llAdminTextContainer);
            tvAdminText = view.findViewById(R.id.tvAdminText);
            tvdesc = view.findViewById(R.id.tvdesc);
        }
    }




}