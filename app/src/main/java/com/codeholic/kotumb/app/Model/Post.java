package com.codeholic.kotumb.app.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Post implements Serializable {

    @SerializedName("post_id")
    public String post_id = "";

    @SerializedName("reply_id")
    public String reply_id = "";


    @SerializedName("group_id")
    public String group_id = "";

    @SerializedName("userId")
    public String userId = "";

    @SerializedName("title")
    public String title = "";

    @SerializedName("content")
    public String content = "";

    @SerializedName("image")
    public String image = "";

    @SerializedName("postType")
    public String postType = "";

    @SerializedName("sharedPostId")
    public String sharedPostId = "";

    @SerializedName("role")
    public String role = "";

    @SerializedName("sharedPostGroupId")
    public String sharedPostGroupId = "";

    @SerializedName("sharedPostUserId")
    public String sharedPostUserId = "";

    @SerializedName("createdAt")
    public String createdAt = "";

    @SerializedName("updatedAt")
    public String updatedAt = "";

    @SerializedName("firstName")
    public String firstName = "";

    @SerializedName("middleName")
    public String middleName = "";

    @SerializedName("lastName")
    public String lastName = "";

    @SerializedName("avatar")
    public String avatar = "";

    @SerializedName("profileHeadline")
    public String profileHeadline = "";

    @SerializedName("duration")
    public String duration = "";

    @SerializedName("self_like")
    public int self_like = 0;

    @SerializedName("total_likes")
    public int total_likes = 0;

    @SerializedName("total_comments")
    public int total_comments = 0;

    @SerializedName("sharedPostUserName")
    public String sharedPostUserName = "";

    @SerializedName("message")
    public String message = "";

    @SerializedName("group_post_likes")
    @Expose
    public List<PostLikes> group_post_likes = new ArrayList<>();

    @SerializedName("group_post_comments")
    @Expose
    public List<PostComments> group_post_comments = new ArrayList<>();

}
