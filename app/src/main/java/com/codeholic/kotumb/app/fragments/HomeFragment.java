package com.codeholic.kotumb.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeholic.kotumb.app.Activity.ConnectionsActivity;
import com.codeholic.kotumb.app.Activity.HomeScreenActivity;
import com.codeholic.kotumb.app.Activity.SearchActivity;
import com.codeholic.kotumb.app.Activity.ShowVideos;
import com.codeholic.kotumb.app.Activity.UnomerActivity;
import com.codeholic.kotumb.app.Adapter.UserRecyclerViewAdapter;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.vidphon;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.codeholic.kotumb.app.Activity.HomeScreenActivity.TAG;


public class HomeFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.llProfile)
    LinearLayout llProfile;

    @BindView(R.id.linear)
    LinearLayout linear;

    @BindView(R.id.layout_search)
    CardView layoutSearch;

    @BindView(R.id.llInvite)
    LinearLayout llInvite;

    @BindView(R.id.llvideos)
    LinearLayout llvideos;

    @BindView(R.id.share)
    LinearLayout share;

    @BindView(R.id.llNotepad)
    LinearLayout llNotepad;

    @BindView(R.id.llFvrts)
    LinearLayout llFvrts;

     @BindView(R.id.llGovLink)
    LinearLayout llGovLink;

    @BindView(R.id.llSurvey)
    LinearLayout llSurvey;

    /*@BindView(R.id.vidphone)
    FloatingActionButton vidphone;

      @BindView(R.id.videos)
    FloatingActionButton videos;*/

    @BindView(R.id.llSkillDev)
    LinearLayout llSkillDev;

    @BindView(R.id.llAwards)
    LinearLayout llAwards;

    @BindView(R.id.llFinance)
    LinearLayout llFinance;

    @BindView(R.id.llJobs)
    LinearLayout llJobs;

    @BindView(R.id.tvProfileCount)
    public TextView tvProfileCount;

    @BindView(R.id.tvInviteCount)
    TextView tvInviteCount;

    @BindView(R.id.rvSugUserList)
    RecyclerView rvSugUserList;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.rlYouMayKnow)
    RelativeLayout rlYouMayKnow;






    private LinearLayoutManager linearLayoutManager;
    UserRecyclerViewAdapter userRecyclerViewAdapter;
    List<User> users;
    public NotificationFragment fragment;
    private Boolean is75ProfileComplete;
    private String post_earn_points = "";


    private String languageToLoad = "";
    Locale locale;
    Configuration config = new Configuration();


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languageToLoad = SharedPreferencesMethod.getDefaultLanguage(getContext());
        changeLocale(languageToLoad);
        setHasOptionsMenu(true);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        ApiCallForRewardHistory();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.home_tab, container, false);


        ButterKnife.bind(this, view);
        //mContainer = (PagerContainer) view.findViewById(R.id.pager_container);


        setOnClickListner();

        setProfileText("" + SharedPreferencesMethod.getString(getActivity(), ResponseParameters.PROFILE_STRENGTH));
        setInviteText("" + SharedPreferencesMethod.getString(getActivity(), ResponseParameters.INVITATIONS_COUNT));
        ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.header_main_menu_option1));
        getSuggestions();
        Log.d("check", "HomeFragment opened");


        FragmentManager fm = getChildFragmentManager();
        fragment = (NotificationFragment) fm.findFragmentByTag("myFragmentTag");
        if (fragment == null) {
            FragmentTransaction ft = fm.beginTransaction();
            fragment = new NotificationFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("hide", true);
            fragment.setArguments(bundle);
            ft.add(R.id.frag_notification, fragment, "myFragmentTag");
            ft.commit();
            ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.header_main_menu_option1));
        }

        // nestedScrolling.scrollTo(0,0);

        return view;
//        View view = inflater.inflate(R.layout.home_tab, container, false);
//
//
//        ButterKnife.bind(this, view);
//        //mContainer = (PagerContainer) view.findViewById(R.id.pager_container);
//        setOnClickListner();
//
//        setProfileText("" + SharedPreferencesMethod.getString(getActivity(), ResponseParameters.PROFILE_STRENGTH));
//        setInviteText("" + SharedPreferencesMethod.getString(getActivity(), ResponseParameters.INVITATIONS_COUNT));
//        ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.header_main_menu_option1));
//        getSuggestions();
//        Log.d("check", "HomeFragment opened");
//
//
//        FragmentManager fm = getChildFragmentManager();
//        fragment = (NotificationFragment) fm.findFragmentByTag("myFragmentTag");
//        if (fragment == null) {
//            FragmentTransaction ft = fm.beginTransaction();
//            fragment = new NotificationFragment();
//            Bundle bundle = new Bundle();
//            bundle.putBoolean("hide", true);
//            fragment.setArguments(bundle);
//            ft.add(R.id.frag_notification, fragment, "myFragmentTag");
//            ft.commit();
//            ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.header_main_menu_option1));
//        }
//
//        // nestedScrolling.scrollTo(0,0);
//
//        return view;
    }

    private void getSuggestions() {
        if (!Utility.isConnectingToInternet(getActivity())) { //net connection check
            return;
        }
        String userId = SharedPreferencesMethod.getUserId(getActivity());
        System.out.println("Suggestion User In Home   "+API.SUGGESTION_USERS + "0" + "/?userid=" + userId+" "+API.SUGGESTION_USERS);
        API.sendRequestToServerGET_FRAGMENT(getActivity(), this, API.SUGGESTION_USERS + "0" + "/?userid=" + userId, API.SUGGESTION_USERS); // service call for user list
    }

    private void ApiCallForRewardHistory() {
        try {
            if (!Utility.isConnectingToInternet(getActivity())) {
                final Snackbar snackbar = Snackbar.make(rvSugUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            /*HashMap<String, Object> input = new HashMap<>();
            input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(getActivity()));
            Log.e("input", "" + input);
//            String values= SharedPreferencesMethod.getString(getContext(),RequestParameters.AVATAR);
//            String value_profile= SharedPreferencesMethod.getString(getContext(),ResponseParameters.PROFILE);
//            Toast.makeText(getContext(), values, Toast.LENGTH_SHORT).show();
            API.sendRequestToServerPOST_PARAM_FRAGMENT(getActivity(), this, API.USER_REFER_REWARD, input);*/

            String userId = SharedPreferencesMethod.getUserId(getContext());
            String url = API.REWARD_HISTORY + userId + "/" + 0;
            API.sendRequestToServerGET_FRAGMENT(getContext(), this, url, API.REWARD_HISTORY);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ApiCallForPopUpRead(String rewardid, String rewardtype) {
        try {
            if (!Utility.isConnectingToInternet(getActivity())) {
                final Snackbar snackbar = Snackbar.make(rvSugUserList, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }

            String userId = SharedPreferencesMethod.getUserId(getContext());
            String url = API.POPUP_READ + "/" + rewardid + "/" + rewardtype;
            API.sendRequestToServerGET_FRAGMENT(getContext(), this, url, API.POPUP_READ);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setOnClickListner()
    {
        llProfile.setOnClickListener(this);
        layoutSearch.setOnClickListener(this);
        llInvite.setOnClickListener(this);
        llJobs.setOnClickListener(this);
        llFinance.setOnClickListener(this);
        llSkillDev.setOnClickListener(this);
        llAwards.setOnClickListener(this);
        rlYouMayKnow.setOnClickListener(this);
        llNotepad.setOnClickListener(this);
        llFvrts.setOnClickListener(this);
        llGovLink.setOnClickListener(this);
        llSurvey.setOnClickListener(this);


        llvideos.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isNetworkAvailable())
                {
                    Intent intent = new Intent(getActivity(), ShowVideos.class);
                    intent.putExtra("tab", "0");
                    startActivity(intent);
                }
                else
                {
                    Utils.showPopup(getActivity(),"Please check your internet connection");
                }
            }
        });

       /* vidphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), vidphon.class);
                startActivity(intent);
            }
        });
//        llSurvey.setVisibility(View.GONE);*/
    }

    public void performGroupClick()
    {
        if (llJobs != null)
        {
            llJobs.performClick();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.llSurvey:
//                Utils.canTakeSurvey = "1";
                if (Utils.canTakeSurvey.equalsIgnoreCase("1"))
                {
//                    if (!Utils.tookSurvey && getTimeElapsed() > Utils.surveyTimeInSeconds) {
                        startActivity(new Intent(getActivity(), UnomerActivity.class));
//                    } else {
//                        showWaitDialog();
//                    }
                }
                else
                    {
//                    String string = "You can't take survey because:\n1. You have less than 10 Connections" +
//                            "\n2. You are not member of minimum 2 groups\n3. Profile is not 75% complete";
                    String string = "You will earn Rs 10 for qualified reply of survey - IF\n" +
                            "-    At least your Personal Profile with Picture and Education qualification is complete\n" +
                            "-    You are connected to at least 10  people\n" +
                            "-    You are connected to at least 2 groups";
                    Utils.showPopup(getActivity(), string);
                }
                break;
            case R.id.llInvite:
               // mFragmentNavigation.pushFragment(new InviteFragment());
                ((HomeScreenActivity) getActivity()).openInviteFragment();
                break;
            case R.id.llProfile:
                Log.e("HOME","Click works");
                ((HomeScreenActivity) getActivity()).openMeFragment();
                break;
            case R.id.llNotepad:
                //getActivity().startActivity(new Intent(getActivity(), NotepadActivity.class));
                mFragmentNavigation.pushFragment(new NotepadFragment());
                break;
            case R.id.llFvrts:

                mFragmentNavigation.pushFragment(new FavrtLinksFragment());
                // ((HomeScreenActivity) getActivity()).updateToolbarTitle(getResources().getString(R.string.header_main_menu_option3));

               /* Intent intent = new Intent(getActivity(), FavrtLinksActivity.class);
                intent.putExtra("FAVOURITES", "");
                getActivity().startActivity(intent);*/
                break;
            case R.id.llJobs:
                mFragmentNavigation.pushFragment(new GroupFragment());

                break;
            case R.id.llSkillDev:
                mFragmentNavigation.pushFragment(new SkillDevFragment());
                break;
            case R.id.llFinance:
            case R.id.llAwards:
                Utility.ComingSoonPopup(getActivity());
                break;
            case R.id.rlYouMayKnow:
                Intent intent = new Intent(getActivity(), ConnectionsActivity.class);
                intent.putExtra("SUGGESTION", true);
                getActivity().startActivity(intent);
                break;
            case R.id.llGovLink:
                //getActivity().startActivity(new Intent(getActivity(), LinksActivity.class));
                mFragmentNavigation.pushFragment(new LinksFragment());
                break;

            case R.id.layout_search:
                startActivity(new Intent(getActivity(),SearchActivity.class));
                break;

        }
    }



    public void setProfileText(String profileCount) {
        if (tvProfileCount != null) {
            tvProfileCount.setText(profileCount);
            if (profileCount.isEmpty()) {
                tvProfileCount.setVisibility(View.GONE);
            } else {
                tvProfileCount.setVisibility(View.VISIBLE);
                if (profileCount.contains("100")) {
                    linear.removeView(llProfile);
                    linear.addView(llProfile);
                }
            }
        }
    }

    public void setInviteText(String inviteCount) {
        if (tvInviteCount != null) {
            tvInviteCount.setText(inviteCount);
            if (inviteCount.isEmpty()) {
                tvInviteCount.setVisibility(View.GONE);
            } else {
                tvInviteCount.setVisibility(View.VISIBLE);
            }
        }
    }

    //getting response from server
    public void getResponse(JSONObject outPut, int i) {
        try {
            //Log.e("response", outPut.toString());
            if (i == 0) { // get search user list
                progressBar.setVisibility(View.GONE);
                users = new ArrayList<>();
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error

                    } else if (outPut.has(ResponseParameters.Success)) {
                        if (outPut.has(ResponseParameters.RESULTS)) {
                            users = new ArrayList<>();
                            for (int j = 0; j < outPut.getJSONArray(ResponseParameters.RESULTS).length(); j++) {
                                JSONObject sugResults = outPut.getJSONArray(ResponseParameters.RESULTS).getJSONObject(j);
                                User user = new User();
                                user.setFirstName(sugResults.getString(ResponseParameters.FirstName));
                                user.setMiddleName(sugResults.getString(ResponseParameters.MiddleName));
                                user.setLastName(sugResults.getString(ResponseParameters.LastName));
                                user.setAvatar(sugResults.getString(ResponseParameters.Avatar));
                                user.setUserId(sugResults.getString(ResponseParameters.UserId));
                                user.setProfileHeadline(sugResults.getString(ResponseParameters.PROFILE_HEADLINE));
                                user.setProfileSummary(sugResults.getString(ResponseParameters.PROFILE_SUMMERY));
                                user.setAadhaarInfo(sugResults.getString(ResponseParameters.AADHARINFO));
                                user.setCity(sugResults.getString(ResponseParameters.City));
                                user.setState(sugResults.getString(ResponseParameters.State));
                                user.setUserInfo(sugResults.toString());
                                users.add(user);
                               System.out.println("User Data Get    "+users.get(i));
                            }
                        }
                    } else {
                        final Snackbar snackbar = Snackbar.make(rvSugUserList, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        //snackbar.show();
                    }
                }
                users.add(null);
                //setting recycler view
                linearLayoutManager = new LinearLayoutManager(getActivity());
                linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                rvSugUserList.setLayoutManager(linearLayoutManager);
                rvSugUserList.setNestedScrollingEnabled(false);
                userRecyclerViewAdapter = new UserRecyclerViewAdapter(rvSugUserList, getActivity(), users);
                userRecyclerViewAdapter.setSuggestion(true);
                rvSugUserList.setAdapter(userRecyclerViewAdapter);

            }else if(i == 1){   //get user refer reward if profile updated above 75%
                Log.e(TAG, "getResponse user refer reward: "+outPut );

                if (outPut.has("success")) {
                    Log.e(TAG, "getResponse: inside success" );
                    try {
                        JSONArray outPutJSONArray = outPut.getJSONArray("reward_points");

                        for (int k = 0; k < outPutJSONArray.length(); k++) {
                            JSONObject jsonObject = outPutJSONArray.getJSONObject(k);

                            Log.e(TAG, "getResponse: xxxx "+ jsonObject.getString("read_notification"));

                            if(jsonObject.getString("rewardType").equalsIgnoreCase("referral_reward")) {
                                if (jsonObject.getString("read_notification").equalsIgnoreCase("null") || jsonObject.getString("read_notification").equalsIgnoreCase("0")) {
                                    String rewardid   = jsonObject.getString("rewardId");
                                    String rewardtype = jsonObject.getString("rewardType");
                                    String totalvalue = jsonObject.getString("points");
                                    //showPointsEarnDialog(getContext(), getActivity(), "You have received " + totalvalue + " Referral Reward Points ", rewardid, rewardtype);
                                }
                            }else if(jsonObject.getString("rewardType").equalsIgnoreCase("profile_reward")) {
                                if (jsonObject.getString("read_notification").equalsIgnoreCase("null") || jsonObject.getString("read_notification").equalsIgnoreCase("0") ) {
                                    String rewardid   = jsonObject.getString("rewardId");
                                    String rewardtype = jsonObject.getString("rewardType");
                                    String totalvalue = jsonObject.getString("points");
                                    //showPointsEarnDialog(getContext(), getActivity(), "You have received "+ totalvalue + " Profile Completion points", rewardid, rewardtype);
                                }
                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }else if( i == 2){
                Log.e(TAG, "getResponse: notification read successfull" );
                Log.e(TAG, "getResponse: "+outPut );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPointsEarnDialog(Context context, Activity activity, String pointValue, String rewardid, String rewardtype) {

        ViewGroup viewGroup = activity.findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(context).inflate(R.layout.alert_reward_points, viewGroup, false);
        TextView tv_points = dialogView.findViewById(R.id.tv_points);
        TextView tv_ok = dialogView.findViewById(R.id.tv_ok);
        //tv_points.setText(getString(R.string.reward_str_pre)+" "+pointValue+" "+getString(R.string.reward_str_post));
        tv_points.setText(pointValue);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();
                ApiCallForPopUpRead(rewardid, rewardtype);
                alertDialog.dismiss();
            }
        });
        Log.e(TAG, "showPointsEarnDialog: we here");
    }

    public void openGroup() {
        try {
            if(llJobs!=null)
            llJobs.performClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    //change locale
    private void changeLocale(String languageToLoad) {
        locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        config = new Configuration();
        config.locale = locale;
        getContext().getResources().updateConfiguration(config,
                getContext().getResources().getDisplayMetrics());

    }



}
