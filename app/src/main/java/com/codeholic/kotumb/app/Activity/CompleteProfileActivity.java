package com.codeholic.kotumb.app.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Adapter.EducationListAdapter;
import com.codeholic.kotumb.app.Adapter.ProfessionListAdapter;
import com.codeholic.kotumb.app.Adapter.RecomRecyclerViewAdapter;
import com.codeholic.kotumb.app.Model.Education;
import com.codeholic.kotumb.app.Model.Profession;
import com.codeholic.kotumb.app.Model.Recommendations;
import com.codeholic.kotumb.app.Model.User;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.Constants;
import com.codeholic.kotumb.app.Utility.ConvertBitmap;
import com.codeholic.kotumb.app.Utility.DataAttributes;
import com.codeholic.kotumb.app.Utility.ImgeCroper.Crop;
import com.codeholic.kotumb.app.Utility.PermissionsUtils;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.ResponseParameters;
import com.codeholic.kotumb.app.Utility.RotateImageCode;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.codeholic.kotumb.app.View.OtpEditText;
import com.crashlytics.android.Crashlytics;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.shagi.materialdatepicker.date.DatePickerFragmentDialog;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import butterknife.BindView;
import butterknife.ButterKnife;
import static android.os.FileObserver.DELETE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.github.florent37.viewtooltip.ViewTooltip.ALIGN.CENTER;
import static com.github.florent37.viewtooltip.ViewTooltip.Position.TOP;

public class CompleteProfileActivity extends AppCompatActivity implements View.OnClickListener, IPickResult {
    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.etAdharCardNumberLayout)
    TextInputLayout etAdharCardNumberLayout;

    @BindView(R.id.etAdharCardNumber)
    EditText etAdharCardNumber;

    @BindView(R.id.etConfirmAdharCardNumberLayout)
    TextInputLayout etConfirmAdharCardNumberLayout;

    @BindView(R.id.etConfirmAdharCardNumber)
    EditText etConfirmAdharCardNumber;

    @BindView(R.id.etAdharNameLayout)
    TextInputLayout etAdharNameLayout;

    @BindView(R.id.etAdharName)
    EditText etAdharName;

    @BindView(R.id.etAdharDOBLayout)
    TextInputLayout etAdharDOBLayout;

    @BindView(R.id.etAdharDOB)
    EditText etAdharDOB;

    @BindView(R.id.cvEdit)
    CardView cvEdit;

    @BindView(R.id.etAdharFatherNameLayout)
    TextInputLayout etAdharFatherNameLayout;

    @BindView(R.id.etAdharFatherName)
    EditText etAdharFatherName;

    @BindView(R.id.etAdharPinLayout)
    TextInputLayout etAdharPinLayout;

    @BindView(R.id.etAdharPin)
    EditText etAdharPin;

    @BindView(R.id.cvAdharSave)
    CardView cvAdharSave;

    @BindView(R.id.tvAdharSave)
    TextView tvAdharSave;

    @BindView(R.id.pbAdharLoading)
    ProgressBar pbAdharLoading;

    @BindView(R.id.pbImageLoading)
    ProgressBar pbImageLoading;



    @BindView(R.id.ivQrCode)
    ImageView ivQrCode;

    @BindView(R.id.ivCamera)
    ImageView ivCamera;

    @BindView(R.id.ivChallenged)
    ImageView ivChallenged;

    @BindView(R.id.rvOrganisationList)
    RecyclerView rvOrganisationList;

    @BindView(R.id.cvAddProfession)
    CardView cvAddProfession;

    @BindView(R.id.cvAddEducation)
    CardView cvAddEducation;

    @BindView(R.id.rvEducationList)
    RecyclerView rvEducationList;

    @BindView(R.id.rvRecommendations)
    RecyclerView rvRecommendations;

    @BindView(R.id.tvRecom)
    TextView tvRecom;

    @BindView(R.id.maSkills)
    MultiAutoCompleteTextView maSkills;

    @BindView(R.id.maLanguageLayout)
    TextInputLayout maLanguageLayout;

    @BindView(R.id.maLanguage)
    MultiAutoCompleteTextView maLanguage;

    @BindView(R.id.maIndustries)
    AutoCompleteTextView maIndustries;

    @BindView(R.id.maIndustriesLayout)
    TextInputLayout maIndustriesLayout;

    @BindView(R.id.maDIndustries)
    AutoCompleteTextView maDIndustries;

    @BindView(R.id.maDIndustriesLayout)
    TextInputLayout maDIndustriesLayout;

    @BindView(R.id.maSkillsLayout)
    TextInputLayout maSkillsLayout;

    @BindView(R.id.etSkillsDes)
    EditText etSkillsDes;

    @BindView(R.id.etSkillsDesLayout)
    TextInputLayout etSkillsDesLayout;

    @BindView(R.id.maDSkills)
    MultiAutoCompleteTextView maDSkills;

    @BindView(R.id.maDSkillsLayout)
    TextInputLayout maDSkillsLayout;

    @BindView(R.id.etDSkillsDes)
    EditText etDSkillsDes;

    @BindView(R.id.etDSkillsDesLayout)
    TextInputLayout etDSkillsDesLayout;

    @BindView(R.id.cvSaveSkills)
    CardView cvSaveSkills;

    @BindView(R.id.cvSaveDSkills)
    CardView cvSaveDSkills;

    @BindView(R.id.tvSaveSkills)
    TextView tvSaveSkills;

    @BindView(R.id.tvSaveDSkills)
    TextView tvSaveDSkills;

    @BindView(R.id.tvWarning)
    TextView tvWarning;

    @BindView(R.id.ivDesired)
    ImageView ivDesired;

    @BindView(R.id.ivSkills)
    ImageView ivSkills;

    @BindView(R.id.ivIndustries)
    ImageView ivIndustries;

    @BindView(R.id.ivDIndustries)
    ImageView ivDIndustries;

    @BindView(R.id.ivLanguage)
    ImageView ivLanguage;

    @BindView(R.id.ivUser)
    ImageView ivUser;

    @BindView(R.id.rlChangeImage)
    RelativeLayout rlChangeImage;

    @BindView(R.id.tvUpdatePhone)
    TextView tvUpdatePhone;

    @BindView(R.id.adView)
    ImageView adView;

    @BindView(R.id.rlAdView)
    RelativeLayout rlAdView;

    RadioButton rbMale;
    RadioButton rbFemale;
    RadioButton rbOther;
    RadioGroup rgGenderGroup;

    @BindView(R.id.cbEmail)
    CheckBox cbEmail;

    @BindView(R.id.cbChallenged)
    CheckBox cbChallenged;

    @BindView(R.id.cbAddress)
    CheckBox cbAddress;

    @BindView(R.id.cbMobile)
    CheckBox cbMobile;

    @BindView(R.id.ivMobile)
    ImageView ivMobile;
    @BindView(R.id.ivAddress)
    ImageView ivAddress;
    @BindView(R.id.ivEmail)
    ImageView ivEmail;

    @BindView(R.id.cbDOB)
    CheckBox cbDOB;

    @BindView(R.id.ivDOB)
    ImageView ivDOB;

    @BindView(R.id.cbGender)
    CheckBox cbGender;

    @BindView(R.id.ivGender)
    ImageView ivGender;

    @BindView(R.id.cvAddMoreSkills)
    CardView cvAddMoreSkills;

    @BindView(R.id.cv_delete_acc)
    TextView cv_delete_acc;

    @BindView(R.id.cvAddMoreDSkills)
    CardView cvAddMoreDSkills;

    @BindView(R.id.llAddSkills)
    LinearLayout llAddSkills;

    @BindView(R.id.llAddDSkills)
    LinearLayout llAddDSkills;


    ViewTooltip v;
    int tag = 0;

    @BindView(R.id.nvcontainer)
    NestedScrollView nvcontainer;

    private AutoCompleteTextView etPincode;
    private EditText etFirstName, etLastName, etDOB, etPhone, etAPhone, etCity, etState, etEmail, etGender, etCAddress, etProfileSummery, etProfileHeadLine;
    private TextInputLayout etFirstNameLayout, etLastNameLayout, etAPhoneLayout, etDOBLayout, etGenderLayout, etPhoneLayout, etPincodeLayout, etCityLayout, etStateLayout, etEmailLayout, etAnswerOneLayout, etAnswerTwoLayout, etCAddressLayout;
    private TextView tvSave;
    private CheckBox cbTerms;
    private CardView cvSave;
    private android.widget.ProgressBar pbLoading, pbPinCode, pbSkill, pbDSkill, pbSkillsLoading, pbDSkillsLoading;
    List<String> skills = new ArrayList<>();
    List<String> sector = new ArrayList<>();
    List<String> languages = new ArrayList<>();
    boolean pin = true, skill = true, dSkill = true;
    private LinearLayoutManager linearLayoutManager;
    private ProfessionListAdapter professionListAdapter;
    private EducationListAdapter educationListAdapter;
    public static final int REQUEST_CROP = 6709;
    View focusView;
    int id;
    Activity context;
    private Calendar calendar;
    private int year, month, day;
    JSONObject profile = new JSONObject();
    JSONArray recommedations = new JSONArray();
    private String gender = "";
    List<Education> educations = new ArrayList<>();
    List<Profession> professions = new ArrayList<>();
    private RecomRecyclerViewAdapter recomRecyclerViewAdapter;

    AlertDialog alertDialog;
    com.codeholic.kotumb.app.View.ProgressBar progressBar;
    AlertDialog mobileAlertDialog;
    AlertDialog otpAlertDialog;
    HashMap<String, Object> changeMobile;
    String otp = "";

    //qr code scanner object
    private IntentIntegrator qrScan;

    @BindView(R.id.etMiddleName)
    EditText etMiddleName;
    private ProgressDialog dialog;
    ArrayList<SkillsObject> cskillsArray = new ArrayList<>();
    ArrayList<SkillsObject> dskillsArray = new ArrayList<>();
    private int cskillCount;
    private int dskillCount;
    private boolean onBackPressed;
    private SaveObject retrievedObject;
    private boolean dontShowDialog;
    private ImageView deleteProfilePic;

    // oncreate method initializing view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_profile);
        deleteProfilePic=findViewById(R.id.delete_image);
        ButterKnife.bind(this);
        initToolbar();
        init();
        initView();
        String retrieved = SharedPreferencesMethod.getString(this, ResponseParameters.PROFILE);
        retrievedObject = null;
        User userInfo = SharedPreferencesMethod.getUserInfo(this);
        if (retrieved == null || retrieved.isEmpty()) {
            fillValues(userInfo);
        } else {
            try {
                retrievedObject = new Gson().fromJson(retrieved, SaveObject.class);
                retrievedObject.user.setAvatar(userInfo.getAvatar());
                fillValues(retrievedObject.user);
                maLanguage.setText(retrievedObject.languages);
                showChangesNotSavedDialog(retrievedObject, false);
            } catch (Exception e) {
                Utils.showPopup(this, getString(R.string.something_wrong));
                e.printStackTrace();
                Crashlytics.setString("retrievedObject", retrieved);
                Crashlytics.setString("msg", "This case occurs when user has made changes to his/her profile and close/exited app without saving, so in response this app saves a local copy of all the changes made." /* string value */);
                Crashlytics.logException(e);
            }
        }
        addharFields();
        if (getIntent().hasExtra("DATA")) {
            fillFromJSON();
            if (retrievedObject != null) {
                maLanguage.setText(retrievedObject.languages);
            }
            setWorkRecyclerView();
            setEducationRecyclerView();
            setRecomRecyclerView(recommedations);
            setEditTextWatcher();
        } else {
            serViceCall();
        }

        setClickListener();
        setCheckedChangeListener();


        etPincode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                try {
                    if (hasFocus) {
                        if (!etPincodeLayout.isErrorEnabled())
                            etPincode.performClick();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        etPincode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utility.isConnectingToInternet(context)) {
                    return;
                }
                Log.e("TAG", "CLICKED " + etPincode.isPopupShowing());
                if (etPincode.getText().toString().trim().length() < 6) {
                    //pbPinCode.setVisibility(VISIBLE);
                    //API.sendRequestToServerGET(context, API.ZIPCODE + etPincode.getText().toString(), API.ZIPCODE_CLICKED);
                }
            }
        });


        maLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("onclick", "onclick");
                try {
                    if (!SharedPreferencesMethod.getString(CompleteProfileActivity.this, SharedPreferencesMethod.ALL_LANGUAGES).toString().isEmpty()) {
                        setUpLanguageAdapter(new JSONObject(SharedPreferencesMethod.getString(CompleteProfileActivity.this, SharedPreferencesMethod.ALL_LANGUAGES)), maLanguage);
                        clearFocus();
                        maLanguage.showDropDown();
                    } else {
                        if (!Utility.isConnectingToInternet(context)) {
                            return;
                        }
                        API.sendRequestToServerGET(context, API.GET_LANGUAGE, API.GET_LANGUAGE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ivLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!maLanguage.getText().toString().trim().isEmpty()) {
                    clearAutoCompleteEditText(maLanguage);
                }
            }
        });

        //for sector of skills
        maIndustries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("onclick", "onclick");
                try {
                    if (!SharedPreferencesMethod.getString(CompleteProfileActivity.this, SharedPreferencesMethod.INDUSTRIES).toString().isEmpty()) {
                        setUpIndustriesAdapter(new JSONObject(SharedPreferencesMethod.getString(CompleteProfileActivity.this, SharedPreferencesMethod.INDUSTRIES)), maIndustries);
                        maIndustries.showDropDown();
                    } else {
                        if (!Utility.isConnectingToInternet(context)) {
                            return;
                        }
                        API.sendRequestToServerGET(context, API.GET_INDUSTRIES, API.GET_INDUSTRIES);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        maSkills.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    maSkills.performClick();
            }
        });


        maDSkills.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    maDSkills.performClick();
            }
        });

        maSkills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS).isEmpty()) {
                        if (!Utility.isConnectingToInternet(context)) {
                            return;
                        }
                        API.sendRequestToServerGET(context, API.GET_SKILLS, API.GET_SKILLS);
                    } else {
                        getResponse(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS)), 4);
                    }
                    maSkills.showDropDown();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(CompleteProfileActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
//                maSkills.showDropDown();
            }
        });

        maIndustries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(maDSkills.getWindowToken(), 0);
            }
        });

        maSkills.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(maSkills.getWindowToken(), 0);
            }
        });

        maDSkills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS).isEmpty()) {
                        if (!Utility.isConnectingToInternet(context)) {
                            return;
                        }
                        API.sendRequestToServerGET(context, API.GET_SKILLS, API.GET_SKILLS);
                    } else {
                        getResponse(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS)), 4);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                maDSkills.showDropDown();
            }
        });

        ivIndustries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!maIndustries.getText().toString().trim().isEmpty()) {
                    maIndustries.setText("");
                    maSkills.setText("");
                }
            }
        });

        ivDIndustries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!maDIndustries.getText().toString().trim().isEmpty()) {
                    maDIndustries.setText("");
                    maDSkills.setText("");
                }
            }
        });

        maDIndustries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("onclick", "onclick");
                try {
                    if (!SharedPreferencesMethod.getString(CompleteProfileActivity.this, SharedPreferencesMethod.INDUSTRIES).toString().isEmpty()) {
                        setUpIndustriesAdapter(new JSONObject(SharedPreferencesMethod.getString(CompleteProfileActivity.this, SharedPreferencesMethod.INDUSTRIES)), maDIndustries);
                        maDIndustries.showDropDown();
                    } else {
                        if (!Utility.isConnectingToInternet(context)) {
                            return;
                        }
                        API.sendRequestToServerGET(context, API.GET_INDUSTRIES, API.GET_INDUSTRIES);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        IntentFilter filterAdd = new IntentFilter();
        filterAdd.addAction(ResponseParameters.ADD_RECEIVED);
        registerReceiver(getAd, filterAdd);
        deleteProfilePic();
    }


    //method is created by  -  Arpit Kanda for Delete User Profile Pictures
    public void deleteProfilePic(){
        final User user = new User();
        try {
            deleteProfilePic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, Object> input = new HashMap<>();
                    input.put(RequestParameters.AVATAR, API.imageUrl+"default-profile.png");
                    input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(context));
                    System.out.println("Get Response   "+input);
                    API.sendRequestToServerPOST_PARAM(context, API.UPDATE_PROFILE_PIC, input);
                    pbImageLoading.setVisibility(View.VISIBLE);
                    cvEdit.setVisibility(View.GONE);
                    SharedPreferencesMethod.setString(CompleteProfileActivity.this, RequestParameters.AVATAR, API.imageUrl+"default-profile.png");
                    new AQuery(CompleteProfileActivity.this).id(ivUser).image(API.imageUrl+"default-profile.png", true, true, 300, R.drawable.default_profile);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setupSkillsAdapter(JSONObject outPut, MultiAutoCompleteTextView maSkills, AutoCompleteTextView maIndustries) {
        try {
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Success)) {
                    if (outPut.has(ResponseParameters.SKILLS)) {
                        SharedPreferencesMethod.setString(this, SharedPreferencesMethod.SKILLS, outPut.toString());
                        //sector text

                        List<String> skills = new ArrayList<>();
                        List<String> selectedList = new ArrayList<>();
                        //sector list
                        List<String> selectedSectorList = new ArrayList<>();
                        String tempText = "";
                        String tempSectorText = "";
                        if (maSkills.hasFocus()) {
                            tempText = maSkills.getText().toString().trim().replace(", ", ",");
                            //sector text
                            tempSectorText = maIndustries.getText().toString().trim().replace(", ", ",");
                        }

                        if (tempText.endsWith(",")) {
                            tempText = tempText.trim().substring(0, tempText.length() - 1);
                        }
                        if (tempSectorText.endsWith(",")) {
                            tempSectorText = tempSectorText.trim().substring(0, tempSectorText.length() - 1);
                        }

                        Log.e("TAG 1", tempText + " " + tempSectorText);
                        if (!tempText.trim().isEmpty())
                            if (!tempText.contains(",")) {
                                selectedList.add(tempText);
                            } else {
                                String[] temp = tempText.trim().split(",");
                                selectedList = Arrays.asList(temp);
                            }
                        Log.e("TAG 2", selectedList.toString());

                        //for sector list
                        if (!tempSectorText.trim().isEmpty())
                            if (!tempSectorText.contains(",")) {
                                selectedSectorList.add(tempSectorText);
                            } else {
                                String[] temp = tempSectorText.trim().split(",");
                                selectedSectorList = Arrays.asList(temp);
                            }
                        Log.e("TAG 3", selectedSectorList.toString());


                        for (int j = 0; j < outPut.getJSONArray(ResponseParameters.SKILLS).length(); j++) {
                           /* if (!skills.contains(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SKILL))
                                    && !selectedList.contains(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SKILL))
                                    && selectedSectorList.contains(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SECTOR)))*/
                            skills.add(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SKILL));
                        }
                        skills.add("Other");
                        String[] data = skills.toArray(new String[skills.size()]);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.autocomplete_item, data);
                        if (maSkills.hasFocus()) {
                            maSkills.setAdapter(adapter);
                            maSkills.setThreshold(1);
                            Utils.addOtherOptionHandler(maSkills, skills, CompleteProfileActivity.this, true);
                            if (!maSkills.getText().toString().trim().isEmpty() && !maSkills.getText().toString().trim().endsWith(","))
                                maSkills.showDropDown();
                            maSkills.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver getAd = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //do something based on the intent's action
            try {
//                HomeScreenActivity.setUpAdImage(intent.getStringExtra(ResponseParameters.ADD_RECEIVED), rlAdView, adView, CompleteProfileActivity.this, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void setUpAdImage(String adResponse) {
        hideAdView();
        try {
            if (!adResponse.isEmpty()) {
                final JSONObject ad = new JSONObject(adResponse);
                if (ad.has(ResponseParameters.AD)) {
                    if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.IMAGE)) {
                        if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE).isEmpty()) {
                            rlAdView.setVisibility(VISIBLE);
                            new AQuery(this).id(adView).image(API.imageAdUrl + ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.IMAGE), true, true, 300, R.drawable.ic_user);
                            adView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        if (ad.getJSONObject(ResponseParameters.AD).has(ResponseParameters.URL)) {
                                            if (!ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL).isEmpty()) {
                                                Utility.prepareCustomTab(CompleteProfileActivity.this, ad.getJSONObject(ResponseParameters.AD).getString(ResponseParameters.URL));
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideAdView() {
        rlAdView.setVisibility(GONE);
    }

    @Override
    protected void onDestroy() {
        if (getAd != null) {
            unregisterReceiver(getAd);
            getAd = null;
        }
        super.onDestroy();
    }

    //on resume method
    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.completeProfileActivity) { // for updating eduction recycler view
            if (Constants.education_position != -1 && Constants.education != null) {
                educations.remove(Constants.education_position);
                educations.add(Constants.education_position, Constants.education);
                educationListAdapter.notifyDataSetChanged();
            } else if (Constants.education != null) {
                if (educations.size() > 0) {
                    educations.add(Constants.education);
                    educationListAdapter.notifyDataSetChanged();
                } else {
                    educations.add(Constants.education);
                    educationListAdapter = new EducationListAdapter(this, educations);
                    rvEducationList.setAdapter(educationListAdapter);
                    rvEducationList.setVisibility(VISIBLE);
                }
            }
            if (Constants.profession_position != -1 && Constants.profession != null) {
                professions.remove(Constants.profession_position);
                professions.add(Constants.profession_position, Constants.profession);
                professionListAdapter.notifyDataSetChanged();
            } else if (Constants.profession != null) { // for updating professional recycler view
                if (professions.size() > 0) {
                    professions.add(Constants.profession);
                    professionListAdapter.notifyDataSetChanged();
                } else {
                    professions.add(Constants.profession);
                    professionListAdapter = new ProfessionListAdapter(this, professions);
                    rvOrganisationList.setAdapter(professionListAdapter);
                    rvOrganisationList.setVisibility(VISIBLE);
                }
            }
        }
        Constants.education = null;
        Constants.education_position = -1;
        Constants.profession_position = -1;
        Constants.profession = null;
        Constants.completeProfileActivity = false;
    }

    //addharFields check aadhar fields
    private void addharFields() {
        try {
            if (profile.has(ResponseParameters.CHECK_ADHAR_INFO)) {
                if (profile.getBoolean(ResponseParameters.CHECK_ADHAR_INFO)) {// if aadhar details are verified
                    ivQrCode.setClickable(false);
                    ivQrCode.setEnabled(false);
                    ivCamera.setClickable(false);
                    ivCamera.setEnabled(false);
                    etAdharCardNumber.setEnabled(false);
                    etConfirmAdharCardNumber.setEnabled(false);
                    etAdharName.setEnabled(false);
                    etAdharDOB.setEnabled(false);
                    etAdharDOB.setClickable(false);
                    etAdharFatherName.setEnabled(false);
                    etAdharPin.setEnabled(false);
                    cvAdharSave.setClickable(false);
                    cvAdharSave.setEnabled(false);
                    cvAdharSave.setVisibility(GONE);
                    tvWarning.setVisibility(GONE);
                } else {
                    ivQrCode.setClickable(true);
                    ivQrCode.setEnabled(true);
                    ivCamera.setClickable(true);
                    ivCamera.setEnabled(true);
                    etAdharCardNumber.setEnabled(true);
                    etConfirmAdharCardNumber.setEnabled(true);
                    etAdharName.setEnabled(true);
                    etAdharDOB.setEnabled(true);
                    etAdharDOB.setClickable(true);
                    etAdharFatherName.setEnabled(true);
                    etAdharPin.setEnabled(true);
                    cvAdharSave.setClickable(true);
                    cvAdharSave.setEnabled(true);
                    cvAdharSave.setVisibility(VISIBLE);
                    tvWarning.setVisibility(VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //service call for getting use info if not available
    private void serViceCall() {
        try {
            if (!Utility.isConnectingToInternet(this)) { // checking internet connection
                return;
            }

            HashMap<String, Object> input = new HashMap<>();
            input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(this));
            input.put(RequestParameters.VIEWERID, "" + SharedPreferencesMethod.getUserId(this));
            Log.e("input", "" + input);
            API.sendRequestToServerPOST_PARAM(this, API.PROFILE, input); // service call
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // initView method initializing views
    private void initView() {
        try {
            tvSave = findViewById(R.id.tvSave);
            cbTerms = findViewById(R.id.cbTerms);
            cvSave = findViewById(R.id.cv_save);
            etFirstName = findViewById(R.id.etFirstName);
            etLastName = findViewById(R.id.etLastName);
            etDOB = findViewById(R.id.etDOB);
            etPhone = findViewById(R.id.etPhone);
            etPincode = findViewById(R.id.etPincode);
            etCity = findViewById(R.id.etCity);
            etState = findViewById(R.id.etState);
            etEmail = findViewById(R.id.etEmail);
            etProfileHeadLine = findViewById(R.id.etProfileHeadLine);
            etProfileSummery = findViewById(R.id.etProfileSummery);
            etGender = findViewById(R.id.etGender);
            pbLoading = findViewById(R.id.pbLoading);
            pbPinCode = findViewById(R.id.pbPinCode);
            pbSkill = findViewById(R.id.pbSkill);
            pbDSkill = findViewById(R.id.pbDSkill);
            pbSkillsLoading = findViewById(R.id.pbSkillsLoading);
            pbDSkillsLoading = findViewById(R.id.pbDSkillsLoading);

            etFirstNameLayout = findViewById(R.id.etFirstNameLayout);
            etLastNameLayout = findViewById(R.id.etLastNameLayout);
            etDOBLayout = findViewById(R.id.etDOBLayout);
            etGenderLayout = findViewById(R.id.etGenderLayout);
            etPhoneLayout = findViewById(R.id.etPhoneLayout);
            etPincodeLayout = findViewById(R.id.etPincodeLayout);
            etCityLayout = findViewById(R.id.etCityLayout);
            etStateLayout = findViewById(R.id.etStateLayout);
            etEmailLayout = findViewById(R.id.etEmailLayout);

            etAPhone = findViewById(R.id.etAPhone);
            etAPhoneLayout = findViewById(R.id.etAPhoneLayout);
            etCAddress = findViewById(R.id.etCAddress);
            etCAddressLayout = findViewById(R.id.etCAddressLayout);

            rgGenderGroup = findViewById(R.id.rgGenderGroup);
            rbMale = findViewById(R.id.rbMale);
            rbFemale = findViewById(R.id.rbFemale);
            rbOther = findViewById(R.id.rbOther);
        } catch (Exception e) {

        }
    }

    // initToolbar method initializing view
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        //setting screen title
        header.setText(getResources().getString(R.string.app_my_profile_text));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    // init method initializing objects
    private void init() {
        try {
            context = this;
            Constants.fromProfile = true;
            qrScan = new IntentIntegrator(this); // for qr code scanner initialization
            id = getIntent().getIntExtra(ResponseParameters.Id, R.id.ll_personal_details);
            Log.e("id", "" + id);
            if (getIntent().hasExtra("DATA"))
                profile = new JSONObject(getIntent().getStringExtra("DATA"));
            if (getIntent().hasExtra("RECOMMENDATIONS"))
                recommedations = new JSONArray(getIntent().getStringExtra("RECOMMENDATIONS"));
            calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
            focusView = findViewById(id);
            focusView.getParent().requestChildFocus(focusView, focusView); // for scrolling to specific view

            Constants.education = null;
            Constants.education_position = -1;
            Constants.profession_position = -1;
            Constants.profession = null;
            Constants.completeProfileActivity = false;

      /*      if (id == R.id.ivUser) {
                galleryOrCameraPopup();
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // setWorkRecyclerView creating list for Profession details
    private void setWorkRecyclerView() {
        try {
            // setting options for professional recycler view
            linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvOrganisationList.setLayoutManager(linearLayoutManager);
            rvOrganisationList.setNestedScrollingEnabled(false);
            // rvOrganisationList.setHasFixedSize(true);
            if (profile.has(ResponseParameters.WORK_EXPERIANCE)) { // checking if user have experience
                //By Me

                professions = new ArrayList<>();
                for (int i = 0; i < profile.getJSONArray(ResponseParameters.WORK_EXPERIANCE).length(); i++) {
                    JSONObject jsonObject = profile.getJSONArray(ResponseParameters.WORK_EXPERIANCE).getJSONObject(i);
                    Profession profession = new Profession();
                    profession.setId(jsonObject.getString(ResponseParameters.Id));
                    profession.setCompanyName(jsonObject.getString(ResponseParameters.ORGANIZATION_NAME));
                    profession.setProfessionName(jsonObject.getString(ResponseParameters.ORGANIZATION_ROLE));
                    profession.setYearFrom(jsonObject.getString(ResponseParameters.YEARFROM));
                    profession.setYearTo(jsonObject.getString(ResponseParameters.YEARTO));
                    profession.setDescription(jsonObject.getString(ResponseParameters.DESCRIPTION));
                    profession.setIsCurrent(jsonObject.getString(ResponseParameters.ISCURRENT));
                    if (jsonObject.has(ResponseParameters.ORGANIZATION_INDUSTRY))
                        profession.setSector(jsonObject.getString(ResponseParameters.ORGANIZATION_INDUSTRY));
                    professions.add(profession);
                }
                professionListAdapter = new ProfessionListAdapter(this, professions);
                rvOrganisationList.setAdapter(professionListAdapter);

                if (profile.getJSONArray(ResponseParameters.WORK_EXPERIANCE).length() <= 0) { // if user dont have any experience
                    rvOrganisationList.setVisibility(GONE);
                    cvAddProfession.setVisibility(VISIBLE);
                } else {
                    rvOrganisationList.setVisibility(VISIBLE);
                    cvAddProfession.setVisibility(VISIBLE);
                }
            } else {
                rvOrganisationList.setVisibility(VISIBLE);
                cvAddProfession.setVisibility(VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // setEducationRecyclerView creating list for Education details
    private void setEducationRecyclerView() {
        try {
            // setting options for education recycler view
            linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvEducationList.setLayoutManager(linearLayoutManager);
            rvEducationList.setNestedScrollingEnabled(false);
            // rvEducationList.setHasFixedSize(true);
            if (profile.has(ResponseParameters.EDUCATION_DETAIL)) { // checking user have education details
                educations = new ArrayList<>();
                for (int i = 0; i < profile.getJSONArray(ResponseParameters.EDUCATION_DETAIL).length(); i++) {
                    JSONObject jsonObject = profile.getJSONArray(ResponseParameters.EDUCATION_DETAIL).getJSONObject(i);
                    Education education = new Education();
                    education.setId(jsonObject.getString(ResponseParameters.Id));
                    education.setCourse(jsonObject.getString(RequestParameters.COURSE));
                    education.setSchool(jsonObject.getString(RequestParameters.SCHOOL));
                    education.setYearOfPassing(jsonObject.getString(RequestParameters.YEAR));
                    education.setDetails(jsonObject.getString(RequestParameters.DESCRIPTION));
                    educations.add(education);
                }
                educationListAdapter = new EducationListAdapter(this, educations);
                Log.e("profile", "" + educations.size());
                rvEducationList.setAdapter(educationListAdapter);

                if (profile.getJSONArray(ResponseParameters.EDUCATION_DETAIL).length() <= 0) { // checking if user have don't have education data
                    rvEducationList.setVisibility(GONE);
                    cvAddEducation.setVisibility(VISIBLE);
                } else {
                    rvEducationList.setVisibility(VISIBLE);
                    cvAddEducation.setVisibility(VISIBLE);
                }
            } else {
                rvEducationList.setVisibility(VISIBLE);
                cvAddEducation.setVisibility(VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //setting recommendations list
    private void setRecomRecyclerView(JSONArray recommendations) {
        try {
            //set up recycler view
            linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rvRecommendations.setLayoutManager(linearLayoutManager);
            rvRecommendations.setNestedScrollingEnabled(false);
            // rvOrganisationList.setHasFixedSize(true);
            List<Recommendations> recommendationsList = new ArrayList<>();
            for (int i = 0; i < recommendations.length(); i++) {
                JSONObject jsonObject = recommendations.getJSONObject(i);
                Recommendations recommendations1 = new Recommendations();
                User user = new User();
                user.setFirstName(jsonObject.getString(ResponseParameters.FirstName));
                user.setMiddleName(jsonObject.getString(ResponseParameters.MiddleName));
                user.setLastName(jsonObject.getString(ResponseParameters.LastName));
                user.setAvatar(jsonObject.getString(ResponseParameters.Avatar));
                user.setUserId(jsonObject.getString(ResponseParameters.UserId));
                user.setProfileHeadline(jsonObject.getString(ResponseParameters.PROFILE_HEADLINE));
                user.setProfileSummary(jsonObject.getString(ResponseParameters.PROFILE_SUMMERY));
                //user.setAadhaarInfo(results.getString(ResponseParameters.AADHARINFO));
                user.setCity(jsonObject.getString(ResponseParameters.City));
                user.setState(jsonObject.getString(ResponseParameters.State));
                user.setUserInfo(jsonObject.toString());
                recommendations1.setUser(user);
                recommendations1.setRecomId(jsonObject.getString(ResponseParameters.Id));
                recommendations1.setDescription(jsonObject.getString(ResponseParameters.DESCRIPTION));
                recommendations1.setStatus(jsonObject.getString(ResponseParameters.STATUS));
                recommendationsList.add(recommendations1);
            }
            recomRecyclerViewAdapter = new RecomRecyclerViewAdapter(rvRecommendations, this, recommendationsList);
            recomRecyclerViewAdapter.setShowSwitch(true);
            rvRecommendations.setAdapter(recomRecyclerViewAdapter);

            if (recommendations.length() <= 0) {
                tvRecom.setVisibility(VISIBLE);
            } else {
                tvRecom.setVisibility(GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //setClickListener for setting click listener
    @SuppressLint("ClickableViewAccessibility")
    private void setClickListener() {
        //cvAdharSave.setOnClickListener(this);
        cvSaveSkills.setOnClickListener(this);
        cvSaveDSkills.setOnClickListener(this);
        //ivQrCode.setOnClickListener(this);
        //ivCamera.setOnClickListener(this);
        cvAddEducation.setOnClickListener(this);
        cvAddProfession.setOnClickListener(this);
        cvSave.setOnClickListener(this);
        etDOB.setOnClickListener(this);
        //etAdharDOB.setOnClickListener(this);
        rlChangeImage.setOnClickListener(this);
        ivDesired.setOnClickListener(this);
        ivSkills.setOnClickListener(this);
        tvUpdatePhone.setOnClickListener(this);
        ivMobile.setOnClickListener(this);
        ivAddress.setOnClickListener(this);
        ivEmail.setOnClickListener(this);
        ivDOB.setOnClickListener(this);
        ivGender.setOnClickListener(this);
        ivChallenged.setOnClickListener(this);
        cvAddMoreSkills.setOnClickListener(this);
        cvAddMoreDSkills.setOnClickListener(this);
        cv_delete_acc.setOnClickListener(this);
        nvcontainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (v != null) {
                    v.close();
                }
                return false;
            }
        });

        cv_delete_acc.setText(Html.fromHtml("Click here to <font color='blue'>Delete your account</font>"));
    }

    //onActivityResult
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == DELETE) {
            showDeleteAccountPopup();
        }

        if (requestCode == REQUEST_CROP) {// for request crop of image
            Uri destination = Crop.getOutput(data);

            try {
                Bitmap bitmap1 = MediaStore.Images.Media.getBitmap(context.getContentResolver(), destination);
                bitmap1 = ConvertBitmap.Mytransform(bitmap1);
                bitmap1 = RotateImageCode.rotateImage(bitmap1, new File(Environment.getExternalStorageDirectory() + "/appy_celebration_temp.jpeg"));
                ByteArrayOutputStream datasecond = new ByteArrayOutputStream();
                bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, datasecond);
                byte[] bitmapdata = datasecond.toByteArray();

                File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temp2.jpeg");
                if (f.exists())
                    f.delete();
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bitmapdata);
                fo.close();
                String PicturePath = Environment.getExternalStorageDirectory() + File.separator + "temp2.jpeg";
                bitmap1 = BitmapFactory.decodeFile(PicturePath);

                //picByteArray = bitmapdata;


                if (bitmap1 != null) {
                    try {
                        ivUser.setImageBitmap(bitmap1);
                        HashMap<String, Object> input = new HashMap<>();

                        input.put(RequestParameters.AVATAR, "data:image/png;base64," + Base64.encodeToString(bitmapdata, Base64.NO_WRAP));
                        input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(context));
                        Log.e("input", "" + input);
                        API.sendRequestToServerPOST_PARAM(context, API.UPDATE_PROFILE_PIC, input);
                        pbImageLoading.setVisibility(View.VISIBLE);
                        cvEdit.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        } else { // for getting scanning data of qr code
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                //if qrcode has nothing in it
                if (result.getContents() == null) {
                    //Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
                    Utils.showPopup(this, "Result Not Found");
                } else {
                    //if qr contains data
                    try {
                        String scanContent = result.getContents();
                        //converting the data to json
                        if (scanContent != null && !scanContent.isEmpty()) {
                            processScannedData(scanContent);
                        }
                        //setting values to textviews
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showPopup(this, result.getContents());
                        //Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void showDeleteAccountPopup() {
        String s = "Are you sure you want to delete your kotumb account?";
        String delete_account = "Delete";
        String cancel = "Cancel";
        Utils.showPopup(this, s, delete_account, cancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickLogout();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void onClickLogout() {
        if (!Utility.isConnectingToInternet(this)) { // check net connection
            final Snackbar snackbar = Snackbar.make(header, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
            snackbar.show();
            return;
        }

        progressBar = new com.codeholic.kotumb.app.View.ProgressBar(this);
        progressBar.show(getResources().getString(R.string.app_please_wait_text));
//        alertDialog.dismiss();
        HashMap<String, Object> input = new HashMap<>();
        input.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(this));
        input.put(RequestParameters.TOKEN, "" + FirebaseInstanceId.getInstance().getToken());

        Log.e("request", "" + input);

        API.sendRequestToServerPOST_PARAM(this, API.LOGOUT, input);//service call for logout
    }

    //fill data of aadhar card from scanned data
    protected void processScannedData(String scanData) {
        Log.d("data", scanData);
        XmlPullParserFactory pullParserFactory;

        try {
            // init the parserfactory
            pullParserFactory = XmlPullParserFactory.newInstance();
            // get the parser
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(new StringReader(scanData));

            // parse the XML
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {
                    Log.d("log", "Start document");
                } else if (eventType == XmlPullParser.START_TAG && DataAttributes.AADHAAR_DATA_TAG.equals(parser.getName())) {
                    // extract data from tag
                    //uid
                    etAdharCardNumber.setText("" + parser.getAttributeValue(null, DataAttributes.AADHAR_UID_ATTR));
                    etConfirmAdharCardNumber.setText("" + parser.getAttributeValue(null, DataAttributes.AADHAR_UID_ATTR));
                    //name
                    etAdharName.setText("" + parser.getAttributeValue(null, DataAttributes.AADHAR_NAME_ATTR));
                    // year of birth
                    etAdharDOB.setText("" + parser.getAttributeValue(null, DataAttributes.AADHAR_YOB_ATTR));
                    // care of
                    etAdharFatherName.setText("" + parser.getAttributeValue(null, DataAttributes.AADHAR_CO_ATTR));
                    // Post Code
                    etAdharPin.setText("" + parser.getAttributeValue(null, DataAttributes.AADHAR_PC_ATTR));

                } else if (eventType == XmlPullParser.END_TAG) {
                    Log.d("Rajdeol", "End tag " + parser.getName());
                } else if (eventType == XmlPullParser.TEXT) {
                    Log.d("log", "Text " + parser.getText());
                }
                // update eventType
                eventType = parser.next();
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // adharValidation for aadhar fields
    private boolean adharValidation() {
        disableAdharError();
        boolean result = true;
        if (etAdharCardNumber.getText().toString().trim().isEmpty()) {
            enableError(etAdharCardNumberLayout, getResources().getString(R.string.aadhar_empty_input));
            result = false;
        }
        if (etAdharCardNumber.getText().toString().trim().length() < 12) {
            enableError(etAdharCardNumberLayout, getResources().getString(R.string.aadhar_incorrect_input_length));
            result = false;
        }
        if (etConfirmAdharCardNumber.getText().toString().trim().isEmpty()) {
            enableError(etConfirmAdharCardNumberLayout, getResources().getString(R.string.aadhar_empty_input));
            result = false;
        }
        if (!etConfirmAdharCardNumber.getText().toString().trim().equalsIgnoreCase(etAdharCardNumber.getText().toString().trim())) {
            enableError(etConfirmAdharCardNumberLayout, getResources().getString(R.string.aadhar_incorrect_input_number_match));
            result = false;
        }
        if (etAdharName.getText().toString().trim().isEmpty()) {
            enableError(etAdharNameLayout, getResources().getString(R.string.name_empty_input));
            result = false;
        }
        if (etAdharDOB.getText().toString().trim().isEmpty()) {
            enableError(etAdharDOBLayout, getResources().getString(R.string.dob_empty_input));
            result = false;
        }
        if (etAdharFatherName.getText().toString().trim().isEmpty()) {
            enableError(etAdharFatherNameLayout, getResources().getString(R.string.father_name_empty_input));
            result = false;
        }
        if (etAdharPin.getText().toString().trim().isEmpty()) {
            enableError(etAdharPinLayout, getResources().getString(R.string.zipcode_empty_input));
            result = false;
        }
        return result;
    }

    //disableAdharError for disabling aadhar errors
    private void disableAdharError() {
        etAdharCardNumberLayout.setErrorEnabled(false);
        etConfirmAdharCardNumberLayout.setErrorEnabled(false);
        etAdharNameLayout.setErrorEnabled(false);
        etAdharDOBLayout.setErrorEnabled(false);
        etAdharFatherNameLayout.setErrorEnabled(false);
        etAdharPinLayout.setErrorEnabled(false);
    }

    // skillValidation for Skills fields
    private boolean dSkillValidation() {
        disableDSkillsError();
        boolean result = true;
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(maDIndustries.getWindowToken(), 0);
        if (maDIndustries.getText().toString().trim().isEmpty()) {
            if (result) {
                maDIndustriesLayout.getParent().requestChildFocus(((ViewGroup) maDIndustriesLayout.getParent()).getChildAt(0), ((ViewGroup) maDIndustriesLayout.getParent()).getChildAt(0));
            }
//            enableError(maDIndustriesLayout, getResources().getString(R.string.industry_empty_select));
//            result = false;
        }

        if (maDSkills.getText().toString().trim().isEmpty()) {
            if (result) {
                maDSkills.clearFocus();
                //maDSkills.requestFocus();
                imm.showSoftInput(maDSkills, InputMethodManager.SHOW_IMPLICIT);
            }
            if (!maDIndustries.getText().toString().trim().isEmpty()) {
                enableError(maDSkillsLayout, getResources().getString(R.string.desired_skills_input_empty));
                result = false;
            }
        }

        for (int i = 0; i < llAddDSkills.getChildCount(); i++) {
            View view = llAddDSkills.getChildAt(i);
            TextInputLayout maSkillsLayout = view.findViewById(R.id.maSkillsLayout);
            MultiAutoCompleteTextView maSkills = view.findViewById(R.id.maSkills);
            TextInputLayout maIndustriesLayout = view.findViewById(R.id.maIndustriesLayout);
            AutoCompleteTextView maIndustries = view.findViewById(R.id.maIndustries);
            if (maIndustries.getText().toString().trim().isEmpty()) {
                if (result) {
                    maIndustriesLayout.getParent().requestChildFocus(((ViewGroup) maIndustriesLayout.getParent()).getChildAt(0), ((ViewGroup) maIndustriesLayout.getParent()).getChildAt(0));
                }
//                enableError(maIndustriesLayout, getResources().getString(R.string.industry_empty_select));
//                result = false;
            }
            if (maSkills.getText().toString().trim().isEmpty()) {
                if (result) {
                    maSkills.clearFocus();
                    //maSkills.requestFocus();
                    imm.showSoftInput(maSkills, InputMethodManager.SHOW_IMPLICIT);
                }
                if (!maIndustries.getText().toString().trim().isEmpty()) {
                    enableError(maSkillsLayout, getResources().getString(R.string.skills_empty_input));
                    result = false;
                }
            }
        }

        //Disabling Desired Skill Interest EditText validation, as per Gunjan mam, so that it can be saved empty also
       /* if (etDSkillsDes.getText().toString().trim().isEmpty()) {
            if (result) {
                etDSkillsDes.requestFocus();
                imm.showSoftInput(etDSkillsDes, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etDSkillsDesLayout, getResources().getString(R.string.interests_empty_input));
            result = false;
        }*/

        Object tag2 = maDSkills.getTag();
        if (!maDSkills.getText().toString().isEmpty() && tag2 != null) {
            if (!(boolean) tag2) {
                enableError(maDSkillsLayout, getString(R.string.select_from_dropdown_text));
                result = false;
            }
        }

        for (SkillsObject skillsObject : dskillsArray) {
            Object tag3 = skillsObject.autoCompleteTextView.getTag();
            if (tag3 != null) {
                if (!skillsObject.autoCompleteTextView.getText().toString().isEmpty() && !(boolean) tag3) {
                    enableError(skillsObject.textInputLayout, getString(R.string.select_from_dropdown_text));
                    result = false;
                }
            }
        }
        return result;
    }


    // skillValidation for Skills fields
    private boolean skillSaveValidation() {
        disableSkillsError();
        boolean result = true;
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(maDIndustries.getWindowToken(), 0);
        if (maIndustries.getText().toString().trim().isEmpty()) {
            if (result) {
                maIndustriesLayout.getParent().requestChildFocus(((ViewGroup) maIndustriesLayout.getParent()).getChildAt(0), ((ViewGroup) maIndustriesLayout.getParent()).getChildAt(0));
            }
//            enableError(maIndustriesLayout, getResources().getString(R.string.industry_empty_select));
//            result = false;
        }

        if (maSkills.getText().toString().trim().isEmpty()) {
            if (result) {
                maSkills.clearFocus();
                //maSkills.requestFocus();
                imm.showSoftInput(maDSkills, InputMethodManager.SHOW_IMPLICIT);
            }
            if (!maIndustries.getText().toString().trim().isEmpty()) {
                enableError(maSkillsLayout, getResources().getString(R.string.skills_empty_input));
                result = false;
            }
        }


        Object tag = maSkills.getTag();
        if (tag != null) {
            if (!maSkills.getText().toString().isEmpty() && !(boolean) tag) {
                enableError(maSkillsLayout, getString(R.string.select_from_dropdown_text));
                result = false;
            }
        }

        for (SkillsObject skillsObject : cskillsArray) {
            Object tag3 = skillsObject.autoCompleteTextView.getTag();
            if (tag3 != null) {
                if (!skillsObject.autoCompleteTextView.getText().toString().isEmpty() && !(boolean) tag3) {
                    enableError(skillsObject.textInputLayout, getString(R.string.select_from_dropdown_text));
                    result = false;
                }
            }
        }

        for (int i = 0; i < llAddSkills.getChildCount(); i++) {
            View view = llAddSkills.getChildAt(i);
            TextInputLayout maSkillsLayout = view.findViewById(R.id.maSkillsLayout);
            MultiAutoCompleteTextView maSkills = view.findViewById(R.id.maSkills);
            TextInputLayout maIndustriesLayout = view.findViewById(R.id.maIndustriesLayout);
            AutoCompleteTextView maIndustries = view.findViewById(R.id.maIndustries);
            if (maIndustries.getText().toString().trim().isEmpty()) {
                if (result) {
                    maIndustriesLayout.getParent().requestChildFocus(((ViewGroup) maIndustriesLayout.getParent()).getChildAt(0), ((ViewGroup) maIndustriesLayout.getParent()).getChildAt(0));
                }
//                enableError(maIndustriesLayout, getResources().getString(R.string.industry_empty_select));
//                result = false;
            }
            if (maSkills.getText().toString().trim().isEmpty()) {
                if (result) {
                    maSkills.clearFocus();
                    //   maSkills.requestFocus();
                    imm.showSoftInput(maSkills, InputMethodManager.SHOW_IMPLICIT);
                }
                if (!maIndustries.getText().toString().trim().isEmpty()) {
                    enableError(maSkillsLayout, getResources().getString(R.string.skills_empty_input));
                    result = false;
                }
            }
        }

       /* if (etSkillsDes.getText().toString().trim().isEmpty()) {
            enableError(etSkillsDesLayout, getResources().getString(R.string.skill_description_empty_input));
            result = false;
        }*/


        return result;
    }

    //disableSkillsError for disabling skills errors
    private void disableSkillsError() {
        maSkillsLayout.setErrorEnabled(false);
        maIndustriesLayout.setErrorEnabled(false);
        etSkillsDesLayout.setErrorEnabled(false);
    }

    //disableSkillsError for disabling skills errors
    private void disableDSkillsError() {
        maDIndustriesLayout.setErrorEnabled(false);
        maDSkillsLayout.setErrorEnabled(false);
        etDSkillsDesLayout.setErrorEnabled(false);
    }

    //disableError for disabling personal info errors
    void disableError() {
        try {
            etFirstNameLayout.setErrorEnabled(false);
            etLastNameLayout.setErrorEnabled(false);
            etDOBLayout.setErrorEnabled(false);
            etGenderLayout.setErrorEnabled(false);
            etPhoneLayout.setErrorEnabled(false);
            etPincodeLayout.setErrorEnabled(false);
            etCityLayout.setErrorEnabled(false);
            etStateLayout.setErrorEnabled(false);
            etEmailLayout.setErrorEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //enableError for enabling errors for TextInputLayout
    private void enableError(TextInputLayout inputLayout, String error) {
        try {
            inputLayout.setErrorEnabled(true);
            inputLayout.setError(error);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //setTextWatcher method for every edittext
    private void setTextWatcher(final EditText editText, final TextInputLayout textInputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    try {
                        textInputLayout.setErrorEnabled(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (editText == etPincode) {
                    try {
                        if (s.length() >= 6) {
                            if (!Utility.isConnectingToInternet(context)) {
                                return;
                            }
                            if (pin) {
                                pbPinCode.setVisibility(VISIBLE);
                                API.sendRequestToServerGET(context, API.ZIPCODES + "/" + s.toString(), API.ZIPCODES);
                            }
                            pin = true;
                        } else {
                            if (!Utility.isConnectingToInternet(context)) {
                                return;
                            }
                            if (pin) {
                                if (!s.toString().trim().isEmpty() && s.length() >= 6) {
                                    pbPinCode.setVisibility(VISIBLE);
                                    API.sendRequestToServerGET(context, API.ZIPCODE + s.toString().trim(), API.ZIPCODE);
                                }
                            }
                            pin = true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (s.length() >= 1) {
                    try {
                        if (editText == maSkills) {
//                            maSkills.showDropDown();
                            if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS).isEmpty()) {
                                if (!Utility.isConnectingToInternet(context)) {
                                    return;
                                }
                                if (skill) {
                                    if (!s.toString().trim().endsWith(",")) {
                                        pbSkill.setVisibility(VISIBLE);
                                        if (s.toString().trim().contains(",")) {
                                            API.sendRequestToServerGET(context, API.GET_SKILLS + s.toString().trim().substring(s.toString().trim().lastIndexOf(",") + 1).trim(), API.GET_SKILLS);
                                        } else {
                                            API.sendRequestToServerGET(context, API.GET_SKILLS + s.toString().trim(), API.GET_SKILLS);
                                        }
                                    }
                                }
                                skill = true;
                            } else {
                                if (skill) {
                                    getResponse(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS)), 4);
                                }
                                skill = true;
                            }
                        }

                        if (editText == maDSkills) {
                            maDSkills.showDropDown();
                            if (SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS).isEmpty()) {
                                if (!Utility.isConnectingToInternet(context)) {
                                    return;
                                }
                                if (dSkill) {
                                    if (!s.toString().trim().endsWith(",")) {
                                        pbDSkill.setVisibility(VISIBLE);
                                        if (s.toString().trim().contains(",")) {
                                            API.sendRequestToServerGET(context, API.GET_SKILLS + s.toString().trim().substring(s.toString().trim().lastIndexOf(",") + 1).trim(), API.GET_SKILLS);
                                        } else {
                                            API.sendRequestToServerGET(context, API.GET_SKILLS + s.toString().trim(), API.GET_SKILLS);
                                        }
                                    }
                                }
                                dSkill = true;
                            } else {
                                if (dSkill) {
                                    getResponse(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS)), 4);
                                }
                                dSkill = true;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editText == maSkills) {
                    maSkills.setTag(false);
                    Log.e("TAG", "maSkills");
                    if (maSkills.getText().toString().trim().isEmpty() || maSkills.getText().toString().trim().endsWith(",")) {
                        Log.e("TAG", "if");
                        maSkills.performClick();
                    }
                }

              /*  if (editText == maDSkills) {
                    maDSkills.setTag(false);
                    if (maDSkills.getText().toString().trim().isEmpty() || maDSkills.getText().toString().trim().endsWith(",")) {

                        maDSkills.performClick();
                    }
                }*/
            }
        });
    }

    //setEditTextWatcher for setting textwatcher for edittext
    private void setEditTextWatcher() {
        setTextWatcher(etAdharCardNumber, etAdharCardNumberLayout);
        setTextWatcher(etConfirmAdharCardNumber, etConfirmAdharCardNumberLayout);
        setTextWatcher(etAdharName, etAdharNameLayout);
        setTextWatcher(etAdharDOB, etAdharDOBLayout);
        setTextWatcher(etAdharFatherName, etAdharFatherNameLayout);
        setTextWatcher(etAdharPin, etAdharPinLayout);
        setTextWatcher(etFirstName, etFirstNameLayout);
        setTextWatcher(etLastName, etLastNameLayout);
        setTextWatcher(etDOB, etDOBLayout);
        setTextWatcher(etPhone, etPhoneLayout);
        setTextWatcher(etPincode, etPincodeLayout);
        setTextWatcher(etCity, etCityLayout);
        setTextWatcher(etState, etStateLayout);
        setTextWatcher(etEmail, etEmailLayout);
        setTextWatcher(etAPhone, etAPhoneLayout);
        setTextWatcher(etCAddress, etCAddressLayout);
        setTextWatcher(maSkills, maSkillsLayout);
        setTextWatcher(maIndustries, maIndustriesLayout);
        setTextWatcher(maDIndustries, maDIndustriesLayout);
        setTextWatcher(maDSkills, maDSkillsLayout);
        setTextWatcher(etSkillsDes, etSkillsDesLayout);
        setTextWatcher(etDSkillsDes, etDSkillsDesLayout);
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(final View v) {

        switch (v.getId()) {

            case R.id.cv_delete_acc:
                Utils.showPopup(this, getString(R.string.delete_acc_msg_dialog), getString(R.string.primary_number_yes_btn_text), getString(R.string.primary_number_no_btn_text), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(CompleteProfileActivity.this, LoginActivity.class);
                        intent.putExtra("delete", true);
                        startActivityForResult(intent, DELETE);
                    }
                }, null);
                break;
            case R.id.cvAdharSave: // aadhar submit click
                try {
                    if (adharValidation()) { // validation
                        if (checkConnectivity(v)) return;
                        HashMap<String, Object> input = new HashMap<>();
                        input.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(this));
                        input.put(RequestParameters.ADHAR_NUMBER, "" + etAdharCardNumber.getText().toString().trim());
                        input.put(RequestParameters.CONFIRM_ADHAR_NUMBER, "" + etConfirmAdharCardNumber.getText().toString().trim());
                        input.put(RequestParameters.NAME_ADHAR, "" + etAdharName.getText().toString().trim());
                        input.put(RequestParameters.DATEOFBIRTH, "" + etAdharDOB.getText().toString().trim());
                        input.put(RequestParameters.FATHER_NAME, "" + etAdharFatherName.getText().toString().trim());
                        input.put(RequestParameters.PINCODE, "" + etAdharPin.getText().toString().trim());
                        input.put(RequestParameters.VERIFIED, "0");
                        Log.e("input", "" + input);
                        tvAdharSave.setVisibility(GONE);
                        pbAdharLoading.setVisibility(VISIBLE);
                        cvAdharSave.setClickable(false);
                        etAdharCardNumber.setEnabled(false);
                        etConfirmAdharCardNumber.setEnabled(false);
                        etAdharName.setEnabled(false);
                        etAdharDOB.setEnabled(false);
                        etAdharFatherName.setEnabled(false);
                        etAdharPin.setEnabled(false);
                        API.sendRequestToServerPOST_PARAM(context, API.ADHAR_VERIFICATION, input); // service call for aadhar details save
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.cvSaveSkills: // skills save click
                try {
                    if (skillSaveValidation()) { // validation
                        if (checkConnectivity(v)) return;


                        maIndustries.setEnabled(false);
                        ivIndustries.setClickable(false);
                        ivSkills.setClickable(false);
                        maSkills.setEnabled(false);
                        etSkillsDes.setEnabled(false);
                        cvSaveSkills.setClickable(false);
                        tvSaveSkills.setVisibility(GONE);
                        pbSkillsLoading.setVisibility(VISIBLE);

                        setSkillViewsDisable(llAddSkills, false);

                        HashMap<String, Object> input = new HashMap<>();
                        /*input.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(this));
                        input.put(RequestParameters.DESCRIPTION, "" + etSkillsDes.getText().toString().trim());*/

                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(this));
                            jsonObject.put(RequestParameters.DESCRIPTION, "" + etSkillsDes.getText().toString().trim());
                            jsonObject.put(RequestParameters.CURRENT_INDUSTRIES + "0", maIndustries.getText().toString().trim());
                            String currentSkills = "";
                            String currentCustomSkills = "";


                            List<String> selectedList = new ArrayList<>();
                            String tempText = "";
                            tempText = maSkills.getText().toString().trim().replace(", ", ",");

                            if (tempText.endsWith(",")) {
                                tempText = tempText.trim().substring(0, tempText.length() - 1);
                            }

                            if (!tempText.trim().isEmpty())
                                if (!tempText.contains(",")) {
                                    selectedList.add(tempText);
                                } else {
                                    String[] temp = tempText.trim().split(",");
                                    selectedList = Arrays.asList(temp);
                                }
                            List<String> skills = new ArrayList<>();
                            final JSONObject outPut = new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS));
                            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                                if (outPut.has(ResponseParameters.Success)) {
                                    if (outPut.has(ResponseParameters.SKILLS)) {
                                        SharedPreferencesMethod.setString(CompleteProfileActivity.this, SharedPreferencesMethod.SKILLS, outPut.toString());
                                        for (int j = 0; j < outPut.getJSONArray(ResponseParameters.SKILLS).length(); j++) {
                                            skills.add(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SKILL));
                                        }
                                    }
                                }
                            }


                            for (int i = 0; i < selectedList.size(); i++) {
                                if (skills.contains(selectedList.get(i).toString().trim()))
                                    currentSkills = currentSkills + "," + selectedList.get(i).toString().trim();
                                else
                                    currentCustomSkills = currentCustomSkills + "," + selectedList.get(i).toString().trim();
                            }
                            if (currentSkills.length() > 1)
                                jsonObject.put(RequestParameters.CURRENT_SKILLS + "_0", currentSkills.substring(1));
                            else
                                jsonObject.put(RequestParameters.CURRENT_SKILLS + "_0", "");
                            if (currentCustomSkills.length() > 1)
                                jsonObject.put(RequestParameters.CURRENT_CUSTOM_SKILLS + "0", currentCustomSkills.substring(1));
                            else
                                jsonObject.put(RequestParameters.CURRENT_CUSTOM_SKILLS + "0", "");
                            jsonObject = getSkillViewsJSON(jsonObject, llAddSkills, skills);
                            Log.e("request", jsonObject.toString());
                            input = jsonToMap(jsonObject);
                            Log.e("request", input.toString());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        API.sendRequestToServerPOST_PARAM(context, API.ADD_SKILLS, input); // service call for data skills save
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.cvSaveDSkills: // skills save click
                try {
                    if (dSkillValidation()) { // validation
                        if (checkConnectivity(v)) return;
                        maDIndustries.setEnabled(false);
                        ivDIndustries.setClickable(false);
                        ivDesired.setClickable(false);
                        maDSkills.setEnabled(false);
                        etDSkillsDes.setEnabled(false);
                        cvSaveDSkills.setClickable(false);
                        tvSaveDSkills.setVisibility(GONE);
                        pbDSkillsLoading.setVisibility(VISIBLE);

                        setSkillViewsDisable(llAddDSkills, false);

                       /* input.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(this));
                        input.put(RequestParameters.CURRENT_SKILLS, "" + maSkills.getText().toString().trim());
                        input.put(RequestParameters.DESCRIPTION, "" + etSkillsDes.getText().toString().trim());
                        input.put(RequestParameters.DESIRED_SKILLS, "" + maDSkills.getText().toString().trim());
                        input.put(RequestParameters.INTEREST, "" + etDSkillsDes.getText().toString().trim());*/


                        HashMap<String, Object> input = new HashMap<>();
                        /*input.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(this));
                        input.put(RequestParameters.DESCRIPTION, "" + etSkillsDes.getText().toString().trim());*/

                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(RequestParameters.USERID, SharedPreferencesMethod.getUserId(this));
                            jsonObject.put(RequestParameters.INTEREST, "" + etDSkillsDes.getText().toString().trim());
                            jsonObject.put(RequestParameters.CURRENT_INDUSTRIES + "0", maDIndustries.getText().toString().trim());
                            String currentSkills = "";
                            String currentCustomSkills = "";


                            List<String> selectedList = new ArrayList<>();
                            String tempText = "";
                            tempText = maDSkills.getText().toString().trim().replace(", ", ",");

                            if (tempText.endsWith(",")) {
                                tempText = tempText.trim().substring(0, tempText.length() - 1);
                            }

                            if (!tempText.trim().isEmpty())
                                if (!tempText.contains(",")) {
                                    selectedList.add(tempText);
                                } else {
                                    String[] temp = tempText.trim().split(",");
                                    selectedList = Arrays.asList(temp);
                                }
                            List<String> skills = new ArrayList<>();
                            final JSONObject outPut = new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS));
                            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                                if (outPut.has(ResponseParameters.Success)) {
                                    if (outPut.has(ResponseParameters.SKILLS)) {
                                        SharedPreferencesMethod.setString(CompleteProfileActivity.this, SharedPreferencesMethod.SKILLS, outPut.toString());
                                        for (int j = 0; j < outPut.getJSONArray(ResponseParameters.SKILLS).length(); j++) {
                                            skills.add(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SKILL));
                                        }
                                    }
                                }
                            }


                            for (int i = 0; i < selectedList.size(); i++) {
                                if (skills.contains(selectedList.get(i).toString().trim()))
                                    currentSkills = currentSkills + "," + selectedList.get(i).toString().trim();
                                else
                                    currentCustomSkills = currentCustomSkills + "," + selectedList.get(i).toString().trim();
                            }
                            if (currentSkills.length() > 0)
                                jsonObject.put(RequestParameters.DESIRED_SKILLS + "_0", currentSkills.substring(1));
                            else
                                jsonObject.put(RequestParameters.DESIRED_SKILLS + "_0", "");
                            if (currentCustomSkills.length() > 0)
                                jsonObject.put(RequestParameters.CURRENT_CUSTOM_SKILLS + "0", currentCustomSkills.substring(1));
                            else
                                jsonObject.put(RequestParameters.CURRENT_CUSTOM_SKILLS + "0", "");
                            jsonObject = getSkillViewsJSON(jsonObject, llAddDSkills, skills);
                            Log.e("request", jsonObject.toString());
                            input = jsonToMap(jsonObject);
                            Log.e("request", input.toString());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        API.sendRequestToServerPOST_PARAM(context, API.ADD_DSKILLS, input); // service call for data skills save
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.etAdharDOB:
            case R.id.etDOB: // dob click
                //date picker dialog
                //showCal((EditText) v);
                showCal2((EditText) v);
                break;
            case R.id.ivQrCode:
            case R.id.ivCamera: // for qr code scanner
                qrScan.setPrompt(getResources().getString(R.string.app_qr_code_message));
                qrScan.initiateScan();
                break;
            case R.id.cvAddEducation: // for add education screen
                startActivity(new Intent(context, EducationActivity.class));
                break;
            case R.id.cvAddProfession: // for add profession screen
                startActivity(new Intent(context, ProfessionActivity.class));
                break;
            case R.id.rlChangeImage: // update profile image
                galleryOrCameraPopup();
                break;
            case R.id.ivDesired: // for getting focus on desired skills text box
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                maDSkills.setSelection(maDSkills.getText().length());
                maDSkills.requestFocus();
                imm.showSoftInput(maDSkills, InputMethodManager.SHOW_IMPLICIT);
                break;
            case R.id.ivSkills: // for getting focus on skills text box
                InputMethodManager immS = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                maSkills.setSelection(maSkills.getText().length());
                maSkills.requestFocus();
                immS.showSoftInput(maSkills, InputMethodManager.SHOW_IMPLICIT);
                break;

            case R.id.cv_save: // for savinf personal info
                OnSaveProfileClick(v);
                break;
            case R.id.tvUpdatePhone:
                MobilePopup();
                break;
            case R.id.ivMobile:
                showToolTip(v, getResources().getString(R.string.mobile_visibility_helptext));
                break;
            case R.id.ivAddress:
                showToolTip(v, getResources().getString(R.string.address_visibility_helptext));
                break;
            case R.id.ivEmail:
                showToolTip(v, getResources().getString(R.string.Email_visibility_helptext));
                break;
            case R.id.ivGender:
                showToolTip(v, getResources().getString(R.string.gender_visibility_helptext));
                break;
            case R.id.ivChallenged:
                showToolTip(v, getResources().getString(R.string.help_tip_text_challenged));
                break;
            case R.id.ivDOB:
                showToolTip(v, getResources().getString(R.string.dob_visibility_helptext));
                break;
            case R.id.cvAddMoreSkills:
                Log.e("TAG", "adding");
                setupview(llAddSkills, new JSONObject(), true, true);
                break;
            case R.id.cvAddMoreDSkills:
                Log.e("TAG", "adding");
                setupview(llAddDSkills, new JSONObject(), false, true);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        validation();
        final SaveObject saveObject = getSaveObject();
        User userInfo;
        if (retrievedObject == null) {
            userInfo = SharedPreferencesMethod.getUserInfo(this);
        } else {
            userInfo = retrievedObject.getUser();
        }
        User user2 = saveObject.getUser();
        boolean equal = isEqual(userInfo, user2);
        boolean equalLang = isEqualLang(saveObject.languages, SharedPreferencesMethod.getString(CompleteProfileActivity.this, SharedPreferencesMethod.LANGUAGES));
        boolean b = !equal || !equalLang;
        if (b || retrievedObject != null) {
            if (!dontShowDialog) {
                //nvcontainer.smoothScrollTo(0,0);
                showChangesNotSavedDialog(saveObject, true);
            } else {
                String s = new Gson().toJson(retrievedObject);
                SharedPreferencesMethod.setString(CompleteProfileActivity.this, ResponseParameters.PROFILE, s);
                onBackPressed = true;
                super.onBackPressed();
            }
        } else {
            onBackPressed = true;
            super.onBackPressed();
            finish();
        }
    }

    private void showChangesNotSavedDialog(final SaveObject saveObject, final boolean onBackPressed) {
        String string;
        String yes;
        String no;
        if (onBackPressed) {
            string = getString(R.string.profile_changes_not_saved);
            yes = getString(R.string.primary_number_yes_btn_text);
            no = getString(R.string.primary_number_no_btn_text);
        } else {
            string = getString(R.string.profile_unsaved_changes_found);
            yes = getString(R.string.discard_btn_text);
            no = getString(R.string.continue_btn_text);
        }
        Utils.showPopup(this, string, yes, no, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!onBackPressed) {
                    fillValues(SharedPreferencesMethod.getUserInfo(CompleteProfileActivity.this));
                    SharedPreferencesMethod.setString(CompleteProfileActivity.this, ResponseParameters.PROFILE, "");
                } else {
                    OnSaveProfileClick(cvSave);
                    //SharedPreferencesMethod.setUserInfo(CompleteProfileActivity.this, getSaveObject().getUser());
                    SharedPreferencesMethod.setString(CompleteProfileActivity.this, ResponseParameters.PROFILE, "");
                }
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onBackPressed) {
                    String s = new Gson().toJson(saveObject);
                    CompleteProfileActivity.this.onBackPressed = onBackPressed;
                    SharedPreferencesMethod.setString(CompleteProfileActivity.this, ResponseParameters.PROFILE, "");
                    finish();
                } else {

                }
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        // Toast.makeText(context, ""+onBackPressed, Toast.LENGTH_SHORT).show();
        SaveObject saveObject = getSaveObject();
        User userInfo;
        if (retrievedObject == null) {
            userInfo = SharedPreferencesMethod.getUserInfo(this);
        } else {
            userInfo = retrievedObject.getUser();
        }
        User user2 = saveObject.getUser();
        boolean equal = isEqual(userInfo, user2);
        String languages1 = SharedPreferencesMethod.getString(CompleteProfileActivity.this, SharedPreferencesMethod.LANGUAGES);
        boolean equalLang = isEqualLang(saveObject.languages, languages1);
        if (!onBackPressed) {
            if (!equal || !equalLang) {
                String s = new Gson().toJson(saveObject);
                SharedPreferencesMethod.setString(CompleteProfileActivity.this, ResponseParameters.PROFILE, s);
                // Toast.makeText(context, "Saved as "+user2.getMiddleName(), Toast.LENGTH_SHORT).show();
            } else {
                // Toast.makeText(context, "Not Saved\nonBackPressed:" + onBackPressed + "\nequal:" + equal+ "\nequalLang:" + equalLang, Toast.LENGTH_LONG).show();
                Log.d("Ayush", "Not Saved\nonBackPressed:" + onBackPressed + "\nequal:" + equal);
                Log.d("Ayush", "SaveObject: " + user2.getMiddleName() + "\nuserInfo: " + userInfo.getMiddleName());
            }
        }
    }

    private boolean isEqualLang(String languages, String languages1) {
       /* if (languages.length()>0) {
            languages = languages.substring(0, languages.trim().length() - 1);
        }*/
        languages = languages.replace(",", "");
        languages = languages.replace(" ", "");
        languages1 = languages1.replace(",", "");
        languages1 = languages1.replace(" ", "");
        return languages.equalsIgnoreCase(languages1);
    }

    private boolean isEqual(@NonNull User user1, @NonNull User user2) {
        int count = 0;
        if (!user1.getFirstName().equalsIgnoreCase(user2.getFirstName())) {
            count++;
//            return false;
        }
        if (!user1.getLastName().equalsIgnoreCase(user2.getLastName())) {
            count++;
//            return false;
        }
        if (!user1.getMiddleName().equalsIgnoreCase(user2.getMiddleName())) {
            count++;
//            return false;
        }
        if (!user1.getDOB().equalsIgnoreCase(user2.getDOB())) {
            count++;
//            return false;
        }
        if (!user1.getDobVisibility().equalsIgnoreCase(user2.getDobVisibility())) {
            count++;
//            return false;
        }
        if (!user1.getGender().equalsIgnoreCase(user2.getGender())) {
            count++;
//            return false;
        }
        if (!user1.getGenderVisibility().equalsIgnoreCase(user2.getGenderVisibility())) {
            count++;
//            return false;
        }
        if (!user1.getProfileSummary().equalsIgnoreCase(user2.getProfileSummary())) {
            count++;
//            return false;
        }
        if (!user1.getProfileHeadline().trim().equalsIgnoreCase(user2.getProfileHeadline().trim())) {
            count++;
//            return false;
        }
        if (!user1.getMobileNumber().equalsIgnoreCase(user2.getMobileNumber())) {
            count++;
//            return false;
        }
        if (!user1.getMobileVisibility().equalsIgnoreCase(user2.getMobileVisibility())) {
            count++;
//            return false;
        }
        if (!user1.getEmail().equalsIgnoreCase(user2.getEmail())) {
            count++;
//            return false;
        }
        if (!user1.getEmailVisibility().equalsIgnoreCase(user2.getEmailVisibility())) {
            count++;
//            return false;
        }
        if (!user1.getAddress().equalsIgnoreCase(user2.getAddress())) {
            count++;
//            return false;
        }
        if (!user1.getAddressVisibility().equalsIgnoreCase(user2.getAddressVisibility())) {
            count++;
//            return false;
        }
        if (!user1.getZipCode().equalsIgnoreCase(user2.getZipCode())) {
            count++;
//            return false;
        }
        if (!user1.getCity().equalsIgnoreCase(user2.getCity())) {
            count++;
//            return false;
        }
        if (!user1.getState().equalsIgnoreCase(user2.getState())) {
            count++;
//            return false;
        }
        if (count == 0) {
            return true;
        } else {
            //Toast.makeText(context, count + " changes found", Toast.LENGTH_SHORT).show();
            return false;
        }
    }


    private SaveObject getSaveObject() {
        TextView usernameTV = findViewById(R.id.username);
        User user = new User();
        String userName = usernameTV.getText().toString();
        userName = userName.replace("@", "");
        user.setUserName(userName);
        user.setUserId(SharedPreferencesMethod.getUserId(CompleteProfileActivity.this));
        user.setFirstName("" + etFirstName.getText().toString().trim());
        user.setMiddleName("" + etMiddleName.getText().toString().trim());
        user.setLastName("" + etLastName.getText().toString().trim());
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date date = df_.parse(etDOB.getText().toString().trim());
            user.setDOB(df.format(date) + "");
        } catch (ParseException e) {
            e.printStackTrace();
            user.setDOB("");
        }
        user.setGender(gender + "");
        user.setEmail("" + etEmail.getText().toString().trim());
        user.setCity("" + etCity.getText().toString().trim());
        user.setState(etState.getText().toString().trim() + "");
        user.setAddress(etCAddress.getText().toString().trim() + "");
        user.setZipCode(etPincode.getText().toString().trim() + "");
        user.setProfileSummary(etProfileSummery.getText().toString().trim() + "");
        user.setProfileHeadline(etProfileHeadLine.getText().toString().trim() + "");
        user.setMobileNumber(etPhone.getText().toString().trim() + "");
        user.setAddressVisibility(cbAddress.isChecked() ? "0" : "1");
        user.setDobVisibility(cbDOB.isChecked() ? "0" : "1");
        user.setEmailVisibility(cbEmail.isChecked() ? "0" : "1");
        user.setMobileVisibility(cbMobile.isChecked() ? "0" : "1");
        user.setGenderVisibility(cbGender.isChecked() ? "0" : "1");
        String languages = maLanguage.getText().toString().trim();
        return new SaveObject(user, languages);
    }

    class SaveObject {
        private User user;
        private String languages;

        SaveObject(User user, String languages) {
            this.user = user;
            this.languages = languages;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getLanguages() {
            return languages;
        }

        public void setLanguages(String languages) {
            this.languages = languages;
        }
    }

    private void OnSaveProfileClick(View v) {
        if (validation()) { // validation
            try {
                if (checkConnectivity(v)) return;
                HashMap<String, Object> input = new HashMap<>();
                input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(CompleteProfileActivity.this));
                input.put(RequestParameters.FIRSTNAME, "" + etFirstName.getText().toString().trim());
                input.put(RequestParameters.MIDDLENAME, "" + etMiddleName.getText().toString().trim());
                input.put(RequestParameters.LASTNAME, "" + etLastName.getText().toString().trim());

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");
                Date date = df_.parse(etDOB.getText().toString().trim());


                input.put(RequestParameters.DATEOFBIRTH, "" + df.format(date));
                input.put(RequestParameters.GENDER, "" + gender);
                input.put(RequestParameters.EMAIL, "" + etEmail.getText().toString().trim());
                input.put(RequestParameters.CITY, "" + etCity.getText().toString().trim());
                input.put(RequestParameters.STATE, "" + etState.getText().toString().trim());
                input.put(RequestParameters.ADDRESS, "" + etCAddress.getText().toString().trim());
                input.put(RequestParameters.ZIP, "" + etPincode.getText().toString().trim());
                if (maLanguage.getText().toString().trim().length() > 0) {
                    String lang = maLanguage.getText().toString().trim().substring(0, maLanguage.getText().toString().trim().length() - 1);
                    input.put(RequestParameters.LANGUAGE, "" + lang.trim());
                    SharedPreferencesMethod.setString(this, SharedPreferencesMethod.LANGUAGES, lang);
                } else {
                    input.put(RequestParameters.LANGUAGE, "");
                    SharedPreferencesMethod.setString(this, SharedPreferencesMethod.LANGUAGES, "");
                }

                //input.put(RequestParameters.ALTERNATE_NUMBER, "" + etAPhone.getText().toString().trim());
                input.put(RequestParameters.PROFILE_SUMMERY, "" + etProfileSummery.getText().toString().trim());
                input.put(RequestParameters.PROFILE_HEADLINE, "" + etProfileHeadLine.getText().toString().trim());
                if (cbEmail.isChecked()) {
                    input.put(RequestParameters.SHOW_EMAIL, "0");
                } else {
                    input.put(RequestParameters.SHOW_EMAIL, "1");
                }
                if (!cbChallenged.isChecked()) {
                    input.put(RequestParameters.IS_CHALLENGED, "0");
                } else {
                    input.put(RequestParameters.IS_CHALLENGED, "1");
                }
                if (cbMobile.isChecked()) {
                    input.put(RequestParameters.SHOW_MOBILE, "0");
                } else {
                    input.put(RequestParameters.SHOW_MOBILE, "1");
                }
                if (cbAddress.isChecked()) {
                    input.put(RequestParameters.SHOW_ADDRESS, "0");
                } else {
                    input.put(RequestParameters.SHOW_ADDRESS, "1");
                }
                if (cbDOB.isChecked()) {
                    input.put(RequestParameters.SHOW_DOB, "0");
                } else {
                    input.put(RequestParameters.SHOW_DOB, "1");
                }
                if (cbGender.isChecked()) {
                    input.put(RequestParameters.SHOW_GENDER, "0");
                } else {
                    input.put(RequestParameters.SHOW_GENDER, "1");
                }
                setDisable();

                Log.e("request", "" + input);
                dialog = new ProgressDialog(this);
                dialog.setMessage("Saving");
                dialog.show();
                API.sendRequestToServerPOST_PARAM(context, API.UPDATE_PERSONAL_INFO, input); // service call for updating personal info

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkConnectivity(View v) {
        if (!Utility.isConnectingToInternet(context)) { // net connection check
            final Snackbar snackbar = Snackbar.make(v, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
            snackbar.show();
            return true;
        }
        return false;
    }

    private void showCal2(final EditText v) {
        Calendar instance = Calendar.getInstance();
        String[] split = {};
        String setDate = v.getText().toString();

        int year = instance.get(Calendar.YEAR);
        int monthIndexedFromZero = instance.get(Calendar.MONTH);
        int day = instance.get(Calendar.DAY_OF_MONTH);

        if (!setDate.isEmpty()) {
            split = setDate.split("-");
            year = Integer.parseInt(split[2]);
            monthIndexedFromZero = Integer.parseInt(split[1]) - 1;
            day = Integer.parseInt(split[0]);
        }

        new SpinnerDatePickerDialogBuilder()
                .context(CompleteProfileActivity.this)
                .callback(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        monthOfYear += 1;
                        String month = "" + monthOfYear;
                        String date = "" + dayOfMonth;
                        if (date.length() == 1) {
                            date = "0" + date;
                        }
                        if (month.length() == 1) {
                            month = "0" + month;
                        }
                        //filling date into edittext
                        v.setText(date + "-" + month + "-" + year);
                        //((EditText) v).setText(year + "-" + month + "-" + date);
                    }
                })
                .spinnerTheme(R.style.NumberPickerStyle)
                .showTitle(true)
                .showDaySpinner(true)
                .defaultDate(year, monthIndexedFromZero, day)
                .maxDate(instance.get(Calendar.YEAR) - 16, instance.get(Calendar.MONTH), instance.get(Calendar.DAY_OF_MONTH))
                .minDate(instance.get(Calendar.YEAR) - 100, instance.get(Calendar.MONTH), instance.get(Calendar.DAY_OF_MONTH))
                .build()
                .show();
    }

    private void showCal(final EditText v) {
        DatePickerFragmentDialog datePickerDialog = DatePickerFragmentDialog.newInstance(new DatePickerFragmentDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerFragmentDialog view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear += 1;
                String month = "" + monthOfYear;
                String date = "" + dayOfMonth;
                if (date.length() == 1) {
                    date = "0" + date;
                }
                if (month.length() == 1) {
                    month = "0" + month;
                }
                //filling date into edittext
                v.setText(date + "-" + month + "-" + year);
                //((EditText) v).setText(year + "-" + month + "-" + date);
            }
        }, year, month, day);


        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date date = df.parse("1947-01-01");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            datePickerDialog.setMinDate(calendar.getTimeInMillis());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, -18); // for minimum age of 18
            datePickerDialog.setMaxDate(cal.getTimeInMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }
        datePickerDialog.setTitle("");
        datePickerDialog.show(getSupportFragmentManager(), "");
    }


    public static HashMap<String, Object> jsonToMap(JSONObject json) throws JSONException {
        HashMap<String, Object> retMap = new HashMap<String, Object>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static HashMap<String, Object> toMap(JSONObject object) throws JSONException {
        HashMap<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }


    private void setSkillViewsDisable(LinearLayout llAddSkills, boolean enabled) {
        for (int i = 0; i < llAddSkills.getChildCount(); i++) {
            View view = llAddSkills.getChildAt(i);
            MultiAutoCompleteTextView maSkills = view.findViewById(R.id.maSkills);
            AutoCompleteTextView maIndustries = view.findViewById(R.id.maIndustries);
            ImageView ivIndustries = view.findViewById(R.id.ivIndustries);
            ImageView ivSkills = view.findViewById(R.id.ivSkills);
            maIndustries.setEnabled(enabled);
            maSkills.setEnabled(enabled);
            ivIndustries.setClickable(enabled);
            ivSkills.setClickable(enabled);
        }
    }

    private JSONObject getSkillViewsJSON(JSONObject jsonObject, LinearLayout llAddSkills, List<String> skills) {
        try {
            for (int i = 0; i < llAddSkills.getChildCount(); i++) {
                View view = llAddSkills.getChildAt(i);
                String currentSkills = "";
                String currentCustomSkills = "";
                MultiAutoCompleteTextView maSkills = view.findViewById(R.id.maSkills);
                AutoCompleteTextView maIndustries = view.findViewById(R.id.maIndustries);
                jsonObject.put(RequestParameters.CURRENT_INDUSTRIES + "" + (i + 1), maIndustries.getText().toString().trim());
                List<String> selectedList = new ArrayList<>();
                String tempText = "";
                tempText = maSkills.getText().toString().trim().replace(", ", ",");

                if (tempText.endsWith(",")) {
                    tempText = tempText.trim().substring(0, tempText.length() - 1);
                }

                if (!tempText.trim().isEmpty())
                    if (!tempText.contains(",")) {
                        selectedList.add(tempText);
                    } else {
                        String[] temp = tempText.trim().split(",");
                        selectedList = Arrays.asList(temp);
                    }

                for (int j = 0; j < selectedList.size(); j++) {
                    if (skills.contains(selectedList.get(j).toString().trim()))
                        currentSkills = currentSkills + "," + selectedList.get(j).toString().trim();
                    else
                        currentCustomSkills = currentCustomSkills + "," + selectedList.get(j).toString().trim();
                }

                if (llAddSkills == CompleteProfileActivity.this.llAddSkills) {
                    if (currentSkills.length() > 1)
                        jsonObject.put(RequestParameters.CURRENT_SKILLS + "_" + (i + 1), currentSkills.substring(1));
                    else
                        jsonObject.put(RequestParameters.CURRENT_SKILLS + "_" + (i + 1), "");
                } else if (llAddSkills == CompleteProfileActivity.this.llAddDSkills) {
                    if (currentSkills.length() > 1)
                        jsonObject.put(RequestParameters.DESIRED_SKILLS + "_" + (i + 1), currentSkills.substring(1));
                    else
                        jsonObject.put(RequestParameters.DESIRED_SKILLS + "_" + (i + 1), "");
                }
                if (currentCustomSkills.length() > 1)
                    jsonObject.put(RequestParameters.CURRENT_CUSTOM_SKILLS + (i + 1), currentCustomSkills.substring(1));
                else
                    jsonObject.put(RequestParameters.CURRENT_CUSTOM_SKILLS + (i + 1), "");
            }
            return jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void setupview(final LinearLayout llAddSkills, JSONObject jsonObject, final boolean current, final boolean manual) {
        try {
            LayoutInflater inflater = getLayoutInflater();
            final View view = inflater.inflate(R.layout.layout_new_skill_information, null);
            final AutoCompleteTextView maIndustries = view.findViewById(R.id.maIndustries);
            ImageView ivIndustries = view.findViewById(R.id.ivIndustries);
            ImageView ivSkills = view.findViewById(R.id.ivSkills);
            final TextInputLayout maSkillsLayout = view.findViewById(R.id.maSkillsLayout);
            TextInputLayout maIndustriesLayout = view.findViewById(R.id.maIndustriesLayout);
            final TextView tvRemove = view.findViewById(R.id.tvRemove);
            tvRemove.setTag(cskillCount);
            TextView tvSkillsType = view.findViewById(R.id.tvSkillsType);
            if (llAddSkills == llAddDSkills) {
                tvSkillsType.setText(getResources().getString(R.string.profile_desired_skills_title));
            } else {
                tvSkillsType.setText(getResources().getString(R.string.profile_skills_title));
            }
            final MultiAutoCompleteTextView maSkills = view.findViewById(R.id.maSkills);
            if (current) {
                cskillsArray.add(new SkillsObject(maSkills, maSkillsLayout, tvRemove));
            } else {
                dskillsArray.add(new SkillsObject(maSkills, maSkillsLayout, tvRemove));
            }
            setTextWatcher(maIndustries, maIndustriesLayout);


            maSkills.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() > 0) {
                        maSkillsLayout.setErrorEnabled(false);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (manual) {
                        maSkills.setTag(false);
                    }
                    if (maSkills.getText().toString().trim().isEmpty() || maSkills.getText().toString().trim().endsWith(",")) {
                        Log.e("TAG", "if");
                        maSkills.performClick();
                    }
                }
            });


            ivSkills.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputMethodManager immS = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    maSkills.setSelection(maSkills.getText().length());
                    maSkills.requestFocus();
                    immS.showSoftInput(maSkills, InputMethodManager.SHOW_IMPLICIT);
                }
            });


            tvRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("ERROR", "" + llAddSkills.indexOfChild(view));
                    llAddSkills.removeView(view);
                    try {
                        if (current) {
                            cskillsArray.remove((int) tvRemove.getTag());
                            cskillCount--;
                        } else {
                            dskillsArray.remove((int) tvRemove.getTag());
                            dskillCount--;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(CompleteProfileActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });


            if (jsonObject.has(ResponseParameters.SECTOR) && jsonObject.has(ResponseParameters.SKILL)) {
                maIndustries.setText(jsonObject.getString(ResponseParameters.SECTOR));
                maSkills.setText(jsonObject.getString(ResponseParameters.SKILL) + ", ");
                maSkills.setSelection(maSkills.getText().length());
            }

            maSkills.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        setupSkillsAdapter(new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS)), maSkills, maIndustries);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    maSkills.showDropDown();
                }
            });

            maSkills.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(maSkills.getWindowToken(), 0);
                }
            });


            maSkills.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus)
                        maSkills.performClick();
                }
            });


            maIndustries.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("onclick", "onclick");
                    try {
                        setUpIndustriesAdapter(new JSONObject(SharedPreferencesMethod.getString(CompleteProfileActivity.this, SharedPreferencesMethod.INDUSTRIES)), maIndustries);
                        maIndustries.showDropDown();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }




//                    Log.e("onclick", "onclick");
//                    try {
//                        if (!SharedPreferencesMethod.getString(CompleteProfileActivity.this, SharedPreferencesMethod.INDUSTRIES).toString().isEmpty()) {
//                            setUpIndustriesAdapter(new JSONObject(SharedPreferencesMethod.getString(CompleteProfileActivity.this, SharedPreferencesMethod.INDUSTRIES)), maIndustries);
//                            maIndustries.showDropDown();
//                        } else {
//                            if (!Utility.isConnectingToInternet(context)) {
//                                return;
//                            }
//                            API.sendRequestToServerGET(context, API.GET_INDUSTRIES, API.GET_INDUSTRIES);
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }
            });

            ivIndustries.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!maIndustries.getText().toString().trim().isEmpty()) {
                        maIndustries.setText("");
                        maSkills.setText("");
                    }
                }
            });


            try {

                maSkills.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        try {
                            final JSONObject outPut = new JSONObject(SharedPreferencesMethod.getString(context, SharedPreferencesMethod.SKILLS));
                            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                                if (outPut.has(ResponseParameters.Success)) {
                                    if (outPut.has(ResponseParameters.SKILLS)) {
                                        SharedPreferencesMethod.setString(CompleteProfileActivity.this, SharedPreferencesMethod.SKILLS, outPut.toString());
                                        //sector text
                                        setUpIndustriesAdapter(outPut, maIndustries);

                                        List<String> skills = new ArrayList<>();
                                        List<String> selectedList = new ArrayList<>();

                                        //sector list
                                        List<String> selectedSectorList = new ArrayList<>();
                                        String tempText = "";
                                        String tempSectorText = "";
                                        if (maSkills.hasFocus()) {
                                            tempText = maSkills.getText().toString().trim().replace(", ", ",");
                                            //sector text
                                            tempSectorText = maIndustries.getText().toString().trim().replace(", ", ",");
                                        }

                                        if (tempText.endsWith(",")) {
                                            tempText = tempText.trim().substring(0, tempText.length() - 1);
                                        }
                                        if (tempSectorText.endsWith(",")) {
                                            tempSectorText = tempSectorText.trim().substring(0, tempSectorText.length() - 1);
                                        }

                                        Log.e("TAG 1", tempText + " " + tempSectorText);
                                        if (!tempText.trim().isEmpty())
                                            if (!tempText.contains(",")) {
                                                selectedList.add(tempText);
                                            } else {
                                                String[] temp = tempText.trim().split(",");
                                                selectedList = Arrays.asList(temp);
                                            }
                                        Log.e("TAG 2", selectedList.toString());

                                        //for sector list
                                        if (!tempSectorText.trim().isEmpty())
                                            if (!tempSectorText.contains(",")) {
                                                selectedSectorList.add(tempSectorText);
                                            } else {
                                                String[] temp = tempSectorText.trim().split(",");
                                                selectedSectorList = Arrays.asList(temp);
                                            }
                                        Log.e("TAG 3", selectedSectorList.toString());

                                        for (int j = 0; j < outPut.getJSONArray(ResponseParameters.SKILLS).length(); j++) {
                                          /*  if (!skills.contains(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SKILL))
                                                    && !selectedList.contains(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SKILL))
                                                    && selectedSectorList.contains(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SECTOR)))*/
                                            skills.add(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SKILL));
                                        }
//                                        skills.add("Other");
                                        String[] data = skills.toArray(new String[skills.size()]);
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(CompleteProfileActivity.this, R.layout.autocomplete_item, data);
                                        if (maSkills.hasFocus()) {
                                            maSkills.setAdapter(adapter);
                                            maSkills.setThreshold(1);
                                            Utils.addOtherOptionHandler(maSkills, skills, CompleteProfileActivity.this, true);
                                            if (!maSkills.getText().toString().trim().isEmpty() && !maSkills.getText().toString().trim().endsWith(","))
                                                maSkills.showDropDown();
                                            maSkills.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }


            llAddSkills.addView(view);
            if (current) {
                tvRemove.setTag(cskillCount);
                cskillCount++;
                //Toast.makeText(this, cskillCount + " added", Toast.LENGTH_SHORT).show();
            } else {
                tvRemove.setTag(dskillCount);
                dskillCount++;
                //Toast.makeText(this, dskillCount + " added", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    void showToolTip(View view, String toolTipTExt) {
        if (v != null) {
            v.close();
        }
        v = ViewTooltip
                .on(view)
                .autoHide(true, 20000)
                .clickToHide(true)
                .align(CENTER)
                .position(TOP)
                .text(toolTipTExt)
                .textColor(Color.WHITE)
                .color(getResources().getColor(R.color.colorPrimary))
                .corner(10)
                .onHide(new ViewTooltip.ListenerHide() {
                    @Override
                    public void onHide(View view) {
                        v = null;
                    }
                });
        v.show();
    }

    //galleryOrCameraPopup for selecting image
    public void galleryOrCameraPopup() {
        PickSetup pickSetup = new PickSetup().setTitle(getResources().getString(R.string.app_select_text));
        pickSetup.setTitleColor(getResources().getColor(R.color.textColor));
        pickSetup.setProgressText(getResources().getString(R.string.app_please_wait_text));
        pickSetup.setProgressTextColor(getResources().getColor(R.color.textColor));
        pickSetup.setCancelText(getResources().getString(R.string.cancel_btn_text));
        pickSetup.setCancelTextColor(getResources().getColor(R.color.colorPrimary));
        pickSetup.setButtonTextColor(getResources().getColor(R.color.textColor));
        pickSetup.setDimAmount(0.3f);
        pickSetup.setFlip(true);
        pickSetup.setMaxSize(500);
        pickSetup.setPickTypes(EPickType.GALLERY, EPickType.CAMERA);
        pickSetup.setCameraButtonText(getResources().getString(R.string.app_camera_btn_text));
        pickSetup.setGalleryButtonText(getResources().getString(R.string.app_gallery_btn_text));
        pickSetup.setButtonOrientationInt(LinearLayoutCompat.HORIZONTAL);
        pickSetup.setSystemDialog(false);
        pickSetup.setGalleryIcon(R.mipmap.gallery_colored);
        pickSetup.setCameraIcon(R.mipmap.camera_colored);
        pickSetup.setIconGravityInt(Gravity.LEFT);

        if (Build.VERSION.SDK_INT >= PermissionsUtils.SDK_INT_MARSHMALLOW) { // for permission
            if (PermissionsUtils.getInstance(context).requiredPermissionsGranted(context)) {
                PickImageDialog.build(pickSetup)
                        .setOnPickResult(this).show(this);
            }else {

                PickImageDialog.build(pickSetup)
                        .setOnPickResult(this).show(this);
            }
        } else {
            PickImageDialog.build(pickSetup)
                    .setOnPickResult(this).show(this);
            //new MyDialog(context, MyDialog.UPDATE_PROFILE, null).show();
        }
    }

    // onRequestPermissionsResult for geeting permission from user
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermissionsUtils.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                if (PermissionsUtils.getInstance(context).areAllPermissionsGranted(grantResults)) {
                    galleryOrCameraPopup();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    //setDisable for disabling all personal info fields
    private void setDisable() {
        tvSave.setVisibility(GONE);
        pbLoading.setVisibility(VISIBLE);
        cvSave.setClickable(false);
        rgGenderGroup.setEnabled(false);
        rbFemale.setEnabled(false);
        rbMale.setEnabled(false);
        rbOther.setEnabled(false);
        etFirstName.setEnabled(false);
        etLastName.setEnabled(false);
        etDOB.setEnabled(false);
        etPhone.setEnabled(false);
        etAPhone.setEnabled(false);
        etPincode.setEnabled(false);
        etCity.setEnabled(false);
        etState.setEnabled(false);
        etProfileSummery.setEnabled(false);
        etProfileHeadLine.setEnabled(false);
        etCAddress.setEnabled(false);
        etEmail.setEnabled(false);
        etGender.setEnabled(false);
        cbTerms.setClickable(false);
        cbAddress.setClickable(false);
        cbAddress.setEnabled(false);
        cbEmail.setClickable(false);
        cbEmail.setEnabled(false);
        cbMobile.setClickable(false);
        cbMobile.setEnabled(false);
        cbDOB.setClickable(false);
        cbDOB.setEnabled(false);
        cbGender.setClickable(false);
        cbGender.setEnabled(false);
        ivLanguage.setClickable(false);
        maLanguage.setEnabled(false);
    }

    // set enabling all personal info fields
    void setEnable() {
        tvSave.setVisibility(VISIBLE);
        pbLoading.setVisibility(GONE);
        cvSave.setClickable(true);
        rgGenderGroup.setEnabled(true);
        rbFemale.setEnabled(true);
        rbMale.setEnabled(true);
        rbOther.setEnabled(true);
        etFirstName.setEnabled(true);
        etLastName.setEnabled(true);
        etDOB.setEnabled(true);
        etPhone.setEnabled(true);
        etAPhone.setEnabled(true);
        etPincode.setEnabled(true);
        etCity.setEnabled(true);
        etState.setEnabled(true);
        etProfileSummery.setEnabled(true);
        etProfileHeadLine.setEnabled(true);
        etCAddress.setEnabled(true);
        etEmail.setEnabled(true);
        etGender.setEnabled(true);
        cbTerms.setClickable(true);
        cbAddress.setClickable(true);
        cbAddress.setEnabled(true);
        cbEmail.setClickable(true);
        cbEmail.setEnabled(true);
        cbMobile.setClickable(true);
        cbMobile.setEnabled(true);
        ivLanguage.setClickable(true);
        maLanguage.setEnabled(true);
        cbDOB.setClickable(true);
        cbDOB.setEnabled(true);
        cbGender.setClickable(true);
        cbGender.setEnabled(true);
    }


    void clearFocus() {
        etFirstName.clearFocus();
        etMiddleName.clearFocus();
        etLastName.clearFocus();
        etDOB.clearFocus();
        etPhone.clearFocus();
        etPincode.clearFocus();
        etCity.clearFocus();
        etState.clearFocus();
        etEmail.clearFocus();
        etGender.clearFocus();
        etCAddress.clearFocus();
    }

    //validation for checking personal info fields
    private boolean validation() {
        disableError();
        clearFocus();
        boolean result = true;
        boolean fNameMatcher = Pattern.matches("[a-zA-Z0-9 ]+", etFirstName.getText().toString().trim());
        boolean lNameMatcher = Pattern.matches("[a-zA-Z0-9 ]+", etLastName.getText().toString().trim());
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etFirstName.getWindowToken(), 0);
        if (etFirstName.getText().toString().trim().isEmpty()) {
            if (result) {
                etFirstName.requestFocus();
                imm.showSoftInput(etFirstName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etFirstNameLayout, getResources().getString(R.string.firstname_empty_input));
            Log.e("etFirstName", "etFirstName");
            result = false;
        } else if (!fNameMatcher) {
            if (result) {
                etFirstName.requestFocus();
                imm.showSoftInput(etFirstName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etFirstNameLayout, getResources().getString(R.string.name_incorrect_input_numeric));
            result = false;
        }
        if (etLastName.getText().toString().trim().isEmpty()) {
            Log.e("etLastName", "etLastName");
            if (result) {
                etLastName.requestFocus();
                imm.showSoftInput(etLastName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etLastNameLayout, getResources().getString(R.string.lastname_empty_input));
            result = false;
        } else if (!lNameMatcher) {
            if (result) {
                etLastName.requestFocus();
                imm.showSoftInput(etLastName, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etLastNameLayout, getResources().getString(R.string.name_incorrect_input_numeric));
            result = false;
        }
        if (etDOB.getText().toString().trim().isEmpty()) {
            enableError(etDOBLayout, getResources().getString(R.string.dob_empty_input));
            result = false;
        }
        if (gender.trim().isEmpty()) {
            enableError(etGenderLayout, getResources().getString(R.string.gender_choose_option));
            result = false;
        }
        if (etPhone.getText().toString().trim().isEmpty()) {
            if (result) {
                etPhone.requestFocus();
                imm.showSoftInput(etPhone, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etPhoneLayout, getResources().getString(R.string.mobile_empty_input));
            result = false;
        }

        if (!etEmail.getText().toString().trim().isEmpty() && !Utility.isEmailIdValid(etEmail.getText().toString().trim())) {
            if (result) {
                etEmail.requestFocus();
                imm.showSoftInput(etEmail, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etEmailLayout, getResources().getString(R.string.email_incorrect_input_format));
            result = false;
        }
        /*if (etCAddress.getText().toString().trim().isEmpty()) {
            if (result) {
                etCAddress.requestFocus();
                imm.showSoftInput(etCAddress, InputMethodManager.SHOW_IMPLICIT);
            }
            enableError(etCAddressLayout, getResources().getString(R.string.address_empty_input));
            result = false;
        }*/
        if (etPincode.getText().toString().trim().isEmpty()) {
            enableError(etPincodeLayout, getResources().getString(R.string.zipcode_empty_input));
            //Utils.showPopup(CompleteProfileActivity.this,  getResources().getString(R.string.zipcode_empty_input));
            if (result) {
                etPincode.requestFocus();
                imm.showSoftInput(etPincode, InputMethodManager.SHOW_IMPLICIT);
            }

            result = false;
        } else if (etPincode.getText().toString().trim().length() < 6) {
            enableError(etPincodeLayout, getResources().getString(R.string.zipcode_incorrect_input));
            if (result) {
                etPincode.requestFocus();
                imm.showSoftInput(etPincode, InputMethodManager.SHOW_IMPLICIT);
            }

            result = false;
        }
        if (etCity.getText().toString().trim().isEmpty()) {
            /*if(result) {
                etCity.requestFocus();
                imm.showSoftInput(etCity, InputMethodManager.SHOW_IMPLICIT);
            }*/
            enableError(etCityLayout, getResources().getString(R.string.city_empty_input));
            result = false;
        }
        if (etState.getText().toString().trim().isEmpty()) {
            /*etState.requestFocus();
            imm.showSoftInput(etState, InputMethodManager.SHOW_IMPLICIT);*/
            enableError(etStateLayout, getResources().getString(R.string.state_empty_input));
            result = false;
        }
         /*else if (!cbTerms.isChecked()) {
            final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.accept_terms_error), Toast.LENGTH_SHORT);
            snackbar.show();
            return false;
        }*/
        return result;
    }

    //setCheckedChangeListener for setting check change listener
    private void setCheckedChangeListener() {
        rgGenderGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbMale:
                        gender = "M";
                        break;
                    case R.id.rbFemale:
                        gender = "F";
                        break;
                    case R.id.rbOther:
                        gender = "O";
                        break;
                }
            }
        });
    }

    // fillValues method for filling values
    private void fillValues(User user) {
        try {
            etFirstName.setText(user.getOnlyFirstName());
            etMiddleName.setText(user.getMiddleName());
            etAdharName.setText(user.getFirstName());
            etAdharDOB.setText(user.getDOB());
            etLastName.setText(user.getLastName());

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat df_ = new SimpleDateFormat("dd-MM-yyyy");

            etDOB.setText(df_.format(df.parse(user.getDOB())));
            etPhone.setText(user.getMobileNumber());
            etAPhone.setText(user.getAlternateMobile());
            pin = false;
            etPincode.setText(user.getZipCode());
            etAdharPin.setText(user.getZipCode());
            etCity.setText(user.getCity());
            etState.setText(user.getState());
            etEmail.setText(user.getEmail());
            etProfileHeadLine.setText(user.getProfileHeadline());
            etProfileSummery.setText(user.getProfileSummary());
            if (user.getGender().equalsIgnoreCase("M")) {
                gender = "M";
                rbMale.setChecked(true);
            } else if (user.getGender().equalsIgnoreCase("F")) {
                gender = "F";
                rbFemale.setChecked(true);
            } else {
                gender = "O";
                rbOther.setChecked(true);
            }

            if (user.getEmailVisibility().equalsIgnoreCase("0")) {
                cbEmail.setChecked(true);
            } else {
                cbEmail.setChecked(false);
            }

            if (user.getIsChallenged().equalsIgnoreCase("1")) {
                cbChallenged.setChecked(true);
            } else {
                cbChallenged.setChecked(false);
            }

            if (user.getMobileVisibility().equalsIgnoreCase("0")) {
                cbMobile.setChecked(true);
            } else {
                cbMobile.setChecked(false);
            }
            if (user.getAddressVisibility().equalsIgnoreCase("0")) {
                cbAddress.setChecked(true);
            } else {
                cbAddress.setChecked(false);
            }

            if (user.getGenderVisibility().equalsIgnoreCase("0")) {
                cbGender.setChecked(true);
            } else {
                cbGender.setChecked(false);
            }

            if (user.getDobVisibility().equalsIgnoreCase("0")) {
                cbDOB.setChecked(true);
            } else {
                cbDOB.setChecked(false);
            }

            TextView username = findViewById(R.id.username);
            String userName = user.getUserName();
            username.setText("@" + userName);
            etCAddress.setText(user.getAddress());
            new AQuery(this).id(ivUser).image(API.imageUrl + user.getAvatar(), true, true, 300, R.drawable.default_profile);
         System.out.println("Image URL   "+user.getAvatar());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // fillFromJSON method filling data to edit text
    private void fillFromJSON() {
        try {
            if (profile.has(ResponseParameters.AADHAR_INFO)) { // checking for aadhar info
                JSONObject adharInfo = profile.getJSONObject(ResponseParameters.AADHAR_INFO);
                if (adharInfo.has(ResponseParameters.ADHAR_NUMBER)) {
                    etAdharCardNumber.setText(adharInfo.getString(ResponseParameters.ADHAR_NUMBER));
                    etConfirmAdharCardNumber.setText(adharInfo.getString(ResponseParameters.ADHAR_NUMBER));
                }
                if (adharInfo.has(ResponseParameters.NAME_ON_ADHAR)) {
                    etAdharName.setText(adharInfo.getString(ResponseParameters.NAME_ON_ADHAR));
                }
                if (adharInfo.has(ResponseParameters.DateOfBirth)) {
                    etAdharDOB.setText(adharInfo.getString(ResponseParameters.DateOfBirth));
                }
                if (adharInfo.has(ResponseParameters.FATHER_NAME)) {
                    etAdharFatherName.setText(adharInfo.getString(ResponseParameters.FATHER_NAME));
                }
                if (adharInfo.has(ResponseParameters.ZIP)) {
                    etAdharPin.setText(adharInfo.getString(ResponseParameters.ZIP));
                }
            }

            if (profile.has(ResponseParameters.USER_SKILLS)) { // checking for user skills
                String text = "";
                JSONArray skills = new JSONArray();
                ArrayList<String> sector = new ArrayList<>();
                for (int i = 0; i < profile.getJSONArray(ResponseParameters.USER_SKILLS).length(); i++) {
                    if (!sector.contains(profile.getJSONArray(ResponseParameters.USER_SKILLS).getJSONObject(i).getString(ResponseParameters.SECTOR).trim()))
                        sector.add(profile.getJSONArray(ResponseParameters.USER_SKILLS).getJSONObject(i).getString(ResponseParameters.SECTOR).trim());
                }


                for (int j = 0; j < sector.size(); j++) {
                    for (int i = 0; i < profile.getJSONArray(ResponseParameters.USER_SKILLS).length(); i++) {
                        if (sector.get(j).equalsIgnoreCase(profile.getJSONArray(ResponseParameters.USER_SKILLS).getJSONObject(i).getString(ResponseParameters.SECTOR).trim())) {
                            if (!text.isEmpty()) {
                                if (!profile.getJSONArray(ResponseParameters.USER_SKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim().isEmpty())
                                    text = text + ", " + profile.getJSONArray(ResponseParameters.USER_SKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim();
                            } else {
                                if (!profile.getJSONArray(ResponseParameters.USER_SKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim().isEmpty())
                                    text = profile.getJSONArray(ResponseParameters.USER_SKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim();
                            }
                        }
                    }
                    JSONObject skill = new JSONObject();
                    skill.put(ResponseParameters.SECTOR, sector.get(j));
                    skill.put(ResponseParameters.SKILL, text);
                    skills.put(skill);
                    text = "";
                }

                Log.e("skills", "" + skills.toString());
                if (skills.length() > 0) {
                    maIndustries.setText(skills.getJSONObject(0).getString(ResponseParameters.SECTOR).trim());
                    maSkills.setText(skills.getJSONObject(0).getString(ResponseParameters.SKILL).trim() + ", ");
                    maSkills.setSelection(maSkills.getText().length());
                    for (int i = 1; i < skills.length(); i++) {
                        setupview(llAddSkills, skills.getJSONObject(i), true, false);
                    }
                }

              /*  if (!text.isEmpty()) {
                    text = text + ", ";
                }
                skill = false;
                maSkills.setText(text);
                maSkills.setSelection(text.length());*/
            }
            if (profile.has(ResponseParameters.USER_DSKILLS)) { // checking for user desired skills
                String text = "";

                JSONArray skills = new JSONArray();
                ArrayList<String> sector = new ArrayList<>();
                for (int i = 0; i < profile.getJSONArray(ResponseParameters.USER_DSKILLS).length(); i++) {
                    if (!sector.contains(profile.getJSONArray(ResponseParameters.USER_DSKILLS).getJSONObject(i).getString(ResponseParameters.SECTOR).trim()))
                        sector.add(profile.getJSONArray(ResponseParameters.USER_DSKILLS).getJSONObject(i).getString(ResponseParameters.SECTOR).trim());
                }

                for (int j = 0; j < sector.size(); j++) {
                    for (int i = 0; i < profile.getJSONArray(ResponseParameters.USER_DSKILLS).length(); i++) {
                        if (!text.isEmpty()) {
                            if (!profile.getJSONArray(ResponseParameters.USER_DSKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim().isEmpty())
                                text = text + ", " + profile.getJSONArray(ResponseParameters.USER_DSKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim();
                        } else {
                            if (!profile.getJSONArray(ResponseParameters.USER_DSKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim().isEmpty())
                                text = profile.getJSONArray(ResponseParameters.USER_DSKILLS).getJSONObject(i).getString(ResponseParameters.SKILL).trim();
                        }
                    }
                    JSONObject skill = new JSONObject();
                    skill.put(ResponseParameters.SECTOR, sector.get(j));
                    skill.put(ResponseParameters.SKILL, text);
                    skills.put(skill);
                    text = "";
                }
                Log.e("skills", "" + skills.toString());
                if (skills.length() > 0) {
                    maDIndustries.setText(skills.getJSONObject(0).getString(ResponseParameters.SECTOR).trim());
                    maDSkills.setText(skills.getJSONObject(0).getString(ResponseParameters.SKILL).trim() + ", ");
                    maDSkills.setSelection(maDSkills.getText().length());
                    for (int i = 1; i < skills.length(); i++) {
                        setupview(llAddDSkills, skills.getJSONObject(i), false, false);
                    }
                }

               /* if (!text.isEmpty()) {
                    text = text + ", ";
                }
                dSkill = false;
                maDSkills.setText(text);
                maDSkills.setSelection(text.length());*/
            }
            if (profile.has(ResponseParameters.PersonalInfo)) { // checking for personal info
                JSONObject PERSONALINFO = profile.getJSONObject(ResponseParameters.PersonalInfo);
                if (PERSONALINFO.getString(ResponseParameters.SkillDescription) != null && !PERSONALINFO.getString(ResponseParameters.SkillDescription).equalsIgnoreCase("null")) {
                    etSkillsDes.setText("" + PERSONALINFO.getString(ResponseParameters.SkillDescription));
                }
                if (PERSONALINFO.getString(ResponseParameters.Interests) != null && !PERSONALINFO.getString(ResponseParameters.Interests).equalsIgnoreCase("null")) {
                    etDSkillsDes.setText("" + PERSONALINFO.getString(ResponseParameters.Interests));
                }
            }
            //filling language known
            String string = SharedPreferencesMethod.getString(CompleteProfileActivity.this, SharedPreferencesMethod.LANGUAGES);
            maLanguage.setText(string);
            maLanguage.setSelection(maLanguage.getText().length());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //get response from server
    public void getResponse(JSONObject outPut, int i) { // response from server
        try {
            if (progressBar != null) {
                progressBar.dismiss(); // dismissing progressbar
            }
            if (i == 0) {  // response for updating profile pic
                pbImageLoading.setVisibility(View.GONE);
                cvEdit.setVisibility(View.VISIBLE);
                if (outPut.has(ResponseParameters.Avatar)) {
                    SharedPreferencesMethod.setString(context, ResponseParameters.Avatar, outPut.getString(ResponseParameters.Avatar));
                }
                Log.e("output", "result " + outPut);
            }
            if (i == 1) { // response for adhar details
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        final Snackbar snackbar = Snackbar.make(cvSave, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else if (outPut.has(ResponseParameters.Success)) {
                        final Snackbar snackbar = Snackbar.make(cvSave, outPut.getString(ResponseParameters.Success), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        Utils.showPopup(this, getResources().getString(R.string.error_text));
                        //Toast.makeText(context, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT).show();
                    }
                    tvAdharSave.setVisibility(VISIBLE);
                    pbAdharLoading.setVisibility(GONE);
                    cvAdharSave.setClickable(true);
                    etAdharCardNumber.setEnabled(true);
                    etConfirmAdharCardNumber.setEnabled(true);
                    etAdharName.setEnabled(true);
                    etAdharDOB.setEnabled(true);
                    etAdharFatherName.setEnabled(true);
                    etAdharPin.setEnabled(true);
                }
            }

            if (i == 2) { // response for update personal info
                setEnable(); // enabling fields after getting response
                if (dialog != null) {
                    dialog.dismiss();
                }
                SharedPreferencesMethod.setString(this, ResponseParameters.PROFILE, "");
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        final Snackbar snackbar = Snackbar.make(cvSave, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        if (outPut.has(ResponseParameters.Success)) {
                            if (outPut.getString(ResponseParameters.Success).equalsIgnoreCase("success")) {
                                final Snackbar snackbar = Snackbar.make(cvSave, outPut.getString(ResponseParameters.Success), Toast.LENGTH_SHORT);
                                snackbar.show();
                                Utils.showPopup(CompleteProfileActivity.this, getString(R.string.success_text));
                                if (outPut.has(ResponseParameters.PersonalInformation)) {
                                    User user = new User();
                                    JSONObject PERSONALINFO = outPut.getJSONObject(ResponseParameters.PersonalInformation);
                                    user.setFirstName(PERSONALINFO.getString(ResponseParameters.FirstName));
                                    user.setMiddleName(PERSONALINFO.getString(ResponseParameters.MiddleName));
                                    user.setLastName(PERSONALINFO.getString(ResponseParameters.LastName));
                                    user.setDOB(PERSONALINFO.getString(ResponseParameters.DateOfBirth));
                                    user.setGender(PERSONALINFO.getString(ResponseParameters.Gender));
                                    user.setEmail(PERSONALINFO.getString(ResponseParameters.Email));
                                    user.setAvatar(PERSONALINFO.getString(ResponseParameters.Avatar));
                                    user.setAddress(PERSONALINFO.getString(ResponseParameters.Address));
                                    user.setCity(PERSONALINFO.getString(ResponseParameters.City));
                                    user.setState(PERSONALINFO.getString(ResponseParameters.State));
                                    user.setZipCode(PERSONALINFO.getString(ResponseParameters.ZIP));
                                    user.setEmailVisibility(PERSONALINFO.getString(ResponseParameters.EMAIL_VISIBILITY));
                                    user.setMobileVisibility(PERSONALINFO.getString(ResponseParameters.MOBILE_VISIBILITY));
                                    user.setAddressVisibility(PERSONALINFO.getString(ResponseParameters.ADDRESS_VISIBILITY));
                                    user.setDobVisibility(PERSONALINFO.getString(ResponseParameters.DOB_VISIBILITY));
                                    user.setGenderVisibility(PERSONALINFO.getString(ResponseParameters.GENDER_VISIBILITY));
                                    if (PERSONALINFO.has(ResponseParameters.AlternateMobile))
                                        if (!PERSONALINFO.getString(ResponseParameters.AlternateMobile).equalsIgnoreCase("null") && PERSONALINFO.getString(ResponseParameters.AlternateMobile) != null)
                                            user.setAlternateMobile(PERSONALINFO.getString(ResponseParameters.AlternateMobile));

                                    if (PERSONALINFO.has(ResponseParameters.PROFILE_SUMMERY))
                                        if (!PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY).equalsIgnoreCase("null") && PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY) != null)
                                            user.setProfileSummary(PERSONALINFO.getString(ResponseParameters.PROFILE_SUMMERY));

                                    if (PERSONALINFO.has(ResponseParameters.PROFILE_HEADLINE)) {
                                        if (!PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE).equalsIgnoreCase("null") && PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE) != null) {
                                            user.setProfileHeadline(PERSONALINFO.getString(ResponseParameters.PROFILE_HEADLINE));
                                        }
                                    }
                                    SharedPreferencesMethod.setUserInfo(this, user); // setting personal info
                                    //fillValues(user);
                                    retrievedObject = null;
                                }
                            } else {
                                final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                                snackbar.show();
                            }
                        } else {
                            final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                            snackbar.show();
                        }
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) {
                    final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) {
                    final Snackbar snackbar = Snackbar.make(cvSave, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            }

            if (i == 3) {
                pbPinCode.setVisibility(GONE);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) {
                        if (outPut.has(ResponseParameters.ZIPCODES)) {
                            if (outPut.getJSONObject(ResponseParameters.ZIPCODES).has(ResponseParameters.State)) {
                                etState.setText(outPut.getJSONObject(ResponseParameters.ZIPCODES).getString(ResponseParameters.State));
                            }
                            if (outPut.getJSONObject(ResponseParameters.ZIPCODES).has(ResponseParameters.DISTRICT)) {
                                etCity.setText(outPut.getJSONObject(ResponseParameters.ZIPCODES).getString(ResponseParameters.DISTRICT));
                            }
                        }
                    } else {
                        etPincode.setText("");
                        enableError(etPincodeLayout, getResources().getString(R.string.zipcode_incorrect_input));
                    }
                } else {
                    etPincode.setText("");
                    enableError(etPincodeLayout, getResources().getString(R.string.zipcode_incorrect_input));
                }
            }
            if (i == 4) {
                pbSkill.setVisibility(GONE);
                pbDSkill.setVisibility(GONE);
                Log.e("output", "result " + outPut);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) {
                        if (outPut.has(ResponseParameters.SKILLS)) {
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.SKILLS, outPut.toString());
                            //sector text
                            setUpIndustriesAdapter(outPut, maIndustries);
                            setUpIndustriesAdapter(outPut, maDIndustries);

                            skills = new ArrayList<>();
                            List<String> selectedList = new ArrayList<>();
                            //sector list
                            List<String> selectedSectorList = new ArrayList<>();
                            String tempText = "";
                            String tempSectorText = "";
                            if (maSkills.hasFocus()) {
                                tempText = maSkills.getText().toString().trim().replace(", ", ",");
                                //sector text
                                tempSectorText = maIndustries.getText().toString().trim().replace(", ", ",");
                            }
                            if (maDSkills.hasFocus()) {
                                tempText = maDSkills.getText().toString().trim().replace(", ", ",");
                                //sector text
                                tempSectorText = maDIndustries.getText().toString().trim().replace(", ", ",");
                            }
                            if (tempText.endsWith(",")) {
                                tempText = tempText.trim().substring(0, tempText.length() - 1);
                            }
                            if (tempSectorText.endsWith(",")) {
                                tempSectorText = tempSectorText.trim().substring(0, tempSectorText.length() - 1);
                            }

                            Log.e("TAG 1", tempText + " " + tempSectorText);
                            if (!tempText.trim().isEmpty())
                                if (!tempText.contains(",")) {
                                    selectedList.add(tempText);
                                } else {
                                    String[] temp = tempText.trim().split(",");
                                    selectedList = Arrays.asList(temp);
                                }
                            Log.e("TAG 2", selectedList.toString());

                            //for sector list
                            if (!tempSectorText.trim().isEmpty())
                                if (!tempSectorText.contains(",")) {
                                    selectedSectorList.add(tempSectorText);
                                } else {
                                    String[] temp = tempSectorText.trim().split(",");
                                    selectedSectorList = Arrays.asList(temp);
                                }
                            Log.e("TAG 3", selectedSectorList.toString());


                            for (int j = 0; j < outPut.getJSONArray(ResponseParameters.SKILLS).length(); j++) {
                               /* if (!skills.contains(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SKILL))
                                        && !selectedList.contains(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SKILL))
                                        && selectedSectorList.contains(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SECTOR)))*/
                                skills.add(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SKILL));
                            }
                            skills.add("Other");
                            String[] data = skills.toArray(new String[skills.size()]);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.autocomplete_item, data);
                            if (maSkills.hasFocus()) {
                                maSkills.setAdapter(adapter);
                                maSkills.setThreshold(1);
                                Utils.addOtherOptionHandler(maSkills, skills, CompleteProfileActivity.this, true);
                             /*   new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        maSkills.showDropDown();
                                    }
                                }, 50);*/
                                if (!maSkills.getText().toString().trim().isEmpty() && !maSkills.getText().toString().trim().endsWith(","))
                                    maSkills.showDropDown();
                                maSkills.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
                            }
                            if (maDSkills.hasFocus()) {
                                maDSkills.setAdapter(adapter);
                                maDSkills.setThreshold(1);
                                Utils.addOtherOptionHandler(maDSkills, skills, CompleteProfileActivity.this, true);
                                if (!maDSkills.getText().toString().trim().isEmpty() && !maDSkills.getText().toString().trim().endsWith(","))
                                    maDSkills.showDropDown();
                                maDSkills.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
                            }
                        }
                    }
                }
            }

            if (i == 5) { //skills
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        final Snackbar snackbar = Snackbar.make(cvSave, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else if (outPut.has(ResponseParameters.Success)) {
                        final Snackbar snackbar = Snackbar.make(cvSave, outPut.getString(ResponseParameters.Success), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        Utils.showPopup(this, getResources().getString(R.string.error_text));
//                        Toast.makeText(context, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT).show();
                    }
                    maIndustries.setEnabled(true);
                    ivIndustries.setClickable(true);
                    maSkills.setEnabled(true);
                    etSkillsDes.setEnabled(true);
                    cvSaveSkills.setClickable(true);
                    tvSaveSkills.setVisibility(VISIBLE);
                    pbSkillsLoading.setVisibility(GONE);
                    setSkillViewsDisable(llAddSkills, true);
                    dontShowDialog = true;
                }
            }

            if (i == 11) { //skills desired
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) {
                        final Snackbar snackbar = Snackbar.make(cvSave, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else if (outPut.has(ResponseParameters.Success)) {
                        final Snackbar snackbar = Snackbar.make(cvSave, outPut.getString(ResponseParameters.Success), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        Utils.showPopup(this, getResources().getString(R.string.error_text));
//                        Toast.makeText(context, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT).show();
                    }
                    maDIndustries.setEnabled(true);
                    ivDIndustries.setClickable(true);
                    maDSkills.setEnabled(true);
                    etDSkillsDes.setEnabled(true);
                    cvSaveDSkills.setClickable(true);
                    tvSaveDSkills.setVisibility(VISIBLE);
                    pbDSkillsLoading.setVisibility(GONE);
                    setSkillViewsDisable(llAddDSkills, true);
                    dontShowDialog = true;
                }
            }
            if (i == 6 || i == 12) { //skills
                pbPinCode.setVisibility(GONE);
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) {
                        if (outPut.has(ResponseParameters.ZIPCODES)) {
                            List<String> roles = new ArrayList<>();
                            for (int j = 0; j < outPut.getJSONArray(ResponseParameters.ZIPCODES).length(); j++) {
                                roles.add(outPut.getJSONArray(ResponseParameters.ZIPCODES).getJSONObject(j).getString(ResponseParameters.ZIP));
                            }
                            String[] data = roles.toArray(new String[roles.size()]);
                            ArrayAdapter<?> adapter = new ArrayAdapter<Object>(this, R.layout.autocomplete_item, data);
                            etPincode.setAdapter(adapter);
                            etPincode.setThreshold(1);
                            if (!etPincode.getText().toString().trim().isEmpty() || i == 12)
                                etPincode.showDropDown();
                        }
                    }
                }
            }
            if (i == 7) {
                try {
                    if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                        if (outPut.has(ResponseParameters.Success)) {
                            if (outPut.has(ResponseParameters.LANGUAGES)) {
                                String langauge = "";
                                for (int j = 0; j < outPut.getJSONArray(ResponseParameters.LANGUAGES).length(); j++) {
                                    langauge = langauge + outPut.getJSONArray(ResponseParameters.LANGUAGES).getJSONObject(j).getString(ResponseParameters.LANGUAGE).trim() + ", ";
                                }
                                SharedPreferencesMethod.setString(CompleteProfileActivity.this, SharedPreferencesMethod.LANGUAGES, langauge.trim());
                            }
                            if (outPut.has(ResponseParameters.PROFILE)) {
                                profile = outPut.getJSONObject(ResponseParameters.PROFILE);
                                fillFromJSON();
                                setWorkRecyclerView();
                                setEducationRecyclerView();
                            }
                            if (outPut.has(ResponseParameters.recommendations)) {
                                recommedations = outPut.getJSONArray(ResponseParameters.recommendations);
                                setRecomRecyclerView(recommedations);
                            }
                            setEditTextWatcher();
                        }
                    } else {
                        setEditTextWatcher();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (i == 8) {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(etPhoneLayout, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else {
                        otp = outPut.getString(ResponseParameters.Otp).trim();// updating otp
                        OtpPopup();
                        /*final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.verify_otp), Toast.LENGTH_SHORT);
                        snackbar.show();*/
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { // error
                    final Snackbar snackbar = Snackbar.make(etPhoneLayout, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { // no net connection
                    final Snackbar snackbar = Snackbar.make(etPhoneLayout, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            }
            if (i == 9) {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error
                        final Snackbar snackbar = Snackbar.make(etPhoneLayout, Utils.getErrorMessage(outPut), Toast.LENGTH_SHORT);
                        snackbar.show();
                    } else if (outPut.has(ResponseParameters.Success)) {
                        SharedPreferencesMethod.setString(CompleteProfileActivity.this, ResponseParameters.MobileNumber, "" + changeMobile.get(RequestParameters.MOBILE));
                        etPhone.setText("" + changeMobile.get(RequestParameters.MOBILE));
                        final Snackbar snackbar = Snackbar.make(etPhoneLayout, getResources().getString(R.string.number_changed_success), Toast.LENGTH_SHORT);
                        snackbar.show();
                    }
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ERROR")) { // error
                    final Snackbar snackbar = Snackbar.make(etPhoneLayout, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                } else if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("NO NETWORK")) { // no net connection
                    final Snackbar snackbar = Snackbar.make(etPhoneLayout, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                    snackbar.show();
                }
            }
            if (i == 10) {
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Errors)) { // error

                    } else if (outPut.has(ResponseParameters.Success)) {
                        if (outPut.has(ResponseParameters.ALL_LANGUAGES)) { //check output have language
                            SharedPreferencesMethod.setString(this, SharedPreferencesMethod.ALL_LANGUAGES, outPut.toString());
                            setUpLanguageAdapter(outPut, maLanguage);
                        }
                    }
                }
            }
            if (i == 20) {//logout
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) {
                        // performLogout();
                        CompleteProfileActivity context = CompleteProfileActivity.this;
                        String url = API.DELETE_ACC + SharedPreferencesMethod.getUserId(context);
                        API.sendRequestToServerPOST_PARAM(context, url, new HashMap<String, Object>());// service call for otp
                    }
                } else {

                }
            }
            if (i == 30) {//delete response
                if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                    if (outPut.has(ResponseParameters.Success)) {
                        performLogout();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void performLogout() {
        SharedPreferencesMethod.clear(this);
        //   SharedPreferencesMethod.setInt(HomeScreenActivity.this, "run", run);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("popup", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent); // open loginactivity
    }


    //for sector
    void setUpIndustriesAdapter(JSONObject outPut, final AutoCompleteTextView multiAutoCompleteTextView) {
        try {
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Success)) {
                    if (outPut.has(ResponseParameters.INDUSTRIES)) {
                        sector = new ArrayList<>();
                        List<String> selectedList = new ArrayList<>();
                        String tempText = multiAutoCompleteTextView.getText().toString().trim().replace(", ", ",");
                        if (tempText.endsWith(",")) {
                            tempText = tempText.trim().substring(0, tempText.length() - 1);
                        }
                        if (!tempText.trim().isEmpty())
                            if (!tempText.contains(",")) {
                                selectedList.add(tempText);
                            } else {
                                String[] temp = tempText.trim().split(",");
                                selectedList = Arrays.asList(temp);
                            }
                        for (int j = 0; j < outPut.getJSONArray(ResponseParameters.INDUSTRIES).length(); j++) {
                            if (!sector.contains(outPut.getJSONArray(ResponseParameters.INDUSTRIES).getJSONObject(j).getString(ResponseParameters.SECTOR).toString().trim())
                                /*&& !selectedList.contains(outPut.getJSONArray(ResponseParameters.SKILLS).getJSONObject(j).getString(ResponseParameters.SECTOR).toString().trim())*/)
                                sector.add(outPut.getJSONArray(ResponseParameters.INDUSTRIES).getJSONObject(j).getString(ResponseParameters.SECTOR).toString().trim());
                        }

                        String[] data = sector.toArray(new String[sector.size()]);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.autocomplete_item, data);
                        multiAutoCompleteTextView.setAdapter(adapter);
                        multiAutoCompleteTextView.setThreshold(1);
                        //multiAutoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void setUpLanguageAdapter(JSONObject outPut, final MultiAutoCompleteTextView multiAutoCompleteTextView) {
        try {
            if (outPut.getString("RP_MESSAGE").equalsIgnoreCase("ALL_OKAY")) {
                if (outPut.has(ResponseParameters.Success)) {
                    if (outPut.has(ResponseParameters.ALL_LANGUAGES)) {
                        languages = new ArrayList<>();
                        List<String> selectedList = new ArrayList<>();
                        String tempText = multiAutoCompleteTextView.getText().toString().trim().replace(", ", ",");
                        if (tempText.endsWith(",")) {
                            tempText = tempText.trim().substring(0, tempText.length() - 1);
                        }
                        if (!tempText.trim().isEmpty())
                            if (!tempText.contains(",")) {
                                selectedList.add(tempText);
                            } else {
                                String[] temp = tempText.trim().split(",");
                                selectedList = Arrays.asList(temp);
                            }
                        for (int j = 0; j < outPut.getJSONArray(ResponseParameters.ALL_LANGUAGES).length(); j++) {
                            if (!languages.contains(outPut.getJSONArray(ResponseParameters.ALL_LANGUAGES).getJSONObject(j).getString(ResponseParameters.LANGUAGE).toString().trim())
                                    && !selectedList.contains(outPut.getJSONArray(ResponseParameters.ALL_LANGUAGES).getJSONObject(j).getString(ResponseParameters.LANGUAGE).toString().trim()))
                                languages.add(outPut.getJSONArray(ResponseParameters.ALL_LANGUAGES).getJSONObject(j).getString(ResponseParameters.LANGUAGE).toString().trim());
                        }
                        String[] data = languages.toArray(new String[languages.size()]);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.autocomplete_item, data);
                        multiAutoCompleteTextView.setAdapter(adapter);
                        multiAutoCompleteTextView.setThreshold(1);
                        multiAutoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearAutoCompleteEditText(AutoCompleteTextView multiAutoCompleteTextView) {
        try {
            String tempText = multiAutoCompleteTextView.getText().toString().trim().replace(", ", ",");
            if (tempText.endsWith(",")) {
                tempText = tempText.trim().substring(0, tempText.length() - 1);
            }
            if (!tempText.contains(",")) {
                multiAutoCompleteTextView.setText("");
            } else {
                String[] temp = tempText.trim().split(",");
                List<String> list = new LinkedList<String>(Arrays.asList(temp));
                list.remove(list.size() - 1);
                StringBuilder sb = new StringBuilder();
                for (String str : list.toArray(new String[list.size()]))
                    sb.append(str).append(", ");
                multiAutoCompleteTextView.setText(sb.substring(0, sb.length() - 1));
                multiAutoCompleteTextView.setSelection(multiAutoCompleteTextView.getText().length());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //override method of picking image
    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            //If you want the Uri.
            //Mandatory to refresh image from Uri.
            //getImageView().setImageURI(null);

            //Setting the real returned image.
            //getImageView().setImageURI(r.getUri());

            //If you want the Bitmap.

            //imvUserPic.setImageBitmap(pickResult.getBitmap());
            beginCrop(pickResult.getUri());
            //r.getPath();
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
//            Toast.makeText(this, pickResult.getError().getMessage(), Toast.LENGTH_LONG).show();
            Utils.showPopup(this, pickResult.getError().getMessage());
        }
    }

    //for cropping image
    private void beginCrop(Uri source) {
        try {
            Uri destination = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/kotumb_profile.jpeg"));
            Crop.of(source, destination).asSquare().start(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void MobilePopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.phone_popup, null);
        dialogBuilder.setView(dialogView);
        final EditText etUserPhone = dialogView.findViewById(R.id.etPhone);
        final TextInputLayout etPhoneLayout = dialogView.findViewById(R.id.etPhoneLayout);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.change_login_info_change_btn), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.setNegativeButton(getResources().getString(R.string.cancel_btn_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        this.mobileAlertDialog = dialogBuilder.create();
        this.mobileAlertDialog.setCancelable(false);
        this.mobileAlertDialog.show();
        this.mobileAlertDialog.getButton(-1).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Boolean.valueOf(false).booleanValue()) {
                    mobileAlertDialog.dismiss();
                }
                etPhoneLayout.setErrorEnabled(false);
                if (etUserPhone.getText().toString().trim().isEmpty()) {
                    etPhoneLayout.setErrorEnabled(true);
                    etPhoneLayout.setError(getResources().getString(R.string.mobile_empty_input));
                } else if (isValidLong(etUserPhone.getText().toString())) {
                    if (etUserPhone.getText().toString().length() < 10) {
                        etPhoneLayout.setError(getResources().getString(R.string.mobile_incorrect_input_minlength));
                    } else if (Utility.isConnectingToInternet(CompleteProfileActivity.this)) {
                        changeMobile = new HashMap<>();
                        changeMobile.put(RequestParameters.MOBILE, "" + etUserPhone.getText().toString().trim());
                        changeMobile.put(RequestParameters.LANG, "" + SharedPreferencesMethod.getDefaultLanguageName(CompleteProfileActivity.this));
                        changeMobile.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(CompleteProfileActivity.this));
                        progressBar = new com.codeholic.kotumb.app.View.ProgressBar(CompleteProfileActivity.this);
                        progressBar.show(CompleteProfileActivity.this.getResources().getString(R.string.app_please_wait_text));
                        Log.e("input", changeMobile.toString());
                        API.sendRequestToServerPOST_PARAM(CompleteProfileActivity.this, API.SEND_OTP, changeMobile);// service call for otp
                        mobileAlertDialog.dismiss();
                    } else {
                        Snackbar.make(etPhoneLayout, getResources().getString(R.string.app_no_internet_error), 0).show();
                    }
                } else {
                    etPhoneLayout.setError(getResources().getString(R.string.mobile_incorrect_input_number));
                }
            }
        });
    }

    private void OtpPopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.otp_popup, null);
        dialogBuilder.setView(dialogView);
        final OtpEditText etOtp = dialogView.findViewById(R.id.etOtp);
        final TextView tvOtpHint = dialogView.findViewById(R.id.tvOtpHint);
        final TextView tvResend = dialogView.findViewById(R.id.tv_resend);
        final TextView tv_timer=dialogView.findViewById(R.id.tv_otp_timer);
        tv_timer.setVisibility(VISIBLE);
        tvResend.setVisibility(GONE);
        final CountDownTimer countDownTimer=new CountDownTimer(30000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tv_timer.setText(String.valueOf(getResources().getString(R.string.otp_timer_text_1)+" sec. "+(int)millisUntilFinished/1000)+" "+getResources().getString(R.string.otp_timer_text_2));
            }

            @Override
            public void onFinish() {
                tv_timer.setVisibility(View.GONE);
                tvResend.setVisibility(View.VISIBLE);
            }
        };
        //timer for otp resend start
        countDownTimer.start();
        etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    if (s.length() >= 6) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(etOtp.getWindowToken(), 0);
                        if (s.toString().equalsIgnoreCase(otp)) { // checking otp
                            if (!Utility.isConnectingToInternet(CompleteProfileActivity.this)) { // check net connection
                                /*final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.no_net), Toast.LENGTH_SHORT);
                                snackbar.show();*/
                                Utils.showPopup(CompleteProfileActivity.this, getResources().getString(R.string.app_no_internet_error));
//                                Toast.makeText(CompleteProfileActivity.this, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            progressBar = new com.codeholic.kotumb.app.View.ProgressBar(CompleteProfileActivity.this);
                            progressBar.show(getResources().getString(R.string.app_please_wait_text));
                            verifyOtp();
                            otpAlertDialog.dismiss();
                        } else {
                            Utils.showPopup(CompleteProfileActivity.this, getResources().getString(R.string.app_no_internet_error));
//                            Toast.makeText(CompleteProfileActivity.this, getResources().getString(R.string.otp_incorrect_input), Toast.LENGTH_SHORT).show();
                            /*final Snackbar snackbar = Snackbar.make(bottomTabLayout, getResources().getString(R.string.check_otp), Toast.LENGTH_SHORT);
                            snackbar.show();*/
                        }
                    }
                    tvOtpHint.setVisibility(GONE);
                } else {
                    tvOtpHint.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!Utility.isConnectingToInternet(CompleteProfileActivity.this)) { //check net connection
                        return;
                    }
                    progressBar = new com.codeholic.kotumb.app.View.ProgressBar(CompleteProfileActivity.this);
                    progressBar.show(getResources().getString(R.string.app_please_wait_text));
                    otpAlertDialog.dismiss();
                    API.sendRequestToServerPOST_PARAM(CompleteProfileActivity.this, API.SEND_OTP, changeMobile); // resend web service call

                } catch (Exception e) {
                    final Snackbar snackbar = Snackbar.make(etPhoneLayout, getResources().getString(R.string.error_text), Toast.LENGTH_SHORT);
                    snackbar.show();
                    e.printStackTrace();
                }
                tvResend.setVisibility(View.GONE);
                tv_timer.setVisibility(View.VISIBLE);
                countDownTimer.start();
            }
        });
        otpAlertDialog = dialogBuilder.create();
        otpAlertDialog.setCancelable(true);
        otpAlertDialog.show();
    }

    //verify otp method
    private void verifyOtp() {
        try {
            if (!Utility.isConnectingToInternet(CompleteProfileActivity.this)) { // check net connection
                if (progressBar != null) {
                    progressBar.dismiss(); // dismissing progressbar
                }
                final Snackbar snackbar = Snackbar.make(etPhoneLayout, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                return;
            }

            API.sendRequestToServerPOST_PARAM(CompleteProfileActivity.this, API.CHANGE_MOBILE, changeMobile);// service call for account verification

        } catch (Exception e) {
            final Snackbar snackbar = Snackbar.make(etPhoneLayout, getResources().getString(R.string.otp_incorrect_input), Toast.LENGTH_SHORT);
            snackbar.show();
            e.printStackTrace();
        }
    }

    //checking for number is valid or not
    public static Boolean isValidLong(String value) {
        try {
            if (value.length() <= 0)
                return false;
            if (value.startsWith("0"))
                return false;
            Long val = Long.valueOf(value);
            return val != null;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private class SkillsObject {
        AutoCompleteTextView autoCompleteTextView;
        TextInputLayout textInputLayout;
        TextView textView;

        public SkillsObject(MultiAutoCompleteTextView maSkills, TextInputLayout maSkillsLayout, TextView tvRemove) {
            this.textView = tvRemove;
            this.autoCompleteTextView = maSkills;
            this.textInputLayout = maSkillsLayout;
        }
    }
}
