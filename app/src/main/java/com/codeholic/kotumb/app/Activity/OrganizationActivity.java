package com.codeholic.kotumb.app.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.AQuery;
import com.codeholic.kotumb.app.Adapter.GalleryAdapter;
import com.codeholic.kotumb.app.Adapter.MyListAdapter;
import com.codeholic.kotumb.app.Interface.OnGetViewListener;
import com.codeholic.kotumb.app.Model.DisplayUserObject;
import com.codeholic.kotumb.app.Model.GalleryData;
import com.codeholic.kotumb.app.Model.PostComments;
import com.codeholic.kotumb.app.Model.SkillDevLikeUserObject;
import com.codeholic.kotumb.app.R;
import com.codeholic.kotumb.app.Utility.API;
import com.codeholic.kotumb.app.Utility.EventNames;
import com.codeholic.kotumb.app.Utility.RequestParameters;
import com.codeholic.kotumb.app.Utility.SharedPreferencesMethod;
import com.codeholic.kotumb.app.Utility.Utility;
import com.codeholic.kotumb.app.Utility.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class OrganizationActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.toolbar_actionbar)
    Toolbar mToolbar;

    @BindView(R.id.ivUser)
    ImageView ivUser;

    @BindView(R.id.header)
    TextView header;

    @BindView(R.id.back)
    LinearLayout back;

    @BindView(R.id.llLike)
    LinearLayout llLike;

    @BindView(R.id.llComment)
    LinearLayout llComment;

    @BindView(R.id.tvLike)
    TextView tvLike;

    @BindView(R.id.tvAbout)
    TextView tvAbout;

    @BindView(R.id.llLoading)
    LinearLayout llLoading;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.llNotFoundLikes)
    TextView llNotFoundLikes;

    @BindView(R.id.content_slide_up_view)
    View sliderView;

    @BindView(R.id.dim)
    View dim;

//    @BindView(R.id.nestedScrollView)
//    NestedScrollView nestedScrollView;

    @BindView(R.id.tvSkillDevLikesCount)
    TextView tvSkillDevLikesCount;


    @BindView(R.id.tvSkillDevCommentsCount)
    TextView tvSkillDevCommentsCount;


    @BindView(R.id.llCommentsLoading)
    LinearLayout llCommentsLoading;

    @BindView(R.id.layout_group_chat_chatbox)
    View layout_group_chat_chatbox;

    @BindView(R.id.slideView)
    View slideView;

    @BindView(R.id.cvProfession)
    CardView cvProfession;


    @BindView(R.id.galleryRecyclerview)
    RecyclerView galleryRecyclerview;

    @BindView(R.id.cvGallery)
    CardView cvGallery;

    @BindView(R.id.activity_part)
    TextView activity_part;


    @BindView(R.id.gallery_part)
    TextView gallery_part;


    @BindView(R.id.gallery_loader)
    ProgressBar gallery_loader;

    @BindView(R.id.swipelayout)
    SwipeRefreshLayout swipelayout;

    private DisplayUserObject userObject;
    private TextView tvSkillDevName;
    private TextView tvSkillDevAddress;
    private TextView tvSkillDevNumber;
    private TextView tvSkillDevEmail;
    private TextView tvSkillDevAddComment;
    private ImageView ivSkillDevImage;
    private boolean isLiked;
    private int pagination;
    private ArrayList<PostComments> commentObjects = new ArrayList<>();
    private ArrayList<SkillDevLikeUserObject> likedUsers = new ArrayList<>();
    private boolean isCommented;
    private PostComments postComment;
    private AlertDialog dialog;
    private TextInputLayout etTitleLayout;
    private EditText etTitle;
    private MyListAdapter commentAdapter;
    private MyListAdapter likesAdapter;
    private boolean isAddingNewComment;
    private boolean flag_loading;
    private ListView listView;
    private View footerView;
    private SlideUp slideUp;
    private int likesPagination;
    private ListView likersListView;
    String  getLikedUserId="";
    GalleryAdapter galleryAdapter;
    List<GalleryData>galleryData=new ArrayList<>();

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.organization_page_layout);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();

        findViews();

        if (data != null) {
            String url = data.toString();
            int lastIndexOf = url.lastIndexOf("/");
            String userId = url.substring(lastIndexOf + 1, url.length());
            swipelayout.setRefreshing(false);
            userObject = new DisplayUserObject();
            userObject.setUserId(userId);
            userObject.setComments_count("0");
            fetchProfile();
            initToolbar();
            setupCommentList();
            fetchComments();
            setupLikestView();
            setupLikersList();
            slideUp.hideImmediately();
            return;
        }

        getDataFromIntent();
        fillValues();
        initToolbar();
        setupCommentList();
        fetchComments();
        setupLikestView();
        setupLikersList();
        setPartView();
        //setListViewHeightBasedOnChildren(listView);
    }







    private void setPartView(){
        cvProfession.setVisibility(VISIBLE);
        cvGallery.setVisibility(GONE);
        activity_part.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        gallery_part.setBackgroundColor(getResources().getColor(R.color.white));
        gallery_part.setTextColor(getResources().getColor(R.color.black));
        gallery_part.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvProfession.setVisibility(GONE);
                cvGallery.setVisibility(VISIBLE);
                gallery_part.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                activity_part.setBackgroundColor(getResources().getColor(R.color.white));
                activity_part.setTextColor(getResources().getColor(R.color.black));
                gallery_part.setTextColor(getResources().getColor(R.color.white));
                LoadGallery();
            }
        });

        activity_part.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryData.clear();
                cvProfession.setVisibility(VISIBLE);
                cvGallery.setVisibility(GONE);
                activity_part.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                gallery_part.setBackgroundColor(getResources().getColor(R.color.white));
                gallery_part.setTextColor(getResources().getColor(R.color.black));
                activity_part.setTextColor(getResources().getColor(R.color.white));
            }
        });
    }

    private void LoadGallery(){
        gallery_loader.setVisibility(VISIBLE);
        galleryRecyclerview.setVisibility(GONE);
        final String url= API.LOAD_GALLERY+userObject.getUserId();
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET,
                url, (String) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println("Gallery Response   "+response);
                gallery_loader.setVisibility(GONE);
                galleryRecyclerview.setVisibility(VISIBLE);
//                if (galleryData.size()!=0){
//                    galleryData.clear();
//                }else{
                    try{
                        for (int i=0; i<response.getJSONArray("gallery").length(); i++){
                            JSONObject results = response.getJSONArray("gallery").getJSONObject(i);
                            GalleryData data = new GalleryData();
                            data.setGalleryId(results.getString("id"));
                            data.setGalleryUserId(results.getString("userId"));
                            data.setGalleryMedia(results.getString("media"));
                            data.setGalleryMediaCreated(results.getString("createdAt"));
                            data.setGalleryMediaUpdated(results.getString("updatedAt"));
                            galleryData.add(data);
                            GridLayoutManager mLayoutManager = new GridLayoutManager(OrganizationActivity.this, 2);
                            galleryRecyclerview.setLayoutManager(mLayoutManager);
                            galleryAdapter = new GalleryAdapter(OrganizationActivity.this, galleryData);
                            galleryRecyclerview.setAdapter(galleryAdapter);
                        }
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
//                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Gallery Response Error  "+error.toString());
            }
        });
        Volley.newRequestQueue(OrganizationActivity.this).add(getRequest);
    }















    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    private void fetchComments() {
        try {
            if (!Utility.isConnectingToInternet(this)) {
                // llLoading.setVisibility(GONE);
                // llNotFound.setVisibility(View.VISIBLE);
                Utils.showPopup(this, getString(R.string.app_no_internet_error), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
                return;
            }
            pagination = 0;
            commentObjects.clear();
            commentAdapter.notifyDataSetChanged();
            llLoading.setVisibility(VISIBLE);
            llNotFound.setVisibility(View.GONE);
            String url = API.SKILL_DEV_COMMENTS + pagination + "/" + SharedPreferencesMethod.getUserId(this) + "/" + userObject.getUserId();
            API.sendRequestToServerGET(this, url, API.SKILL_DEV_COMMENTS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("");
        //setting screen title
        setImageText();
        String url = API.imageUrl_displayUsers + userObject.getAvatar();
        new AQuery(this).id(ivSkillDevImage).image(url, true, true, 300, R.drawable.factory);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void setImageText() {
        String text = userObject.getFirstName() + " " + userObject.getMiddleName() + " " + userObject.getLastName();
        header.setText(text.replaceAll("  "," "));
        String url = API.imageUrl + userObject.getAvatar();
        new AQuery(this).id(ivUser).image(url, true, true, 300, R.drawable.ic_user);
    }

    private void fillValues() {


        Log.e("tag", "Fillvalues");

        String name = userObject.getFirstName() + " " + userObject.getMiddleName() + " " + userObject.getLastName();
        tvSkillDevName.setText(name.replaceAll("  "," "));
        String text = "";
        if (!userObject.getAddress().trim().isEmpty() &&
                !userObject.getCity().trim().isEmpty() &&
                !userObject.getZipcode().trim().isEmpty()) {
            text = userObject.getAddress() + ",\n" + userObject.getCity() + ",\n" + userObject.getZipcode();
        } else if (!userObject.getCity().trim().isEmpty() &&
                !userObject.getZipcode().trim().isEmpty()) {
            text = userObject.getCity() + ",\n" + userObject.getZipcode();
        } else if (!userObject.getZipcode().trim().isEmpty()) {
            text = userObject.getZipcode();
        }
        if (!text.replace(",", "").trim().isEmpty()) {
            tvSkillDevAddress.setText(text);
        } else {
            tvSkillDevAddress.setVisibility(GONE);
        }
        if (!userObject.getEmail().trim().isEmpty()) {
            tvSkillDevEmail.setText(userObject.getEmail() + "");
        } else {
            tvSkillDevEmail.setVisibility(GONE);
        }
        String likes = getString(R.string.like_btn_text);
        String comments = getString(R.string.comment_btn_text);
        tvSkillDevLikesCount.setText(userObject.getLikes_count() + " " + likes);

        tvSkillDevCommentsCount.setText(userObject.getComments_count() + " " + comments);
        if (!userObject.getMobileNumber().trim().isEmpty()) {
            tvSkillDevNumber.setText(userObject.getMobileNumber() + "");
        } else {
            tvSkillDevNumber.setVisibility(GONE);
        }
//        String url = API.imageUrl + userObject.getAvatar().trim();
//        new AQuery(this).id(ivSkillDevImage).image(url, true, true, 300, R.drawable.ic_user);

        //Line Comment By- Arpit Kanda
        tvSkillDevAddComment.setOnClickListener(this);


        if (userObject.getIsLiked().equalsIgnoreCase("true")) {
            isLiked = true;
            Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up_liked);
            tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        } else {
            isLiked = false;
            Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up);
            tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        }

        if (userObject.getIsCommented().size() == 0) {
            isCommented = false;
        } else {
            isCommented = true;
            String string = userObject.getIsCommented().get(0) + "";
            postComment = new Gson().fromJson(string, PostComments.class);
            System.out.println("Get User Comment  "+postComment.id);
        }

        if (userObject.getProfileSummary()!=null && !userObject.getProfileSummary().isEmpty()) {
            tvAbout.setVisibility(VISIBLE);
            tvAbout.setText(userObject.getProfileSummary());
        } else {
            tvAbout.setVisibility(GONE);
        }

    }

    private void findViews() {
        ivSkillDevImage = findViewById(R.id.ivSkillDevImage);
        tvSkillDevName = findViewById(R.id.tvSkillDevName);
        tvSkillDevAddress = findViewById(R.id.tvSkillDevAddress);
        tvSkillDevNumber = findViewById(R.id.tvSkillDevNumber);
        tvSkillDevEmail = findViewById(R.id.tvSkillDevEmail);
        tvSkillDevLikesCount = findViewById(R.id.tvSkillDevLikesCount);
        tvSkillDevCommentsCount = findViewById(R.id.tvSkillDevCommentsCount);
        tvSkillDevAddComment = findViewById(R.id.tvSkillDevAddComment);
        llLike.setOnClickListener(this);
        llComment.setOnClickListener(this);
        tvSkillDevNumber.setOnClickListener(this);

        tvSkillDevLikesCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLikesView();
            }
        });

        tvSkillDevCommentsCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchComments();
            }
        });

        swipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipelayout.setRefreshing(false);
                fetchProfile();
            }
        });

        //  swipelayout.setEnabled(false);
    }

    public void fetchProfile() {
        try {
            if (!Utility.isConnectingToInternet(this)) {
                final Snackbar snackbar = Snackbar.make(llNotFound, getResources().getString(R.string.app_no_internet_error), Toast.LENGTH_SHORT);
                snackbar.show();
                llLoading.setVisibility(GONE);
                llNotFound.setVisibility(View.VISIBLE);
                // textView.setText(getResources().getSuccesMsgString(R.string.app_no_internet_error));
                return;
            }

            pagination = 0;

            // llLoading.setVisibility(VISIBLE);
            llNotFound.setVisibility(View.GONE);
//            cvContainer.setVisibility(View.GONE);

            // displayUserObjects.clear();
            String url = API.SKILL_DEV + pagination + "/" + SharedPreferencesMethod.getUserId(this) + "?displayuser=" + userObject.getUserId();
            System.out.println("Display URL   "+url);
            API.sendRequestToServerGET(this, url, API.SKILL_DEV + 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showLikesView() {
        likedUsers.clear();
        mToolbar.setVisibility(GONE);
        llNotFoundLikes.setVisibility(GONE);
        slideUp.show();
        likesPagination = 0;
        /*this.group_post_comments = new ArrayList<>(post.group_post_comments);*/
        llCommentsLoading.setVisibility(VISIBLE);
        slideView.setVisibility(GONE);
        layout_group_chat_chatbox.setVisibility(GONE);
        //this.group_post_comments = new ArrayList<>();

//        http://dev.codeholic.in/mamta/kotumb/api/v1/services/skill_dev_likes/(offset)/(userid)/(displayuserid)
        String userId = SharedPreferencesMethod.getUserId(OrganizationActivity.this);
        String url = API.SKILL_DEV_LIKES + (likesPagination) + "/" + userId + "/" + userObject.getUserId();
        System.out.println("URL SKILL"+ url);
        API.sendRequestToServerGET(OrganizationActivity.this, url, API.SKILL_DEV_LIKES);// service call for getting messages
    }

    @Override
    public void onBackPressed() {
        if (slideUp.isVisible()) {
            slideUp.hide();
            mToolbar.setVisibility(VISIBLE);
        } else {
            String json = new Gson().toJson(userObject);
            Intent data = new Intent();
            data.putExtra("data", json);
            setResult(RESULT_OK, data);
            super.onBackPressed();
        }
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            String data = intent.getStringExtra("data");
            if (data != null) {
                userObject = new Gson().fromJson(data, DisplayUserObject.class);
                System.out.println("User Obejct Data "+ userObject.getUsername());
            } else {
                Toast.makeText(this, "Data not provided", Toast.LENGTH_SHORT).show();
                finish();
            }
        } else {
            Toast.makeText(this, "No intent provided", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void setupCommentList() {
        listView = findViewById(R.id.listView);
        final String userId = SharedPreferencesMethod.getUserId(OrganizationActivity.this);
        commentAdapter = new MyListAdapter(commentObjects, new OnGetViewListener() {
            @Override
            public View onGetView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.post_comment_item, viewGroup, false);
                ImageView image = v.findViewById(R.id.ivUserImage);
                TextView tvUserName = v.findViewById(R.id.tvUserName);
                TextView tvTime = v.findViewById(R.id.tvTime);
                TextView tvPostContent = v.findViewById(R.id.tvPostContent);
                final LinearLayout llEditPost = v.findViewById(R.id.llEditPost);

                final PostComments commentObj = commentObjects.get(i);
                tvPostContent.setText(Utils.decode(commentObj.comment));
                tvTime.setText(commentObj.updatedAt + "");
                tvUserName.setText(commentObj.firstName + " " + commentObj.lastName);
                tvUserName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        API.openOtherUserProfile(commentObj.userCommentedBy,OrganizationActivity.this);//Open Profile In Comments
                    }
                });

                String url = API.imageUrl + commentObj.avatar;
                AQuery aQuery = new AQuery(OrganizationActivity.this);
                aQuery.id(image).image(url, true, true, 300, R.drawable.ic_user);

                boolean b = commentObj.userId.equalsIgnoreCase(userId);
                boolean b2 = postComment != null && i == 0;
                if (b || b2) {
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showAddCommentUI();
                        }
                    });
                }
                llEditPost.setVisibility(View.GONE);

                // llEditPost.setOnClickListener(getClickListener(llEditPost, commentObj));

                return v;
            }
        });
        listView.setAdapter(commentAdapter);
        setOnLoadMoreListener(listView, new OnGetViewListener() {
            @Override
            public View onGetView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater inflater = getLayoutInflater();
                footerView = inflater.inflate(R.layout.item_loading, viewGroup, false);
                listView.addFooterView(footerView);
                pagination = pagination + 8;
                String url = API.SKILL_DEV_COMMENTS + pagination + "/" + userId + "/" + userObject.getUserId();
                System.out.println("URL New "+url);
                API.sendRequestToServerGET(OrganizationActivity.this, url, API.SKILL_DEV_COMMENTS + 1);
                return null;
            }
        });

    }

    private void setOnLoadMoreListener(final ListView listView, final OnGetViewListener listener) {
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {


            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if (!flag_loading) {
                        flag_loading = true;
                        listener.onGetView(0, null, null);
                    }
                }

                int topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();
                swipelayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });
    }

    private void setupLikersList() {
        likersListView = findViewById(R.id.rvCommentList);
        likesAdapter = new MyListAdapter(likedUsers, new OnGetViewListener() {
            @Override
            public View onGetView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater inflater = getLayoutInflater();
                View v = inflater.inflate(R.layout.user_list_item2, viewGroup, false);
                ImageView image = v.findViewById(R.id.ivUserImage);
                TextView tvUserName = v.findViewById(R.id.tvUserName);

                final SkillDevLikeUserObject skillDevLikeUserObject = likedUsers.get(i);
                System.out.println("User Like Bye "+skillDevLikeUserObject.getUserLikedBy());
                tvUserName.setText(skillDevLikeUserObject.getFirstName() + " " + skillDevLikeUserObject.getLastName());
                String url = API.imageUrl + skillDevLikeUserObject.getAvatar();
                AQuery aQuery = new AQuery(OrganizationActivity.this);
                aQuery.id(image).image(url, true, true, 300, R.drawable.ic_user);
                return v;
            }
        });
        likersListView.setAdapter(likesAdapter);

        setOnLoadMoreListener(likersListView, new OnGetViewListener() {
            @Override
            public View onGetView(int i, View view, ViewGroup viewGroup) {
                LayoutInflater inflater = getLayoutInflater();
                footerView = inflater.inflate(R.layout.item_loading, viewGroup, false);
                likersListView.addFooterView(footerView);
                likesPagination = likesPagination + 8;
                String userId = SharedPreferencesMethod.getUserId(OrganizationActivity.this);
                String url = API.SKILL_DEV_LIKES + likesPagination + "/" + userId + "/" + userObject.getUserId();
                API.sendRequestToServerGET(OrganizationActivity.this, url, API.SKILL_DEV_LIKES + 1);
                return null;
            }
        });
        likersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final SkillDevLikeUserObject skillDevLikeUserObject = likedUsers.get(position);
                API.openOtherUserProfile(skillDevLikeUserObject.getUserLikedBy(),OrganizationActivity.this);//Open Profile In Likes
            }
        });
    }






    @NonNull
    private View.OnClickListener getClickListener(final LinearLayout llEditPost, final PostComments commentObj) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(OrganizationActivity.this, llEditPost);
                MenuInflater menuInflater = popup.getMenuInflater();
                menuInflater.inflate(R.menu.menu_comments, popup.getMenu());
                if (commentObj.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(OrganizationActivity.this))) {
                    popup.getMenu().findItem(R.id.action_edit).setVisible(true);
                    popup.getMenu().findItem(R.id.action_delete).setVisible(true);
                }
                popup.getMenu().findItem(R.id.action_report).setVisible(true);
                popup.setOnMenuItemClickListener(getPopupMenuListener(commentObj));
                popup.show();
            }
        };
    }

    @NonNull
    private PopupMenu.OnMenuItemClickListener getPopupMenuListener(final PostComments commentObj) {
        return new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_edit:
                        showAddCommentUI();
                        break;
                    case R.id.action_delete:
                        String url = API.GROUP_POST_COMMENT_DELETE + commentObj.comment_id + "/" + SharedPreferencesMethod.getUserId(OrganizationActivity.this);
                        Log.e("TAG", "" + url);
                        //showPopUp(activity.getResources().getSuccesMsgString(R.string.delete_comment_dialog_msg), url, API.GROUP_POST_COMMENT_DELETE, position, null);
                        break;
                    case R.id.action_report:
                        HashMap<String, Object> input = new HashMap<>();
                        input.put(RequestParameters.USERID, "" + SharedPreferencesMethod.getUserId(OrganizationActivity.this));
//                        input.put(RequestParameters.GROUPID, "" + group.groupId);
//                        input.put(RequestParameters.POSTID, "" + comment.post_id);
//                        input.put(RequestParameters.COMMENTID, "" + comment.comment_id);
                        //input.put(RequestParameters.DESCRIPTION, "");

                        //  reportPopup(activity.getResources().getSuccesMsgString(R.string.report_comment_btn_text), API.GROUP_POST_COMMENT_REPORT, API.GROUP_POST_COMMENT_REPORT, position, input);
                        break;
                }
                return false;
            }
        };
    }

  /*  public void setOnClickListener(final View view, final PostComments comment, final int position) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(OrganizationActivity.this, view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_comments, popup.getMenu());

                if (comment.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(OrganizationActivity.this))) {
                    popup.getMenu().findItem(R.id.action_edit).setVisible(true);
                    popup.getMenu().findItem(R.id.action_delete).setVisible(true);
                }

                if (comment.role.equalsIgnoreCase("1")
                        && !comment.userId.equalsIgnoreCase(SharedPreferencesMethod.getUserId(OrganizationActivity.this))) {
                    popup.getMenu().findItem(R.id.action_report).setVisible(true);
                    //popup.getMenu().findItem(R.id.action_delete).setVisible(true);
                }
*//*
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                    }
                });*//*

                popup.show();

            }
        });
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSkillDevAddComment:
                showAddCommentUI();
                break;
            case R.id.llLike:
                if (!isLiked) {
                    isLiked = true;
                    Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up_liked);
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                    String url = API.SKILL_DEV_LIKE_UNLIKE + SharedPreferencesMethod.getUserId(this) + "/" + userObject.getUserId() + "/1";
                    API.sendRequestToServerGET(this, url, API.SKILL_DEV_LIKE_UNLIKE);
                    llLike.setEnabled(false);
                    tvLike.setText(getString(R.string.liking_text));
                } else {
                    isLiked = false;
                    Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up);
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                    String url = API.SKILL_DEV_LIKE_UNLIKE + SharedPreferencesMethod.getUserId(this) + "/" + userObject.getUserId() + "/0";
                    API.sendRequestToServerGET(this, url, API.SKILL_DEV_LIKE_UNLIKE);
                    llLike.setEnabled(false);
                    tvLike.setText(getString(R.string.unlike_btn_text));
                }
                break;
            case R.id.llComment:
                showAddCommentUI();
                break;
            case R.id.tvSkillDevNumber:
                String phone = tvSkillDevNumber.getText().toString();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
                break;
        }
    }

    private void showAddCommentUI() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.add_comment_dialog, null);
        dialogBuilder.setView(dialogView);
        etTitle = dialogView.findViewById(R.id.etTitle);
        etTitle.setSelection(etTitle.getText().toString().trim().length());
        etTitleLayout = dialogView.findViewById(R.id.etTitleLayout);
        final LinearLayout llYes = dialogView.findViewById(R.id.llYes);
        final LinearLayout llCancel = dialogView.findViewById(R.id.llCancel);
        final TextView tvTitle = dialogView.findViewById(R.id.Pop_title);
        if (isCommented) {
            tvTitle.setText(getResources().getString(R.string.skill_dev_edit_comment));
            etTitle.setText(Utils.decode(postComment.comment));
        } else {
            tvTitle.setText(getResources().getString(R.string.skill_dev_add_comment));
            isAddingNewComment = true;
        }
        llYes.setOnClickListener(getPostClickListener());
        llCancel.setOnClickListener(getCancelClickListener());
        dialog = dialogBuilder.create();
        dialog.setCancelable(true);
        dialog.show();
    }

    @NonNull
    private View.OnClickListener getCancelClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etTitle.getWindowToken(), 0);
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        };
    }

    @NonNull
    private View.OnClickListener getPostClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comment = etTitle.getText().toString().trim();
                if (!comment.trim().isEmpty()) {
                    String url = API.SKILL_DEV_ADD_COMMENT;
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("userId", SharedPreferencesMethod.getUserId(OrganizationActivity.this));
                    params.put("dpuUserId", userObject.getUserId());
                    params.put("comment", Utils.encode(comment));
                    API.sendRequestToServerPOST_PARAM(OrganizationActivity.this, url, params);
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    Utility.showLoading(OrganizationActivity.this, null);
                } else {
                    etTitleLayout.setError(getString(R.string.cannot_be_empty));
                    // etTitle.setColorFilter(R.color.white);
                }
            }
        };
    }

    public void getResponse(JSONObject outPut, int i) {
        Utility.hideLoading();
        Log.d("Response", String.valueOf(outPut) + " "+i);
        String comments_count = userObject.getComments_count();
        int commentCountInteger = Integer.parseInt(comments_count);
        switch (i) {
            case 0:
                onGetResponseLikeUnLike(outPut);
                break;
            case 1:
                onGetResponseComments(outPut);
                break;
            case 2:
                onGerResponseCommented(outPut, commentCountInteger);
                break;
            case 3:
                onGetResponsecommentsUpdate(outPut);
                break;
            case 4:
                onGetResponseLikers(outPut);
                break;
            case 5:
                onGetResponseLikersUpdate(outPut);
                break;
            case 6:
                boolean has = outPut.has("skill_devs");
                if (has) {
                    try {
                        JSONArray outPutJSONArray = outPut.getJSONArray("skill_devs");
                        ArrayList<DisplayUserObject> userObjects = Utility.getDisplayUserObjects(outPutJSONArray);
                        userObject = userObjects.get(0);
                        fillValues();
                        fetchComments();
                        initToolbar();

                        Bundle bundle = new Bundle();
                        bundle.putString("UserId", SharedPreferencesMethod.getUserId(this));
                        bundle.putString("DisplayUserId", userObject.getUserId());
                        Utils.logEvent(EventNames.SKILL_DEV_PROFILE_VIEW, bundle, this);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    private void onGetResponseLikersUpdate(JSONObject outPut) {
        likersListView.removeFooterView(footerView);
        if (outPut.toString().contains("success")) {
            try {
                JSONArray array = outPut.getJSONArray("skill_devs_likes");
                likedUsers.addAll(Utility.getLikedUserObjects(array));
                likesAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void onGetResponseLikers(JSONObject outPut) {
        llCommentsLoading.setVisibility(GONE);
        if (outPut.toString().contains("success")) {
            try {
                likedUsers.clear();
                JSONArray array = outPut.getJSONArray("skill_devs_likes");
                likedUsers.addAll(Utility.getLikedUserObjects(array));
                likesAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (likedUsers.size() == 0) {
                llNotFoundLikes.setVisibility(VISIBLE);
            }
        } else {
            llNotFoundLikes.setVisibility(VISIBLE);
        }
    }

    private void onGetResponsecommentsUpdate(JSONObject outPut) {
        listView.removeFooterView(footerView);
        if (outPut.toString().contains("success")) {

            logEventCommented(true);
            try {
                JSONArray array = outPut.getJSONArray("skill_devs_comments");
                ArrayList<PostComments> comments = Utility.getPostCommentsObjects(array);
                commentObjects.addAll(comments);
                int index = contains(postComment.d_comment_id, 1);
                if (index > 0) {
                    commentObjects.remove(index);
                }
                commentAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void logEventCommented(boolean value) {
        Bundle bundle = new Bundle();
        bundle.putString("UserId", SharedPreferencesMethod.getUserId(this));
        bundle.putString("DisplayUserId", userObject.getUserId());
        bundle.putBoolean("CommentUpdated", value);
        Utils.logEvent(EventNames.SKILL_COMMENT, bundle, this);
    }

    private void onGerResponseCommented(JSONObject outPut, int commentCountInteger) {
        if (outPut.toString().contains("success")) {

            logEventCommented(false);

            String string = getSuccesMsgString();
            Utils.showPopup(OrganizationActivity.this, string);
            if (postComment == null) {
                try {
                    postComment = getPostCommentObject(outPut);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Utils.showPopup(OrganizationActivity.this, getString(R.string.something_wrong));
                }
            } else {
                updateUserObjectwithNewCommentObject(outPut);
            }
            String comment = etTitle.getText().toString();
            postComment.comment = comment;
            if (isAddingNewComment) {
                commentObjects.add(0, postComment);
                llNotFound.setVisibility(GONE);
                isAddingNewComment = false;
                userObject.setComments_count((commentCountInteger + 1) + "");
                fillValues();
            }
            commentObjects.set(0, postComment);
            commentAdapter.notifyDataSetChanged();
        } else {
            Utils.showPopup(OrganizationActivity.this, getString(R.string.something_wrong));
        }
    }

    private void updateUserObjectwithNewCommentObject(JSONObject outPut) {
        try {
            JSONArray jsonArray = outPut.getJSONArray("skill_devs_comments");
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            JsonArray jsonArrayGson = new JsonParser().parse(String.valueOf(jsonArray)).getAsJsonArray();
//                    isCommented.add(new Gson().toJson(jsonObject));
            userObject.setIsCommented(jsonArrayGson);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    private String getSuccesMsgString() {
        String string;
        if (isAddingNewComment) {
            string = getString(R.string.skill_dev_comment_success);
        } else {
            string = getString(R.string.skill_dev_comment_update_success);
        }
        return string;
    }

    private PostComments getPostCommentObject(JSONObject outPut) throws JSONException {
        PostComments postComment = new PostComments();
        JSONArray jsonArray = outPut.getJSONArray("skill_devs_comments");
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        JsonArray jsonArrayGson = new JsonParser().parse(String.valueOf(jsonArray)).getAsJsonArray();
//                    isCommented.add(new Gson().toJson(jsonObject));
        userObject.setIsCommented(jsonArrayGson);
        System.out.println("Get Post Comment Json  "+jsonArrayGson.toString());
        postComment.firstName = jsonObject.getString("firstName");
        postComment.lastName = jsonObject.getString("lastName");
        postComment.avatar = jsonObject.getString("avatar");
        postComment.d_comment_id = jsonObject.getString("d_comment_id");
        postComment.userCommentedBy = jsonObject.getString("userCommentedBy");
        postComment.userId = jsonObject.getString("userId");
        return postComment;
    }

    private void onGetResponseComments(JSONObject outPut) {
        llLoading.setVisibility(View.GONE);
        if (outPut.toString().contains("success")) {
            try {
                commentObjects.clear();
                JSONArray array = outPut.getJSONArray("skill_devs_comments");
                commentObjects.addAll(Utility.getPostCommentsObjects(array));
                int index = contains(postComment.d_comment_id, 0);
                if (index >= 0) {
                    commentObjects.remove(index);
                }
                if (postComment != null) {
                    commentObjects.add(0, postComment);
                }
                commentAdapter.notifyDataSetChanged();
                /*if (isAddingNewComment) {
                    userObject.setComments_count((commentCountInteger + 1) + "");
                    isAddingNewComment = false;
                }*/
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            llNotFound.setVisibility(VISIBLE);
        }
    }

    private void onGetResponseLikeUnLike(JSONObject outPut) {
        if (outPut.toString().contains("success")) {
            tvLike.setText(isLiked ? "Liked" : "Like");
            llLike.setEnabled(true);
            if (isLiked) {
                userObject.setLikes_count((Integer.parseInt(userObject.getLikes_count()) + 1) + "");
                userObject.setIsLiked("true");
                logLikeEvent(true);
            } else {
                userObject.setLikes_count((Integer.parseInt(userObject.getLikes_count()) - 1) + "");
                userObject.setIsLiked("false");
                logLikeEvent(false);
            }
            fillValues();
        } else {
            tvLike.setText(!isLiked ? "Liked" : "Like");
            llLike.setEnabled(true);
            isLiked = !isLiked;
        }
    }

    private void logLikeEvent(boolean b) {
        Bundle bundle = new Bundle();
        bundle.putString("UserId", SharedPreferencesMethod.getUserId(this));
        bundle.putString("DisplayUserId", userObject.getUserId());
        bundle.putBoolean("Liked", b);
        Utils.logEvent(EventNames.SKILL_LIKE, bundle, this);
    }

    public void setupLikestView() {
        slideUp = new SlideUpBuilder(sliderView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                        dim.setAlpha(1 - (percent / 100));
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        if (visibility == GONE) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                Window window = getWindow();
                                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
                            }
                        } else if (visibility == VISIBLE) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                Window window = getWindow();
                                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                window.setStatusBarColor(getResources().getColor(R.color.dimBg));
                            }
                        }
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                //.withInterpolator(new BounceInterpolator(0.3, 35))
                .withStartState(SlideUp.State.HIDDEN)
                .withTouchableAreaDp(1000)
                .withAutoSlideDuration(500)
                .build();
    }

    private int contains(String id, int startFrom) {
        int index = 0;
        for (PostComments comments : commentObjects) {
            if (comments.d_comment_id.equalsIgnoreCase(id) && index >= startFrom) {
                return index;
            }
            index++;
        }
        return -1;
    }
}
