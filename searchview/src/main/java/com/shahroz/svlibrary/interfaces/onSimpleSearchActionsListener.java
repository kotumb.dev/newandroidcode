package com.shahroz.svlibrary.interfaces;


public interface onSimpleSearchActionsListener {
    void onItemClicked(String item);
    void onScroll();
    void error(String localizedMessage);
}
